

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetBatchAmounts]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetBatchAmounts]
  
create procedure [dbo].[RmsGetBatchAmounts]
                @iHdrTable      VARCHAR(255),
                @iLineTable     VARCHAR(255),
                @iStartDate     DateTime,
                @iEndDate       DateTime,
                @iRmsServerName VARCHAR(255),
                @iHqDbName      VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lBatchTable VARCHAR(255)
  
  DECLARE  @lTenderTotalsTable VARCHAR(255)
  
  SELECT @lBatchTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Batch as Batch'
  
  SELECT @lTenderTotalsTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.TenderTotals as TenderTotals'
  
  EXEC( 'INSERT INTO ' + @iHdrTable + '(RmsStoreID,  RmsBatchNumber,  RmsPaidOut,  RmsDebitSurcharge,  RmsCashBackFee,  DATE1)  SELECT  Batch.StoreID,  Batch.BatchNumber,  Batch.PaidOut,  Batch.DebitSurcharge,  Batch.CashBackSurcharge,  CONVERT(datetime,CONVERT(VARCHAR,Batch.ClosingTime,111))  FROM ' + @lBatchTable + ' WHERE   ((Batch.PaidOut <> 0  OR  Batch.DebitSurcharge <> 0  OR   Batch.CashBackSurcharge <> 0)  OR  EXISTS   (SELECT   1   FROM ' + @lTenderTotalsTable + ' WHERE   TenderTotals.[Close] - TenderTotals.[Open] - TenderTotals.Shift <> 0  AND  TenderTotals.StoreID = Batch.StoreID  AND  TenderTotals.BatchNumber = Batch.BatchNumber))  AND   (Batch.Status = 1  OR  Batch.Status = 3  OR  Batch.Status = 5  OR  Batch.Status = 7  OR  Batch.Status = 9  OR  Batch.Status = 11  OR  Batch.Status = 13  OR  Batch.Status = 15  OR  Batch.Status = 17  OR  Batch.Status = 19  OR  Batch.Status = 21  OR  Batch.Status = 23  OR  Batch.Status = 25  OR  Batch.Status = 27  OR  Batch.Status = 29  OR  Batch.Status = 31)  AND  CONVERT(DATETIME,CONVERT(VARCHAR,Batch.ClosingTime,111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  NOT EXISTS(  SELECT   1   FROM   RMS30300   WHERE   Batch.BatchNumber = RMS30300.RmsBatchNumber  AND   Batch.StoreID = RMS30300.RmsStoreID)')
  
  EXEC( 'INSERT INTO ' + @iLineTable + ' (RmsStoreID,  RmsBatchNumber,  RmsTenderID,  RmsOverShortAmount)  SELECT  TenderTotals.StoreID,  TenderTotals.BatchNumber,  TenderTotals.TenderID,  TenderTotals.[Close] - TenderTotals.[Open] - TenderTotals.Shift  FROM ' + @lTenderTotalsTable + ' WHERE   TenderTotals.[Close] - TenderTotals.[Open] - TenderTotals.Shift <> 0  AND  BatchNumber IN(  SELECT   BatchNumber   FROM ' + @iHdrTable + ')')

