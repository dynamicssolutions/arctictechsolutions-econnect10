

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAProjectFee]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAProjectFee]
  
create procedure [dbo].[taPAProjectFee]
                @I_vPAPROJNUMBER         char(15),
                @I_vPAFeeID              char(15),
                @I_vPALineItemSeq        int  = 0,
                @I_vPAFeeAmount          numeric(19,5)  = 0,
                @I_vPAPercent_Cost       numeric(19,5)  = 0,
                @I_vPAPercent_Revenue    numeric(19,5)  = 0,
                @I_vPA_Retention_Percent numeric(19,5)  = 0,
                @I_vPAService_Fee_Amount numeric(19,5)  = 0,
                @I_vPAbilltaxscheduleid  char(15)  = '',
                @I_vUpdateExisting       tinyint  = 0,
                @I_vRequesterTrx         smallint  = 0,
                @I_vUSRDEFND1            char(50)  = '',
                @I_vUSRDEFND2            char(50)  = '',
                @I_vUSRDEFND3            char(50)  = '',
                @I_vUSRDEFND4            varchar(8000)  = '',
                @I_vUSRDEFND5            varchar(8000)  = '',
                @O_iErrorState           int  output,
                @oErrString              varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @PAFeeID                                char(15),
           @PAFeeName                              char(31),
           @PALineItemSeq                          int,
           @PAFeeType                              smallint,
           @PAFeeAmount                            numeric(19,5),
           @PAPercent_Cost                         numeric(19,5),
           @PAPercent_Revenue                      numeric(19,5),
           @PA_Retention_Percent                   numeric(19,5),
           @PAService_Fee_Amount                   numeric(19,5),
           @PAFeeToUse                             smallint,
           @PAbilltaxscheduleid                    char(15),
           @PASales_Tax_Options                    smallint,
           @PAbegindate                            datetime,
           @PAEnDate                               datetime,
           @PAFrequency                            smallint,
           @STRTDATE                               datetime,
           @ENDDATE                                datetime,
           @PARenew                                smallint,
           @PARenewal_Date                         datetime,
           @PATotAmt                               numeric(19,5),
           @TM_AR_Fee_SRC                          smallint,
           @TM_Prj_Deferred_Rev_Fee_               smallint,
           @TM_Prj_Rev_Fee_SRC                     smallint,
           @PAFF_AR_Fee_SRC                        smallint,
           @FF_Prj_Billings_Fee_SRC                smallint,
           @FF_Prj_Rev_Fee_SRC                     smallint,
           @FF_Retention_Fee_SRC                   smallint,
           @FF_WIP_Fee_SRC                         smallint,
           @FF_Prj_Deferred_Rev_Fee_               smallint,
           @FF_BIEE_Fee_SRC                        smallint,
           @FF_EIEB_Fee_SRC                        smallint,
           @PAtotcbts                              smallint,
           @PAtotcbEL                              smallint,
           @PAtotcbML                              smallint,
           @PAtotcbvi                              smallint,
           @PAtotcber                              smallint,
           @PAtotcbinv                             smallint,
           @PA_Renew_Day                           int,
           @PA_Renew_Month                         int,
           @PAPROJNUMBER                           char(15),
           @PAProjectType                          smallint,
           @PAAcctgMethod                          smallint,
           @PACONTNUMBER                           char(11),
           @iStatus                                int,
           @iError                                 int,
           @iCustomState                           int,
           @O_oErrorState                          int,
           @PASTAT                                 smallint,
           @PATRKCHGORDS                           smallint,
           @PA_Prev_Fee_Percent                    numeric(19,5),
           @PAUD1                                  char(21),
           @PAUD2                                  char(21),
           @PACHGORDNO                             char(17),
           @PAbillnoteidx                          int,
           @CUSTNMBR                               char(17),
           @userdate                               datetime,
           @iUpdDistErrState                       int,
           @YEAR1                                  int,
           @PATU                                   int,
           @l_Period                               int,
           @fClosed                                int,
           @nErr                                   int,
           @sumFeeAmt                              decimal(19,5),
           @iglCalculateGLPeriodErrState           int,
           @Existing_PAPROJNUMBER                  char(15),
           @Existing_PAFeeAmount                   int,
           @PA_Create_Fee_Periodic_RecordsErrState int,
           @DECPLCUR                               smallint,
           @PA_MC_Fee_Amount                       numeric(19,5),
           @PA_MC_Service_Fee_Amount               numeric(19,5),
           @PA_MC_Total_Amount                     numeric(19,5)
  
  select @PAFeeID = '',
         @PAFeeName = '',
         @PALineItemSeq = 0,
         @PAFeeType = 0,
         @PAFeeAmount = 0,
         @PAPercent_Cost = 0,
         @PAPercent_Revenue = 0,
         @PA_Retention_Percent = 0,
         @PAFeeToUse = 0,
         @PAbilltaxscheduleid = '',
         @PASales_Tax_Options = 0,
         @PAbegindate = '',
         @PAEnDate = '',
         @PAFrequency = 0,
         @STRTDATE = '',
         @ENDDATE = '',
         @PARenew = 0,
         @PARenewal_Date = '',
         @PATotAmt = 0,
         @TM_AR_Fee_SRC = 0,
         @TM_Prj_Deferred_Rev_Fee_ = 0,
         @TM_Prj_Rev_Fee_SRC = 0,
         @PAFF_AR_Fee_SRC = 0,
         @FF_Prj_Billings_Fee_SRC = 0,
         @FF_Prj_Rev_Fee_SRC = 0,
         @FF_Retention_Fee_SRC = 0,
         @FF_WIP_Fee_SRC = 0,
         @FF_Prj_Deferred_Rev_Fee_ = 0,
         @FF_BIEE_Fee_SRC = 0,
         @FF_EIEB_Fee_SRC = 0,
         @PAtotcbts = 0,
         @PAtotcbEL = 0,
         @PAtotcbML = 0,
         @PAtotcbvi = 0,
         @PAtotcber = 0,
         @PAtotcbinv = 0,
         @PA_Renew_Day = 0,
         @PA_Renew_Month = 0,
         @PAService_Fee_Amount = 0,
         @PAPROJNUMBER = '',
         @PAProjectType = 0,
         @PAAcctgMethod = 0,
         @PACONTNUMBER = '',
         @iStatus = 0,
         @iError = 0,
         @iCustomState = 0,
         @O_oErrorState = 0,
         @PASTAT = 0,
         @PATRKCHGORDS = 0,
         @PA_Prev_Fee_Percent = 0,
         @PAUD1 = '',
         @PAUD2 = '',
         @PACHGORDNO = '',
         @PAbillnoteidx = 0,
         @CUSTNMBR = '',
         @userdate = '',
         @iUpdDistErrState = 0,
         @YEAR1 = 0,
         @PATU = 0,
         @l_Period = 0,
         @fClosed = 0,
         @nErr = 0,
         @sumFeeAmt = 0,
         @iglCalculateGLPeriodErrState = 0,
         @Existing_PAPROJNUMBER = '',
         @Existing_PAFeeAmount = 0,
         @PA_Create_Fee_Periodic_RecordsErrState = 0,
         @DECPLCUR = 0,
         @PA_MC_Fee_Amount = 0,
         @PA_MC_Service_Fee_Amount = 0,
         @PA_MC_Total_Amount = 0,
         @O_iErrorState = 0
  
  exec @iStatus = taPAProjectFeePre
    @I_vPAPROJNUMBER output ,
    @I_vPAFeeID output ,
    @I_vPALineItemSeq output ,
    @I_vPAFeeAmount output ,
    @I_vPAPercent_Cost output ,
    @I_vPAPercent_Revenue output ,
    @I_vPA_Retention_Percent output ,
    @I_vPAService_Fee_Amount output ,
    @I_vPAbilltaxscheduleid output ,
    @I_vUpdateExisting output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @O_iErrorState = 6187
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAPROJNUMBER is null 
       or @I_vPAFeeID is null
       or @I_vPALineItemSeq is null
       or @I_vPAFeeAmount is null
       or @I_vPAbilltaxscheduleid is null)
    begin
      select @O_iErrorState = 6188
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAPROJNUMBER = ''
       or @I_vPAFeeID = '')
    begin
      select @O_iErrorState = 6189
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vPAPROJNUMBER = upper(@I_vPAPROJNUMBER),
         @I_vPAFeeID = upper(@I_vPAFeeID),
         @I_vPAbilltaxscheduleid = upper(@I_vPAbilltaxscheduleid)
  
  if (@I_vPAPROJNUMBER <> '')
    begin
      select @PAPROJNUMBER = PAPROJNUMBER,
             @PAProjectType = PAProjectType,
             @PAAcctgMethod = PAAcctgMethod,
             @PACONTNUMBER = PACONTNUMBER,
             @PAbegindate = PABBeginDate,
             @PAEnDate = PABEndDate,
             @CUSTNMBR = CUSTNMBR
      from   PA01201 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@PAPROJNUMBER = '')
        begin
          select @O_iErrorState = 6190
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@PASTAT = 3)
           or (@PASTAT = 5))
        begin
          select @O_iErrorState = 6296
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @DECPLCUR = DECPLCUR
  from   PA41701
  WHERE  PAsetupkey = 1
  
  if (@I_vPAFeeID <> '')
    begin
      if (@PAFeeID = '')
        begin
          select @PAFeeID = PAFeeID,
                 @PAFeeName = PAFeeName,
                 @PAFeeType = PAFeeType,
                 @PAFrequency = PAFrequency,
                 @PAFeeAmount = PAFeeAmount,
                 @PAPercent_Cost = PAPercent_Cost,
                 @PAPercent_Revenue = PAPercent_Revenue,
                 @PA_Retention_Percent = PA_Retention_Percent,
                 @PAFeeToUse = PAFeeToUse,
                 @PAbilltaxscheduleid = PAbilltaxscheduleid,
                 @PASales_Tax_Options = PASales_Tax_Options,
                 @STRTDATE = STRTDATE,
                 @ENDDATE = ENDDATE,
                 @PARenew = PARenew,
                 @PARenewal_Date = PARenewal_Date,
                 @PAtotcbts = @PAtotcbts,
                 @PAtotcbEL = @PAtotcbEL,
                 @PAtotcbML = @PAtotcbML,
                 @PAtotcbvi = @PAtotcbvi,
                 @PAtotcber = @PAtotcber,
                 @PAtotcbinv = @PAtotcbinv,
                 @PA_Renew_Day = @PA_Renew_Day,
                 @PA_Renew_Month = @PA_Renew_Month
          from   PA00401 (nolock)
          where  PAFeeID = @I_vPAFeeID
          
          if (@PAFeeID = '')
            begin
              select @O_iErrorState = 6191
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if (@PAFeeType = 1)
            select @TM_Prj_Rev_Fee_SRC = TM_Prj_Rev_Fee_SRC,
                   @TM_AR_Fee_SRC = TM_AR_Fee_SRC,
                   @FF_WIP_Fee_SRC = FF_WIP_Fee_SRC,
                   @FF_Prj_Billings_Fee_SRC = FF_Prj_Billings_Fee_SRC,
                   @PAFF_AR_Fee_SRC = PAFF_AR_Fee_SRC,
                   @FF_Prj_Rev_Fee_SRC = FF_Prj_Rev_Fee_SRC,
                   @FF_BIEE_Fee_SRC = FF_BIEE_Fee_SRC,
                   @FF_EIEB_Fee_SRC = FF_EIEB_Fee_SRC
            from   PA41701 (nolock)
            where  PAsetupkey = 1
          
          if (@PAFeeType = 2)
            select @TM_AR_Fee_SRC = TM_AR_Fee_SRC,
                   @TM_Prj_Deferred_Rev_Fee_ = TM_Prj_Deferred_Rev_Fee_,
                   @PAFF_AR_Fee_SRC = PAFF_AR_Fee_SRC,
                   @FF_Prj_Deferred_Rev_Fee_ = FF_Prj_Deferred_Rev_Fee_
            from   PA41701 (nolock)
            where  PAsetupkey = 1
          
          if (@PAFeeType = 3)
            select @FF_Retention_Fee_SRC = FF_Retention_Fee_SRC,
                   @PAFF_AR_Fee_SRC = PAFF_AR_Fee_SRC
            from   PA41701 (nolock)
            where  PAsetupkey = 1
          
          if (@PAFeeType = 4)
            select @TM_Prj_Rev_Fee_SRC = TM_Prj_Rev_Fee_SRC,
                   @TM_AR_Fee_SRC = TM_AR_Fee_SRC,
                   @TM_Prj_Deferred_Rev_Fee_ = TM_Prj_Deferred_Rev_Fee_,
                   @FF_BIEE_Fee_SRC = FF_BIEE_Fee_SRC,
                   @FF_EIEB_Fee_SRC = FF_EIEB_Fee_SRC
            from   PA41701 (nolock)
            where  PAsetupkey = 1
        end
    end
  
  if (@I_vPA_Retention_Percent <> 0)
     and (@PAFeeType = 2)
    begin
      select @O_iErrorState = 6192
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPA_Retention_Percent <> 0)
       or (@I_vPAPercent_Cost <> 0)
       or (@I_vPAPercent_Revenue <> 0)
       or (@I_vPAService_Fee_Amount <> 0))
     and (@PAFeeType = 2)
    begin
      select @O_iErrorState = 6193
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPAFeeAmount <> 0)
       or (@I_vPAPercent_Cost <> 0)
       or (@I_vPAPercent_Revenue <> 0)
       or (@I_vPAService_Fee_Amount <> 0))
     and (@PAFeeType = 3)
    begin
      select @O_iErrorState = 6194
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPA_Retention_Percent <> 0)
       or (@I_vPAPercent_Cost <> 0)
       or (@I_vPAPercent_Revenue <> 0))
     and (@PAFeeType = 4)
    begin
      select @O_iErrorState = 6195
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAService_Fee_Amount = 0)
     and (@I_vPAFeeAmount <> 0)
     and (@PAFeeType = 4)
    begin
      select @O_iErrorState = 7119
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PAProjectType = 1)
     and (@PAFeeType = 3)
    begin
      select @O_iErrorState = 6196
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PAProjectType = 2)
     and (@PAFeeType = 4)
    begin
      select @O_iErrorState = 6197
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PAProjectType = 3)
     and (@PAFeeType = 4)
    begin
      select @O_iErrorState = 6198
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPALineItemSeq = 0)
     and (@PAFeeType <> 2)
     and (@PAFeeType <> 3)
    begin
      select @I_vPALineItemSeq = isnull(max(PALineItemSeq),0) + 100
      from   PA02101 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PAFeeID = @I_vPAFeeID
    end
  
  if ((@PAFeeType = 2)
       or (@PAFeeType = 3))
     and (@I_vPALineItemSeq <> 0)
    begin
      select @O_iErrorState = 6426
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vUpdateExisting = 0)
    begin
      if exists (select 1
                 from   PA02101 (nolock)
                 where  PAPROJNUMBER = @I_vPAPROJNUMBER
                        and PALineItemSeq = @I_vPALineItemSeq
                        and PAFeeID = @I_vPAFeeID)
        begin
          select @O_iErrorState = 6199
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      Select @Existing_PAPROJNUMBER = PAPROJNUMBER,
             @Existing_PAFeeAmount = PAFeeAmount
      from   PA02101
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PALineItemSeq = @I_vPALineItemSeq
             and PAFeeID = @I_vPAFeeID
    end
  
  if (@I_vRequesterTrx < 0)
      or (@I_vRequesterTrx > 1)
    begin
      select @O_iErrorState = 6200
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PAFrequency < 1)
      or (@PAFrequency > 3)
    begin
      select @O_iErrorState = 4899
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PAFrequency <> 1)
     and (@PAFeeType = 2)
    begin
      select @O_iErrorState = 4900
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PAFrequency = 1)
     and (@PAFeeType = 3)
    begin
      select @O_iErrorState = 4901
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PAFrequency <> 1)
     and (@PAFeeType = 4)
    begin
      select @O_iErrorState = 4902
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PAFeeToUse = 0)
     and (@PAFeeType <> 3)
     and ((@PAFrequency = 3)
           or (@PAFrequency = 4))
    begin
      select @O_iErrorState = 4903
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAbilltaxscheduleid <> '')
    begin
      if (not exists (select 1
                      from   TX00101 (nolock)
                      where  TAXSCHID = @I_vPAbilltaxscheduleid))
        begin
          select @O_iErrorState = 4904
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      select @I_vPAbilltaxscheduleid = @PAbilltaxscheduleid
    end
  
  if (@PASales_Tax_Options = 1
      and @I_vPAbilltaxscheduleid = '')
    begin
      select @O_iErrorState = 4905
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @userdate = getdate()
  
  execute @iStatus = glCalculateGLPeriod
    7 ,
    'Timesheet Entry' ,
    @userdate ,
    @userdate ,
    @l_Period output ,
    @fClosed output ,
    @YEAR1 output ,
    @nErr output ,
    @iglCalculateGLPeriodErrState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iglCalculateGLPeriodErrState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iglCalculateGLPeriodErrState))
      
      select @O_iErrorState = 4906
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@fClosed = 1)
    begin
      select @O_iErrorState = 4907
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @sumFeeAmt = isnull(sum(PAFeeAmount),0)
  from   PA05200 (nolock)
  where  PAPROJNUMBER = @I_vPAPROJNUMBER
         and PAFeeID = @I_vPAFeeID
  
  if (@sumFeeAmt <> @I_vPAFeeAmount)
    begin
      select @O_iErrorState = 4908
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  if (@I_vUpdateExisting = 0)
    begin
      insert PA00402
            (PAPROJNUMBER,
             PAFeeID,
             PAFeeType,
             PAFeeAmount)
      select @I_vPAPROJNUMBER,
             @I_vPAFeeID,
             @PAFeeType,
             @I_vPAFeeAmount
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5261
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      update PA00402
      set    PAFeeAmount = @I_vPAFeeAmount
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PAFeeID = @I_vPAFeeID
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6427
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@Existing_PAPROJNUMBER <> '')
    begin
      update PA01101
      set    PAProject_Amount = PAProject_Amount - @Existing_PAFeeAmount,
             PAProject_Fee_Amount = PAProject_Fee_Amount - @Existing_PAFeeAmount
      where  PACONTNUMBER = @PACONTNUMBER
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6532
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      update PA01201
      set    PAProject_Amount = PAProject_Amount - @Existing_PAFeeAmount,
             PAProject_Fee_Amount = PAProject_Fee_Amount - @Existing_PAFeeAmount
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6533
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      update PA00501
      set    PAProject_Amount = PAProject_Amount - @Existing_PAFeeAmount,
             PAProject_Fee_Amount = PAProject_Fee_Amount - @Existing_PAFeeAmount
      where  CUSTNMBR = @CUSTNMBR
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6534
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  update PA01101
  set    PAProject_Amount = PAProject_Amount + @I_vPAFeeAmount,
         PAProject_Fee_Amount = PAProject_Fee_Amount + @I_vPAFeeAmount
  where  PACONTNUMBER = @PACONTNUMBER
  
  if @@error <> 0
    begin
      select @O_iErrorState = 6297
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  update PA01201
  set    PAProject_Amount = PAProject_Amount + @I_vPAFeeAmount,
         PAProject_Fee_Amount = PAProject_Fee_Amount + @I_vPAFeeAmount
  where  PAPROJNUMBER = @I_vPAPROJNUMBER
  
  if @@error <> 0
    begin
      select @O_iErrorState = 6298
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  update PA00501
  set    PAProject_Amount = PAProject_Amount + @I_vPAFeeAmount,
         PAProject_Fee_Amount = PAProject_Fee_Amount + @I_vPAFeeAmount
  where  CUSTNMBR = @CUSTNMBR
  
  if @@error <> 0
    begin
      select @O_iErrorState = 6299
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@Existing_PAPROJNUMBER = '')
    begin
      insert PA02101
            (PAPROJNUMBER,
             PALineItemSeq,
             PAFeeID,
             PAFeeName,
             PAFeeType,
             PAFeeAmount,
             PAPercent_Cost,
             PAPercent_Revenue,
             PA_Retention_Percent,
             PAFeeToUse,
             PAbilltaxscheduleid,
             PASales_Tax_Options,
             PAbillnoteidx,
             PAbegindate,
             PAEnDate,
             PAFrequency,
             STRTDATE,
             ENDDATE,
             PARenew,
             PARenewal_Date,
             PATotAmt,
             TM_AR_Fee_SRC,
             TM_Prj_Deferred_Rev_Fee_,
             TM_Prj_Rev_Fee_SRC,
             PAFF_AR_Fee_SRC,
             FF_Prj_Billings_Fee_SRC,
             FF_Prj_Rev_Fee_SRC,
             FF_Retention_Fee_SRC,
             FF_WIP_Fee_SRC,
             FF_Prj_Deferred_Rev_Fee_,
             FF_BIEE_Fee_SRC,
             FF_EIEB_Fee_SRC,
             PAtotcbts,
             PAtotcbEL,
             PAtotcbML,
             PAtotcbvi,
             PAtotcber,
             PAtotcbinv,
             PA_Renew_Day,
             PA_Renew_Month,
             PAUD1,
             PAUD2,
             PACHGORDNO,
             PAService_Fee_Amount,
             PA_MC_Fee_Amount,
             PA_MC_Service_Fee_Amount,
             PA_MC_Total_Amount)
      select @I_vPAPROJNUMBER,
             @I_vPALineItemSeq,
             @I_vPAFeeID,
             @PAFeeName,
             @PAFeeType,
             @I_vPAFeeAmount,
             @I_vPAPercent_Cost,
             @I_vPAPercent_Revenue,
             @I_vPA_Retention_Percent,
             @PAFeeToUse,
             @I_vPAbilltaxscheduleid,
             @PASales_Tax_Options,
             @PAbillnoteidx,
             @PAbegindate,
             @PAEnDate,
             @PAFrequency,
             @STRTDATE,
             @ENDDATE,
             @PARenew,
             @PARenewal_Date,
             @PATotAmt,
             @TM_AR_Fee_SRC,
             @TM_Prj_Deferred_Rev_Fee_,
             @TM_Prj_Rev_Fee_SRC,
             @PAFF_AR_Fee_SRC,
             @FF_Prj_Billings_Fee_SRC,
             @FF_Prj_Rev_Fee_SRC,
             @FF_Retention_Fee_SRC,
             @FF_WIP_Fee_SRC,
             @FF_Prj_Deferred_Rev_Fee_,
             @FF_BIEE_Fee_SRC,
             @FF_EIEB_Fee_SRC,
             @PAtotcbts,
             @PAtotcbEL,
             @PAtotcbML,
             @PAtotcbvi,
             @PAtotcber,
             @PAtotcbinv,
             @PA_Renew_Day,
             @PA_Renew_Month,
             @PAUD1,
             @PAUD2,
             @PACHGORDNO,
             @I_vPAService_Fee_Amount,
             @PA_MC_Fee_Amount,
             @PA_MC_Service_Fee_Amount,
             @PA_MC_Total_Amount
      
      if @@error <> 0
        begin
          select @O_iErrorState = 4915
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      update PA02101
      set    PAFeeAmount = @I_vPAFeeAmount,
             PAPercent_Cost = @I_vPAPercent_Cost,
             PAPercent_Revenue = @I_vPAPercent_Revenue,
             PA_Retention_Percent = @I_vPA_Retention_Percent,
             PAbilltaxscheduleid = @I_vPAbilltaxscheduleid,
             PAService_Fee_Amount = @I_vPAService_Fee_Amount
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PALineItemSeq = @I_vPALineItemSeq
             and PAFeeID = @I_vPAFeeID
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6428
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  execute @iStatus = PA_Create_Fee_Periodic_Records
    @IN_contractnumber = @PACONTNUMBER ,
    @IN_projectnumber = @I_vPAPROJNUMBER ,
    @IN_feeID = @I_vPAFeeID ,
    @IN_customernumber = @CUSTNMBR ,
    @IN_freq = 1 ,
    @IN_begindate = @PAbegindate ,
    @IN_enddate = @PAEnDate ,
    @IN_periodicstartdate = @PAbegindate ,
    @IN_periodicenddate = @PAEnDate ,
    @IN_userdate = @userdate ,
    @IN_currencydecimals = @DECPLCUR ,
    @IO_err = @PA_Create_Fee_Periodic_RecordsErrState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@PA_Create_Fee_Periodic_RecordsErrState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@PA_Create_Fee_Periodic_RecordsErrState))
      
      select @O_iErrorState = 6429
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  exec @iStatus = taPAProjectFeePost
    @I_vPAPROJNUMBER ,
    @I_vPAFeeID ,
    @I_vPALineItemSeq ,
    @I_vPAFeeAmount ,
    @I_vPAPercent_Cost ,
    @I_vPAPercent_Revenue ,
    @I_vPA_Retention_Percent ,
    @I_vPAService_Fee_Amount ,
    @I_vPAbilltaxscheduleid ,
    @I_vUpdateExisting ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @O_iErrorState = 4916
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)
  
  grant
    execute
    on dbo.taPAProjectFee
  to DYNGRP

