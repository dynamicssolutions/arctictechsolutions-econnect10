

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetDepositsForInvoice]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetDepositsForInvoice]
  
create procedure [dbo].[RmsGetDepositsForInvoice]
                @iRMDepositApplyTempTable VARCHAR(255),
                @iTransactionNumber       INT,
                @iStoreID                 INT,
                @iRmsServerName           VARCHAR(255),
                @iHqDbName                VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lOrderHistoryTable VARCHAR(255)
  
  DECLARE  @lOrderTable VARCHAR(255)
  
  DECLARE  @lApplyFromTable VARCHAR(255)
  
  DECLARE  @lTransactionTable VARCHAR(255)
  
  SELECT @lOrderHistoryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.OrderHistory as OrderHistory'
  
  SELECT @lOrderTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.[Order] as Orders'
  
  SELECT @lApplyFromTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.OrderHistory as ApplyFromTable'
  
  SELECT @lTransactionTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.[Transaction] as Transactions'
  
  EXEC( 'INSERT INTO ' + @iRMDepositApplyTempTable + ' (RmsStoreID,  RmsOrderHistoryID,  Total_Sales,  RmsApplyToID,  RmsApplyFromID,  RmsApplyDate)  SELECT  OrderHistory.StoreID,  OrderHistory.[ID],  case   when   abs(ApplyFromTable.DeltaDeposit) <= abs(OrderHistory.DeltaDeposit)  then  ApplyFromTable.DeltaDeposit  when  abs(ApplyFromTable.DeltaDeposit) > abs(OrderHistory.DeltaDeposit)   then  OrderHistory.DeltaDeposit  end,  OrderHistory.TransactionNumber,  ApplyFromTable.[ID],  CONVERT(DATETIME,CONVERT(VARCHAR,OrderHistory.[Date],111))   FROM ' + @lOrderHistoryTable + ',' + @lApplyFromTable + ' WHERE  OrderHistory.StoreID = ApplyFromTable.StoreID  AND  OrderHistory.StoreID = ' + @iStoreID + ' AND  OrderHistory.OrderID = ApplyFromTable.OrderID  AND  OrderHistory.[ID] <> ApplyFromTable.[ID]  AND  OrderHistory.DeltaDeposit < 0  AND  ApplyFromTable.DeltaDeposit > 0  AND  OrderHistory.TransactionNumber = ' + @iTransactionNumber)
  
  EXEC( 'INSERT INTO ' + @iRMDepositApplyTempTable + ' (RmsStoreID,  RmsOrderHistoryID,  Total_Sales,  RmsApplyToID,  RmsApplyFromID,  RmsApplyDate)  SELECT  OrderHistory.StoreID,  OrderHistory.[ID],  case   when   abs(OrderHistory.DeltaDeposit) <= abs(Transactions.Total)  then  OrderHistory.DeltaDeposit  when  abs(OrderHistory.DeltaDeposit) > abs(Transactions.Total)   then  Transactions.Total  end,  OrderHistory.TransactionNumber,  OrderHistory.[ID],  CONVERT(DATETIME,CONVERT(VARCHAR,OrderHistory.[Date],111))   FROM ' + @lOrderHistoryTable + ',' + @lTransactionTable + ' WHERE  OrderHistory.TransactionNumber = Transactions.TransactionNumber  AND  OrderHistory.StoreID = Transactions.StoreID  AND  OrderHistory.StoreID = ' + @iStoreID + ' AND  OrderHistory.DeltaDeposit > 0  AND  OrderHistory.TransactionNumber = ' + @iTransactionNumber)

