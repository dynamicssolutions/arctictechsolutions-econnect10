

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsRMTransactionTaxInsertWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsRMTransactionTaxInsertWrapper]
  
create procedure [dbo].[RmsRMTransactionTaxInsertWrapper]
                @iCustNum      char(15),
                @iDocNum       char(20),
                @iRMDocType    smallint,
                @iBatchNum     char(15),
                @iTaxDtlID     char(15),
                @iTaxAmnt      numeric(19,5),
                @iSTaxAmnt     numeric(19,5),
                @iTaxDtlTSls   numeric(19,5),
                @iTaxDtlTTxSls numeric(19,5)  = null,
                @oErrorState   int  output,
                @oErrString    varchar(255)  output  /*with encryption*/
AS
  EXEC RmsRMTransactionTaxInsert
    @I_vCUSTNMBR = @iCustNum ,
    @I_vDOCNUMBR = @iDocNum ,
    @I_vRMDTYPAL = @iRMDocType ,
    @I_vBACHNUMB = @iBatchNum ,
    @I_vTAXDTLID = @iTaxDtlID ,
    @I_vTAXAMNT = @iTaxAmnt ,
    @I_vSTAXAMNT = @iSTaxAmnt ,
    @I_vTAXDTSLS = @iTaxDtlTSls ,
    @I_vTDTTXSLS = @iTaxDtlTTxSls ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

