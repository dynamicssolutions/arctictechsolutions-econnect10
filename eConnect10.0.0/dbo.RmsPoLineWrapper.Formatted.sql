

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsPoLineWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsPoLineWrapper]
  
create procedure [dbo].[RmsPoLineWrapper]
                @iPOType     smallint,
                @iPONumber   char(17),
                @iOrd        int,
                @iVendID     char(15),
                @iLocCode    char(10),
                @iVenItmNum  char(30),
                @iItemNum    char(30),
                @iQuantity   numeric(19,5),
                @iUnitCost   numeric(19,5),
                @iTaxAmount  numeric(19,5),
                @iUpdate     smallint,
                @iCurrencyID char(15),
                @oErrorState int  output,
                @oErrString  varchar(255)  output   /*with encryption*/
AS
  EXEC RmsPoLine
    @I_vPOTYPE = @iPOType ,
    @I_vPONUMBER = @iPONumber ,
    @I_vORD = @iOrd ,
    @I_vVENDORID = @iVendID ,
    @I_vLOCNCODE = @iLocCode ,
    @I_vVNDITNUM = @iVenItmNum ,
    @I_vITEMNMBR = @iItemNum ,
    @I_vQUANTITY = @iQuantity ,
    @I_vUNITCOST = @iUnitCost ,
    @I_vTAXAMNT = @iTaxAmount ,
    @I_vUpdateIfExists = @iUpdate ,
    @I_vCURNCYID = @iCurrencyID ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

