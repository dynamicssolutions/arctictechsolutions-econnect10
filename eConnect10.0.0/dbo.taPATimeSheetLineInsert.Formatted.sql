

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPATimeSheetLineInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPATimeSheetLineInsert]
  
create procedure [dbo].[taPATimeSheetLineInsert]
                @I_vPATSTYP               smallint,
                @I_vPATSNO                char(17),
                @I_vEMPLOYID              char(15),
                @I_vCURNCYID              char(15),
                @I_vPAREFNO               char(17)  = '',
                @I_vLNITMSEQ              integer  = 0,
                @I_vPADT                  datetime,
                @I_vPAPROJNUMBER          char(15),
                @I_vPACOSTCATID           char(15),
                @I_vPATB                  datetime  = '',
                @I_vPATE                  datetime  = '',
                @I_vPAQtyQ                numeric(19,5)  = 0,
                @I_vPAbllngtype           smallint  = -1,
                @I_vPAUnit_of_Measure     char(9)  = '',
                @I_vPAUNITCOST            numeric(19,5)  = 0,
                @I_vPAPayCode             char(7)  = '',
                @I_vPADepartment          char(7)  = '',
                @I_vPAJob_Title           char(7)  = '',
                @I_vPATOTCST              numeric(19,5)  = 0,
                @I_vPASalary_Posting_Type smallint  = 0,
                @I_vRequesterTrx          smallint  = 0,
                @I_vPAtx500               varchar(8000)  = '',
                @I_vUSRDEFND1             char(50)  = '',
                @I_vUSRDEFND2             char(50)  = '',
                @I_vUSRDEFND3             char(50)  = '',
                @I_vUSRDEFND4             varchar(8000)  = '',
                @I_vUSRDEFND5             varchar(8000)  = '',
                @O_iErrorState            int  output,
                @oErrString               varchar(255)  output   /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @l_PAJob_Title                          char(7),
           @l_PADepartment                         char(7),
           @iStatus                                int,
           @iCustomState                           int,
           @iCustomErrString                       varchar(255),
           @iError                                 int,
           @iCursorError                           int,
           @O_oErrorState                          int,
           @sCompanyID                             smallint,
           @PAreportingperiods                     smallint,
           @PAnumofreportingperiods                smallint,
           @PA1stdatereportperiod                  datetime,
           @PAallow_1                              int,
           @PAallow_2                              int,
           @PAallow_3                              int,
           @PAallow_4                              int,
           @PAallow_5                              int,
           @PAallow_6                              int,
           @PAallow_7                              int,
           @PAallow_8                              int,
           @PAallow_9                              int,
           @PAallow_10                             int,
           @PABase_Qty                             numeric(19,5),
           @UOMSCHDL                               char(11),
           @PAROUNDAMT                             numeric(19,5),
           @ORUNTCST                               numeric(19,5),
           @PABase_Unit_Cost                       numeric(19,5),
           @PAORGBSUNITCST                         numeric(19,5),
           @PAEXTCOST                              numeric(19,5),
           @OREXTCST                               numeric(19,5),
           @PAORIGTOTCOST                          numeric(19,5),
           @PAOverhead_Amount                      numeric(19,5),
           @PABaseOvhdCost                         numeric(19,5),
           @PAORIGOVHDAMT                          numeric(19,5),
           @PAORIGBASEOVRHCST                      numeric(19,5),
           @PAOverheaPercentage                    numeric(19,5),
           @PATOTALOVERH                           numeric(19,5),
           @PAORIGTOTOVRHD                         numeric(19,5),
           @PABILRATE                              numeric(19,5),
           @PAORIGBILLRATE                         numeric(19,5),
           @PA_Base_Billing_Rate                   numeric(19,5),
           @PAORIGBSBILLRTE                        numeric(19,5),
           @PAMARKPERCENT                          numeric(19,5),
           @PAACREV                                numeric(19,5),
           @PAORIACCRREV                           numeric(19,5),
           @PASTD_Qty                              numeric(19,5),
           @PANC_Qty                               numeric(19,5),
           @PANB_Qty                               numeric(19,5),
           @PAReference_Line_Seq_N                 int,
           @PAbillnoteidx                          numeric(19,5),
           @PAbllngtype                            smallint,
           @PACONTNUMBER                           char(11),
           @PASalary_Posting_Type                  smallint,
           @PAProfitType                           smallint,
           @PAProfitPercent                        numeric(19,5),
           @PAProfitAmount                         numeric(19,5),
           @PAORIGPROFAMT                          numeric(19,5),
           @PATotalProfit                          numeric(19,5),
           @PAORIGTOTPROF                          numeric(19,5),
           @STATECD                                char(3),
           @LOCLCODE                               char(7),
           @SUTASTAT                               char(3),
           @WRKRCOMP                               char(7),
           @PAexptdate                             datetime,
           @PAApproved_Quantity                    numeric(19,5),
           @PAAPPROVBILLRATE                       numeric(19,5),
           @PAAPPROVMKUPPCT                        numeric(19,5),
           @PAApproved_Billing_Amou                numeric(19,5),
           @PABilledProfitPercentage               numeric(19,5),
           @PA_Billed_Profit_Amount_               numeric(19,5),
           @PABilled_Amount                        numeric(19,5),
           @PABilledTax                            numeric(19,5),
           @PABilled_Misc                          numeric(19,5),
           @PABilled_Freight                       numeric(19,5),
           @PABilled_Discount                      numeric(19,5),
           @PAWrite_UpDown_Amount                  numeric(19,5),
           @PAWrite_UpDown_Percenta                int,
           @PAApprover_ID                          char(15),
           @PABilled_QtyN                          numeric(19,5),
           @PAApprover_Type                        smallint,
           @PAPartial_Bill                         tinyint,
           @PABilling_StatusN                      smallint,
           @PACGBWIPIDX                            int,
           @PAUnbilled_AR_Idx                      int,
           @PACogs_Idx                             int,
           @PAContra_Account_IDX                   int,
           @PAOverhead_IDX                         int,
           @PAUnbilled_Proj_Rev_Idx                int,
           @RNDDIFF                                int,
           @PACHGORDNO                             char(17),
           @PASTAT                                 int,
           @PAcloseProjcosts                       int,
           @CUSTNMBR                               char(17),
           @DEX_ROW_ID                             int,
           @PAProjectType                          int,
           @PAAcctgMethod                          int,
           @add_employee_access                    int,
           @PATU                                   int,
           @PAinactive                             int,
           @l_PAPROJNUMBER                         char(15),
           @DECPLQTY                               int,
           @DECPLCUR                               int,
           @EQUIVUOM                               char(9),
           @BASEUOFM                               char(9),
           @PAUsePayCodes                          int,
           @PAtsunitcostfrom                       int,
           @PAEmployee_Type                        int,
           @PAYRTDEC                               numeric(19,5),
           @PAPay_Code_Hourly                      char(7),
           @PAPay_Code_Salary                      char(7),
           @WKHRPRYR                               numeric(19,5),
           @PAYUNPER                               numeric(19,5),
           @PAYUNIT                                char(25),
           @PAYPRPRD                               numeric(19,5),
           @PAYPEROD                               numeric(19,5),
           @PAYRCORD                               char(7),
           @PAYTYPE                                int,
           @PAYRTAMT                               numeric(19,5),
           @l_pay_period                           numeric(19,5),
           @l_pay_unit_period                      numeric(19,5),
           @l_hours_worked                         numeric(19,5),
           @PALabor_Rate_Table_ID                  char(31),
           @PALabor_RateTable_Type                 int,
           @l_PALabor_Rate_Table_ID                char(31),
           @l_PAUnit_of_Measure                    char(9),
           @l_EMPLOYID                             char(15),
           @INACTIVE                               int,
           @PATMProfitAmount                       numeric(19,5),
           @PAFProfitAmt                           numeric(19,5),
           @PAFProfitPcnt                          numeric(19,5),
           @PATMProfitPercent                      numeric(19,5),
           @PAProfit_Type__CP                      int,
           @PAFFProfitType                         int,
           @EQUOMQTY                               numeric(19,5),
           @LOCALTAX                               char(7),
           @l_UOMSCHDL                             char(11),
           @l_qty                                  numeric(19,5),
           @l_base_qty                             numeric(19,5),
           @l_overhead                             numeric(19,5),
           @l_extended                             numeric(19,5),
           @l_accrued                              numeric(19,5),
           @l_cost                                 numeric(19,5),
           @l_ocost                                numeric(19,5),
           @l_scroll_UofM_Qty                      numeric(19,5),
           @l_oqty                                 numeric(19,5),
           @O_overhead                             numeric(19,5),
           @O_extended                             numeric(19,5),
           @O_accrued                              numeric(19,5),
           @O_cost                                 numeric(19,5),
           @PAPostedQty                            numeric(19,5),
           @PAFQuantity                            numeric(19,5),
           @l_line_UofM_Qty                        numeric(19,5),
           @PAPostedTotalCostN                     numeric(19,5),
           @PAFTotalCost                           numeric(19,5),
           @l_oaccrued                             numeric(19,5),
           @PAFBillings                            numeric(19,5),
           @PAUnpostedQty                          numeric(19,5),
           @PAUnpostedTotalCostN                   numeric(19,5),
           @PAPosted_Accr_RevN                     numeric(19,5),
           @PABilled_Accrued_Revenu                numeric(19,5),
           @PAUnpostAccrRevN                       numeric(19,5),
           @FUNLCURR                               char(15),
           @FUNCRIDX                               int,
           @CURRNIDX                               int,
           @ODECPLCU                               int,
           @UnitCurrPlace                          int,
           @l_PAProfitAmount                       numeric(19,5),
           @l_PAProfitPercent                      numeric(19,5),
           @iGetNextNoteIdxErrState                int,
           @PAUNITCOST                             numeric(19,5),
           @uneditable                             smallint,
           @BSPAYRCD                               char(7),
           @l_PAYTYPE                              smallint,
           @PAPayCode                              char(7),
           @PAtsprofittypefrom                     smallint,
           @PA_Line_Update_Totals_TablesErrorState int,
           @taPAProjectBudgetMasterErrorState      int,
           @otaPAProjectBudgetMasterErrString      varchar(255),
           @temp_PAProfitType                      smallint,
           @temp_PAbllngtype                       smallint,
           @temp_LNITMSEQ                          int,
           @temp_PALineItemSeq                     int,
           @temp_PAFQuantity                       numeric(19,5),
           @temp_PAFUnitCost                       numeric(19,5),
           @temp_PAFTotalCost                      numeric(19,5),
           @temp_PAFProfitAmt                      numeric(19,5),
           @temp_PAFProfitPcnt                     numeric(19,5),
           @temp_PAForecastOvrhdAmtPerUnt          numeric(19,5),
           @temp_PAForecastOvrhdPct                numeric(19,5),
           @temp_PAForecastPTaxOptions             smallint,
           @temp_PAForecastCTaxSchedID             char(15),
           @temp_PAForecastSTaxOptions             smallint,
           @temp_PAForecastBTaxSchedID             char(15),
           @temp_PAFBeginDate                      datetime,
           @temp_PAFEndDate                        datetime,
           @PAUnit_of_Measure                      char(9),
           @PAMCFProfitAmt                         numeric(19,5),
           @PAForecastBaseProfitAmt                numeric(19,5),
           @PAMCCURNCYID                           char(15),
           @RATETPID                               char(15),
           @EXGTBLID                               char(15),
           @XCHGRATE                               numeric(19,5),
           @EXCHDATE                               datetime,
           @TIME1                                  datetime,
           @RATECALC                               int,
           @DENXRATE                               numeric(19,5),
           @MCTRXSTT                               int,
           @PA_MC_Accrued_Revenue                  numeric(19,5),
           @PA_MC_Base_Billing_Rate                numeric(19,5),
           @PA_MC_Billing_Rate                     numeric(19,5),
           @IsMC                                   bit,
           @PAMCCURRNIDX                           int,
           @CCROWID                                int,
           @EMPTYDATE                              datetime,
           @OCDECPLCUR                             int,
           @FCDECPLCUR                             int
  
  select @l_PAJob_Title = '',
         @l_PADepartment = '',
         @iStatus = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @iError = 0,
         @iCursorError = 0,
         @O_iErrorState = 0,
         @O_oErrorState = 0,
         @sCompanyID = 0,
         @PAreportingperiods = 0,
         @PAnumofreportingperiods = 0,
         @PA1stdatereportperiod = '',
         @PAallow_1 = 0,
         @PAallow_2 = 0,
         @PAallow_3 = 0,
         @PAallow_4 = 0,
         @PAallow_5 = 0,
         @PAallow_6 = 0,
         @PAallow_7 = 0,
         @PAallow_8 = 0,
         @PAallow_9 = 0,
         @PAallow_10 = 0,
         @PABase_Qty = 0,
         @UOMSCHDL = '',
         @PAROUNDAMT = 0,
         @ORUNTCST = 0,
         @PABase_Unit_Cost = 0,
         @PAORGBSUNITCST = 0,
         @PAEXTCOST = 0,
         @OREXTCST = 0,
         @PAORIGTOTCOST = 0,
         @PAOverhead_Amount = 0,
         @PABaseOvhdCost = 0,
         @PAORIGOVHDAMT = 0,
         @PAORIGBASEOVRHCST = 0,
         @PAOverheaPercentage = 0,
         @PATOTALOVERH = 0,
         @PAORIGTOTOVRHD = 0,
         @PABILRATE = 0,
         @PAORIGBILLRATE = 0,
         @PA_Base_Billing_Rate = 0,
         @PAORIGBSBILLRTE = 0,
         @PAMARKPERCENT = 0,
         @PAACREV = 0,
         @PAORIACCRREV = 0,
         @PASTD_Qty = 0,
         @PANC_Qty = 0,
         @PANB_Qty = 0,
         @PAReference_Line_Seq_N = 0,
         @PAbillnoteidx = 0,
         @PAbllngtype = 0,
         @PACONTNUMBER = '',
         @PASalary_Posting_Type = 0,
         @PAProfitType = 0,
         @PAProfitPercent = 0,
         @PAProfitAmount = 0,
         @PAORIGPROFAMT = 0,
         @PATotalProfit = 0,
         @PAORIGTOTPROF = 0,
         @STATECD = '',
         @LOCLCODE = '',
         @SUTASTAT = '',
         @WRKRCOMP = '',
         @PAApproved_Quantity = 0,
         @PAAPPROVBILLRATE = 0,
         @PAAPPROVMKUPPCT = 0,
         @PAApproved_Billing_Amou = 0,
         @PABilledProfitPercentage = 0,
         @PA_Billed_Profit_Amount_ = 0,
         @PABilled_Amount = 0,
         @PABilledTax = 0,
         @PABilled_Misc = 0,
         @PABilled_Freight = 0,
         @PABilled_Discount = 0,
         @PAWrite_UpDown_Amount = 0,
         @PAWrite_UpDown_Percenta = 0,
         @PAApprover_ID = '',
         @PABilled_QtyN = 0,
         @PAApprover_Type = 0,
         @PAPartial_Bill = 0,
         @PABilling_StatusN = 0,
         @PACGBWIPIDX = 0,
         @PAUnbilled_AR_Idx = 0,
         @PACogs_Idx = 0,
         @PAContra_Account_IDX = 0,
         @PAOverhead_IDX = 0,
         @PAUnbilled_Proj_Rev_Idx = 0,
         @RNDDIFF = 0,
         @PACHGORDNO = '',
         @PASTAT = 0,
         @PAcloseProjcosts = 0,
         @CUSTNMBR = '',
         @DEX_ROW_ID = 0,
         @add_employee_access = 0,
         @PAProjectType = 0,
         @PAAcctgMethod = 0,
         @PATU = 0,
         @PAinactive = 0,
         @l_PAPROJNUMBER = '',
         @DECPLQTY = 0,
         @DECPLCUR = 0,
         @EQUIVUOM = '',
         @BASEUOFM = '',
         @PAUsePayCodes = 0,
         @PAtsunitcostfrom = 0,
         @PAEmployee_Type = 0,
         @PAYRTDEC = 0,
         @PAPay_Code_Hourly = '',
         @PAPay_Code_Salary = '',
         @WKHRPRYR = 0,
         @PAYUNPER = 0,
         @PAYUNIT = '',
         @PAYPRPRD = 0,
         @PAYPEROD = 0,
         @PAYRCORD = '',
         @PAYTYPE = 0,
         @PAYRTAMT = 0,
         @l_pay_period = 0,
         @l_pay_unit_period = 0,
         @l_hours_worked = 0,
         @PALabor_Rate_Table_ID = '',
         @PALabor_RateTable_Type = 0,
         @l_PALabor_Rate_Table_ID = '',
         @l_PAUnit_of_Measure = '',
         @l_EMPLOYID = '',
         @INACTIVE = 0,
         @PATMProfitAmount = 0,
         @PAFProfitAmt = 0,
         @PAFProfitPcnt = 0,
         @PATMProfitPercent = 0,
         @PAProfit_Type__CP = 0,
         @PAFFProfitType = 0,
         @EQUOMQTY = 0,
         @LOCALTAX = '',
         @l_UOMSCHDL = '',
         @l_qty = 0,
         @l_base_qty = 0,
         @O_overhead = 0,
         @O_extended = 0,
         @O_cost = 0,
         @l_UOMSCHDL = '',
         @l_overhead = 0,
         @l_extended = 0,
         @l_accrued = 0,
         @l_cost = 0,
         @l_ocost = 0,
         @l_scroll_UofM_Qty = 0,
         @l_oqty = 0,
         @O_accrued = 0,
         @PAPostedQty = 0,
         @PAFQuantity = 0,
         @l_line_UofM_Qty = 0,
         @PAPostedTotalCostN = 0,
         @PAFTotalCost = 0,
         @l_oaccrued = 0,
         @PAFBillings = 0,
         @PAUnpostedQty = 0,
         @PAUnpostedTotalCostN = 0,
         @PAPosted_Accr_RevN = 0,
         @PABilled_Accrued_Revenu = 0,
         @PAUnpostAccrRevN = 0,
         @FUNLCURR = '',
         @CURRNIDX = 0,
         @ODECPLCU = 0,
         @UnitCurrPlace = 0,
         @PAexptdate = '',
         @l_PAProfitAmount = 0,
         @l_PAProfitPercent = 0,
         @FUNCRIDX = 0,
         @iGetNextNoteIdxErrState = 0,
         @PAUNITCOST = 0,
         @uneditable = 0,
         @BSPAYRCD = '',
         @l_PAYTYPE = 0,
         @PAPayCode = '',
         @PAtsprofittypefrom = 0,
         @PA_Line_Update_Totals_TablesErrorState = 0,
         @taPAProjectBudgetMasterErrorState = 0,
         @otaPAProjectBudgetMasterErrString = '',
         @temp_PAProfitType = 0,
         @temp_PAbllngtype = 0,
         @temp_LNITMSEQ = 0,
         @temp_PALineItemSeq = 0,
         @temp_PAFQuantity = 0,
         @temp_PAFUnitCost = 0,
         @temp_PAFTotalCost = 0,
         @temp_PAFProfitAmt = 0,
         @temp_PAFProfitPcnt = 0,
         @temp_PAForecastOvrhdAmtPerUnt = 0,
         @temp_PAForecastOvrhdPct = 0,
         @temp_PAForecastPTaxOptions = 0,
         @temp_PAForecastCTaxSchedID = '',
         @temp_PAForecastSTaxOptions = 0,
         @temp_PAForecastBTaxSchedID = '',
         @temp_PAFBeginDate = '',
         @temp_PAFEndDate = '',
         @EMPTYDATE = convert(datetime,'1/1/1900'),
         @PAUnit_of_Measure = '',
         @PAMCFProfitAmt = 0,
         @PAForecastBaseProfitAmt = 0,
         @PAMCCURNCYID = '',
         @RATETPID = '',
         @EXGTBLID = '',
         @XCHGRATE = 0,
         @EXCHDATE = @EMPTYDATE,
         @TIME1 = @EMPTYDATE,
         @RATECALC = 0,
         @DENXRATE = 0,
         @MCTRXSTT = 0,
         @PA_MC_Accrued_Revenue = 0,
         @PA_MC_Base_Billing_Rate = 0,
         @PA_MC_Billing_Rate = 0,
         @IsMC = 0,
         @PAMCCURRNIDX = 0,
         @OCDECPLCUR = 0,
         @FCDECPLCUR = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taPATimeSheetLineInsertPre
    @I_vPATSTYP output ,
    @I_vPATSNO output ,
    @I_vEMPLOYID output ,
    @I_vCURNCYID output ,
    @I_vPAREFNO output ,
    @I_vLNITMSEQ output ,
    @I_vPADT output ,
    @I_vPAPROJNUMBER output ,
    @I_vPACOSTCATID output ,
    @I_vPATB output ,
    @I_vPATE output ,
    @I_vPAQtyQ output ,
    @I_vPAbllngtype output ,
    @I_vPAUnit_of_Measure output ,
    @I_vPAUNITCOST output ,
    @I_vPAPayCode output ,
    @I_vPADepartment output ,
    @I_vPAJob_Title output ,
    @I_vPATOTCST output ,
    @I_vPASalary_Posting_Type output ,
    @I_vRequesterTrx output ,
    @I_vPAtx500 output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1205
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPATSTYP is NULL 
       or @I_vPATSNO is NULL
       or @I_vEMPLOYID is NULL
       or @I_vCURNCYID is NULL
       or @I_vPADT is NULL
       or @I_vPAPROJNUMBER is NULL
       or @I_vPACOSTCATID is NULL)
    begin
      select @O_iErrorState = 1206
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPATSTYP = 0
       or @I_vPATSNO = ''
       or @I_vEMPLOYID = ''
       or @I_vCURNCYID = ''
       or @I_vPADT = ''
       or @I_vPAPROJNUMBER = ''
       or @I_vPACOSTCATID = '')
    begin
      select @O_iErrorState = 1207
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAUNITCOST < 0)
    begin
      select @O_iErrorState = 1208
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vEMPLOYID = UPPER(@I_vEMPLOYID),
         @I_vCURNCYID = UPPER(@I_vCURNCYID),
         @I_vPAPROJNUMBER = UPPER(@I_vPAPROJNUMBER),
         @I_vPACOSTCATID = UPPER(@I_vPACOSTCATID),
         @I_vPAPayCode = UPPER(@I_vPAPayCode),
         @I_vPADepartment = UPPER(@I_vPADepartment),
         @I_vPAJob_Title = UPPER(@I_vPAJob_Title)
  
  select @DEX_ROW_ID = DEX_ROW_ID,
         @PAreportingperiods = isnull(PAreportingperiods,0),
         @PAnumofreportingperiods = isnull(PAnumofreportingperiods,0),
         @PA1stdatereportperiod = isnull(PA1stdatereportperiod,0),
         @PAtsunitcostfrom = PAtsunitcostfrom,
         @PAtsprofittypefrom = isnull(PAtsprofittypefrom,0),
         @PAallow_1 = isnull(PAallow_1,0),
         @PAallow_2 = isnull(PAallow_2,0),
         @PAallow_3 = isnull(PAallow_3,0),
         @PAallow_4 = isnull(PAallow_4,0),
         @PAallow_5 = isnull(PAallow_5,0),
         @PAallow_6 = isnull(PAallow_6,0),
         @PAallow_7 = isnull(PAallow_7,0),
         @PAallow_8 = isnull(PAallow_8,0),
         @PAallow_9 = isnull(PAallow_9,0),
         @PAallow_10 = isnull(PAallow_10,0)
  from   PA41801 (nolock)
  
  if (@DEX_ROW_ID IS NULL)
    begin
      select @O_iErrorState = 1210
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAPROJNUMBER <> '')
      and (@I_vPAPROJNUMBER <> '<NONE>'))
    begin
      select @CCROWID = DEX_ROW_ID
      from   PA01301
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PACOSTCATID = @I_vPACOSTCATID
      
      if (@CCROWID is NULL)
        begin
          select @O_iErrorState = 9392
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  set @DEX_ROW_ID = 0
  
  select @DEX_ROW_ID = DEX_ROW_ID,
         @PATU = isnull(PATU,0)
  from   PA01001 (nolock)
  where  PACOSTCATID = @I_vPACOSTCATID
  
  if (@DEX_ROW_ID IS NULL)
      or (@DEX_ROW_ID = 0)
    begin
      select @O_iErrorState = 8143
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAPROJNUMBER <> '')
      and (@I_vPAPROJNUMBER <> '<NONE>'))
    begin
      select @DECPLCUR = DECPLCUR,
             @DECPLQTY = (DECPLQTY - 1)
      from   PA01301 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PACOSTCATID = @I_vPACOSTCATID
             and PATU = @PATU
      
      if (@DECPLCUR >= 7)
        begin
          select @UnitCurrPlace = @DECPLCUR - 7
        end
      else
        begin
          select @UnitCurrPlace = @DECPLCUR
        end
    end
  else
    begin
      select @DECPLCUR = DECPLCUR,
             @DECPLQTY = (DECPLQTY - 1)
      from   PA01001 (nolock)
      where  PACOSTCATID = @I_vPACOSTCATID
      
      if (@DECPLCUR >= 7)
        begin
          select @UnitCurrPlace = @DECPLCUR - 7
        end
      else
        begin
          select @UnitCurrPlace = @DECPLCUR
        end
    end
  
  if ((@I_vPAQtyQ <> round(@I_vPAQtyQ,@DECPLQTY))
       or (@I_vPAUNITCOST <> round(@I_vPAUNITCOST,@DECPLCUR))
       or (@I_vPATOTCST <> round(@I_vPATOTCST,@DECPLCUR)))
    begin
      select @O_iErrorState = 7744
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPATSTYP < 1)
       or (@I_vPATSTYP > 2))
    begin
      select @O_iErrorState = 1211
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPATSTYP = 1)
    select @I_vPAREFNO = ''
  
  if @I_vEMPLOYID <> ''
    begin
      select @l_EMPLOYID = isnull(EMPLOYID,''),
             @l_PAJob_Title = isnull(JOBTITLE,''),
             @l_PADepartment = isnull(DEPRTMNT,'')
      from   UPR00100 (nolock)
      where  EMPLOYID = @I_vEMPLOYID
      
      if (@l_EMPLOYID = '')
        begin
          select @O_iErrorState = 1212
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      select @l_EMPLOYID = ''
      
      select @l_EMPLOYID = isnull(EMPLOYID,''),
             @PAEmployee_Type = isnull(PAEmployee_Type,0)
      from   PA00601 (nolock)
      where  EMPLOYID = @I_vEMPLOYID
      
      if (@l_EMPLOYID = '')
        begin
          select @O_iErrorState = 7578
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPADepartment = '')
     and (@l_PADepartment <> '')
    select @I_vPADepartment = @l_PADepartment
  
  if (@I_vPAJob_Title = '')
     and (@l_PAJob_Title <> '')
    select @I_vPAJob_Title = @l_PAJob_Title
  
  if not exists (select 1
                 from   PA01408 (nolock)
                 where  PAPROJNUMBER = @I_vPAPROJNUMBER
                        and EMPLOYID = @I_vEMPLOYID)
    begin
      if (@PAallow_10 = 0)
        begin
          select @O_iErrorState = 1254
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          select @add_employee_access = 1
        end
    end
  
  if (@I_vPAPayCode <> '')
    begin
      if not exists (select 1
                     from   UPR00400 (nolock)
                     where  EMPLOYID = @I_vEMPLOYID
                            and PAYRCORD = @I_vPAPayCode
                            and INACTIVE = 0)
        begin
          select @O_iErrorState = 1265
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if exists (select 1
                 from   UPR00400 (nolock)
                 where  EMPLOYID = @I_vEMPLOYID
                        and PAYRCORD = @I_vPAPayCode
                        and INACTIVE = 1)
        begin
          select @O_iErrorState = 1328
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      select @PAYTYPE = isnull(PAYTYPE,0)
      from   UPR00400 (nolock)
      where  EMPLOYID = @I_vEMPLOYID
             and PAYRCORD = @I_vPAPayCode
      
      if (@PAYTYPE = 2
          and @PAEmployee_Type = 0)
        begin
          select @O_iErrorState = 1329
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@PAYTYPE = 11
           or @PAYTYPE = 13
           or @PAYTYPE = 14
           or @PAYTYPE = 15)
        begin
          select @O_iErrorState = 1330
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vLNITMSEQ = 0)
    begin
      select @I_vLNITMSEQ = isnull(max(LNITMSEQ),0) + 100
      from   PA10001 a (nolock)
      where  a.PATSNO = @I_vPATSNO
    end
  
  if exists (select 1
             from   PA10001 (nolock)
             where  PATSNO = @I_vPATSNO
                    and LNITMSEQ = @I_vLNITMSEQ)
    begin
      select @O_iErrorState = 1213
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @FUNLCURR = isnull(FUNLCURR,''),
         @FUNCRIDX = isnull(FUNCRIDX,0)
  from   MC40000 (nolock)
  
  select @CURRNIDX = isnull(CURRNIDX,0)
  from   DYNAMICS..MC40200 (nolock)
  where  CURNCYID = @I_vCURNCYID
  
  select @FCDECPLCUR = isnull(DECPLCUR - 1,@UnitCurrPlace)
  from   DYNAMICS..MC40200
  WHERE  CURNCYID = (SELECT FUNLCURR
                     FROM   MC40000)
  
  if (@I_vCURNCYID <> @FUNLCURR)
    begin
      select @OCDECPLCUR = isnull(DECPLCUR - 1,@UnitCurrPlace)
      from   DYNAMICS..MC40200
      WHERE  CURNCYID = @I_vCURNCYID
    end
  else
    begin
      select @OCDECPLCUR = @FCDECPLCUR
    end
  
  if ((@I_vPAPROJNUMBER <> '')
      and (@I_vPAPROJNUMBER <> '<NONE>'))
    begin
      select @DEX_ROW_ID = DEX_ROW_ID,
             @PASTAT = isnull(PASTAT,0),
             @PAcloseProjcosts = isnull(PAcloseProjcosts,0),
             @PACONTNUMBER = isnull(PACONTNUMBER,''),
             @PAProjectType = isnull(PAProjectType,0),
             @PAAcctgMethod = isnull(PAAcctgMethod,0),
             @CUSTNMBR = isnull(CUSTNMBR,'')
      from   PA01201 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      select @PALabor_Rate_Table_ID = isnull(PALabor_Rate_Table_ID,''),
             @PALabor_RateTable_Type = isnull(PALabor_RateTable_Type,0)
      from   PA01301 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PACOSTCATID = @I_vPACOSTCATID
             and PATU = @PATU
      
      if (@PALabor_Rate_Table_ID = '')
        begin
          select @PALabor_Rate_Table_ID = isnull(PALabor_Rate_Table_ID,''),
                 @PALabor_RateTable_Type = isnull(PALabor_RateTable_Type,0)
          from   PA01201 (nolock)
          where  PAPROJNUMBER = @I_vPAPROJNUMBER
        end
      
      if (@DEX_ROW_ID IS NULL)
        begin
          select @O_iErrorState = 1214
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if (@PASTAT = 2)
              or (@PASTAT = 3)
              or (@PASTAT = 4)
            begin
              select @O_iErrorState = 1215
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          if (@PAcloseProjcosts = 1)
            begin
              select @O_iErrorState = 6449
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  else
    begin
      select @PAProjectType = 1,
             @PACONTNUMBER = '<NONE>',
             @PABilling_StatusN = 4,
             @I_vPAPROJNUMBER = '<NONE>'
    end
  
  if (@PACONTNUMBER <> '')
    begin
      select @DEX_ROW_ID = DEX_ROW_ID,
             @PASTAT = ISNULL(PASTAT,0),
             @PAcloseProjcosts = ISNULL(PAcloseProjcosts,0),
             @CUSTNMBR = ISNULL(CUSTNMBR,''),
             @PAMCCURNCYID = ISNULL(PAMCCURNCYID,'')
      from   PA01101 (nolock)
      where  PACONTNUMBER = @PACONTNUMBER
      
      if (@DEX_ROW_ID IS NULL)
        begin
          select @O_iErrorState = 1250
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if ((@PASTAT = 2)
               or (@PASTAT = 3)
               or (@PASTAT = 4))
            begin
              select @O_iErrorState = 1251
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          if (@PAcloseProjcosts = 1)
            begin
              select @O_iErrorState = 6450
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          select @DEX_ROW_ID = DEX_ROW_ID,
                 @PAcloseProjcosts = ISNULL(PAcloseProjcosts,0)
          from   PA00501 (nolock)
          where  CUSTNMBR = @CUSTNMBR
          
          if (@DEX_ROW_ID IS NULL)
            begin
              select @O_iErrorState = 1252
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          else
            begin
              if (@PAcloseProjcosts = 1)
                begin
                  select @O_iErrorState = 1253
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
        end
    end
  
  if (@PAMCCURNCYID <> '')
    select @PAMCCURRNIDX = isnull(CURRNIDX,0)
    from   DYNAMICS..MC40200 (nolock)
    where  CURNCYID = @PAMCCURNCYID
  
  if @FUNLCURR <> @PAMCCURNCYID
    begin
      select @IsMC = 1
      
      select @RATETPID = isnull(PAFRATETPID,''),
             @EXGTBLID = isnull(PAFEXGTBLID,''),
             @XCHGRATE = isnull(PAFXCHGRATE,0),
             @EXCHDATE = isnull(PAFEXCHDATE,@EMPTYDATE),
             @TIME1 = isnull(PAFTIME1,@EMPTYDATE),
             @RATECALC = isnull(PAFRTCLCMTD,0),
             @DENXRATE = isnull(PAFDENXRATE,0),
             @MCTRXSTT = isnull(PAFMCTRXSTT,0)
      from   PA01202 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
    end
  
  if (@I_vPATSTYP = 2)
    begin
      if (@I_vPAREFNO = '')
        begin
          select @O_iErrorState = 1216
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if not exists (select 1
                         from   PA30101 (nolock)
                         where  PATSNO = @I_vPAREFNO)
            begin
              select @O_iErrorState = 1217
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if (@I_vCURNCYID <> @FUNLCURR)
     and (@I_vPAUNITCOST = 0)
    begin
      select @O_iErrorState = 5397
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPACOSTCATID <> '')
    begin
      select @DEX_ROW_ID = DEX_ROW_ID,
             @PATU = isnull(PATU,0),
             @PAinactive = isnull(PAinactive,0)
      from   PA01001 (nolock)
      where  PACOSTCATID = @I_vPACOSTCATID
      
      if (@DEX_ROW_ID IS NULL)
        begin
          select @O_iErrorState = 1218
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if (@PATU <> 1)
            begin
              select @O_iErrorState = 1219
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          if (@PAinactive = 1)
            begin
              select @O_iErrorState = 1220
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          if ((@I_vPAPROJNUMBER = '<NONE>')
              and (@I_vPACOSTCATID = '<NONE>'))
            begin
              if ((@PATU = 1)
                   or (@PATU = 2)
                   or (@PATU = 3))
                begin
                  select @O_iErrorState = 1221
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
          
          if (@I_vPAPROJNUMBER <> '<NONE>')
            begin
              if (@I_vPACOSTCATID <> '<NONE>')
                begin
                  if not exists (select 1
                                 from   PA01301 (nolock)
                                 where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                        and PACOSTCATID = @I_vPACOSTCATID
                                        and PATU = @PATU)
                    begin
                      execute @iStatus = taPAProjectBudgetMaster
                        @I_vPAPROJNUMBER = @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID = @I_vPACOSTCATID ,
                        @I_vPABQuantity = 0 ,
                        @O_iErrorState = @taPAProjectBudgetMasterErrorState output ,
                        @oErrString = @otaPAProjectBudgetMasterErrString output
                      
                      select @iError = @@error
                      
                      if ((@iStatus <> 0)
                           or (@taPAProjectBudgetMasterErrorState <> 0)
                           or (@iError <> 0))
                        begin
                          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@taPAProjectBudgetMasterErrorState))
                          
                          select @O_iErrorState = 6413
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      select @temp_PAProfitType = PAProfitType,
                             @temp_PAbllngtype = PAbllngtype,
                             @temp_LNITMSEQ = LNITMSEQ,
                             @temp_PALineItemSeq = PALineItemSeq,
                             @temp_PAFQuantity = PAFQuantity,
                             @temp_PAFUnitCost = PAFUnitCost,
                             @temp_PAFTotalCost = PAFTotalCost,
                             @temp_PAFProfitAmt = PAFProfitAmt,
                             @temp_PAFProfitPcnt = PAFProfitPcnt,
                             @temp_PAForecastOvrhdAmtPerUnt = PAForecastOvrhdAmtPerUnt,
                             @temp_PAForecastOvrhdPct = PAForecastOvrhdPct,
                             @temp_PAForecastPTaxOptions = PAForecastPTaxOptions,
                             @temp_PAForecastCTaxSchedID = PAForecastCTaxSchedID,
                             @temp_PAForecastSTaxOptions = PAForecastSTaxOptions,
                             @temp_PAForecastBTaxSchedID = PAForecastBTaxSchedID,
                             @temp_PAFBeginDate = PAFBeginDate,
                             @temp_PAFEndDate = PAFEndDate
                      from   PA01301 (nolock)
                      where  PAPROJNUMBER = @I_vPAPROJNUMBER
                             and PACOSTCATID = @I_vPACOSTCATID
                             and PATU = @PATU
                      
                      execute @iStatus = taPAProjectBudgetMaster
                        @I_vPAPROJNUMBER = @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID = @I_vPACOSTCATID ,
                        @I_vPASTAT = 1 ,
                        @I_vPAProfitType = @temp_PAProfitType ,
                        @I_vPAbllngtype = @temp_PAbllngtype ,
                        @I_vLNITMSEQ = @temp_LNITMSEQ ,
                        @I_vPALineItemSeq = @temp_PALineItemSeq ,
                        @I_vPAFQuantity = @temp_PAFQuantity ,
                        @I_vPAFUnitCost = @temp_PAFUnitCost ,
                        @I_vPAFTotalCost = @temp_PAFTotalCost ,
                        @I_vPAFProfitAmt = @temp_PAFProfitAmt ,
                        @I_vPAFProfitPcnt = @temp_PAFProfitPcnt ,
                        @I_vPAForecastOvrhdAmtPerUnt = @temp_PAForecastOvrhdAmtPerUnt ,
                        @I_vPAForecastOvrhdPct = @temp_PAForecastOvrhdPct ,
                        @I_vPAForecastPTaxOptions = @temp_PAForecastPTaxOptions ,
                        @I_vPAForecastCTaxSchedID = @temp_PAForecastCTaxSchedID ,
                        @I_vPAForecastSTaxOptions = @temp_PAForecastSTaxOptions ,
                        @I_vPAForecastBTaxSchedID = @temp_PAForecastBTaxSchedID ,
                        @I_vPAFBeginDate = @temp_PAFBeginDate ,
                        @I_vPAFEndDate = @temp_PAFEndDate ,
                        @I_vUpdateExisting = 1 ,
                        @O_iErrorState = @taPAProjectBudgetMasterErrorState output ,
                        @oErrString = @otaPAProjectBudgetMasterErrString output
                      
                      select @iError = @@error
                      
                      if ((@iStatus <> 0)
                           or (@taPAProjectBudgetMasterErrorState <> 0)
                           or (@iError <> 0))
                        begin
                          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@taPAProjectBudgetMasterErrorState))
                          
                          select @O_iErrorState = 6414
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                  
                  select @DECPLCUR = (isnull(DECPLCUR,0) + 6)
                  from   PA01301 (nolock)
                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                         and PACOSTCATID = @I_vPACOSTCATID
                         and PATU = @PATU
                  
                  if (@I_vCURNCYID <> @FUNLCURR)
                    select @ODECPLCU = case 
                                         when ((@CURRNIDX = 0)
                                                or (@CURRNIDX = 2999)) then (((@DECPLCUR - 6) - 1) + 7)
                                         else ((@CURRNIDX - 1000) * 6) + 2000 + ((@DECPLCUR - 6) - 1)
                                       end
                  
                  if (@DECPLCUR >= 7)
                    begin
                      select @UnitCurrPlace = @DECPLCUR - 7
                    end
                  else
                    begin
                      select @UnitCurrPlace = @DECPLCUR
                    end
                  
                  select @l_PAPROJNUMBER = isnull(PAPROJNUMBER,''),
                         @PASTAT = isnull(PASTAT,0),
                         @DECPLQTY = (isnull(DECPLQTY,0) - 1),
                         @DECPLCUR = (isnull(DECPLCUR,0) + 6),
                         @I_vPAUnit_of_Measure = case 
                                                   when @I_vPAUnit_of_Measure = '' then isnull(PAUnit_of_Measure,'')
                                                   else @I_vPAUnit_of_Measure
                                                 end,
                         @UOMSCHDL = isnull(UOMSCHDL,''),
                         @PAbllngtype = isnull(PAbllngtype,0),
                         @PAOverhead_Amount = isnull(PAForecastOvrhdAmtPerUnt,0),
                         @PAOverheaPercentage = isnull(PAForecastOvrhdPct,0),
                         @PABilling_StatusN = case 
                                                WHEN @PAProjectType = 1
                                                     and @I_vPAbllngtype <> 3 THEN 1
                                                WHEN @PAProjectType <> 1
                                                     and @I_vPAbllngtype <> 3 THEN 5
                                                ELSE 4
                                              END
                  from   PA01301 (nolock)
                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                         and PACOSTCATID = @I_vPACOSTCATID
                         and PATU = @PATU
                  
                  if (@l_PAPROJNUMBER = '')
                    begin
                      select @O_iErrorState = 1222
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  else
                    begin
                      if (@PASTAT not in (1,5,6,7,
                                          8,9,10))
                        begin
                          select @O_iErrorState = 1223
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                end
            end
          
          if (@I_vPATSTYP = 2)
            begin
              if (@PAReference_Line_Seq_N = '')
                begin
                  if not exists (select 1
                                 from   PA30101 (nolock)
                                 where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                        and PACOSTCATID = @I_vPACOSTCATID
                                        and PADT = @I_vPADT
                                        and PAREFNO = @I_vPAREFNO)
                    begin
                      select @O_iErrorState = 1224
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                end
              
              select @PAReference_Line_Seq_N = LNITMSEQ,
                     @I_vPADT = PADT,
                     @I_vPAPROJNUMBER = PAPROJNUMBER,
                     @I_vPACOSTCATID = PACOSTCATID,
                     @PACONTNUMBER = PACONTNUMBER,
                     @PA_Base_Billing_Rate = PA_Base_Billing_Rate,
                     @PAORIGBSBILLRTE = PAORIGBSBILLRTE,
                     @PABase_Unit_Cost = PABase_Unit_Cost,
                     @PAORGBSUNITCST = PAORGBSUNITCST,
                     @PABILRATE = PABILRATE,
                     @PAORIGBILLRATE = PAORIGBILLRATE,
                     @PAbllngtype = PAbllngtype,
                     @I_vPADepartment = PADepartment,
                     @I_vPAJob_Title = PAJob_Title,
                     @PAMARKPERCENT = PAMARKPERCENT,
                     @PAProfitType = PAProfitType,
                     @I_vPAPayCode = PAPayCode,
                     @I_vPASalary_Posting_Type = PASalary_Posting_Type,
                     @PAOverhead_Amount = PAOverhead_Amount,
                     @PAORIGOVHDAMT = PAORIGOVHDAMT,
                     @PABaseOvhdCost = PABaseOvhdCost,
                     @PAORIGBASEOVRHCST = PAORIGBASEOVRHCST,
                     @PAOverheaPercentage = PAOverheaPercentage,
                     @I_vPAUNITCOST = PAUNITCOST,
                     @PAUNITCOST = PAUNITCOST,
                     @ORUNTCST = ORUNTCST,
                     @I_vPAUnit_of_Measure = PAUnit_of_Measure,
                     @UOMSCHDL = UOMSCHDL,
                     @PACogs_Idx = PACogs_Idx,
                     @PAContra_Account_IDX = PAContra_Account_IDX,
                     @PAOverhead_IDX = PAOverhead_IDX,
                     @PAUnbilled_AR_Idx = PAUnbilled_AR_Idx,
                     @PAUnbilled_Proj_Rev_Idx = PAUnbilled_Proj_Rev_Idx,
                     @PACGBWIPIDX = PACGBWIPIDX,
                     @RNDDIFF = RNDDIFF,
                     @LOCLCODE = LOCLCODE,
                     @STATECD = STATECD,
                     @SUTASTAT = SUTASTAT,
                     @WRKRCOMP = WRKRCOMP,
                     @PAProfitAmount = PAProfitAmount,
                     @PAORIGPROFAMT = PAORIGPROFAMT,
                     @PAProfitPercent = PAProfitPercent,
                     @PABilling_StatusN = case 
                                            WHEN @PAProjectType = 1
                                                 and @I_vPAbllngtype <> 3 THEN 1
                                            WHEN @PAProjectType <> 1
                                                 and @I_vPAbllngtype <> 3 THEN 5
                                            ELSE 4
                                          END
              from   PA30101 (nolock)
              where  PAPROJNUMBER = @I_vPAPROJNUMBER
                     and PACOSTCATID = @I_vPACOSTCATID
                     and PADT = @I_vPADT
                     and PAREFNO = @I_vPAREFNO
              
              if (@I_vPAPROJNUMBER = '<NONE>')
                select @DECPLQTY = (isnull(DECPLQTY,0) - 1),
                       @DECPLCUR = isnull(DECPLCUR,0)
                from   PA01001 (nolock)
                where  PACOSTCATID = @I_vPACOSTCATID
            end
          else
            begin
              if (@I_vPAPROJNUMBER = '<NONE>')
                begin
                  select @DECPLQTY = (isnull(DECPLQTY,0) - 1),
                         @DECPLCUR = (isnull(DECPLCUR,0) + 6),
                         @I_vPAUnit_of_Measure = case 
                                                   when @I_vPAUnit_of_Measure = '' then isnull(PAUnit_of_Measure,'')
                                                   else @I_vPAUnit_of_Measure
                                                 end,
                         @UOMSCHDL = isnull(UOMSCHDL,''),
                         @PAbllngtype = 3,
                         @PAOverheaPercentage = isnull(PAOverheaPercentage,0),
                         @PABilling_StatusN = 4
                  from   PA01001 (nolock)
                  where  PACOSTCATID = @I_vPACOSTCATID
                end
              
              if (@I_vPAbllngtype = -1)
                 and (@PAbllngtype > 0)
                select @I_vPAbllngtype = @PAbllngtype
              
              if (@I_vPAbllngtype = -1)
                 and (@PAbllngtype = 0)
                select @I_vPAbllngtype = 1
              
              if ((@I_vPAUnit_of_Measure <> '')
                  and (@UOMSCHDL <> ''))
                begin
                  select @BASEUOFM = BASEUOFM
                  from   PA40101 (nolock)
                  where  UOMSCHDL = @UOMSCHDL
                  
                  if not exists (select 1
                                 from   PA40102 (nolock)
                                 where  UOMSCHDL = @UOMSCHDL
                                        and UOFM = @I_vPAUnit_of_Measure
                                        and EQUIVUOM = @BASEUOFM)
                    begin
                      select @O_iErrorState = 1225
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                end
              
              select @PACogs_Idx = 0,
                     @PAContra_Account_IDX = 0,
                     @PAOverhead_IDX = 0,
                     @PAUnbilled_AR_Idx = 0,
                     @PAUnbilled_Proj_Rev_Idx = 0,
                     @PACGBWIPIDX = 0,
                     @RNDDIFF = 0
              
              if (@PAProjectType = 1)
                begin
                  if (@I_vPAbllngtype = 1)
                    begin
                      if (@PAAcctgMethod <> 1)
                        begin
                          exec @iStatus = taPAAuxAcctsGetIdx
                            @I_vPAPROJNUMBER ,
                            @I_vPACOSTCATID ,
                            @I_vEMPLOYID ,
                            1 ,
                            'TS' ,
                            1 ,
                            @CUSTNMBR ,
                            @PACONTNUMBER ,
                            @PACGBWIPIDX output ,
                            @O_oErrorState output
                          
                          select @iError = @@error
                          
                          if @iStatus = 0
                             and @iError <> 0
                            select @iStatus = @iError
                          
                          if (@iStatus <> 0)
                              or (@O_oErrorState <> 0)
                            begin
                              select @O_iErrorState = 1226
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                        end
                      
                      if (IsNull(@PACGBWIPIDX,0) = 0)
                        begin
                          exec @iStatus = taPAAuxAcctsGetIdx
                            @I_vPAPROJNUMBER ,
                            @I_vPACOSTCATID ,
                            @I_vEMPLOYID ,
                            1 ,
                            'TS' ,
                            2 ,
                            @CUSTNMBR ,
                            @PACONTNUMBER ,
                            @PACogs_Idx output ,
                            @O_oErrorState output
                          
                          select @iError = @@error
                          
                          if @iStatus = 0
                             and @iError <> 0
                            select @iStatus = @iError
                          
                          if (@iStatus <> 0)
                              or (@O_oErrorState <> 0)
                            begin
                              select @O_iErrorState = 1227
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                        end
                    end
                  else
                    begin
                      exec @iStatus = taPAAuxAcctsGetIdx
                        @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID ,
                        @I_vEMPLOYID ,
                        1 ,
                        'TS' ,
                        2 ,
                        @CUSTNMBR ,
                        @PACONTNUMBER ,
                        @PACogs_Idx output ,
                        @O_oErrorState output
                      
                      select @iError = @@error
                      
                      if @iStatus = 0
                         and @iError <> 0
                        select @iStatus = @iError
                      
                      if (@iStatus <> 0)
                          or (@O_oErrorState <> 0)
                        begin
                          select @O_iErrorState = 1228
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                  
                  if ((@I_vPAPROJNUMBER <> '<NONE>')
                      and (@PAAcctgMethod <> 2))
                    begin
                      exec @iStatus = taPAAuxAcctsGetIdx
                        @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID ,
                        @I_vEMPLOYID ,
                        1 ,
                        'TS' ,
                        4 ,
                        @CUSTNMBR ,
                        @PACONTNUMBER ,
                        @PAUnbilled_AR_Idx output ,
                        @O_oErrorState output
                      
                      select @iError = @@error
                      
                      if @iStatus = 0
                         and @iError <> 0
                        select @iStatus = @iError
                      
                      if (@iStatus <> 0)
                          or (@O_oErrorState <> 0)
                        begin
                          select @O_iErrorState = 1229
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (@PAUnbilled_AR_Idx <> 0)
                        begin
                          exec @iStatus = taPAAuxAcctsGetIdx
                            @I_vPAPROJNUMBER ,
                            @I_vPACOSTCATID ,
                            @I_vEMPLOYID ,
                            1 ,
                            'TS' ,
                            5 ,
                            @CUSTNMBR ,
                            @PACONTNUMBER ,
                            @PAUnbilled_Proj_Rev_Idx output ,
                            @O_oErrorState output
                          
                          select @iError = @@error
                          
                          if @iStatus = 0
                             and @iError <> 0
                            select @iStatus = @iError
                          
                          if (@iStatus <> 0)
                              or (@O_oErrorState <> 0)
                            begin
                              select @O_iErrorState = 1230
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                          
                          if (@PAUnbilled_Proj_Rev_Idx = 0)
                            select @PAUnbilled_AR_Idx = 0
                        end
                    end
                  
                  exec @iStatus = taPAAuxAcctsGetIdx
                    @I_vPAPROJNUMBER ,
                    @I_vPACOSTCATID ,
                    @I_vEMPLOYID ,
                    1 ,
                    'TS' ,
                    3 ,
                    @CUSTNMBR ,
                    @PACONTNUMBER ,
                    @PAContra_Account_IDX output ,
                    @O_oErrorState output
                  
                  select @iError = @@error
                  
                  if @iStatus = 0
                     and @iError <> 0
                    select @iStatus = @iError
                  
                  if (@iStatus <> 0)
                      or (@O_oErrorState <> 0)
                    begin
                      select @O_iErrorState = 1231
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  
                  if (@PAContra_Account_IDX <> 0)
                    begin
                      exec @iStatus = taPAAuxAcctsGetIdx
                        @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID ,
                        @I_vEMPLOYID ,
                        1 ,
                        'TS' ,
                        8 ,
                        @CUSTNMBR ,
                        @PACONTNUMBER ,
                        @PAOverhead_IDX output ,
                        @O_oErrorState output
                      
                      select @iError = @@error
                      
                      if @iStatus = 0
                         and @iError <> 0
                        select @iStatus = @iError
                      
                      if (@iStatus <> 0)
                          or (@O_oErrorState <> 0)
                        begin
                          select @O_iErrorState = 1232
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                  else
                    begin
                      select @PACGBWIPIDX = 0,
                             @PACogs_Idx = 0
                    end
                end
              else
                begin
                  exec @iStatus = taPAAuxAcctsGetIdx
                    @I_vPAPROJNUMBER ,
                    @I_vPACOSTCATID ,
                    @I_vEMPLOYID ,
                    1 ,
                    'TS' ,
                    30 ,
                    @CUSTNMBR ,
                    @PACONTNUMBER ,
                    @PACGBWIPIDX output ,
                    @O_oErrorState output
                  
                  select @iError = @@error
                  
                  if @iStatus = 0
                     and @iError <> 0
                    select @iStatus = @iError
                  
                  if (@iStatus <> 0)
                      or (@O_oErrorState <> 0)
                    begin
                      select @O_iErrorState = 1239
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  
                  if (@PACGBWIPIDX <> 0)
                    begin
                      exec @iStatus = taPAAuxAcctsGetIdx
                        @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID ,
                        @I_vEMPLOYID ,
                        1 ,
                        'TS' ,
                        31 ,
                        @CUSTNMBR ,
                        @PACONTNUMBER ,
                        @PAContra_Account_IDX output ,
                        @O_oErrorState output
                      
                      select @iError = @@error
                      
                      if @iStatus = 0
                         and @iError <> 0
                        select @iStatus = @iError
                      
                      if (@iStatus <> 0)
                          or (@O_oErrorState <> 0)
                        begin
                          select @O_iErrorState = 1240
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (@PAContra_Account_IDX <> 0)
                        begin
                          exec @iStatus = taPAAuxAcctsGetIdx
                            @I_vPAPROJNUMBER ,
                            @I_vPACOSTCATID ,
                            @I_vEMPLOYID ,
                            1 ,
                            'TS' ,
                            39 ,
                            @CUSTNMBR ,
                            @PACONTNUMBER ,
                            @PAOverhead_IDX output ,
                            @O_oErrorState output
                          
                          select @iError = @@error
                          
                          if @iStatus = 0
                             and @iError <> 0
                            select @iStatus = @iError
                          
                          if (@iStatus <> 0)
                              or (@O_oErrorState <> 0)
                            begin
                              select @O_iErrorState = 1241
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                        end
                      else
                        begin
                          select @PACGBWIPIDX = 0
                        end
                    end
                  else
                    begin
                      select @PAContra_Account_IDX = 0
                    end
                  
                  if (@FUNCRIDX <> @CURRNIDX)
                    begin
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PACGBWIPIDX
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PACGBWIPIDX <> 0))
                        begin
                          select @O_iErrorState = 1233
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PACogs_Idx
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PACogs_Idx <> 0))
                        begin
                          select @O_iErrorState = 1234
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PAUnbilled_AR_Idx
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PAUnbilled_AR_Idx <> 0))
                        begin
                          select @O_iErrorState = 1235
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PAUnbilled_Proj_Rev_Idx
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PAUnbilled_Proj_Rev_Idx <> 0))
                        begin
                          select @O_iErrorState = 1236
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PAContra_Account_IDX
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PAContra_Account_IDX <> 0))
                        begin
                          select @O_iErrorState = 1237
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PAOverhead_IDX
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PAOverhead_IDX <> 0))
                        begin
                          select @O_iErrorState = 1238
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                end
              
              select @PAUsePayCodes = PAUsePayCodes
              from   PA41701 (nolock)
              
              select @PAYRTDEC = isnull(PAYRTDEC,0)
              from   UPR40200 (nolock)
              
              if (@PAtsunitcostfrom = 1)
                begin
                  select @PAPayCode = isnull(PAPayCode,''),
                         @PAUNITCOST = isnull(PAUNITCOST,0),
                         @PABase_Unit_Cost = isnull(PAUNITCOST,0),
                         @I_vPAUnit_of_Measure = isnull(PAUnit_of_Measure,0),
                         @PAOverhead_Amount = isnull(PAOvhdAmtPerUnit,0),
                         @PABaseOvhdCost = isnull(PAOvhdAmtPerUnit,0),
                         @PAOverheaPercentage = isnull(PAOverheaPercentage,0)
                  from   PA00601 (nolock)
                  where  EMPLOYID = @I_vEMPLOYID
                  
                  if (@PALabor_Rate_Table_ID <> '')
                    begin
                      if (@PAPayCode = '')
                         and (@PAEmployee_Type = 0)
                        begin
                          select @PAPayCode = isnull(PAPay_Code_Hourly,0)
                          from   PA01301 (nolock)
                          where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                 and PACOSTCATID = @I_vPACOSTCATID
                                 and PATU = @PATU
                        end
                      else
                        if (@PAPayCode = '')
                           and (@PAEmployee_Type <> 0)
                          begin
                            select @PAPayCode = isnull(PAPay_Code_Salary,0)
                            from   PA01301 (nolock)
                            where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                   and PACOSTCATID = @I_vPACOSTCATID
                                   and PATU = @PATU
                          end
                      
                      if (@PAPayCode = '')
                         and (@PAEmployee_Type = 0)
                        begin
                          select @PAPayCode = isnull(PAPay_Code_Hourly,0)
                          from   PA01001 (nolock)
                          where  PACOSTCATID = @I_vPACOSTCATID
                        end
                      else
                        if (@PAPayCode = '')
                           and (@PAEmployee_Type <> 0)
                          begin
                            select @PAPayCode = isnull(PAPay_Code_Salary,0)
                            from   PA01001 (nolock)
                            where  PACOSTCATID = @I_vPACOSTCATID
                          end
                    end
                end
              
              if (@PAtsunitcostfrom = 2)
                begin
                  if (@I_vPAPROJNUMBER <> '<NONE>')
                    begin
                      select @PAUNITCOST = isnull(PAFUnitCost,0),
                             @PABase_Unit_Cost = isnull(PABase_Unit_Cost,0)
                      from   PA01301 (nolock)
                      where  PAPROJNUMBER = @I_vPAPROJNUMBER
                             and PACOSTCATID = @I_vPACOSTCATID
                             and PATU = @PATU
                    end
                  else
                    begin
                      select @PAUNITCOST = isnull(PAUNITCOST,0),
                             @PABase_Unit_Cost = isnull(PAUNITCOST,0),
                             @I_vPAUnit_of_Measure = isnull(PAUnit_of_Measure,0),
                             @PAPay_Code_Hourly = isnull(PAPay_Code_Hourly,0),
                             @PAPay_Code_Salary = isnull(PAPay_Code_Salary,0)
                      from   PA01001 (nolock)
                      where  PACOSTCATID = @I_vPACOSTCATID
                      
                      if (@PAEmployee_Type = 0)
                        begin
                          select @PAPayCode = @PAPay_Code_Hourly
                        end
                      else
                        begin
                          select @PAPayCode = @PAPay_Code_Salary
                        end
                    end
                  
                  if (@PAPayCode = '')
                     and (@PAEmployee_Type = 0)
                    begin
                      select @PAPayCode = isnull(PAPay_Code_Hourly,0)
                      from   PA01301 (nolock)
                      where  PAPROJNUMBER = @I_vPAPROJNUMBER
                             and PACOSTCATID = @I_vPACOSTCATID
                             and PATU = @PATU
                    end
                  else
                    if (@PAPayCode = '')
                       and (@PAEmployee_Type <> 0)
                      begin
                        select @PAPayCode = isnull(PAPay_Code_Salary,0)
                        from   PA01301 (nolock)
                        where  PAPROJNUMBER = @I_vPAPROJNUMBER
                               and PACOSTCATID = @I_vPACOSTCATID
                               and PATU = @PATU
                      end
                  
                  if (@PALabor_Rate_Table_ID <> '')
                    begin
                      if (@PAPayCode = '')
                        begin
                          select @PAPayCode = isnull(PAPayCode,'')
                          from   PA00601 (nolock)
                          where  EMPLOYID = @I_vEMPLOYID
                        end
                      
                      if (@PAPayCode = '')
                         and (@PAEmployee_Type = 0)
                        begin
                          select @PAPayCode = isnull(PAPay_Code_Hourly,0)
                          from   PA01001 (nolock)
                          where  PACOSTCATID = @I_vPACOSTCATID
                        end
                      else
                        if (@PAPayCode = '')
                           and (@PAEmployee_Type <> 0)
                          begin
                            select @PAPayCode = isnull(PAPay_Code_Salary,0)
                            from   PA01001 (nolock)
                            where  PACOSTCATID = @I_vPACOSTCATID
                          end
                    end
                end
              
              if (@PAtsunitcostfrom = 3)
                begin
                  select @PAUNITCOST = isnull(PAUNITCOST,0),
                         @PABase_Unit_Cost = isnull(PAUNITCOST,0),
                         @I_vPAUnit_of_Measure = isnull(PAUnit_of_Measure,0),
                         @PAOverhead_Amount = isnull(PAOvhdAmtPerUnit,0),
                         @PABaseOvhdCost = isnull(PAOvhdAmtPerUnit,0),
                         @PAOverheaPercentage = isnull(PAOverheaPercentage,0)
                  from   PA01001 (nolock)
                  where  PACOSTCATID = @I_vPACOSTCATID
                  
                  if (@PAPayCode = '')
                     and (@PAEmployee_Type = 0)
                    begin
                      select @PAPayCode = isnull(PAPay_Code_Hourly,0)
                      from   PA01001 (nolock)
                      where  PACOSTCATID = @I_vPACOSTCATID
                    end
                  else
                    if (@PAPayCode = '')
                       and (@PAEmployee_Type <> 0)
                      begin
                        select @PAPayCode = isnull(PAPay_Code_Salary,0)
                        from   PA01001 (nolock)
                        where  PACOSTCATID = @I_vPACOSTCATID
                      end
                  
                  if (@PALabor_Rate_Table_ID <> '')
                    begin
                      if (@PAPayCode = '')
                        begin
                          select @PAPayCode = isnull(PAPayCode,'')
                          from   PA00601 (nolock)
                          where  EMPLOYID = @I_vEMPLOYID
                        end
                      
                      if (@PAPayCode = '')
                         and (@PAEmployee_Type = 0)
                        begin
                          select @PAPayCode = isnull(PAPay_Code_Hourly,0)
                          from   PA01301 (nolock)
                          where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                 and PACOSTCATID = @I_vPACOSTCATID
                                 and PATU = @PATU
                        end
                      else
                        if (@PAPayCode = '')
                           and (@PAEmployee_Type <> 0)
                          begin
                            select @PAPayCode = isnull(PAPay_Code_Salary,0)
                            from   PA01301 (nolock)
                            where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                   and PACOSTCATID = @I_vPACOSTCATID
                                   and PATU = @PATU
                          end
                    end
                end
              
              if (@PAUsePayCodes = 1)
                begin
                  select @PAUNITCOST = 0,
                         @PABase_Unit_Cost = 0,
                         @I_vPAUnit_of_Measure = ''
                  
                  if (@I_vPAPayCode = '')
                    select @I_vPAPayCode = @PAPayCode
                  
                  select @WKHRPRYR = isnull(WKHRPRYR,0)
                  from   UPR00100 (nolock)
                  where  EMPLOYID = @I_vEMPLOYID
                  
                  select @PAYUNPER = isnull(PAYUNPER,0),
                         @PAYUNIT = isnull(PAYUNIT,''),
                         @PAYPRPRD = isnull(PAYPRPRD,0),
                         @PAYPEROD = isnull(PAYPEROD,0),
                         @PAYRCORD = isnull(PAYRCORD,''),
                         @PAYTYPE = isnull(PAYTYPE,0),
                         @PAYRTAMT = isnull(PAYRTAMT,0),
                         @BSPAYRCD = isnull(BSPAYRCD,'')
                  from   UPR00400 (nolock)
                  where  EMPLOYID = @I_vEMPLOYID
                         and PAYRCORD = @I_vPAPayCode
                  
                  select @l_pay_period = case 
                                           when @PAYPEROD = 1 then 52
                                           when @PAYPEROD = 2 then 26
                                           when @PAYPEROD = 3 then 24
                                           when @PAYPEROD = 4 then 12
                                           when @PAYPEROD = 5 then 4
                                           when @PAYPEROD = 6 then 2
                                           when @PAYPEROD = 7 then 1
                                           when @PAYPEROD = 8 then 260
                                         end
                  
                  select @l_pay_unit_period = case 
                                                when @PAYUNPER = 1 then 52
                                                when @PAYUNPER = 2 then 26
                                                when @PAYUNPER = 3 then 24
                                                when @PAYUNPER = 4 then 12
                                                when @PAYUNPER = 5 then 4
                                                when @PAYUNPER = 6 then 2
                                                when @PAYUNPER = 7 then 1
                                                when @PAYUNPER = 8 then 260
                                              end
                  
                  if (@PAYRCORD = '')
                    begin
                      select @O_iErrorState = 1245
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  
                  select @I_vPAUnit_of_Measure = 'Hour'
                  
                  if ((@PAYTYPE = 1)
                       or (@PAYTYPE = 16))
                    begin
                      select @PAUNITCOST = @PAYRTAMT
                    end
                  else
                    if (@PAYTYPE = 2)
                      begin
                        select @l_hours_worked = round(@WKHRPRYR / @l_pay_period,@OCDECPLCUR)
                        
                        if @l_hours_worked > 0
                          begin
                            select @PAUNITCOST = @PAYPRPRD / @l_hours_worked
                          end
                      end
                    else
                      if (@PAYTYPE = 7)
                        begin
                          select @PAYTYPE = isnull(PAYTYPE,0)
                          from   UPR40600 (nolock)
                          where  PAYRCORD = @BSPAYRCD
                          
                          if (@PAYTYPE = 2)
                            begin
                              if (@l_pay_period > 0)
                                select @l_hours_worked = round(@WKHRPRYR / @l_pay_period,@PAYRTDEC + 1)
                              
                              if @l_hours_worked > 0
                                select @PAUNITCOST = ((@PAYRTAMT * @l_pay_unit_period / @l_pay_period) / @l_hours_worked)
                            end
                          else
                            begin
                              select @PAUNITCOST = @PAYRTAMT
                            end
                        end
                      else
                        if ((@PAYTYPE = 3)
                             or (@PAYTYPE = 15)
                             or (@PAYTYPE = 14))
                          begin
                            select @PAUNITCOST = @PAYRTAMT,
                                   @I_vPAUnit_of_Measure = @PAYUNIT
                          end
                        else
                          if ((@PAYTYPE = 4)
                               or (@PAYTYPE = 5)
                               or (@PAYTYPE = 12))
                            begin
                              select @PAUNITCOST = @PAYRTAMT
                            end
                          else
                            if ((@PAYTYPE = 6)
                                 or (@PAYTYPE = 8)
                                 or (@PAYTYPE = 9)
                                 or (@PAYTYPE = 10))
                              begin
                                select @PAYTYPE = isnull(PAYTYPE,0)
                                from   UPR40600 (nolock)
                                where  PAYRCORD = @BSPAYRCD
                                
                                if (@PAYTYPE = 2)
                                  begin
                                    if (@l_pay_period > 0)
                                      select @l_hours_worked = round(@WKHRPRYR / @l_pay_period,@PAYRTDEC + 1)
                                    
                                    if @l_hours_worked > 0
                                      select @PAUNITCOST = (@PAYRTAMT * @l_pay_unit_period / @l_pay_period) / @l_hours_worked
                                  end
                                else
                                  begin
                                    select @PAUNITCOST = @PAYRTAMT
                                  end
                              end
                  
                  select @PAUNITCOST = round(@PAUNITCOST,@PAYRTDEC + 1)
                end
              
              if (@I_vPAPROJNUMBER <> '<NONE>')
                if (@PALabor_Rate_Table_ID <> '')
                  begin
                    if (@PALabor_RateTable_Type = 1)
                      begin
                        select @l_PALabor_Rate_Table_ID = isnull(PARate_Table_ID,''),
                               @PAUNITCOST = isnull(PAUNITCOST,0),
                               @PABase_Unit_Cost = isnull(PAUNITCOST,0),
                               @PAOverhead_Amount = isnull(PAOvhdAmtPerUnit,0),
                               @PABaseOvhdCost = isnull(PAOvhdAmtPerUnit,0),
                               @PAOverheaPercentage = isnull(PAOverheaPercentage,0),
                               @PABaseOvhdCost = isnull(PAOvhdAmtPerUnit,0),
                               @PAOverheaPercentage = isnull(PAOverheaPercentage,0),
                               @SUTASTAT = isnull(SUTASTAT,''),
                               @WRKRCOMP = isnull(WRKRCOMP,'')
                        from   PA01403 (nolock)
                        where  PARate_Table_ID = @PALabor_Rate_Table_ID
                               and EMPLOYID = @I_vEMPLOYID
                               and PAPayCode = @I_vPAPayCode
                        
                        if (@l_PALabor_Rate_Table_ID = '')
                          begin
                            select @O_iErrorState = 1246
                            
                            exec @iStatus = taUpdateString
                              @O_iErrorState ,
                              @oErrString ,
                              @oErrString output ,
                              @O_oErrorState output
                            
                            return (@O_iErrorState)
                          end
                      end
                    else
                      if (@PALabor_RateTable_Type = 2)
                        begin
                          select @l_PALabor_Rate_Table_ID = isnull(PARate_Table_ID,''),
                                 @SUTASTAT = isnull(SUTASTAT,''),
                                 @WRKRCOMP = isnull(WRKRCOMP,'')
                          from   PA01405 (nolock)
                          where  PARate_Table_ID = @PALabor_Rate_Table_ID
                                 and PAJob_Title = @I_vPAJob_Title
                                 and PAPayCode = @I_vPAPayCode
                          
                          if (@l_PALabor_Rate_Table_ID = '')
                            begin
                              select @O_iErrorState = 1331
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                        end
                  end
            end
          
          if ((@I_vPAUNITCOST <> 0)
              and (@I_vPAUNITCOST <> @PAUNITCOST))
            begin
              if ((@PAYTYPE = 2)
                   or (@PAYTYPE = 4)
                   or (@PAYTYPE = 5)
                   or (@PAYTYPE = 11)
                   or (@PAYTYPE = 12)
                   or (@PAYTYPE = 13)
                   or (@PAYTYPE = 16)
                   or (@PAYTYPE = 14)
                   or (@PAYTYPE = 15))
                begin
                  select @uneditable = 1
                end
              else
                if ((@PAYTYPE = 6)
                     or (@PAYTYPE = 8)
                     or (@PAYTYPE = 9)
                     or (@PAYTYPE = 10)
                     or (@PAYTYPE = 7))
                  begin
                    select @l_PAYTYPE = isnull(PAYTYPE,0)
                    from   UPR00400 (nolock)
                    where  EMPLOYID = @I_vEMPLOYID
                           and PAYRCORD = @BSPAYRCD
                    
                    if (@l_PAYTYPE = 2)
                      select @uneditable = 1
                  end
            end
          
          if (@uneditable = 1)
             and (@I_vCURNCYID = @FUNLCURR)
            begin
              select @O_iErrorState = 1300
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          if (@I_vPAUNITCOST = 0)
            select @I_vPAUNITCOST = @PAUNITCOST
          
          if (@UOMSCHDL = '')
            begin
              select @PABase_Unit_Cost = @I_vPAUNITCOST,
                     @PABaseOvhdCost = @PAOverhead_Amount
            end
          else
            begin
              if (@I_vPAUnit_of_Measure <> '')
                begin
                  select @BASEUOFM = BASEUOFM
                  from   PA40101 (nolock)
                  where  UOMSCHDL = @UOMSCHDL
                  
                  select @l_UOMSCHDL = isnull(UOMSCHDL,''),
                         @EQUOMQTY = isnull(EQUOMQTY,0)
                  from   PA40102 (nolock)
                  where  UOMSCHDL = @UOMSCHDL
                         and UOFM = @I_vPAUnit_of_Measure
                         and EQUIVUOM = @BASEUOFM
                  
                  if (@l_UOMSCHDL = '')
                    begin
                      select @I_vPAUnit_of_Measure = @l_PAUnit_of_Measure
                      
                      select @EQUOMQTY = isnull(EQUOMQTY,0)
                      from   PA40102 (nolock)
                      where  UOMSCHDL = @UOMSCHDL
                             and UOFM = @I_vPAUnit_of_Measure
                             and EQUIVUOM = @BASEUOFM
                    end
                  
                  if (@EQUOMQTY <> 0)
                    select @PABase_Unit_Cost = @I_vPAUNITCOST / @EQUOMQTY,
                           @PABaseOvhdCost = @PAOverhead_Amount / @EQUOMQTY
                end
            end
          
          if (@I_vPAUNITCOST <> 0)
             and (@PABase_Unit_Cost = 0)
            select @PABase_Unit_Cost = @I_vPAUNITCOST
          
          select @I_vPAUNITCOST = round(@I_vPAUNITCOST,@UnitCurrPlace),
                 @PABase_Unit_Cost = round(@PABase_Unit_Cost,@UnitCurrPlace),
                 @PAOverhead_Amount = round(@PAOverhead_Amount,@UnitCurrPlace),
                 @PABaseOvhdCost = round(@PABaseOvhdCost,@UnitCurrPlace),
                 @ORUNTCST = round(@I_vPAUNITCOST,@UnitCurrPlace),
                 @PAORGBSUNITCST = round(@PABase_Unit_Cost,@UnitCurrPlace),
                 @PAORIGOVHDAMT = round(@PAOverhead_Amount,@UnitCurrPlace),
                 @PAORIGBASEOVRHCST = round(@PABaseOvhdCost,@UnitCurrPlace)
          
          if ((@SUTASTAT = '')
               or (@WRKRCOMP = ''))
            begin
              if (@I_vPAPROJNUMBER <> '<NONE>')
                select @SUTASTAT = isnull(SUTASTAT,''),
                       @WRKRCOMP = isnull(WRKRCOMP,'')
                from   PA01201 (nolock)
                where  PAPROJNUMBER = @I_vPAPROJNUMBER
              
              if (@SUTASTAT = '')
                  or (@WRKRCOMP = '')
                select @SUTASTAT = isnull(SUTASTAT,''),
                       @WRKRCOMP = isnull(WRKRCOMP,'')
                from   UPR00400 (nolock)
                where  EMPLOYID = @I_vEMPLOYID
                       and PAYRCORD = @I_vPAPayCode
            end
          
          select @l_EMPLOYID = isnull(EMPLOYID,''),
                 @LOCALTAX = isnull(LOCALTAX,''),
                 @STATECD = isnull(STATECD,'')
          from   UPR00300 (nolock)
          where  EMPLOYID = @I_vEMPLOYID
          
          if (@l_EMPLOYID <> '')
            begin
              select @LOCLCODE = @LOCALTAX
              
              select @l_EMPLOYID = isnull(EMPLOYID,''),
                     @INACTIVE = isnull(INACTIVE,0)
              from   UPR00800 (nolock)
              where  EMPLOYID = @I_vEMPLOYID
                     and LOCALTAX = @LOCLCODE
              
              if ((@l_EMPLOYID = '')
                   or (@INACTIVE = 1))
                begin
                  select @LOCLCODE = ''
                end
              
              select @l_EMPLOYID = isnull(EMPLOYID,''),
                     @INACTIVE = isnull(INACTIVE,0)
              from   UPR00700 (nolock)
              where  EMPLOYID = @I_vEMPLOYID
                     and STATECD = @STATECD
              
              if ((@l_EMPLOYID = '')
                   or (@INACTIVE = 1))
                select @STATECD = ''
            end
          
          if (@IsMC = 1)
            begin
              select @PAUnit_of_Measure = isnull(PAUnit_of_Measure,''),
                     @PAProfitType = isnull(PAProfitType,0),
                     @PAFProfitAmt = isnull(PAFProfitAmt,0),
                     @PAFProfitPcnt = isnull(PAFProfitPcnt,0),
                     @PAMCFProfitAmt = isnull(PAMCFProfitAmt,0),
                     @PAForecastBaseProfitAmt = isnull(PAForecastBaseProfitAmt,0)
              from   PA01301 (nolock)
              where  PAPROJNUMBER = @I_vPAPROJNUMBER
                     and PACOSTCATID = @I_vPACOSTCATID
                     and PATU = @PATU
              
              if (@PAProfitType = 1)
                begin
                  if ((@PAUnit_of_Measure <> '')
                      and (@PAtsunitcostfrom = 2))
                    begin
                      if ((@PAUnit_of_Measure) <> (@I_vPAUnit_of_Measure))
                        begin
                          select @EQUOMQTY = isnull(PA40102.EQUOMQTY,0)
                          from   PA40101
                                 inner join PA40102
                                   on PA40101.UOMSCHDL = PA40102.UOMSCHDL
                          where  PA40102.UOFM = @PAUnit_of_Measure
                          
                          if (@EQUOMQTY <> 0)
                            begin
                              select @PA_MC_Base_Billing_Rate = @PAMCFProfitAmt / @EQUOMQTY,
                                     @PA_MC_Billing_Rate = @PA_MC_Base_Billing_Rate,
                                     @PA_Base_Billing_Rate = @PAFProfitAmt / @EQUOMQTY,
                                     @PABILRATE = @PA_Base_Billing_Rate
                            end
                          else
                            begin
                              select @PA_MC_Base_Billing_Rate = @PAMCFProfitAmt,
                                     @PA_MC_Billing_Rate = @PAMCFProfitAmt,
                                     @PA_Base_Billing_Rate = @PAForecastBaseProfitAmt,
                                     @PABILRATE = @PAFProfitAmt
                            end
                        end
                      else
                        begin
                          select @PA_MC_Base_Billing_Rate = @PAMCFProfitAmt,
                                 @PA_MC_Billing_Rate = @PAMCFProfitAmt,
                                 @PA_Base_Billing_Rate = @PAForecastBaseProfitAmt,
                                 @PABILRATE = @PAFProfitAmt
                        end
                    end
                  else
                    begin
                      select @PA_MC_Base_Billing_Rate = @PAMCFProfitAmt,
                             @PA_MC_Billing_Rate = @PAMCFProfitAmt,
                             @PA_Base_Billing_Rate = @PAForecastBaseProfitAmt,
                             @PABILRATE = @PAFProfitAmt
                    end
                end
              else
                if (@PAProfitType = 8)
                  begin
                    select @PA_MC_Base_Billing_Rate = 0,
                           @PA_MC_Billing_Rate = 0,
                           @PA_Base_Billing_Rate = 0,
                           @PABILRATE = 0
                  end
              
              select @PAORIGBILLRATE = round(@PABILRATE,@UnitCurrPlace),
                     @PAORIGBSBILLRTE = round(@PA_Base_Billing_Rate,@UnitCurrPlace)
            end
          else
            begin
              if ((@I_vPAbllngtype = 1)
                   or (@PAAcctgMethod <> 2))
                begin
                  select @PALabor_Rate_Table_ID = isnull(PALabor_Rate_Table_ID,''),
                         @PALabor_RateTable_Type = isnull(PALabor_RateTable_Type,0)
                  from   PA01301 (nolock)
                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                         and PACOSTCATID = @I_vPACOSTCATID
                         and PATU = @PATU
                  
                  if (@PALabor_Rate_Table_ID = '')
                    begin
                      select @PALabor_Rate_Table_ID = isnull(PALabor_Rate_Table_ID,''),
                             @PALabor_RateTable_Type = isnull(PALabor_RateTable_Type,0)
                      from   PA01201 (nolock)
                      where  PAPROJNUMBER = @I_vPAPROJNUMBER
                    end
                  
                  if (@PALabor_Rate_Table_ID <> '')
                    begin
                      if (@PALabor_RateTable_Type = 1)
                        begin
                          select @l_PALabor_Rate_Table_ID = isnull(PARate_Table_ID,''),
                                 @PAProfitType = isnull(PAProfitType,0),
                                 @l_PAProfitAmount = isnull(PAProfitAmount,0),
                                 @l_PAProfitPercent = isnull(PAProfitPercent,0)
                          from   PA01403 (nolock)
                          where  PARate_Table_ID = @PALabor_Rate_Table_ID
                                 and EMPLOYID = @I_vEMPLOYID
                                 and PAPayCode = @I_vPAPayCode
                          
                          if (@l_PALabor_Rate_Table_ID = '')
                            begin
                              select @O_iErrorState = 1247
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                        end
                      else
                        if (@PALabor_RateTable_Type = 2)
                          begin
                            if (@PAUsePayCodes = 0)
                              begin
                                select @O_iErrorState = 1248
                                
                                exec @iStatus = taUpdateString
                                  @O_iErrorState ,
                                  @oErrString ,
                                  @oErrString output ,
                                  @O_oErrorState output
                                
                                return (@O_iErrorState)
                              end
                            
                            select @l_PALabor_Rate_Table_ID = isnull(PARate_Table_ID,''),
                                   @PAProfitType = isnull(PAProfitType,0),
                                   @l_PAProfitAmount = isnull(PAProfitAmount,0),
                                   @l_PAProfitPercent = isnull(PAProfitPercent,0)
                            from   PA01405 (nolock)
                            where  PARate_Table_ID = @PALabor_Rate_Table_ID
                                   and PAJob_Title = @I_vPAJob_Title
                                   and PAPayCode = @I_vPAPayCode
                            
                            if (@l_PALabor_Rate_Table_ID = '')
                              begin
                                select @O_iErrorState = 1249
                                
                                exec @iStatus = taUpdateString
                                  @O_iErrorState ,
                                  @oErrString ,
                                  @oErrString output ,
                                  @O_oErrorState output
                                
                                return (@O_iErrorState)
                              end
                          end
                      
                      if ((@PAProjectType = 1)
                          and (@PAProfitType = 1))
                        select @PABILRATE = @l_PAProfitAmount,
                               @PA_Base_Billing_Rate = @l_PAProfitAmount
                      
                      if ((@PAProjectType = 1)
                          and (@PAProfitType = 2))
                        select @PAMARKPERCENT = @l_PAProfitPercent
                      
                      if ((@PAProjectType = 1)
                          and (@PAProfitType = 8))
                        select @PABILRATE = 0,
                               @PA_Base_Billing_Rate = 0
                    end
                  else
                    begin
                      if ((@PAtsprofittypefrom = 1)
                          and (@I_vPAPROJNUMBER <> '<NONE>'))
                        begin
                          select @l_PALabor_Rate_Table_ID = isnull(PALabor_Rate_Table_ID,''),
                                 @PAProfitType = isnull(PAProfitType,0),
                                 @PAFProfitAmt = isnull(PAFProfitAmt,0),
                                 @PAFProfitPcnt = isnull(PAFProfitPcnt,0)
                          from   PA01301 (nolock)
                          where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                 and PACOSTCATID = @I_vPACOSTCATID
                                 and PATU = @PATU
                          
                          if ((@PAProjectType = 1)
                              and (@PAProfitType = 1))
                            select @PABILRATE = @PAFProfitAmt,
                                   @PA_Base_Billing_Rate = @PAFProfitAmt
                          
                          if ((@PAProjectType = 1)
                              and (@PAProfitType = 2))
                            select @PAMARKPERCENT = @PAFProfitPcnt
                          
                          if ((@PAProjectType = 1)
                              and (@PAProfitType = 8))
                            select @PABILRATE = 0,
                                   @PA_Base_Billing_Rate = 0
                        end
                      else
                        if (@PAtsprofittypefrom = 2)
                          begin
                            select @PAProfitType = isnull(PATMProfitType,0),
                                   @PATMProfitAmount = isnull(PATMProfitAmount,0),
                                   @PATMProfitPercent = isnull(PATMProfitPercent,0),
                                   @PAProfit_Type__CP = isnull(PAProfit_Type__CP,0),
                                   @PAFFProfitType = isnull(PAFFProfitType,0)
                            from   PA00601 (nolock)
                            where  EMPLOYID = @I_vEMPLOYID
                            
                            if ((@PAProjectType = 1)
                                and (@PAProfitType = 1))
                              select @PABILRATE = @PATMProfitAmount,
                                     @PA_Base_Billing_Rate = @PATMProfitAmount
                            
                            if ((@PAProjectType = 1)
                                and (@PAProfitType = 2))
                              select @PAMARKPERCENT = @PATMProfitPercent
                            
                            if ((@PAProjectType = 1)
                                and (@PAProfitType = 8))
                              select @PABILRATE = 0,
                                     @PA_Base_Billing_Rate = 0
                            
                            if (@PAProjectType = 2)
                              select @PAProfitType = @PAProfit_Type__CP
                            
                            if (@PAProjectType = 3)
                              select @PAProfitType = @PAFFProfitType
                          end
                        else
                          begin
                            select @PAProfitType = isnull(PATMProfitType,0),
                                   @PATMProfitAmount = isnull(PATMProfitAmount,0),
                                   @PATMProfitPercent = isnull(PATMProfitPercent,0),
                                   @PAProfit_Type__CP = isnull(PAProfit_Type__CP,0),
                                   @PAFFProfitType = isnull(PAFFProfitType,0)
                            from   PA01001 (nolock)
                            where  PACOSTCATID = @I_vPACOSTCATID
                            
                            if ((@PAProjectType = 1)
                                and (@PAProfitType = 1))
                              select @PABILRATE = @PATMProfitAmount,
                                     @PA_Base_Billing_Rate = @PATMProfitAmount
                            
                            if ((@PAProjectType = 1)
                                and (@PAProfitType = 2))
                              select @PAMARKPERCENT = @PATMProfitPercent
                            
                            if ((@PAProjectType = 1)
                                and (@PAProfitType = 8))
                              select @PABILRATE = 0,
                                     @PA_Base_Billing_Rate = 0
                            
                            if (@PAProjectType = 2)
                              select @PAProfitType = @PAProfit_Type__CP
                            
                            if (@PAProjectType = 3)
                              select @PAProfitType = @PAFFProfitType
                          end
                    end
                  
                  if (@I_vPAPROJNUMBER = '<NONE>')
                    select @PAProfitAmount = 0,
                           @PABILRATE = 0,
                           @PA_Base_Billing_Rate = 0,
                           @PAMARKPERCENT = 0,
                           @PAProfitPercent = 0,
                           @PATotalProfit = 0,
                           @PAACREV = 0,
                           @PA_MC_Accrued_Revenue = 0
                  
                  if (@UOMSCHDL = '')
                    begin
                      select @PA_Base_Billing_Rate = @PABILRATE
                    end
                  else
                    begin
                      if (@I_vPAUnit_of_Measure <> '')
                        begin
                          select @BASEUOFM = BASEUOFM
                          from   PA40101 (nolock)
                          where  UOMSCHDL = @UOMSCHDL
                          
                          select @l_UOMSCHDL = isnull(UOMSCHDL,''),
                                 @EQUOMQTY = isnull(EQUOMQTY,0)
                          from   PA40102 (nolock)
                          where  UOMSCHDL = @UOMSCHDL
                                 and UOFM = @I_vPAUnit_of_Measure
                                 and EQUIVUOM = @BASEUOFM
                          
                          if (@l_UOMSCHDL = '')
                            begin
                              select @I_vPAUnit_of_Measure = @l_PAUnit_of_Measure
                              
                              select @EQUOMQTY = isnull(EQUOMQTY,0)
                              from   PA40102 (nolock)
                              where  UOMSCHDL = @UOMSCHDL
                                     and UOFM = @I_vPAUnit_of_Measure
                                     and EQUIVUOM = @BASEUOFM
                            end
                          
                          if (@EQUOMQTY <> 0)
                            select @PA_Base_Billing_Rate = @PABILRATE / @EQUOMQTY
                        end
                    end
                  
                  select @PABILRATE = round(@PABILRATE,@UnitCurrPlace),
                         @PA_Base_Billing_Rate = round(@PA_Base_Billing_Rate,@UnitCurrPlace),
                         @PAORIGBILLRATE = round(@PABILRATE,@UnitCurrPlace),
                         @PAORIGBSBILLRTE = round(@PA_Base_Billing_Rate,@UnitCurrPlace)
                end
              else
                begin
                  select @PABILRATE = 0,
                         @PA_Base_Billing_Rate = 0,
                         @PAMARKPERCENT = 0,
                         @PAProfitAmount = 0,
                         @PAProfitPercent = 0
                end
            end
        end
    end
  
  if (@I_vPAbllngtype = -1)
     and (@PAbllngtype > 0)
    select @I_vPAbllngtype = @PAbllngtype
  
  if (@I_vPAbllngtype = -1)
     and (@PAbllngtype = 0)
    select @I_vPAbllngtype = 1
  
  if ((@I_vPAbllngtype < 1)
       or (@I_vPAbllngtype > 3))
    begin
      select @O_iErrorState = 1263
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAPROJNUMBER = '<NONE>')
     and (@I_vPAbllngtype <> 3)
    begin
      select @O_iErrorState = 6454
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@PAProjectType = 2)
       or (@PAProjectType = 3))
     and (@I_vPAbllngtype <> 1)
    begin
      select @O_iErrorState = 6455
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@PAallow_3 = 0)
     and (@I_vPATB <> '')
    begin
      select @O_iErrorState = 6451
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@PAallow_3 = 0)
     and (@I_vPATE <> '')
    begin
      select @O_iErrorState = 6452
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@PAallow_4 = 0)
     and (@I_vPAQtyQ <> 0)
    begin
      select @O_iErrorState = 6483
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPATB <> ''
      and @I_vPATE <> ''
      and @I_vPAQtyQ <> 0)
    begin
      if round((DATEDIFF(minute,@I_vPATB,@I_vPATE) / 60.00),
               @DECPLQTY) <> @I_vPAQtyQ
        begin
          select @I_vPATB = '',
                 @I_vPATE = ''
        end
    end
  
  if ((@I_vPATB > @I_vPATE)
      and (@I_vPAQtyQ <> 0))
    begin
      select @O_iErrorState = 1255
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    if ((@I_vPATB <> '')
        and (@I_vPATE <> '')
        and (@I_vPATE > @I_vPATB))
      select @l_qty = round((DATEDIFF(minute,@I_vPATB,@I_vPATE) / 60.00),
                            @DECPLQTY)
    else
      if ((@I_vPATB = '')
          and (@I_vPATE <> '')
          and (@I_vPATE > @I_vPATB))
        select @l_qty = round((DATEDIFF(minute,@I_vPATB,@I_vPATE) / 60.00),
                              @DECPLQTY)
      else
        if ((@I_vPATB = '')
            and (@I_vPAQtyQ = 0))
          select @l_qty = 0
        else
          begin
            select @l_qty = @I_vPAQtyQ
          end
  
  if (@UOMSCHDL = '')
      or (@I_vPAUnit_of_Measure = '')
    begin
      select @l_base_qty = @l_qty
    end
  else
    begin
      select @l_UOMSCHDL = isnull(UOMSCHDL,''),
             @BASEUOFM = BASEUOFM
      from   PA40101 (nolock)
      where  UOMSCHDL = @UOMSCHDL
      
      if (@l_UOMSCHDL <> '')
        begin
          select @l_UOMSCHDL = isnull(UOMSCHDL,''),
                 @EQUOMQTY = isnull(EQUOMQTY,0)
          from   PA40102 (nolock)
          where  UOMSCHDL = @UOMSCHDL
                 and UOFM = @I_vPAUnit_of_Measure
                 and EQUIVUOM = @BASEUOFM
          
          if (@l_UOMSCHDL <> '')
            begin
              select @l_base_qty = @l_qty * @EQUOMQTY
            end
          else
            begin
              select @l_base_qty = @l_qty
            end
        end
      else
        begin
          select @l_base_qty = @l_qty
        end
    end
  
  select @l_base_qty = round(@l_base_qty,@DECPLQTY),
         @O_overhead = round((@PAORIGBASEOVRHCST + (@PAORGBSUNITCST * (@PAOverheaPercentage / 100))) * @l_base_qty,
                             @OCDECPLCUR),
         @O_extended = round((@PAORGBSUNITCST * @l_base_qty),@OCDECPLCUR),
         @O_cost = (@O_extended + @O_overhead),
         @l_overhead = round(@O_overhead,@UnitCurrPlace),
         @l_extended = round(@O_extended,@FCDECPLCUR),
         @l_cost = round(@O_cost,@FCDECPLCUR)
  
  if (@I_vPAbllngtype = 1)
    begin
      if (@PAProfitType = 1)
        select @O_accrued = round(@PAORIGBSBILLRTE * @l_base_qty,@OCDECPLCUR)
      
      if (@IsMC = 1)
        select @PA_MC_Accrued_Revenue = round(@PA_MC_Base_Billing_Rate * @l_base_qty,@OCDECPLCUR)
      
      if (@PAProfitType = 2)
        select @O_accrued = round(@O_extended * (1 + (@PAMARKPERCENT / 100)),
                                  @OCDECPLCUR)
      
      if (@PAProfitType = 8)
        select @O_accrued = 0
      
      select @l_accrued = round(@O_accrued,@UnitCurrPlace)
    end
  else
    begin
      select @l_accrued = 0,
             @O_accrued = 0
    end
  
  if (@I_vPAPROJNUMBER <> '<NONE>')
    begin
      select @l_UOMSCHDL = isnull(UOMSCHDL,''),
             @l_PAUnit_of_Measure = isnull(PAUnit_of_Measure,''),
             @PAPostedQty = isnull(PAPostedQty,0),
             @PAUnpostedQty = isnull(PAUnpostedQty,0),
             @PAFQuantity = isnull(PAFQuantity,0),
             @PAPostedTotalCostN = isnull(PAPostedTotalCostN,0),
             @PAUnpostedTotalCostN = isnull(PAUnpostedTotalCostN,0),
             @PAFTotalCost = isnull(PAFTotalCost,0),
             @PAPosted_Accr_RevN = isnull(PAPosted_Accr_RevN,0),
             @PABilled_Accrued_Revenu = isnull(PABilled_Accrued_Revenu,0),
             @PAUnpostAccrRevN = isnull(PAUnpostAccrRevN,0),
             @PAFBillings = isnull(PAFBillings,0)
      from   PA01301 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PACOSTCATID = @I_vPACOSTCATID
             and PATU = @PATU
      
      if (@l_UOMSCHDL <> '')
         AND (@I_vPAUnit_of_Measure <> '')
        begin
          select @l_UOMSCHDL = isnull(UOMSCHDL,''),
                 @BASEUOFM = BASEUOFM
          from   PA40101 (nolock)
          where  UOMSCHDL = @l_UOMSCHDL
          
          select @l_UOMSCHDL = isnull(UOMSCHDL,''),
                 @EQUOMQTY = isnull(EQUOMQTY,0)
          from   PA40102 (nolock)
          where  UOMSCHDL = @l_UOMSCHDL
                 and UOFM = @I_vPAUnit_of_Measure
                 and EQUIVUOM = @BASEUOFM
          
          if (@l_UOMSCHDL <> '')
            begin
              select @l_scroll_UofM_Qty = (@l_base_qty / @EQUOMQTY),
                     @l_line_UofM_Qty = (@l_base_qty / @EQUOMQTY)
            end
        end
      else
        begin
          select @l_scroll_UofM_Qty = (@l_base_qty),
                 @l_line_UofM_Qty = (@l_base_qty)
        end
      
      if (@I_vPAQtyQ < 0)
        select @l_oqty = @PAPostedQty + @l_scroll_UofM_Qty
      
      select @l_oqty = (@PAPostedQty + @PAUnpostedQty) + @I_vPAQtyQ
      
      if (@l_oqty < 0)
        begin
          select @O_iErrorState = 1256
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@PAFQuantity < @l_oqty)
        begin
          if (@PAallow_7 = 0)
            begin
              select @O_iErrorState = 1257
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      select @l_ocost = (@PAPostedTotalCostN + @PAUnpostedTotalCostN)
      
      if (@l_ocost < 0.0)
        begin
          select @O_iErrorState = 1258
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vPAbllngtype = 1)
        begin
          select @l_oaccrued = (@PAPosted_Accr_RevN + @PABilled_Accrued_Revenu + @l_accrued + @PAUnpostAccrRevN)
          
          if (@l_oaccrued < 0.0)
            begin
              select @O_iErrorState = 1260
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  select @I_vPAQtyQ = @l_qty,
         @PABase_Qty = @l_base_qty,
         @PATOTALOVERH = @l_overhead,
         @PAORIGTOTOVRHD = @O_overhead,
         @PAEXTCOST = @l_extended,
         @OREXTCST = @O_extended,
         @I_vPATOTCST = @l_cost,
         @PAORIGTOTCOST = @O_cost,
         @PAACREV = @l_accrued,
         @PAORIACCRREV = @O_accrued,
         @PASTD_Qty = case 
                        when (@I_vPAbllngtype = 1) then @l_base_qty
                        else 0
                      end,
         @PANC_Qty = case 
                       when (@I_vPAbllngtype = 2) then @l_base_qty
                       else 0
                     end,
         @PANB_Qty = case 
                       when (@I_vPAbllngtype = 3) then @l_base_qty
                       else 0
                     end
  
  if (@PAACREV <> 0)
    select @PATotalProfit = (@PAACREV - @I_vPATOTCST),
           @PAORIGTOTPROF = (@PAORIACCRREV - @PAORIGTOTCOST)
  
  if (@I_vPAPROJNUMBER <> '<NONE>')
    begin
      if ((@PAFTotalCost < (@l_ocost + @I_vPATOTCST)))
        begin
          if (@PAallow_8 = 0)
            begin
              select @O_iErrorState = 1259
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      if (@PAFBillings < (@l_oaccrued + @PAACREV))
        begin
          if (@PAallow_9 = 0)
            begin
              select @O_iErrorState = 1261
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if (@I_vPAQtyQ = 0)
    begin
      if (@PAallow_5 = 0)
        begin
          select @O_iErrorState = 1262
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAUNITCOST = 0)
    begin
      if (@PAallow_6 = 0)
        begin
          select @O_iErrorState = 6456
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if ((@I_vPASalary_Posting_Type < 0)
       or (@I_vPASalary_Posting_Type > 3))
    begin
      select @O_iErrorState = 1264
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPADepartment <> '')
    begin
      if not exists (select 1
                     from   UPR40300 (nolock)
                     where  DEPRTMNT = @I_vPADepartment)
        begin
          select @O_iErrorState = 1266
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAJob_Title <> '')
    begin
      if not exists (select 1
                     from   UPR40301 (nolock)
                     where  JOBTITLE = @I_vPAJob_Title)
        begin
          select @O_iErrorState = 1267
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vCURNCYID <> '')
    begin
      if not exists (select 1
                     from   DYNAMICS..MC40200 (nolock)
                     where  CURNCYID = @I_vCURNCYID)
        begin
          select @O_iErrorState = 1268
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 c (nolock)
  where  c.INTERID = db_name()
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_Timesheet' ,
        @I_vINDEX1 = @I_vPATSNO ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 0 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2043
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@add_employee_access = 1)
    begin
      insert into PA01408
                 (PAPROJNUMBER,
                  EMPLOYID,
                  PACONTNUMBER)
      select @I_vPAPROJNUMBER,
             @I_vEMPLOYID,
             @PACONTNUMBER
      
      if @@error <> 0
        begin
          select @O_iErrorState = 1269
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
    @I_sCompanyID = @sCompanyID ,
    @I_iSQLSessionID = 0 ,
    @I_noteincrement = 1 ,
    @O_mNoteIndex = @PAbillnoteidx output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iGetNextNoteIdxErrState <> 0)
       or (@iError <> 0))
    begin
      if @iGetNextNoteIdxErrState <> 0
        select @oErrString = rtrim(@oErrString) + ' ' + @iGetNextNoteIdxErrState
      
      select @O_iErrorState = 6486
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAtx500 <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidx,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAtx500
      
      if @@error <> 0
        begin
          select @O_iErrorState = 1324
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  insert into PA10001
             (PATSTYP,
              PATSNO,
              PAREFNO,
              EMPLOYID,
              PADT,
              PAPROJNUMBER,
              PACOSTCATID,
              PATB,
              PATE,
              PAQtyQ,
              PABase_Qty,
              PAUnit_of_Measure,
              UOMSCHDL,
              PAROUNDAMT,
              PAUNITCOST,
              ORUNTCST,
              PABase_Unit_Cost,
              PAORGBSUNITCST,
              PAEXTCOST,
              OREXTCST,
              PATOTCST,
              PAORIGTOTCOST,
              PAOverhead_Amount,
              PABaseOvhdCost,
              PAORIGOVHDAMT,
              PAORIGBASEOVRHCST,
              PAOverheaPercentage,
              PATOTALOVERH,
              PAORIGTOTOVRHD,
              PABILRATE,
              PAORIGBILLRATE,
              PA_Base_Billing_Rate,
              PAORIGBSBILLRTE,
              PAMARKPERCENT,
              PAACREV,
              PAORIACCRREV,
              PASTD_Qty,
              PANC_Qty,
              PANB_Qty,
              LNITMSEQ,
              PAReference_Line_Seq_N,
              PAbillnoteidx,
              PAbllngtype,
              PACONTNUMBER,
              PAPayCode,
              PASalary_Posting_Type,
              PAJob_Title,
              PADepartment,
              PAProfitType,
              PAProfitPercent,
              PAProfitAmount,
              PAORIGPROFAMT,
              PATotalProfit,
              PAORIGTOTPROF,
              STATECD,
              LOCLCODE,
              SUTASTAT,
              WRKRCOMP,
              PAexptdate,
              PAApproved_Quantity,
              PAAPPROVBILLRATE,
              PAAPPROVMKUPPCT,
              PAApproved_Billing_Amou,
              PABilledProfitPercentage,
              PA_Billed_Profit_Amount_,
              PABilled_Amount,
              PABilledTax,
              PABilled_Misc,
              PABilled_Freight,
              PABilled_Discount,
              PAWrite_UpDown_Amount,
              PAWrite_UpDown_Percenta,
              PAApprover_ID,
              PABilled_QtyN,
              PAApprover_Type,
              PAPartial_Bill,
              PABilling_StatusN,
              PACGBWIPIDX,
              PAUnbilled_AR_Idx,
              PACogs_Idx,
              PAContra_Account_IDX,
              PAOverhead_IDX,
              PAUnbilled_Proj_Rev_Idx,
              RNDDIFF,
              PACHGORDNO,
              PATSLINEERR2,
              PATS_Line_Errors,
              PAMCCURNCYID,
              CURRNIDX,
              RATETPID,
              EXGTBLID,
              XCHGRATE,
              EXCHDATE,
              TIME1,
              RATECALC,
              DENXRATE,
              MCTRXSTT,
              PA_MC_Accrued_Revenue,
              PA_MC_Base_Billing_Rate,
              PA_MC_Billing_Rate,
              Correcting_Trx_Type,
              PAORIGINLDOCNUM,
              PAORIGLINEIMSEQ)
  select @I_vPATSTYP,
         @I_vPATSNO,
         @I_vPAREFNO,
         @I_vEMPLOYID,
         @I_vPADT,
         @I_vPAPROJNUMBER,
         @I_vPACOSTCATID,
         @I_vPATB,
         @I_vPATE,
         @I_vPAQtyQ,
         @PABase_Qty,
         @I_vPAUnit_of_Measure,
         @UOMSCHDL,
         @PAROUNDAMT,
         @I_vPAUNITCOST,
         @ORUNTCST,
         @PABase_Unit_Cost,
         @PAORGBSUNITCST,
         @PAEXTCOST,
         @OREXTCST,
         @I_vPATOTCST,
         @PAORIGTOTCOST,
         @PAOverhead_Amount,
         @PABaseOvhdCost,
         @PAORIGOVHDAMT,
         @PAORIGBASEOVRHCST,
         @PAOverheaPercentage,
         @PATOTALOVERH,
         @PAORIGTOTOVRHD,
         @PABILRATE,
         @PAORIGBILLRATE,
         @PA_Base_Billing_Rate,
         @PAORIGBSBILLRTE,
         @PAMARKPERCENT,
         @PAACREV,
         @PAORIACCRREV,
         @PASTD_Qty,
         @PANC_Qty,
         @PANB_Qty,
         @I_vLNITMSEQ,
         @PAReference_Line_Seq_N,
         @PAbillnoteidx,
         @I_vPAbllngtype,
         @PACONTNUMBER,
         @I_vPAPayCode,
         @I_vPASalary_Posting_Type,
         @I_vPAJob_Title,
         @I_vPADepartment,
         @PAProfitType,
         @PAProfitPercent,
         @PAProfitAmount,
         @PAORIGPROFAMT,
         @PATotalProfit,
         @PAORIGTOTPROF,
         @STATECD,
         @LOCLCODE,
         @SUTASTAT,
         @WRKRCOMP,
         @PAexptdate,
         @PAApproved_Quantity,
         @PAAPPROVBILLRATE,
         @PAAPPROVMKUPPCT,
         @PAApproved_Billing_Amou,
         @PABilledProfitPercentage,
         @PA_Billed_Profit_Amount_,
         @PABilled_Amount,
         @PABilledTax,
         @PABilled_Misc,
         @PABilled_Freight,
         @PABilled_Discount,
         @PAWrite_UpDown_Amount,
         @PAWrite_UpDown_Percenta,
         @PAApprover_ID,
         @PABilled_QtyN,
         @PAApprover_Type,
         @PAPartial_Bill,
         @PABilling_StatusN,
         @PACGBWIPIDX,
         @PAUnbilled_AR_Idx,
         @PACogs_Idx,
         @PAContra_Account_IDX,
         @PAOverhead_IDX,
         @PAUnbilled_Proj_Rev_Idx,
         @RNDDIFF,
         @PACHGORDNO,
         0,
         0,
         @PAMCCURNCYID,
         @CURRNIDX,
         @RATETPID,
         @EXGTBLID,
         @XCHGRATE,
         @EXCHDATE,
         @TIME1,
         @RATECALC,
         @DENXRATE,
         @MCTRXSTT,
         @PA_MC_Accrued_Revenue,
         @PA_MC_Base_Billing_Rate,
         @PA_MC_Billing_Rate,
         0,
         '',
         0
  
  if @@error <> 0
    begin
      select @O_iErrorState = 1270
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPATimeSheetLineInsertPost
    @I_vPATSTYP ,
    @I_vPATSNO ,
    @I_vEMPLOYID ,
    @I_vCURNCYID ,
    @I_vPAREFNO ,
    @I_vLNITMSEQ ,
    @I_vPADT ,
    @I_vPAPROJNUMBER ,
    @I_vPACOSTCATID ,
    @I_vPATB ,
    @I_vPATE ,
    @I_vPAQtyQ ,
    @I_vPAbllngtype ,
    @I_vPAUnit_of_Measure ,
    @I_vPAUNITCOST ,
    @I_vPAPayCode ,
    @I_vPADepartment ,
    @I_vPAJob_Title ,
    @I_vPATOTCST ,
    @I_vPASalary_Posting_Type ,
    @I_vRequesterTrx ,
    @I_vPAtx500 ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1271
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_Timesheet' ,
        @I_vINDEX1 = @I_vPATSNO ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 1 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2044
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  return (@O_iErrorState)

