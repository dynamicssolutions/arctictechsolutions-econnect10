

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAChangeOrderEntryFeeSchedule]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAChangeOrderEntryFeeSchedule]
  
create procedure [dbo].[taPAChangeOrderEntryFeeSchedule]
                @I_vPAFeeID          char(15),
                @I_vPACHGORDNO       char(17),
                @I_vPACONTNUMBER     CHAR(11),
                @I_vPAPROJNUMBER     char(15),
                @I_vPAsequencenumber smallint,
                @I_vLNITMSEQ         int,
                @I_vPADT             datetime  = '',
                @I_vPAFeeAmount      numeric(19,5)  = 0,
                @I_vPAGBTRKCHG       smallint,
                @I_vRequesterTrx     smallint  = 0,
                @I_vUSRDEFND1        char(50)  = '',
                @I_vUSRDEFND2        char(50)  = '',
                @I_vUSRDEFND3        char(50)  = '',
                @I_vUSRDEFND4        varchar(8000)  = '',
                @I_vUSRDEFND5        varchar(8000)  = '',
                @O_iErrorState       int  output,
                @oErrString          varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @PAAmount_Billed                  smallint,
           @userdate                         datetime,
           @O_iglCalculateGLPeriodErrorState int,
           @PAsfid                           int,
           @PeriodID                         int,
           @Closed                           int,
           @Year                             int,
           @iCustomGLPeriodErr               int,
           @iStatus                          int,
           @PAPROJNUMBER                     char(15),
           @O_oErrorState                    int,
           @iError                           int,
           @PASTAT                           smallint,
           @PAProjectType                    smallint,
           @PATRKCHGORDS                     smallint,
           @PAbegindate                      datetime,
           @PAFeeType                        smallint,
           @PAAcctgMethod                    smallint,
           @PACONTNUMBER                     char(11),
           @PAFeeID                          char(15),
           @PAFeeToUse                       smallint,
           @PAFrequency                      int,
           @PAFeeAmount                      numeric(19,5),
           @l_Period                         int,
           @PAEnDate                         datetime,
           @sum_FeeAmount                    numeric(19,5),
           @fClosed                          smallint,
           @YEAR1                            int,
           @nErr                             int,
           @iCustomState                     int
  
  select @PAAmount_Billed = 0,
         @O_iErrorState = 0,
         @O_iglCalculateGLPeriodErrorState = 0,
         @iCustomGLPeriodErr = 0,
         @O_oErrorState = 0,
         @iError = 0,
         @iCustomState = 0
  
  exec @iStatus = taPAChangeOrderEntryFeeSchedulePre
    @I_vPAFeeID output ,
    @I_vPACHGORDNO output ,
    @I_vPACONTNUMBER output ,
    @I_vPAPROJNUMBER output ,
    @I_vPAsequencenumber output ,
    @I_vLNITMSEQ output ,
    @I_vPADT output ,
    @I_vPAFeeAmount output ,
    @I_vPAGBTRKCHG output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@oErrString))
      
      select @O_iErrorState = 4140
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAFeeID is null 
       or @I_vPACHGORDNO is null
       or @I_vPACONTNUMBER is null
       or @I_vPAPROJNUMBER is null
       or @I_vPAsequencenumber is null
       or @I_vLNITMSEQ is null
       or @I_vPADT is null
       or @I_vPAFeeAmount is null
       or @I_vPAGBTRKCHG is null)
    begin
      select @O_iErrorState = 4141
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPACHGORDNO = ''
       or @I_vPACONTNUMBER = ''
       or @I_vPAPROJNUMBER = '')
    begin
      select @O_iErrorState = 4142
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vPACHGORDNO = upper(@I_vPACHGORDNO),
         @I_vPACONTNUMBER = upper(@I_vPACONTNUMBER),
         @I_vPAPROJNUMBER = upper(@I_vPAPROJNUMBER)
  
  if (@I_vPAPROJNUMBER <> '')
    begin
      select @PAPROJNUMBER = PAPROJNUMBER,
             @PAProjectType = PAProjectType,
             @PAAcctgMethod = PAAcctgMethod,
             @PACONTNUMBER = PACONTNUMBER
      from   PA01201 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@PAPROJNUMBER = '')
        begin
          select @O_iErrorState = 4143
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@PASTAT = 3)
           or (@PASTAT = 5))
        begin
          select @O_iErrorState = 4144
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @PAPROJNUMBER = PAPROJNUMBER,
             @PATRKCHGORDS = PATRKCHGORDS
      from   PA01241 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@PATRKCHGORDS = 0)
        begin
          select @O_iErrorState = 4145
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPAFeeID <> '')
    begin
      select @PAFeeID = PAFeeID,
             @PAFeeType = PAFeeType,
             @PAFeeToUse = PAFeeToUse,
             @PAFeeType = PAFeeType,
             @PAFrequency = PAFrequency,
             @PAFeeAmount = PAFeeAmount
      from   PA00401 (nolock)
      where  PAFeeID = @I_vPAFeeID
      
      if (@PAFeeID = '')
        begin
          select @O_iErrorState = 4146
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPAGBTRKCHG not in (1,2))
    begin
      select @O_iErrorState = 8690
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if exists (select 1
             from   PA12103 (nolock)
             where  PAPROJNUMBER = @I_vPAPROJNUMBER
                    and PACONTNUMBER = @I_vPACONTNUMBER
                    and PACHGORDNO = @I_vPACHGORDNO
                    and PAFeeID = @I_vPAFeeID)
    begin
      select @O_iErrorState = 4148
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if @I_vPADT <> ''
    begin
      select @userdate = getdate()
      
      execute @iStatus = glCalculateGLPeriod
        2 ,
        'General Entry' ,
        @I_vPADT ,
        @userdate ,
        @l_Period output ,
        @fClosed output ,
        @YEAR1 output ,
        @nErr output ,
        @O_iglCalculateGLPeriodErrorState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@O_iglCalculateGLPeriodErrorState <> 0)
           or (@iError <> 0))
        begin
          select @O_iErrorState = 4149
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAGBTRKCHG = 1)
    select @PAbegindate = PABBeginDate,
           @PAEnDate = PABEndDate
    from   PA01101 (nolock)
    where  PACONTNUMBER = @I_vPACONTNUMBER
  else
    begin
      select @PAbegindate = PAFBeginDate,
             @PAEnDate = PAFEndDate
      from   PA01101 (nolock)
      where  PACONTNUMBER = @I_vPACONTNUMBER
    end
  
  if (@PAFeeType = 4)
     and ((@I_vPADT < @PAbegindate)
           or (@I_vPADT > @PAEnDate))
    begin
      select @O_iErrorState = 4150
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAFeeAmount < 0)
    begin
      if (@PAFeeType = 1)
        begin
          if (@PAProjectType = 1)
            begin
              select @O_iErrorState = 4151
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      else
        begin
          select @O_iErrorState = 4152
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @sum_FeeAmount = sum(PAFeeAmount)
  from   PA12103 (nolock)
  where  PACONTNUMBER = @I_vPACONTNUMBER
         and PACHGORDNO = @I_vPACHGORDNO
         and PAPROJNUMBER = @I_vPAPROJNUMBER
         and PAFeeID = @I_vPAFeeID
  
  if ((@I_vPAFeeAmount + @sum_FeeAmount) > @PAFeeAmount)
    begin
      select @O_iErrorState = 4153
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  insert into PA12103
             (PACONTNUMBER,
              PACHGORDNO,
              PAPROJNUMBER,
              LNITMSEQ,
              PAsequencenumber,
              PAFeeID,
              PADT,
              PAPREVDATE,
              PAFeeAmount,
              PAPREVFEEAMT,
              PAAmount_Billed)
  select @I_vPACONTNUMBER,
         @I_vPACHGORDNO,
         @I_vPAPROJNUMBER,
         @I_vLNITMSEQ,
         @I_vPAsequencenumber,
         @I_vPAFeeID,
         @I_vPADT,
         '',
         @I_vPAFeeAmount,
         0,
         @PAAmount_Billed
  
  if @@error <> 0
    begin
      select @O_iErrorState = 4154
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  exec @iStatus = taPAChangeOrderEntryFeeSchedulePost
    @I_vPAFeeID ,
    @I_vPACHGORDNO ,
    @I_vPACONTNUMBER ,
    @I_vPAPROJNUMBER ,
    @I_vPAsequencenumber ,
    @I_vLNITMSEQ ,
    @I_vPADT ,
    @I_vPAFeeAmount ,
    @I_vPAGBTRKCHG ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@oErrString))
      
      select @O_iErrorState = 4155
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

