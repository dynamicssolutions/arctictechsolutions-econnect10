

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAMiscLogLineInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAMiscLogLineInsert]
  
create procedure [dbo].[taPAMiscLogLineInsert]
                @I_vPSMISCLTRXTYPE    smallint,
                @I_vPAMISCLDOCNO      char(17),
                @I_vPADOCDT           datetime,
                @I_vPSMISCID          char(15),
                @I_vPAREPD            int,
                @I_vPAREPDT           datetime,
                @I_vCURNCYID          char(15),
                @I_vPAREFNO           char(17)  = '',
                @I_vLNITMSEQ          integer  = 0,
                @I_vPADT              datetime,
                @I_vPAPROJNUMBER      char(15),
                @I_vPACOSTCATID       char(15),
                @I_vPAQtyQ            numeric(19,5)  = 0,
                @I_vPAbllngtype       smallint  = 1,
                @I_vPAUnit_of_Measure char(9)  = '',
                @I_vPAUNITCOST        numeric(19,5)  = 0,
                @I_vPATOTCST          numeric(19,5)  = 0,
                @I_vPAtx500           char(256)  = '',
                @I_vRequesterTrx      smallint  = 0,
                @I_vUSRDEFND1         char(50)  = '',
                @I_vUSRDEFND2         char(50)  = '',
                @I_vUSRDEFND3         char(50)  = '',
                @I_vUSRDEFND4         varchar(8000)  = '',
                @I_vUSRDEFND5         varchar(8000)  = '',
                @O_iErrorState        int  output,
                @oErrString           varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iStatus                  int,
           @iCustomState             int,
           @iCustomErrString         varchar(255),
           @iError                   int,
           @iCursorError             int,
           @O_oErrorState            int,
           @sCompanyID               smallint,
           @PAreportingperiods       smallint,
           @PAnumofreportingperiods  smallint,
           @PA1stdatereportperiod    datetime,
           @PAallow_1                int,
           @PAallow_2                int,
           @PAallow_3                int,
           @PAallow_4                int,
           @PAallow_5                int,
           @PAallow_6                int,
           @PAallow_7                int,
           @PAallow_8                int,
           @PAallow_9                int,
           @PAallow_10               int,
           @PABase_Qty               numeric(19,5),
           @UOMSCHDL                 char(11),
           @PAROUNDAMT               numeric(19,5),
           @ORUNTCST                 numeric(19,5),
           @PABase_Unit_Cost         numeric(19,5),
           @PAORGBSUNITCST           numeric(19,5),
           @PAEXTCOST                numeric(19,5),
           @OREXTCST                 numeric(19,5),
           @PAORIGTOTCOST            numeric(19,5),
           @PAOverhead_Amount        numeric(19,5),
           @PABaseOvhdCost           numeric(19,5),
           @PAORIGOVHDAMT            numeric(19,5),
           @PAORIGBASEOVRHCST        numeric(19,5),
           @PAOverheaPercentage      numeric(19,5),
           @PATOTALOVERH             numeric(19,5),
           @PAORIGTOTOVRHD           numeric(19,5),
           @PABILRATE                numeric(19,5),
           @PAORIGBILLRATE           numeric(19,5),
           @PA_Base_Billing_Rate     numeric(19,5),
           @PAORIGBSBILLRTE          numeric(19,5),
           @PAMARKPERCENT            numeric(19,5),
           @PAACREV                  numeric(19,5),
           @PAORIACCRREV             numeric(19,5),
           @PASTD_Qty                numeric(19,5),
           @PANC_Qty                 numeric(19,5),
           @PANB_Qty                 numeric(19,5),
           @PAReference_Line_Seq_N   int,
           @PAbillnoteidx            numeric(19,5),
           @PAbllngtype              smallint,
           @PACONTNUMBER             char(11),
           @PASalary_Posting_Type    smallint,
           @PAProfitType             smallint,
           @PAProfitPercent          numeric(19,5),
           @PAProfitAmount           numeric(19,5),
           @PAORIGPROFAMT            numeric(19,5),
           @PATotalProfit            numeric(19,5),
           @PAORIGTOTPROF            numeric(19,5),
           @STATECD                  char(3),
           @LOCLCODE                 char(7),
           @SUTASTAT                 char(3),
           @WRKRCOMP                 char(7),
           @PAexptdate               datetime,
           @PAApproved_Quantity      numeric(19,5),
           @PAAPPROVBILLRATE         numeric(19,5),
           @PAAPPROVMKUPPCT          numeric(19,5),
           @PAApproved_Billing_Amou  numeric(19,5),
           @PABilledProfitPercentage numeric(19,5),
           @PA_Billed_Profit_Amount_ numeric(19,5),
           @PABilled_Amount          numeric(19,5),
           @PABilledTax              numeric(19,5),
           @PABilled_Misc            numeric(19,5),
           @PABilled_Freight         numeric(19,5),
           @PABilled_Discount        numeric(19,5),
           @PAWrite_UpDown_Amount    numeric(19,5),
           @PAWrite_UpDown_Percenta  int,
           @PAApprover_ID            char(15),
           @PABilled_QtyN            numeric(19,5),
           @PAApprover_Type          smallint,
           @PAPartial_Bill           tinyint,
           @PABilling_StatusN        smallint,
           @PACGBWIPIDX              int,
           @PAUnbilled_AR_Idx        int,
           @PACogs_Idx               int,
           @PAContra_Account_IDX     int,
           @PAOverhead_IDX           int,
           @PAUnbilled_Proj_Rev_Idx  int,
           @RNDDIFF                  int,
           @PACHGORDNO               char(17),
           @PASTAT                   int,
           @PAcloseProjcosts         int,
           @CUSTNMBR                 char(17),
           @DEX_ROW_ID               int,
           @PAProjectType            int,
           @PAAcctgMethod            int,
           @add_employee_access      int,
           @PATU                     int,
           @PAinactive               int,
           @l_PAPROJNUMBER           char(15),
           @DECPLQTY                 int,
           @DECPLCUR                 int,
           @EQUIVUOM                 char(9),
           @BASEUOFM                 char(9),
           @PAUsePayCodes            int,
           @PAtsunitcostfrom         int,
           @PAEmployee_Type          int,
           @PAYRTDEC                 int,
           @PAPay_Code_Hourly        int,
           @PAPay_Code_Salary        int,
           @WKHRPRYR                 int,
           @PAYUNPER                 int,
           @PAYUNIT                  char(25),
           @PAYPRPRD                 numeric(19,5),
           @PAYPEROD                 int,
           @PAYRCORD                 char(7),
           @PAYTYPE                  int,
           @PAYRTAMT                 numeric(19,5),
           @l_pay_period             int,
           @l_pay_unit_period        int,
           @l_hours_worked           numeric(19,5),
           @PALabor_Rate_Table_ID    char(31),
           @PALabor_RateTable_Type   int,
           @l_PALabor_Rate_Table_ID  char(31),
           @l_PAUnit_of_Measure      char(9),
           @l_EMPLOYID               char(15),
           @INACTIVE                 int,
           @PATMProfitAmount         numeric(19,5),
           @PAFProfitAmt             numeric(19,5),
           @PAFProfitPcnt            numeric(19,5),
           @PATMProfitPercent        numeric(19,5),
           @PAProfit_Type__CP        int,
           @PAFFProfitType           int,
           @EQUOMQTY                 numeric(19,5),
           @LOCALTAX                 char(7),
           @l_UOMSCHDL               char(11),
           @l_qty                    numeric(19,5),
           @l_base_qty               numeric(19,5),
           @l_overhead               numeric(19,5),
           @l_extended               numeric(19,5),
           @l_accrued                numeric(19,5),
           @l_cost                   numeric(19,5),
           @l_ocost                  numeric(19,5),
           @l_scroll_UofM_Qty        numeric(19,5),
           @l_oqty                   numeric(19,5),
           @O_overhead               numeric(19,5),
           @O_extended               numeric(19,5),
           @O_accrued                numeric(19,5),
           @O_cost                   numeric(19,5),
           @PAPostedQty              numeric(19,5),
           @PAFQuantity              numeric(19,5),
           @l_line_UofM_Qty          numeric(19,5),
           @PAPostedTotalCostN       numeric(19,5),
           @PAFTotalCost             numeric(19,5),
           @l_oaccrued               numeric(19,5),
           @PAFBillings              numeric(19,5),
           @PAUnpostedQty            numeric(19,5),
           @PAUnpostedTotalCostN     numeric(19,5),
           @PAPosted_Accr_RevN       numeric(19,5),
           @PABilled_Accrued_Revenu  numeric(19,5),
           @PAUnpostAccrRevN         numeric(19,5),
           @FUNLCURR                 char(15),
           @FUNCRIDX                 int,
           @CURRNIDX                 int,
           @ODECPLCU                 int,
           @UnitCurrPlace            int,
           @l_PAProfitAmount         numeric(19,5),
           @l_PAProfitPercent        numeric(19,5),
           @iGetNextNoteIdxErrState  int,
           @PAUNITCOST               numeric(19,5),
           @uneditable               smallint,
           @BSPAYRCD                 char(7),
           @l_PAYTYPE                smallint,
           @l_PACOSTCATID            char(15),
           @PAMLunitcostfrom         int,
           @PAMLprofittypefrom       int,
           @l_PACONTNUMBER           char(11),
           @PAsetupkey               int,
           @l_PSMISCID               char(15),
           @PATMProfitType           int,
           @PAUnit_of_Measure        char(9),
           @PAMCFProfitAmt           numeric(19,5),
           @PAForecastBaseProfitAmt  numeric(19,5),
           @PAMCCURNCYID             char(15),
           @RATETPID                 char(15),
           @EXGTBLID                 char(15),
           @XCHGRATE                 numeric(19,5),
           @EXCHDATE                 datetime,
           @TIME1                    datetime,
           @RATECALC                 int,
           @DENXRATE                 numeric(19,5),
           @MCTRXSTT                 int,
           @PA_MC_Accrued_Revenue    numeric(19,5),
           @PA_MC_Base_Billing_Rate  numeric(19,5),
           @PA_MC_Billing_Rate       numeric(19,5),
           @IsMC                     bit,
           @PAMCCURRNIDX             char(15),
           @EMPTYDATE                datetime,
           @OCDECPLCUR               int,
           @FCDECPLCUR               int
  
  select @iStatus = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @iError = 0,
         @iCursorError = 0,
         @O_iErrorState = 0,
         @O_oErrorState = 0,
         @sCompanyID = 0,
         @PAreportingperiods = 0,
         @PAnumofreportingperiods = 0,
         @PA1stdatereportperiod = '',
         @PAallow_1 = 0,
         @PAallow_2 = 0,
         @PAallow_3 = 0,
         @PAallow_4 = 0,
         @PAallow_5 = 0,
         @PAallow_6 = 0,
         @PAallow_7 = 0,
         @PAallow_8 = 0,
         @PAallow_9 = 0,
         @PAallow_10 = 0,
         @PABase_Qty = 0,
         @UOMSCHDL = '',
         @PAROUNDAMT = 0,
         @ORUNTCST = 0,
         @PABase_Unit_Cost = 0,
         @PAORGBSUNITCST = 0,
         @PAEXTCOST = 0,
         @OREXTCST = 0,
         @PAORIGTOTCOST = 0,
         @PAOverhead_Amount = 0,
         @PABaseOvhdCost = 0,
         @PAORIGOVHDAMT = 0,
         @PAORIGBASEOVRHCST = 0,
         @PAOverheaPercentage = 0,
         @PATOTALOVERH = 0,
         @PAORIGTOTOVRHD = 0,
         @PABILRATE = 0,
         @PAORIGBILLRATE = 0,
         @PA_Base_Billing_Rate = 0,
         @PAORIGBSBILLRTE = 0,
         @PAMARKPERCENT = 0,
         @PAACREV = 0,
         @PAORIACCRREV = 0,
         @PASTD_Qty = 0,
         @PANC_Qty = 0,
         @PANB_Qty = 0,
         @PAReference_Line_Seq_N = 0,
         @PAbillnoteidx = 0,
         @PAbllngtype = 0,
         @PACONTNUMBER = '',
         @PASalary_Posting_Type = 0,
         @PAProfitType = 0,
         @PAProfitPercent = 0,
         @PAProfitAmount = 0,
         @PAORIGPROFAMT = 0,
         @PATotalProfit = 0,
         @PAORIGTOTPROF = 0,
         @STATECD = '',
         @LOCLCODE = '',
         @SUTASTAT = '',
         @WRKRCOMP = '',
         @PAApproved_Quantity = 0,
         @PAAPPROVBILLRATE = 0,
         @PAAPPROVMKUPPCT = 0,
         @PAApproved_Billing_Amou = 0,
         @PABilledProfitPercentage = 0,
         @PA_Billed_Profit_Amount_ = 0,
         @PABilled_Amount = 0,
         @PABilledTax = 0,
         @PABilled_Misc = 0,
         @PABilled_Freight = 0,
         @PABilled_Discount = 0,
         @PAWrite_UpDown_Amount = 0,
         @PAWrite_UpDown_Percenta = 0,
         @PAApprover_ID = '',
         @PABilled_QtyN = 0,
         @PAApprover_Type = 0,
         @PAPartial_Bill = 0,
         @PABilling_StatusN = 0,
         @PACGBWIPIDX = 0,
         @PAUnbilled_AR_Idx = 0,
         @PACogs_Idx = 0,
         @PAContra_Account_IDX = 0,
         @PAOverhead_IDX = 0,
         @PAUnbilled_Proj_Rev_Idx = 0,
         @RNDDIFF = 0,
         @PACHGORDNO = '',
         @PASTAT = 0,
         @PAcloseProjcosts = 0,
         @CUSTNMBR = '',
         @DEX_ROW_ID = 0,
         @add_employee_access = 0,
         @PAProjectType = 0,
         @PAAcctgMethod = 0,
         @PATU = 0,
         @PAinactive = 0,
         @l_PAPROJNUMBER = '',
         @DECPLQTY = 0,
         @DECPLCUR = 0,
         @EQUIVUOM = '',
         @BASEUOFM = '',
         @PAUsePayCodes = 0,
         @PAtsunitcostfrom = 0,
         @PAEmployee_Type = 0,
         @PAYRTDEC = 0,
         @PAPay_Code_Hourly = 0,
         @PAPay_Code_Salary = 0,
         @WKHRPRYR = 0,
         @PAYUNPER = 0,
         @PAYUNIT = '',
         @PAYPRPRD = 0,
         @PAYPEROD = 0,
         @PAYRCORD = '',
         @PAYTYPE = 0,
         @PAYRTAMT = 0,
         @l_pay_period = 0,
         @l_pay_unit_period = 0,
         @l_hours_worked = 0,
         @PALabor_Rate_Table_ID = '',
         @PALabor_RateTable_Type = 0,
         @l_PALabor_Rate_Table_ID = '',
         @l_PAUnit_of_Measure = '',
         @l_EMPLOYID = '',
         @INACTIVE = 0,
         @PATMProfitAmount = 0,
         @PAFProfitAmt = 0,
         @PAFProfitPcnt = 0,
         @PATMProfitPercent = 0,
         @PAProfit_Type__CP = 0,
         @PAFFProfitType = 0,
         @EQUOMQTY = 0,
         @LOCALTAX = '',
         @l_UOMSCHDL = '',
         @l_qty = 0,
         @l_base_qty = 0,
         @O_overhead = 0,
         @O_extended = 0,
         @O_cost = 0,
         @l_UOMSCHDL = '',
         @l_overhead = 0,
         @l_extended = 0,
         @l_accrued = 0,
         @l_cost = 0,
         @l_ocost = 0,
         @l_scroll_UofM_Qty = 0,
         @l_oqty = 0,
         @O_accrued = 0,
         @PAPostedQty = 0,
         @PAFQuantity = 0,
         @l_line_UofM_Qty = 0,
         @PAPostedTotalCostN = 0,
         @PAFTotalCost = 0,
         @l_oaccrued = 0,
         @PAFBillings = 0,
         @PAUnpostedQty = 0,
         @PAUnpostedTotalCostN = 0,
         @PAPosted_Accr_RevN = 0,
         @PABilled_Accrued_Revenu = 0,
         @PAUnpostAccrRevN = 0,
         @FUNLCURR = '',
         @CURRNIDX = 0,
         @ODECPLCU = 0,
         @UnitCurrPlace = 0,
         @PAexptdate = '',
         @l_PAProfitAmount = 0,
         @l_PAProfitPercent = 0,
         @FUNCRIDX = 0,
         @iGetNextNoteIdxErrState = 0,
         @PAUNITCOST = 0,
         @uneditable = 0,
         @BSPAYRCD = '',
         @l_PAYTYPE = 0,
         @l_PAPROJNUMBER = '',
         @l_PACOSTCATID = '',
         @PAMLunitcostfrom = 0,
         @PAMLprofittypefrom = 0,
         @l_PACONTNUMBER = '',
         @PAsetupkey = 0,
         @l_PSMISCID = '',
         @PATMProfitType = 0,
         @EMPTYDATE = convert(datetime,'1/1/1900'),
         @PAUnit_of_Measure = '',
         @PAMCFProfitAmt = 0,
         @PAForecastBaseProfitAmt = 0,
         @PAMCCURNCYID = '',
         @RATETPID = '',
         @EXGTBLID = '',
         @XCHGRATE = 0,
         @EXCHDATE = @EMPTYDATE,
         @TIME1 = @EMPTYDATE,
         @RATECALC = 0,
         @DENXRATE = 0,
         @MCTRXSTT = 0,
         @PA_MC_Accrued_Revenue = 0,
         @PA_MC_Base_Billing_Rate = 0,
         @PA_MC_Billing_Rate = 0,
         @IsMC = 0,
         @PAMCCURRNIDX = '',
         @OCDECPLCUR = 0,
         @FCDECPLCUR = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taPAMiscLogLineInsertPre
    @I_vPSMISCLTRXTYPE output ,
    @I_vPAMISCLDOCNO output ,
    @I_vPADOCDT output ,
    @I_vPSMISCID output ,
    @I_vPAREPD output ,
    @I_vPAREPDT output ,
    @I_vCURNCYID output ,
    @I_vPAREFNO output ,
    @I_vLNITMSEQ output ,
    @I_vPADT output ,
    @I_vPAPROJNUMBER output ,
    @I_vPACOSTCATID output ,
    @I_vPAQtyQ output ,
    @I_vPAbllngtype output ,
    @I_vPAUnit_of_Measure output ,
    @I_vPAUNITCOST output ,
    @I_vPATOTCST output ,
    @I_vPAtx500 output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1054
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPSMISCLTRXTYPE is NULL 
       or @I_vPAMISCLDOCNO is NULL
       or @I_vPADOCDT is NULL
       or @I_vPSMISCID is NULL
       or @I_vPAREPD is NULL
       or @I_vPAREPDT is NULL
       or @I_vCURNCYID is NULL
       or @I_vPADT is NULL
       or @I_vPAPROJNUMBER is NULL
       or @I_vPACOSTCATID is NULL)
    begin
      select @O_iErrorState = 1055
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPSMISCLTRXTYPE = 0
       or @I_vPAMISCLDOCNO = ''
       or @I_vPADOCDT = ''
       or @I_vPSMISCID = ''
       or @I_vPAREPD = ''
       or @I_vPAREPDT = ''
       or @I_vCURNCYID = ''
       or @I_vPADT = ''
       or @I_vPAPROJNUMBER = ''
       or @I_vPACOSTCATID = '')
    begin
      select @O_iErrorState = 1056
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAbllngtype < 0
       or @I_vPAUNITCOST < 0
       or @I_vPATOTCST < 0
       or @I_vPAREPD < 0)
    begin
      select @O_iErrorState = 1057
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vPSMISCID = UPPER(@I_vPSMISCID),
         @I_vCURNCYID = UPPER(@I_vCURNCYID),
         @I_vPAPROJNUMBER = UPPER(@I_vPAPROJNUMBER),
         @I_vPACOSTCATID = UPPER(@I_vPACOSTCATID)
  
  select @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 c (nolock)
  where  c.INTERID = db_name()
  
  exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
    @I_sCompanyID = @sCompanyID ,
    @I_iSQLSessionID = 0 ,
    @O_mNoteIndex = @PAbillnoteidx output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iGetNextNoteIdxErrState <> 0)
    begin
      if (@iGetNextNoteIdxErrState <> 0)
        begin
          exec @iStatus = taUpdateString
            @iGetNextNoteIdxErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 1058
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @PAsetupkey = isnull(PAsetupkey,0),
         @PAreportingperiods = isnull(PAreportingperiods,0),
         @PAMLunitcostfrom = isnull(PAMLunitcostfrom,0),
         @PAMLprofittypefrom = isnull(PAMLprofittypefrom,0),
         @PAnumofreportingperiods = isnull(PAnumofreportingperiods,0),
         @PA1stdatereportperiod = isnull(PA1stdatereportperiod,0),
         @PAallow_1 = isnull(PAallow_1,0),
         @PAallow_2 = isnull(PAallow_2,0),
         @PAallow_3 = isnull(PAallow_3,0),
         @PAallow_4 = isnull(PAallow_4,0),
         @PAallow_5 = isnull(PAallow_5,0),
         @PAallow_6 = isnull(PAallow_6,0),
         @PAallow_7 = isnull(PAallow_7,0),
         @PAallow_8 = isnull(PAallow_8,0),
         @PAallow_9 = isnull(PAallow_9,0),
         @PAallow_10 = isnull(PAallow_10,0)
  from   PA42001 (nolock)
  
  if (@PAsetupkey = 0)
    begin
      select @O_iErrorState = 1059
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPSMISCLTRXTYPE < 1)
       or (@I_vPSMISCLTRXTYPE > 2))
    begin
      select @O_iErrorState = 1060
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPSMISCLTRXTYPE = 1)
    select @I_vPAREFNO = ''
  
  if (@I_vLNITMSEQ = 0)
    begin
      select @I_vLNITMSEQ = isnull(max(LNITMSEQ),0)
      from   PA10201 a (nolock)
      where  a.PAMISCLDOCNO = @I_vPAMISCLDOCNO
    end
  
  if @I_vLNITMSEQ = 0
    select @I_vLNITMSEQ = 120
  else
    begin
      select @I_vLNITMSEQ = @I_vLNITMSEQ + 100
    end
  
  if exists (select 1
             from   PA10201 (nolock)
             where  PAMISCLDOCNO = @I_vPAMISCLDOCNO
                    and LNITMSEQ = @I_vLNITMSEQ)
    begin
      select @O_iErrorState = 1061
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @FUNLCURR = isnull(FUNLCURR,''),
         @FUNCRIDX = isnull(FUNCRIDX,0)
  from   MC40000 (nolock)
  
  select @CURRNIDX = isnull(CURRNIDX,0)
  from   DYNAMICS..MC40200 (nolock)
  WHERE  CURNCYID = @I_vCURNCYID
  
  select @FCDECPLCUR = DECPLCUR - 1
  from   MC40000 a (nolock),
         DYNAMICS..MC40200 b (nolock)
  where  a.FUNCRIDX = b.CURRNIDX
         and a.FUNLCURR = b.CURNCYID
  
  if ((@I_vPAPROJNUMBER <> '')
      and (@I_vPAPROJNUMBER <> '<NONE>'))
    begin
      select @l_PAPROJNUMBER = isnull(PAPROJNUMBER,''),
             @PASTAT = isnull(PASTAT,0),
             @PAcloseProjcosts = isnull(PAcloseProjcosts,0),
             @PACONTNUMBER = isnull(PACONTNUMBER,''),
             @PAProjectType = isnull(PAProjectType,0),
             @PAAcctgMethod = isnull(PAAcctgMethod,0),
             @CUSTNMBR = isnull(CUSTNMBR,'')
      from   PA01201 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@l_PAPROJNUMBER = '')
        begin
          select @O_iErrorState = 1062
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      else
        begin
          if ((@PASTAT = 2)
               or (@PASTAT = 3)
               or (@PASTAT = 4)
               or (@PAcloseProjcosts = 1))
            begin
              select @O_iErrorState = 1063
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
    end
  else
    begin
      select @PAProjectType = 1,
             @PABilling_StatusN = 4,
             @I_vPAPROJNUMBER = '<NONE>',
             @I_vPAbllngtype = 3
    end
  
  if (@I_vPSMISCLTRXTYPE = 2)
    begin
      if (@I_vPAREFNO = '')
        begin
          select @O_iErrorState = 1064
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      else
        begin
          if not exists (select 1
                         from   PA30301 (nolock)
                         where  PAMISCLDOCNO = @I_vPAREFNO)
            begin
              select @O_iErrorState = 1065
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
    end
  
  if (@I_vPSMISCID <> '')
    begin
      select @l_PSMISCID = isnull(PSMISCID,''),
             @PAinactive = isnull(PAinactive,0)
      from   PA00801 (nolock)
      where  PSMISCID = @I_vPSMISCID
      
      if (@l_PSMISCID = '')
        begin
          select @O_iErrorState = 1066
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@PAinactive = 1)
        begin
          select @O_iErrorState = 1067
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      select @O_iErrorState = 1068
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PACONTNUMBER <> '')
    begin
      select @l_PACONTNUMBER = isnull(PACONTNUMBER,''),
             @PASTAT = ISNULL(PASTAT,0),
             @PAcloseProjcosts = ISNULL(PAcloseProjcosts,0),
             @CUSTNMBR = ISNULL(CUSTNMBR,'')
      from   PA01101 (nolock)
      where  PACONTNUMBER = @PACONTNUMBER
      
      if (@l_PACONTNUMBER = '')
        begin
          select @O_iErrorState = 1069
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      else
        begin
          if ((@PASTAT = 2)
               or (@PASTAT = 3)
               or (@PASTAT = 4)
               or (@PAcloseProjcosts = 1))
            begin
              select @O_iErrorState = 1070
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          select @DEX_ROW_ID = DEX_ROW_ID,
                 @PAcloseProjcosts = ISNULL(PAcloseProjcosts,0)
          from   PA00501 (nolock)
          where  CUSTNMBR = @CUSTNMBR
          
          if (@DEX_ROW_ID IS NULL)
            begin
              select @O_iErrorState = 1071
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          else
            begin
              if (@PAcloseProjcosts = 1)
                begin
                  select @O_iErrorState = 1072
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
        end
    end
  
  if (@I_vPAPROJNUMBER = '<NONE>')
    begin
      select @PACONTNUMBER = '<NONE>',
             @DECPLCUR = @FCDECPLCUR
    end
  
  if (@PAMCCURNCYID <> '')
    select @PAMCCURRNIDX = isnull(CURRNIDX,0)
    from   DYNAMICS..MC40200 (nolock)
    where  CURNCYID = @PAMCCURNCYID
  
  if @FUNLCURR <> @PAMCCURNCYID
    begin
      select @IsMC = 1
      
      select @RATETPID = isnull(PAFRATETPID,''),
             @EXGTBLID = isnull(PAFEXGTBLID,''),
             @XCHGRATE = isnull(PAFXCHGRATE,0),
             @EXCHDATE = isnull(PAFEXCHDATE,@EMPTYDATE),
             @TIME1 = isnull(PAFTIME1,@EMPTYDATE),
             @RATECALC = isnull(PAFRTCLCMTD,0),
             @DENXRATE = isnull(PAFDENXRATE,0),
             @MCTRXSTT = isnull(PAFMCTRXSTT,0)
      from   PA01202 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
    end
  
  if (@I_vPACOSTCATID <> '')
    begin
      select @l_PACOSTCATID = isnull(PACOSTCATID,''),
             @PATU = isnull(PATU,0),
             @PAinactive = isnull(PAinactive,0)
      from   PA01001 (nolock)
      where  PACOSTCATID = @I_vPACOSTCATID
      
      if (@l_PACOSTCATID IS NULL)
        begin
          select @O_iErrorState = 1073
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      else
        begin
          if (@PATU <> 3)
            begin
              select @O_iErrorState = 1074
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if (@PAinactive = 1)
            begin
              select @O_iErrorState = 1075
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if ((@I_vPAPROJNUMBER = '<NONE>')
              and (@I_vPACOSTCATID = '<NONE>'))
            begin
              if ((@PATU = 1)
                   or (@PATU = 2)
                   or (@PATU = 3))
                begin
                  select @O_iErrorState = 1076
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
          
          select @DECPLCUR = (isnull(DECPLCUR,0) + 6)
          from   PA01301 (nolock)
          where  PAPROJNUMBER = @I_vPAPROJNUMBER
                 and PACOSTCATID = @I_vPACOSTCATID
                 and PATU = @PATU
          
          if (@I_vCURNCYID <> @FUNLCURR)
            select @ODECPLCU = case 
                                 when ((@CURRNIDX = 0)
                                        or (@CURRNIDX = 2999)) then (((@DECPLCUR - 6) - 1) + 7)
                                 else ((@CURRNIDX - 1000) * 6) + 2000 + ((@DECPLCUR - 6) - 1)
                               end
          
          if (@DECPLCUR >= 7)
            begin
              select @UnitCurrPlace = @DECPLCUR - 7
            end
          else
            begin
              select @UnitCurrPlace = @DECPLCUR
            end
          
          if (@I_vPAPROJNUMBER <> '<NONE>')
            begin
              if (@I_vPACOSTCATID <> '<NONE>')
                begin
                  select @l_PAPROJNUMBER = isnull(PAPROJNUMBER,''),
                         @PASTAT = isnull(PASTAT,0),
                         @DECPLQTY = isnull(DECPLQTY,0),
                         @DECPLCUR = (isnull(DECPLCUR,0) + 6),
                         @I_vPAUnit_of_Measure = isnull(PAUnit_of_Measure,''),
                         @UOMSCHDL = isnull(UOMSCHDL,''),
                         @PAbllngtype = isnull(PAbllngtype,0),
                         @PAUNITCOST = isnull(PAFUnitCost,0),
                         @PABase_Unit_Cost = isnull(PABase_Unit_Cost,0),
                         @I_vPAUnit_of_Measure = isnull(PAUnit_of_Measure,0),
                         @PAProfitType = isnull(PAProfitType,''),
                         @PAFProfitPcnt = isnull(PAFProfitPcnt,0),
                         @PAFProfitAmt = isnull(PAFProfitAmt,0),
                         @PAOverhead_Amount = isnull(PABaseOvhdCost,0),
                         @PABilling_StatusN = case 
                                                WHEN @PAProjectType = 1
                                                     and @I_vPAbllngtype <> 3 THEN 1
                                                WHEN @PAProjectType <> 1
                                                     and @I_vPAbllngtype <> 3 THEN 5
                                                ELSE 4
                                              END
                  from   PA01301 (nolock)
                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                         and PACOSTCATID = @I_vPACOSTCATID
                         and PATU = @PATU
                  
                  if (@l_PAPROJNUMBER = '')
                    begin
                      select @O_iErrorState = 1077
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                    end
                  else
                    begin
                      if (@PASTAT <> 1)
                        begin
                          select @O_iErrorState = 1078
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                        end
                    end
                end
            end
          
          if (@I_vPSMISCLTRXTYPE = 2)
            begin
              if (@PAReference_Line_Seq_N = '')
                begin
                  if not exists (select 1
                                 from   PA30301 (nolock)
                                 where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                        and PACOSTCATID = @I_vPACOSTCATID
                                        and PADT = @I_vPADT
                                        and PAREFNO = @I_vPAREFNO)
                    begin
                      select @O_iErrorState = 1079
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                    end
                end
              
              select @PAReference_Line_Seq_N = LNITMSEQ,
                     @I_vPADT = PADT,
                     @I_vPAPROJNUMBER = PAPROJNUMBER,
                     @I_vPACOSTCATID = PACOSTCATID,
                     @PACONTNUMBER = PACONTNUMBER,
                     @PA_Base_Billing_Rate = PA_Base_Billing_Rate,
                     @PAORIGBSBILLRTE = PAORIGBSBILLRTE,
                     @PABase_Unit_Cost = PABase_Unit_Cost,
                     @PAORGBSUNITCST = PAORGBSUNITCST,
                     @PABILRATE = PABILRATE,
                     @PAORIGBILLRATE = PAORIGBILLRATE,
                     @PAbllngtype = PAbllngtype,
                     @PAMARKPERCENT = PAMARKPERCENT,
                     @PAProfitType = PAProfitType,
                     @PAOverhead_Amount = PAOverhead_Amount,
                     @PAORIGOVHDAMT = PAORIGOVHDAMT,
                     @PABaseOvhdCost = PABaseOvhdCost,
                     @PAORIGBASEOVRHCST = PAORIGBASEOVRHCST,
                     @PAOverheaPercentage = PAOverheaPercentage,
                     @I_vPAUNITCOST = PAUNITCOST,
                     @ORUNTCST = ORUNTCST,
                     @I_vPAUnit_of_Measure = PAUnit_of_Measure,
                     @UOMSCHDL = UOMSCHDL,
                     @PACogs_Idx = PACogs_Idx,
                     @PAContra_Account_IDX = PAContra_Account_IDX,
                     @PAOverhead_IDX = PAOverhead_IDX,
                     @PAUnbilled_AR_Idx = PAUnbilled_AR_Idx,
                     @PAUnbilled_Proj_Rev_Idx = PAUnbilled_Proj_Rev_Idx,
                     @PACGBWIPIDX = PACGBWIPIDX,
                     @RNDDIFF = RNDDIFF,
                     @PAProfitAmount = PAProfitAmount,
                     @PAORIGPROFAMT = PAORIGPROFAMT,
                     @PAProfitPercent = PAProfitPercent,
                     @PABilling_StatusN = case 
                                            WHEN @PAProjectType = 1
                                                 and @PAbllngtype <> 3 THEN 1
                                            WHEN @PAProjectType <> 1
                                                 and @PAbllngtype <> 3 THEN 5
                                            ELSE 4
                                          END
              from   PA30301 (nolock)
              where  PAPROJNUMBER = @I_vPAPROJNUMBER
                     and PACOSTCATID = @I_vPACOSTCATID
                     and PADT = @I_vPADT
                     and PAREFNO = @I_vPAREFNO
              
              if (@I_vPAPROJNUMBER = '<NONE>')
                select @DECPLQTY = isnull(DECPLQTY,0),
                       @DECPLCUR = isnull(DECPLCUR,0)
                from   PA01001 (nolock)
                where  PACOSTCATID = @I_vPACOSTCATID
            end
          else
            begin
              if (@I_vPAPROJNUMBER = '<NONE>')
                begin
                  select @DECPLQTY = isnull(DECPLQTY,0),
                         @DECPLCUR = (isnull(DECPLCUR,0) + 6),
                         @I_vPAUnit_of_Measure = isnull(PAUnit_of_Measure,''),
                         @UOMSCHDL = isnull(UOMSCHDL,''),
                         @PAbllngtype = 3,
                         @PAOverhead_Amount = isnull(PAOvhdAmtPerUnit,0),
                         @PAOverheaPercentage = isnull(PAOverheaPercentage,0),
                         @PABilling_StatusN = 4
                  from   PA01001 (nolock)
                  where  PACOSTCATID = @I_vPACOSTCATID
                end
              
              if ((@I_vPAUnit_of_Measure <> '')
                  and (@UOMSCHDL <> ''))
                begin
                  select @BASEUOFM = BASEUOFM
                  from   PA40101 (nolock)
                  where  UOMSCHDL = @UOMSCHDL
                  
                  if not exists (select 1
                                 from   PA40102 (nolock)
                                 where  UOMSCHDL = @UOMSCHDL
                                        and UOFM = @I_vPAUnit_of_Measure
                                        and EQUIVUOM = @BASEUOFM)
                    begin
                      select @O_iErrorState = 1080
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                    end
                end
              
              select @PACogs_Idx = 0,
                     @PAContra_Account_IDX = 0,
                     @PAOverhead_IDX = 0,
                     @PAUnbilled_AR_Idx = 0,
                     @PAUnbilled_Proj_Rev_Idx = 0,
                     @PACGBWIPIDX = 0,
                     @RNDDIFF = 0
              
              if (@PAProjectType = 1)
                begin
                  if (@I_vPAbllngtype = 1)
                    begin
                      if (@PAAcctgMethod <> 1)
                        begin
                          exec @iStatus = taPAAuxAcctsGetIdx
                            @I_vPAPROJNUMBER ,
                            @I_vPACOSTCATID ,
                            @I_vPSMISCID ,
                            3 ,
                            'ML' ,
                            1 ,
                            @CUSTNMBR ,
                            @PACONTNUMBER ,
                            @PACGBWIPIDX output ,
                            @O_oErrorState output
                          
                          select @iError = @@error
                          
                          if @iStatus = 0
                             and @iError <> 0
                            select @iStatus = @iError
                          
                          if (@iStatus <> 0)
                              or (@O_oErrorState <> 0)
                            begin
                              select @O_iErrorState = 1081
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                        end
                      else
                        begin
                          exec @iStatus = taPAAuxAcctsGetIdx
                            @I_vPAPROJNUMBER ,
                            @I_vPACOSTCATID ,
                            @I_vPSMISCID ,
                            3 ,
                            'ML' ,
                            2 ,
                            @CUSTNMBR ,
                            @PACONTNUMBER ,
                            @PACogs_Idx output ,
                            @O_oErrorState output
                          
                          select @iError = @@error
                          
                          if @iStatus = 0
                             and @iError <> 0
                            select @iStatus = @iError
                          
                          if (@iStatus <> 0)
                              or (@O_oErrorState <> 0)
                            begin
                              select @O_iErrorState = 1082
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                        end
                    end
                  else
                    begin
                      exec @iStatus = taPAAuxAcctsGetIdx
                        @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID ,
                        @I_vPSMISCID ,
                        3 ,
                        'ML' ,
                        2 ,
                        @CUSTNMBR ,
                        @PACONTNUMBER ,
                        @PACogs_Idx output ,
                        @O_oErrorState output
                      
                      select @iError = @@error
                      
                      if @iStatus = 0
                         and @iError <> 0
                        select @iStatus = @iError
                      
                      if (@iStatus <> 0)
                          or (@O_oErrorState <> 0)
                        begin
                          select @O_iErrorState = 1083
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                  
                  if ((@I_vPAPROJNUMBER <> '<NONE>')
                      and (@PAAcctgMethod <> 2))
                    begin
                      exec @iStatus = taPAAuxAcctsGetIdx
                        @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID ,
                        @I_vPSMISCID ,
                        3 ,
                        'ML' ,
                        4 ,
                        @CUSTNMBR ,
                        @PACONTNUMBER ,
                        @PAUnbilled_AR_Idx output ,
                        @O_oErrorState output
                      
                      select @iError = @@error
                      
                      if @iStatus = 0
                         and @iError <> 0
                        select @iStatus = @iError
                      
                      if (@iStatus <> 0)
                          or (@O_oErrorState <> 0)
                        begin
                          select @O_iErrorState = 1084
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (@PAUnbilled_AR_Idx <> 0)
                        begin
                          exec @iStatus = taPAAuxAcctsGetIdx
                            @I_vPAPROJNUMBER ,
                            @I_vPACOSTCATID ,
                            @I_vPSMISCID ,
                            3 ,
                            'ML' ,
                            5 ,
                            @CUSTNMBR ,
                            @PACONTNUMBER ,
                            @PAUnbilled_Proj_Rev_Idx output ,
                            @O_oErrorState output
                          
                          select @iError = @@error
                          
                          if @iStatus = 0
                             and @iError <> 0
                            select @iStatus = @iError
                          
                          if (@iStatus <> 0)
                              or (@O_oErrorState <> 0)
                            begin
                              select @O_iErrorState = 1085
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                          
                          if (@PAUnbilled_Proj_Rev_Idx = 0)
                            select @PAUnbilled_AR_Idx = 0
                        end
                    end
                  
                  exec @iStatus = taPAAuxAcctsGetIdx
                    @I_vPAPROJNUMBER ,
                    @I_vPACOSTCATID ,
                    @I_vPSMISCID ,
                    3 ,
                    'ML' ,
                    3 ,
                    @CUSTNMBR ,
                    @PACONTNUMBER ,
                    @PAContra_Account_IDX output ,
                    @O_oErrorState output
                  
                  select @iError = @@error
                  
                  if @iStatus = 0
                     and @iError <> 0
                    select @iStatus = @iError
                  
                  if (@iStatus <> 0)
                      or (@O_oErrorState <> 0)
                    begin
                      select @O_iErrorState = 1086
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  
                  if (@PAContra_Account_IDX <> 0)
                    begin
                      exec @iStatus = taPAAuxAcctsGetIdx
                        @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID ,
                        @I_vPSMISCID ,
                        3 ,
                        'ML' ,
                        8 ,
                        @CUSTNMBR ,
                        @PACONTNUMBER ,
                        @PAOverhead_IDX output ,
                        @O_oErrorState output
                      
                      select @iError = @@error
                      
                      if @iStatus = 0
                         and @iError <> 0
                        select @iStatus = @iError
                      
                      if (@iStatus <> 0)
                          or (@O_oErrorState <> 0)
                        begin
                          select @O_iErrorState = 1087
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                  else
                    begin
                      select @PACGBWIPIDX = 0,
                             @PACogs_Idx = 0
                    end
                end
              else
                begin
                  exec @iStatus = taPAAuxAcctsGetIdx
                    @I_vPAPROJNUMBER ,
                    @I_vPACOSTCATID ,
                    @I_vPSMISCID ,
                    3 ,
                    'ML' ,
                    30 ,
                    @CUSTNMBR ,
                    @PACONTNUMBER ,
                    @PACGBWIPIDX output ,
                    @O_oErrorState output
                  
                  select @iError = @@error
                  
                  if @iStatus = 0
                     and @iError <> 0
                    select @iStatus = @iError
                  
                  if (@iStatus <> 0)
                      or (@O_oErrorState <> 0)
                    begin
                      select @O_iErrorState = 1088
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  
                  if (@PACGBWIPIDX <> 0)
                    begin
                      exec @iStatus = taPAAuxAcctsGetIdx
                        @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID ,
                        @I_vPSMISCID ,
                        3 ,
                        'ML' ,
                        31 ,
                        @CUSTNMBR ,
                        @PACONTNUMBER ,
                        @PAContra_Account_IDX output ,
                        @O_oErrorState output
                      
                      select @iError = @@error
                      
                      if @iStatus = 0
                         and @iError <> 0
                        select @iStatus = @iError
                      
                      if (@iStatus <> 0)
                          or (@O_oErrorState <> 0)
                        begin
                          select @O_iErrorState = 1089
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (@PAContra_Account_IDX <> 0)
                        begin
                          exec @iStatus = taPAAuxAcctsGetIdx
                            @I_vPAPROJNUMBER ,
                            @I_vPACOSTCATID ,
                            @I_vPSMISCID ,
                            3 ,
                            'ML' ,
                            39 ,
                            @CUSTNMBR ,
                            @PACONTNUMBER ,
                            @PAOverhead_IDX output ,
                            @O_oErrorState output
                          
                          select @iError = @@error
                          
                          if @iStatus = 0
                             and @iError <> 0
                            select @iStatus = @iError
                          
                          if (@iStatus <> 0)
                              or (@O_oErrorState <> 0)
                            begin
                              select @O_iErrorState = 1090
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                        end
                      else
                        begin
                          select @PACGBWIPIDX = 0
                        end
                    end
                  else
                    begin
                      select @PAContra_Account_IDX = 0
                    end
                  
                  if (@FUNCRIDX <> @CURRNIDX)
                    begin
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PACGBWIPIDX
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PACGBWIPIDX <> 0))
                        begin
                          select @O_iErrorState = 1091
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PACogs_Idx
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PACogs_Idx <> 0))
                        begin
                          select @O_iErrorState = 1092
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PAUnbilled_AR_Idx
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PAUnbilled_AR_Idx <> 0))
                        begin
                          select @O_iErrorState = 1093
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PAUnbilled_Proj_Rev_Idx
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PAUnbilled_Proj_Rev_Idx <> 0))
                        begin
                          select @O_iErrorState = 1094
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PAContra_Account_IDX
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PAContra_Account_IDX <> 0))
                        begin
                          select @O_iErrorState = 1095
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (not exists (select 1
                                      from   MC00200 (nolock)
                                      where  ACTINDX = @PAOverhead_IDX
                                             and CURNCYID = @I_vCURNCYID)
                          and (@PAOverhead_IDX <> 0))
                        begin
                          select @O_iErrorState = 1096
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                end
              
              if ((@PAMLunitcostfrom = 3)
                   or ((@PAMLunitcostfrom = 2)
                       and (@I_vPAPROJNUMBER = '<NONE>')))
                begin
                  select @PAUNITCOST = isnull(PAUNITCOST,0),
                         @PABase_Unit_Cost = isnull(PAUNITCOST,0),
                         @I_vPAUnit_of_Measure = isnull(PAUnit_of_Measure,0)
                  from   PA01001 (nolock)
                  where  PACOSTCATID = @I_vPACOSTCATID
                end
              else
                if (@PAMLunitcostfrom = 4)
                  begin
                    select @PAUNITCOST = isnull(PAUNITCOST,0),
                           @PABase_Unit_Cost = isnull(PAUNITCOST,0),
                           @I_vPAUnit_of_Measure = isnull(PAUnit_of_Measure,0)
                    from   PA00801 (nolock)
                    where  PSMISCID = @I_vPSMISCID
                  end
              
              if (@I_vPAUNITCOST = 0)
                select @I_vPAUNITCOST = @PAUNITCOST
              
              if ((@PAMLprofittypefrom = 1)
                  and (@I_vPAPROJNUMBER <> '<NONE>'))
                begin
                  if ((@PAProjectType = 1)
                      and (@PAProfitType = 1))
                    select @PABILRATE = @PAFProfitAmt,
                           @PA_Base_Billing_Rate = @PAFProfitAmt
                  
                  if ((@PAProjectType = 1)
                      and (@PAProfitType = 2))
                    select @PAMARKPERCENT = @PAFProfitPcnt
                  
                  if ((@PAProjectType = 1)
                      and (@PAProfitType = 8))
                    select @PABILRATE = 0,
                           @PA_Base_Billing_Rate = 0
                end
              else
                if (@PAMLprofittypefrom = 2)
                  begin
                    select @PATMProfitAmount = isnull(PATMProfitAmount,0),
                           @PATMProfitPercent = isnull(PATMProfitPercent,0),
                           @PATMProfitType = isnull(PATMProfitType,0),
                           @PAFFProfitType = isnull(PAFFProfitType,0),
                           @PAProfit_Type__CP = isnull(PAProfit_Type__CP,0)
                    from   PA00801 (nolock)
                    where  PSMISCID = @I_vPSMISCID
                    
                    if ((@PAProjectType = 1)
                        and (@PATMProfitType = 1))
                      select @PABILRATE = @PATMProfitAmount,
                             @PA_Base_Billing_Rate = @PATMProfitAmount,
                             @PAProfitType = @PATMProfitType
                    
                    if ((@PAProjectType = 1)
                        and (@PATMProfitType = 2))
                      select @PAMARKPERCENT = @PATMProfitPercent,
                             @PAProfitType = @PATMProfitType
                    
                    if ((@PAProjectType = 1)
                        and (@PATMProfitType = 8))
                      select @PABILRATE = 0,
                             @PA_Base_Billing_Rate = 0,
                             @PAProfitType = @PATMProfitType
                    
                    if (@PAProjectType = 2)
                      select @PAProfitType = @PAProfit_Type__CP
                    
                    if (@PAProjectType = 3)
                      select @PAProfitType = @PAFFProfitType
                  end
                else
                  begin
                    select @PATMProfitAmount = isnull(PATMProfitAmount,0),
                           @PATMProfitPercent = isnull(PATMProfitPercent,0),
                           @PAProfit_Type__CP = isnull(PAProfit_Type__CP,0),
                           @PAFFProfitType = isnull(PAFFProfitType,0),
                           @PAProfitType = isnull(PATMProfitType,0)
                    from   PA01001 (nolock)
                    where  PACOSTCATID = @I_vPACOSTCATID
                    
                    if ((@PAProjectType = 1)
                        and (@PATMProfitType = 1))
                      select @PABILRATE = @PATMProfitAmount,
                             @PA_Base_Billing_Rate = @PATMProfitAmount,
                             @PAProfitType = @PATMProfitType
                    
                    if ((@PAProjectType = 1)
                        and (@PATMProfitType = 2))
                      select @PAMARKPERCENT = @PATMProfitPercent,
                             @PAProfitType = @PATMProfitType
                    
                    if ((@PAProjectType = 1)
                        and (@PATMProfitType = 8))
                      select @PABILRATE = 0,
                             @PA_Base_Billing_Rate = 0,
                             @PAProfitType = @PATMProfitType
                    
                    if (@PAProjectType = 2)
                      select @PAProfitType = @PAProfit_Type__CP
                    
                    if (@PAProjectType = 3)
                      select @PAProfitType = @PAFFProfitType
                  end
              
              if (@I_vPAPROJNUMBER = '<NONE>')
                select @PAProfitAmount = 0,
                       @PABILRATE = 0,
                       @PA_Base_Billing_Rate = 0,
                       @PAMARKPERCENT = 0,
                       @PAProfitPercent = 0,
                       @PATotalProfit = 0,
                       @PAACREV = 0
              
              if (@UOMSCHDL = '')
                begin
                  select @PABase_Unit_Cost = @I_vPAUNITCOST,
                         @PABaseOvhdCost = @PAOverhead_Amount
                end
              else
                begin
                  if (@I_vPAUnit_of_Measure <> '')
                    begin
                      select @BASEUOFM = BASEUOFM
                      from   PA40101 (nolock)
                      where  UOMSCHDL = @UOMSCHDL
                      
                      select @l_UOMSCHDL = isnull(UOMSCHDL,''),
                             @EQUOMQTY = isnull(EQUOMQTY,0)
                      from   PA40102 (nolock)
                      where  UOMSCHDL = @UOMSCHDL
                             and UOFM = @I_vPAUnit_of_Measure
                             and EQUIVUOM = @BASEUOFM
                      
                      if (@l_UOMSCHDL = '')
                        begin
                          select @I_vPAUnit_of_Measure = @l_PAUnit_of_Measure
                          
                          select @EQUOMQTY = isnull(EQUOMQTY,0)
                          from   PA40102 (nolock)
                          where  UOMSCHDL = @UOMSCHDL
                                 and UOFM = @I_vPAUnit_of_Measure
                                 and EQUIVUOM = @BASEUOFM
                        end
                      
                      if (@EQUOMQTY <> 0)
                        select @PABase_Unit_Cost = @I_vPAUNITCOST / @EQUOMQTY,
                               @PABaseOvhdCost = @PAOverhead_Amount / @EQUOMQTY,
                               @PA_Base_Billing_Rate = @PABILRATE / @EQUOMQTY
                    end
                end
              
              select @I_vPAUNITCOST = round(@I_vPAUNITCOST,@UnitCurrPlace),
                     @PABase_Unit_Cost = round(@PABase_Unit_Cost,@UnitCurrPlace),
                     @PAOverhead_Amount = round(@PAOverhead_Amount,@UnitCurrPlace),
                     @PABaseOvhdCost = round(@PABaseOvhdCost,@UnitCurrPlace),
                     @ORUNTCST = round(@I_vPAUNITCOST,@UnitCurrPlace),
                     @PAORGBSUNITCST = round(@PABase_Unit_Cost,@UnitCurrPlace),
                     @PAORIGOVHDAMT = round(@PAOverhead_Amount,@UnitCurrPlace),
                     @PAORIGBASEOVRHCST = round(@PABaseOvhdCost,@UnitCurrPlace),
                     @PABILRATE = round(@PABILRATE,@UnitCurrPlace),
                     @PA_Base_Billing_Rate = round(@PA_Base_Billing_Rate,@UnitCurrPlace),
                     @PAORIGBILLRATE = round(@PABILRATE,@UnitCurrPlace),
                     @PAORIGBSBILLRTE = round(@PA_Base_Billing_Rate,@UnitCurrPlace)
            end
        end
    end
  
  if ((@I_vPAQtyQ = 0)
      and (@PAallow_3 <> 1))
    begin
      select @O_iErrorState = 1097
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @l_qty = @I_vPAQtyQ
  
  if (@UOMSCHDL = '')
    begin
      select @l_base_qty = @l_qty
    end
  else
    begin
      select @l_UOMSCHDL = isnull(UOMSCHDL,''),
             @BASEUOFM = BASEUOFM
      from   PA40101 (nolock)
      where  UOMSCHDL = @UOMSCHDL
      
      if (@l_UOMSCHDL <> '')
        begin
          select @l_UOMSCHDL = isnull(UOMSCHDL,''),
                 @EQUOMQTY = isnull(EQUOMQTY,0)
          from   PA40102 (nolock)
          where  UOMSCHDL = @UOMSCHDL
                 and UOFM = @I_vPAUnit_of_Measure
                 and EQUIVUOM = @BASEUOFM
          
          if (@l_UOMSCHDL <> '')
            begin
              select @l_base_qty = @l_qty * @EQUOMQTY
            end
          else
            begin
              select @l_base_qty = @l_qty
            end
        end
      else
        begin
          select @l_base_qty = @l_qty
        end
    end
  
  select @l_base_qty = round(@l_base_qty,@DECPLQTY - 1),
         @O_overhead = round((@PAORIGBASEOVRHCST + (@PAORGBSUNITCST * (@PAOverheaPercentage / 100))) * @l_base_qty,
                             @UnitCurrPlace),
         @O_extended = round((@PAORGBSUNITCST * @l_base_qty),@UnitCurrPlace),
         @O_cost = (@O_extended + @O_overhead),
         @l_overhead = round(@O_overhead,@UnitCurrPlace),
         @l_extended = round(@O_extended,@UnitCurrPlace),
         @l_cost = round(@O_cost,@UnitCurrPlace)
  
  if ((@I_vPAbllngtype = 1)
      and (@PAProjectType = 1))
    begin
      if (@PAProfitType = 1)
        select @O_accrued = round(@PAORIGBSBILLRTE * @l_base_qty,@UnitCurrPlace)
      
      if (@PAProfitType = 2)
        select @O_accrued = round(@O_extended * (1 + (@PAMARKPERCENT / 100)),
                                  @UnitCurrPlace)
      
      if (@PAProfitType = 8)
        select @O_accrued = 0
      
      select @l_accrued = round(@O_accrued,@UnitCurrPlace)
    end
  else
    begin
      select @l_accrued = 0,
             @O_accrued = 0
    end
  
  if (@I_vPAPROJNUMBER <> '<NONE>')
    begin
      select @l_UOMSCHDL = isnull(UOMSCHDL,''),
             @l_PAUnit_of_Measure = isnull(PAUnit_of_Measure,''),
             @PAPostedQty = isnull(PAPostedQty,0),
             @PAUnpostedQty = isnull(PAUnpostedQty,0),
             @PAFQuantity = isnull(PAFQuantity,0),
             @PAPostedTotalCostN = isnull(PAPostedTotalCostN,0),
             @PAUnpostedTotalCostN = isnull(PAUnpostedTotalCostN,0),
             @PAFTotalCost = isnull(PAFTotalCost,0),
             @PAPosted_Accr_RevN = isnull(PAPosted_Accr_RevN,0),
             @PABilled_Accrued_Revenu = isnull(PABilled_Accrued_Revenu,0),
             @PAUnpostAccrRevN = isnull(PAUnpostAccrRevN,0),
             @PAFBillings = isnull(PAFBillings,0)
      from   PA01301 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PACOSTCATID = @I_vPACOSTCATID
             and PATU = @PATU
      
      select @l_oqty = (@PAPostedQty + @PAUnpostedQty) + @l_base_qty
      
      if (@PAFQuantity < @l_oqty)
        begin
          if (@PAallow_7 = 0)
            begin
              select @O_iErrorState = 1099
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      select @l_ocost = (@PAPostedTotalCostN + @PAUnpostedTotalCostN)
      
      if ((@PAFTotalCost < @l_ocost))
        begin
          if (@PAallow_8 = 0)
            begin
              select @O_iErrorState = 1105
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      if (@I_vPAbllngtype = 1)
        begin
          select @l_oaccrued = (@PAPosted_Accr_RevN + @PABilled_Accrued_Revenu + @l_accrued + @PAUnpostAccrRevN)
          
          if (@PAFBillings < @l_oaccrued)
            begin
              if (@PAallow_5 = 0)
                begin
                  select @O_iErrorState = 1107
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
        end
    end
  
  select @I_vPAQtyQ = @l_qty,
         @PABase_Qty = @l_base_qty,
         @PATOTALOVERH = @l_overhead,
         @PAORIGTOTOVRHD = @O_overhead,
         @PAEXTCOST = @l_extended,
         @OREXTCST = @O_extended,
         @I_vPATOTCST = @l_cost,
         @PAORIGTOTCOST = @O_cost,
         @PAACREV = @l_accrued,
         @PAORIACCRREV = @O_accrued,
         @PASTD_Qty = case 
                        when (@I_vPAbllngtype = 1) then @l_base_qty
                        else 0
                      end,
         @PANC_Qty = case 
                       when (@I_vPAbllngtype = 2) then @l_base_qty
                       else 0
                     end,
         @PANB_Qty = case 
                       when (@I_vPAbllngtype = 3) then @l_base_qty
                       else 0
                     end,
         @PATotalProfit = case 
                            when @l_qty < 0 then @l_extended * -1
                            when (@PAACREV - @I_vPATOTCST) < 0 then 0
                            else (@PAACREV - @I_vPATOTCST)
                          end,
         @PAORIGTOTPROF = case 
                            when (@PAORIACCRREV - @PAORIGTOTCOST) < 0 then 0
                            else (@PAORIACCRREV - @PAORIGTOTCOST)
                          end
  
  if ((@I_vPAbllngtype < 1)
       or (@I_vPAbllngtype > 3))
    begin
      select @O_iErrorState = 1108
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vCURNCYID <> '')
    begin
      if not exists (select 1
                     from   DYNAMICS..MC40200 (nolock)
                     where  CURNCYID = @I_vCURNCYID)
        begin
          select @O_iErrorState = 1109
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@add_employee_access = 1)
    begin
      insert into PA01408
                 (PAPROJNUMBER,
                  EMPLOYID,
                  PACONTNUMBER)
      select @I_vPAPROJNUMBER,
             @I_vPSMISCID,
             @PACONTNUMBER
      
      if @@error <> 0
        begin
          select @O_iErrorState = 1110
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPAtx500 <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidx,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAtx500
      
      if @@error <> 0
        begin
          select @O_iErrorState = 1111
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@oErrString <> '')
    return (@O_iErrorState)
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_MiscLog' ,
        @I_vINDEX1 = @I_vPAMISCLDOCNO ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 0 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2039
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  insert into PA10201
             (PSMISCLTRXTYPE,
              PAMISCLDOCNO,
              PAREFNO,
              PSMISCID,
              PADT,
              PAPROJNUMBER,
              PACOSTCATID,
              PAQtyQ,
              PABase_Qty,
              PAUnit_of_Measure,
              UOMSCHDL,
              PAROUNDAMT,
              PAUNITCOST,
              ORUNTCST,
              PABase_Unit_Cost,
              PAORGBSUNITCST,
              PAEXTCOST,
              OREXTCST,
              PATOTCST,
              PAORIGTOTCOST,
              PAOverhead_Amount,
              PABaseOvhdCost,
              PAORIGOVHDAMT,
              PAORIGBASEOVRHCST,
              PAOverheaPercentage,
              PATOTALOVERH,
              PAORIGTOTOVRHD,
              PABILRATE,
              PAORIGBILLRATE,
              PA_Base_Billing_Rate,
              PAORIGBSBILLRTE,
              PAMARKPERCENT,
              PAACREV,
              PAORIACCRREV,
              PASTD_Qty,
              PANC_Qty,
              PANB_Qty,
              LNITMSEQ,
              PAReference_Line_Seq_N,
              PAbillnoteidx,
              PAbllngtype,
              PACONTNUMBER,
              PAProfitType,
              PAProfitPercent,
              PAProfitAmount,
              PAORIGPROFAMT,
              PATotalProfit,
              PAORIGTOTPROF,
              PAexptdate,
              PAApproved_Quantity,
              PAAPPROVBILLRATE,
              PAAPPROVMKUPPCT,
              PAApproved_Billing_Amou,
              PABilledProfitPercentage,
              PA_Billed_Profit_Amount_,
              PABilled_Amount,
              PABilledTax,
              PABilled_Misc,
              PABilled_Freight,
              PABilled_Discount,
              PAWrite_UpDown_Amount,
              PAWrite_UpDown_Percenta,
              PAApprover_ID,
              PABilled_QtyN,
              PAApprover_Type,
              PAPartial_Bill,
              PABilling_StatusN,
              PACGBWIPIDX,
              PAUnbilled_AR_Idx,
              PACogs_Idx,
              PAContra_Account_IDX,
              PAOverhead_IDX,
              PAUnbilled_Proj_Rev_Idx,
              RNDDIFF,
              PACHGORDNO,
              PAML_Line_Errors,
              PAMLLINEERR2,
              PAMCCURNCYID,
              CURRNIDX,
              RATETPID,
              EXGTBLID,
              XCHGRATE,
              EXCHDATE,
              TIME1,
              RATECALC,
              DENXRATE,
              MCTRXSTT,
              PA_MC_Accrued_Revenue,
              PA_MC_Base_Billing_Rate,
              PA_MC_Billing_Rate,
              Correcting_Trx_Type,
              PAORIGINLDOCNUM,
              PAORIGLINEIMSEQ)
  select @I_vPSMISCLTRXTYPE,
         @I_vPAMISCLDOCNO,
         @I_vPAREFNO,
         @I_vPSMISCID,
         @I_vPADT,
         @I_vPAPROJNUMBER,
         @I_vPACOSTCATID,
         @I_vPAQtyQ,
         @PABase_Qty,
         @I_vPAUnit_of_Measure,
         @UOMSCHDL,
         @PAROUNDAMT,
         @I_vPAUNITCOST,
         @ORUNTCST,
         @PABase_Unit_Cost,
         @PAORGBSUNITCST,
         @PAEXTCOST,
         @OREXTCST,
         @I_vPATOTCST,
         @PAORIGTOTCOST,
         @PAOverhead_Amount,
         @PABaseOvhdCost,
         @PAORIGOVHDAMT,
         @PAORIGBASEOVRHCST,
         @PAOverheaPercentage,
         @PATOTALOVERH,
         @PAORIGTOTOVRHD,
         @PABILRATE,
         @PAORIGBILLRATE,
         @PA_Base_Billing_Rate,
         @PAORIGBSBILLRTE,
         @PAMARKPERCENT,
         @PAACREV,
         @PAORIACCRREV,
         @PASTD_Qty,
         @PANC_Qty,
         @PANB_Qty,
         @I_vLNITMSEQ,
         @PAReference_Line_Seq_N,
         @PAbillnoteidx,
         @I_vPAbllngtype,
         @PACONTNUMBER,
         @PAProfitType,
         @PAProfitPercent,
         @PAProfitAmount,
         @PAORIGPROFAMT,
         @PATotalProfit,
         @PAORIGTOTPROF,
         @PAexptdate,
         @PAApproved_Quantity,
         @PAAPPROVBILLRATE,
         @PAAPPROVMKUPPCT,
         @PAApproved_Billing_Amou,
         @PABilledProfitPercentage,
         @PA_Billed_Profit_Amount_,
         @PABilled_Amount,
         @PABilledTax,
         @PABilled_Misc,
         @PABilled_Freight,
         @PABilled_Discount,
         @PAWrite_UpDown_Amount,
         @PAWrite_UpDown_Percenta,
         @PAApprover_ID,
         @PABilled_QtyN,
         @PAApprover_Type,
         @PAPartial_Bill,
         @PABilling_StatusN,
         @PACGBWIPIDX,
         @PAUnbilled_AR_Idx,
         @PACogs_Idx,
         @PAContra_Account_IDX,
         @PAOverhead_IDX,
         @PAUnbilled_Proj_Rev_Idx,
         @RNDDIFF,
         @PACHGORDNO,
         0,
         0,
         @PAMCCURNCYID,
         @PAMCCURRNIDX,
         @RATETPID,
         @EXGTBLID,
         @XCHGRATE,
         @EXCHDATE,
         @TIME1,
         @RATECALC,
         @DENXRATE,
         @MCTRXSTT,
         @PA_MC_Accrued_Revenue,
         @PA_MC_Base_Billing_Rate,
         @PA_MC_Billing_Rate,
         0,
         '',
         0
  
  if @@error <> 0
    begin
      select @O_iErrorState = 1112
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAMiscLogLineInsertPost
    @I_vPSMISCLTRXTYPE ,
    @I_vPAMISCLDOCNO ,
    @I_vPADOCDT ,
    @I_vPSMISCID ,
    @I_vPAREPD ,
    @I_vPAREPDT ,
    @I_vCURNCYID ,
    @I_vPAREFNO ,
    @I_vLNITMSEQ ,
    @I_vPADT ,
    @I_vPAPROJNUMBER ,
    @I_vPACOSTCATID ,
    @I_vPAQtyQ ,
    @I_vPAbllngtype ,
    @I_vPAUnit_of_Measure ,
    @I_vPAUNITCOST ,
    @I_vPATOTCST ,
    @I_vPAtx500 ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1113
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_MiscLog' ,
        @I_vINDEX1 = @I_vPAMISCLDOCNO ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 1 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2040
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  return (@O_iErrorState)

