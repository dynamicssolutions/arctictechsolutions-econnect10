

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAProjectBillingCycle]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAProjectBillingCycle]
  
create procedure [dbo].[taPAProjectBillingCycle]
                @I_vPAPROJNUMBER  char(15),
                @I_vPABILLCYCLEID char(15),
                @I_vPABILLFORMAT  char(15),
                @I_vAction        smallint  = 1,
                @I_vRequesterTrx  smallint  = 0,
                @I_vUSRDEFND1     char(50)  = '',
                @I_vUSRDEFND2     char(50)  = '',
                @I_vUSRDEFND3     char(50)  = '',
                @I_vUSRDEFND4     varchar(8000)  = '',
                @I_vUSRDEFND5     varchar(8000)  = '',
                @O_iErrorState    int  output,
                @oErrString       varchar(255)  output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @CURNCYID         char(15),
           @CURRNIDX         int,
           @iStatus          int,
           @iCustomState     int,
           @iCustomErrString varchar(255),
           @iError           int,
           @O_oErrorState    int
  
  select @CURNCYID = '',
         @CURRNIDX = 0,
         @O_oErrorState = 0,
         @O_iErrorState = 0,
         @iStatus = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taPAProjectBillingCyclePre
    @I_vPAPROJNUMBER output ,
    @I_vPABILLCYCLEID output ,
    @I_vPABILLFORMAT output ,
    @I_vAction output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4925
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAPROJNUMBER is NULL 
       or @I_vPABILLCYCLEID is NULL
       or @I_vPABILLFORMAT is NULL
       or @I_vRequesterTrx is NULL)
    begin
      select @O_iErrorState = 4926
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAPROJNUMBER = ''
       OR @I_vPABILLCYCLEID = ''
       OR @I_vPABILLFORMAT = '')
    begin
      select @O_iErrorState = 4927
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vPAPROJNUMBER = UPPER(@I_vPAPROJNUMBER),
         @I_vPABILLCYCLEID = UPPER(@I_vPABILLCYCLEID),
         @I_vPABILLFORMAT = UPPER(@I_vPABILLFORMAT)
  
  if not exists (select 1
                 from   PA01201 (nolock)
                 where  PAPROJNUMBER = @I_vPAPROJNUMBER)
    begin
      select @O_iErrorState = 5259
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vAction = 1)
    begin
      if exists (select 1
                 from   PA61020 (nolock)
                 where  PABILLCYCLEID = @I_vPABILLCYCLEID
                        and PABILLFORMAT = @I_vPABILLFORMAT
                        and PAPROJNUMBER = @I_vPAPROJNUMBER)
        begin
          select @O_iErrorState = 4928
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if not exists (select 1
                 from   PA02000 (nolock)
                 where  PABILLCYCLEID = @I_vPABILLCYCLEID)
    begin
      select @O_iErrorState = 4929
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if not exists (select 1
                 from   PA43101 (nolock)
                 where  PA_Bill_Format_Number = @I_vPABILLFORMAT)
    begin
      select @O_iErrorState = 4930
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vAction < 1
       or @I_vAction > 2)
    begin
      select @O_iErrorState = 5260
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@oErrString <> '')
    return (@O_iErrorState)
  
  if (@I_vAction = 1)
    begin
      insert into PA61020
                 (PAPROJNUMBER,
                  PABILLCYCLEID,
                  PABILLFORMAT)
      select @I_vPAPROJNUMBER,
             @I_vPABILLCYCLEID,
             @I_vPABILLFORMAT
    end
  else
    begin
      delete PA61020
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PABILLCYCLEID = @I_vPABILLCYCLEID
    end
  
  if @@error <> 0
    begin
      select @O_iErrorState = 4931
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAProjectBillingCyclePost
    @I_vPAPROJNUMBER ,
    @I_vPABILLCYCLEID ,
    @I_vPABILLFORMAT ,
    @I_vAction ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4932
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

