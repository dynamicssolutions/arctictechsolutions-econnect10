

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsCreateInternetAddresses]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsCreateInternetAddresses]
  
create procedure [dbo].[RmsCreateInternetAddresses]
                @I_vMaster_Type char(3),
                @I_vMaster_ID   char(30),
                @I_vADRSCODE    char(15)  = '',
                @I_vINET1       char(200)  = '',
                @I_vINET2       char(200)  = '',
                @I_vINET3       char(200)  = '',
                @I_vINET4       char(200)  = '',
                @I_vINET5       char(200)  = '',
                @I_vINET6       char(200)  = '',
                @I_vINET7       char(200)  = '',
                @I_vINET8       char(200)  = '',
                @I_vINETINFO    text  = '',
                @I_vUSRDEFND1   char(50)  = '',
                @I_vUSRDEFND2   char(50)  = '',
                @I_vUSRDEFND3   char(50)  = '',
                @I_vUSRDEFND4   varchar(8000)  = '',
                @I_vUSRDEFND5   varchar(8000)  = '',
                @O_iErrorState  int  output,
                @oErrString     varchar(255)  output   /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iCustomState     int,
           @O_oErrorState    int,
           @iCustomErrString varchar(255),
           @iError           int,
           @iStatus          int,
           @iAddCodeErrState int,
           @sCompanyID       smallint
  
  select @iStatus = 0,
         @iAddCodeErrState = 0,
         @iCustomState = 0,
         @O_oErrorState = 0,
         @O_iErrorState = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  select @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 c (nolock)
  where  c.INTERID = db_name()
  
  if (@I_vMaster_Type is null 
       or @I_vMaster_ID is null
       or @I_vADRSCODE is null
       or @I_vINET1 is null
       or @I_vINET2 is null
       or @I_vINET3 is null
       or @I_vINET4 is null
       or @I_vINET5 is null
       or @I_vINET6 is null
       or @I_vINET7 is null
       or @I_vINET8 is null
       or @I_vINETINFO is null)
    begin
      select @O_iErrorState = 10900
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  select @I_vMaster_Type = UPPER(@I_vMaster_Type),
         @I_vMaster_ID = UPPER(@I_vMaster_ID),
         @I_vADRSCODE = UPPER(@I_vADRSCODE)
  
  if (@I_vMaster_Type = '')
    begin
      select @O_iErrorState = 10901
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vMaster_ID = '')
    begin
      select @O_iErrorState = 10902
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vADRSCODE = ''
      and (@I_vMaster_Type = 'CUS'
            or @I_vMaster_Type = 'VEN'
            or @I_vMaster_Type = 'CMP'
            or @I_vMaster_Type = 'EMP'))
    begin
      select @O_iErrorState = 10903
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@oErrString <> '')
    return (@O_iErrorState)
  
  if @I_vMaster_Type = 'SLP'
    begin
      if not exists (select 1
                     from   RM00301 (nolock)
                     where  SLPRSNID = @I_vMaster_ID)
        begin
          select @O_iErrorState = 653
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  
  if @I_vMaster_Type = 'CUS'
    begin
      if not exists (select 1
                     from   RM00101 (nolock)
                     where  CUSTNMBR = @I_vMaster_ID)
        begin
          select @O_iErrorState = 654
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
      
      if not exists (select 1
                     from   RM00102 (nolock)
                     where  ADRSCODE = @I_vADRSCODE)
        begin
          select @O_iErrorState = 655
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  
  if @I_vMaster_Type = 'VEN'
    begin
      if not exists (select 1
                     from   PM00200 (nolock)
                     where  VENDORID = @I_vMaster_ID)
        begin
          select @O_iErrorState = 656
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
      
      if not exists (select 1
                     from   PM00300 (nolock)
                     where  ADRSCODE = @I_vADRSCODE)
        begin
          select @O_iErrorState = 657
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  
  if @I_vMaster_Type = 'ITM'
    begin
      if not exists (select 1
                     from   IV00101 (nolock)
                     where  ITEMNMBR = @I_vMaster_ID)
        begin
          select @O_iErrorState = 658
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  
  if @I_vMaster_Type = 'EMP'
    begin
      if not exists (select 1
                     from   UPR00100 (nolock)
                     where  EMPLOYID = @I_vMaster_ID)
        begin
          select @O_iErrorState = 659
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
      
      if not exists (select 1
                     from   UPR00102 (nolock)
                     where  ADRSCODE = @I_vADRSCODE)
        begin
          select @O_iErrorState = 660
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  
  if @I_vMaster_Type = 'CMP'
    begin
      if not exists (select 1
                     from   DYNAMICS..SY01500 (nolock)
                     where  INTERID = @I_vMaster_ID)
        begin
          select @O_iErrorState = 661
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
      
      if not exists (select 1
                     from   SY00600 (nolock)
                     where  LOCATNID = @I_vADRSCODE)
        begin
          select @O_iErrorState = 662
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  
  if (not exists (select 1
                  from   SY01200 (nolock)
                  where  Master_Type = @I_vMaster_Type
                         and Master_ID = @I_vMaster_ID
                         and ADRSCODE = @I_vADRSCODE))
    begin
      insert SY01200
            (Master_Type,
             Master_ID,
             ADRSCODE,
             INET1,
             INET2,
             INET3,
             INET4,
             INET5,
             INET6,
             INET7,
             INET8,
             INETINFO)
      select @I_vMaster_Type,
             @I_vMaster_ID,
             @I_vADRSCODE,
             @I_vINET1,
             @I_vINET2,
             @I_vINET3,
             @I_vINET4,
             @I_vINET5,
             @I_vINET6,
             @I_vINET7,
             @I_vINET8,
             @I_vINETINFO
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 10904
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  else
    begin
      update SY01200
      set    INET1 = @I_vINET1,
             INET2 = @I_vINET2,
             INET3 = @I_vINET3,
             INET4 = @I_vINET4,
             INET5 = @I_vINET5,
             INET6 = @I_vINET6,
             INET7 = @I_vINET7,
             INET8 = @I_vINET8,
             INETINFO = @I_vINETINFO
      where  Master_Type = @I_vMaster_Type
             and Master_ID = @I_vMaster_ID
             and ADRSCODE = @I_vADRSCODE
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 10905
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  
  if (not exists (select 1
                  from   SY04800 (nolock)
                  where  CMPANYID = @sCompanyID))
    begin
      insert SY04800
            (CMPANYID,
             INETPRMPTS_1,
             INETPRMPTS_2,
             INETPRMPTS_3,
             INETPRMPTS_4,
             INETPRMPTS_5,
             INETPRMPTS_6,
             INETPRMPTS_7,
             INETPRMPTS_8)
      select @sCompanyID,
             'E-mail',
             'Home Page',
             'FTP Site',
             'Image',
             'Login',
             'Password',
             'User Defined 1',
             'User Defined 2'
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 5374
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  
  return (@O_iErrorState)

