

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsUpdateCreateItemRcdWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsUpdateCreateItemRcdWrapper]
  
create procedure [dbo].[RmsUpdateCreateItemRcdWrapper]
                @iItemNumber    char(30),
                @iItemDesc      char(100),
                @iItemClassCode char(10),
                @oErrorState    int  output,
                @oErrString     varchar(255)  output   /*with encryption*/
AS
  DECLARE  @lShorDesc char(15);
  
  SELECT @lShorDesc = LEFT(@iItemDesc,15)
  
  EXEC RmsUpdateCreateItemRcd
    @I_vITEMNMBR = @iItemNumber ,
    @I_vITEMDESC = @iItemDesc ,
    @I_vITMSHNAM = @lShorDesc ,
    @I_vITMCLSCD = @iItemClassCode ,
    @I_vUseItemClass = 1 ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

