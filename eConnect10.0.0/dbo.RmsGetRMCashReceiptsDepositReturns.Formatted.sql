

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetRMCashReceiptsDepositReturns]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetRMCashReceiptsDepositReturns]
  
create procedure [dbo].[RmsGetRMCashReceiptsDepositReturns]
                @iRMCashReceiptsTempTable VARCHAR(255),
                @iRMInvoiceTenderTotals   VARCHAR(255),
                @iStartDate               DATETIME,
                @iEndDate                 DATETIME,
                @iRmsServerName           VARCHAR(255),
                @iHqDbName                VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lOrderHistoryTable VARCHAR(255)
  
  DECLARE  @lOrderTable VARCHAR(255)
  
  DECLARE  @lTenderEntryTable VARCHAR(255)
  
  DECLARE  @lCustomerTable VARCHAR(255)
  
  DECLARE  @lTenderTable VARCHAR(255)
  
  SELECT @lOrderHistoryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.OrderHistory as OrderHistory'
  
  SELECT @lOrderTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.[Order] as Orders'
  
  SELECT @lTenderEntryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.TenderEntry as TenderEntry'
  
  SELECT @lCustomerTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Customer as Customer'
  
  SELECT @lTenderTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Tender as Tender'
  
  EXEC( 'INSERT INTO ' + @iRMCashReceiptsTempTable + ' (RmsStoreID,  RmsOrderHistoryID,  CUSTNMBR,  SLSAMNT,  RmsOrderType,  DATE1,  CASHAMNT,  CRCRDAMT,  CHEKAMNT,  ACCTAMNT,  RmsDepositKept)  SELECT  OrderHistory.StoreID,  OrderHistory.[ID],  LEFT(ISNULL(UPPER(Customer.AccountNumber),''''),15),  0,  Orders.Type,  CONVERT(DATETIME,CONVERT(VARCHAR,OrderHistory.[Date],111)),  0,  0,  0,  0,  ISNULL(Orders.Deposit,0)  FROM ' + @lOrderHistoryTable + ' JOIN ' + @lOrderTable + ' ON ' + ' OrderHistory.OrderID = Orders.[ID]  AND  OrderHistory.StoreID = Orders.StoreID  LEFT OUTER JOIN ' + @lCustomerTable + ' ON ' + 'Orders.CustomerID = Customer.[ID]  WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,OrderHistory.[Date],111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  ((Orders.Type = 2 AND OrderHistory.TransactionNumber = 0)  OR  ((Orders.Type = 4 AND DeltaDeposit > 0 AND OrderHistory.TransactionNumber <> 0)  OR   (Orders.Type = 4 AND OrderHistory.TransactionNumber = 0))  OR  (Orders.Type = 5 AND OrderHistory.TransactionNumber = 0))  AND  NOT EXISTS(SELECT  1  FROM  RMS30800  WHERE  OrderHistory.StoreID = RMS30800.RmsStoreID  AND  OrderHistory.[ID] = RMS30800.RmsOrderHistoryID)')
  
  EXEC( 'INSERT INTO ' + @iRMInvoiceTenderTotals + ' (RmsTransactionNumber,  RmsStoreID,  IVCLINNO,  RmsTenderID,  SLSAMNT)  SELECT  TenderEntry.OrderHistoryID,  TenderEntry.StoreID,  0,  TenderEntry.TenderID,  SUM(Amount)  FROM ' + @lTenderEntryTable + ' WHERE  EXISTS(SELECT  1  FROM ' + @iRMCashReceiptsTempTable + ' WHERE  TenderEntry.OrderHistoryID = ' + @iRMCashReceiptsTempTable + '.RmsOrderHistoryID  AND  TenderEntry.StoreID = ' + @iRMCashReceiptsTempTable + '.RmsStoreID)   GROUP BY  TenderEntry.OrderHistoryID,  TenderEntry.StoreID,  TenderEntry.TenderID')
  
  EXEC( 'UPDATE ' + @iRMCashReceiptsTempTable + ' SET  SLSAMNT = ISNULL((SELECT  SUM(SLSAMNT)  FROM ' + @iRMInvoiceTenderTotals + ' WHERE ' + @iRMCashReceiptsTempTable + '.RmsStoreID ' + ' = ' + @iRMInvoiceTenderTotals + '.RmsStoreID ' + ' AND ' + @iRMCashReceiptsTempTable + '.RmsOrderHistoryID ' + ' = ' + @iRMInvoiceTenderTotals + '.RmsTransactionNumber ' + 'GROUP BY ' + @iRMInvoiceTenderTotals + '.RmsStoreID, ' + @iRMInvoiceTenderTotals + '.RmsTransactionNumber),0)')
  
  EXEC( 'UPDATE ' + @iRMCashReceiptsTempTable + ' SET  CASHAMNT = ISNULL((SELECT  SUM(Amount)   FROM ' + @lTenderEntryTable + ',' + @lTenderTable + ' WHERE ' + @iRMCashReceiptsTempTable + '.RmsStoreID = TenderEntry.StoreID   AND ' + @iRMCashReceiptsTempTable + '.RmsOrderHistoryID = TenderEntry.OrderHistoryID  AND  TenderEntry.TenderID = Tender.[ID]  AND  (Tender.AdditionalDetailType = 0  OR  Tender.AdditionalDetailType = 1  OR  Tender.AdditionalDetailType = 6  OR  Tender.AdditionalDetailType = 8)  GROUP BY  TenderEntry.StoreID,  TenderEntry.TransactionNumber),0),  CHEKAMNT =  ISNULL((SELECT  SUM(Amount)  FROM ' + @lTenderEntryTable + ',' + @lTenderTable + ' WHERE ' + @iRMCashReceiptsTempTable + '.RmsStoreID = TenderEntry.StoreID  AND ' + @iRMCashReceiptsTempTable + '.RmsOrderHistoryID = TenderEntry.OrderHistoryID  AND  TenderEntry.TenderID = Tender.[ID]  AND  Tender.AdditionalDetailType = 2  GROUP BY  TenderEntry.StoreID,  TenderEntry.TransactionNumber),0),  CRCRDAMT =  ISNULL((SELECT   SUM(Amount)  FROM ' + @lTenderEntryTable + ',' + @lTenderTable + ' WHERE ' + @iRMCashReceiptsTempTable + '.RmsStoreID = TenderEntry.StoreID  AND ' + @iRMCashReceiptsTempTable + '.RmsOrderHistoryID = TenderEntry.OrderHistoryID  AND  TenderEntry.TenderID = Tender.[ID]  AND  (Tender.AdditionalDetailType = 3  OR  Tender.AdditionalDetailType = 7  OR  Tender.AdditionalDetailType = 9)  GROUP BY  TenderEntry.StoreID,  TenderEntry.TransactionNumber),0),  ACCTAMNT =  ISNULL((SELECT   SUM(Amount)  FROM ' + @lTenderEntryTable + ',' + @lTenderTable + ' WHERE ' + @iRMCashReceiptsTempTable + '.RmsStoreID = TenderEntry.StoreID  AND ' + @iRMCashReceiptsTempTable + '.RmsOrderHistoryID = TenderEntry.OrderHistoryID  AND  TenderEntry.TenderID = Tender.[ID]  AND  Tender.AdditionalDetailType = 4  GROUP BY  TenderEntry.StoreID,  TenderEntry.TransactionNumber),0)')

