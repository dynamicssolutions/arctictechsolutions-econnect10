

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsRMApplyWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsRMApplyWrapper]
  
create procedure [dbo].[RmsRMApplyWrapper]
                @iAppFromDocNum  char(20),
                @iAppFromDocType integer,
                @iAppToDocNum    char(20),
                @iAppToDocType   integer,
                @iAppToAmnt      numeric(19,5),
                @iApplyDate      datetime,
                @oErrorState     int  output,
                @oErrString      varchar(255)  output   /*with encryption*/
AS
  EXEC RmsRMApply
    @I_vAPTODCNM = @iAppToDocNum ,
    @I_vAPFRDCNM = @iAppFromDocNum ,
    @I_vAPPTOAMT = @iAppToAmnt ,
    @I_vAPFRDCTY = @iAppFromDocType ,
    @I_vAPTODCTY = @iAppToDocType ,
    @I_vAPPLYDATE = @iApplyDate ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

