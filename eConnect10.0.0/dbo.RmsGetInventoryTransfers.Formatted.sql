

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetInventoryTransfers]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetInventoryTransfers]
  
create procedure [dbo].[RmsGetInventoryTransfers]
                @iInventoryTransferTempTable VARCHAR(255),
                @iStartDate                  datetime,
                @iEndDate                    datetime,
                @iRmsServerName              VARCHAR(255),
                @iHqDbName                   VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lInventoryTransferLogTable VARCHAR(255)
  
  DECLARE  @lItemTable VARCHAR(255)
  
  SELECT @lInventoryTransferLogTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.InventoryTransferLog as InventoryTransferLog'
  
  SELECT @lItemTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Item as Item'
  
  EXEC( 'INSERT INTO ' + @iInventoryTransferTempTable + '(RmsInventoryTransferID,  RmsStoreID,  QTYRECVD,  COSTAMNT,  ITEMNMBR,  DATE1)  SELECT  InventoryTransferLog.[ID],  InventoryTransferLog.StoreID,  InventoryTransferLog.Quantity,  InventoryTransferLog.Cost,  UPPER(Item.ItemLookupCode),  CONVERT(datetime,CONVERT(VARCHAR,InventoryTransferLog.DateTransferred,111))  FROM ' + @lInventoryTransferLogTable + ', ' + @lItemTable + ' WHERE  (InventoryTransferLog.Type = 5  OR   InventoryTransferLog.Type = 6  OR   InventoryTransferLog.Type = 7  OR  InventoryTransferLog.Type = 8  OR   InventoryTransferLog.Type = 9  OR  InventoryTransferLog.Type = 10)  AND  CONVERT(DATETIME,CONVERT(VARCHAR,InventoryTransferLog.DateTransferred,111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + 'AND  InventoryTransferLog.ItemID = Item.[ID]  AND  NOT EXISTS(SELECT  1  FROM  RMS30400   WHERE  InventoryTransferLog.StoreID = RMS30400.RmsStoreID  AND  InventoryTransferLog.[ID] = RMS30400.RmsInventoryTransferID) ')

