

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPATimeSheetHdrInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPATimeSheetHdrInsert]
  
create procedure [dbo].[taPATimeSheetHdrInsert]
                @I_vPATSTYP      smallint,
                @I_vPATSNO       char(17),
                @I_vPADOCDT      datetime,
                @I_vBACHNUMB     char(15),
                @I_vEMPLOYID     char(15),
                @I_vPAREPD       int,
                @I_vPAREPDT      datetime,
                @I_vCURNCYID     char(15),
                @I_vPAREFNO      char(17)  = '',
                @I_vPAreptsuff   char(5)  = '',
                @I_vPACOMM       char(51)  = '',
                @I_vPATQTY       numeric(19,5)  = 0,
                @I_vPAtotcosts   numeric(19,5)  = 0,
                @I_vPATACRV      numeric(19,5)  = 0,
                @I_vXCHGRATE     numeric(19,7)  = 0,
                @I_vRATETPID     char(15)  = '',
                @I_vEXPNDATE     datetime  = '',
                @I_vEXCHDATE     datetime  = '',
                @I_vEXGTBDSC     char(30)  = '',
                @I_vEXTBLSRC     char(50)  = '',
                @I_vRATEEXPR     smallint  = -1,
                @I_vDYSTINCR     smallint  = -1,
                @I_vRATEVARC     numeric(19,7)  = 0,
                @I_vTRXDTDEF     smallint  = -1,
                @I_vRTCLCMTD     smallint  = -1,
                @I_vPRVDSLMT     smallint  = 0,
                @I_vDATELMTS     smallint  = 0,
                @I_vTIME1        datetime  = '',
                @I_vUSERID       char(15)  = '',
                @I_vPDK_TS_No    char(31)  = '',
                @I_vRequesterTrx smallint  = 0,
                @I_vUSRDEFND1    char(50)  = '',
                @I_vUSRDEFND2    char(50)  = '',
                @I_vUSRDEFND3    char(50)  = '',
                @I_vUSRDEFND4    varchar(8000)  = '',
                @I_vUSRDEFND5    varchar(8000)  = '',
                @O_iErrorState   int  output,
                @oErrString      varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iStatus int,  @iCustomState int,  @iCustomErrString varchar(255),  @iError int,  @iCursorError int,  @O_oErrorState int,  @PDK_TS_No char (31),  @PADOCDT datetime,  @PAYR smallint,  @USERID char (15),  @BCHSOURC char (15),  @TRXSORCE char (13),  @PA_Employed_By smallint,  @PAPeriodEndDate datetime,  @PACOMM char (51),  @PAUD1 char (21),  @PAUD2 char (21),  @NOTEINDX numeric (19,5),  @PAreptsuff char (5),  @PAPD datetime,  @LNITMSEQ int,  @PATS_HDR_Errors binary (4),  @PATS_Dist_Errors binary (4),  @CURRNIDX smallint,  @EXGTBLID char (15),  @DENXRATE numeric (19,5),  @MCTRXSTT smallint,  @PAORIGTOTCOSTS numeric (19,5),  @PAORIACCRREV numeric (19,5),  @PDK_Proxy_ID char (15),  @PAsetupkey smallint,  @PAallow_1 int,  @PAallow_2 int,  @PAallow_3 int,  @PAallow_4 int,  @PAallow_5 int,  @PAallow_6 int,  @PAallow_7 int,  @PAallow_8 int,  @PAallow_9 int,  @PAallow_10 int,  @PAreportingperiods smallint,  @PAnumofreportingperiods smallint,  @PA1stdatereportperiod datetime,  @FUNLCURR char(15),  @ISMCTRX int,  @DECPLCUR numeric(19,5),  @iUpdtBthErrState int,  @iUpdDistErrState int,  @iCreateBatchErrString varchar(255),    @PAQtyQ numeric(19,5),  @PATOTCST numeric(19,5),  @PAACREV numeric(19,5),  @PAREPDT datetime,  @sumtotalqty numeric(19,5),  @sumtotalcost numeric(19,5),  @sumttotaloverhead numeric(19,5),  @sumtotalprofit numeric(19,5),  @sumaccruedrevenues numeric(19,5),  @PAPROJNUMBER char(15),  @PACOSTCATID char(15),  @CUSTNMBR char(17),  @PATU int,  @PACONTNUMBER char (11),  @PADT datetime,  @YEAR1 smallint,  @l_Period smallint,  @fClosed smallint,  @nErr smallint,  @userdate datetime,  @UPSTDTFR int,  @RNDDIFF int,  @DBName char(50),     @O_iInitErrorState int,     @oInitErrString varchar(255),    @datediff int,  @PAREPD int,  @yeardiff int,  @origdate datetime,  @sCompanyID int,  @linecount int,  @iGetNextNoteIdxErrState int,  @tempNOTEINDX int,  @iCalculateGLPeriodErrState int,  @PACONTNUMBER_cursor char (11),  @PAPROJNUMBER_cursor char(15),  @PACOSTCATID_cursor char(15),  @PADT_cursor datetime,  @PAQtyQ_cursor numeric(19,5),  @PATOTCST_cursor numeric(19,5),  @PATotalProfit_cursor numeric(19,5),  @PAACREV_cursor numeric(19,5),  @UOMSCHDL_cursor char (11),  @PAUnit_of_Measure_cursor char(9),  @PAbllngtype_cursor smallint,  @PATOTALOVERH_cursor numeric(19,5),  @CUSTNMBR_cursor char(15),  @PATU_cursor smallint,  @PA_Line_Update_Totals_TablesErrorState int,  @temp_year int,  @temp_yearbegindate datetime,  @l_date datetime,  @l_date1 datetime,  @tempdate datetime,  @l_end_of_February int,  @l_days_per_period int,  @l_decimal int,  @l_ctr int,  @l_Suffix char(5),  @l_Suffix1 char(5),  @l_Loop int,  @l_counter int,  @l_counter_length int,  @l_temp_length int,  @count int
  
  select @iStatus = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @iError = 0,
         @iCursorError = 0,
         @O_oErrorState = 0,
         @PDK_TS_No = '',
         @PADOCDT = '',
         @PAYR = 0,
         @USERID = 'eConnect',
         @BCHSOURC = 'PA_TS',
         @TRXSORCE = '',
         @PA_Employed_By = 0,
         @PAPeriodEndDate = '',
         @PACOMM = '',
         @PAUD1 = '',
         @PAUD2 = '',
         @NOTEINDX = 0,
         @PAreptsuff = '',
         @PAPD = '',
         @LNITMSEQ = 0,
         @PATS_HDR_Errors = 0,
         @PATS_Dist_Errors = 0,
         @CURRNIDX = 0,
         @EXGTBLID = '',
         @DENXRATE = 0,
         @MCTRXSTT = 0,
         @PAORIGTOTCOSTS = 0,
         @PAORIACCRREV = 0,
         @PDK_Proxy_ID = '',
         @PAsetupkey = 0,
         @PAallow_1 = 0,
         @PAallow_2 = 0,
         @PAallow_3 = 0,
         @PAallow_4 = 0,
         @PAallow_5 = 0,
         @PAallow_6 = 0,
         @PAallow_7 = 0,
         @PAallow_8 = 0,
         @PAallow_9 = 0,
         @PAallow_10 = 0,
         @PAreportingperiods = 0,
         @PAnumofreportingperiods = 0,
         @PA1stdatereportperiod = '',
         @FUNLCURR = '',
         @ISMCTRX = 0,
         @DECPLCUR = 0,
         @PAQtyQ = 0,
         @PATOTCST = 0,
         @PAACREV = 0,
         @PAREPDT = '',
         @sumtotalqty = 0,
         @sumtotalcost = 0,
         @sumttotaloverhead = 0,
         @sumtotalprofit = 0,
         @sumaccruedrevenues = 0,
         @PAPROJNUMBER = '',
         @PACOSTCATID = '',
         @CUSTNMBR = '',
         @PATU = 0,
         @PACONTNUMBER = '',
         @PADT = '',
         @YEAR1 = 0,
         @l_Period = 0,
         @fClosed = 0,
         @nErr = 0,
         @userdate = '',
         @UPSTDTFR = 0,
         @RNDDIFF = 0,
         @O_iErrorState = 0,
         @DBName = '',
         @O_iInitErrorState = 0,
         @oInitErrString = '',
         @datediff = 0,
         @PAREPD = 0,
         @yeardiff = 0,
         @origdate = '',
         @iUpdtBthErrState = 0,
         @iUpdDistErrState = 0,
         @sCompanyID = 0,
         @linecount = 0,
         @iGetNextNoteIdxErrState = 0,
         @tempNOTEINDX = 0,
         @iCalculateGLPeriodErrState = 0,
         @PACONTNUMBER_cursor = '',
         @PAPROJNUMBER_cursor = '',
         @PACOSTCATID_cursor = '',
         @PADT_cursor = '',
         @PAQtyQ_cursor = 0,
         @PATOTCST_cursor = 0,
         @PATotalProfit_cursor = 0,
         @PAACREV_cursor = 0,
         @UOMSCHDL_cursor = '',
         @PAUnit_of_Measure_cursor = '',
         @PAbllngtype_cursor = 0,
         @PATOTALOVERH_cursor = 0,
         @CUSTNMBR_cursor = '',
         @PATU_cursor = 0,
         @PA_Line_Update_Totals_TablesErrorState = 0,
         @temp_year = 0,
         @temp_yearbegindate = '',
         @l_Suffix = '',
         @l_Suffix1 = '',
         @l_Loop = 0,
         @l_counter = 0,
         @l_counter_length = 0,
         @l_temp_length = 0,
         @count = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taPATimeSheetHdrInsertPre
    @I_vPATSTYP output ,
    @I_vPATSNO output ,
    @I_vPADOCDT output ,
    @I_vBACHNUMB output ,
    @I_vEMPLOYID output ,
    @I_vPAREPD output ,
    @I_vPAREPDT output ,
    @I_vCURNCYID output ,
    @I_vPAREFNO output ,
    @I_vPAreptsuff output ,
    @I_vPACOMM output ,
    @I_vPATQTY output ,
    @I_vPAtotcosts output ,
    @I_vPATACRV output ,
    @I_vXCHGRATE output ,
    @I_vRATETPID output ,
    @I_vEXPNDATE output ,
    @I_vEXCHDATE output ,
    @I_vEXGTBDSC output ,
    @I_vEXTBLSRC output ,
    @I_vRATEEXPR output ,
    @I_vDYSTINCR output ,
    @I_vRATEVARC output ,
    @I_vTRXDTDEF output ,
    @I_vRTCLCMTD output ,
    @I_vPRVDSLMT output ,
    @I_vDATELMTS output ,
    @I_vTIME1 output ,
    @I_vUSERID output ,
    @I_vPDK_TS_No output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1956
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPATSTYP is NULL 
       or @I_vPATSNO is NULL
       or @I_vPADOCDT is NULL
       or @I_vBACHNUMB is NULL
       or @I_vEMPLOYID is NULL
       or @I_vPAREPD is NULL
       or @I_vPAREPDT is NULL
       or @I_vCURNCYID is NULL)
    begin
      select @O_iErrorState = 1957
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPATSTYP = 0
       or @I_vPATSNO = ''
       or @I_vPADOCDT = ''
       or @I_vBACHNUMB = ''
       or @I_vEMPLOYID = ''
       or @I_vPAREPD = ''
       or @I_vPAREPDT = ''
       or @I_vCURNCYID = '')
    begin
      select @O_iErrorState = 1958
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPATACRV < 0)
    begin
      select @O_iErrorState = 1959
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vEMPLOYID = UPPER(@I_vEMPLOYID),
         @I_vCURNCYID = UPPER(@I_vCURNCYID),
         @I_vPATSNO = UPPER(@I_vPATSNO),
         @I_vPAREFNO = UPPER(@I_vPAREFNO)
  
  select @PAsetupkey = isnull(PAsetupkey,0),
         @PAreportingperiods = isnull(PAreportingperiods,0),
         @PAnumofreportingperiods = isnull(PAnumofreportingperiods,0),
         @PA1stdatereportperiod = isnull(PA1stdatereportperiod,0),
         @PAallow_1 = isnull(PAallow_1,0),
         @PAallow_2 = isnull(PAallow_2,0),
         @PAallow_3 = isnull(PAallow_3,0),
         @PAallow_4 = isnull(PAallow_4,0),
         @PAallow_5 = isnull(PAallow_5,0),
         @PAallow_6 = isnull(PAallow_6,0),
         @PAallow_7 = isnull(PAallow_7,0),
         @PAallow_8 = isnull(PAallow_8,0),
         @PAallow_9 = isnull(PAallow_9,0),
         @PAallow_10 = isnull(PAallow_10,0)
  from   PA41801 (nolock)
  
  if (@PAsetupkey = 0)
    begin
      select @O_iErrorState = 1960
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vUSERID = '')
    select @I_vUSERID = 'eConnect'
  
  if ((@I_vPATSTYP < 1)
       or (@I_vPATSTYP > 2))
    begin
      select @O_iErrorState = 1961
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPATSTYP = 1)
    select @I_vPAREFNO = ''
  
  if exists (select 1
             from   PA30101 (nolock)
             where  PATSNO = @I_vPATSNO)
      or exists (select 1
                 from   PA10000 (nolock)
                 where  PATSNO = @I_vPATSNO)
      or exists (select 1
                 from   PA01901 (nolock)
                 where  PADocnumber20 = @I_vPATSNO
                        and PATranType = 1)
    begin
      select @O_iErrorState = 1962
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vEMPLOYID <> ''
    begin
      if not exists (select 1
                     from   UPR00100 (nolock)
                     where  EMPLOYID = @I_vEMPLOYID)
        begin
          select @O_iErrorState = 1963
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if exists (select 1
                 from   UPR00100 (nolock)
                 where  EMPLOYID = @I_vEMPLOYID
                        and INACTIVE = 1)
        begin
          select @O_iErrorState = 6482
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if exists (select 1
                 from   PA10001 (nolock)
                 where  PATSNO = @I_vPATSNO
                        and EMPLOYID <> @I_vEMPLOYID)
        begin
          select @O_iErrorState = 6448
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPATSTYP = 2)
    begin
      if (@I_vPAREFNO = '')
        begin
          select @O_iErrorState = 1964
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if not exists (select 1
                         from   PA30101 (nolock)
                         where  PATSNO = @I_vPAREFNO)
            begin
              select @O_iErrorState = 1965
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  select @PAQtyQ = isnull(sum(PAQtyQ),0),
         @PATOTCST = isnull(sum(PATOTCST),0)
  from   PA10001 (nolock)
  where  PATSNO = @I_vPATSNO
  
  if (@I_vPATQTY = 0)
    select @I_vPATQTY = @PAQtyQ
  
  if (@PAQtyQ <> @I_vPATQTY)
    begin
      select @O_iErrorState = 1967
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAtotcosts < 0)
    begin
      select @O_iErrorState = 1968
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAtotcosts <> 0)
      and (@PATOTCST <> @I_vPAtotcosts))
    begin
      select @O_iErrorState = 1969
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAtotcosts = 0)
    select @I_vPAtotcosts = @PATOTCST
  
  if (@I_vPATACRV < 0)
    begin
      select @O_iErrorState = 1970
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @PAYR = datepart(year,@I_vPAREPDT)
  
  if ((@I_vPAREPD > @PAnumofreportingperiods)
      and (@PAreportingperiods <> 1))
    begin
      select @O_iErrorState = 1972
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@PAreportingperiods = 1)
    begin
      select @temp_year = datediff(year,@PA1stdatereportperiod,@I_vPAREPDT)
      
      select @temp_yearbegindate = DATEADD(year,@temp_year,@PA1stdatereportperiod)
      
      if @temp_yearbegindate > @I_vPAREPDT
        select @temp_yearbegindate = DATEADD(year,-1,@temp_yearbegindate)
      
      select @datediff = datediff(day,@temp_yearbegindate,@I_vPAREPDT)
      
      select @PAREPD = @datediff + 1
      
      SELECT @PAREPDT = DATEADD(day,@datediff,@temp_yearbegindate),
             @PAPeriodEndDate = DATEADD(day,(@datediff),@temp_yearbegindate)
    end
  else
    if (@PAreportingperiods = 2)
      begin
        select @datediff = datediff(day,dateadd(day,-1,@PA1stdatereportperiod),
                                    @I_vPAREPDT)
        
        if @datediff > 7
          begin
            if (@datediff%7 <> 0)
              select @PAREPD = @datediff / 7 + 1
            else
              select @PAREPD = @datediff / 7
          end
        else
          begin
            select @datediff = 1,
                   @PAREPD = 1
          end
        
        SELECT @PAREPDT = DATEADD(week,@PAREPD - 1,@PA1stdatereportperiod),
               @PAPeriodEndDate = DATEADD(day,-1,DATEADD(week,(@PAREPD),@PA1stdatereportperiod)),
               @PAREPD = case 
                           when (@PAREPD > 52)
                                and ((@PAREPD%52) <> 0) then (@PAREPD%52)
                           when (@PAREPD > 52)
                                and ((@PAREPD%52) = 0) then 52
                           else @PAREPD
                         end
      end
    else
      if (@PAreportingperiods = 3)
        begin
          select @datediff = datediff(week,@PA1stdatereportperiod,dateadd(day,1,@I_vPAREPDT)) + 1
          
          if (@datediff%2) <> 0
            select @datediff = @datediff + 1
          
          if ((@datediff / 2)%26) <> 0
            begin
              select @PAREPD = ((@datediff / 2)%26)
            end
          else
            begin
              select @PAREPD = 26
            end
          
          SELECT @PAREPDT = DATEADD(week,@datediff - 2,@PA1stdatereportperiod),
                 @PAPeriodEndDate = DATEADD(day,-1,DATEADD(week,(@datediff),@PA1stdatereportperiod))
        end
      else
        if (@PAreportingperiods = 4)
          begin
            if ((@I_vPAREPD%2) = 0)
              begin
                select @origdate = @PA1stdatereportperiod,
                       @PA1stdatereportperiod = dateadd(day,15,@PA1stdatereportperiod)
                
                select @datediff = (datediff(month,@PA1stdatereportperiod,@I_vPAREPDT)) + 1
                
                select @yeardiff = (@datediff / 12)
                
                if (@datediff%12) = 0
                  begin
                    select @PAREPD = 24
                  end
                else
                  begin
                    select @PAREPD = ((@datediff%12) * 2)
                  end
                
                SELECT @PAREPDT = DATEADD(month,@datediff - 1,@PA1stdatereportperiod),
                       @PAPeriodEndDate = DATEADD(day,-1,DATEADD(month,@datediff,@origdate))
              end
            else
              begin
                select @datediff = (datediff(month,@PA1stdatereportperiod,@I_vPAREPDT)) + 1
                
                select @yeardiff = (@datediff / 12)
                
                if (@datediff%12) = 0
                  begin
                    select @PAREPD = 23
                  end
                else
                  begin
                    select @PAREPD = (@datediff%12) + ((@datediff%12) - 1)
                  end
                
                SELECT @PAREPDT = DATEADD(month,@datediff - 1,@PA1stdatereportperiod),
                       @PAPeriodEndDate = DATEADD(day,14,DATEADD(month,@datediff - 1,@PA1stdatereportperiod))
              end
          end
        else
          if (@PAreportingperiods = 5)
            begin
              select @datediff = datediff(month,@PA1stdatereportperiod,@I_vPAREPDT) + 1
              
              if (@datediff%12) = 0
                begin
                  select @PAREPD = 12
                end
              else
                begin
                  select @PAREPD = @datediff%12
                end
              
              SELECT @PAREPDT = DATEADD(month,@datediff - 1,@PA1stdatereportperiod),
                     @PAPeriodEndDate = DATEADD(day,-1,DATEADD(month,(@datediff),@PA1stdatereportperiod))
            end
          else
            if (@PAreportingperiods = 6)
              begin
                select @datediff = datediff(month,@PA1stdatereportperiod,dateadd(day,1,@I_vPAREPDT))
                
                select @yeardiff = (@datediff / 12)
                
                select @PAREPD = (((@datediff%12) / 2) + 1)
                
                SELECT @PAREPDT = DATEADD(month,(@yeardiff * 12) + ((@PAREPD - 1) * 2),
                                          @PA1stdatereportperiod),
                       @PAPeriodEndDate = DATEADD(day,-1,DATEADD(month,((@yeardiff * 12) + (@PAREPD * 2)),
                                                                 @PA1stdatereportperiod))
              end
            else
              if (@PAreportingperiods = 7)
                begin
                  select @datediff = datediff(month,@PA1stdatereportperiod,dateadd(day,1,@I_vPAREPDT))
                  
                  select @yeardiff = (@datediff / 12)
                  
                  select @PAREPD = (((@datediff%12) / 3) + 1)
                  
                  SELECT @PAREPDT = DATEADD(month,(@yeardiff * 12) + ((@PAREPD - 1) * 3),
                                            @PA1stdatereportperiod),
                         @PAPeriodEndDate = DATEADD(day,-1,DATEADD(month,((@yeardiff * 12) + ((@PAREPD - 1) * 3) + 3),
                                                                   @PA1stdatereportperiod))
                end
              else
                begin
                  select @l_date = @PA1stdatereportperiod
                  
                  while @l_date <= @I_vPAREPDT
                    select @l_date1 = @l_date,
                           @l_date = DATEADD(year,1,@l_date)
                  
                  if (Month(@l_date1) = 1
                       or Month(@l_date1) = 2)
                    begin
                      select @tempdate = '3/1/' + convert(char(4),year(@l_date1))
                      
                      select @l_end_of_February = datepart(day,dateadd(day,-1,@tempdate))
                    end
                  else
                    begin
                      select @tempdate = '3/1/' + convert(char(4),year(dateadd(year,1,@l_date1)))
                      
                      select @l_end_of_February = datepart(day,dateadd(day,-1,@tempdate))
                    end
                  
                  if @l_end_of_February = 28
                    begin
                      if @PAnumofreportingperiods > 365
                        begin
                          select @l_days_per_period = 1
                        end
                      else
                        begin
                          select @l_days_per_period = (365 / @PAnumofreportingperiods)
                        end
                    end
                  else
                    begin
                      if (@PAnumofreportingperiods > 366)
                        begin
                          select @l_days_per_period = 1
                        end
                      else
                        begin
                          set @l_days_per_period = (366 / @PAnumofreportingperiods)
                        end
                    end
                  
                  select @l_decimal = datediff(day,@l_date1,@I_vPAREPDT)
                  
                  if round((@l_decimal / @l_days_per_period),0) <> (@l_decimal / @l_days_per_period)
                    begin
                      select @O_iErrorState = 9279
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  
                  select @l_date = @l_date1,
                         @l_ctr = 1
                  
                  while (@l_date <> @I_vPAREPDT)
                        and (@l_date < @I_vPAREPDT)
                    select @l_ctr = @l_ctr + 1,
                           @l_date = @l_date + @l_days_per_period
                  
                  if (@l_ctr > @PAnumofreportingperiods)
                      or (@l_date <> @I_vPAREPDT)
                    begin
                      select @O_iErrorState = 9280
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  else
                    begin
                      select @PAREPD = @l_ctr,
                             @PAREPDT = @I_vPAREPDT
                    end
                  
                  if @PAREPD = @PAnumofreportingperiods
                    begin
                      select @PAPeriodEndDate = dateadd(day,-1,dateadd(month,12,@l_date1))
                    end
                  else
                    begin
                      select @PAPeriodEndDate = @PAREPDT + @l_days_per_period - 1
                    end
                end
  
  if ((@PAREPDT <> @I_vPAREPDT)
      and (@I_vPAREPDT <> ''))
    begin
      select @O_iErrorState = 1973
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@PAREPD <> @I_vPAREPD)
    begin
      select @O_iErrorState = 1432
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (exists (select 1
              from   PA10001 (nolock)
              where  PATSNO = @I_vPATSNO
                     and ((PADT < @I_vPAREPDT)
                           or (PADT > @PAPeriodEndDate))))
    begin
      select @O_iErrorState = 1974
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAreptsuff <> '')
    begin
      if exists (select 1
                 from   PA30100 (nolock)
                 where  EMPLOYID = @I_vEMPLOYID
                        and PAREPD = @I_vPAREPD
                        and PAreptsuff = @I_vPAreptsuff
                        and PAYR = @PAYR)
          or exists (select 1
                     from   PA10000 (nolock)
                     where  EMPLOYID = @I_vEMPLOYID
                            and PAREPD = @I_vPAREPD
                            and PAreptsuff = @I_vPAreptsuff
                            and PAYR = @PAYR)
        begin
          select @O_iErrorState = 1332
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      select   top 1 @l_Suffix = PAreptsuff
      from     PA10000 (nolock)
      where    EMPLOYID = @I_vEMPLOYID
               and PAREPD = @I_vPAREPD
               and PAYR = @PAYR
      order by EMPLOYID,
               PAREPD,
               PAYR,
               PAreptsuff desc
      
      select @count = @count + @@rowcount
      
      select   top 1 @l_Suffix1 = PAreptsuff
      from     PA30100 (nolock)
      where    EMPLOYID = @I_vEMPLOYID
               and PAREPD = @I_vPAREPD
               and PAYR = @PAYR
      order by EMPLOYID,
               PAREPD,
               PAYR,
               PAreptsuff desc
      
      select @count = @count + @@rowcount
      
      if @l_Suffix1 > @l_Suffix
        begin
          select @l_Loop = 1,
                 @l_counter = 1
          
          while @l_Loop = 1
            begin
              select @l_counter_length = len(@l_counter)
              
              select @l_temp_length = 5 - @l_counter_length
              
              select @l_Suffix1 = rtrim(substring(@l_Suffix1,1,@l_temp_length)) + str(@l_counter,@l_counter_length)
              
              select @l_counter = @l_counter + 1
              
              if not exists (select 1
                             from   PA30100 (nolock)
                             where  EMPLOYID = @I_vEMPLOYID
                                    and PAREPD = @I_vPAREPD
                                    and PAreptsuff = @l_Suffix1
                                    and PAYR = @PAYR)
                 and not exists (select 1
                                 from   PA10000 (nolock)
                                 where  EMPLOYID = @I_vEMPLOYID
                                        and PAREPD = @I_vPAREPD
                                        and PAreptsuff = @l_Suffix1
                                        and PAYR = @PAYR)
                begin
                  break
                end
              else
                begin
                  continue
                end
            end
          
          select @I_vPAreptsuff = @l_Suffix1
        end
      else
        if (@l_Suffix1 < @l_Suffix)
            or (@count > 0)
          begin
            select @l_Loop = 1,
                   @l_counter = 1
            
            while @l_Loop = 1
              begin
                select @l_counter_length = len(@l_counter)
                
                select @l_temp_length = 5 - @l_counter_length
                
                select @l_Suffix = rtrim(substring(@l_Suffix,1,@l_temp_length)) + str(@l_counter,@l_counter_length)
                
                select @l_counter = @l_counter + 1
                
                if not exists (select 1
                               from   PA30100 (nolock)
                               where  EMPLOYID = @I_vEMPLOYID
                                      and PAREPD = @I_vPAREPD
                                      and PAreptsuff = @l_Suffix
                                      and PAYR = @PAYR)
                   and not exists (select 1
                                   from   PA10000 (nolock)
                                   where  EMPLOYID = @I_vEMPLOYID
                                          and PAREPD = @I_vPAREPD
                                          and PAreptsuff = @l_Suffix
                                          and PAYR = @PAYR)
                  begin
                    break
                  end
                else
                  begin
                    continue
                  end
              end
            
            select @I_vPAreptsuff = @l_Suffix
          end
    end
  
  select @UPSTDTFR = isnull(UPSTDTFR,0)
  from   SY02300 (nolock)
  where  SERIES = 7
         and TRXSOURC = 'Timesheet Entry'
  
  if (@UPSTDTFR = 0)
    begin
      select @PAPD = isnull(GLPOSTDT,'')
      from   SY00500 (nolock)
      where  BCHSOURC = @BCHSOURC
             and BACHNUMB = @I_vBACHNUMB
      
      if (@PAPD = '')
        select @PAPD = @I_vPADOCDT
    end
  else
    begin
      select @PAPD = @I_vPADOCDT
    end
  
  if (@I_vCURNCYID <> '')
    begin
      if not exists (select 1
                     from   DYNAMICS..MC40200 (nolock)
                     where  CURNCYID = @I_vCURNCYID)
        begin
          select @O_iErrorState = 1975
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vRATETPID <> '')
    begin
      if (not exists (select 1
                      from   MC40100 (nolock)
                      where  RATETPID = @I_vRATETPID))
        begin
          select @O_iErrorState = 1976
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPADOCDT <> '')
    begin
      execute @iStatus = glCalculateGLPeriod
        7 ,
        'Timesheet Entry' ,
        @I_vPADOCDT ,
        @I_vPADOCDT ,
        @l_Period output ,
        @fClosed output ,
        @YEAR1 output ,
        @nErr output ,
        @iCalculateGLPeriodErrState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iCalculateGLPeriodErrState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCalculateGLPeriodErrState))
          
          select @O_iErrorState = 6446
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@fClosed = 1)
        begin
          select @O_iErrorState = 6447
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vEXCHDATE <> '')
    begin
      select @l_Period = 0,
             @fClosed = 0,
             @YEAR1 = 0,
             @nErr = 0,
             @iCalculateGLPeriodErrState = 0
      
      execute @iStatus = glCalculateGLPeriod
        7 ,
        'Timesheet Entry' ,
        @I_vEXCHDATE ,
        @I_vEXCHDATE ,
        @l_Period output ,
        @fClosed output ,
        @YEAR1 output ,
        @nErr output ,
        @iCalculateGLPeriodErrState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iCalculateGLPeriodErrState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCalculateGLPeriodErrState))
          
          select @O_iErrorState = 6444
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@fClosed = 1)
        begin
          select @O_iErrorState = 6445
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @FUNLCURR = FUNLCURR
  from   MC40000 (nolock)
  
  select @DECPLCUR = DECPLCUR - 1
  from   DYNAMICS..MC40200 (nolock)
  where  CURNCYID = @FUNLCURR
  
  select @PAORIGTOTCOSTS = @I_vPAtotcosts
  
  if (@I_vCURNCYID <> '')
     and (@I_vCURNCYID <> @FUNLCURR)
    begin
      select @ISMCTRX = 1
      
      select @MCTRXSTT = 1
      
      select @CURRNIDX = isnull(CURRNIDX,0)
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @I_vCURNCYID
      
      if (@CURRNIDX = 0)
        begin
          select @O_iErrorState = 1977
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      exec @iStatus = taMCCurrencyValidate
        @I_vMASTERID = @I_vEMPLOYID ,
        @I_vDOCDATE = @I_vPADOCDT ,
        @I_vCURNCYID = @I_vCURNCYID ,
        @I_vEXCHDATE = @I_vEXCHDATE ,
        @I_vEXGTBDSC = @I_vEXGTBDSC ,
        @I_vEXTBLSRC = @I_vEXTBLSRC ,
        @I_vRATEEXPR = @I_vRATEEXPR output ,
        @I_vDYSTINCR = @I_vDYSTINCR output ,
        @I_vRATEVARC = @I_vRATEVARC ,
        @I_vTRXDTDEF = @I_vTRXDTDEF ,
        @I_vPRVDSLMT = @I_vPRVDSLMT ,
        @I_vDATELMTS = @I_vDATELMTS ,
        @I_vMODULE = 2 ,
        @I_vTIME1 = @I_vTIME1 output ,
        @I_vXCHGRATE = @I_vXCHGRATE output ,
        @I_vEXPNDATE = @I_vEXPNDATE output ,
        @I_vRATETPID = @I_vRATETPID output ,
        @I_vRTCLCMTD = @I_vRTCLCMTD output ,
        @I_vEXGTBLID = @EXGTBLID output ,
        @oErrString = @oErrString output ,
        @O_iErrorState = @O_iErrorState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        select @iStatus = @iError
      
      if (@iStatus <> 0)
          or (@O_iErrorState <> 0)
        begin
          select @O_iErrorState = 1978
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 1979
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vEXPNDATE < @I_vPADOCDT)
        begin
          select @O_iErrorState = 6481
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      select @ISMCTRX = 0,
             @I_vRATEEXPR = 0,
             @I_vDYSTINCR = 0,
             @I_vRTCLCMTD = 0
      
      select @CURRNIDX = isnull(FUNCRIDX,0)
      from   MC40000 (nolock)
      where  MC40000.FUNLCURR = @I_vCURNCYID
      
      select @CURRNIDX = isnull(@CURRNIDX,0)
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_Timesheet' ,
        @I_vINDEX1 = @I_vPATSNO ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 0 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2041
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@ISMCTRX = 1)
    begin
      select @RNDDIFF = isnull(RNDDIFF,0)
      from   MC40301 (nolock)
      where  RATETPID = @I_vRATETPID
             and CURNCYID = @I_vCURNCYID
             and EXGTBLID = @EXGTBLID
      
      if (@RNDDIFF = 0)
        begin
          select @RNDDIFF = isnull(RNDDIFF,0)
          from   MC40201 (nolock)
          where  CURNCYID = @I_vCURNCYID
        end
      
      if (@RNDDIFF = 0)
        begin
          select @RNDDIFF = isnull(ACTINDX,0)
          from   SY01100 (nolock)
          where  SERIES = 2
                 and SEQNUMBR = 900
        end
      
      if @RNDDIFF = 0
        begin
          select @O_iErrorState = 741
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if @I_vXCHGRATE = 0
        begin
          select @O_iErrorState = 1980
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if @I_vRTCLCMTD = 0
        begin
          select @I_vPAtotcosts = round(@I_vPAtotcosts * @I_vXCHGRATE,@DECPLCUR)
        end
      else
        if @I_vRTCLCMTD = 1
          begin
            select @I_vPAtotcosts = round(@I_vPAtotcosts / @I_vXCHGRATE,@DECPLCUR)
          end
      
      if (@I_vRTCLCMTD = 0)
        begin
          update PA10001
          set    PAUNITCOST = round(PAUNITCOST * @I_vXCHGRATE,@DECPLCUR),
                 ORUNTCST = PAUNITCOST,
                 PABase_Unit_Cost = round(PABase_Unit_Cost * @I_vXCHGRATE,@DECPLCUR),
                 PAORGBSUNITCST = PABase_Unit_Cost,
                 PAEXTCOST = round(PAEXTCOST * @I_vXCHGRATE,@DECPLCUR),
                 OREXTCST = PAEXTCOST,
                 PATOTCST = round(PATOTCST * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGTOTCOST = PATOTCST,
                 PAOverhead_Amount = round(PAOverhead_Amount * @I_vXCHGRATE,@DECPLCUR),
                 PABaseOvhdCost = round(PABaseOvhdCost * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGOVHDAMT = PAOverhead_Amount,
                 PAORIGBASEOVRHCST = PABaseOvhdCost,
                 PATOTALOVERH = round(PATOTALOVERH * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGTOTOVRHD = PATOTALOVERH,
                 PABILRATE = PABILRATE,
                 PAORIGBILLRATE = round(PABILRATE / @I_vXCHGRATE,@DECPLCUR),
                 PA_Base_Billing_Rate = PA_Base_Billing_Rate,
                 PAORIGBSBILLRTE = round(PA_Base_Billing_Rate / @I_vXCHGRATE,@DECPLCUR),
                 PAACREV = case 
                             when PAMARKPERCENT <> 0 then round(PAACREV * @I_vXCHGRATE,@DECPLCUR)
                             else PAACREV
                           end,
                 PAORIACCRREV = case 
                                  when PAMARKPERCENT = 0 then round(PAACREV / @I_vXCHGRATE,@DECPLCUR)
                                  else PAACREV
                                end,
                 PAProfitAmount = round(PAProfitAmount * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGPROFAMT = PAProfitAmount,
                 RNDDIFF = @RNDDIFF
          where  PATSNO = @I_vPATSNO
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 1981
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        if @I_vRTCLCMTD = 1
          begin
            update PA10001
            set    PAUNITCOST = round(PAUNITCOST / @I_vXCHGRATE,@DECPLCUR),
                   ORUNTCST = PAUNITCOST,
                   PABase_Unit_Cost = round(PABase_Unit_Cost / @I_vXCHGRATE,@DECPLCUR),
                   PAORGBSUNITCST = PABase_Unit_Cost,
                   PAEXTCOST = round(PAEXTCOST / @I_vXCHGRATE,@DECPLCUR),
                   OREXTCST = PAEXTCOST,
                   PATOTCST = round(PATOTCST / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGTOTCOST = PATOTCST,
                   PAOverhead_Amount = round(PAOverhead_Amount / @I_vXCHGRATE,@DECPLCUR),
                   PABaseOvhdCost = round(PABaseOvhdCost / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGOVHDAMT = PAOverhead_Amount,
                   PAORIGBASEOVRHCST = PABaseOvhdCost,
                   PATOTALOVERH = round(PATOTALOVERH / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGTOTOVRHD = PATOTALOVERH,
                   PABILRATE = PABILRATE,
                   PAORIGBILLRATE = round(PABILRATE * @I_vXCHGRATE,@DECPLCUR),
                   PA_Base_Billing_Rate = PA_Base_Billing_Rate,
                   PAORIGBSBILLRTE = round(PA_Base_Billing_Rate * @I_vXCHGRATE,@DECPLCUR),
                   PAACREV = case 
                               when PAMARKPERCENT <> 0 then round(PAACREV / @I_vXCHGRATE,@DECPLCUR)
                               else PAACREV
                             end,
                   PAORIACCRREV = case 
                                    when PAMARKPERCENT = 0 then round(PAACREV * @I_vXCHGRATE,@DECPLCUR)
                                    else PAACREV
                                  end,
                   PAProfitAmount = round(PAProfitAmount / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGPROFAMT = PAProfitAmount,
                   RNDDIFF = @RNDDIFF
            where  PATSNO = @I_vPATSNO
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 1982
                
                exec @iStatus = taUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
                
                return (@O_iErrorState)
              end
          end
      
      update PA10001
      set    PATotalProfit = round(PAACREV - PATOTCST,@DECPLCUR),
             PAORIGTOTPROF = round(PAORIACCRREV - PAORIGTOTCOST,@DECPLCUR)
      where  PATSNO = @I_vPATSNO
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 5419
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @PAORIACCRREV = isnull(sum(PAORIACCRREV),0),
         @PAACREV = isnull(sum(PAACREV),0)
  from   PA10001 (nolock)
  where  PATSNO = @I_vPATSNO
  
  if (@I_vPATACRV = 0)
    select @I_vPATACRV = @PAACREV
  
  if ((@I_vPATACRV <> 0)
      and (@PAACREV <> @I_vPATACRV))
    begin
      select @O_iErrorState = 1971
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taCreateUpdateBatchHeaderRcd
    @I_vBACHNUMB = @I_vBACHNUMB ,
    @I_vSERIES = 7 ,
    @I_vGLPOSTDT = @I_vPADOCDT ,
    @I_vBCHSOURC = 'PA_TS' ,
    @I_vDOCAMT = @I_vPAtotcosts ,
    @I_vORIGIN = 0 ,
    @I_vNUMOFTRX = 1 ,
    @I_vCHEKBKID = '' ,
    @O_iErrorState = @iUpdtBthErrState output ,
    @oErrString = @iCreateBatchErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    select @iStatus = @iError
  
  if (@iStatus <> 0)
      or (@iUpdtBthErrState <> 0)
    begin
      if (@iUpdtBthErrState <> 0)
        begin
          exec @iStatus = taUpdateString
            @iUpdtBthErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 1983
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 c (nolock)
  where  c.INTERID = db_name()
  
  select @linecount = @linecount + 1
  
  exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
    @I_sCompanyID = @sCompanyID ,
    @I_iSQLSessionID = 0 ,
    @I_noteincrement = @linecount ,
    @O_mNoteIndex = @NOTEINDX output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iGetNextNoteIdxErrState <> 0)
       or (@iError <> 0))
    begin
      if @iGetNextNoteIdxErrState <> 0
        select @oErrString = rtrim(@oErrString) + ' ' + @iGetNextNoteIdxErrState
      
      select @O_iErrorState = 5263
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @tempNOTEINDX = @NOTEINDX
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  insert PA01901
        (PATranType,
         DOCNUMBR,
         CUSTNMBR,
         PADOCDT,
         PACOSTOWNER,
         RMDTYPAL,
         PABILLTRXT,
         PADocnumber20,
         DCSTATUS)
  select 1,
         '',
         '',
         @I_vPADOCDT,
         @I_vEMPLOYID,
         0,
         0,
         @I_vPATSNO,
         1
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 1278
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  declare TSLineSummary INSENSITIVE cursor  for
  select PACONTNUMBER,
         PAPROJNUMBER,
         PACOSTCATID,
         PADT,
         PAQtyQ,
         PATOTCST,
         PATotalProfit,
         PAACREV,
         UOMSCHDL,
         PAUnit_of_Measure,
         PAbllngtype,
         PATOTALOVERH
  from   PA10001 (nolock)
  where  PATSNO = @I_vPATSNO
  
  open TSLineSummary
  
  select @iCursorError = @@cursor_rows
  
  if (@iCursorError > 0)
    begin
      fetch next from TSLineSummary
      into @PACONTNUMBER_cursor,
           @PAPROJNUMBER_cursor,
           @PACOSTCATID_cursor,
           @PADT_cursor,
           @PAQtyQ_cursor,
           @PATOTCST_cursor,
           @PATotalProfit_cursor,
           @PAACREV_cursor,
           @UOMSCHDL_cursor,
           @PAUnit_of_Measure_cursor,
           @PAbllngtype_cursor,
           @PATOTALOVERH_cursor
      
      while (@@fetch_status <> -1)
        begin
          if (@@fetch_status = -2)
            begin
              select @O_iErrorState = 7326
              
              break
            end
          
          if (@PAPROJNUMBER_cursor <> '<NONE>')
            begin
              select @PATU_cursor = isnull(PATU,0)
              from   PA01001 (nolock)
              where  PACOSTCATID = @PACOSTCATID_cursor
              
              select @CUSTNMBR_cursor = isnull(CUSTNMBR,'')
              from   PA01201 (nolock)
              where  PAPROJNUMBER = @PAPROJNUMBER_cursor
              
              execute @iStatus = taPA_Line_Update_Totals_Tables
                @I_Cust = @CUSTNMBR_cursor ,
                @I_Contract = @PACONTNUMBER_cursor ,
                @I_Proj = @PAPROJNUMBER_cursor ,
                @I_CostCat = @PACOSTCATID_cursor ,
                @I_Date = @PADT_cursor ,
                @I_Qty = @PAQtyQ_cursor ,
                @IN_Qty_canceled = 0 ,
                @I_Cost = @PATOTCST_cursor ,
                @I_Tax = 0 ,
                @IN_total_backout_amount = 0 ,
                @I_Profit = @PATotalProfit_cursor ,
                @I_Accrued = @PAACREV_cursor ,
                @I_Update_Accrued_Revenues = 1 ,
                @IN_non_iv = 1 ,
                @IN_uofm_schedule = @UOMSCHDL_cursor ,
                @IN_uofm = @PAUnit_of_Measure_cursor ,
                @includePurchTaxInCost = 0 ,
                @I_itemnumber = '' ,
                @PALineItemSeq = 0 ,
                @IN_billingtype = @PAbllngtype_cursor ,
                @PATU = @PATU_cursor ,
                @I_Overhead = @PATOTALOVERH_cursor ,
                @I_err = @PA_Line_Update_Totals_TablesErrorState output
              
              select @iError = @@error
              
              if ((@iStatus <> 0)
                   or (@PA_Line_Update_Totals_TablesErrorState <> 0)
                   or (@iError <> 0))
                begin
                  select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@PA_Line_Update_Totals_TablesErrorState))
                  
                  select @O_iErrorState = 6415
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
          
          fetch next from TSLineSummary
          into @PACONTNUMBER_cursor,
               @PAPROJNUMBER_cursor,
               @PACOSTCATID_cursor,
               @PADT_cursor,
               @PAQtyQ_cursor,
               @PATOTCST_cursor,
               @PATotalProfit_cursor,
               @PAACREV_cursor,
               @UOMSCHDL_cursor,
               @PAUnit_of_Measure_cursor,
               @PAbllngtype_cursor,
               @PATOTALOVERH_cursor
        end
    end
  
  deallocate TSLineSummary
  
  insert into PA10000
             (PATSTYP,
              PATSNO,
              PDK_TS_No,
              PADOCDT,
              PAYR,
              USERID,
              BACHNUMB,
              BCHSOURC,
              TRXSORCE,
              PAREFNO,
              EMPLOYID,
              PA_Employed_By,
              PAREPD,
              PAREPDT,
              PAPeriodEndDate,
              PACOMM,
              PAUD1,
              PAUD2,
              PATQTY,
              PAtotcosts,
              PATACRV,
              NOTEINDX,
              PAreptsuff,
              PAPD,
              LNITMSEQ,
              PATS_HDR_Errors,
              PATS_Dist_Errors,
              CURNCYID,
              CURRNIDX,
              RATETPID,
              EXGTBLID,
              XCHGRATE,
              EXCHDATE,
              TIME1,
              RTCLCMTD,
              DENXRATE,
              MCTRXSTT,
              PAORIGTOTCOSTS,
              PAORIACCRREV,
              PDK_Proxy_ID,
              Correcting_Trx_Type,
              CREATDDT,
              CRUSRID,
              PAORIGINLDOCNUM)
  select @I_vPATSTYP,
         @I_vPATSNO,
         @I_vPDK_TS_No,
         @I_vPADOCDT,
         @PAYR,
         @I_vUSERID,
         @I_vBACHNUMB,
         @BCHSOURC,
         @TRXSORCE,
         @I_vPAREFNO,
         @I_vEMPLOYID,
         @PA_Employed_By,
         @I_vPAREPD,
         @I_vPAREPDT,
         @PAPeriodEndDate,
         @I_vPACOMM,
         @PAUD1,
         @PAUD2,
         @I_vPATQTY,
         @I_vPAtotcosts,
         @I_vPATACRV,
         @NOTEINDX,
         @I_vPAreptsuff,
         @PAPD,
         @LNITMSEQ,
         @PATS_HDR_Errors,
         @PATS_Dist_Errors,
         @I_vCURNCYID,
         @CURRNIDX,
         @I_vRATETPID,
         @EXGTBLID,
         @I_vXCHGRATE,
         @I_vEXCHDATE,
         @I_vTIME1,
         @I_vRTCLCMTD,
         @DENXRATE,
         @MCTRXSTT,
         @PAORIGTOTCOSTS,
         @PAORIACCRREV,
         @PDK_Proxy_ID,
         0,
         '1/1/1900',
         '',
         ''
  
  if @@error <> 0
    begin
      select @O_iErrorState = 1994
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPATSCreateDistributions
    @I_vCURNCYID ,
    @CURRNIDX ,
    @I_vPATSNO ,
    @I_vEMPLOYID ,
    @O_iErrorState output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    select @iStatus = @iError
  
  if (@iStatus <> 0)
      or (@O_iErrorState <> 0)
    begin
      if (@O_iErrorState <> 0)
        begin
          exec @iStatus = taUpdateString
            @iUpdDistErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 1995
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPATimeSheetHdrInsertPost
    @I_vPATSTYP ,
    @I_vPATSNO ,
    @I_vPADOCDT ,
    @I_vBACHNUMB ,
    @I_vEMPLOYID ,
    @I_vPAREPD ,
    @I_vPAREPDT ,
    @I_vCURNCYID ,
    @I_vPAREFNO ,
    @I_vPAreptsuff ,
    @I_vPACOMM ,
    @I_vPATQTY ,
    @I_vPAtotcosts ,
    @I_vPATACRV ,
    @I_vXCHGRATE ,
    @I_vRATETPID ,
    @I_vEXPNDATE ,
    @I_vEXCHDATE ,
    @I_vEXGTBDSC ,
    @I_vEXTBLSRC ,
    @I_vRATEEXPR ,
    @I_vDYSTINCR ,
    @I_vRATEVARC ,
    @I_vTRXDTDEF ,
    @I_vRTCLCMTD ,
    @I_vPRVDSLMT ,
    @I_vDATELMTS ,
    @I_vTIME1 ,
    @I_vUSERID ,
    @I_vPDK_TS_No ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1996
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_Timesheet' ,
        @I_vINDEX1 = @I_vPATSNO ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 1 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2042
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  return (@O_iErrorState)

