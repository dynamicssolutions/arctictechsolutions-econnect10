

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetPOLineItem]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetPOLineItem]
  
create procedure [dbo].[RmsGetPOLineItem]
                @iPOHdrTempTable  VARCHAR(255),
                @iPOLineTempTable VARCHAR(255),
                @iItemNumber      VARCHAR(255),
                @iPOID            INT,
                @iStoreID         INT,
                @iRmsServerName   VARCHAR(255),
                @iHqDbName        VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lPurchaseOrderTable VARCHAR(255)
  
  DECLARE  @lPurchaseOrderEntryTable VARCHAR(255)
  
  DECLARE  @lSupplierTable VARCHAR(255)
  
  DECLARE  @lItemTable VARCHAR(255)
  
  SELECT @lPurchaseOrderTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.PurchaseOrder as PurchaseOrder'
  
  SELECT @lPurchaseOrderEntryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.PurchaseOrderEntry as PurchaseOrderEntry'
  
  SELECT @lItemTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Item as Item'
  
  SELECT @lSupplierTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Supplier as Supplier'
  
  EXEC( 'INSERT INTO ' + @iPOHdrTempTable + ' (RmsPOID,  RmsStoreID,  RmsPONumber,  VENDORID,  DATE1,  FRTAMNT,  RmsCurrencyID,  XCHGRATE)  SELECT  PurchaseOrder.[ID],  PurchaseOrder.StoreID,  PurchaseOrder.PONumber,  ISNULL(LEFT(UPPER(Supplier.Code), 15),''''),  CONVERT(datetime,CONVERT(VARCHAR,PurchaseOrder.DateCreated,111)),  PurchaseOrder.Shipping,  ISNULL(Supplier.CurrencyID,''''),  PurchaseOrder.ExchangeRate  FROM ' + @lPurchaseOrderTable + ' LEFT JOIN ' + @lSupplierTable + ' ON ' + ' PurchaseOrder.SupplierID = Supplier.ID    WHERE  PurchaseOrder.StoreID = ' + @iStoreID + '  AND  PurchaseOrder.[ID] = ' + @iPOID)
  
  EXEC( 'INSERT INTO ' + @iPOLineTempTable + ' (RmsStoreID,  RmsPOID,  RmsTransferLogID,  QTYORDER,  ITEMNMBR,  UNITCOST,  TAXAMNT)  SELECT  PurchaseOrderEntry.StoreID,  PurchaseOrderEntry.PurchaseOrderID,  PurchaseOrderEntry.[ID],  PurchaseOrderEntry.QuantityOrdered,  ISNULL(UPPER(Item.ItemLookupCode),''''),  PurchaseOrderEntry.Price,  PurchaseOrderEntry.QuantityOrdered * PurchaseOrderEntry.Price * (PurchaseOrderEntry.TaxRate / 100)  FROM ' + @lPurchaseOrderEntryTable + ',' + @lItemTable + '   WHERE  Item.[ID] = PurchaseOrderEntry.ItemID  AND  PurchaseOrderEntry.StoreID = ' + @iStoreID + '  AND  PurchaseOrderEntry.PurchaseOrderID = ' + @iPOID + '  AND  Item.ItemLookupCode = ISNULL(UPPER(' + '''' + @iItemNumber + '''' + '), '''')')

