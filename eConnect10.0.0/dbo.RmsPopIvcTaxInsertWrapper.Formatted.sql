

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsPopIvcTaxInsertWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsPopIvcTaxInsertWrapper]
  
create procedure [dbo].[RmsPopIvcTaxInsertWrapper]
                @iPoNum       char(17),
                @iVendID      char(15),
                @iOrd         int,
                @iTaxDetailID char(15),
                @iTaxAmount   numeric(19,5),
                @iTaxPurch    numeric(19,5),
                @iTotalPurch  numeric(19,5),
                @oErrorState  int  output,
                @oErrString   varchar(255)  output   /*with encryption*/
AS
  EXEC RmsPopIvcTaxInsert
    @I_vPONUMBER = @iPoNum ,
    @I_vVENDORID = @iVendID ,
    @I_vORD = @iOrd ,
    @I_vTAXDTLID = @iTaxDetailID ,
    @I_vTAXAMNT = @iTaxAmount ,
    @I_vTAXPURCH = @iTaxPurch ,
    @I_vTOTPURCH = @iTotalPurch ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

