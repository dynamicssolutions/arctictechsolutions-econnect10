

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAMLDistributionInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAMLDistributionInsert]
  
create procedure [dbo].[taPAMLDistributionInsert]
                @I_vCURNCYID     char(15),
                @I_vCURRNIDX     int,
                @I_vORCRDAMT     numeric(19,5),
                @I_vORDBTAMT     numeric(19,5),
                @I_vPAMISCLDOCNO char(17),
                @I_vTRXSORCE     char(13),
                @I_vCNTRLTYP     smallint,
                @I_vCRDTAMNT     numeric(19,5),
                @I_vDEBITAMT     numeric(19,5),
                @I_vDSTINDX      int,
                @I_vDISTTYPE     smallint,
                @I_vDistRef      char(31),
                @I_vUSERID       char(15),
                @I_vPSMISCID     char(15),
                @O_iErrorState   int  = NULL output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @DSTSQNUM int,
           @iStatus  int,
           @iError   int,
           @CURNCYID char(15)
  
  select @O_iErrorState = 0,
         @iStatus = 0,
         @iError = 0,
         @DSTSQNUM = 0
  
  if (@I_vCURNCYID is NULL 
       or @I_vCURRNIDX is NULL
       or @I_vORCRDAMT is NULL
       or @I_vORDBTAMT is NULL
       or @I_vPAMISCLDOCNO is NULL
       or @I_vTRXSORCE is NULL
       or @I_vCNTRLTYP is NULL
       or @I_vCRDTAMNT is NULL
       or @I_vDEBITAMT is NULL
       or @I_vDSTINDX is NULL
       or @I_vDISTTYPE is NULL
       or @I_vDistRef is NULL
       or @I_vUSERID is NULL
       or @I_vPSMISCID is NULL)
    begin
      select @O_iErrorState = 1118
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAMISCLDOCNO = ''
       or @I_vDISTTYPE = 0)
    begin
      select @O_iErrorState = 1119
      
      return (@O_iErrorState)
    end
  
  select @I_vPAMISCLDOCNO = UPPER(@I_vPAMISCLDOCNO),
         @I_vCURNCYID = UPPER(@I_vCURNCYID)
  
  select @DSTSQNUM = isnull(max(DSTSQNUM),0) + 10
  from   PA10203 (nolock)
  where  PAMISCLDOCNO = @I_vPAMISCLDOCNO
  
  if (@DSTSQNUM = 10)
    select @DSTSQNUM = 1
  
  insert into PA10203
             (CURNCYID,
              CURRNIDX,
              ORCRDAMT,
              ORDBTAMT,
              PAMISCLDOCNO,
              TRXSORCE,
              DSTSQNUM,
              CNTRLTYP,
              CRDTAMNT,
              DEBITAMT,
              DSTINDX,
              DISTTYPE,
              DistRef,
              USERID,
              PSMISCID)
  select @I_vCURNCYID,
         @I_vCURRNIDX,
         @I_vORCRDAMT,
         @I_vORDBTAMT,
         @I_vPAMISCLDOCNO,
         @I_vTRXSORCE,
         @DSTSQNUM,
         @I_vCNTRLTYP,
         @I_vCRDTAMNT,
         @I_vDEBITAMT,
         @I_vDSTINDX,
         @I_vDISTTYPE,
         @I_vDistRef,
         @I_vUSERID,
         @I_vPSMISCID
  
  if @@error <> 0
    begin
      select @O_iErrorState = 1120
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

