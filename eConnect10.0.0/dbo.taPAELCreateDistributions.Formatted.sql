

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAELCreateDistributions]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAELCreateDistributions]
  
create procedure [dbo].[taPAELCreateDistributions]
                @I_vCURNCYID   char(15),
                @I_vCURRNIDX   smallint,
                @I_vPAEQLOGNO  char(17),
                @I_vPAEQUIPTID char(15),
                @O_iErrorState int  = NULL output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @O_oErrorState int,
           @cDTYPE        int,
           @cDTAMT        numeric(19,5),
           @cCTAMT        numeric(19,5),
           @iCursorError  int,
           @cINDEX        int,
           @cStatement    varchar(255),
           @iStatus       int,
           @iError        int,
           @ORDTAMT       numeric(19,5),
           @ORCTAMT       numeric(19,5)
  
  select @O_iErrorState = 0,
         @iStatus = 0,
         @O_oErrorState = 0,
         @iCursorError = 0,
         @iError = 0
  
  if (@I_vCURNCYID is NULL 
       or @I_vCURRNIDX is NULL
       or @I_vPAEQLOGNO is NULL
       or @I_vPAEQUIPTID is NULL)
    begin
      select @O_iErrorState = 6005
      
      return
    end
  
  if (@I_vCURNCYID = ''
       or @I_vCURRNIDX = 0
       or @I_vPAEQLOGNO = ''
       or @I_vPAEQUIPTID = '')
    begin
      select @O_iErrorState = 6006
      
      return
    end
  
  select @I_vCURNCYID = UPPER(@I_vCURNCYID),
         @I_vPAEQLOGNO = UPPER(@I_vPAEQLOGNO),
         @I_vPAEQUIPTID = UPPER(@I_vPAEQUIPTID)
  
  delete PA10103
  where  PAEQLOGNO = @I_vPAEQLOGNO
  
  if @@error <> 0
    begin
      select @O_iErrorState = 4641
      
      return
    end
  
  create table #temp (
    DTYPE   int,
    DTAMT   numeric(19,5),
    CTAMT   numeric(19,5),
    ORDTAMT numeric(19,5),
    ORCTAMT numeric(19,5),
    DTINDEX int)
  
  create table #temp1 (
    DTYPE   int,
    DTAMT   numeric(19,5),
    CTAMT   numeric(19,5),
    ORDTAMT numeric(19,5),
    ORCTAMT numeric(19,5),
    DTINDEX int)
  
  insert into #temp
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   1,
           isnull(sum(PATOTCST),0),
           0,
           isnull(sum(PAORIGTOTCOST),0),
           0,
           PACGBWIPIDX
  from     PA10101 (nolock)
  where    PAEQLOGNO = @I_vPAEQLOGNO
           and PACGBWIPIDX <> 0
  group by PACGBWIPIDX
  
  insert into #temp
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   2,
           isnull(sum(PATOTCST),0),
           0,
           isnull(sum(PAORIGTOTCOST),0),
           0,
           PACogs_Idx
  from     PA10101 (nolock)
  where    PAEQLOGNO = @I_vPAEQLOGNO
           and PACogs_Idx <> 0
  group by PACogs_Idx
  
  insert into #temp
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   3,
           0,
           isnull(sum(PAEXTCOST),0),
           0,
           isnull(sum(OREXTCST),0),
           PAContra_Account_IDX
  from     PA10101 (nolock)
  where    PAEQLOGNO = @I_vPAEQLOGNO
           and PAContra_Account_IDX <> 0
  group by PAContra_Account_IDX
  
  insert into #temp
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   4,
           isnull(sum(PAACREV),0),
           0,
           isnull(sum(PAORIACCRREV),0),
           0,
           PAUnbilled_AR_Idx
  from     PA10101 (nolock)
  where    PAEQLOGNO = @I_vPAEQLOGNO
           and PAUnbilled_AR_Idx <> 0
  group by PAUnbilled_AR_Idx
  
  insert into #temp
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   5,
           0,
           isnull(sum(PAACREV),0),
           0,
           isnull(sum(PAORIACCRREV),0),
           PAUnbilled_Proj_Rev_Idx
  from     PA10101 (nolock)
  where    PAEQLOGNO = @I_vPAEQLOGNO
           and PAUnbilled_Proj_Rev_Idx <> 0
  group by PAUnbilled_Proj_Rev_Idx
  
  insert into #temp
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   6,
           0,
           isnull(sum(PATOTALOVERH),0),
           0,
           isnull(sum(PAORIGTOTOVRHD),0),
           PAOverhead_IDX
  from     PA10101 (nolock)
  where    PAEQLOGNO = @I_vPAEQLOGNO
           and PAOverhead_IDX <> 0
  group by PAOverhead_IDX
  
  delete #temp
  where  DTAMT = 0
         and CTAMT = 0
         and ORDTAMT = 0
         and ORCTAMT = 0
  
  declare DistLine INSENSITIVE cursor  for
  select DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX
  from   #temp
  
  open DistLine
  
  select @iCursorError = @@cursor_rows
  
  if (@iCursorError > 0)
    begin
      fetch next from DistLine
      into @cDTYPE,
           @cDTAMT,
           @cCTAMT,
           @ORDTAMT,
           @ORCTAMT,
           @cINDEX
      
      while (@@fetch_status <> -1)
        begin
          if (@@fetch_status = -2)
            begin
              select @O_iErrorState = 6007
              
              break
            end
          
          exec @iStatus = taPAELDistributionInsert
            @I_vCURNCYID ,
            @I_vCURRNIDX ,
            @ORCTAMT ,
            @ORDTAMT ,
            @I_vPAEQLOGNO ,
            '' ,
            0 ,
            @cCTAMT ,
            @cDTAMT ,
            @cINDEX ,
            @cDTYPE ,
            '' ,
            '' ,
            @I_vPAEQUIPTID ,
            @O_oErrorState output
          
          select @iError = @@error
          
          if @iStatus = 0
             and @iError <> 0
            select @iStatus = @iError
          
          if (@iStatus <> 0)
              or (@O_oErrorState <> 0)
            begin
              if (@iStatus <> 0)
                begin
                  select @O_iErrorState = 6008
                end
              else
                begin
                  select @O_iErrorState = @O_oErrorState
                end
              
              deallocate DistLine
              
              return
            end
          
          fetch next from DistLine
          into @cDTYPE,
               @cDTAMT,
               @cCTAMT,
               @ORDTAMT,
               @ORCTAMT,
               @cINDEX
        end
    end
  
  deallocate DistLine
  
  return (@O_iErrorState)

