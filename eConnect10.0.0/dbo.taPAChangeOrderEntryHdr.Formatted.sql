

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAChangeOrderEntryHdr]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAChangeOrderEntryHdr]
  
create procedure [dbo].[taPAChangeOrderEntryHdr]
                @I_vPACHGORDNO    char(17),
                @I_vPACONTNUMBER  char(11),
                @I_vPADOCDT       char(11),
                @I_vPAGBTRKCHG    smallint,
                @I_vPACODESC      char(15)  = '',
                @I_vPACOCUSTNO    char(21)  = '',
                @I_vPACOTYPE      smallint  = 0,
                @I_vPACOSTATUS    smallint  = 1,
                @I_vREQSTDBY      char(21)  = '',
                @I_vPAESTBY       char(21)  = '',
                @I_vPAREVBY       char(15)  = '',
                @I_vPAREVPOSITION char(31)  = '',
                @I_vPAREVREASON   char(31)  = '',
                @I_vPAAPPROVBY    char(15)  = '',
                @I_vDSCRIPTN      char(31)  = '',
                @I_vAPPRVLDT      datetime  = '',
                @I_vRequesterTrx  smallint  = 0,
                @I_vUSRDEFND1     char(50)  = '',
                @I_vUSRDEFND2     char(50)  = '',
                @I_vUSRDEFND3     char(50)  = '',
                @I_vUSRDEFND4     varchar(8000)  = '',
                @I_vUSRDEFND5     varchar(8000)  = '',
                @O_iErrorState    int  output,
                @oErrString       varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iStatus           int,
           @iError            int,
           @iCustomState      int,
           @O_oErrorState     int,
           @PACONTNUMBER      char(11),
           @CUSTNMBR          char(17),
           @PABBeginDate      datetime,
           @PAFBeginDate      datetime,
           @PABEndDate        datetime,
           @PAbegindate       datetime,
           @PAProject_Amount  numeric(19,5),
           @PACOREVSFEEAMT    numeric(19,5),
           @PALSTREVSEDATE    datetime,
           @PAFEndDate        datetime,
           @PAEnDate          datetime,
           @PAREVSBUDGMT      numeric(19,5),
           @LSTUSRED          char(15),
           @PAVARTOTQTY       numeric(19,5),
           @PAPREVBEGINDT     datetime,
           @PACOTOTCOST       numeric(19,5),
           @PAPREVENDDT       datetime,
           @PAVARTOTCOST      numeric(19,5),
           @PACOTOTBILL       numeric(19,5),
           @PAVARTOTBILLINGS  numeric(19,5),
           @APPRVLDT          datetime,
           @PACOTOTQTY        numeric(19,5),
           @PAPREVPROJAMT     numeric(19,5),
           @PAVARPROFAMT      numeric(19,5),
           @PAREVPROJAMT      numeric(19,5),
           @PADOCSTATUS       smallint,
           @PAsequencenumber  int,
           @NOTEINDX          int,
           @NOTEINDX2         int,
           @DBName            char(50),
           @O_iInitErrorState int,
           @oInitErrString    varchar(255)
  
  select @iStatus = 0,
         @iError = 0,
         @iCustomState = 0,
         @O_oErrorState = 0,
         @PACONTNUMBER = '',
         @CUSTNMBR = '',
         @PABBeginDate = '',
         @PAFBeginDate = '',
         @PABEndDate = '',
         @PAbegindate = '',
         @PAProject_Amount = 0,
         @PACOREVSFEEAMT = 0,
         @PALSTREVSEDATE = convert(varchar(12),getdate()),
         @PAFEndDate = '',
         @PAEnDate = '',
         @PAREVSBUDGMT = 0,
         @LSTUSRED = 'eConnect',
         @PAVARTOTQTY = 0,
         @PAPREVBEGINDT = '',
         @PACOTOTCOST = 0,
         @PAPREVENDDT = '',
         @PAVARTOTCOST = 0,
         @PACOTOTBILL = 0,
         @PAVARTOTBILLINGS = 0,
         @APPRVLDT = '',
         @PACOTOTQTY = 0,
         @PAPREVPROJAMT = 0,
         @PAVARPROFAMT = 0,
         @PAREVPROJAMT = 0,
         @PADOCSTATUS = 2,
         @PAsequencenumber = 1,
         @NOTEINDX = 0,
         @NOTEINDX2 = 0,
         @DBName = '',
         @O_iInitErrorState = 0,
         @oInitErrString = '',
         @O_iErrorState = 0
  
  select @DBName = DB_Name()
  
  exec @iStatus = taPAChangeOrderEntryHdrPre
    @I_vPACHGORDNO output ,
    @I_vPACONTNUMBER output ,
    @I_vPADOCDT output ,
    @I_vPAGBTRKCHG output ,
    @I_vPACODESC output ,
    @I_vPACOCUSTNO output ,
    @I_vPACOTYPE output ,
    @I_vPACOSTATUS output ,
    @I_vREQSTDBY output ,
    @I_vPAESTBY output ,
    @I_vPAREVBY output ,
    @I_vPAREVPOSITION output ,
    @I_vPAREVREASON output ,
    @I_vPAAPPROVBY output ,
    @I_vDSCRIPTN output ,
    @I_vAPPRVLDT output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@oErrString))
      
      select @O_iErrorState = 2425
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPACHGORDNO is null 
       or @I_vPACONTNUMBER is null
       or @I_vPADOCDT is null
       or @I_vPAGBTRKCHG is null
       or @I_vPACODESC is null
       or @I_vPACOCUSTNO is null
       or @I_vPACOTYPE is null
       or @I_vPACOSTATUS is null
       or @I_vREQSTDBY is null
       or @I_vPAESTBY is null
       or @I_vPAREVBY is null
       or @I_vPAREVPOSITION is null
       or @I_vPAREVREASON is null
       or @I_vPAAPPROVBY is null
       or @I_vDSCRIPTN is null
       or @I_vAPPRVLDT is null)
    begin
      select @O_iErrorState = 2426
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPACHGORDNO = ''
       or @I_vPACONTNUMBER = '')
    begin
      select @O_iErrorState = 2427
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vPACHGORDNO = upper(@I_vPACHGORDNO),
         @I_vPACONTNUMBER = upper(@I_vPACONTNUMBER)
  
  if (@I_vPACONTNUMBER <> '')
    begin
      select @PACONTNUMBER = PACONTNUMBER,
             @CUSTNMBR = CUSTNMBR,
             @PABBeginDate = PABBeginDate,
             @PABEndDate = PABEndDate,
             @PAFBeginDate = PAFBeginDate,
             @PAFEndDate = PAFEndDate,
             @PAProject_Amount = PAProject_Amount
      from   PA01101 (nolock)
      where  PACONTNUMBER = @I_vPACONTNUMBER
      
      if (@PACONTNUMBER = '')
        begin
          select @O_iErrorState = 2428
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPAGBTRKCHG = 0)
    select @PAbegindate = @PABBeginDate,
           @PAEnDate = @PABEndDate
  else
    select @PAbegindate = @PAFBeginDate,
           @PAEnDate = @PAFEndDate
  
  if (@I_vPACOSTATUS <> 1)
    begin
      select @O_iErrorState = 2429
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPACOTYPE < 1)
      or (@I_vPACOTYPE > 3)
    begin
      select @O_iErrorState = 2430
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAAPPROVBY <> '')
    begin
      if not exists (select 1
                     from   UPR00100 (nolock)
                     where  EMPLOYID = @I_vPAAPPROVBY)
        begin
          select @O_iErrorState = 2431
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@I_vDSCRIPTN = '')
        select @I_vDSCRIPTN = DSCRIPTN
        from   UPR40301 a (nolock)
               join UPR00100 b
                 on a.JOBTITLE = b.JOBTITLE
        where  b.EMPLOYID = @I_vPAAPPROVBY
    end
  else
    begin
      if (@I_vDSCRIPTN <> '')
        begin
          select @O_iErrorState = 2432
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPAREVBY <> '')
    begin
      if not exists (select 1
                     from   UPR00100 (nolock)
                     where  EMPLOYID = @I_vPAREVBY)
        begin
          select @O_iErrorState = 2433
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@I_vPAREVPOSITION = '')
        select @I_vPAREVPOSITION = DSCRIPTN
        from   UPR40301 a (nolock)
               join UPR00100 b
                 on a.JOBTITLE = b.JOBTITLE
        where  b.EMPLOYID = @I_vPAREVBY
    end
  else
    begin
      if (@I_vPAREVPOSITION <> '')
        begin
          select @O_iErrorState = 2434
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @PAREVSBUDGMT = @PAProject_Amount,
         @PAVARTOTQTY = isnull(sum(PAVarianceQty),0),
         @PACOTOTCOST = isnull(sum(PATOTCST),0),
         @PAVARTOTCOST = isnull(sum(PAVARTOTCOST),0),
         @PACOTOTBILL = isnull(sum(PAtotbillings),0),
         @PAVARTOTBILLINGS = isnull(sum(PAVARTOTBILLINGS),0),
         @PACOTOTQTY = isnull(sum(PAQtyQ),0),
         @PAREVSBUDGMT = isnull(sum(PAVARPROJAMT),0)
  from   PA12001 (nolock)
  where  PACHGORDNO = @I_vPACHGORDNO
  
  select @PACOREVSFEEAMT = PAFeeAmount
  from   PA12102 (nolock)
  where  PACHGORDNO = @I_vPACHGORDNO
         and PAFeeType = 4
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  update PA00501
  set    PANOPENDCO = PANOPENDCO + 1,
         PANUMCO = PANUMCO + 1
  where  CUSTNMBR = @CUSTNMBR
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 6615
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_oErrorState)
    end
  
  update PA01141
  set    PANOPENDCO = PANOPENDCO + 1,
         PANUMCO = PANUMCO + 1
  where  PACONTNUMBER = @I_vPACONTNUMBER
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 6616
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_oErrorState)
    end
  
  insert PA02701
        (PACONTNUMBER,
         PACHGORDNO,
         PADOCDT,
         PACOSTATUS,
         PACOTYPE,
         DCSTATUS)
  select @I_vPACONTNUMBER,
         @I_vPACHGORDNO,
         @I_vPADOCDT,
         @I_vPACOSTATUS,
         @I_vPACOTYPE,
         1
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 5393
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  insert into PA12000
             (PACHGORDNO,
              PACREATEADDFLY,
              PACONTNUMBER,
              CUSTNMBR,
              PADOCDT,
              PACODESC,
              PACOCUSTNO,
              PACOTYPE,
              PACOSTATUS,
              REQSTDBY,
              PAESTBY,
              PAREVBY,
              PAREVPOSITION,
              PALSTREVSEDATE,
              LSTUSRED,
              PAPREVBEGINDT,
              PAbegindate,
              PAPREVENDDT,
              PAEnDate,
              PAAPPROVBY,
              DSCRIPTN,
              APPRVLDT,
              PAGBTRKCHG,
              PAREVSBUDGMT,
              PACOREVSFEEAMT,
              PATOTCHGORDAMT,
              PAPREVPROJAMT,
              PAVARPROFAMT,
              PAREVPROJAMT,
              PAProject_Amount,
              PAREVREASON,
              PADOCSTATUS,
              PAsequencenumber,
              NOTEINDX,
              NOTEINDX2,
              PACOTOTCOST,
              PACOTOTBILL,
              PACOTOTQTY,
              PAVARTOTBILLINGS,
              PAVARTOTCOST,
              PAVARTOTQTY)
  select @I_vPACHGORDNO,
         0,
         @I_vPACONTNUMBER,
         @CUSTNMBR,
         @I_vPADOCDT,
         @I_vPACODESC,
         @I_vPACOCUSTNO,
         @I_vPACOTYPE,
         @I_vPACOSTATUS,
         @I_vREQSTDBY,
         @I_vPAESTBY,
         @I_vPAREVBY,
         @I_vPAREVPOSITION,
         @PALSTREVSEDATE,
         @LSTUSRED,
         @PAPREVBEGINDT,
         @PAbegindate,
         @PAPREVENDDT,
         @PAEnDate,
         @I_vPAAPPROVBY,
         @I_vDSCRIPTN,
         @I_vAPPRVLDT,
         @I_vPAGBTRKCHG,
         @PAREVSBUDGMT,
         @PACOREVSFEEAMT,
         @PACOREVSFEEAMT + @PAREVSBUDGMT,
         @PAPREVPROJAMT,
         @PAVARPROFAMT,
         @PAREVPROJAMT,
         @PAProject_Amount,
         @I_vPAREVREASON,
         @PADOCSTATUS,
         @PAsequencenumber,
         @NOTEINDX,
         @NOTEINDX2,
         @PACOTOTCOST,
         @PACOTOTBILL,
         @PACOTOTQTY,
         @PAVARTOTBILLINGS,
         @PAVARTOTCOST,
         @PAVARTOTQTY
  
  if @@error <> 0
    begin
      select @O_iErrorState = 2435
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAChangeOrderEntryHdrPost
    @I_vPACHGORDNO ,
    @I_vPACONTNUMBER ,
    @I_vPADOCDT ,
    @I_vPAGBTRKCHG ,
    @I_vPACODESC ,
    @I_vPACOCUSTNO ,
    @I_vPACOTYPE ,
    @I_vPACOSTATUS ,
    @I_vREQSTDBY ,
    @I_vPAESTBY ,
    @I_vPAREVBY ,
    @I_vPAREVPOSITION ,
    @I_vPAREVREASON ,
    @I_vPAAPPROVBY ,
    @I_vDSCRIPTN ,
    @I_vAPPRVLDT ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@oErrString))
      
      select @O_iErrorState = 2436
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

