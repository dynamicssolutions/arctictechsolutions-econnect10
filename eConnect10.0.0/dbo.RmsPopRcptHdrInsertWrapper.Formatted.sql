

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsPopRcptHdrInsertWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsPopRcptHdrInsertWrapper]
  
create procedure [dbo].[RmsPopRcptHdrInsertWrapper]
                @iPoReceiptNum char(17),
                @iPoType       smallint,
                @iReceiptDate  datetime,
                @iBatchNum     char(15),
                @iVendID       char(15),
                @iCurrencyID   char(15),
                @iExchangeRate numeric(19,7),
                @oErrorState   int  output,
                @oErrString    varchar(255)  output   /*with encryption*/
AS
  EXEC RmsPopRcptHdrInsert
    @I_vPOPRCTNM = @iPoReceiptNum ,
    @I_vPOPTYPE = @iPoType ,
    @I_vreceiptdate = @iReceiptDate ,
    @I_vBACHNUMB = @iBatchNum ,
    @I_vVENDORID = @iVendID ,
    @I_vAUTOCOST = 1 ,
    @I_vSUBTOTAL = 0 ,
    @I_vCURNCYID = @iCurrencyID ,
    @I_vXCHGRATE = @iExchangeRate ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

