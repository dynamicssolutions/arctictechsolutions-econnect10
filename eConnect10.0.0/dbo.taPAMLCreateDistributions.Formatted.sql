

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAMLCreateDistributions]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAMLCreateDistributions]
  
create procedure [dbo].[taPAMLCreateDistributions]
                @I_vCURNCYID     char(15),
                @I_vCURRNIDX     smallint,
                @I_vPAMISCLDOCNO char(17),
                @I_vPSMISCID     char(15),
                @O_iErrorState   int  = NULL output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @O_oErrorState int,
           @cDTYPE        int,
           @cDTAMT        numeric(19,5),
           @cCTAMT        numeric(19,5),
           @iCursorError  int,
           @cINDEX        int,
           @cStatement    varchar(255),
           @iStatus       int,
           @iError        int,
           @ORDTAMT       numeric(19,5),
           @ORCTAMT       numeric(19,5)
  
  select @O_iErrorState = 0,
         @iStatus = 0,
         @O_oErrorState = 0,
         @iCursorError = 0,
         @iError = 0
  
  if (@I_vCURNCYID is NULL 
       or @I_vCURRNIDX is NULL
       or @I_vPAMISCLDOCNO is NULL
       or @I_vPSMISCID is NULL)
    begin
      select @O_iErrorState = 1114
      
      return
    end
  
  if (@I_vCURNCYID = ''
       or @I_vCURRNIDX = 0
       or @I_vPAMISCLDOCNO = ''
       or @I_vPSMISCID = '')
    begin
      select @O_iErrorState = 1115
      
      return
    end
  
  select @I_vCURNCYID = UPPER(@I_vCURNCYID),
         @I_vPAMISCLDOCNO = UPPER(@I_vPAMISCLDOCNO),
         @I_vPSMISCID = UPPER(@I_vPSMISCID)
  
  delete PA10203
  where  PAMISCLDOCNO = @I_vPAMISCLDOCNO
  
  if @@error <> 0
    begin
      select @O_iErrorState = 1116
      
      return
    end
  
  create table #temp (
    DTYPE   int,
    DTAMT   numeric(19,5),
    CTAMT   numeric(19,5),
    ORDTAMT numeric(19,5),
    ORCTAMT numeric(19,5),
    DTINDEX int)
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select   1,
           case 
             when isnull(sum(PATOTCST),0) > 0 then isnull(sum(PATOTCST),0)
             else 0
           end,
           case 
             when isnull(sum(PATOTCST),0) < 0 then isnull(sum(PATOTCST) * -1,0)
             else 0
           end,
           case 
             when isnull(sum(PAORIGTOTCOST),0) > 0 then isnull(sum(PAORIGTOTCOST),0)
             else 0
           end,
           case 
             when isnull(sum(PAORIGTOTCOST),0) < 0 then isnull(sum(PAORIGTOTCOST) * -1,0)
             else 0
           end,
           PACGBWIPIDX
  from     PA10201 (nolock)
  where    PAMISCLDOCNO = @I_vPAMISCLDOCNO
           and PACGBWIPIDX <> 0
  group by PACGBWIPIDX
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select   2,
           case 
             when isnull(sum(PATOTCST),0) > 0 then isnull(sum(PATOTCST),0)
             else 0
           end,
           case 
             when isnull(sum(PATOTCST),0) < 0 then isnull(sum(PATOTCST) * -1,0)
             else 0
           end,
           case 
             when isnull(sum(PAORIGTOTCOST),0) > 0 then isnull(sum(PAORIGTOTCOST),0)
             else 0
           end,
           case 
             when isnull(sum(PAORIGTOTCOST),0) < 0 then isnull(sum(PAORIGTOTCOST) * -1,0)
             else 0
           end,
           PACogs_Idx
  from     PA10201 (nolock)
  where    PAMISCLDOCNO = @I_vPAMISCLDOCNO
           and PACogs_Idx <> 0
  group by PACogs_Idx
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select   3,
           case 
             when isnull(sum(PAEXTCOST),0) < 0 then isnull(sum(PAEXTCOST) * -1,0)
             else 0
           end,
           case 
             when isnull(sum(PAEXTCOST),0) > 0 then isnull(sum(PAEXTCOST),0)
             else 0
           end,
           case 
             when isnull(sum(OREXTCST),0) < 0 then isnull(sum(OREXTCST) * -1,0)
             else 0
           end,
           case 
             when isnull(sum(OREXTCST),0) > 0 then isnull(sum(OREXTCST),0)
             else 0
           end,
           PAContra_Account_IDX
  from     PA10201 (nolock)
  where    PAMISCLDOCNO = @I_vPAMISCLDOCNO
           and PAContra_Account_IDX <> 0
  group by PAContra_Account_IDX
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select   4,
           case 
             when isnull(sum(PAACREV),0) > 0 then isnull(sum(PAACREV),0)
             else 0
           end,
           case 
             when isnull(sum(PAACREV),0) < 0 then isnull(sum(PAACREV) * -1,0)
             else 0
           end,
           case 
             when isnull(sum(PAORIACCRREV),0) > 0 then isnull(sum(PAORIACCRREV),0)
             else 0
           end,
           case 
             when isnull(sum(PAORIACCRREV),0) < 0 then isnull(sum(PAORIACCRREV) * -1,0)
             else 0
           end,
           PAUnbilled_AR_Idx
  from     PA10201 (nolock)
  where    PAMISCLDOCNO = @I_vPAMISCLDOCNO
           and PAUnbilled_AR_Idx <> 0
  group by PAUnbilled_AR_Idx
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select   5,
           case 
             when isnull(sum(PAACREV),0) < 0 then isnull(sum(PAACREV) * -1,0)
             else 0
           end,
           case 
             when isnull(sum(PAACREV),0) > 0 then isnull(sum(PAACREV),0)
             else 0
           end,
           case 
             when isnull(sum(PAORIACCRREV),0) < 0 then isnull(sum(PAORIACCRREV) * -1,0)
             else 0
           end,
           case 
             when isnull(sum(PAORIACCRREV),0) > 0 then isnull(sum(PAORIACCRREV),0)
             else 0
           end,
           PAUnbilled_Proj_Rev_Idx
  from     PA10201 (nolock)
  where    PAMISCLDOCNO = @I_vPAMISCLDOCNO
           and PAUnbilled_Proj_Rev_Idx <> 0
  group by PAUnbilled_Proj_Rev_Idx
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select   6,
           case 
             when isnull(sum(PATOTALOVERH),0) < 0 then isnull(sum(PATOTALOVERH) * -1,0)
             else 0
           end,
           case 
             when isnull(sum(PATOTALOVERH),0) > 0 then isnull(sum(PATOTALOVERH),0)
             else 0
           end,
           case 
             when isnull(sum(PAORIGTOTOVRHD),0) < 0 then isnull(sum(PAORIGTOTOVRHD) * -1,0)
             else 0
           end,
           case 
             when isnull(sum(PAORIGTOTOVRHD),0) > 0 then isnull(sum(PAORIGTOTOVRHD),0)
             else 0
           end,
           PAOverhead_IDX
  from     PA10201 (nolock)
  where    PAMISCLDOCNO = @I_vPAMISCLDOCNO
           and PAOverhead_IDX <> 0
  group by PAOverhead_IDX
  
  delete #temp
  where  DTAMT = 0
         and CTAMT = 0
         and ORDTAMT = 0
         and ORCTAMT = 0
  
  declare DistLine INSENSITIVE cursor  for
  select DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX
  from   #temp
  
  open DistLine
  
  select @iCursorError = @@cursor_rows
  
  if (@iCursorError > 0)
    begin
      fetch next from DistLine
      into @cDTYPE,
           @cDTAMT,
           @cCTAMT,
           @ORDTAMT,
           @ORCTAMT,
           @cINDEX
      
      while (@@fetch_status <> -1)
        begin
          if (@@fetch_status = -2)
            begin
              select @O_iErrorState = 945
              
              break
            end
          
          exec @iStatus = taPAMLDistributionInsert
            @I_vCURNCYID = @I_vCURNCYID ,
            @I_vCURRNIDX = @I_vCURRNIDX ,
            @I_vORCRDAMT = @ORCTAMT ,
            @I_vORDBTAMT = @ORDTAMT ,
            @I_vPAMISCLDOCNO = @I_vPAMISCLDOCNO ,
            @I_vTRXSORCE = '' ,
            @I_vCNTRLTYP = 0 ,
            @I_vCRDTAMNT = @cCTAMT ,
            @I_vDEBITAMT = @cDTAMT ,
            @I_vDSTINDX = @cINDEX ,
            @I_vDISTTYPE = @cDTYPE ,
            @I_vDistRef = '' ,
            @I_vUSERID = '' ,
            @I_vPSMISCID = @I_vPSMISCID ,
            @O_iErrorState = @O_oErrorState output
          
          select @iError = @@error
          
          if @iStatus = 0
             and @iError <> 0
            select @iStatus = @iError
          
          if (@iStatus <> 0)
              or (@O_oErrorState <> 0)
            begin
              if (@iStatus <> 0)
                begin
                  select @O_iErrorState = 1117
                end
              else
                begin
                  select @O_iErrorState = @O_oErrorState
                end
              
              deallocate DistLine
              
              return
            end
          
          fetch next from DistLine
          into @cDTYPE,
               @cDTAMT,
               @cCTAMT,
               @ORDTAMT,
               @ORCTAMT,
               @cINDEX
        end
    end
  
  deallocate DistLine
  
  return (@O_iErrorState)

