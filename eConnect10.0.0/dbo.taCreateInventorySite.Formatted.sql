

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taCreateInventorySite]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taCreateInventorySite]
  
create procedure [dbo].[taCreateInventorySite]
                @I_vLOCNCODE         char(10),
                @I_vLOCNDSCR         char(30),
                @I_vADDRESS1         char(60)  = '',
                @I_vADDRESS2         char(60)  = '',
                @I_vADDRESS3         char(60)  = '',
                @I_vCITY             char(35)  = '',
                @I_vSTATE            char(29)  = '',
                @I_vZIPCODE          char(10)  = '',
                @I_vCOUNTRY          char(60)  = '',
                @I_vPHONE1           char(20)  = '',
                @I_vPHONE2           char(20)  = '',
                @I_vPHONE3           char(20)  = '',
                @I_vFAXNUMBR         char(20)  = '',
                @I_vLocation_Segment char(66)  = '',
                @I_vSTAXSCHD         char(15)  = '',
                @I_vPCTAXSCH         char(15)  = '',
                @I_vINCLDDINPLNNNG   tinyint  = 1,
                @I_vCCode            char(6)  = '',
                @I_vUSRDEFND1        char(50)  = '',
                @I_vUSRDEFND2        char(50)  = '',
                @I_vUSRDEFND3        char(50)  = '',
                @I_vUSRDEFND4        varchar(8000)  = '',
                @I_vUSRDEFND5        varchar(8000)  = '',
                @O_iErrorState       int  output,
                @oErrString          varchar(255)  output   /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @NOTEINDX                numeric(19,5),
           @CMPANYID                smallint,
           @ACSGFLOC                tinyint,
           @iGetNextNoteIdxErrState int,
           @iAddCodeErrState        int,
           @iStatus                 int,
           @iCustomState            int,
           @O_oErrorState           int,
           @iCustomErrString        varchar(255),
           @iError                  int
  
  select @CMPANYID = 0,
         @ACSGFLOC = 0,
         @iStatus = 0,
         @iAddCodeErrState = 0,
         @iCustomState = 0,
         @O_oErrorState = 0,
         @O_iErrorState = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taCreateInventorySitePre
    @I_vLOCNCODE output ,
    @I_vLOCNDSCR output ,
    @I_vADDRESS1 output ,
    @I_vADDRESS2 output ,
    @I_vADDRESS3 output ,
    @I_vCITY output ,
    @I_vSTATE output ,
    @I_vZIPCODE output ,
    @I_vCOUNTRY output ,
    @I_vPHONE1 output ,
    @I_vPHONE2 output ,
    @I_vPHONE3 output ,
    @I_vFAXNUMBR output ,
    @I_vLocation_Segment output ,
    @I_vSTAXSCHD output ,
    @I_vPCTAXSCH output ,
    @I_vINCLDDINPLNNNG output ,
    @I_vCCode output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 2236
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vLOCNCODE is null 
       or @I_vLOCNDSCR is null
       or @I_vADDRESS1 is null
       or @I_vADDRESS2 is null
       or @I_vADDRESS3 is null
       or @I_vCITY is null
       or @I_vSTATE is null
       or @I_vZIPCODE is null
       or @I_vCOUNTRY is null
       or @I_vPHONE1 is null
       or @I_vPHONE2 is null
       or @I_vPHONE3 is null
       or @I_vFAXNUMBR is null
       or @I_vLocation_Segment is null
       or @I_vSTAXSCHD is null
       or @I_vPCTAXSCH is null
       or @I_vINCLDDINPLNNNG is null
       or @I_vCCode is NULL
       or @I_vUSRDEFND1 is null
       or @I_vUSRDEFND2 is null
       or @I_vUSRDEFND3 is null
       or @I_vUSRDEFND4 is null
       or @I_vUSRDEFND5 is null)
    begin
      select @O_iErrorState = 2237
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  select @I_vLOCNCODE = UPPER(@I_vLOCNCODE)
  
  select @CMPANYID = CMPANYID
  from   DYNAMICS..SY01500 (nolock)
  where  INTERID = db_name()
  
  exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
    @I_sCompanyID = @CMPANYID ,
    @I_iSQLSessionID = 0 ,
    @I_noteincrement = 1 ,
    @O_mNoteIndex = @NOTEINDX output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iGetNextNoteIdxErrState <> 0)
    begin
      if (@iGetNextNoteIdxErrState <> 0)
        begin
          exec @iStatus = taUpdateString
            @iGetNextNoteIdxErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 2676
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vLOCNCODE = '')
    begin
      select @O_iErrorState = 2238
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vLOCNDSCR = '')
    begin
      select @O_iErrorState = 2239
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  select @ACSGFLOC = ACSGFLOC
  from   IV40100 (nolock)
  
  if (@I_vLocation_Segment <> '')
     and (@ACSGFLOC = 1)
    begin
      if not exists (select 1
                     from   GL40200 (nolock)
                     where  SGMTNUMB = 1
                            and SGMNTID = @I_vLocation_Segment)
        begin
          select @O_iErrorState = 2240
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vLocation_Segment <> '')
     and (@ACSGFLOC = 2)
    begin
      if not exists (select 1
                     from   GL40200 (nolock)
                     where  SGMTNUMB = 2
                            and SGMNTID = @I_vLocation_Segment)
        begin
          select @O_iErrorState = 9553
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vLocation_Segment <> '')
     and (@ACSGFLOC = 3)
    begin
      if not exists (select 1
                     from   GL40200 (nolock)
                     where  SGMTNUMB = 3
                            and SGMNTID = @I_vLocation_Segment)
        begin
          select @O_iErrorState = 9554
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vLocation_Segment <> '')
     and (@ACSGFLOC = 4)
    begin
      if not exists (select 1
                     from   GL40200 (nolock)
                     where  SGMTNUMB = 4
                            and SGMNTID = @I_vLocation_Segment)
        begin
          select @O_iErrorState = 9555
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vLocation_Segment <> '')
     and (@ACSGFLOC = 5)
    begin
      if not exists (select 1
                     from   GL40200 (nolock)
                     where  SGMTNUMB = 5
                            and SGMNTID = @I_vLocation_Segment)
        begin
          select @O_iErrorState = 9556
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vLocation_Segment <> '')
     and (@ACSGFLOC = 6)
    begin
      if not exists (select 1
                     from   GL40200 (nolock)
                     where  SGMTNUMB = 6
                            and SGMNTID = @I_vLocation_Segment)
        begin
          select @O_iErrorState = 9557
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vLocation_Segment <> '')
     and (@ACSGFLOC = 7)
    begin
      if not exists (select 1
                     from   GL40200 (nolock)
                     where  SGMTNUMB = 7
                            and SGMNTID = @I_vLocation_Segment)
        begin
          select @O_iErrorState = 9558
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vLocation_Segment <> '')
     and (@ACSGFLOC = 8)
    begin
      if not exists (select 1
                     from   GL40200 (nolock)
                     where  SGMTNUMB = 8
                            and SGMNTID = @I_vLocation_Segment)
        begin
          select @O_iErrorState = 9559
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vLocation_Segment <> '')
     and (@ACSGFLOC = 9)
    begin
      if not exists (select 1
                     from   GL40200 (nolock)
                     where  SGMTNUMB = 9
                            and SGMNTID = @I_vLocation_Segment)
        begin
          select @O_iErrorState = 9560
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vLocation_Segment <> '')
     and (@ACSGFLOC = 10)
    begin
      if not exists (select 1
                     from   GL40200 (nolock)
                     where  SGMTNUMB = 10
                            and SGMNTID = @I_vLocation_Segment)
        begin
          select @O_iErrorState = 9561
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vSTAXSCHD <> '')
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vSTAXSCHD)
        begin
          select @O_iErrorState = 2241
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPCTAXSCH <> '')
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vPCTAXSCH)
        begin
          select @O_iErrorState = 2242
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vINCLDDINPLNNNG < 0
       or @I_vINCLDDINPLNNNG > 1)
    begin
      select @O_iErrorState = 2686
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vCCode <> '')
    begin
      if not exists (select 1
                     from   VAT10001 (nolock)
                     where  CCode = @I_vCCode)
        begin
          select @O_iErrorState = 5341
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  
  if (@O_iErrorState = 0)
    begin
      if (not exists (select 1
                      from   IV40700 (nolock)
                      where  LOCNCODE = @I_vLOCNCODE))
        begin
          insert into IV40700
                     (LOCNCODE,
                      LOCNDSCR,
                      NOTEINDX,
                      ADDRESS1,
                      ADDRESS2,
                      ADDRESS3,
                      CITY,
                      STATE,
                      ZIPCODE,
                      COUNTRY,
                      PHONE1,
                      PHONE2,
                      PHONE3,
                      FAXNUMBR,
                      Location_Segment,
                      STAXSCHD,
                      PCTAXSCH,
                      INCLDDINPLNNNG,
                      PICKTICKETSITEOPT,
                      BINBREAK,
                      CCode,
                      DECLID)
          select @I_vLOCNCODE,
                 @I_vLOCNDSCR,
                 @NOTEINDX,
                 @I_vADDRESS1,
                 @I_vADDRESS2,
                 @I_vADDRESS3,
                 @I_vCITY,
                 @I_vSTATE,
                 @I_vZIPCODE,
                 @I_vCOUNTRY,
                 @I_vPHONE1,
                 @I_vPHONE2,
                 @I_vPHONE3,
                 @I_vFAXNUMBR,
                 @I_vLocation_Segment,
                 @I_vSTAXSCHD,
                 @I_vPCTAXSCH,
                 @I_vINCLDDINPLNNNG,
                 3,
                 0,
                 @I_vCCode,
                 ''
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 2243
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @iAddCodeErrState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if @O_iErrorState <> 0
    return (@O_iErrorState)
  
  exec @iStatus = taCreateInventorySitePost
    @I_vLOCNCODE ,
    @I_vLOCNDSCR ,
    @I_vADDRESS1 ,
    @I_vADDRESS2 ,
    @I_vADDRESS3 ,
    @I_vCITY ,
    @I_vSTATE ,
    @I_vZIPCODE ,
    @I_vCOUNTRY ,
    @I_vPHONE1 ,
    @I_vPHONE2 ,
    @I_vPHONE3 ,
    @I_vFAXNUMBR ,
    @I_vLocation_Segment ,
    @I_vSTAXSCHD ,
    @I_vPCTAXSCH ,
    @I_vINCLDDINPLNNNG ,
    @I_vCCode ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 2245
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

