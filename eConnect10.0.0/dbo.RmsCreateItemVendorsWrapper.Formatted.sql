

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsCreateItemVendorsWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsCreateItemVendorsWrapper]
  
create procedure [dbo].[RmsCreateItemVendorsWrapper]
                @iItemNum    char(30),
                @iVendID     char(15),
                @iPurchUofM  char(8),
                @oErrorState int  output,
                @oErrString  varchar(255)  output   /*with encryption*/
AS
  EXEC RmsCreateItemVendors
    @I_vITEMNMBR = @iItemNum ,
    @I_vVENDORID = @iVendID ,
    @I_vPRCHSUOM = @iPurchUofM ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

