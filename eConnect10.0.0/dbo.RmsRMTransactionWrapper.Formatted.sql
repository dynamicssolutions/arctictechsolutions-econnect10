

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsRMTransactionWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsRMTransactionWrapper]
  
create procedure [dbo].[RmsRMTransactionWrapper]
                @iRMDTYPAL    smallint,
                @iDocNum      char(17),
                @iDocDate     datetime,
                @iBatchNum    char(15),
                @iGLPostDate  datetime,
                @iCustNum     char(15),
                @iDocAmnt     numeric(19,5),
                @iSlsAmnt     numeric(19,5),
                @iDocDescrip  char(30)  = '',
                @iTaxAmnt     numeric(19,5),
                @iCashAmnt    numeric(19,5),
                @iCkbkIDCash  char(15)  = '',
                @iCashDate    datetime  = '',
                @iDocNumChs   char(20)  = '',
                @iChkAmnt     numeric(19,5),
                @iCkbkIDCk    char(15)  = '',
                @iChkNum      char(20)  = '',
                @iChkDate     datetime  = '',
                @iDocNumChk   char(20)  = '',
                @iCCAmnt      numeric(19,5),
                @iCCName      char(15)  = '',
                @iRcptNumCC   char(20)  = '',
                @iCCDate      datetime  = '',
                @iDocNumCC    char(20)  = '',
                @iBatchCkbkID char(15)  = '',
                @iCostAmount  numeric(19,5),
                @iFreight     numeric(19,5),
                @iCreateDist  smallint  = 1,
                @oErrorState  int  output,
                @oErrString   varchar(255)  output   /*with encryption*/
AS
  EXEC RmsRMTransaction
    @I_vRMDTYPAL = @iRMDTYPAL ,
    @I_vDOCNUMBR = @iDocNum ,
    @I_vDOCDATE = @iDocDate ,
    @I_vBACHNUMB = @iBatchNum ,
    @I_vGLDATE = @iGLPostDate ,
    @I_vCUSTNMBR = @iCustNum ,
    @I_vDOCAMNT = @iDocAmnt ,
    @I_vSLSAMNT = @iSlsAmnt ,
    @I_vDOCDESCR = @iDocDescrip ,
    @I_vTAXAMNT = @iTaxAmnt ,
    @I_vCASHAMNT = @iCashAmnt ,
    @I_vCBKIDCSH = @iCkbkIDCash ,
    @I_vCASHDATE = @iCashDate ,
    @I_vDCNUMCSH = @iDocNumChs ,
    @I_vCHEKAMNT = @iChkAmnt ,
    @I_vCBKIDCHK = @iCkbkIDCk ,
    @I_vCHEKNMBR = @iChkNum ,
    @I_vCHEKDATE = @iChkDate ,
    @I_vDCNUMCHK = @iDocNumChk ,
    @I_vCRCRDAMT = @iCCAmnt ,
    @I_vCRCRDNAM = @iCCName ,
    @I_vRCTNCCRD = @iRcptNumCC ,
    @I_vCRCARDDT = @iCCDate ,
    @I_vDCNUMCRD = @iDocNumCC ,
    @I_vBatchCHEKBKID = @iBatchCkbkID ,
    @I_vCREATEDIST = @iCreateDist ,
    @I_vCOSTAMNT = @iCostAmount ,
    @I_vFRTAMNT = @iFreight ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

