

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGLTransactionHeaderInsertWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGLTransactionHeaderInsertWrapper]
  
create procedure [dbo].[RmsGLTransactionHeaderInsertWrapper]
                @iBatchNum   char(15),
                @iJrnlEntry  int,
                @iReference  char(30),
                @iTrxDate    datetime,
                @iTrxType    smallint,
                @oErrorState int  output,
                @oErrString  varchar(255)  output   /*with encryption*/
AS
  EXEC RmsGLTransactionHeaderInsert
    @I_vBACHNUMB = @iBatchNum ,
    @I_vJRNENTRY = @iJrnlEntry ,
    @I_vREFRENCE = @iReference ,
    @I_vTRXDATE = @iTrxDate ,
    @I_vTRXTYPE = @iTrxType ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

