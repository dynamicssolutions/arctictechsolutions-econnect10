

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsCreateInternetAddressesWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsCreateInternetAddressesWrapper]
  
create procedure [dbo].[RmsCreateInternetAddressesWrapper]
                @iMasterType  char(3),
                @iMasterID    char(30),
                @iAddressCode char(15),
                @iInet1       char(200),
                @iInet2       char(200),
                @oErrorState  int  output,
                @oErrString   varchar(255)  output   /*with encryption*/
AS
  EXEC RmsCreateInternetAddresses
    @I_vMaster_Type = @iMasterType ,
    @I_vMaster_ID = @iMasterID ,
    @I_vADRSCODE = @iAddressCode ,
    @I_vINET1 = @iInet1 ,
    @I_vINET2 = @iInet2 ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

