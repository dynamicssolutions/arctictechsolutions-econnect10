

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGLTransactionLineInsertWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGLTransactionLineInsertWrapper]
  
create procedure [dbo].[RmsGLTransactionLineInsertWrapper]
                @iBatchNum   char(15),
                @iJrnlEntry  int,
                @iAcctIdx    int,
                @iCrdtAmt    numeric(19,5),
                @iDbtAmt     numeric(19,5),
                @oErrorState int  output,
                @oErrString  varchar(255)  output   /*with encryption*/
AS
  IF @iCrdtAmt = 0
     AND @iDbtAmt = 0
    BEGIN
      SELECT @oErrorState = -6
      
      SELECT @oErrString = 'Debit & Credit amounts 0'
      
      RETURN
    END
  
  EXEC RmsGLTransactionLineInsert
    @I_vBACHNUMB = @iBatchNum ,
    @I_vJRNENTRY = @iJrnlEntry ,
    @I_vACTINDX = @iAcctIdx ,
    @I_vCRDTAMNT = @iCrdtAmt ,
    @I_vDEBITAMT = @iDbtAmt ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

