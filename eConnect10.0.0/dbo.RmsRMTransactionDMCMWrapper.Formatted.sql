

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsRMTransactionDMCMWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsRMTransactionDMCMWrapper]
  
create procedure [dbo].[RmsRMTransactionDMCMWrapper]
                @iRMDTYPAL    smallint,
                @iDocNum      char(17),
                @iDocDate     datetime,
                @iBatchNum    char(15),
                @iBatchDate   datetime,
                @iCustNum     char(15),
                @iDocAmnt     numeric(19,5),
                @iSlsAmnt     numeric(19,5),
                @iDocDescrip  char(30)  = '',
                @iBatchCkbkID char(15)  = '',
                @iCreateDist  smallint  = 1,
                @oErrorState  int  output,
                @oErrString   varchar(255)  output   /*with encryption*/
AS
  EXEC RmsRMTransaction
    @I_vRMDTYPAL = @iRMDTYPAL ,
    @I_vDOCNUMBR = @iDocNum ,
    @I_vDOCDATE = @iDocDate ,
    @I_vBACHNUMB = @iBatchNum ,
    @I_vGLDATE = @iBatchDate ,
    @I_vCUSTNMBR = @iCustNum ,
    @I_vDOCAMNT = @iDocAmnt ,
    @I_vSLSAMNT = @iSlsAmnt ,
    @I_vDOCDESCR = @iDocDescrip ,
    @I_vBatchCHEKBKID = @iBatchCkbkID ,
    @I_vCREATEDIST = @iCreateDist ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

