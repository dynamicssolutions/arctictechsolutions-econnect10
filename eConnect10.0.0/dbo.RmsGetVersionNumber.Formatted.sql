

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetVersionNumber]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetVersionNumber]
  
create procedure [dbo].[RmsGetVersionNumber]
                @iVersionTempTable VARCHAR(255),
                @iRmsServerName    VARCHAR(255),
                @iHqDbName         VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lHqVersionHistoryTable VARCHAR(255)
  
  SELECT @lHqVersionHistoryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.HQVersionHistory'
  
  EXEC( 'INSERT INTO ' + @iVersionTempTable + ' (DSCRIPTN)  SELECT  Version   FROM ' + @lHqVersionHistoryTable + ' WHERE   [ID] = (SELECT   MAX([ID])  FROM ' + @lHqVersionHistoryTable + ')')

