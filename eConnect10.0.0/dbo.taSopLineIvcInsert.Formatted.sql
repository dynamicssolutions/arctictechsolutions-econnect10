

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taSopLineIvcInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taSopLineIvcInsert]
  
create procedure [dbo].[taSopLineIvcInsert]
                @I_vSOPTYPE            smallint,
                @I_vSOPNUMBE           char(17),
                @I_vCUSTNMBR           char(15),
                @I_vDOCDATE            datetime,
                @I_vLOCNCODE           char(10)  = '',
                @I_vITEMNMBR           char(30),
                @I_vAutoAssignBin      smallint  = 1,
                @I_vUNITPRCE           numeric(19,5)  = 0,
                @I_vXTNDPRCE           numeric(19,5)  = 0,
                @I_vQUANTITY           numeric(19,5),
                @I_vMRKDNAMT           numeric(19,5)  = null,
                @I_vMRKDNPCT           numeric(19,2)  = null,
                @I_vCOMMNTID           char(15)  = '',
                @I_vCOMMENT_1          char(50)  = '',
                @I_vCOMMENT_2          char(50)  = '',
                @I_vCOMMENT_3          char(50)  = '',
                @I_vCOMMENT_4          char(50)  = '',
                @I_vUNITCOST           numeric(19,5)  = null,
                @I_vPRCLEVEL           char(10)  = '',
                @I_vITEMDESC           char(100)  = '',
                @I_vTAXAMNT            numeric(19,5)  = 0,
                @I_vQTYONHND           numeric(19,5)  = 0,
                @I_vQTYRTRND           numeric(19,5)  = 0,
                @I_vQTYINUSE           numeric(19,5)  = 0,
                @I_vQTYINSVC           numeric(19,5)  = 0,
                @I_vQTYDMGED           numeric(19,5)  = 0,
                @I_vNONINVEN           smallint  = 0,
                @I_vLNITMSEQ           int  = 0,
                @I_vDROPSHIP           smallint  = 0,
                @I_vQTYTBAOR           numeric(19,5)  = 0,
                @I_vDOCID              char(15)  = '',
                @I_vSALSTERR           char(15)  = null,
                @I_vSLPRSNID           char(15)  = null,
                @I_vITMTSHID           char(15)  = null,
                @I_vIVITMTXB           smallint  = 0,
                @I_vTAXSCHID           char(15)  = null,
                @I_vPRSTADCD           char(15)  = null,
                @I_vShipToName         char(64)  = '',
                @I_vCNTCPRSN           char(60)  = '',
                @I_vADDRESS1           char(60)  = '',
                @I_vADDRESS2           char(60)  = '',
                @I_vADDRESS3           char(60)  = '',
                @I_vCITY               char(35)  = '',
                @I_vSTATE              char(29)  = '',
                @I_vZIPCODE            char(10)  = '',
                @I_vCOUNTRY            char(60)  = '',
                @I_vPHONE1             char(14)  = '',
                @I_vPHONE2             char(14)  = '',
                @I_vPHONE3             char(14)  = '',
                @I_vFAXNUMBR           char(14)  = '',
                @I_vEXCEPTIONALDEMAND  tinyint  = 0,
                @I_vReqShipDate        datetime  = '',
                @I_vFUFILDAT           datetime  = '',
                @I_vACTLSHIP           datetime  = '',
                @I_vSHIPMTHD           char(15)  = null,
                @I_vINVINDX            varchar(75)  = '',
                @I_vCSLSINDX           varchar(75)  = '',
                @I_vSLSINDX            varchar(75)  = '',
                @I_vMKDNINDX           varchar(75)  = '',
                @I_vRTNSINDX           varchar(75)  = '',
                @I_vINUSINDX           varchar(75)  = '',
                @I_vINSRINDX           varchar(75)  = '',
                @I_vDMGDINDX           varchar(75)  = '',
                @I_vAUTOALLOCATESERIAL int  = 0,
                @I_vAUTOALLOCATELOT    int  = 0,
                @I_vGPSFOINTEGRATIONID char(30)  = '',
                @I_vINTEGRATIONSOURCE  smallint  = 0,
                @I_vINTEGRATIONID      char(30)  = '',
                @I_vRequesterTrx       smallint  = 0,
                @I_vQTYCANCE           numeric(19,5)  = 0,
                @I_vQTYFULFI           numeric(19,5)  = null,
                @I_vALLOCATE           smallint  = 0,
                @I_vUpdateIfExists     smallint  = 0,
                @I_vRecreateDist       smallint  = 0,
                @I_vQUOTEQTYTOINV      numeric(19,5)  = 0,
                @I_vTOTALQTY           numeric(19,5)  = 0,
                @I_vCMMTTEXT           varchar(500)  = '',
                @I_vKitCompMan         smallint  = 0,
                @I_vDEFPRICING         int  = 0,
                @I_vDEFEXTPRICE        int  = 0,
                @I_vCURNCYID           char(15)  = '',
                @I_vUOFM               char(8)  = '',
                @I_vIncludePromo       smallint  = 0,
                @I_vCKCreditLimit      tinyint  = 0,
                @I_vQtyShrtOpt         smallint  = 2,
                @I_vUSRDEFND1          char(50)  = '',
                @I_vUSRDEFND2          char(50)  = '',
                @I_vUSRDEFND3          char(50)  = '',
                @I_vUSRDEFND4          varchar(8000)  = '',
                @I_vUSRDEFND5          varchar(8000)  = '',
                @O_iErrorState         int  output,
                @oErrString            varchar(255)  output /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @tCURNCYID                  char(15),
           @ROUNDTO                    smallint,
           @ROUNDHOW                   smallint,
           @RNDGAMNT                   numeric(19,5),
           @DECPLCURMINUS1             smallint,
           @CURRCOST                   numeric(19,5),
           @STNDCOST                   numeric(19,5),
           @LISTPRCE                   numeric(19,5),
           @sCompanyID                 smallint,
           @I_vLNITMSEQFREE            int,
           @FREEUOFM                   char(8),
           @FREEITEM                   char(30),
           @MAXQTYFR                   numeric(19,5),
           @piST_DorP                  char(1),
           @poIN_PromFound             integer,
           @poIN_PromType              integer,
           @poDE_PromPrice             decimal(19,5),
           @poST_SchemeCode            char(15),
           @poST_SchemeItemType        char(1),
           @poST_SchemeGroup           char(31),
           @poDE_QtyFrom               decimal(19,5),
           @poDE_QtyTo                 decimal(19,5),
           @poIN_CurrPriceFound        integer,
           @Description                char(50),
           @StartDate                  datetime,
           @EndDate                    datetime,
           @QuantityFree               decimal(19,5),
           @PRCLEVEL                   char(10),
           @PRICMTHD                   smallint,
           @EXPRINST                   smallint,
           @ALLOCATED                  int,
           @LOFSGMNTALL                int,
           @NEW_ACCNT_STRING           varchar(100),
           @MAXSEG                     int,
           @ACTINDX                    int,
           @LOFSGMNTEND                int,
           @Location_Segment           char(67),
           @ACSGFLOC                   smallint,
           @ACCNT_STRING               char(100),
           @LOFSGMNT                   int,
           @NONIVSCH                   char(15),
           @NONIVTXB                   int,
           @ITMTSHID                   char(15),
           @ALLOCABY                   smallint,
           @USPFULPR                   smallint,
           @USEACFRM                   smallint,
           @LOCNCODE                   char(10),
           @LOCNCODECustomer           char(10),     --Bug ID#14518
           
           @LOCNCODEPassed             smallint,    --Bug ID#14518
           @QTYBSUOM                   numeric(19,5),
           @ITMTRKOP                   smallint,
           @DECPLQTY                   smallint,
           @DECPLCUR                   smallint,
           @CMPTITNM                   char(30),
           @sCMPITQTY                  numeric(19,5),
           @QTYTOBO                    numeric(19,5),
           @QTYAVL                     numeric(19,5),
           @QTTYALLOC                  numeric(19,5),
           @ITEMTYPE                   smallint,
           @EXTQTYAL                   numeric(19,5),
           @INVINDX                    int,
           @CSLSINDX                   int,
           @SLSINDX                    int,
           @MKDNINDX                   int,
           @RTNSINDX                   int,
           @INUSINDX                   int,
           @INSRINDX                   int,
           @DMGDINDX                   int,
           @INVINDX_2                  int,
           @CSLSINDX_2                 int,
           @SLSINDX_2                  int,
           @MKDNINDX_2                 int,
           @RTNSINDX_2                 int,
           @INUSINDX_2                 int,
           @INSRINDX_2                 int,
           @DMGDINDX_2                 int,
           @UNITCOST                   numeric(19,5),
           @UOFMBASE                   char(8),
           @UOMSCHDL                   char(10),
           @ITEMDESC                   char(100),
           @dtDEFAULT                  datetime,
           @CURRNIDX                   int,
           @KitComponent               char(30),
           @FUNLCURR                   char(15),
           @FUNDECPLCUR                int,
           @serialstoallocate          numeric(19,5),
           @serialsallocated           numeric(19,5),
           @VCTNMTHD                   smallint,
           @SERLTNUM                   char(20),
           @LOTQTY                     numeric(19,5),
           @LotsToAllocate             numeric(19,5),
           @ENABLEMULTIBIN             smallint,
           @BinFulfillQty              numeric(19,5),
           @BinQtyType                 smallint,
           @iStatus                    int,
           @iCustomState               int,
           @iCustomErrString           varchar(255),
           @iError                     int,
           @iCursorError               int,
           @O_oErrorState              int,
           @O_iCommentMstErrState      int,
           @O_iLineIvcInsrtErrState    int,
           @LineCount                  int,
           @taSopLineDeleteErrState    int,
           @taSopLineDeleteErrString   char(255),
           @exists                     smallint,
           @SopHdrUpdateState          int,
           @SopHdrUpdateErrString      char(255),
           @SOPHdrTbl                  char(18),
           @OInsStatus                 int,
           @DexLockErrorState          int,
           @DexRowID                   int,
           @oExists                    int,
           @mylock                     tinyint,
           @EXTDCOST                   numeric(19,5),
           @QTYPRBAC                   numeric(19,5),
           @QTYPRBOO                   numeric(19,5),
           @QTYPRINV                   numeric(19,5),
           @QTYPRORD                   numeric(19,5),
           @LOTQUANTITY                numeric(19,5),
           @SERIALQUANTITY             numeric(19,5),
           @SHIPTYPE                   smallint,
           @TXSCHSRC                   smallint,
           @SERIALQTY                  numeric(19,5),
           @SOP10201COUNT              smallint,
           @SOP10201COUNTSERIAL        smallint,
           @QTYFULFI                   smallint,
           @USRDEFND5                  varchar(80),
           @CMPITUOM                   char(9),
           @SEQNUMBR                   int,
           @CMPQTYTBAOR                numeric(19,5),
           @CMPQTYCANCE                numeric(19,5),
           @CMPQTYFULFI                numeric(19,5),
           @CMPQUOTEQTYTOINV           numeric(19,5),
           @CMPQTYONHND                numeric(19,5),
           @CMPQTYRTRND                numeric(19,5),
           @CMPQTYINUSE                numeric(19,5),
           @CMPQTYINSVC                numeric(19,5),
           @CMPQTYDMGED                numeric(19,5),
           @CMPQUANTITY                numeric(19,5),
           @iCustomerBalanceErrState   int,
           @taCustomerBalanceErrString varchar(255),
           @OverCreditLimit            tinyint,
           @CRLMTTYP                   smallint,
           @CurrBalance                numeric(19,5),
           @CRLMTAMT                   numeric(19,5),
           @CUSTBLNC                   numeric(19,5),
           @UNPSTDSA                   numeric(19,5),
           @UNPSTOSA1                  numeric(19,5),
           @ONORDAMT                   numeric(19,5),
           @UNPSTDCA                   numeric(19,5),
           @UNPSTOCA1                  numeric(19,5),
           @DEPRECV                    numeric(19,5),
           @REMPRICE                   numeric(19,5),
           @ISMCTRX                    int,
           @NEWDECPL                   smallint,
           @EDITDECPLCUR               int,
           @LOTFULFILLED               numeric(19,5),
           @ALWBKORD                   smallint,
           @TotQtyPassed               smallint,
           @QTYONHND                   numeric(19,5),
           @LowQUANTITY                numeric(19,5),
           @CompQTYBSUOM               numeric(19,5),
           @ITEMFUNCTDEC               int,
           @OverrideBin                smallint,
           @lock                       char(30),
           @QuantityFrom               decimal(19,5),
           @QuantityTo                 decimal(19,5),
           @PriceSheetID               char(15),
           @AvailSERIALQUANTITY        numeric(19,5),
           @Prices_Not_Required_In_Pr  tinyint,
           @VATEnabled                 int,
           @MS_ITEM_1                  int,
           @QTYREMAI                   numeric(19,5),
           @TRDISAMT                   numeric(19,5),
           @TRDISPCT                   numeric(19,5),
           @PYMTRMID                   char(20),
           @DUEDATE                    datetime,
           @DISCDATE                   datetime,
           @CALCKITC                   tinyint,
           @UOMPRICE                   numeric(19,5),
           @DeleteType                 int,
           @SOP10201LOTSUM             numeric(19,5),
           @fieldupdate                tinyint
  
  select @tCURNCYID = '',
         @ROUNDTO = 0,
         @ROUNDHOW = 0,
         @RNDGAMNT = 0,
         @DECPLCURMINUS1 = 0,
         @CURRCOST = 0,
         @STNDCOST = 0,
         @LISTPRCE = 0,
         @I_vLNITMSEQFREE = 0,
         @FREEUOFM = '',
         @FREEITEM = '',
         @MAXQTYFR = 0,
         @piST_DorP = 'C',
         @poIN_PromFound = 0,
         @poIN_PromType = 0,
         @poDE_PromPrice = 0,
         @poST_SchemeCode = '',
         @poST_SchemeItemType = '',
         @poST_SchemeGroup = '',
         @poDE_QtyFrom = 0,
         @poDE_QtyTo = 0,
         @poIN_CurrPriceFound = 0,
         @Description = '',
         @StartDate = '',
         @EndDate = '',
         @QuantityFree = 0,
         @PRCLEVEL = '',
         @PRICMTHD = 0,
         @EXPRINST = 0,
         @ALLOCATED = 0,
         @LOFSGMNTALL = 0,
         @NEW_ACCNT_STRING = '',
         @MAXSEG = 0,
         @ACTINDX = 0,
         @LOFSGMNTEND = 0,
         @LOFSGMNT = 0,
         @ACSGFLOC = 0,
         @ACCNT_STRING = '',
         @Location_Segment = '',
         @NONIVSCH = '',
         @NONIVTXB = 0,
         @ITMTSHID = '',
         @ALLOCABY = 0,
         @USPFULPR = 0,
         @DECPLCUR = 0,
         @DECPLQTY = 0,
         @ITMTRKOP = 0,
         @EXTQTYAL = 0,
         @QTYBSUOM = 1,
         @UOMSCHDL = '',
         @O_oErrorState = 0,
         @CURRNIDX = 0,
         @O_iErrorState = 0,
         @iStatus = 0,
         @O_iCommentMstErrState = 0,
         @O_iLineIvcInsrtErrState = 0,
         @dtDEFAULT = '',
         @QTYTOBO = 0,
         @QTYAVL = 0,
         @QTTYALLOC = 0,
         @LOCNCODE = '',
         @LOCNCODECustomer = '',   --Bug ID#14518
         
         @LOCNCODEPassed = 1,   --Bug ID#14518
         @USEACFRM = 0,
         @INVINDX = 0,
         @CSLSINDX = 0,
         @SLSINDX = 0,
         @MKDNINDX = 0,
         @RTNSINDX = 0,
         @INUSINDX = 0,
         @INSRINDX = 0,
         @DMGDINDX = 0,
         @INVINDX_2 = 0,
         @CSLSINDX_2 = 0,
         @SLSINDX_2 = 0,
         @MKDNINDX_2 = 0,
         @RTNSINDX_2 = 0,
         @INUSINDX_2 = 0,
         @INSRINDX_2 = 0,
         @DMGDINDX_2 = 0,
         @FUNLCURR = '',
         @FUNDECPLCUR = 0,
         @serialstoallocate = 0,
         @serialsallocated = 0,
         @UNITCOST = 0,
         @VCTNMTHD = 0,
         @SERLTNUM = '',
         @LOTQTY = 0,
         @LotsToAllocate = 0,
         @ENABLEMULTIBIN = 0,
         @BinFulfillQty = 0,
         @BinQtyType = 1,
         @LineCount = 0,
         @taSopLineDeleteErrState = 0,
         @taSopLineDeleteErrString = '',
         @exists = 0,
         @SopHdrUpdateState = 0,
         @SopHdrUpdateErrString = '',
         @SOPHdrTbl = '',
         @DexRowID = 0,
         @OInsStatus = 0,
         @DexLockErrorState = 0,
         @DexRowID = 0,
         @oExists = 0,
         @mylock = 0,
         @EXTDCOST = 0,
         @QTYPRBAC = 0,
         @QTYPRBOO = 0,
         @QTYPRINV = 0,
         @QTYPRORD = 0,
         @LOTQUANTITY = 0,
         @SERIALQUANTITY = 0,
         @SHIPTYPE = -1,
         @TXSCHSRC = 0,
         @SERIALQTY = 0,
         @SOP10201COUNT = 0,
         @SOP10201COUNTSERIAL = 0,
         @QTYFULFI = 0,
         @USRDEFND5 = '',
         @SEQNUMBR = 0,
         @CMPQTYTBAOR = 0,
         @CMPQTYCANCE = 0,
         @CMPQTYFULFI = 0,
         @CMPQUOTEQTYTOINV = 0,
         @CMPQTYONHND = 0,
         @CMPQTYRTRND = 0,
         @CMPQTYINUSE = 0,
         @CMPQTYINSVC = 0,
         @CMPQTYDMGED = 0,
         @CMPQUANTITY = 0,
         @iCustomerBalanceErrState = 0,
         @taCustomerBalanceErrString = '',
         @OverCreditLimit = 0,
         @CRLMTTYP = 0,
         @CurrBalance = 0,
         @CRLMTAMT = 0,
         @CUSTBLNC = 0,
         @UNPSTDSA = 0,
         @UNPSTOSA1 = 0,
         @ONORDAMT = 0,
         @UNPSTDCA = 0,
         @UNPSTOCA1 = 0,
         @DEPRECV = 0,
         @REMPRICE = 0,
         @ISMCTRX = 0,
         @NEWDECPL = 0,
         @EDITDECPLCUR = 2,
         @LOTFULFILLED = 0,
         @ALWBKORD = 0,
         @TotQtyPassed = 1,
         @QTYONHND = 0,
         @LowQUANTITY = 0,
         @CompQTYBSUOM = 0,
         @ITEMFUNCTDEC = 0,
         @OverrideBin = 0,
         @lock = '',
         @QuantityFrom = 0,
         @QuantityTo = 0,
         @PriceSheetID = '',
         @AvailSERIALQUANTITY = 0,
         @Prices_Not_Required_In_Pr = 0,
         @VATEnabled = 0,
         @MS_ITEM_1 = power(2,24),
         @QTYREMAI = 0,
         @TRDISPCT = 0,
         @PYMTRMID = '',
         @DUEDATE = '',
         @DISCDATE = '',
         @CALCKITC = 0,
         @UOMPRICE = 0,
         @DeleteType = 0,
         @SOP10201LOTSUM = 0,
         @fieldupdate = 0
  
  if (@oErrString is null)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taSopLineIvcInsertPre
    @I_vSOPTYPE output ,
    @I_vSOPNUMBE output ,
    @I_vCUSTNMBR output ,
    @I_vDOCDATE output ,
    @I_vLOCNCODE output ,
    @I_vITEMNMBR output ,
    @I_vAutoAssignBin output ,
    @I_vUNITPRCE output ,
    @I_vXTNDPRCE output ,
    @I_vQUANTITY output ,
    @I_vMRKDNAMT output ,
    @I_vMRKDNPCT output ,   --Suggestion ID#7111
    @I_vCOMMNTID output ,
    @I_vCOMMENT_1 output ,
    @I_vCOMMENT_2 output ,
    @I_vCOMMENT_3 output ,
    @I_vCOMMENT_4 output ,
    @I_vUNITCOST output ,
    @I_vPRCLEVEL output ,
    @I_vITEMDESC output ,
    @I_vTAXAMNT output ,
    @I_vQTYONHND output ,
    @I_vQTYRTRND output ,
    @I_vQTYINUSE output ,
    @I_vQTYINSVC output ,
    @I_vQTYDMGED output ,
    @I_vNONINVEN output ,
    @I_vLNITMSEQ output ,
    @I_vDROPSHIP output ,
    @I_vQTYTBAOR output ,
    @I_vDOCID output ,
    @I_vSALSTERR output ,
    @I_vSLPRSNID output ,
    @I_vITMTSHID output ,
    @I_vIVITMTXB output ,
    @I_vTAXSCHID output ,
    @I_vPRSTADCD output ,
    @I_vShipToName output ,
    @I_vCNTCPRSN output ,
    @I_vADDRESS1 output ,
    @I_vADDRESS2 output ,
    @I_vADDRESS3 output ,
    @I_vCITY output ,
    @I_vSTATE output ,
    @I_vZIPCODE output ,
    @I_vCOUNTRY output ,
    @I_vPHONE1 output ,
    @I_vPHONE2 output ,
    @I_vPHONE3 output ,
    @I_vFAXNUMBR output ,
    @I_vEXCEPTIONALDEMAND output ,
    @I_vReqShipDate output ,
    @I_vFUFILDAT output ,
    @I_vACTLSHIP output ,
    @I_vSHIPMTHD output ,
    @I_vINVINDX output ,
    @I_vCSLSINDX output ,
    @I_vSLSINDX output ,
    @I_vMKDNINDX output ,
    @I_vRTNSINDX output ,
    @I_vINUSINDX output ,
    @I_vINSRINDX output ,
    @I_vDMGDINDX output ,
    @I_vAUTOALLOCATESERIAL output ,
    @I_vAUTOALLOCATELOT output ,
    @I_vGPSFOINTEGRATIONID output ,
    @I_vINTEGRATIONSOURCE output ,
    @I_vINTEGRATIONID output ,
    @I_vRequesterTrx output ,
    @I_vQTYCANCE output ,
    @I_vQTYFULFI output ,
    @I_vALLOCATE output ,
    @I_vUpdateIfExists output ,
    @I_vRecreateDist output ,
    @I_vQUOTEQTYTOINV output ,
    @I_vTOTALQTY output ,
    @I_vCMMTTEXT output ,
    @I_vKitCompMan output ,
    @I_vDEFPRICING output ,
    @I_vDEFEXTPRICE output ,
    @I_vCURNCYID output ,
    @I_vUOFM output ,
    @I_vIncludePromo output ,
    @I_vCKCreditLimit output ,
    @I_vQtyShrtOpt output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + @iCustomErrString
      
      select @O_iErrorState = 671
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vGPSFOINTEGRATIONID is null 
       or @I_vINTEGRATIONSOURCE is null
       or @I_vINTEGRATIONID is null
       or @I_vIVITMTXB is null
       or @I_vDOCID is null
       or @I_vSOPTYPE is null
       or @I_vSOPNUMBE is null
       or @I_vCUSTNMBR is null
       or @I_vSOPNUMBE is null
       or @I_vLOCNCODE is null
       or @I_vITEMNMBR is null
       or @I_vAutoAssignBin is null
       or @I_vDOCDATE is null
       or @I_vUNITPRCE is null
       or @I_vXTNDPRCE is null
       or @I_vQUANTITY is null
       or @I_vCOMMNTID is null
       or @I_vCOMMENT_1 is null
       or @I_vCOMMENT_2 is null
       or @I_vCOMMENT_3 is null
       or @I_vCOMMENT_4 is null
       or @I_vPRCLEVEL is null
       or @I_vITEMDESC is null
       or @I_vTAXAMNT is null
       or @I_vNONINVEN is null
       or @I_vDROPSHIP is null
       or @I_vLNITMSEQ is null
       or @I_vQTYTBAOR is null
       or @I_vShipToName is null
       or @I_vCNTCPRSN is null
       or @I_vADDRESS1 is null
       or @I_vADDRESS2 is null
       or @I_vADDRESS3 is null
       or @I_vCITY is null
       or @I_vSTATE is null
       or @I_vZIPCODE is null
       or @I_vCOUNTRY is null
       or @I_vPHONE1 is null
       or @I_vPHONE2 is null
       or @I_vPHONE3 is null
       or @I_vFAXNUMBR is null
       or @I_vEXCEPTIONALDEMAND is null
       or @I_vReqShipDate is null
       or @I_vFUFILDAT is null
       or @I_vACTLSHIP is null
       or @I_vINVINDX is null
       or @I_vCSLSINDX is null
       or @I_vSLSINDX is null
       or @I_vMKDNINDX is null
       or @I_vRTNSINDX is null
       or @I_vINUSINDX is null
       or @I_vINSRINDX is null
       or @I_vDMGDINDX is null
       or @I_vRequesterTrx is null
       or @I_vAUTOALLOCATESERIAL is null
       or @I_vAUTOALLOCATELOT is null
       or @I_vUpdateIfExists is null
       or @I_vRecreateDist is null
       or @I_vQTYCANCE is null
       or @I_vALLOCATE is null
       or @I_vQUOTEQTYTOINV is null
       or @I_vTOTALQTY is null
       or @I_vCMMTTEXT is null
       or @I_vDEFPRICING is null
       or @I_vDEFEXTPRICE is null
       or @I_vCURNCYID is null
       or @I_vUOFM is null
       or @I_vIncludePromo is null
       or @I_vCKCreditLimit is null
       or @I_vQtyShrtOpt is null)
    begin
      select @O_iErrorState = 101
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vMRKDNAMT < 0)
       or (@I_vMRKDNPCT < 0)
       or (@I_vUNITCOST < 0)
       or (@I_vQTYONHND < 0)
       or (@I_vQTYRTRND < 0)
       or (@I_vQTYINUSE < 0)
       or (@I_vQTYINSVC < 0)
       or (@I_vQTYDMGED < 0)
       or (@I_vQTYTBAOR < 0)
       or (@I_vQTYCANCE < 0)
       or (@I_vQTYFULFI < 0)
       or (@I_vQUOTEQTYTOINV < 0))
    begin
      select @O_iErrorState = 788
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vSOPNUMBE = UPPER(@I_vSOPNUMBE),
         @I_vCUSTNMBR = UPPER(@I_vCUSTNMBR),
         @I_vLOCNCODE = UPPER(@I_vLOCNCODE),
         @I_vITEMNMBR = UPPER(@I_vITEMNMBR),
         @I_vPRCLEVEL = UPPER(@I_vPRCLEVEL),
         @I_vSALSTERR = UPPER(@I_vSALSTERR),
         @I_vSLPRSNID = UPPER(@I_vSLPRSNID),
         @I_vCOMMNTID = UPPER(@I_vCOMMNTID),
         @I_vDOCID = UPPER(@I_vDOCID),
         @I_vPRSTADCD = UPPER(@I_vPRSTADCD),
         @I_vSHIPMTHD = UPPER(@I_vSHIPMTHD),
         @I_vITMTSHID = UPPER(@I_vITMTSHID),
         @I_vTAXSCHID = UPPER(@I_vTAXSCHID),
         @I_vCURNCYID = UPPER(@I_vCURNCYID)
  
  if (@I_vQTYFULFI is null)
    begin
      select @QTYFULFI = -1
    end
  
  if (@I_vUpdateIfExists = 1
      and @I_vAUTOALLOCATELOT = 2)
    begin
      select @O_iErrorState = 3567
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 (nolock)
  where  INTERID = db_name()
  
  select @EXPRINST = EXPRINST
  from   DYNAMICS..SY05501 (nolock)
  where  CMPANYID = @sCompanyID
  
  select @VATEnabled = abs(sign(isnull(Company_Options,0) & @MS_ITEM_1))
  from   DYNAMICS..SY01500 (nolock)
  where  INTERID = DB_Name()
  
  if ((@I_vSOPTYPE = '')
       or (@I_vSOPNUMBE = '')
       or (@I_vITEMNMBR = ''))
    begin
      select @O_iErrorState = 102
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vSOPTYPE < 1)
       or (@I_vSOPTYPE > 6))
    begin
      select @O_iErrorState = 104
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vSOPTYPE <> 3)
     and (@I_vQUANTITY < 0)
    begin
      select @O_iErrorState = 7470
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vSOPTYPE <> 3)
     and (@I_vXTNDPRCE < 0)
     and (@I_vUNITPRCE > 0)  --Suggestion ID#8345 added and (@I_vUNITPRCE > 0) for neg pricing
    begin
      select @O_iErrorState = 7471
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vSOPTYPE <> 3)
     and (@I_vTAXAMNT < 0)
    begin
      select @O_iErrorState = 7472
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vNONINVEN < 0)
       or (@I_vNONINVEN > 1))
    begin
      select @O_iErrorState = 2611
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vDROPSHIP < 0)
       or (@I_vDROPSHIP > 1))
    begin
      select @O_iErrorState = 2612
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vAUTOALLOCATESERIAL < 0)
       or (@I_vAUTOALLOCATESERIAL > 2))
    begin
      select @O_iErrorState = 2615
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vAUTOALLOCATELOT < 0)
       or (@I_vAUTOALLOCATELOT > 2))
    begin
      select @O_iErrorState = 3908
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vRequesterTrx < 0)
       or (@I_vRequesterTrx > 1))
    begin
      select @O_iErrorState = 2616
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vALLOCATE not in (0,1,99))
    begin
      select @O_iErrorState = 2224
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vUpdateIfExists < 0)
       or (@I_vUpdateIfExists > 1))
    begin
      select @O_iErrorState = 2225
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vRecreateDist < 0)
       or (@I_vRecreateDist > 1))
    begin
      select @O_iErrorState = 2226
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vRecreateDist = 1)
      and (@I_vUpdateIfExists = 0))
    begin
      select @O_iErrorState = 2227
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vCKCreditLimit < 0)
       or (@I_vCKCreditLimit > 1))
    begin
      select @O_iErrorState = 3474
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vIVITMTXB not in (0,1,2,3))
    begin
      select @O_iErrorState = 3409
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vQtyShrtOpt not in (1,2,3,4,
                             5,6))
    begin
      select @O_iErrorState = 6232
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vUNITCOST >= 0)
      and (@I_vSOPTYPE <> 4)
      and (@I_vNONINVEN = 0)
      and (@I_vQUANTITY >= 0))
    begin
      select @O_iErrorState = 3407
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vDOCID <> ''
       and @I_vSOPTYPE <> 6)
      and (not exists (select 1
                       from   SOP40200 (nolock)
                       where  DOCID = @I_vDOCID
                              and SOPTYPE = @I_vSOPTYPE)))  --Suggestion ID#10793 added and @I_vSOPTYPE <> 6) for SOP Fulfillment
    begin
      select @O_iErrorState = 3441
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vSOPTYPE = 6)
      and (not exists (select 1
                       from   SOP40200 (nolock)
                       where  DOCID = @I_vDOCID
                              and SOPTYPE = 3
                              and WORKFLOWENABLED = 1)))  --Suggestion ID#10793 - added this edit check for SOP Fulfillment
    begin
      select @O_iErrorState = 55
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vSOPTYPE <> 1)
      and (@I_vQUOTEQTYTOINV > 0))
    begin
      select @O_iErrorState = 2699
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vKitCompMan = 1)
      and (@I_vLNITMSEQ = 0))
    begin
      select @O_iErrorState = 3425
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vNONINVEN = 1)
    begin
      if (exists (select 1
                  from   IV00101 (nolock)
                  where  ITEMNMBR = @I_vITEMNMBR))
        begin
          select @O_iErrorState = 3771
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @USEACFRM = USEACFRM,
         @Prices_Not_Required_In_Pr = Prices_Not_Required_In_Pr,
         @CALCKITC = CALCKITC
  from   SOP40100 (nolock)
  
  if (@I_vTOTALQTY = 0)
    begin
      select @TotQtyPassed = 0
      
      select @I_vTOTALQTY = @I_vQUANTITY + @I_vQTYCANCE + @I_vQTYTBAOR + @I_vQUOTEQTYTOINV
    end
  
  if ((exists (select 1
               from   SOP10100 (nolock)
               where  SOPTYPE = @I_vSOPTYPE
                      and SOPNUMBE = @I_vSOPNUMBE))
       or (exists (select 1
                   from   SOP30200 (nolock)
                   where  SOPTYPE = @I_vSOPTYPE
                          and SOPNUMBE = @I_vSOPNUMBE)))
    begin
      select @exists = 1
    end
  
  if ((@exists = 1)
      and (@I_vUpdateIfExists = 0))
    begin
      select @O_iErrorState = 2221
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vLOCNCODE = '')
    begin
      select @LOCNCODEPassed = 0     --Bug ID#14518
      
      select @I_vLOCNCODE = LOCNCODE
      from   SOP40100 (nolock)
    end
  
  if (@I_vNONINVEN = 0)
    begin
      select @STNDCOST = STNDCOST,
             @CURRCOST = CURRCOST,
             @PRCLEVEL = PRCLEVEL,
             @PRICMTHD = PRICMTHD,
             @I_vUOFM = case 
                          when @I_vUOFM = '' then SELNGUOM
                          else @I_vUOFM
                        end,
             @UOMSCHDL = UOMSCHDL,
             @ITMTSHID = ITMTSHID,
             @I_vIVITMTXB = case 
                              when @I_vIVITMTXB = 0 then TAXOPTNS
                              else @I_vIVITMTXB
                            end,
             @ITEMDESC = ITEMDESC,
             @UNITCOST = case 
                           when VCTNMTHD in (4,5) then STNDCOST
                           else CURRCOST
                         end,
             @INVINDX = case 
                          when @I_vDROPSHIP = 0 then IVIVINDX
                          else DPSHPIDX
                        end,
             @CSLSINDX = IVCOGSIX,
             @SLSINDX = case 
                          when @I_vSOPTYPE <> 4
                               and @I_vQUANTITY >= 0 then IVSLSIDX
                          else IVSLRNIX
                        end,
             @MKDNINDX = IVSLDSIX,
             @RTNSINDX = IVRETIDX,
             @INUSINDX = IVINUSIX,
             @INSRINDX = IVINSVIX,
             @DMGDINDX = IVDMGIDX,
             @ITEMTYPE = ITEMTYPE,
             @DECPLQTY = DECPLQTY,
             @ITMTRKOP = ITMTRKOP,
             @ALWBKORD = ALWBKORD
      from   IV00101 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
    end
  else
    begin
      select @DECPLQTY = DECPLQTY,
             @DECPLCUR = DECPLCUR,
             @I_vIVITMTXB = case 
                              when @I_vIVITMTXB = 0 then NONIVTXB
                              else @I_vIVITMTXB
                            end
      from   SOP40100 (nolock)
      where  SETUPKEY = 1
      
      if (@I_vUOFM = '')
        begin
          select @I_vUOFM = 'Each'
        end
      
      select @I_vPRCLEVEL = ''
    end
  
  if ((@ITEMTYPE in (4,5,6))
       or (@I_vSOPTYPE = 1))
    begin
      select @I_vQtyShrtOpt = 2
    end
  
  if (@I_vUpdateIfExists = 1)
    begin
      if (@exists = 0)
        begin
          select @O_iErrorState = 2608
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      select @SOPHdrTbl = rtrim(db_name()) + '.dbo.SOP10100'
      
      select @DexRowID = DEX_ROW_ID
      from   SOP10100 (nolock)
      where  SOPTYPE = @I_vSOPTYPE
             and SOPNUMBE = @I_vSOPNUMBE
      
      if (not exists (select 1
                      from   tempdb..DEX_LOCK (nolock)
                      where  table_path_name = @SOPHdrTbl
                             and row_id = @DexRowID
                             and session_id = @@spid))
        begin
          select @mylock = 1
          
          exec @iStatus = DYNAMICS..taDEXLOCKS
            @I_vOperation = 1 ,
            @I_vtable_path_name = @SOPHdrTbl ,
            @I_vrow_id = @DexRowID ,
            @O_oExists = @oExists output ,
            @O_oInsertStatus = @OInsStatus output ,
            @O_iErrorState = @DexLockErrorState output
          
          select @iError = @@error
          
          if ((@OInsStatus <> 1)
               or (@DexLockErrorState <> 0)
               or (@iError <> 0)
               or (@iStatus <> 0))
            begin
              if (@DexLockErrorState <> 0)
                begin
                  select @oErrString = rtrim(@oErrString) + ' ' + @DexLockErrorState
                end
              
              select @O_iErrorState = 2079
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      select @LineCount = count(SOPTYPE)
      from   SOP10200 (nolock)
      where  SOPNUMBE = @I_vSOPNUMBE
             and SOPTYPE = @I_vSOPTYPE
             and ITEMNMBR = @I_vITEMNMBR
      
      if ((@LineCount > 1
           and @I_vLNITMSEQ = 0))
        begin
          select @O_iErrorState = 2609
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (exists (select LNITMSEQ
                  from   SOP10200 (nolock)
                  where  SOPNUMBE = @I_vSOPNUMBE
                         and SOPTYPE = @I_vSOPTYPE
                         and LNITMSEQ = @I_vLNITMSEQ
                         and ITEMNMBR <> @I_vITEMNMBR
                         and CMPNTSEQ = 0))
        begin
          select @O_iErrorState = 1741
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (((@I_vLNITMSEQ = 0)
           and (exists (select 1
                        from   SOP10200 (nolock)
                        where  SOPNUMBE = @I_vSOPNUMBE
                               and SOPTYPE = @I_vSOPTYPE
                               and ITEMNMBR = @I_vITEMNMBR)))
           or (exists (select 1
                       from   SOP10200 (nolock)
                       where  SOPNUMBE = @I_vSOPNUMBE
                              and SOPTYPE = @I_vSOPTYPE
                              and ITEMNMBR = @I_vITEMNMBR
                              and LNITMSEQ = @I_vLNITMSEQ)))
        begin
          if (@I_vLNITMSEQ = 0)
            begin
              select @I_vLNITMSEQ = LNITMSEQ
              from   SOP10200 (nolock)
              where  SOPNUMBE = @I_vSOPNUMBE
                     and SOPTYPE = @I_vSOPTYPE
                     and ITEMNMBR = @I_vITEMNMBR
            end
          
          select @QTYPRBAC = QTYPRBAC,
                 @QTYPRBOO = QTYPRBOO,
                 @QTYPRINV = QTYPRINV,
                 @QTYPRORD = QTYPRORD,
                 @REMPRICE = REMPRICE
          from   SOP10200 (nolock)
          where  SOPNUMBE = @I_vSOPNUMBE
                 and SOPTYPE = @I_vSOPTYPE
                 and LNITMSEQ = @I_vLNITMSEQ
                 and ITEMNMBR = @I_vITEMNMBR
                 and CMPNTSEQ = 0
          
          if (@I_vTOTALQTY < @QTYPRBAC + @QTYPRBOO + @QTYPRINV + @QTYPRORD)
            begin
              select @O_iErrorState = 2262
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if (@QTYPRBAC + @QTYPRBOO + @QTYPRINV + @QTYPRORD + @I_vQTYCANCE + @I_vQUANTITY + @I_vQTYTBAOR + @I_vQUOTEQTYTOINV > @I_vTOTALQTY)
            begin
              select @O_iErrorState = 4777
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if ((@I_vAUTOALLOCATESERIAL in (1,2)
               and @ITMTRKOP = 2)
               or (@I_vAUTOALLOCATELOT in (1,2)
                   and @ITMTRKOP = 3))
            begin
              select @DeleteType = 999
            end
          else
            begin
              select @DeleteType = 1
            end
          
          if ((@I_vAUTOALLOCATESERIAL = 3
               and @ITMTRKOP = 2)
               or (@I_vAUTOALLOCATELOT = 3
                   and @ITMTRKOP = 3))
            begin
              select @O_iErrorState = 1472
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          exec @iStatus = taSopLineDelete
            @I_vSOPTYPE = @I_vSOPTYPE ,
            @I_vSOPNUMBE = @I_vSOPNUMBE ,
            @I_vLNITMSEQ = @I_vLNITMSEQ ,
            @I_vITEMNMBR = @I_vITEMNMBR ,
            @I_vRequesterTrx = 0 ,
            @I_vRecreateDist = @I_vRecreateDist ,
            @I_vDeleteType = @DeleteType ,
            @I_vUSRDEFND1 = '' ,
            @I_vUSRDEFND2 = '' ,
            @I_vUSRDEFND3 = '' ,
            @I_vUSRDEFND4 = '' ,
            @I_vUSRDEFND5 = '' ,
            @O_iErrorState = @taSopLineDeleteErrState output ,
            @oErrString = @taSopLineDeleteErrString output
          
          select @iError = @@error
          
          if ((@iStatus <> 0)
               or (@taSopLineDeleteErrState <> 0)
               or (@iError <> 0))
            begin
              select @oErrString = rtrim(@oErrString) + ' ' + @taSopLineDeleteErrString
              
              select @O_iErrorState = 2610
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
    end
  
  select @I_vSLPRSNID = case 
                          when @I_vSLPRSNID is not null then @I_vSLPRSNID
                          else SLPRSNID
                        end,
         @I_vSALSTERR = case 
                          when @I_vSALSTERR is not null then @I_vSALSTERR
                          else SALSTERR
                        end
  from   RM00101 (nolock)
  where  CUSTNMBR = @I_vCUSTNMBR
  
  select @I_vPRSTADCD = case 
                          when @I_vPRSTADCD is null then PRSTADCD
                          else @I_vPRSTADCD
                        end
  from   RM00101 (nolock)
  where  CUSTNMBR = @I_vCUSTNMBR
  
  select @I_vSHIPMTHD = case 
                          when @I_vSHIPMTHD is not null then @I_vSHIPMTHD
                          else SHIPMTHD
                        end,
         @LOCNCODECustomer = LOCNCODE     --Bug ID#14518
  from   RM00102 (nolock)
  where  CUSTNMBR = @I_vCUSTNMBR
         and ADRSCODE = @I_vPRSTADCD
  
  select @I_vSHIPMTHD = isnull(@I_vSHIPMTHD,'')
  
  --Bug ID#14518 set the input parameter location code to the one off the Ship to Address ID like the front end does if empty leave it alone
  if (@LOCNCODECustomer <> '')
     and (@LOCNCODEPassed = 0)
    begin
      select @I_vLOCNCODE = @LOCNCODECustomer
    end
  
  select @EXTQTYAL = case 
                       when QTYONHND - ATYALLOC > 0 then QTYONHND - ATYALLOC
                       when @I_vQUANTITY < 0 then 0
                       else 0
                     end
  from   IV00102 (nolock)
  where  ITEMNMBR = @I_vITEMNMBR
         and LOCNCODE = @I_vLOCNCODE
  
  if ((@I_vSOPTYPE = 1)
      and (@I_vQTYTBAOR > 0))
    begin
      select @O_iErrorState = 580
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vDOCID <> ''
      and @I_vSOPTYPE <> 6)  --Suggestion ID#10793 added and @I_vSOPTYPE <> 6 for SOP Fulfillment
    select @ALLOCABY = ALLOCABY,
           @USPFULPR = USPFULPR
    from   SOP40200 (nolock)
    where  DOCID = @I_vDOCID
           and SOPTYPE = @I_vSOPTYPE
  else
    select @ALLOCABY = 1,
           @USPFULPR = 0
  
  if (@I_vDOCID <> ''
      and @I_vSOPTYPE = 6)
    begin
      select @ALLOCABY = ALLOCABY,
             @USPFULPR = USPFULPR
      from   SOP40200 (nolock)
      where  DOCID = @I_vDOCID
             and SOPTYPE = 3
    end
  
  if ((@I_vQtyShrtOpt in (1,3,4,5,
                          6))
      and ((@QTYFULFI <> -1)
           and (@I_vQTYFULFI <> 0)))
    begin
      select @O_iErrorState = 2229
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vQTYFULFI > 0)
      and (@I_vALLOCATE = 0)
      and ((@ALLOCABY = 2)
            or (@ALLOCABY = 3)))
    begin
      select @O_iErrorState = 2230
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@QTYFULFI <> -1)
      and ((@I_vSOPTYPE = 1)
            or (@I_vSOPTYPE = 4)
            or (@I_vSOPTYPE = 5)))
    begin
      select @O_iErrorState = 301
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vQTYCANCE > 0)
      and (@I_vSOPTYPE = 4))
    begin
      select @O_iErrorState = 2235
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vQTYCANCE <> 0)
      and (@I_vSOPTYPE = 3)
      and (@I_vQUANTITY < 0))
    begin
      select @O_iErrorState = 5527
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@ALLOCABY = 3)
      and ((@I_vALLOCATE = 1)
            or @I_vQTYFULFI > 0))
    begin
      select @O_iErrorState = 2232
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vQTYFULFI > @I_vTOTALQTY - @I_vQTYCANCE - @I_vQTYTBAOR)
     and (@I_vTOTALQTY > 0)
    begin
      select @O_iErrorState = 2233
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vTOTALQTY) < (@I_vQUANTITY + @I_vQTYCANCE + @I_vQTYTBAOR + @I_vQUOTEQTYTOINV))
    begin
      select @O_iErrorState = 2274
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vSOPTYPE = 4)
      and (@I_vTOTALQTY <> @I_vQUANTITY))
    begin
      select @O_iErrorState = 2275
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vCMMTTEXT <> '')
      and ((@I_vCOMMENT_1 <> '')
            or (@I_vCOMMENT_2 <> '')
            or (@I_vCOMMENT_3 <> '')
            or (@I_vCOMMENT_1 <> '')))
    begin
      select @O_iErrorState = 2273
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vCURNCYID = '')
    begin
      select @I_vCURNCYID = isnull(CURNCYID,'')
      from   RM00101 (nolock)
      where  CUSTNMBR = @I_vCUSTNMBR
      
      if (@I_vCURNCYID <> '')
        begin
          select @CURRNIDX = isnull(CURRNIDX,0)
          from   DYNAMICS..MC40200 (nolock)
          where  CURNCYID = @I_vCURNCYID
        end
      else
        begin
          select @I_vCURNCYID = isnull(FUNLCURR,''),
                 @CURRNIDX = isnull(FUNCRIDX,0)
          from   MC40000 (nolock)
        end
    end
  else
    begin
      select @CURRNIDX = isnull(CURRNIDX,0)
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @I_vCURNCYID
    end
  
  select @FUNLCURR = FUNLCURR
  from   MC40000 (nolock)
  
  if ((@I_vCURNCYID <> '')
      and (@I_vCURNCYID <> @FUNLCURR))
    begin
      select @ISMCTRX = 1
    end
  
  if ((@CURRNIDX = 0)
       or (@I_vCURNCYID = ''))
    begin
      select @O_iErrorState = 3442
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vLNITMSEQ = 0)
    begin
      select @I_vLNITMSEQ = isnull(max(LNITMSEQ),0) + 16384
      from   SOP10200 (nolock)
      where  SOPTYPE = @I_vSOPTYPE
             and SOPNUMBE = @I_vSOPNUMBE
    end
  
  if (@I_vNONINVEN = 0)
    begin
      if (@ISMCTRX = 1)
        begin
          select @DECPLCUR = DECPLCUR,
                 @LISTPRCE = LISTPRCE
          from   IV00105 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and CURNCYID = @I_vCURNCYID
          
          select @ITEMFUNCTDEC = DECPLCUR - 1
          from   IV00105 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and CURNCYID = @FUNLCURR
        end
      else
        begin
          select @DECPLCUR = DECPLCUR,
                 @LISTPRCE = LISTPRCE
          from   IV00105 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and (CURNCYID = ''
                       or CURNCYID = @I_vCURNCYID)
          
          select @ITEMFUNCTDEC = DECPLCUR - 1
          from   IV00105 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and (CURNCYID = ''
                       or CURNCYID = @FUNLCURR)
        end
    end
  else
    begin
      select @ITEMFUNCTDEC = DECPLCUR - 1
      from   SOP40600 (nolock)
      where  CURNCYID = @FUNLCURR
    end
  
  select @FUNDECPLCUR = DECPLCUR - 1
  from   MC40000 a (nolock),
         DYNAMICS..MC40200 b (nolock)
  where  a.FUNCRIDX = b.CURRNIDX
         and a.FUNLCURR = b.CURNCYID
  
  if (@ISMCTRX = 1)
    begin
      select @EDITDECPLCUR = DECPLCUR - 1
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @I_vCURNCYID
    end
  else
    begin
      select @EDITDECPLCUR = @FUNDECPLCUR
    end
  
  if ((@I_vNONINVEN = 1)
      and exists (select 1
                  from   SOP40600 (nolock)
                  where  CURNCYID = @I_vCURNCYID))
    begin
      select @DECPLCUR = DECPLCUR
      from   SOP40600 (nolock)
      where  CURNCYID = @I_vCURNCYID
    end
  
  if (@I_vMRKDNAMT is not null)
     and (@I_vMRKDNPCT is not null)
    begin
      select @O_iErrorState = 7803
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (round(@I_vUNITPRCE,@DECPLCUR - 1) <> (@I_vUNITPRCE))
    begin
      select @O_iErrorState = 4646
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (round(@I_vXTNDPRCE,@EDITDECPLCUR) <> (@I_vXTNDPRCE))
    begin
      select @O_iErrorState = 4647
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (round(@I_vMRKDNAMT,@DECPLCUR - 1) <> (@I_vMRKDNAMT))
    begin
      select @O_iErrorState = 5447
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vMRKDNAMT is not null)
     and (@I_vMRKDNAMT >= 0)
    begin
      select @I_vMRKDNPCT = 0
    end
  
  if (@I_vMRKDNAMT is null)
     and (@I_vMRKDNPCT is not null)
    begin
      select @I_vMRKDNAMT = round(@I_vUNITPRCE * @I_vMRKDNPCT / 100.00,@DECPLCUR - 1)
      
      select @I_vDEFEXTPRICE = 1
    end
  
  if (@I_vMRKDNAMT is null)
     and (@I_vMRKDNPCT is null)
    begin
      select @I_vMRKDNAMT = 0,
             @I_vMRKDNPCT = 0
    end
  
  select @UOFMBASE = BASEUOFM
  from   IV40201 (nolock)
  where  UOMSCHDL = @UOMSCHDL
  
  select @QTYBSUOM = QTYBSUOM
  from   IV40202 (nolock)
  where  UOMSCHDL = @UOMSCHDL
         and EQUIVUOM = @UOFMBASE
         and UOFM = @I_vUOFM
  
  if (@I_vUOFM = '')
    begin
      select @O_iErrorState = 1599
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if exists (select 1
             from   IV00108 (nolock)
             where  ITEMNMBR = @I_vITEMNMBR
                    and CURNCYID <> '')
    select @tCURNCYID = @I_vCURNCYID
  else
    select @tCURNCYID = ''
  
  if (@I_vNONINVEN = 0)
    begin
      if (not exists (select 1
                      from   IV40202 (nolock)
                      where  UOFM = @I_vUOFM
                             and UOMSCHDL = @UOMSCHDL))
        begin
          select @O_iErrorState = 5313
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@I_vPRCLEVEL = ''
          and @EXPRINST = 0)
        begin
          select @I_vPRCLEVEL = PRCLEVEL
          from   RM00101 (nolock)
          where  CUSTNMBR = @I_vCUSTNMBR
          
          if ((@I_vPRCLEVEL <> '')
              and (@Prices_Not_Required_In_Pr = 0)
              and (not exists (select 1
                               from   IV00108 (nolock)
                               where  ITEMNMBR = @I_vITEMNMBR
                                      and PRCLEVEL = @I_vPRCLEVEL
                                      and CURNCYID = @tCURNCYID
                                      and UOFM = @I_vUOFM)))
            begin
              select @I_vPRCLEVEL = ''
            end
          
          if (@I_vPRCLEVEL = '')
            begin
              select @I_vPRCLEVEL = PRCLEVEL
              from   RM40101 (nolock)
              
              if ((@I_vPRCLEVEL <> '')
                  and (@Prices_Not_Required_In_Pr = 0)
                  and (not exists (select 1
                                   from   IV00108 (nolock)
                                   where  ITEMNMBR = @I_vITEMNMBR
                                          and PRCLEVEL = @I_vPRCLEVEL
                                          and CURNCYID = @tCURNCYID
                                          and UOFM = @I_vUOFM)))
                begin
                  select @I_vPRCLEVEL = ''
                end
            end
          
          if (@I_vPRCLEVEL = '')
            begin
              select @I_vPRCLEVEL = @PRCLEVEL
            end
        end
      else
        if (@I_vPRCLEVEL = ''
            and @EXPRINST = 1)
          begin
            select @I_vPRCLEVEL = PRCLEVEL
            from   RM40101 (nolock)
            
            if @I_vPRCLEVEL = ''
              begin
                select @I_vPRCLEVEL = 'EXTPRCLVL'
              end
          end
    end
  
  if (@I_vPRCLEVEL <> '')
    begin
      if (not exists (select 1
                      from   IV40800 (nolock)
                      where  PRCLEVEL = @I_vPRCLEVEL))
        begin
          select @O_iErrorState = 928
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @DECPLCURMINUS1 = @DECPLCUR - 1
  
  if (@I_vDEFPRICING = 1)
    begin
      if (@EXPRINST = 0)
        begin
          select @fieldupdate = 1
          
          if ((@PRICMTHD in (3,4,5,6))
              and (@ITEMTYPE = 3)
              and (@CALCKITC = 1)
              and (@I_vKitCompMan = 0))
            begin
              select @UOMPRICE = UOMPRICE / 100
              from   IV00108 (nolock)
              where  ITEMNMBR = @I_vITEMNMBR
                     and CURNCYID = @FUNLCURR
                     and PRCLEVEL = @I_vPRCLEVEL
                     and UOFM = @I_vUOFM
                     and FROMQTY <= @I_vQUANTITY
                     and TOQTY >= @I_vQUANTITY
              
              if (@PRICMTHD = 3)
                begin
                  select @I_vUNITPRCE = round(round(sum(CURRCOST * CMPITQTY),@ITEMFUNCTDEC) * isnull(round(1 + @UOMPRICE,@ITEMFUNCTDEC),0),
                                              @ITEMFUNCTDEC)
                  from   IV00104 a,
                         IV00101 b,
                         IV40202 c
                  where  a.CMPTITNM = b.ITEMNMBR
                         and c.UOMSCHDL = b.UOMSCHDL
                         and c.UOFM = a.CMPITUOM
                         and a.ITEMNMBR = @I_vITEMNMBR
                end
              else
                begin
                  if (@PRICMTHD = 4)
                    begin
                      select @I_vUNITPRCE = round(round(sum(STNDCOST * CMPITQTY),@ITEMFUNCTDEC) * isnull(round(1 + @UOMPRICE,@ITEMFUNCTDEC),0),
                                                  @ITEMFUNCTDEC)
                      from   IV00104 a,
                             IV00101 b,
                             IV40202 c
                      where  a.CMPTITNM = b.ITEMNMBR
                             and c.UOMSCHDL = b.UOMSCHDL
                             and c.UOFM = a.CMPITUOM
                             and a.ITEMNMBR = @I_vITEMNMBR
                    end
                  else
                    begin
                      if (@PRICMTHD = 5)
                        begin
                          select @I_vUNITPRCE = round(round(sum(CURRCOST * CMPITQTY),@ITEMFUNCTDEC) / isnull(1 - @UOMPRICE,0),
                                                      @ITEMFUNCTDEC)
                          from   IV00104 a,
                                 IV00101 b,
                                 IV40202 c
                          where  a.CMPTITNM = b.ITEMNMBR
                                 and c.UOMSCHDL = b.UOMSCHDL
                                 and c.UOFM = a.CMPITUOM
                                 and a.ITEMNMBR = @I_vITEMNMBR
                        end
                      else
                        begin
                          if (@PRICMTHD = 6)
                            begin
                              select @I_vUNITPRCE = round(round(sum(STNDCOST * CMPITQTY),@ITEMFUNCTDEC) / isnull(1 - @UOMPRICE,0),
                                                          @ITEMFUNCTDEC)
                              from   IV00104 a,
                                     IV00101 b,
                                     IV40202 c
                              where  a.CMPTITNM = b.ITEMNMBR
                                     and c.UOMSCHDL = b.UOMSCHDL
                                     and c.UOFM = a.CMPITUOM
                                     and a.ITEMNMBR = @I_vITEMNMBR
                            end
                        end
                    end
                end
            end
          else
            begin
              select @I_vUNITPRCE = case 
                                      when @PRICMTHD = 1 then isnull(UOMPRICE,0)
                                      when @PRICMTHD = 2 then isnull(round((@LISTPRCE * @QTYBSUOM) * (UOMPRICE / 100),
                                                                           @DECPLCURMINUS1),0)
                                      when @PRICMTHD = 3 then isnull(round((@CURRCOST * @QTYBSUOM) * (1 + (UOMPRICE / 100)),
                                                                           @DECPLCURMINUS1),0)
                                      when @PRICMTHD = 4 then isnull(round((@STNDCOST * @QTYBSUOM) * (1 + (UOMPRICE / 100)),
                                                                           @DECPLCURMINUS1),0)
                                      when @PRICMTHD = 5 then isnull(round((@CURRCOST * @QTYBSUOM) / (1 - (UOMPRICE / 100)),
                                                                           @DECPLCURMINUS1),0)
                                      when @PRICMTHD = 6 then isnull(round((@STNDCOST * @QTYBSUOM) / (1 - (UOMPRICE / 100)),
                                                                           @DECPLCURMINUS1),0)
                                      else 0
                                    end
              from   IV00108 (nolock)
              where  ITEMNMBR = @I_vITEMNMBR
                     and CURNCYID = @tCURNCYID
                     and PRCLEVEL = @I_vPRCLEVEL
                     and UOFM = @I_vUOFM
                     and QTYBSUOM = @QTYBSUOM
                     and FROMQTY <= @I_vTOTALQTY
                     and TOQTY >= @I_vTOTALQTY
            end
          
          select @RNDGAMNT = isnull(RNDGAMNT,0),
                 @ROUNDHOW = isnull(ROUNDHOW,0),
                 @ROUNDTO = isnull(ROUNDTO,0)
          from   IV00107 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and CURNCYID = @tCURNCYID
                 and PRCLEVEL = @I_vPRCLEVEL
                 and UOFM = @I_vUOFM
                 and QTYBSUOM = @QTYBSUOM
          
          if @ROUNDTO > 1
             and @ROUNDTO < 5
            begin
              exec @iStatus = taPricingGetRoundPrice
                @ROUNDTO ,
                @ROUNDHOW ,
                @RNDGAMNT ,
                @I_vUNITPRCE OUTPUT
              
              select @iError = @@error
              
              if (@iError <> 0)
                  or (@iStatus <> 0)
                begin
                  if (@iStatus <> 0)
                    begin
                      select @oErrString = rtrim(@oErrString) + ' ' + @iStatus
                    end
                  
                  select @O_iErrorState = 3445
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
        end
      else
        begin
          exec sopExtPricingGetPrice
            @piST_DorP ,
            @I_vCUSTNMBR ,
            @I_vITEMNMBR ,
            @I_vUOFM ,
            @I_vDOCDATE ,
            @I_vQUANTITY ,
            @I_vIncludePromo ,
            @I_vCURNCYID ,
            @I_vUNITPRCE OUTPUT ,
            @poIN_PromFound OUTPUT ,
            @poIN_PromType OUTPUT ,
            @poDE_PromPrice OUTPUT ,
            @poST_SchemeCode OUTPUT ,
            @poST_SchemeItemType OUTPUT ,
            @poST_SchemeGroup OUTPUT ,
            @poDE_QtyFrom OUTPUT ,
            @poDE_QtyTo OUTPUT ,
            @poIN_CurrPriceFound OUTPUT ,
            @Description OUTPUT ,
            @StartDate OUTPUT ,
            @EndDate OUTPUT ,
            @QuantityFree OUTPUT ,
            @QuantityFrom OUTPUT ,
            @QuantityTo OUTPUT ,
            @PriceSheetID OUTPUT
          
          select @iError = @@error
          
          if (@iError <> 0)
            begin
              if (@iStatus <> 0)
                begin
                  select @oErrString = rtrim(@oErrString) + ' ' + @iStatus
                end
              
              select @O_iErrorState = 3444
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if @poIN_PromFound = 1
            begin
              select @I_vUNITPRCE = @poDE_PromPrice
            end
          
          if ((@QuantityFree > 0)
              and (@I_vIncludePromo <> 0))
            begin
              select @MAXQTYFR = MAXQTYFR,
                     @FREEITEM = FREEITEM,
                     @FREEUOFM = FREEUOFM
              from   IV10403 (nolock)
              where  EPITMTYP = @poST_SchemeItemType
                     and ITEMNMBR = @poST_SchemeGroup
                     and PRCSHID = @poST_SchemeCode
                     and UOFM = @I_vUOFM
              
              if @MAXQTYFR < @QuantityFree
                 and @MAXQTYFR <> 0
                begin
                  select @QuantityFree = @MAXQTYFR
                end
            end
        end
    end
  
  if ((@I_vPRCLEVEL = '')
      and (@I_vNONINVEN = 0))
    begin
      select @O_iErrorState = 771
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPRCLEVEL <> '')
      and (@Prices_Not_Required_In_Pr = 0)
      and (@I_vNONINVEN = 0))
    begin
      if (not exists (select 1
                      from   IV00108 (nolock)
                      where  ITEMNMBR = @I_vITEMNMBR
                             and PRCLEVEL = @I_vPRCLEVEL
                             and ((CURNCYID = @I_vCURNCYID)
                                   or (CURNCYID = ''))
                             and UOFM = @I_vUOFM))
        begin
          select @O_iErrorState = 9177
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@I_vDEFEXTPRICE = 1)
       or (@I_vDEFPRICING = 1))
    begin
      if (@I_vSOPTYPE = 3)
        begin
          select @I_vXTNDPRCE = isnull(round(@I_vQUANTITY * (@I_vUNITPRCE - @I_vMRKDNAMT),
                                             @EDITDECPLCUR),0)
        end
      else
        begin
          select @I_vXTNDPRCE = isnull(round(@I_vTOTALQTY * (@I_vUNITPRCE - @I_vMRKDNAMT),
                                             @EDITDECPLCUR),0)
        end
    end
  
  if (@I_vINVINDX <> '')
    begin
      if (exists (select 1
                  from   GL00105 (nolock)
                  where  ACTNUMST = @I_vINVINDX))
        begin
          select @INVINDX_2 = ACTINDX
          from   GL00105 (nolock)
          where  ACTNUMST = @I_vINVINDX
        end
      else
        begin
          select @O_iErrorState = 929
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vCSLSINDX <> '')
    begin
      if (exists (select 1
                  from   GL00105 (nolock)
                  where  ACTNUMST = @I_vCSLSINDX))
        begin
          select @CSLSINDX_2 = ACTINDX
          from   GL00105 (nolock)
          where  ACTNUMST = @I_vCSLSINDX
        end
      else
        begin
          select @O_iErrorState = 930
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vSLSINDX <> '')
    begin
      if (exists (select 1
                  from   GL00105 (nolock)
                  where  ACTNUMST = @I_vSLSINDX))
        begin
          select @SLSINDX_2 = ACTINDX
          from   GL00105 (nolock)
          where  ACTNUMST = @I_vSLSINDX
        end
      else
        begin
          select @O_iErrorState = 908
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vMKDNINDX <> '')
    begin
      if (exists (select 1
                  from   GL00105 (nolock)
                  where  ACTNUMST = @I_vMKDNINDX))
        begin
          select @MKDNINDX_2 = ACTINDX
          from   GL00105 (nolock)
          where  ACTNUMST = @I_vMKDNINDX
        end
      else
        begin
          select @O_iErrorState = 909
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vRTNSINDX <> '')
    begin
      if (exists (select 1
                  from   GL00105 (nolock)
                  where  ACTNUMST = @I_vRTNSINDX))
        begin
          select @RTNSINDX_2 = ACTINDX
          from   GL00105 (nolock)
          where  ACTNUMST = @I_vRTNSINDX
        end
      else
        begin
          select @O_iErrorState = 910
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vINUSINDX <> '')
    begin
      if (exists (select 1
                  from   GL00105 (nolock)
                  where  ACTNUMST = @I_vINUSINDX))
        begin
          select @INUSINDX_2 = ACTINDX
          from   GL00105 (nolock)
          where  ACTNUMST = @I_vINUSINDX
        end
      else
        begin
          select @O_iErrorState = 913
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vINSRINDX <> '')
    begin
      if (exists (select 1
                  from   GL00105 (nolock)
                  where  ACTNUMST = @I_vINSRINDX))
        begin
          select @INSRINDX_2 = ACTINDX
          from   GL00105 (nolock)
          where  ACTNUMST = @I_vINSRINDX
        end
      else
        begin
          select @O_iErrorState = 914
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vDMGDINDX <> '')
    begin
      if (exists (select 1
                  from   GL00105 (nolock)
                  where  ACTNUMST = @I_vDMGDINDX))
        begin
          select @DMGDINDX_2 = ACTINDX
          from   GL00105 (nolock)
          where  ACTNUMST = @I_vDMGDINDX
        end
      else
        begin
          select @O_iErrorState = 915
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (not exists (select 1
                  from   RM00101 (nolock)
                  where  CUSTNMBR = @I_vCUSTNMBR))
    begin
      select @O_iErrorState = 598
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vSLPRSNID <> '')
    begin
      if (not exists (select 1
                      from   RM00301 (nolock)
                      where  SLPRSNID = @I_vSLPRSNID))
        begin
          select @O_iErrorState = 179
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vSALSTERR <> '')
    begin
      if (not exists (select 1
                      from   RM00303 (nolock)
                      where  SALSTERR = @I_vSALSTERR))
        begin
          select @O_iErrorState = 245
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@I_vSHIPMTHD <> '')
      and (@I_vSHIPMTHD is not null))
    begin
      if (not exists (select 1
                      from   SY03000 (nolock)
                      where  SHIPMTHD = @I_vSHIPMTHD))
        begin
          select @O_iErrorState = 927
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select top 1 @LOCNCODE = LOCNCODE
  from   IV40700 (nolock)
  where  LOCNCODE <> ''
  
  if (@I_vLOCNCODE = '')
    begin
      if (@LOCNCODE <> '')
        begin
          select @O_iErrorState = 105
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      if (not exists (select 1
                      from   IV40700 (nolock)
                      where  LOCNCODE = @I_vLOCNCODE))
        begin
          select @O_iErrorState = 106
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@I_vNONINVEN = 0)
          and (not exists (select 1
                           from   IV00102 (nolock)
                           where  ITEMNMBR = @I_vITEMNMBR
                                  and LOCNCODE = @I_vLOCNCODE)))
        begin
          select @O_iErrorState = 110
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (((@LOCNCODE = '')
       and (@I_vLOCNCODE = ''))
      and (@I_vQtyShrtOpt in (1,3,4,5,
                              6)))
    begin
      select @O_iErrorState = 1747
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vNONINVEN = 0)
      and (not exists (select 1
                       from   IV00101 (nolock)
                       where  ITEMNMBR = @I_vITEMNMBR)))
    begin
      select @O_iErrorState = 108
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vNONINVEN = 0)
    begin
      if (@RTNSINDX = 0)
        select @RTNSINDX = ACTINDX
        from   SY01100 (nolock)
        where  SERIES = 5
               and SEQNUMBR = 1300
      
      if (@INUSINDX = 0)
        select @INUSINDX = ACTINDX
        from   SY01100 (nolock)
        where  SERIES = 5
               and SEQNUMBR = 700
      
      if (@INSRINDX = 0)
        select @INSRINDX = ACTINDX
        from   SY01100 (nolock)
        where  SERIES = 5
               and SEQNUMBR = 800
      
      if (@DMGDINDX = 0)
        select @DMGDINDX = ACTINDX
        from   SY01100 (nolock)
        where  SERIES = 5
               and SEQNUMBR = 900
    end
  else
    begin
      select @ITEMDESC = @I_vITEMDESC
      
      select @INVINDX = ACTINDX,
             @MKDNINDX = 0,
             @RTNSINDX = ACTINDX,
             @INUSINDX = ACTINDX,
             @INSRINDX = ACTINDX,
             @DMGDINDX = ACTINDX
      from   SY01100 (nolock)
      where  SERIES = 3
             and SEQNUMBR = 2400
      
      select @SLSINDX = 0
      
      select @CSLSINDX = 0
    end
  
  if (@MKDNINDX = 0)
    select @MKDNINDX = ACTINDX
    from   SY01100 (nolock)
    where  SERIES = 5
           and SEQNUMBR = 500
  
  if (@USEACFRM = 1)
    begin
      select @SLSINDX = case 
                          when (@I_vSOPTYPE <> 4) then RMSLSACC
                          when @I_vSOPTYPE = 4 then RMSORACC
                          else 0
                        end,
             @CSLSINDX = RMCOSACC
      from   RM00101 (nolock)
      where  CUSTNMBR = @I_vCUSTNMBR
      
      if (@I_vSOPTYPE <> 4)
        begin
          if (@SLSINDX = 0)
            begin
              select @SLSINDX = ACTINDX
              from   SY01100 (nolock)
              where  SERIES = 3
                     and SEQNUMBR = 1100
            end
        end
      else
        begin
          if (@SLSINDX = 0)
            begin
              select @SLSINDX = ACTINDX
              from   SY01100 (nolock)
              where  SERIES = 3
                     and SEQNUMBR = 1200
            end
        end
      
      if (@CSLSINDX = 0)
        select @CSLSINDX = ACTINDX
        from   SY01100 (nolock)
        where  SERIES = 3
               and SEQNUMBR = 200
      
      if ((@INVINDX = 0)
          and (@I_vNONINVEN = 0)
          and @I_vDROPSHIP = 0)
        select @INVINDX = ACTINDX
        from   SY01100 (nolock)
        where  SERIES = 3
               and SEQNUMBR = 900
    end
  else
    begin
      if ((@INVINDX = 0)
          and (@I_vNONINVEN = 0)
          and @I_vDROPSHIP = 0)
        select @INVINDX = ACTINDX
        from   SY01100 (nolock)
        where  SERIES = 5
               and SEQNUMBR = 100
      
      if (@SLSINDX = 0)
        begin
          if (@I_vSOPTYPE = 4)
              or (@I_vSOPTYPE = 3
                  and @I_vQUANTITY < 0)
            select @SLSINDX = ACTINDX
            from   SY01100 (nolock)
            where  SERIES = 5
                   and SEQNUMBR = 600
          else
            select @SLSINDX = ACTINDX
            from   SY01100 (nolock)
            where  SERIES = 5
                   and SEQNUMBR = 400
        end
      
      if (@CSLSINDX = 0)
        select @CSLSINDX = ACTINDX
        from   SY01100 (nolock)
        where  SERIES = 5
               and SEQNUMBR = 300
    end
  
  if ((@I_vQUANTITY <> @I_vQTYINSVC + @I_vQTYINUSE + @I_vQTYDMGED + @I_vQTYRTRND + @I_vQTYONHND)
      and (@I_vSOPTYPE = 4))
    begin
      select @O_iErrorState = 183
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vMRKDNAMT > @I_vUNITPRCE)
      and (@I_vMRKDNAMT > 0))
    begin
      select @O_iErrorState = 451
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vSOPTYPE = 3)
    begin
      if (@I_vQUANTITY = 0)
        begin
          if (@I_vXTNDPRCE <> 0)
            begin
              select @O_iErrorState = 4655
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      else
        begin
          if ((round(abs((@I_vUNITPRCE - @I_vMRKDNAMT) - round((@I_vXTNDPRCE / @I_vQUANTITY),@DECPLCURMINUS1)) * @I_vQUANTITY,
                     @EDITDECPLCUR)) <> 0)
            begin
              if ((round(abs(round((@I_vUNITPRCE - @I_vMRKDNAMT) * @I_vQUANTITY,
                                   @EDITDECPLCUR) - @I_vXTNDPRCE) * @I_vQUANTITY,
                         @EDITDECPLCUR)) <> 0)
                begin
                  select @O_iErrorState = 722
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
        end
    end
  else
    begin
      if (@I_vTOTALQTY = 0)
        begin
          if (@I_vXTNDPRCE <> 0)
            begin
              select @O_iErrorState = 4644
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      else
        begin
          if ((round(abs((@I_vUNITPRCE - @I_vMRKDNAMT) - round((@I_vXTNDPRCE / @I_vTOTALQTY),@DECPLCURMINUS1)) * @I_vTOTALQTY,
                     @EDITDECPLCUR)) <> 0)
            begin
              if ((round(abs(round((@I_vUNITPRCE - @I_vMRKDNAMT) * @I_vTOTALQTY,
                                   @EDITDECPLCUR) - @I_vXTNDPRCE) * @I_vTOTALQTY,
                         @EDITDECPLCUR)) <> 0)
                begin
                  select @O_iErrorState = 299
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
        end
    end
  
  if ((((@I_vQTYINSVC <> 0)
         or (@I_vQTYINUSE <> 0)
         or (@I_vQTYDMGED <> 0)
         or (@I_vQTYRTRND <> 0)
         or (@I_vQTYONHND <> 0))
       and (@I_vSOPTYPE <> 4)))
    begin
      select @O_iErrorState = 215
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vNONINVEN = 1)
    begin
      if (@I_vUNITCOST is not null)
        begin
          select @UNITCOST = @I_vUNITCOST
        end
      else
        begin
          select @UNITCOST = 0
        end
    end
  
  if (@I_vSOPTYPE = 4)
      or (@I_vSOPTYPE = 3
          and @I_vQUANTITY < 0)
    begin
      if (@I_vUNITCOST is not null)
        begin
          select @UNITCOST = @I_vUNITCOST
        end
    end
  
  if ((@I_vSOPTYPE <> 2)
      and (@I_vSOPTYPE <> 3)
      and (@I_vQTYTBAOR <> 0))
    begin
      select @O_iErrorState = 302
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vSOPTYPE = 3)
     and (@I_vQTYTBAOR <> 0)
     and (@I_vQUANTITY < 0)
    begin
      select @O_iErrorState = 5528
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@ITEMTYPE = 3)
    begin
      if (@I_vDROPSHIP = 1)
        begin
          select @O_iErrorState = 303
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@I_vSOPTYPE = 4)
         and ((@I_vQTYONHND > 0
               and @I_vQTYONHND < @I_vQUANTITY)
               or (@I_vQTYRTRND > 0
                   and @I_vQTYRTRND < @I_vQUANTITY)
               or (@I_vQTYINUSE > 0
                   and @I_vQTYINUSE < @I_vQUANTITY)
               or (@I_vQTYINSVC > 0
                   and @I_vQTYINSVC < @I_vQUANTITY)
               or (@I_vQTYDMGED > 0
                   and @I_vQTYDMGED < @I_vQUANTITY))
        begin
          select @O_iErrorState = 6645
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@I_vQtyShrtOpt not in (1,2,3,5))
        begin
          select @O_iErrorState = 592
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@I_vTOTALQTY <> @I_vQTYCANCE)
          and (@I_vTOTALQTY <> @I_vQUANTITY)
          and (@I_vTOTALQTY <> @I_vQUOTEQTYTOINV)
          and (@I_vTOTALQTY <> @I_vQTYTBAOR))
        begin
          select @O_iErrorState = 2231
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@I_vTAXSCHID <> '')
      and (@I_vTAXSCHID is not null))
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vTAXSCHID)
        begin
          select @O_iErrorState = 3420
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@I_vITMTSHID <> '')
      and (@I_vITMTSHID is not null))
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vITMTSHID)
        begin
          select @O_iErrorState = 870
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@I_vITMTSHID <> '')
      and (@I_vITMTSHID is not null)
      and (@I_vIVITMTXB in (2,3)))
    begin
      select @O_iErrorState = 3408
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vITMTSHID is null)
    begin
      if ((@I_vNONINVEN = 0)
          and (@I_vIVITMTXB = 1))
        begin
          select @I_vITMTSHID = @ITMTSHID
        end
      else
        begin
          select @NONIVTXB = NONIVTXB,
                 @NONIVSCH = NONIVSCH
          from   SOP40100 (nolock)
          where  SETUPKEY = 1
          
          if ((@NONIVTXB = 1)
              and (@I_vNONINVEN = 1)
              and (@I_vIVITMTXB <> 2))
            begin
              select @I_vITMTSHID = @NONIVSCH
            end
          else
            begin
              select @I_vITMTSHID = ''
            end
        end
    end
  
  select @SHIPTYPE = SHIPTYPE
  from   SY03000 (nolock)
  where  SHIPMTHD = @I_vSHIPMTHD
  
  if (@I_vTAXSCHID is null)
    begin
      if (@SHIPTYPE = 0)
         and (@VATEnabled = 1)
        begin
          select @I_vTAXSCHID = STAXSCHD
          from   IV40700 (nolock)
          where  LOCNCODE = @I_vLOCNCODE
        end
      else
        begin
          select @I_vTAXSCHID = TAXSCHID
          from   RM00102 (nolock)
          where  CUSTNMBR = @I_vCUSTNMBR
                 and ADRSCODE = @I_vPRSTADCD
        end
      
      if (@I_vTAXSCHID is null)
        begin
          select @I_vTAXSCHID = ''
        end
    end
  
  if (@I_vCKCreditLimit = 1)
    begin
      exec @iStatus = taCustomerBalance
        @I_vCUSTNMBR = @I_vCUSTNMBR ,
        @I_vNewSaleAmt = @I_vXTNDPRCE ,
        @O_nOverCreditLimit = @OverCreditLimit output ,
        @O_CRLMTTYP = @CRLMTTYP output ,
        @O_nCurrBalance = @CurrBalance output ,
        @O_nCRLMTAMT = @CRLMTAMT output ,
        @O_nCUSTBLNC = @CUSTBLNC output ,
        @O_nUNPSTDSA = @UNPSTDSA output ,
        @O_nUNPSTOSA = @UNPSTOSA1 output ,
        @O_nONORDAMT = @ONORDAMT output ,
        @O_nUNPSTDCA = @UNPSTDCA output ,
        @O_nUNPSTOCA = @UNPSTOCA1 output ,
        @O_nDEPRECV = @DEPRECV output ,
        @O_iErrorState = @iCustomerBalanceErrState output ,
        @oErrString = @taCustomerBalanceErrString output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iCustomerBalanceErrState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + @taCustomerBalanceErrString
          
          select @O_iErrorState = 1151
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@OverCreditLimit = 1)
        begin
          select @O_iErrorState = 1152
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPRSTADCD <> '')
    begin
      select @I_vCNTCPRSN = case 
                              when @I_vCNTCPRSN = '' then CNTCPRSN
                              else @I_vCNTCPRSN
                            end,
             @I_vADDRESS1 = case 
                              when @I_vADDRESS1 = '' then ADDRESS1
                              else @I_vADDRESS1
                            end,
             @I_vADDRESS2 = case 
                              when @I_vADDRESS2 = '' then ADDRESS2
                              else @I_vADDRESS2
                            end,
             @I_vADDRESS3 = case 
                              when @I_vADDRESS3 = '' then ADDRESS3
                              else @I_vADDRESS3
                            end,
             @I_vCITY = case 
                          when @I_vCITY = '' then CITY
                          else @I_vCITY
                        end,
             @I_vSTATE = case 
                           when @I_vSTATE = '' then STATE
                           else @I_vSTATE
                         end,
             @I_vZIPCODE = case 
                             when @I_vZIPCODE = '' then ZIP
                             else @I_vZIPCODE
                           end,
             @I_vCOUNTRY = case 
                             when @I_vCOUNTRY = '' then COUNTRY
                             else @I_vCOUNTRY
                           end,
             @I_vPHONE1 = case 
                            when @I_vPHONE1 = '' then PHONE1
                            else @I_vPHONE1
                          end,
             @I_vPHONE2 = case 
                            when @I_vPHONE2 = '' then PHONE2
                            else @I_vPHONE2
                          end,
             @I_vPHONE3 = case 
                            when @I_vPHONE3 = '' then PHONE3
                            else @I_vPHONE3
                          end,
             @I_vFAXNUMBR = case 
                              when @I_vFAXNUMBR = '' then FAX
                              else @I_vFAXNUMBR
                            end
      from   RM00102 (nolock)
      where  CUSTNMBR = @I_vCUSTNMBR
             and ADRSCODE = @I_vPRSTADCD
      
      if @I_vShipToName = ''
        select @I_vShipToName = CUSTNAME
        from   RM00101 (nolock)
        where  CUSTNMBR = @I_vCUSTNMBR
    end
  
  if ((@ITMTRKOP = 3)
      and (@I_vNONINVEN = 0)
      and (@I_vSOPTYPE in (2,3,6))
      and ((@I_vAUTOALLOCATELOT = 1)
            or (@I_vAUTOALLOCATELOT = 2)))  --Suggestion ID#10793 
    begin
      if (@I_vLNITMSEQ = 0)
        begin
          select @O_iErrorState = 2084
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@ITMTRKOP = 2)
      and (@I_vNONINVEN = 0)
      and (@I_vSOPTYPE in (2,3,6))
      and ((@I_vAUTOALLOCATESERIAL = 1)
            or (@I_vAUTOALLOCATESERIAL = 2)))  --Suggestion ID#10793
    begin
      if (@I_vLNITMSEQ = 0)
        begin
          select @O_iErrorState = 3459
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (exists (select 1
              from   SOP10200 (nolock)
              where  SOPNUMBE = @I_vSOPNUMBE
                     and LNITMSEQ = @I_vLNITMSEQ
                     and SOPTYPE = @I_vSOPTYPE))
    begin
      select @O_iErrorState = 450
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @lock = ITEMNMBR
  from   IV00102 with (updlock index (PKIV00102 ))
  where  ITEMNMBR = @I_vITEMNMBR
         and (LOCNCODE = @I_vLOCNCODE
               or LOCNCODE = '')
  
  if ((@ITEMTYPE <> 3) and (@I_vQtyShrtOpt <> 2))
    begin
      select @QTTYALLOC = ATYALLOC
      from   IV00102 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
             and LOCNCODE = @I_vLOCNCODE
      
      if (@QTTYALLOC >= 0)
        begin
          select @QTYAVL = QTYONHND - ATYALLOC
          from   IV00102 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and LOCNCODE = @I_vLOCNCODE
        end
      else
        begin
          select @QTYAVL = QTYONHND + ATYALLOC
          from   IV00102 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and LOCNCODE = @I_vLOCNCODE
        end
      
      if (@QTYAVL < 0)
        begin
          select @QTYAVL = 0
        end
      
      if ((@I_vNONINVEN = 0) and ((@I_vSOPTYPE = 2) or (@I_vSOPTYPE = 3) or (@I_vSOPTYPE = 6)) and ((@I_vQUANTITY * @QTYBSUOM) > @QTYAVL))  --Suggestion ID#10793 added SOPTYPE = 6 
        begin
          if (@I_vQtyShrtOpt = 1)
            begin
              select @I_vQUANTITY = round(@QTYAVL / @QTYBSUOM,@DECPLQTY - 1,1)
              
              if (@TotQtyPassed = 0)
              begin
                select @I_vTOTALQTY = round(@QTYAVL / @QTYBSUOM,@DECPLQTY - 1,1) + @I_vQTYCANCE + @I_vQTYTBAOR
              end
            end
          else
            begin
              if (@I_vQtyShrtOpt = 3)
                begin
                  select @QTYTOBO = @I_vQUANTITY,
                         @I_vQUANTITY = 0
                  
                  if (@TotQtyPassed = 0)
                    begin
                      select @I_vTOTALQTY = @QTYTOBO + @I_vQTYCANCE + @I_vQTYTBAOR
                    end
                end
              else
                begin
                  if (@I_vQtyShrtOpt = 4)
                    begin
                      select @QTYTOBO = @I_vQUANTITY - round(@QTYAVL / @QTYBSUOM,@DECPLQTY - 1,1)
                      
                      select @I_vQUANTITY = @I_vQUANTITY - @QTYTOBO
                      
                      if (@TotQtyPassed = 0)
                        begin
                          select @I_vTOTALQTY = @I_vQUANTITY + @QTYTOBO + @I_vQTYCANCE + @I_vQTYTBAOR
                        end
                    end
                  else
                    begin
                      if (@I_vQtyShrtOpt = 5)
                        begin
                          select @I_vQTYCANCE = @I_vQTYCANCE + @I_vQUANTITY,
                                 @I_vQUANTITY = 0
                          
                          if (@TotQtyPassed = 0)
                            begin
                              select @I_vTOTALQTY = @I_vQTYCANCE + @I_vQTYTBAOR
                            end
                        end
                      else
                        begin
                          if (@I_vQtyShrtOpt = 6)
                            begin
                              select @I_vQTYCANCE = @I_vQTYCANCE + (@I_vQUANTITY - round(@QTYAVL / @QTYBSUOM,@DECPLQTY - 1,1))
                              
                              select @I_vQUANTITY = @I_vQUANTITY - @I_vQTYCANCE
                              
                              if (@TotQtyPassed = 0)
                                begin
                                  select @I_vTOTALQTY = @I_vQUANTITY + @I_vQTYCANCE + @I_vQTYTBAOR
                                end
                            end
                        end
                    end
                end
            end
        end
    end
  
  if ((@ITMTRKOP = 2)
      and (@ITEMTYPE in (1,2))
      and (@I_vNONINVEN = 0)
      and (@I_vSOPTYPE in (2,3,6))
      and (@LOCNCODE <> ''))  --Suggestion ID#10793
    begin
      select @SOP10201COUNTSERIAL = count(SOPTYPE)
      from   SOP10201 (nolock)
      where  SOPTYPE = @I_vSOPTYPE
             and SOPNUMBE = @I_vSOPNUMBE
             and ITEMNMBR = @I_vITEMNMBR
             and LNITMSEQ = @I_vLNITMSEQ
      
      if (@QTYFULFI <> -1)
        begin
          select @SERIALQUANTITY = @I_vQTYFULFI * @QTYBSUOM
        end
      else
        begin
          if (((@ALLOCABY = 3) or (@ALLOCABY = 2)) or ((@ALLOCABY = 1) and (@USPFULPR = 1) and (@I_vQUANTITY >= 0)))
            begin
              select @SERIALQUANTITY = 0
            end
          else
            begin
              select @SERIALQUANTITY = abs(@I_vQUANTITY) * @QTYBSUOM
              
              select @AvailSERIALQUANTITY = @SOP10201COUNTSERIAL + count(ITEMNMBR)
              from   IV00200 (nolock)
              where  ITEMNMBR = @I_vITEMNMBR
                     and LOCNCODE = @I_vLOCNCODE
                     and SERLNSLD = 0
                     and QTYTYPE = 1
              
              if (@AvailSERIALQUANTITY < @SERIALQUANTITY)
                select @SERIALQUANTITY = @AvailSERIALQUANTITY
            end
          
          if (@I_vAUTOALLOCATESERIAL = 1)
            begin
              select @I_vQTYFULFI = @SOP10201COUNTSERIAL,
                     @QTYFULFI = @SOP10201COUNTSERIAL
            end
        end
      
      select @serialstoallocate = ABS(@SERIALQUANTITY) - isnull(count(SERLTQTY),0)
      from   SOP10201 (nolock)
      where  SOPTYPE = @I_vSOPTYPE
             and SOPNUMBE = @I_vSOPNUMBE
             and ITEMNMBR = @I_vITEMNMBR
             and LNITMSEQ = @I_vLNITMSEQ
      
      if (@serialstoallocate < 0)
        begin
          select @O_iErrorState = 1523
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@SOP10201COUNTSERIAL = 0)
          and ((@I_vAUTOALLOCATESERIAL = 1)
                or (@I_vAUTOALLOCATESERIAL = 2))
          and (@ITMTRKOP = 2)
          and ((@USPFULPR = 0)
                or (@I_vQTYFULFI > 0))
          and ((@I_vQTYFULFI <> 0)
                or (@I_vQTYFULFI is null)))
        begin
          select @O_iErrorState = 2322
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@ITMTRKOP = 2)
      and (@I_vNONINVEN = 0)
      and ((@I_vAUTOALLOCATESERIAL = 0)
            or (@I_vAUTOALLOCATESERIAL = 2))
      and (@I_vSOPTYPE in (2,3,6))
      and (@LOCNCODE <> ''))  --Suggestion ID#10793 added @I_vSOPTYPE in (2,3,6)
    begin
      select @SERIALQTY = count(ITEMNMBR)
      from   IV00200 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
             and SERLNSLD = 0
             and QTYTYPE = 1
      
      if ((@SERIALQTY < @serialstoallocate)
          and (@I_vQtyShrtOpt = 2))
        begin
          select @O_iErrorState = 1536
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@ITMTRKOP = 3)
      and (@ITEMTYPE in (1,2))
      and (@I_vNONINVEN = 0)
      and (@I_vSOPTYPE in (2,3,6))
      and (@LOCNCODE <> ''))  --Suggestion ID#10793
    begin
      select @SOP10201COUNT = count(SOPTYPE),
             @SOP10201LOTSUM = isnull(sum(SERLTQTY),0)
      from   SOP10201 (nolock)
      where  SOPTYPE = @I_vSOPTYPE
             and SOPNUMBE = @I_vSOPNUMBE
             and ITEMNMBR = @I_vITEMNMBR
             and LNITMSEQ = @I_vLNITMSEQ
      
      if (@QTYFULFI <> -1)
        begin
          select @LOTQUANTITY = @I_vQTYFULFI * @QTYBSUOM
        end
      else
        begin
          if (((@ALLOCABY = 3)
                or (@ALLOCABY = 2))
               or ((@ALLOCABY = 1)
                   and (@USPFULPR = 1)))
            begin
              select @LOTQUANTITY = 0
            end
          else
            begin
              select @LOTQUANTITY = @I_vQUANTITY * @QTYBSUOM
            end
          
          if (@I_vAUTOALLOCATELOT = 1)
            begin
              select @I_vQTYFULFI = @SOP10201LOTSUM,
                     @QTYFULFI = @SOP10201LOTSUM
            end
        end
      
      select @LotsToAllocate = @LOTQUANTITY - isnull(sum(SERLTQTY),0)
      from   SOP10201 (nolock)
      where  SOPTYPE = @I_vSOPTYPE
             and SOPNUMBE = @I_vSOPNUMBE
             and ITEMNMBR = @I_vITEMNMBR
             and LNITMSEQ = @I_vLNITMSEQ
      
      if (@LotsToAllocate < 0)
         and (@I_vQUANTITY >= 0)
        begin
          select @O_iErrorState = 2083
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@ITMTRKOP = 3)
      and (@I_vNONINVEN = 0)
      and ((@I_vAUTOALLOCATELOT = 0)
            or (@I_vAUTOALLOCATELOT = 2))
      and (@I_vSOPTYPE in (2,3,6))  --Suggestion ID#10793
      and ((@USPFULPR = 0)
            or (@I_vQTYFULFI > 0))
      and (@I_vQTYFULFI <> 0)
      and (@LOCNCODE <> ''))
    begin
      select @LOTQTY = isnull(sum(QTYRECVD),0)
      from   IV00300 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
             and LTNUMSLD = 0
             and QTYTYPE = 1
      
      if ((@LOTQTY < @LotsToAllocate)
          and (@I_vQtyShrtOpt = 2))
        begin
          select @O_iErrorState = 2082
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@ITMTRKOP = 3)
      and (@LOCNCODE <> ''))
    begin
      if ((@SOP10201COUNT = 0)
          and ((@I_vAUTOALLOCATELOT = 1)
                or (@I_vAUTOALLOCATELOT = 2))
          and (@ITMTRKOP = 3)
          and ((@USPFULPR = 0)
                or (@I_vQTYFULFI > 0))
          and (@I_vQTYFULFI <> 0))
        begin
          select @O_iErrorState = 2305
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@SOP10201COUNT > 0)
          and (@I_vAUTOALLOCATELOT = 0)
          and (@ITMTRKOP = 3))
        begin
          select @O_iErrorState = 3395
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@I_vAUTOALLOCATELOT = 1)
        begin
          select @LOTFULFILLED = isnull(sum(SERLTQTY),0)
          from   SOP10201 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and SOPTYPE = @I_vSOPTYPE
                 and SOPNUMBE = @I_vSOPNUMBE
                 and LNITMSEQ = @I_vLNITMSEQ
          
          if (@QTYFULFI = -1) and (@I_vQUANTITY > 0)
            begin
              if (@LOTFULFILLED <> ((@I_vQUANTITY - @I_vQTYCANCE - @I_vQTYTBAOR - @QTYTOBO - @QTYPRBAC - @QTYPRBOO - @QTYPRINV - @QTYPRORD) * @QTYBSUOM))
                begin
                  select @O_iErrorState = 3460
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
          else
            begin
              if (@LOTFULFILLED <> (@I_vQTYFULFI * @QTYBSUOM)) and (@I_vQUANTITY >= 0)
                begin
                  select @O_iErrorState = 4730
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
        end
      
      if ((@I_vSOPTYPE in (2,3,6))
          and (@ITMTRKOP = 3)
          and (@ITEMTYPE = 1)
          and (@I_vNONINVEN = 0)
          and  --Suggestion ID#10793
           ((@I_vAUTOALLOCATELOT = 0)
             or (@I_vAUTOALLOCATELOT = 2))
          and (@LotsToAllocate > 0)
          and ((@USPFULPR = 0)
                or (@I_vQTYFULFI > 0))
          and ((@I_vQTYFULFI <> 0)
                or (@QTYFULFI = -1))
          and @I_vQUANTITY > 0)
        begin
          exec @iStatus = taSopLot
            @I_vSOPTYPE = @I_vSOPTYPE ,
            @I_vSOPNUMBE = @I_vSOPNUMBE ,
            @I_vLNITMSEQ = @I_vLNITMSEQ ,
            @I_vCMPNTSEQ = 0 ,
            @I_vITEMNMBR = @I_vITEMNMBR ,
            @I_vLOCNCODE = @I_vLOCNCODE ,
            @I_vQUANTITY = @LotsToAllocate ,
            @I_vDOCID = @I_vDOCID ,
            @I_vDROPSHIP = @I_vDROPSHIP ,
            @I_vQTYFULFI = @LotsToAllocate ,
            @I_vALLOCATE = @I_vALLOCATE ,
            @O_iErrorState = @iCustomState output ,
            @oErrString = @iCustomErrString output
          
          select @iError = @@error
          
          if ((@iStatus <> 0)
               or (@iCustomState <> 0)
               or (@iError <> 0))
            begin
              select @oErrString = rtrim(@oErrString) + ' ' + @iCustomErrString
              
              select @O_iErrorState = 1008
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_oErrorState)
            end
        end
    end
  
  if (((@I_vNONINVEN = 0) and (@ALWBKORD = 0)) and ((@I_vQTYTBAOR > 0) or (@QTYTOBO > 0) or (@I_vSOPTYPE = 5)))
    begin
      select @O_iErrorState = 4776
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vEXCEPTIONALDEMAND < 0)
       or (@I_vEXCEPTIONALDEMAND > 1))
    begin
      select @O_iErrorState = 2613
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  while (1 = 1) and (@O_iErrorState = 0)
    begin
      if ((@ITMTRKOP = 2)
          and (@ITEMTYPE = 1)
          and (@I_vNONINVEN = 0)
          and (@I_vSOPTYPE in (2,3,6))  -- Suggestion ID#10793 removed this stuff and added in SOPTYPE in (2,3,6) for SOP Fulfillment Orders
          and ((@I_vAUTOALLOCATESERIAL = 0)
                or (@I_vAUTOALLOCATESERIAL = 2))
          and (@LOCNCODE <> ''))
        begin
          while (@serialsallocated < @serialstoallocate)
            begin
              -- Date SEQ Number - Set to 1 greater than the sequence number assigned to the last serial number for the item at the site and date received in the IV_Serial_MSTR table
              
              -- This select will get the top record where the date received is First In and the Date SEQ Number is the lowest - FIFO
              
              -- Get the valuation method for the item 1,3,4 = Fifo else Lifo
              select @VCTNMTHD = isnull(VCTNMTHD,0)
              from   IV00101 (nolock)
              where  ITEMNMBR = @I_vITEMNMBR
              
              --FIFO valuation method
              if ((@VCTNMTHD = 1)
                   or (@VCTNMTHD = 3)
                   or (@VCTNMTHD = 4))
                select   TOP 1 @SERLTNUM = SERLNMBR
                from     IV00200 (nolock)
                where    ITEMNMBR = @I_vITEMNMBR
                         and LOCNCODE = @I_vLOCNCODE
                         and SERLNSLD = 0
                         and QTYTYPE = 1
                order by DATERECD,
                         DTSEQNUM
              else
                --LIFO valuation method
                select TOP 1 @SERLTNUM = SERLNMBR
                from     IV00200 (nolock)
                where    ITEMNMBR = @I_vITEMNMBR
                         and LOCNCODE = @I_vLOCNCODE
                         and SERLNSLD = 0
                         and QTYTYPE = 1
                order by DATERECD DESC,
                         DTSEQNUM DESC
              
              if (@SERLTNUM = '')
                begin
                  select @O_iErrorState = 1525
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
              else
                begin
                  exec @iStatus = taSopSerial
                    @I_vSOPTYPE = @I_vSOPTYPE ,
                    @I_vSOPNUMBE = @I_vSOPNUMBE ,
                    @I_vLNITMSEQ = @I_vLNITMSEQ ,
                    @I_vCMPNTSEQ = 0 ,
                    @I_vQTYTYPE = 1 ,
                    @I_vITEMNMBR = @I_vITEMNMBR ,
                    @I_vLOCNCODE = @I_vLOCNCODE ,
                    @I_vQUANTITY = 1 ,
                    @I_vSERLNMBR = @SERLTNUM ,
                    @I_vAUTOCREATESERIAL = 0 ,
                    @I_vDOCID = @I_vDOCID ,
                    @I_vRequesterTrx = 0 ,
                    @I_vUSRDEFND5 = 'taSopSerial48B022C4-9017-4C0D-926F-DFC059D91737' ,
                    @O_iErrorState = @iCustomState output ,
                    @oErrString = @iCustomErrString output
                  
                  if (@iCustomState not in (0,1526))
                    begin
                      select @iError = @@error
                      
                      if ((@iStatus <> 0)
                           or (@iCustomState <> 0)
                           or (@iError <> 0))
                        begin
                          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
                          
                          select @O_iErrorState = 1527
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          break
                        end
                    end
                  
                  select @SERLTNUM = ''
                  
                  select @serialsallocated = @serialsallocated + 1
                end
            end
        end
      
      if ((@ITMTRKOP = 2)
          and (@ITEMTYPE = 1)
          and (@I_vNONINVEN = 0)
          and ((@USPFULPR = 0)
                or (@I_vQTYFULFI > 0))
          and (@I_vQTYFULFI <> 0)
          and (@I_vSOPTYPE in (2,3,6))
          and (@LOCNCODE <> ''))
        begin
          select @ALLOCATED = isnull(count(SERLTQTY),0)
          from   SOP10201 (nolock)
          where  SOPTYPE = @I_vSOPTYPE
                 and SOPNUMBE = @I_vSOPNUMBE
                 and ITEMNMBR = @I_vITEMNMBR
                 and LNITMSEQ = @I_vLNITMSEQ
          
          if (@SERIALQUANTITY <> @ALLOCATED)
            begin
              select @O_iErrorState = 1540
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      
      if ((@ITEMTYPE = 3)
          and (@I_vSOPTYPE in (2,3))
          and (@I_vQtyShrtOpt <> 2))
        begin
          declare KitComponent insensitive CURSOR  for
          select CMPTITNM,
                 CMPITQTY,
                 CMPITUOM
          from   IV00104 a (nolock),
                 IV00101 b (nolock)
          where  a.ITEMNMBR = @I_vITEMNMBR
                 and a.CMPTITNM = b.ITEMNMBR
                 and b.ITEMTYPE not in (4,5,6)
          
          set nocount on
          
          OPEN KitComponent
          
          select @iCursorError = @@cursor_rows
          
          if @iCursorError = 0
            begin
              deallocate KitComponent
            end
          else
            begin
              FETCH NEXT FROM KitComponent
              INTO @KitComponent,
                   @sCMPITQTY,
                   @CMPITUOM
              
              WHILE (@@FETCH_STATUS <> -1)
                begin
                  if (@@fetch_status = -2)
                    begin
                      select @O_iErrorState = 588
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      break
                    end
                  
                  select @CompQTYBSUOM = QTYBSUOM
                  from   IV40202 (nolock)
                  where  UOMSCHDL = @UOMSCHDL
                         and UOFM = @CMPITUOM
                  
                  select @QTYAVL = case 
                                     when ATYALLOC >= 0 then QTYONHND - ATYALLOC - ((((@I_vQUANTITY) * (@sCMPITQTY)) * (@QTYBSUOM)) * (@CompQTYBSUOM))
                                     else QTYONHND + ATYALLOC - ((((@I_vQUANTITY) * (@sCMPITQTY)) * (@QTYBSUOM)) * (@CompQTYBSUOM))
                                   end
                  from   IV00102 (nolock)
                  where  ITEMNMBR = @KitComponent
                         and LOCNCODE = @I_vLOCNCODE
                  
                  if (@QTYAVL < 0)
                    begin
                      if (@I_vQtyShrtOpt = 1)
                        begin
                          select @QTYONHND = case 
                                               when ATYALLOC >= 0 then QTYONHND - ATYALLOC
                                               else QTYONHND + ATYALLOC
                                             end
                          from   IV00102 (nolock)
                          where  ITEMNMBR = @KitComponent
                                 and LOCNCODE = @I_vLOCNCODE
                          
                          select @LowQUANTITY = round((@QTYONHND / @sCMPITQTY),@DECPLQTY,1)
                          
                          if (@LowQUANTITY < @I_vQUANTITY)
                            begin
                              select @I_vQUANTITY = @LowQUANTITY
                              
                              if (@I_vQUANTITY < 0)
                                begin
                                  select @I_vQUANTITY = 0
                                end
                            end
                        end
                      else
                        begin
                          if (@I_vQtyShrtOpt = 3)
                            begin
                              select @I_vQTYTBAOR = @I_vQTYTBAOR + @I_vQUANTITY,
                                     @I_vQUANTITY = 0
                              
                              break
                            end
                          else
                            begin
                              if (@I_vQtyShrtOpt = 5)
                                begin
                                  select @I_vQTYCANCE = @I_vQTYCANCE + @I_vQUANTITY,
                                         @I_vQUANTITY = 0
                                  
                                  break
                                end
                            end
                        end
                    end
                  
                  FETCH NEXT FROM KitComponent
                  INTO @KitComponent,
                       @sCMPITQTY,
                       @CMPITUOM
                end
              
              deallocate KitComponent
            end
        end
      
      select @MAXSEG = max(SGMTNUMB)
      from   SY00300 (nolock)
      
      select @Location_Segment = isnull(Location_Segment,'')
      from   IV40700 (nolock)
      where  LOCNCODE = @I_vLOCNCODE
      
      select @ACSGFLOC = ACSGFLOC,
             @ENABLEMULTIBIN = ENABLEMULTIBIN
      from   IV40100 (nolock)
      where  SETUPKEY = 1
      
      if ((@INVINDX <> 0)
          and (@Location_Segment <> '')
          and (@ACSGFLOC <> 0))
        begin
          select @ACTINDX = 0,
                 @ACCNT_STRING = '',
                 @NEW_ACCNT_STRING = ''
          
          select @ACCNT_STRING = ACTNUMST
          from   GL00105 (nolock)
          where  ACTINDX = @INVINDX
          
          if (@ACSGFLOC < @MAXSEG)
            begin
              select @LOFSGMNT = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB >= @ACSGFLOC
              
              select @LOFSGMNTEND = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB > @ACSGFLOC
            end
          else
            begin
              select @LOFSGMNT = sum(LOFSGMNT)
              from   SY00300 (nolock)
              where  SGMTNUMB = @ACSGFLOC
            end
          
          select @LOFSGMNTALL = sum(LOFSGMNT) + (@MAXSEG - 1)
          from   SY00300 (nolock)
          
          if (@ACSGFLOC = @MAXSEG)
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment))
            end
          else
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment)) + substring(@ACCNT_STRING,@LOFSGMNTALL - @LOFSGMNTEND + 1,
                                                                                                                                           @LOFSGMNTALL)
            end
          
          select @ACTINDX = isnull(ACTINDX,0)
          from   GL00105 (nolock)
          where  ACTNUMST = @NEW_ACCNT_STRING
          
          if (@ACTINDX <> 0)
            select @INVINDX = @ACTINDX
        end
      
      if ((@MKDNINDX <> 0)
          and (@Location_Segment <> '')
          and (@ACSGFLOC <> 0))
        begin
          select @ACTINDX = 0,
                 @ACCNT_STRING = '',
                 @NEW_ACCNT_STRING = ''
          
          select @ACCNT_STRING = ACTNUMST
          from   GL00105 (nolock)
          where  ACTINDX = @MKDNINDX
          
          if (@ACSGFLOC < @MAXSEG)
            begin
              select @LOFSGMNT = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB >= @ACSGFLOC
              
              select @LOFSGMNTEND = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB > @ACSGFLOC
            end
          else
            begin
              select @LOFSGMNT = sum(LOFSGMNT)
              from   SY00300 (nolock)
              where  SGMTNUMB = @ACSGFLOC
            end
          
          select @LOFSGMNTALL = sum(LOFSGMNT) + (@MAXSEG - 1)
          from   SY00300 (nolock)
          
          if (@ACSGFLOC = @MAXSEG)
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment))
            end
          else
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment)) + substring(@ACCNT_STRING,@LOFSGMNTALL - @LOFSGMNTEND + 1,
                                                                                                                                           @LOFSGMNTALL)
            end
          
          select @ACTINDX = isnull(ACTINDX,0)
          from   GL00105 (nolock)
          where  ACTNUMST = @NEW_ACCNT_STRING
          
          if (@ACTINDX <> 0)
            select @MKDNINDX = @ACTINDX
        end
      
      if ((@RTNSINDX <> 0)
          and (@Location_Segment <> '')
          and (@ACSGFLOC <> 0))
        begin
          select @ACTINDX = 0,
                 @ACCNT_STRING = '',
                 @NEW_ACCNT_STRING = ''
          
          select @ACCNT_STRING = ACTNUMST
          from   GL00105 (nolock)
          where  ACTINDX = @RTNSINDX
          
          if (@ACSGFLOC < @MAXSEG)
            begin
              select @LOFSGMNT = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB >= @ACSGFLOC
              
              select @LOFSGMNTEND = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB > @ACSGFLOC
            end
          else
            begin
              select @LOFSGMNT = sum(LOFSGMNT)
              from   SY00300 (nolock)
              where  SGMTNUMB = @ACSGFLOC
            end
          
          select @LOFSGMNTALL = sum(LOFSGMNT) + (@MAXSEG - 1)
          from   SY00300 (nolock)
          
          if (@ACSGFLOC = @MAXSEG)
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment))
            end
          else
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment)) + substring(@ACCNT_STRING,@LOFSGMNTALL - @LOFSGMNTEND + 1,
                                                                                                                                           @LOFSGMNTALL)
            end
          
          select @ACTINDX = isnull(ACTINDX,0)
          from   GL00105 (nolock)
          where  ACTNUMST = @NEW_ACCNT_STRING
          
          if (@ACTINDX <> 0)
            select @RTNSINDX_2 = @ACTINDX
        end
      
      if ((@INUSINDX <> 0)
          and (@Location_Segment <> '')
          and (@ACSGFLOC <> 0))
        begin
          select @ACTINDX = 0,
                 @ACCNT_STRING = '',
                 @NEW_ACCNT_STRING = ''
          
          select @ACCNT_STRING = ACTNUMST
          from   GL00105 (nolock)
          where  ACTINDX = @INUSINDX
          
          if (@ACSGFLOC < @MAXSEG)
            begin
              select @LOFSGMNT = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB >= @ACSGFLOC
              
              select @LOFSGMNTEND = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB > @ACSGFLOC
            end
          else
            begin
              select @LOFSGMNT = sum(LOFSGMNT)
              from   SY00300 (nolock)
              where  SGMTNUMB = @ACSGFLOC
            end
          
          select @LOFSGMNTALL = sum(LOFSGMNT) + (@MAXSEG - 1)
          from   SY00300 (nolock)
          
          if (@ACSGFLOC = @MAXSEG)
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment))
            end
          else
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment)) + substring(@ACCNT_STRING,@LOFSGMNTALL - @LOFSGMNTEND + 1,
                                                                                                                                           @LOFSGMNTALL)
            end
          
          select @ACTINDX = isnull(ACTINDX,0)
          from   GL00105 (nolock)
          where  ACTNUMST = @NEW_ACCNT_STRING
          
          if (@ACTINDX <> 0)
            select @INUSINDX_2 = @ACTINDX
        end
      
      if ((@INSRINDX <> 0)
          and (@Location_Segment <> '')
          and (@ACSGFLOC <> 0))
        begin
          select @ACTINDX = 0,
                 @ACCNT_STRING = '',
                 @NEW_ACCNT_STRING = ''
          
          select @ACCNT_STRING = ACTNUMST
          from   GL00105 (nolock)
          where  ACTINDX = @INSRINDX
          
          if (@ACSGFLOC < @MAXSEG)
            begin
              select @LOFSGMNT = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB >= @ACSGFLOC
              
              select @LOFSGMNTEND = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB > @ACSGFLOC
            end
          else
            begin
              select @LOFSGMNT = sum(LOFSGMNT)
              from   SY00300 (nolock)
              where  SGMTNUMB = @ACSGFLOC
            end
          
          select @LOFSGMNTALL = sum(LOFSGMNT) + (@MAXSEG - 1)
          from   SY00300 (nolock)
          
          if (@ACSGFLOC = @MAXSEG)
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment))
            end
          else
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment)) + substring(@ACCNT_STRING,@LOFSGMNTALL - @LOFSGMNTEND + 1,
                                                                                                                                           @LOFSGMNTALL)
            end
          
          select @ACTINDX = isnull(ACTINDX,0)
          from   GL00105 (nolock)
          where  ACTNUMST = @NEW_ACCNT_STRING
          
          if (@ACTINDX <> 0)
            select @INSRINDX = @ACTINDX
        end
      
      if ((@DMGDINDX <> 0)
          and (@Location_Segment <> '')
          and (@ACSGFLOC <> 0))
        begin
          select @ACTINDX = 0,
                 @ACCNT_STRING = '',
                 @NEW_ACCNT_STRING = ''
          
          select @ACCNT_STRING = ACTNUMST
          from   GL00105 (nolock)
          where  ACTINDX = @DMGDINDX
          
          if (@ACSGFLOC < @MAXSEG)
            begin
              select @LOFSGMNT = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB >= @ACSGFLOC
              
              select @LOFSGMNTEND = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB > @ACSGFLOC
            end
          else
            begin
              select @LOFSGMNT = sum(LOFSGMNT)
              from   SY00300 (nolock)
              where  SGMTNUMB = @ACSGFLOC
            end
          
          select @LOFSGMNTALL = sum(LOFSGMNT) + (@MAXSEG - 1)
          from   SY00300 (nolock)
          
          if (@ACSGFLOC = @MAXSEG)
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment))
            end
          else
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment)) + substring(@ACCNT_STRING,@LOFSGMNTALL - @LOFSGMNTEND + 1,
                                                                                                                                           @LOFSGMNTALL)
            end
          
          select @ACTINDX = isnull(ACTINDX,0)
          from   GL00105 (nolock)
          where  ACTNUMST = @NEW_ACCNT_STRING
          
          if (@ACTINDX <> 0)
            select @DMGDINDX = @ACTINDX
        end
      
      if ((@CSLSINDX <> 0)
          and (@Location_Segment <> '')
          and (@ACSGFLOC <> 0))
        begin
          select @ACTINDX = 0,
                 @ACCNT_STRING = '',
                 @NEW_ACCNT_STRING = ''
          
          select @ACCNT_STRING = ACTNUMST
          from   GL00105 (nolock)
          where  ACTINDX = @CSLSINDX
          
          if (@ACSGFLOC < @MAXSEG)
            begin
              select @LOFSGMNT = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB >= @ACSGFLOC
              
              select @LOFSGMNTEND = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB > @ACSGFLOC
            end
          else
            begin
              select @LOFSGMNT = sum(LOFSGMNT)
              from   SY00300 (nolock)
              where  SGMTNUMB = @ACSGFLOC
            end
          
          select @LOFSGMNTALL = sum(LOFSGMNT) + (@MAXSEG - 1)
          from   SY00300 (nolock)
          
          if (@ACSGFLOC = @MAXSEG)
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment))
            end
          else
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment)) + substring(@ACCNT_STRING,@LOFSGMNTALL - @LOFSGMNTEND + 1,
                                                                                                                                           @LOFSGMNTALL)
            end
          
          select @ACTINDX = isnull(ACTINDX,0)
          from   GL00105 (nolock)
          where  ACTNUMST = @NEW_ACCNT_STRING
          
          if (@ACTINDX <> 0)
            select @CSLSINDX = @ACTINDX
        end
      
      if ((@SLSINDX <> 0)
          and (@Location_Segment <> '')
          and (@ACSGFLOC <> 0))
        begin
          select @ACTINDX = 0,
                 @ACCNT_STRING = '',
                 @NEW_ACCNT_STRING = ''
          
          select @ACCNT_STRING = ACTNUMST
          from   GL00105 (nolock)
          where  ACTINDX = @SLSINDX
          
          if (@ACSGFLOC < @MAXSEG)
            begin
              select @LOFSGMNT = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB >= @ACSGFLOC
              
              select @LOFSGMNTEND = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
              from   SY00300 (nolock)
              where  SGMTNUMB > @ACSGFLOC
            end
          else
            begin
              select @LOFSGMNT = sum(LOFSGMNT)
              from   SY00300 (nolock)
              where  SGMTNUMB = @ACSGFLOC
            end
          
          select @LOFSGMNTALL = sum(LOFSGMNT) + (@MAXSEG - 1)
          from   SY00300 (nolock)
          
          if (@ACSGFLOC = @MAXSEG)
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment))
            end
          else
            begin
              select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment)) + substring(@ACCNT_STRING,@LOFSGMNTALL - @LOFSGMNTEND + 1,
                                                                                                                                           @LOFSGMNTALL)
            end
          
          select @ACTINDX = isnull(ACTINDX,0)
          from   GL00105 (nolock)
          where  ACTNUMST = @NEW_ACCNT_STRING
          
          if (@ACTINDX <> 0)
            select @SLSINDX = @ACTINDX
        end
      
      if (@I_vRequesterTrx = 0)
        begin
          exec @iStatus = eConnectOutVerify
            @I_vDOCTYPE = 'Sales_Transaction' ,
            @I_vINDEX1 = @I_vSOPNUMBE ,
            @I_vINDEX2 = @I_vSOPTYPE ,
            @I_vINDEX3 = '' ,
            @I_vINDEX4 = '' ,
            @I_vINDEX5 = '' ,
            @I_vINDEX6 = '' ,
            @I_vINDEX7 = '' ,
            @I_vINDEX8 = '' ,
            @I_vINDEX9 = '' ,
            @I_vINDEX10 = '' ,
            @I_vINDEX11 = '' ,
            @I_vINDEX12 = '' ,
            @I_vINDEX13 = '' ,
            @I_vINDEX14 = '' ,
            @I_vINDEX15 = '' ,
            @I_vDelete = 0 ,
            @O_iErrorState = @iCustomState output
          
          select @iError = @@error
          
          if ((@iStatus <> 0)
               or (@iCustomState <> 0)
               or (@iError <> 0))
            begin
              select @oErrString = rtrim(@oErrString) + ' ' + @iCustomState
              
              select @O_iErrorState = 2019
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      
      if (@ISMCTRX = 1)
        begin
          select @NEWDECPL = @EDITDECPLCUR
        end
      else
        begin
          select @NEWDECPL = @FUNDECPLCUR
        end
      
      if (@I_vUNITCOST is null)
        begin
          select @UNITCOST = round((@UNITCOST * @QTYBSUOM),@ITEMFUNCTDEC)
        end
      
      if (@I_vSOPTYPE = 3)
        begin
          select @EXTDCOST = round(@I_vQUANTITY * @UNITCOST,@FUNDECPLCUR) --Bug ID#14090 switched from @EDITDECPLCUR to @FUNDECPLCUR
        end
      else
        begin
          select @EXTDCOST = round((@I_vTOTALQTY) * @UNITCOST,@FUNDECPLCUR) --Bug ID#14090 switched from @EDITDECPLCUR to @FUNDECPLCUR
        end
      
      if (@SHIPTYPE = 0)
        begin
          select @TXSCHSRC = 1
        end
      else
        if (@SHIPTYPE = 1)
          begin
            select @TXSCHSRC = 2
          end
        else
          if (@SHIPTYPE = -1)
            begin
              select @TXSCHSRC = 0
            end
      
      if (@QTYFULFI = -1)
        begin
          select @I_vQTYFULFI = 0
        end
      
      if ((@ITEMTYPE = 3)
          and (@I_vNONINVEN = 0)
          and (@I_vKitCompMan = 0))
        begin
          select @sCMPITQTY = 0
          
          if (not exists (select 1
                          from   tempdb..sysobjects
                          where  id = (select object_id('tempdb..#eConnectCalledFromSopLine'))))
            begin
              create table #eConnectCalledFromSopLine (
                KITITEMNMBR char(30),
                ALLOCATED   numeric(19,5),
                CURRNIDX    smallint,
                DROPSHIP    smallint,
                ReqShipDate datetime)
            end
          
          declare ItemComponents INSENSITIVE cursor  for
          select   CMPTITNM,
                   CMPITQTY,
                   CMPITUOM
          from     IV00104 (nolock)
          where    ITEMNMBR = @I_vITEMNMBR
          order by SEQNUMBR
          
          open ItemComponents
          
          select @iCursorError = @@cursor_rows
          
          if @iCursorError = 0
            begin
              if (exists (select 1
                          from   tempdb..sysobjects
                          where  id = (select object_id('tempdb..#eConnectCalledFromSopLine'))))
                begin
                  drop table #eConnectCalledFromSopLine
                end
              
              deallocate ItemComponents
            end
          else
            begin
              fetch next from ItemComponents
              into @CMPTITNM,
                   @sCMPITQTY,
                   @CMPITUOM
              
              while (@@fetch_status <> -1)
                begin
                  if (@@fetch_status = -2)
                    begin
                      select @O_iErrorState = 20989
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      break
                    end
                  
                  select @SEQNUMBR = @SEQNUMBR + 16384
                  
                  if ((@I_vSOPTYPE in (2,3,6))
                      and (@USPFULPR = 0)
                      and (@QTYFULFI = -1)
                      and ((@ALLOCABY = 1)
                            or (@I_vALLOCATE = 1)))
                    begin
                      select @CMPQTYFULFI = @I_vQUANTITY - @I_vQTYCANCE - @I_vQTYTBAOR - @QTYTOBO - @QTYPRBAC - @QTYPRBOO - @QTYPRINV - @QTYPRORD
                      
                      if (@CMPQTYFULFI < 0)
                        begin
                          select @CMPQTYFULFI = 0
                        end
                    end
                  else
                    begin
                      select @CMPQTYFULFI = @I_vQTYFULFI
                    end
                  
                  select @CMPQUANTITY = ((@I_vQUANTITY) * (@sCMPITQTY)) * @QTYBSUOM,
                         @CMPQTYTBAOR = ((@I_vQTYTBAOR) * (@sCMPITQTY)) * @QTYBSUOM,
                         @CMPQTYCANCE = ((@I_vQTYCANCE) * (@sCMPITQTY)) * @QTYBSUOM,
                         @CMPQTYFULFI = ((@CMPQTYFULFI) * (@sCMPITQTY)) * @QTYBSUOM,
                         @CMPQUOTEQTYTOINV = ((@I_vQUOTEQTYTOINV) * (@sCMPITQTY)) * @QTYBSUOM,
                         @CMPQTYONHND = ((@I_vQTYONHND) * (@sCMPITQTY)) * @QTYBSUOM,
                         @CMPQTYRTRND = ((@I_vQTYRTRND) * (@sCMPITQTY)) * @QTYBSUOM,
                         @CMPQTYINUSE = ((@I_vQTYINUSE) * (@sCMPITQTY)) * @QTYBSUOM,
                         @CMPQTYINSVC = ((@I_vQTYINSVC) * (@sCMPITQTY)) * @QTYBSUOM,
                         @CMPQTYDMGED = ((@I_vQTYDMGED) * (@sCMPITQTY)) * @QTYBSUOM
                  
                  delete #eConnectCalledFromSopLine
                  
                  insert #eConnectCalledFromSopLine
                  select @I_vITEMNMBR,
                         case 
                           when ((@I_vNONINVEN = 0)
                                 and (@I_vDROPSHIP = 0)
                                 and (@I_vSOPTYPE in (2,3,6))
                                 and (@ITEMTYPE in (1,2,3))
                                 and ((@ALLOCABY = 1)
                                       or (@I_vALLOCATE = 1))) then @I_vTOTALQTY - @I_vQTYCANCE - @I_vQTYTBAOR - @QTYTOBO - @QTYPRBAC - @QTYPRBOO - @QTYPRINV - @QTYPRORD
                           else 0
                         end,
                         @CURRNIDX,
                         @I_vDROPSHIP,
                         case 
                           when (@I_vReqShipDate = '') then @I_vDOCDATE
                           else @I_vReqShipDate
                         end
                  
                  if @@error <> 0
                    begin
                      select @O_iErrorState = 6538
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      break
                    end
                  
                  exec @iStatus = taSopLineIvcInsertComponent
                    @I_vSOPTYPE = @I_vSOPTYPE ,
                    @I_vSOPNUMBE = @I_vSOPNUMBE ,
                    @I_vLOCNCODE = @I_vLOCNCODE ,
                    @I_vLNITMSEQ = @I_vLNITMSEQ ,
                    @I_vITEMNMBR = @CMPTITNM ,
                    @I_vQUANTITY = @CMPQUANTITY ,
                    @I_vQTYTBAOR = @CMPQTYTBAOR ,
                    @I_vQTYCANCE = @CMPQTYCANCE ,
                    @I_vQTYFULFI = @CMPQTYFULFI ,
                    @I_vQUOTEQTYTOINV = @CMPQUOTEQTYTOINV ,
                    @I_vQTYONHND = @CMPQTYONHND ,
                    @I_vQTYRTRND = @CMPQTYRTRND ,
                    @I_vQTYINUSE = @CMPQTYINUSE ,
                    @I_vQTYINSVC = @CMPQTYINSVC ,
                    @I_vQTYDMGED = @CMPQTYDMGED ,
                    @I_vCUSTNMBR = @I_vCUSTNMBR ,
                    @I_vDOCID = @I_vDOCID ,
                    @I_vAUTOALLOCATESERIAL = @I_vAUTOALLOCATESERIAL ,
                    @I_vAUTOALLOCATELOT = @I_vAUTOALLOCATELOT ,
                    @I_vCMPNTSEQ = @SEQNUMBR ,
                    @I_vCMPITUOM = @CMPITUOM ,
                    @I_vCURNCYID = @I_vCURNCYID ,
                    @I_vRequesterTrx = 1 ,
                    @O_iErrorState = @O_iLineIvcInsrtErrState output ,
                    @oErrString = @oErrString output
                  
                  select @iError = @@error
                  
                  if ((@iStatus <> 0)
                       or (@O_iLineIvcInsrtErrState <> 0)
                       or (@iError <> 0))
                    begin
                      select @O_iErrorState = 115
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      break
                    end
                  
                  fetch next from ItemComponents
                  into @CMPTITNM,
                       @sCMPITQTY,
                       @CMPITUOM
                end
              
              if (exists (select 1
                          from   tempdb..sysobjects
                          where  id = (select object_id('tempdb..#eConnectCalledFromSopLine'))))
                begin
                  drop table #eConnectCalledFromSopLine
                end
              
              deallocate ItemComponents
              
              select @EXTDCOST = isnull(round(sum(EXTDCOST),@NEWDECPL),0)
              from   SOP10200 (nolock)
              where  SOPTYPE = @I_vSOPTYPE
                     and SOPNUMBE = @I_vSOPNUMBE
                     and LNITMSEQ = @I_vLNITMSEQ
                     and CMPNTSEQ > 0
              
              if (@I_vQUANTITY = 0)
                begin
                  select @UNITCOST = 0
                end
              else
                begin
                  select @UNITCOST = abs(isnull(round(sum(UNITCOST * QUANTITY) / @I_vQUANTITY,@ITEMFUNCTDEC),
                                                0))
                  from   SOP10200 (nolock)
                  where  SOPTYPE = @I_vSOPTYPE
                         and SOPNUMBE = @I_vSOPNUMBE
                         and LNITMSEQ = @I_vLNITMSEQ
                         and CMPNTSEQ <> 0
                end
            end
        end
      
      select @QTYREMAI = @I_vTOTALQTY - @I_vQTYCANCE - @QTYPRBAC - @QTYPRBOO - @QTYPRINV - @QTYPRORD
      
      insert SOP10200
            (GPSFOINTEGRATIONID,
             INTEGRATIONSOURCE,
             INTEGRATIONID,
             ITMTSHID,
             NONINVEN,
             IVITMTXB,
             TAXAMNT,
             ORTAXAMT,
             PRCLEVEL,
             UOFM,
             SOPTYPE,
             SOPNUMBE,
             LNITMSEQ,
             ITEMNMBR,
             ITEMDESC,
             LOCNCODE,
             COMMNTID,
             UNITCOST,
             ORUNTCST,
             UNITPRCE,
             ORUNTPRC,
             XTNDPRCE,
             OXTNDPRC,
             REMPRICE,
             OREPRICE,
             EXTDCOST,
             OREXTCST,
             MRKDNAMT,
             ORMRKDAM,
             MRKDNPCT,    --Suggestion ID#7111
             MRKDNTYP,
             INVINDX,
             CSLSINDX,
             SLSINDX,
             MKDNINDX,
             RTNSINDX,
             INUSINDX,
             INSRINDX,
             DMGDINDX,
             QUANTITY,
             ATYALLOC,
             QTYONHND,
             QTYCANCE,
             QTYRTRND,
             QTYINUSE,
             QTYINSVC,
             QTYDMGED,
             QTYPRBAC,
             QTYPRBOO,
             QTYPRINV,
             QTYPRORD,
             QTYREMAI,
             QTYTOINV,
             QTYTORDR,
             QTYORDER,
             QTYFULFI,
             QTYSLCTD,
             QTYBSUOM,
             EXTQTYAL,
             EXTQTYSEL,
             ReqShipDate,
             FUFILDAT,
             ACTLSHIP,
             SHIPMTHD,
             SALSTERR,
             SLPRSNID,
             CURRNIDX,
             PURCHSTAT,
             QTYONPO,
             QTYRECVD,
             QTYPRVRECVD,
             DECPLQTY,
             DECPLCUR,
             ODECPLCU,
             DROPSHIP,
             QTYTBAOR,
             TAXSCHID,
             TXSCHSRC,
             PRSTADCD,
             ShipToName,
             CNTCPRSN,
             ADDRESS1,
             ADDRESS2,
             ADDRESS3,
             CITY,
             STATE,
             ZIPCODE,
             CCode,
             COUNTRY,
             PHONE1,
             PHONE2,
             PHONE3,
             FAXNUMBR,
             EXCEPTIONALDEMAND,
             BULKPICKPRNT,
             INDPICKPRNT,
             ISLINEINTRA,
             SOFULFILLMENTBIN,
             MULTIPLEBINS,
             Flags)
      select @I_vGPSFOINTEGRATIONID,
             @I_vINTEGRATIONSOURCE,
             @I_vINTEGRATIONID,
             @I_vITMTSHID,
             @I_vNONINVEN,
             @I_vIVITMTXB,
             @I_vTAXAMNT,
             @I_vTAXAMNT,
             @I_vPRCLEVEL,
             @I_vUOFM,
             @I_vSOPTYPE,
             @I_vSOPNUMBE,
             @I_vLNITMSEQ,
             @I_vITEMNMBR,
             case 
               when @I_vITEMDESC <> '' then @I_vITEMDESC
               else @ITEMDESC
             end,
             @I_vLOCNCODE,
             @I_vCOMMNTID,
             case 
               when ((@ISMCTRX = 0)
                      or (@I_vUNITCOST is null)) then @UNITCOST
               else 0
             end,
             case 
               when ((@ISMCTRX = 0)
                      or (@I_vUNITCOST is not null)) then @UNITCOST
               else 0
             end,
             @I_vUNITPRCE,
             case 
               when @ISMCTRX = 1
                    and @fieldupdate = 1 then 0
               else @I_vUNITPRCE
             end,
             case 
               when ((@ITEMTYPE = 3)
                     and (@I_vSOPTYPE = 3)
                     and ((@I_vQTYTBAOR <> 0)
                           or (@QTYTOBO <> 0))) then 0
               when ((@I_vQtyShrtOpt not in (1,2))
                     and (@I_vSOPTYPE = 3)
                     and (@ITEMTYPE <> 3)
                     and (@ISMCTRX = 1)
                     and ((@I_vQTYTBAOR <> 0)
                           or (@QTYTOBO <> 0)
                           or (@I_vQTYCANCE <> 0)))--baj added 
                then round((@I_vUNITPRCE * @I_vQUANTITY),@DECPLCURMINUS1)
               when ((@I_vQtyShrtOpt not in (1,2))
                     and (@I_vSOPTYPE = 3)
                     and (@ITEMTYPE <> 3)
                     and (@ISMCTRX = 0)
                     and ((@I_vQTYTBAOR <> 0)
                           or (@QTYTOBO <> 0)
                           or (@I_vQTYCANCE <> 0)))--baj added 
                then round((@I_vUNITPRCE * @I_vQUANTITY),@FUNDECPLCUR)
               when (@I_vSOPTYPE = 3)
                    and (@I_vQtyShrtOpt = 1)
                    and (@ISMCTRX = 1) then round((@I_vUNITPRCE * @I_vTOTALQTY),@DECPLCURMINUS1)
               when (@I_vSOPTYPE = 3)
                    and (@I_vQtyShrtOpt = 1)
                    and (@ISMCTRX = 0) then round((@I_vUNITPRCE * @I_vTOTALQTY),@FUNDECPLCUR)
               else @I_vXTNDPRCE
             end,
             case 
               when ((@ITEMTYPE = 3)
                     and (@ISMCTRX = 1)
                     and (@fieldupdate = 1)) then 0
               when ((@ITEMTYPE = 3)
                     and (@I_vSOPTYPE = 3)
                     and ((@I_vQTYTBAOR <> 0)
                           or (@QTYTOBO <> 0))) then 0
               when ((@I_vQtyShrtOpt not in (1,2))
                     and (@I_vSOPTYPE = 3)
                     and (@ITEMTYPE <> 3)
                     and (@ISMCTRX = 1)
                     and ((@I_vQTYTBAOR <> 0)
                           or (@QTYTOBO <> 0)
                           or (@I_vQTYCANCE <> 0)))--baj added 
                then round((@I_vUNITPRCE * @I_vQUANTITY),@DECPLCURMINUS1)
               when ((@I_vQtyShrtOpt not in (1,2))
                     and (@I_vSOPTYPE = 3)
                     and (@ITEMTYPE <> 3)
                     and (@ISMCTRX = 0)
                     and ((@I_vQTYTBAOR <> 0)
                           or (@QTYTOBO <> 0)
                           or (@I_vQTYCANCE <> 0)))--baj added 
                then round((@I_vUNITPRCE * @I_vQUANTITY),@FUNDECPLCUR)
               when (@I_vSOPTYPE = 3)
                    and (@I_vQtyShrtOpt = 1)
                    and (@ISMCTRX = 1) then round((@I_vUNITPRCE * @I_vTOTALQTY),@DECPLCURMINUS1)
               when (@I_vSOPTYPE = 3)
                    and (@I_vQtyShrtOpt = 1)
                    and (@ISMCTRX = 0) then round((@I_vUNITPRCE * @I_vTOTALQTY),@FUNDECPLCUR)
               else @I_vXTNDPRCE
             end,
             case 
               when ((@QTYPRBAC > 0)
                      or (@QTYPRBOO > 0)
                      or (@QTYPRINV > 0)
                      or (@QTYPRORD > 0)) then round((@I_vTOTALQTY - @I_vQTYCANCE) * (@I_vUNITPRCE - @I_vMRKDNAMT),
                                                     @NEWDECPL) - @REMPRICE
               when (@REMPRICE <= 0)
                    and (@ITEMTYPE = 3)
                    and (@I_vSOPTYPE = 3)   --Bug ID#18822 
                then 0
               else round((@I_vTOTALQTY - @I_vQTYCANCE) * (@I_vUNITPRCE - @I_vMRKDNAMT),
                          @NEWDECPL)
             end,
             case 
               when ((@QTYPRBAC > 0)
                      or (@QTYPRBOO > 0)
                      or (@QTYPRINV > 0)
                      or (@QTYPRORD > 0)) then round((@I_vTOTALQTY - @I_vQTYCANCE) * (@I_vUNITPRCE - @I_vMRKDNAMT),
                                                     @NEWDECPL) - @REMPRICE
               when (@REMPRICE <= 0)
                    and (@ITEMTYPE = 3)
                    and (@I_vSOPTYPE = 3)   --Bug ID#18822
                then 0
               else round((@I_vTOTALQTY - @I_vQTYCANCE) * (@I_vUNITPRCE - @I_vMRKDNAMT),
                          @NEWDECPL)
             end,
             case 
               when ((@ISMCTRX = 0)
                      or (@I_vUNITCOST is null)) then @EXTDCOST
               else 0
             end,
             case 
               when ((@ISMCTRX = 0)
                      or (@I_vUNITCOST is not null)) then @EXTDCOST
               else 0
             end,
             @I_vMRKDNAMT,
             @I_vMRKDNAMT,
             @I_vMRKDNPCT * 100,
             case 
               when (@I_vMRKDNAMT > 0)
                    and (@I_vMRKDNPCT = 0) then 1
               else 0
             end,
             case 
               when (@INVINDX_2 <> 0) then @INVINDX_2
               else @INVINDX
             end,
             case 
               when (@CSLSINDX_2 <> 0) then @CSLSINDX_2
               else @CSLSINDX
             end,
             case 
               when (@SLSINDX_2 <> 0) then @SLSINDX_2
               else @SLSINDX
             end,
             case 
               when (@MKDNINDX_2 <> 0) then @MKDNINDX_2
               else @MKDNINDX
             end,
             case 
               when (@RTNSINDX_2 <> 0) then @RTNSINDX_2
               else @RTNSINDX
             end,
             case 
               when (@INUSINDX_2 <> 0) then @INUSINDX_2
               else @INUSINDX
             end,
             case 
               when (@INSRINDX_2 <> 0) then @INSRINDX_2
               else @INSRINDX
             end,
             case 
               when (@DMGDINDX_2 <> 0) then @DMGDINDX_2
               else @DMGDINDX
             end,
             @I_vTOTALQTY,
             case 
               when (@I_vALLOCATE = 99) then 0
               when ((@I_vNONINVEN = 0)
                     and (@I_vDROPSHIP = 0)
                     and (@I_vSOPTYPE in (2,3,6))
                     and (@I_vQUANTITY > 0)
                     and (@ITEMTYPE in (1,2,3)) --Suggestion ID#10793 added (@I_vSOPTYPE in (2,3,6)) and removed (@I_vSOPTYPE > 1) and (@I_vSOPTYPE < 4) for SOP Fulfillment Orders
                     and ((@ALLOCABY = 1)
                           or (@I_vALLOCATE = 1))) then @I_vTOTALQTY - @I_vQTYCANCE - @I_vQTYTBAOR - @QTYTOBO - @QTYPRBAC - @QTYPRBOO - @QTYPRINV - @QTYPRORD
               else 0
             end,
             case 
               when (@I_vSOPTYPE = 4) then @I_vQTYONHND
               when (@I_vSOPTYPE = 3)
                    and (@I_vTOTALQTY < 0)
                    and (@ITEMTYPE in (1,2,3)) then @I_vTOTALQTY * -1
               else 0
             end,
             @I_vQTYCANCE,
             case 
               when (@I_vSOPTYPE = 4) then @I_vQTYRTRND
               else 0
             end,
             case 
               when (@I_vSOPTYPE = 4) then @I_vQTYINUSE
               else 0
             end,
             case 
               when (@I_vSOPTYPE = 4) then @I_vQTYINSVC
               else 0
             end,
             case 
               when (@I_vSOPTYPE = 4) then @I_vQTYDMGED
               else 0
             end,
             @QTYPRBAC,
             @QTYPRBOO,
             @QTYPRINV,
             @QTYPRORD,
             case 
               when (@QTYREMAI < 0)
                    and (@ITEMTYPE = 3)   --Bug ID#18822
                then 0
               else @QTYREMAI
             end,
             case 
               when ((@I_vSOPTYPE = 2)
                      or (@I_vSOPTYPE = 3)
                      or (@I_vSOPTYPE = 5)
                      or @I_vSOPTYPE = 6)
                    and (@I_vQUANTITY > 0) --Suggestion ID#10793 added or @I_vSOPTYPE = 6) for SOP Fulfillment Orders
                then @I_vQUANTITY
               when (@I_vSOPTYPE = 1) then @I_vQUOTEQTYTOINV
               else 0
             end,
             case 
               when (@I_vSOPTYPE = 1) then @I_vQUANTITY
               else 0
             end,
             case 
               when (@I_vSOPTYPE = 3
                      or @I_vSOPTYPE = 6)
                    and (@I_vTOTALQTY > 0)   --Suggestion ID#10793 added or @I_vSOPTYPE = 6 
                then @I_vTOTALQTY
               else 0
             end,
             case 
               when (@I_vALLOCATE = 99) then 0
               when (@I_vTOTALQTY < 0) then 0
               when ((@I_vSOPTYPE in (2,3,6))
                     and (@USPFULPR = 0)
                     and (@QTYFULFI = -1)
                     and ((@ALLOCABY = 1)
                           or (@I_vALLOCATE = 1))) then @I_vTOTALQTY - @I_vQTYCANCE - @I_vQTYTBAOR - @QTYTOBO - @QTYPRBAC - @QTYPRBOO - @QTYPRINV - @QTYPRORD
               else @I_vQTYFULFI
             end,
             case 
               when (@I_vALLOCATE = 99) then 0
               when (@I_vTOTALQTY < 0) then 0
               when (@ITEMTYPE = 3) then 0
               when ((@I_vSOPTYPE in (2,3,6))
                     and (@USPFULPR = 0)
                     and (@QTYFULFI = -1)
                     and ((@ALLOCABY = 1)
                           or (@I_vALLOCATE = 1))) then (@I_vTOTALQTY - @I_vQTYCANCE - @I_vQTYTBAOR - @QTYTOBO - @QTYPRBAC - @QTYPRBOO - @QTYPRINV - @QTYPRORD) * @QTYBSUOM
               else @I_vQTYFULFI * @QTYBSUOM
             end,
             case 
               when (@I_vNONINVEN = 0) then @QTYBSUOM
               else 1
             end,
             case 
               when ((@I_vSOPTYPE not in (1,4,5))
                     and (@ALLOCABY = 1)
                     and (@I_vQUANTITY >= 0)) then @EXTQTYAL
               else 0
             end,
             case 
               when (@I_vTOTALQTY < 0) then 0
               when (@ITMTRKOP = 1) then 0
               when ((@I_vSOPTYPE in (2,3,6))
                     and (@USPFULPR = 0)
                     and (@QTYFULFI = -1)
                     and ((@ALLOCABY = 1)
                           or (@I_vALLOCATE = 1))) then (@I_vTOTALQTY - @I_vQTYCANCE - @I_vQTYTBAOR - @QTYTOBO - @QTYPRBAC - @QTYPRBOO - @QTYPRINV - @QTYPRORD) * @QTYBSUOM
               else @I_vQTYFULFI * @QTYBSUOM    --added for Bug ID#15273
             end,
             case 
               when (@I_vReqShipDate = '') then @I_vDOCDATE
               else @I_vReqShipDate
             end,
             case 
               when ((@I_vSOPTYPE in (2,3,6))
                     and (@I_vFUFILDAT <> '')) then @I_vFUFILDAT
               when ((@I_vSOPTYPE in (2,3,6))
                     and (@I_vQUANTITY > 0)
                     and (((@USPFULPR = 0)
                           and (@QTYFULFI = -1))
                           or (@I_vQTYFULFI > 0))
                     and ((@ALLOCABY = 1)
                           or (@I_vALLOCATE = 1))) then @I_vDOCDATE
               else @dtDEFAULT
             end,
             case 
               when ((@I_vSOPTYPE in (2,3,6))
                     and (@I_vACTLSHIP <> '')) then @I_vACTLSHIP
               when ((@I_vSOPTYPE in (2,3,6))
                     and (@I_vQUANTITY > 0)
                     and (((@USPFULPR = 0)
                           and (@QTYFULFI = -1))
                           or (@I_vQTYFULFI > 0))
                     and ((@ALLOCABY = 1)
                           or (@I_vALLOCATE = 1))) then @I_vDOCDATE
               else @dtDEFAULT
             end,
             @I_vSHIPMTHD,
             @I_vSALSTERR,
             @I_vSLPRSNID,
             @CURRNIDX,
             case 
               when @I_vSOPTYPE = 5
                    and @QTYREMAI > 0 then 2
               when (@I_vSOPTYPE = 2
                     and (@I_vQTYTBAOR > 0
                           or @QTYTOBO > 0)) then 2
               when (@I_vSOPTYPE = 2
                     and @QTYREMAI > 0
                     and @ITEMTYPE in (0,3,4,5,
                                       6))  --Bug ID#18348 added and @ITEMTYPE in(0,3,4,5,6) 
                then 2
               else 1
             end,
             0,
             0,
             0,
             @DECPLQTY,
             @ITEMFUNCTDEC + 1,
             @DECPLCUR,
             @I_vDROPSHIP,
             (@QTYTOBO + @I_vQTYTBAOR),
             @I_vTAXSCHID,
             @TXSCHSRC,
             @I_vPRSTADCD,
             @I_vShipToName,
             @I_vCNTCPRSN,
             @I_vADDRESS1,
             @I_vADDRESS2,
             @I_vADDRESS3,
             @I_vCITY,
             @I_vSTATE,
             @I_vZIPCODE,
             '',
             @I_vCOUNTRY,
             @I_vPHONE1,
             @I_vPHONE2,
             @I_vPHONE3,
             @I_vFAXNUMBR,
             @I_vEXCEPTIONALDEMAND,
             0,
             0,
             0,
             '',
             0,
             0
      
      if @@error <> 0
        begin
          select @O_iErrorState = 112
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          break
        end
      
      if (@QuantityFree > 0
          and @I_vIncludePromo <> 0)
        begin
          select @I_vLNITMSEQFREE = @I_vLNITMSEQ + 16384
          
          exec @iStatus = taSopLineIvcInsert
            @I_vSOPTYPE = @I_vSOPTYPE ,
            @I_vSOPNUMBE = @I_vSOPNUMBE ,
            @I_vCUSTNMBR = @I_vCUSTNMBR ,
            @I_vDOCDATE = @I_vDOCDATE ,
            @I_vLOCNCODE = @I_vLOCNCODE ,
            @I_vITEMNMBR = @FREEITEM ,
            @I_vUNITPRCE = 0 ,
            @I_vXTNDPRCE = 0 ,
            @I_vQUANTITY = @QuantityFree ,
            @I_vMRKDNAMT = 0 ,
            @I_vCOMMNTID = @I_vCOMMNTID ,
            @I_vCOMMENT_1 = @I_vCOMMENT_1 ,
            @I_vCOMMENT_2 = @I_vCOMMENT_2 ,
            @I_vCOMMENT_3 = @I_vCOMMENT_3 ,
            @I_vCOMMENT_4 = @I_vCOMMENT_4 ,
            @I_vUNITCOST = 0 ,
            @I_vPRCLEVEL = @I_vPRCLEVEL ,
            @I_vITEMDESC = '' ,
            @I_vTAXAMNT = 0 ,
            @I_vQTYONHND = 0 ,
            @I_vQTYRTRND = 0 ,
            @I_vQTYINUSE = 0 ,
            @I_vQTYINSVC = 0 ,
            @I_vQTYDMGED = 0 ,
            @I_vNONINVEN = 0 ,
            @I_vLNITMSEQ = @I_vLNITMSEQFREE ,
            @I_vDROPSHIP = @I_vDROPSHIP ,
            @I_vQTYTBAOR = 0 ,
            @I_vDOCID = @I_vDOCID ,
            @I_vSALSTERR = @I_vSALSTERR ,
            @I_vSLPRSNID = @I_vSLPRSNID ,
            @I_vITMTSHID = '' ,
            @I_vIVITMTXB = 0 ,
            @I_vTAXSCHID = '' ,
            @I_vPRSTADCD = @I_vPRSTADCD ,
            @I_vShipToName = @I_vShipToName ,
            @I_vCNTCPRSN = @I_vCNTCPRSN ,
            @I_vADDRESS1 = @I_vADDRESS1 ,
            @I_vADDRESS2 = @I_vADDRESS2 ,
            @I_vADDRESS3 = @I_vADDRESS3 ,
            @I_vCITY = @I_vCITY ,
            @I_vSTATE = @I_vSTATE ,
            @I_vZIPCODE = @I_vZIPCODE ,
            @I_vCOUNTRY = @I_vCOUNTRY ,
            @I_vPHONE1 = @I_vPHONE1 ,
            @I_vPHONE2 = @I_vPHONE2 ,
            @I_vPHONE3 = @I_vPHONE3 ,
            @I_vFAXNUMBR = @I_vFAXNUMBR ,
            @I_vEXCEPTIONALDEMAND = @I_vEXCEPTIONALDEMAND ,
            @I_vReqShipDate = @I_vReqShipDate ,
            @I_vFUFILDAT = @I_vFUFILDAT ,
            @I_vACTLSHIP = @I_vACTLSHIP ,
            @I_vSHIPMTHD = @I_vSHIPMTHD ,
            @I_vINVINDX = '' ,
            @I_vCSLSINDX = '' ,
            @I_vSLSINDX = '' ,
            @I_vMKDNINDX = '' ,
            @I_vRTNSINDX = '' ,
            @I_vINUSINDX = '' ,
            @I_vINSRINDX = '' ,
            @I_vDMGDINDX = '' ,
            @I_vAUTOALLOCATESERIAL = @I_vAUTOALLOCATESERIAL ,
            @I_vAUTOALLOCATELOT = @I_vAUTOALLOCATELOT ,
            @I_vGPSFOINTEGRATIONID = @I_vGPSFOINTEGRATIONID ,
            @I_vINTEGRATIONSOURCE = @I_vINTEGRATIONSOURCE ,
            @I_vINTEGRATIONID = @I_vINTEGRATIONID ,
            @I_vRequesterTrx = 1 ,
            @I_vQTYCANCE = 0 ,
            @I_vQTYFULFI = null ,
            @I_vALLOCATE = @I_vALLOCATE ,
            @I_vUpdateIfExists = 0 ,
            @I_vRecreateDist = 0 ,
            @I_vQUOTEQTYTOINV = 0 ,
            @I_vTOTALQTY = 0 ,
            @I_vCMMTTEXT = @I_vCMMTTEXT ,
            @I_vDEFPRICING = 0 ,
            @I_vDEFEXTPRICE = 0 ,
            @I_vCURNCYID = @I_vCURNCYID ,
            @I_vUOFM = @FREEUOFM ,
            @I_vIncludePromo = 0 ,
            @I_vQtyShrtOpt = @I_vQtyShrtOpt ,
            @I_vUSRDEFND1 = @I_vUSRDEFND1 ,
            @I_vUSRDEFND2 = @I_vUSRDEFND2 ,
            @I_vUSRDEFND3 = @I_vUSRDEFND3 ,
            @I_vUSRDEFND4 = @I_vUSRDEFND4 ,
            @I_vUSRDEFND5 = @I_vUSRDEFND5 ,
            @O_iErrorState = @iCustomState output ,
            @oErrString = @oErrString output
          
          select @iError = @@error
          
          if ((@iStatus <> 0)
               or (@iCustomState <> 0)
               or (@iError <> 0))
            begin
              select @O_iErrorState = 3443
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      
      if (@I_vCOMMNTID <> '')
        begin
          if ((@I_vCMMTTEXT = '')
              and (@I_vCOMMENT_1 = '')
              and (@I_vCOMMENT_2 = '')
              and (@I_vCOMMENT_3 = '')
              and (@I_vCOMMENT_4 = ''))
            begin
              if (exists (select 1
                          from   SY04200 (nolock)
                          where  COMMNTID = @I_vCOMMNTID))
                begin
                  select @I_vCMMTTEXT = CMMTTEXT
                  from   SY04200 (nolock)
                  where  COMMNTID = @I_vCOMMNTID
                  
                  exec @iStatus = taParseCommentText
                    @I_vCMMTTEXT = @I_vCMMTTEXT ,
                    @O_COMMENT_1 = @I_vCOMMENT_1 output ,
                    @O_COMMENT_2 = @I_vCOMMENT_2 output ,
                    @O_COMMENT_3 = @I_vCOMMENT_3 output ,
                    @O_COMMENT_4 = @I_vCOMMENT_4 output
                  
                  select @iError = @@error
                  
                  if ((@iStatus <> 0)
                       or (@iError <> 0))
                    begin
                      select @O_iErrorState = 2276
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                    end
                  
                  insert SOP10202
                        (SOPNUMBE,
                         SOPTYPE,
                         LNITMSEQ,
                         COMMENT_1,
                         COMMENT_2,
                         COMMENT_3,
                         COMMENT_4,
                         CMMTTEXT)
                  select @I_vSOPNUMBE,
                         @I_vSOPTYPE,
                         @I_vLNITMSEQ,
                         @I_vCOMMENT_1,
                         @I_vCOMMENT_2,
                         @I_vCOMMENT_3,
                         @I_vCOMMENT_4,
                         @I_vCMMTTEXT
                  
                  if (@@error <> 0)
                    begin
                      select @O_iErrorState = 113
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      break
                    end
                end
              else
                begin
                  exec @iStatus = taCreateCommentMaster
                    3 ,
                    @I_vCOMMNTID ,
                    @I_vCMMTTEXT ,
                    @O_iErrorState = @O_iCommentMstErrState output
                  
                  select @iError = @@error
                  
                  if ((@iStatus <> 0)
                       or (@O_iCommentMstErrState <> 0)
                       or (@iError <> 0))
                    begin
                      select @oErrString = rtrim(@oErrString) + ' ' + @O_iCommentMstErrState
                      
                      select @O_iErrorState = 738
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      break
                    end
                end
            end
          else
            begin
              if (@I_vCMMTTEXT <> '')
                begin
                  exec @iStatus = taParseCommentText
                    @I_vCMMTTEXT = @I_vCMMTTEXT ,
                    @O_COMMENT_1 = @I_vCOMMENT_1 output ,
                    @O_COMMENT_2 = @I_vCOMMENT_2 output ,
                    @O_COMMENT_3 = @I_vCOMMENT_3 output ,
                    @O_COMMENT_4 = @I_vCOMMENT_4 output
                  
                  select @iError = @@error
                  
                  if ((@iStatus <> 0)
                       or (@iError <> 0))
                    begin
                      select @O_iErrorState = 2277
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                    end
                end
              else
                begin
                  select @I_vCMMTTEXT = rtrim(@I_vCOMMENT_1) + char(13) + rtrim(@I_vCOMMENT_2) + char(13) + rtrim(@I_vCOMMENT_3) + char(13) + rtrim(@I_vCOMMENT_4)
                end
              
              insert SOP10202
                    (SOPNUMBE,
                     SOPTYPE,
                     LNITMSEQ,
                     COMMENT_1,
                     COMMENT_2,
                     COMMENT_3,
                     COMMENT_4,
                     CMMTTEXT)
              select @I_vSOPNUMBE,
                     @I_vSOPTYPE,
                     @I_vLNITMSEQ,
                     @I_vCOMMENT_1,
                     @I_vCOMMENT_2,
                     @I_vCOMMENT_3,
                     @I_vCOMMENT_4,
                     @I_vCMMTTEXT
              
              if (@@error <> 0)
                begin
                  select @O_iErrorState = 114
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
              
              exec @iStatus = taCreateCommentMaster
                3 ,
                @I_vCOMMNTID ,
                @I_vCMMTTEXT ,
                @O_iErrorState = @O_iCommentMstErrState output
              
              select @iError = @@error
              
              if ((@iStatus <> 0)
                   or (@O_iCommentMstErrState <> 0)
                   or (@iError <> 0))
                begin
                  select @oErrString = rtrim(@oErrString) + ' ' + @O_iCommentMstErrState
                  
                  select @O_iErrorState = 3416
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
            end
        end
      else
        begin
          if ((@I_vCOMMENT_1 <> '')
               or (@I_vCOMMENT_2 <> '')
               or (@I_vCOMMENT_3 <> '')
               or (@I_vCOMMENT_4 <> '')
               or (@I_vCMMTTEXT <> ''))
            begin
              if (@I_vCMMTTEXT <> '')
                begin
                  exec @iStatus = taParseCommentText
                    @I_vCMMTTEXT = @I_vCMMTTEXT ,
                    @O_COMMENT_1 = @I_vCOMMENT_1 output ,
                    @O_COMMENT_2 = @I_vCOMMENT_2 output ,
                    @O_COMMENT_3 = @I_vCOMMENT_3 output ,
                    @O_COMMENT_4 = @I_vCOMMENT_4 output
                  
                  select @iError = @@error
                  
                  if ((@iStatus <> 0)
                       or (@iError <> 0))
                    begin
                      select @O_iErrorState = 2297
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                    end
                end
              else
                begin
                  select @I_vCMMTTEXT = rtrim(@I_vCOMMENT_1) + char(13) + rtrim(@I_vCOMMENT_2) + char(13) + rtrim(@I_vCOMMENT_3) + char(13) + rtrim(@I_vCOMMENT_4)
                end
              
              insert SOP10202
                    (SOPNUMBE,
                     SOPTYPE,
                     LNITMSEQ,
                     COMMENT_1,
                     COMMENT_2,
                     COMMENT_3,
                     COMMENT_4,
                     CMMTTEXT)
              select @I_vSOPNUMBE,
                     @I_vSOPTYPE,
                     @I_vLNITMSEQ,
                     @I_vCOMMENT_1,
                     @I_vCOMMENT_2,
                     @I_vCOMMENT_3,
                     @I_vCOMMENT_4,
                     @I_vCMMTTEXT
              
              if (@@error <> 0)
                begin
                  select @O_iErrorState = 593
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
            end
        end
      
      if ((@ITEMTYPE in (1,2,4,5,
                         6))
          and (@I_vSOPTYPE <> 1)
          and (@I_vSOPTYPE <> 4)
          and (@I_vSOPTYPE <> 5)
          and (@I_vDROPSHIP = 0)
          and (@LOCNCODE <> '')
          and (@I_vALLOCATE <> 99)
          and (@I_vQUANTITY >= 0))
        begin
          if ((@ALLOCABY = 1)
               or (@I_vALLOCATE = 1))
             and (@ITEMTYPE in (1,2))
            begin
              update IV00102
              set    ATYALLOC = ATYALLOC + ((@I_vTOTALQTY - @I_vQTYCANCE - @I_vQTYTBAOR - @QTYTOBO - @QTYPRBAC - @QTYPRBOO - @QTYPRINV - @QTYPRORD) * (@QTYBSUOM)),
                     QTYBKORD = QTYBKORD + ((@I_vQTYTBAOR + @QTYTOBO) * (@QTYBSUOM))
              from   IV00102 with (updlock index (PKIV00102 ))
              where  ITEMNMBR = @I_vITEMNMBR
                     and LOCNCODE = @I_vLOCNCODE
            end
          else
            begin
              update IV00102
              set    QTYBKORD = QTYBKORD + ((@I_vQTYTBAOR + @QTYTOBO) * (@QTYBSUOM))
              from   IV00102 with (updlock index (PKIV00102 ))
              where  ITEMNMBR = @I_vITEMNMBR
                     and LOCNCODE = @I_vLOCNCODE
            end
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 96
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
          
          if ((@ALLOCABY = 1)
               or (@I_vALLOCATE = 1))
             and (@ITEMTYPE in (1,2))
            begin
              update IV00102
              set    ATYALLOC = ATYALLOC + ((@I_vTOTALQTY - @I_vQTYCANCE - @I_vQTYTBAOR - @QTYTOBO - @QTYPRBAC - @QTYPRBOO - @QTYPRINV - @QTYPRORD) * (@QTYBSUOM)),
                     QTYBKORD = QTYBKORD + ((@I_vQTYTBAOR + @QTYTOBO) * (@QTYBSUOM))
              from   IV00102 with (updlock index (PKIV00102 ))
              where  ITEMNMBR = @I_vITEMNMBR
                     and LOCNCODE = ''
            end
          else
            begin
              update IV00102
              set    QTYBKORD = QTYBKORD + ((@I_vQTYTBAOR + @QTYTOBO) * (@QTYBSUOM))
              from   IV00102 with (updlock index (PKIV00102 ))
              where  ITEMNMBR = @I_vITEMNMBR
                     and LOCNCODE = ''
            end
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 97
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      
      if ((@I_vSOPTYPE = 5)
          and (@ITEMTYPE in (1,2,3,5))
          and (@LOCNCODE <> ''))
        begin
          update IV00102
          set    QTYBKORD = QTYBKORD + ((@I_vQUANTITY) * (@QTYBSUOM))
          from   IV00102 with (updlock index (PKIV00102 ))
          where  ITEMNMBR = @I_vITEMNMBR
                 and LOCNCODE = @I_vLOCNCODE
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 519
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
          
          update IV00102
          set    QTYBKORD = QTYBKORD + ((@I_vQUANTITY) * (@QTYBSUOM))
          from   IV00102 with (updlock index (PKIV00102 ))
          where  ITEMNMBR = @I_vITEMNMBR
                 and LOCNCODE = ''
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 520
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      
      if ((@ENABLEMULTIBIN = 1)
          and (@ITMTRKOP = 1)
          and (@I_vALLOCATE <> 99))
        begin
          if (@I_vQtyShrtOpt = 2)
            begin
              select @OverrideBin = 1
            end
          
          if (@I_vSOPTYPE = 4)
            begin
              if (@I_vAutoAssignBin = 99)
                begin
                  select @I_vAutoAssignBin = 0
                end
              else
                begin
                  select @I_vAutoAssignBin = 1
                end
            end
          
          if ((@ITEMTYPE = 1)
              and (@I_vNONINVEN = 0)
              and (@I_vSOPTYPE in (2,3,6)))  --Suggestion ID#10793 added 6 for SOP Fulfillment Orders
            begin
              if (@QTYFULFI <> -1)
                begin
                  select @BinFulfillQty = @I_vQTYFULFI * @QTYBSUOM
                end
              else
                begin
                  if ((((@ALLOCABY = 3)
                         or (@ALLOCABY = 2))
                       and (@I_vALLOCATE = 0))
                       or ((@ALLOCABY = 1)
                           and (@USPFULPR = 1)))
                    begin
                      select @BinFulfillQty = 0
                    end
                  else
                    begin
                      select @BinFulfillQty = case 
                                                when @ITMTRKOP = 1 then (@I_vTOTALQTY - @I_vQTYCANCE - @I_vQTYTBAOR - @QTYTOBO - @QTYPRBAC - @QTYPRBOO - @QTYPRINV - @QTYPRORD) * @QTYBSUOM
                                                when @ITMTRKOP = 2 then @serialstoallocate
                                                when @ITMTRKOP = 3 then @LotsToAllocate
                                                else 0
                                              end
                    end
                end
            end
          
          if (@I_vAutoAssignBin = 1)
            begin
              if exists (select 1
                         from   SOP10203 (nolock)
                         where  SOPTYPE = @I_vSOPTYPE
                                and SOPNUMBE = @I_vSOPNUMBE
                                and ITEMNMBR = @I_vITEMNMBR
                                and LNITMSEQ = @I_vLNITMSEQ)
                begin
                  select @O_iErrorState = 5988
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
              
              while (@BinQtyType < 6)
                begin
                  if (@I_vSOPTYPE = 4)
                    begin
                      select @BinFulfillQty = case @BinQtyType 
                                                when 1 then @I_vQTYONHND
                                                when 2 then @I_vQTYRTRND
                                                when 3 then @I_vQTYINUSE
                                                when 4 then @I_vQTYINSVC
                                                when 5 then @I_vQTYDMGED
                                                else @BinFulfillQty
                                              end
                    end
                  
                  if (@BinFulfillQty > 0)
                    begin
                      exec @iStatus = taSopMultiBin
                        @I_vSOPNUMBE = @I_vSOPNUMBE ,
                        @I_vSOPTYPE = @I_vSOPTYPE ,
                        @I_vITEMNMBR = @I_vITEMNMBR ,
                        @I_vLNITMSEQ = @I_vLNITMSEQ ,
                        @I_vCMPNTSEQ = 0 ,
                        @I_vBIN = '' ,
                        @I_vQTYTYPE = @BinQtyType ,
                        @I_vUOFM = @UOFMBASE ,
                        @I_vQUANTITY = @BinFulfillQty ,
                        @I_vCreateBin = 0 ,
                        @I_vOverrideBin = @OverrideBin ,
                        @I_vRequesterTrx = 1 ,
                        @O_iErrorState = @iCustomState output ,
                        @oErrString = @iCustomErrString output
                      
                      select @iError = @@error
                      
                      if ((@iStatus <> 0)
                           or (@iCustomState <> 0)
                           or (@iError <> 0))
                        begin
                          select @oErrString = rtrim(@oErrString) + ' ' + @iCustomErrString
                          
                          select @O_iErrorState = 5996
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          break
                        end
                    end
                  
                  if (@I_vSOPTYPE = 4)
                    begin
                      select @BinQtyType = @BinQtyType + 1
                    end
                  else
                    begin
                      select @BinQtyType = 6
                    end
                end
            end
        end
      
      if (@I_vUpdateIfExists = 1)
        begin
          select @TRDISAMT = TRDISAMT,
                 @TRDISPCT = TRDISPCT,
                 @PYMTRMID = PYMTRMID,
                 @DUEDATE = DUEDATE,
                 @DISCDATE = DISCDATE
          from   SOP10100 (nolock)
          where  SOPTYPE = @I_vSOPTYPE
                 and SOPNUMBE = @I_vSOPNUMBE
          
          if (@TRDISPCT > 0)
            begin
              select @TRDISPCT = @TRDISPCT / 100.00,
                     @TRDISAMT = null
            end
          
          if (@TRDISPCT = 0)
            begin
              select @TRDISPCT = null
            end
          
          exec @iStatus = taSopHdrRecalc
            @I_vSOPTYPE = @I_vSOPTYPE ,
            @I_vSOPNUMBE = @I_vSOPNUMBE ,
            @I_vPYMTRMID = @PYMTRMID ,
            @I_vDUEDATE = @DUEDATE ,
            @I_vDISCDATE = @DISCDATE ,
            @I_vTRDISAMT = @TRDISAMT ,
            @I_vTRADEPCT = @TRDISPCT ,
            @I_vRECREATEDIST = @I_vRecreateDist ,
            @I_vRECALCBTCHTOT = 2 ,
            @I_vCHGAMNT = @I_vXTNDPRCE ,
            @O_iErrorState = @SopHdrUpdateState output ,
            @oErrString = @SopHdrUpdateErrString output
          
          select @iError = @@error
          
          if ((@iStatus <> 0)
               or (@SopHdrUpdateState <> 0)
               or (@iError <> 0))
            begin
              select @oErrString = rtrim(@oErrString) + ' ' + @SopHdrUpdateErrString
              
              select @O_iErrorState = 1731
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      
      break
    end
  
  if (@mylock = 1)
    begin
      exec @iStatus = DYNAMICS..taDEXLOCKS
        @I_vOperation = 3 ,
        @I_vtable_path_name = @SOPHdrTbl ,
        @I_vrow_id = @DexRowID ,
        @O_oExists = @oExists output ,
        @O_oInsertStatus = @OInsStatus output ,
        @O_iErrorState = @DexLockErrorState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@DexLockErrorState <> 0)
           or (@iError <> 0))
        begin
          if (@DexLockErrorState <> 0)
            begin
              select @oErrString = rtrim(@oErrString) + ' ' + @DexLockErrorState
            end
          
          select @O_iErrorState = 2208
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @I_vQTYTBAOR = @I_vQTYTBAOR + @QTYTOBO
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  exec @iStatus = taSopLineIvcInsertPost
    @I_vSOPTYPE ,
    @I_vSOPNUMBE ,
    @I_vCUSTNMBR ,
    @I_vDOCDATE ,
    @I_vLOCNCODE ,
    @I_vITEMNMBR ,
    @I_vAutoAssignBin ,
    @I_vUNITPRCE ,
    @I_vXTNDPRCE ,
    @I_vQUANTITY ,
    @I_vMRKDNAMT ,
    @I_vMRKDNPCT ,   --gouldy Suggestion ID#7111
    @I_vCOMMNTID ,
    @I_vCOMMENT_1 ,
    @I_vCOMMENT_2 ,
    @I_vCOMMENT_3 ,
    @I_vCOMMENT_4 ,
    @I_vUNITCOST ,
    @I_vPRCLEVEL ,
    @I_vITEMDESC ,
    @I_vTAXAMNT ,
    @I_vQTYONHND ,
    @I_vQTYRTRND ,
    @I_vQTYINUSE ,
    @I_vQTYINSVC ,
    @I_vQTYDMGED ,
    @I_vNONINVEN ,
    @I_vLNITMSEQ ,
    @I_vDROPSHIP ,
    @I_vQTYTBAOR ,
    @I_vDOCID ,
    @I_vSALSTERR ,
    @I_vSLPRSNID ,
    @I_vITMTSHID ,
    @I_vIVITMTXB ,
    @I_vTAXSCHID ,
    @I_vPRSTADCD ,
    @I_vShipToName ,
    @I_vCNTCPRSN ,
    @I_vADDRESS1 ,
    @I_vADDRESS2 ,
    @I_vADDRESS3 ,
    @I_vCITY ,
    @I_vSTATE ,
    @I_vZIPCODE ,
    @I_vCOUNTRY ,
    @I_vPHONE1 ,
    @I_vPHONE2 ,
    @I_vPHONE3 ,
    @I_vFAXNUMBR ,
    @I_vEXCEPTIONALDEMAND ,
    @I_vReqShipDate ,
    @I_vFUFILDAT ,
    @I_vACTLSHIP ,
    @I_vSHIPMTHD ,
    @I_vINVINDX ,
    @I_vCSLSINDX ,
    @I_vSLSINDX ,
    @I_vMKDNINDX ,
    @I_vRTNSINDX ,
    @I_vINUSINDX ,
    @I_vINSRINDX ,
    @I_vDMGDINDX ,
    @I_vAUTOALLOCATESERIAL ,
    @I_vAUTOALLOCATELOT ,
    @I_vGPSFOINTEGRATIONID ,
    @I_vINTEGRATIONSOURCE ,
    @I_vINTEGRATIONID ,
    @I_vRequesterTrx ,
    @I_vQTYCANCE ,
    @I_vQTYFULFI ,
    @I_vALLOCATE ,
    @I_vUpdateIfExists ,
    @I_vRecreateDist ,
    @I_vQUOTEQTYTOINV ,
    @I_vTOTALQTY ,
    @I_vCMMTTEXT ,
    @I_vKitCompMan ,
    @I_vDEFPRICING ,
    @I_vDEFEXTPRICE ,
    @I_vCURNCYID ,
    @I_vUOFM ,
    @I_vIncludePromo ,
    @I_vCKCreditLimit ,
    @I_vQtyShrtOpt ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + @iCustomErrString
      
      select @O_iErrorState = 672
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Sales_Transaction' ,
        @I_vINDEX1 = @I_vSOPNUMBE ,
        @I_vINDEX2 = @I_vSOPTYPE ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 1 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iCustomState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + @iCustomState
          
          select @O_iErrorState = 2020
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  return (@O_iErrorState)

