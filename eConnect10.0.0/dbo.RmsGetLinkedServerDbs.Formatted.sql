

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetLinkedServerDbs]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetLinkedServerDbs]
  
create procedure [dbo].[RmsGetLinkedServerDbs]
                @iLinkedServerDbsTempTable VARCHAR(255),
                @iRmsServerName            VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lSysDbTable VARCHAR(255)
  
  SELECT @lSysDbTable = @iRmsServerName + '.master.dbo.sysdatabases'
  
  EXEC( 'INSERT INTO ' + @iLinkedServerDbsTempTable + ' SELECT  [name]  FROM ' + @lSysDbTable + ' WHERE  [name]   NOT IN(''master'',  ''tempdb'',  ''pubs'',  ''Northwind'',  ''msdb'',  ''model'')')

