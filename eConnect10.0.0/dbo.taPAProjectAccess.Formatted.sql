

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAProjectAccess]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAProjectAccess]
  
create procedure [dbo].[taPAProjectAccess]
                @I_vPAPROJNUMBER char(15),
                @I_vEMPLOYID     char(15),
                @I_vAction       smallint,
                @I_vRequesterTrx smallint  = 0,
                @I_vUSRDEFND1    char(50)  = '',
                @I_vUSRDEFND2    char(50)  = '',
                @I_vUSRDEFND3    char(50)  = '',
                @I_vUSRDEFND4    varchar(8000)  = '',
                @I_vUSRDEFND5    varchar(8000)  = '',
                @O_iErrorState   int  output,
                @oErrString      varchar(255)  output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @CURNCYID         char(15),
           @CURRNIDX         int,
           @iStatus          int,
           @iCustomState     int,
           @iCustomErrString varchar(255),
           @iError           int,
           @O_oErrorState    int,
           @PACONTNUMBER     char(17),
           @PAPROJNUMBER     char(15)
  
  select @CURNCYID = '',
         @CURRNIDX = 0,
         @O_oErrorState = 0,
         @O_iErrorState = 0,
         @iStatus = 0,
         @PACONTNUMBER = '',
         @PAPROJNUMBER = ''
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taPAProjectAccessPre
    @I_vPAPROJNUMBER output ,
    @I_vEMPLOYID output ,
    @I_vAction output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4933
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAPROJNUMBER is NULL 
       or @I_vEMPLOYID is NULL
       or @I_vRequesterTrx is NULL)
    begin
      select @O_iErrorState = 4934
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAPROJNUMBER = ''
       OR @I_vEMPLOYID = '')
    begin
      select @O_iErrorState = 4935
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vPAPROJNUMBER = UPPER(@I_vPAPROJNUMBER),
         @I_vEMPLOYID = UPPER(@I_vEMPLOYID)
  
  select @PACONTNUMBER = PACONTNUMBER,
         @PAPROJNUMBER = PAPROJNUMBER
  from   PA01201 (nolock)
  where  PAPROJNUMBER = @I_vPAPROJNUMBER
  
  if (@PAPROJNUMBER = '')
    begin
      select @O_iErrorState = 4936
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if not exists (select 1
                 from   UPR00100 (nolock)
                 where  EMPLOYID = @I_vEMPLOYID)
    begin
      select @O_iErrorState = 4937
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vAction = 1)
    begin
      if exists (select 1
                 from   PA01408 (nolock)
                 where  EMPLOYID = @I_vEMPLOYID
                        and PACONTNUMBER = @PACONTNUMBER
                        and PAPROJNUMBER = @I_vPAPROJNUMBER)
        begin
          select @O_iErrorState = 4938
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vAction < 1
       or @I_vAction > 2)
    begin
      select @O_iErrorState = 5257
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@oErrString <> '')
    return (@O_iErrorState)
  
  if (@I_vAction = 1)
    begin
      insert into PA01408
                 (PACONTNUMBER,
                  PAPROJNUMBER,
                  EMPLOYID)
      select @PACONTNUMBER,
             @I_vPAPROJNUMBER,
             @I_vEMPLOYID
    end
  else
    begin
      delete PA01408
      where  PACONTNUMBER = @PACONTNUMBER
             and PAPROJNUMBER = @I_vPAPROJNUMBER
             and EMPLOYID = @I_vEMPLOYID
    end
  
  if @@error <> 0
    begin
      select @O_iErrorState = 4939
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAProjectAccessPost
    @I_vPAPROJNUMBER ,
    @I_vEMPLOYID ,
    @I_vAction ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4940
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

