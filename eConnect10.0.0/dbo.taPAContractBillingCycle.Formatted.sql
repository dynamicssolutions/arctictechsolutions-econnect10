

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAContractBillingCycle]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAContractBillingCycle]
  
create procedure [dbo].[taPAContractBillingCycle]
                @I_vPACONTNUMBER  char(11),
                @I_vPABILLCYCLEID char(15),
                @I_vPABILLFORMAT  char(15),
                @I_vAction        smallint  = 1,
                @I_vRequesterTrx  smallint  = 0,
                @I_vUSRDEFND1     char(50)  = '',
                @I_vUSRDEFND2     char(50)  = '',
                @I_vUSRDEFND3     char(50)  = '',
                @I_vUSRDEFND4     varchar(8000)  = '',
                @I_vUSRDEFND5     varchar(8000)  = '',
                @O_iErrorState    int  output,
                @oErrString       varchar(255)  output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @CURNCYID         char(15),
           @CURRNIDX         int,
           @iStatus          int,
           @iCustomState     int,
           @iCustomErrString varchar(255),
           @iError           int,
           @O_oErrorState    int
  
  select @CURNCYID = '',
         @CURRNIDX = 0,
         @O_oErrorState = 0,
         @O_iErrorState = 0,
         @iStatus = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taPAContractBillingCyclePre
    @I_vPACONTNUMBER output ,
    @I_vPABILLCYCLEID output ,
    @I_vPABILLFORMAT output ,
    @I_vAction output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4991
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPACONTNUMBER is NULL 
       or @I_vPABILLCYCLEID is NULL
       or @I_vPABILLFORMAT is NULL
       or @I_vRequesterTrx is NULL)
    begin
      select @O_iErrorState = 4992
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPACONTNUMBER = ''
       OR @I_vPABILLCYCLEID = ''
       OR @I_vPABILLFORMAT = '')
    begin
      select @O_iErrorState = 6289
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vPACONTNUMBER = UPPER(@I_vPACONTNUMBER),
         @I_vPABILLCYCLEID = UPPER(@I_vPABILLCYCLEID),
         @I_vPABILLFORMAT = UPPER(@I_vPABILLFORMAT)
  
  if (@I_vAction = 1)
    begin
      if (exists (select 1
                  from   PA02401 (nolock)
                  where  PABILLCYCLEID = @I_vPABILLCYCLEID
                         and PABILLFORMAT = @I_vPABILLFORMAT
                         and PACONTNUMBER = @I_vPACONTNUMBER))
        begin
          select @O_iErrorState = 4993
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if not exists (select 1
                 from   PA02000 (nolock)
                 where  PABILLCYCLEID = @I_vPABILLCYCLEID)
    begin
      select @O_iErrorState = 4994
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if not exists (select 1
                 from   PA43101 (nolock)
                 where  PA_Bill_Format_Number = @I_vPABILLFORMAT)
    begin
      select @O_iErrorState = 4995
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vAction < 1
       or @I_vAction > 2)
    begin
      select @O_iErrorState = 6290
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@oErrString <> '')
    return (@O_iErrorState)
  
  if (@I_vAction = 1)
    begin
      insert into PA02401
                 (PACONTNUMBER,
                  PABILLCYCLEID,
                  PABILLFORMAT)
      select @I_vPACONTNUMBER,
             @I_vPABILLCYCLEID,
             @I_vPABILLFORMAT
    end
  else
    begin
      delete PA02401
      where  PACONTNUMBER = @I_vPACONTNUMBER
             and PABILLCYCLEID = @I_vPABILLCYCLEID
    end
  
  if @@error <> 0
    begin
      select @O_iErrorState = 4996
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAContractBillingCyclePost
    @I_vPACONTNUMBER ,
    @I_vPABILLCYCLEID ,
    @I_vPABILLFORMAT ,
    @I_vAction ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4997
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

