

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsRMTransactionTaxInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsRMTransactionTaxInsert]
  
create procedure [dbo].[RmsRMTransactionTaxInsert]
                @I_vCUSTNMBR     char(15),
                @I_vDOCNUMBR     char(20),
                @I_vRMDTYPAL     smallint,
                @I_vBACHNUMB     char(15),
                @I_vTAXDTLID     char(15),
                @I_vTAXAMNT      numeric(19,5),
                @I_vSTAXAMNT     numeric(19,5),
                @I_vFRTTXAMT     numeric(19,5)  = 0,
                @I_vMSCTXAMT     numeric(19,5)  = 0,
                @I_vTAXDTSLS     numeric(19,5),
                @I_vSEQNUMBR     int  = 0,
                @I_vACTINDX      int  = 0,
                @I_vACTNUMST     varchar(75)  = '',
                @I_vTDTTXSLS     numeric(19,5)  = null,
                @I_vRequesterTrx smallint  = 0,
                @I_vUSRDEFND1    char(50)  = '',
                @I_vUSRDEFND2    char(50)  = '',
                @I_vUSRDEFND3    char(50)  = '',
                @I_vUSRDEFND4    varchar(8000)  = '',
                @I_vUSRDEFND5    varchar(8000)  = '',
                @O_iErrorState   int  output,
                @oErrString      varchar(255)  output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @CURNCYID         char(15),
           @CURRNIDX         smallint,
           @TRXSORCE         char(13),
           @iStatus          int,
           @iError           int,
           @iCustomState     int,
           @iCustomErrString varchar(255),
           @iStatement       int,
           @iAddCodeErrState int,
           @O_oErrorState    int,
           @ISMCREG          tinyint
  
  select @O_oErrorState = 0,
         @TRXSORCE = '',
         @CURNCYID = '',
         @CURRNIDX = 0,
         @O_iErrorState = 0,
         @iStatement = 0,
         @iStatus = 0,
         @ISMCREG = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  if (@I_vCUSTNMBR is NULL 
       or @I_vDOCNUMBR is NULL
       or @I_vRMDTYPAL is NULL
       or @I_vBACHNUMB is NULL
       or @I_vTAXDTLID is NULL
       or @I_vTAXAMNT is NULL
       or @I_vSTAXAMNT is NULL
       or @I_vFRTTXAMT is NULL
       or @I_vMSCTXAMT is NULL
       or @I_vTAXDTSLS is NULL
       or @I_vSEQNUMBR is NULL
       or @I_vACTINDX is NULL
       or @I_vACTNUMST is NULL
       or @I_vRequesterTrx is NULL)
    begin
      select @O_iErrorState = 292
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vDOCNUMBR = ''
      and @I_vTAXDTLID = '')
    begin
      select @O_iErrorState = 293
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vCUSTNMBR = UPPER(@I_vCUSTNMBR),
         @I_vDOCNUMBR = UPPER(@I_vDOCNUMBR),
         @I_vBACHNUMB = UPPER(@I_vBACHNUMB),
         @I_vTAXDTLID = UPPER(@I_vTAXDTLID)
  
  if @I_vRMDTYPAL not in (1,3,4,5,
                          6,7,8)
    begin
      select @O_iErrorState = 294
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vBACHNUMB = '')
    begin
      select @O_iErrorState = 259
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if not exists (select 1
                 from   TX00201 (nolock)
                 where  TAXDTLID = @I_vTAXDTLID)
    begin
      select @O_iErrorState = 295
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vRequesterTrx < 0
       or @I_vRequesterTrx > 1)
    begin
      select @O_iErrorState = 3724
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vACTNUMST <> '')
    begin
      select @I_vACTINDX = ACTINDX
      from   GL00105 (nolock)
      where  ACTNUMST = @I_vACTNUMST
      
      select @I_vACTINDX = isnull(@I_vACTINDX,0)
      
      if (@I_vACTINDX = 0)
        begin
          select @O_iErrorState = 716
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      if (@I_vACTINDX <> 0)
        begin
          select @I_vACTINDX = ACTINDX
          from   GL00105 (nolock)
          where  ACTINDX = @I_vACTINDX
          
          select @I_vACTINDX = isnull(@I_vACTINDX,0)
          
          if (@I_vACTINDX = 0)
            begin
              select @O_iErrorState = 455
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      else
        begin
          select @I_vACTINDX = isnull(ACTINDX,0)
          from   TX00201 (nolock)
          where  TAXDTLID = @I_vTAXDTLID
          
          if (@I_vACTINDX = 0)
            begin
              select @O_iErrorState = 296
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if (@I_vSEQNUMBR = 0)
    begin
      select @I_vSEQNUMBR = max(SEQNUMBR) + 16384
      from   RM10601 (nolock)
      where  TAXDTLID = @I_vTAXDTLID
             and DOCNUMBR = @I_vDOCNUMBR
             and RMDTYPAL = @I_vRMDTYPAL
      
      select @I_vSEQNUMBR = isnull(@I_vSEQNUMBR,16384)
    end
  
  if exists (select 1
             from   RM10601 (nolock)
             where  DOCNUMBR = @I_vDOCNUMBR
                    and SEQNUMBR = @I_vSEQNUMBR
                    and RMDTYPAL = @I_vRMDTYPAL
                    and TRXSORCE = @TRXSORCE
                    and TAXDTLID = @I_vTAXDTLID)
    begin
      select @O_iErrorState = 262
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vTDTTXSLS > @I_vTAXDTSLS)
    begin
      select @O_iErrorState = 5443
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vTDTTXSLS is not null)
     and (@I_vTDTTXSLS < 0)
    begin
      select @O_iErrorState = 5444
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @CURNCYID = isnull(CURNCYID,'')
  from   RM00101 (nolock)
  where  CUSTNMBR = @I_vCUSTNMBR
  
  if (@CURNCYID <> '')
    begin
      select @CURRNIDX = isnull(CURRNIDX,0)
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @CURNCYID
    end
  else
    begin
      select @CURNCYID = isnull(FUNLCURR,''),
             @CURRNIDX = isnull(FUNCRIDX,0)
      from   MC40000 (nolock)
    end
  
  if (exists (select 1
              from   CM00100 (nolock)
              where  CURNCYID <> ''))
    begin
      select @ISMCREG = 1
    end
  
  if (@O_iErrorState = 0)
    begin
      if not exists (select 1
                     from   RM10601 (nolock)
                     where  RMDTYPAL = @I_vRMDTYPAL
                            and DOCNUMBR = @I_vDOCNUMBR
                            and TRXSORCE = @TRXSORCE
                            and TAXDTLID = @I_vTAXDTLID)
        begin
          insert RM10601
                (BACHNUMB,
                 RMDTYPAL,
                 DOCNUMBR,
                 CUSTNMBR,
                 TAXDTLID,
                 TRXSORCE,
                 ACTINDX,
                 BKOUTTAX,
                 TAXAMNT,
                 ORTAXAMT,
                 STAXAMNT,
                 ORSLSTAX,
                 FRTTXAMT,
                 ORFRTTAX,
                 MSCTXAMT,
                 ORMSCTAX,
                 TAXDTSLS,
                 ORTOTSLS,
                 TDTTXSLS,
                 ORTXSLS,
                 POSTED,
                 SEQNUMBR,
                 CURRNIDX)
          select @I_vBACHNUMB,
                 @I_vRMDTYPAL,
                 @I_vDOCNUMBR,
                 @I_vCUSTNMBR,
                 @I_vTAXDTLID,
                 @TRXSORCE,
                 @I_vACTINDX,
                 0,
                 @I_vTAXAMNT,
                 @I_vTAXAMNT,
                 case 
                   WHEN @I_vSTAXAMNT = 0 THEN @I_vTAXAMNT
                   ELSE @I_vSTAXAMNT
                 end,
                 case 
                   WHEN @I_vSTAXAMNT = 0 THEN @I_vTAXAMNT
                   ELSE @I_vSTAXAMNT
                 end,
                 @I_vFRTTXAMT,
                 @I_vFRTTXAMT,
                 @I_vMSCTXAMT,
                 @I_vMSCTXAMT,
                 @I_vTAXDTSLS,
                 @I_vTAXDTSLS,
                 case 
                   WHEN @I_vTDTTXSLS is null THEN @I_vTAXDTSLS
                   ELSE @I_vTDTTXSLS
                 end,
                 case 
                   WHEN @I_vTDTTXSLS is null THEN @I_vTAXDTSLS
                   ELSE @I_vTDTTXSLS
                 end,
                 0,
                 @I_vSEQNUMBR,
                 case 
                   when @ISMCREG = 1 then @CURRNIDX
                   else 0
                 end
          
          if @@error <> 0
            begin
              select @O_iErrorState = 297
              
              select @iStatement = 1
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          select @O_iErrorState = 7148
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  
  return (@O_iErrorState)

