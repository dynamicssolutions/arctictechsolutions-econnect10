

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taIVMultibinBinToBinTransfer]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taIVMultibinBinToBinTransfer]
  
create procedure [dbo].[taIVMultibinBinToBinTransfer]
                @I_vITEMNMBR     char(30),
                @I_vLOCNCODE     char(10),
                @I_vQTYTYPE      smallint  = 1,
                @I_vUOFM         char(8)  = '',
                @I_vBIN          char(15),
                @I_vQUANTITY     numeric(19,5),
                @I_vTOBIN        char(15),
                @I_vCreateBin    tinyint  = 0,
                @I_vResetQty     tinyint  = 0,
                @I_vSerLotNmbr   char(20)  = '',
                @I_vEXPNDATE     datetime  = '',
                @I_vRequesterTrx smallint  = 0,
                @I_vUSRDEFND1    char(50)  = '',
                @I_vUSRDEFND2    char(50)  = '',
                @I_vUSRDEFND3    char(50)  = '',
                @I_vUSRDEFND4    varchar(8000)  = '',
                @I_vUSRDEFND5    varchar(8000)  = '',
                @O_iErrorState   int  output,
                @oErrString      varchar(255)  output   /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @ITEMTYPE                tinyint,
           @QtyAvailable            numeric(19,5),
           @SERIALOT                tinyint,
           @BIN                     char(15),
           @SERLNSLD                tinyint,
           @LTNUMSLD                tinyint,
           @UOFM                    char(8),
           @UOMSCHDL                char(10),
           @QTYBSUOM                numeric(19,5),
           @BaseUOFM                char(8),
           @Qty                     numeric(19,5),
           @DECPLQTY                smallint,
           @DATERECD                datetime,
           @DTSEQNUM                numeric(19,5),
           @NewDTSEQNUM             numeric(19,5),
           @UNITCOST                numeric(19,5),
           @RCTSEQNM                int,
           @VNDRNMBR                char(25),
           @QtyLeft                 numeric(19,5),
           @QtyDone                 numeric(19,5),
           @sCompanyID              int,
           @NOTEINDX                numeric(19,5),
           @iGetNextNoteIdxErrState int,
           @iStatus                 int,
           @iError                  int,
           @iCustomState            int,
           @iCustomErrString        varchar(255),
           @O_oErrorState           int,
           @MFGDATE                 datetime,
           @EXPNDATE                datetime
  
  select @ITEMTYPE = 0,
         @QtyAvailable = 0,
         @SERIALOT = 0,
         @BIN = '',
         @SERLNSLD = 1,
         @LTNUMSLD = 1,
         @UOFM = '',
         @UOMSCHDL = '',
         @QTYBSUOM = 1,
         @BaseUOFM = '',
         @Qty = 0,
         @DECPLQTY = 0,
         @DATERECD = '',
         @DTSEQNUM = 0,
         @NewDTSEQNUM = 0,
         @UNITCOST = 0,
         @RCTSEQNM = 0,
         @VNDRNMBR = '',
         @QtyLeft = 0,
         @QtyDone = 0,
         @sCompanyID = 0,
         @NOTEINDX = 0,
         @iGetNextNoteIdxErrState = 0,
         @iStatus = 0,
         @iError = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @O_iErrorState = 0,
         @MFGDATE = '',
         @EXPNDATE = ''
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taIVMultibinBinToBinTransferPre
    @I_vITEMNMBR output ,
    @I_vLOCNCODE output ,
    @I_vQTYTYPE output ,
    @I_vUOFM output ,
    @I_vBIN output ,
    @I_vQUANTITY output ,
    @I_vTOBIN output ,
    @I_vCreateBin output ,
    @I_vResetQty output ,
    @I_vSerLotNmbr output ,
    @I_vEXPNDATE output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus = 0)
      and (@iError <> 0))
    begin
      select @iStatus = @iError
    end
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4638
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((select ENABLEMULTIBIN
       from   IV40100 (nolock)) <> 1)
    begin
      select @O_iErrorState = 5020
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vITEMNMBR is NULL 
       or @I_vLOCNCODE is NULL
       or @I_vQTYTYPE is NULL
       or @I_vUOFM is NULL
       or @I_vQUANTITY is NULL
       or @I_vBIN is NULL
       or @I_vTOBIN is NULL
       or @I_vCreateBin is NULL
       or @I_vResetQty is NULL
       or @I_vSerLotNmbr is NULL
       or @I_vRequesterTrx is NULL)
    begin
      select @O_iErrorState = 5021
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vITEMNMBR = UPPER(@I_vITEMNMBR),
         @I_vLOCNCODE = UPPER(@I_vLOCNCODE),
         @I_vBIN = UPPER(@I_vBIN),
         @I_vTOBIN = UPPER(@I_vTOBIN),
         @I_vSerLotNmbr = UPPER(@I_vSerLotNmbr)
  
  select @UOMSCHDL = UOMSCHDL,
         @SERIALOT = ITMTRKOP,
         @ITEMTYPE = ITEMTYPE,
         @DECPLQTY = DECPLQTY
  from   IV00101 (nolock)
  where  ITEMNMBR = @I_vITEMNMBR
  
  if (@I_vITEMNMBR = '')
    begin
      select @O_iErrorState = 5022
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if not exists (select 1
                 from   IV00101 (nolock)
                 where  ITEMNMBR = @I_vITEMNMBR)
    begin
      select @O_iErrorState = 5023
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@ITEMTYPE = 0)
    begin
      select @O_iErrorState = 5024
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@ITEMTYPE not in (1,2))
    begin
      select @O_iErrorState = 5025
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vLOCNCODE = '')
    begin
      select @O_iErrorState = 5026
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if not exists (select 1
                 from   IV40700 (nolock)
                 where  LOCNCODE = @I_vLOCNCODE)
    begin
      select @O_iErrorState = 5027
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if not exists (select 1
                 from   IV00102 (nolock)
                 where  ITEMNMBR = @I_vITEMNMBR
                        and LOCNCODE = @I_vLOCNCODE)
    begin
      select @O_iErrorState = 5028
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vQTYTYPE not in (1,2,3,4,
                          5))
    begin
      select @O_iErrorState = 5029
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vUOFM <> '')
    begin
      if (@SERIALOT <> 2)
        begin
          select @UOFM = UOFM
          from   IV40202 (nolock)
          where  UOFM = @I_vUOFM
                 and UOMSCHDL = @UOMSCHDL
          
          if (@UOFM = '')
            begin
              select @O_iErrorState = 5346
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          select @I_vUOFM = rtrim(@I_vUOFM),
                 @UOFM = rtrim(@UOFM)
          
          if (convert(binary,@I_vUOFM) <> convert(binary,@UOFM))
          
          begin
            select @O_iErrorState = 5031
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @O_oErrorState output
          end
          
          select @QTYBSUOM = QTYBSUOM
          from   IV40202 (nolock)
          where  UOFM = @I_vUOFM
                 and UOMSCHDL = @UOMSCHDL
          
          select @I_vQUANTITY = @I_vQUANTITY * @QTYBSUOM
        end
      else
        if (@SERIALOT = 2)
          begin
            select @BaseUOFM = BASEUOFM
            from   IV40201 (nolock)
            where  UOMSCHDL = @UOMSCHDL
            
            if (@BaseUOFM <> @I_vUOFM)
              begin
                select @O_iErrorState = 5032
                
                exec @iStatus = taUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
              end
          end
    end
  
  if (@I_vUOFM = '')
    begin
      select @BaseUOFM = BASEUOFM
      from   IV40201 (nolock)
      where  UOMSCHDL = @UOMSCHDL
      
      if (@BaseUOFM = '')
        begin
          select @O_iErrorState = 5033
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      else
        begin
          select @I_vUOFM = @BaseUOFM
        end
    end
  
  if not exists (select 1
                 from   IV40701 (nolock)
                 where  LOCNCODE = @I_vLOCNCODE
                        and BIN = @I_vBIN)
    begin
      select @O_iErrorState = 5034
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if not exists (select 1
                 from   IV40701 (nolock)
                 where  LOCNCODE = @I_vLOCNCODE
                        and BIN = @I_vTOBIN)
    begin
      if (@I_vCreateBin = 0)
        begin
          select @O_iErrorState = 5038
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@I_vCreateBin = 1)
        begin
          select @sCompanyID = CMPANYID
          from   DYNAMICS..SY01500 a (nolock)
          where  a.INTERID = db_name()
          
          exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
            @I_sCompanyID = @sCompanyID ,
            @I_iSQLSessionID = 0 ,
            @O_mNoteIndex = @NOTEINDX output ,
            @O_iErrorState = @iGetNextNoteIdxErrState output
          
          select @iError = @@error
          
          if ((@iStatus <> 0)
               or (@iGetNextNoteIdxErrState <> 0)
               or (@iError <> 0))
            begin
              select @oErrString = rtrim(@oErrString) + ' ' + @iGetNextNoteIdxErrState
              
              select @O_iErrorState = 5039
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          insert IV40701
                (LOCNCODE,
                 BIN,
                 NOTEINDX)
          select @I_vLOCNCODE,
                 @I_vTOBIN,
                 @NOTEINDX
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 5040
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if not exists (select 1
                 from   IV00112 (nolock)
                 where  ITEMNMBR = @I_vITEMNMBR
                        and LOCNCODE = @I_vLOCNCODE
                        and BIN = @I_vTOBIN
                        and QTYTYPE = @I_vQTYTYPE)
    begin
      if (@I_vCreateBin = 1)
        begin
          insert IV00112
                (ITEMNMBR,
                 LOCNCODE,
                 BIN,
                 QTYTYPE,
                 QUANTITY,
                 ATYALLOC)
          select @I_vITEMNMBR,
                 @I_vLOCNCODE,
                 @I_vTOBIN,
                 @I_vQTYTYPE,
                 0,
                 0
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 5041
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        if (@I_vCreateBin = 0)
          begin
            select @O_iErrorState = 5042
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @O_oErrorState output
          end
    end
  
  if ((@SERIALOT in (2,3))
      and (@I_vSerLotNmbr = ''))
    begin
      select @O_iErrorState = 5043
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@SERIALOT = 2)
    begin
      select @BIN = BIN,
             @SERLNSLD = SERLNSLD
      from   IV00200 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
             and LOCNCODE = @I_vLOCNCODE
             and QTYTYPE = @I_vQTYTYPE
             and SERLNMBR = @I_vSerLotNmbr
      
      if (@BIN <> @I_vBIN)
        begin
          select @O_iErrorState = 5044
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@SERLNSLD <> 0)
        begin
          select @O_iErrorState = 5045
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@SERIALOT = 3)
    begin
      if (@I_vEXPNDATE <> '')
        begin
          select @BIN = BIN,
                 @LTNUMSLD = LTNUMSLD
          from   IV00300 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and LOCNCODE = @I_vLOCNCODE
                 and QTYTYPE = @I_vQTYTYPE
                 and LOTNUMBR = @I_vSerLotNmbr
                 and BIN = @I_vBIN
                 and EXPNDATE = @I_vEXPNDATE
          
          if (@BIN = '')
            begin
              select @O_iErrorState = 8673
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      else
        begin
          select @BIN = BIN,
                 @LTNUMSLD = LTNUMSLD
          from   IV00300 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and LOCNCODE = @I_vLOCNCODE
                 and QTYTYPE = @I_vQTYTYPE
                 and LOTNUMBR = @I_vSerLotNmbr
                 and BIN = @I_vBIN
          
          if (@BIN = '')
            begin
              select @O_iErrorState = 5046
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      
      if (@LTNUMSLD <> 0)
        begin
          select @O_iErrorState = 5047
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vQUANTITY <= 0)
    begin
      select @O_iErrorState = 5048
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@SERIALOT = 1)
    begin
      select @QtyAvailable = (QUANTITY - ATYALLOC)
      from   IV00112 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
             and LOCNCODE = @I_vLOCNCODE
             and BIN = @I_vBIN
             and QTYTYPE = @I_vQTYTYPE
      
      if ((@I_vQUANTITY > @QtyAvailable)
          and (@I_vResetQty = 0))
        begin
          select @O_iErrorState = 5049
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@I_vQUANTITY > @QtyAvailable)
          and (@I_vResetQty = 1))
        begin
          if (@QtyAvailable > 0)
            begin
              select @I_vQUANTITY = @QtyAvailable
            end
          else
            begin
              select @O_iErrorState = 5050
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
    end
  
  if (@SERIALOT = 2)
    begin
      if ((@I_vQUANTITY > 1)
          and (@I_vResetQty = 0))
        begin
          select @O_iErrorState = 5051
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@I_vQUANTITY <> 1)
          and (@I_vResetQty = 1))
        begin
          select @I_vQUANTITY = 1
        end
    end
  
  if (@SERIALOT = 3)
    begin
      if (@I_vEXPNDATE <> '')
        begin
          select @QtyAvailable = sum(QTYRECVD - QTYSOLD - ATYALLOC)
          from   IV00300 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and LOCNCODE = @I_vLOCNCODE
                 and BIN = @I_vBIN
                 and LOTNUMBR = @I_vSerLotNmbr
                 and QTYTYPE = @I_vQTYTYPE
                 and EXPNDATE = @I_vEXPNDATE
        end
      else
        begin
          select @QtyAvailable = sum(QTYRECVD - QTYSOLD - ATYALLOC)
          from   IV00300 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and LOCNCODE = @I_vLOCNCODE
                 and BIN = @I_vBIN
                 and LOTNUMBR = @I_vSerLotNmbr
                 and QTYTYPE = @I_vQTYTYPE
        end
      
      if ((@I_vQUANTITY > @QtyAvailable)
          and (@I_vResetQty = 0))
        begin
          select @O_iErrorState = 5052
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@I_vQUANTITY > @QtyAvailable)
          and (@I_vResetQty = 1))
        begin
          if (@QtyAvailable > 0)
            begin
              select @I_vQUANTITY = @QtyAvailable
            end
          else
            begin
              select @O_iErrorState = 5053
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
    end
  
  if (@I_vQUANTITY <> round(@I_vQUANTITY,@DECPLQTY - 1,1))
    begin
      select @O_iErrorState = 5054
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  update IV00112
  set    QUANTITY = QUANTITY - @I_vQUANTITY
  where  ITEMNMBR = @I_vITEMNMBR
         and LOCNCODE = @I_vLOCNCODE
         and BIN = @I_vBIN
         and QTYTYPE = @I_vQTYTYPE
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 5055
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  update IV00112
  set    QUANTITY = QUANTITY + @I_vQUANTITY
  where  ITEMNMBR = @I_vITEMNMBR
         and LOCNCODE = @I_vLOCNCODE
         and BIN = @I_vTOBIN
         and QTYTYPE = @I_vQTYTYPE
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 5056
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@SERIALOT = 2)
    begin
      update IV00200
      set    BIN = @I_vTOBIN
      where  ITEMNMBR = @I_vITEMNMBR
             and LOCNCODE = @I_vLOCNCODE
             and QTYTYPE = @I_vQTYTYPE
             and SERLNMBR = @I_vSerLotNmbr
             and BIN = @I_vBIN
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 5057
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@SERIALOT = 3)
    begin
      select   @DATERECD = DATERECD,
               @DTSEQNUM = DTSEQNUM
      from     (select DATERECD,
                       DTSEQNUM,
                       (QTYRECVD - QTYSOLD - ATYALLOC) qtyAvail
                from   IV00300 (nolock)
                where  (@I_vEXPNDATE <> ''
                        and ITEMNMBR = @I_vITEMNMBR
                        and LOCNCODE = @I_vLOCNCODE
                        and QTYTYPE = @I_vQTYTYPE
                        and BIN = @I_vBIN
                        and LOTNUMBR = @I_vSerLotNmbr
                        and EXPNDATE = @I_vEXPNDATE)
                        or (@I_vEXPNDATE = ''
                            and ITEMNMBR = @I_vITEMNMBR
                            and LOCNCODE = @I_vLOCNCODE
                            and QTYTYPE = @I_vQTYTYPE
                            and BIN = @I_vBIN
                            and LOTNUMBR = @I_vSerLotNmbr)) as t1
      where    t1.qtyAvail = @I_vQUANTITY
      order by DATERECD,
               DTSEQNUM
      
      if (@DATERECD <> '')
        begin
          update IV00300
          set    BIN = @I_vTOBIN
          where  ITEMNMBR = @I_vITEMNMBR
                 and LOCNCODE = @I_vLOCNCODE
                 and QTYTYPE = @I_vQTYTYPE
                 and BIN = @I_vBIN
                 and LOTNUMBR = @I_vSerLotNmbr
                 and DATERECD = @DATERECD
                 and DTSEQNUM = @DTSEQNUM
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 5058
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          select @QtyLeft = @I_vQUANTITY
          
          while (@QtyDone < @I_vQUANTITY)
            begin
              select   top 1 @Qty = (QTYRECVD - QTYSOLD - ATYALLOC),
                             @DATERECD = DATERECD,
                             @DTSEQNUM = DTSEQNUM,
                             @UNITCOST = UNITCOST,
                             @RCTSEQNM = RCTSEQNM,
                             @MFGDATE = MFGDATE,
                             @EXPNDATE = EXPNDATE
              from     IV00300 (nolock)
              where    (@I_vEXPNDATE <> ''
                        and ITEMNMBR = @I_vITEMNMBR
                        and LOCNCODE = @I_vLOCNCODE
                        and QTYTYPE = @I_vQTYTYPE
                        and BIN = @I_vBIN
                        and LOTNUMBR = @I_vSerLotNmbr
                        and LTNUMSLD = 0
                        and EXPNDATE = @I_vEXPNDATE)
                        or (@I_vEXPNDATE = ''
                            and ITEMNMBR = @I_vITEMNMBR
                            and LOCNCODE = @I_vLOCNCODE
                            and QTYTYPE = @I_vQTYTYPE
                            and BIN = @I_vBIN
                            and LOTNUMBR = @I_vSerLotNmbr
                            and LTNUMSLD = 0)
              order by DATERECD,
                       DTSEQNUM
              
              if (@@rowcount = 0)
                begin
                  select @O_iErrorState = 5059
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
              
              if (@Qty > @QtyLeft)
                begin
                  select @QtyDone = @I_vQUANTITY
                  
                  select @NewDTSEQNUM = max(DTSEQNUM) + 1
                  from   IV00300 (nolock)
                  where  ITEMNMBR = @I_vITEMNMBR
                         and LOCNCODE = @I_vLOCNCODE
                         and QTYTYPE = @I_vQTYTYPE
                         and DATERECD = @DATERECD
                  
                  insert IV00300
                        (ITEMNMBR,
                         LOCNCODE,
                         DATERECD,
                         DTSEQNUM,
                         LOTNUMBR,
                         QTYRECVD,
                         QTYSOLD,
                         ATYALLOC,
                         UNITCOST,
                         RCTSEQNM,
                         VNDRNMBR,
                         LTNUMSLD,
                         QTYTYPE,
                         BIN,
                         MFGDATE,
                         EXPNDATE)
                  select @I_vITEMNMBR,
                         @I_vLOCNCODE,
                         @DATERECD,
                         @NewDTSEQNUM,
                         @I_vSerLotNmbr,
                         @QtyLeft,
                         0,
                         0,
                         @UNITCOST,
                         @RCTSEQNM,
                         @VNDRNMBR,
                         0,
                         @I_vQTYTYPE,
                         @I_vTOBIN,
                         @MFGDATE,
                         @EXPNDATE
                  
                  if (@@error <> 0)
                    begin
                      select @O_iErrorState = 5060
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  
                  update IV00300
                  set    QTYSOLD = QTYSOLD + @QtyLeft
                  where  ITEMNMBR = @I_vITEMNMBR
                         and LOCNCODE = @I_vLOCNCODE
                         and QTYTYPE = @I_vQTYTYPE
                         and BIN = @I_vBIN
                         and LOTNUMBR = @I_vSerLotNmbr
                         and DATERECD = @DATERECD
                         and DTSEQNUM = @DTSEQNUM
                  
                  if (@@error <> 0)
                    begin
                      select @O_iErrorState = 5061
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                end
              else
                if (@Qty <= @QtyLeft)
                  begin
                    select @QtyDone = @QtyDone + @Qty,
                           @QtyLeft = @QtyLeft - @Qty
                    
                    update IV00300
                    set    BIN = @I_vTOBIN
                    where  ITEMNMBR = @I_vITEMNMBR
                           and LOCNCODE = @I_vLOCNCODE
                           and QTYTYPE = @I_vQTYTYPE
                           and BIN = @I_vBIN
                           and LOTNUMBR = @I_vSerLotNmbr
                           and DATERECD = @DATERECD
                           and DTSEQNUM = @DTSEQNUM
                    
                    if (@@error <> 0)
                      begin
                        select @O_iErrorState = 5062
                        
                        exec @iStatus = taUpdateString
                          @O_iErrorState ,
                          @oErrString ,
                          @oErrString output ,
                          @O_oErrorState output
                        
                        return (@O_iErrorState)
                      end
                  end
            end
        end
    end
  
  exec @iStatus = taIVMultibinBinToBinTransferPost
    @I_vITEMNMBR ,
    @I_vLOCNCODE ,
    @I_vQTYTYPE ,
    @I_vUOFM ,
    @I_vBIN ,
    @I_vQUANTITY ,
    @I_vTOBIN ,
    @I_vCreateBin ,
    @I_vResetQty ,
    @I_vSerLotNmbr ,
    @I_vEXPNDATE ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus = 0)
      and (@iError <> 0))
    begin
      select @iStatus = @iError
    end
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 5064
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

