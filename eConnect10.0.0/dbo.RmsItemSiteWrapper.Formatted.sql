

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsItemSiteWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsItemSiteWrapper]
  
create procedure [dbo].[RmsItemSiteWrapper]
                @iItemNum      char(30),
                @iLocationCode char(10),
                @oErrorState   int  output,
                @oErrString    varchar(255)  output   /*with encryption*/
AS
  EXEC RmsItemSite
    @I_vITEMNMBR = @iItemNum ,
    @I_vLOCNCODE = @iLocationCode ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

