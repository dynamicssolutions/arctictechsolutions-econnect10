

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetStores]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetStores]
  
create procedure [dbo].[RmsGetStores]
                @iStoreTempTable VARCHAR(255),
                @iRmsServerName  VARCHAR(255),
                @iHqDbName       VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lStoreTable VARCHAR(255)
  
  SELECT @lStoreTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Store'
  
  EXEC( 'INSERT INTO ' + @iStoreTempTable + ' SELECT  [ID],  [Name]  FROM ' + @lStoreTable)

