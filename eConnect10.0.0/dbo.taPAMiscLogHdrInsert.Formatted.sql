

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAMiscLogHdrInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAMiscLogHdrInsert]
  
create procedure [dbo].[taPAMiscLogHdrInsert]
                @I_vPSMISCLTRXTYPE smallint,
                @I_vPAMISCLDOCNO   char(17),
                @I_vPADOCDT        datetime,
                @I_vBACHNUMB       char(15),
                @I_vPSMISCID       char(15),
                @I_vPAREPD         int,
                @I_vPAREPDT        datetime,
                @I_vCURNCYID       char(15),
                @I_vPAREFNO        char(17)  = '',
                @I_vPAreptsuff     char(5)  = '',
                @I_vPACOMM         char(51)  = '',
                @I_vPATQTY         numeric(19,5)  = 0,
                @I_vPAtotcosts     numeric(19,5)  = 0,
                @I_vPATACRV        numeric(19,5)  = 0,
                @I_vXCHGRATE       numeric(19,7)  = 0,
                @I_vRATETPID       char(15)  = '',
                @I_vEXPNDATE       datetime  = '',
                @I_vEXCHDATE       datetime  = '',
                @I_vEXGTBDSC       char(30)  = '',
                @I_vEXTBLSRC       char(50)  = '',
                @I_vRATEEXPR       smallint  = -1,
                @I_vDYSTINCR       smallint  = -1,
                @I_vRATEVARC       numeric(19,7)  = 0,
                @I_vTRXDTDEF       smallint  = -1,
                @I_vRTCLCMTD       smallint  = -1,
                @I_vPRVDSLMT       smallint  = 0,
                @I_vDATELMTS       smallint  = 0,
                @I_vTIME1          datetime  = '',
                @I_vRequesterTrx   smallint  = 0,
                @I_vUSRDEFND1      char(50)  = '',
                @I_vUSRDEFND2      char(50)  = '',
                @I_vUSRDEFND3      char(50)  = '',
                @I_vUSRDEFND4      varchar(8000)  = '',
                @I_vUSRDEFND5      varchar(8000)  = '',
                @O_iErrorState     int  output,
                @oErrString        varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iStatus int,  @iCustomState int,  @iCustomErrString varchar(255),  @iError int,  @iCursorError int,  @O_oErrorState int,  @PADOCDT datetime,  @PAYR smallint,  @USERID char (15),  @BCHSOURC char (15),  @TRXSORCE char (13),  @PAPeriodEndDate datetime,  @PACOMM char (51),  @PAUD1 char (21),  @PAUD2 char (21),  @NOTEINDX numeric (19,5),  @PAPD datetime,  @LNITMSEQ int,  @PAML_HDR_Errors binary (4),  @PAML_Dist_Errors binary (4),  @CURRNIDX smallint,  @EXGTBLID char (15),  @DENXRATE numeric (19,5),  @MCTRXSTT smallint,  @PAORIGTOTCOSTS numeric (19,5),  @PAORIACCRREV numeric (19,5),  @PAsetupkey smallint,  @PAallow_1 int,  @PAallow_2 int,  @PAallow_3 int,  @PAallow_4 int,  @PAallow_5 int,  @PAallow_6 int,  @PAallow_7 int,  @PAallow_8 int,  @PAallow_9 int,  @PAallow_10 int,  @PAreportingperiods smallint,  @PAnumofreportingperiods smallint,  @PA1stdatereportperiod datetime,  @FUNLCURR char(15),  @ISMCTRX int,  @DECPLCUR numeric(19,5),  @iUpdtBthErrState int,  @iUpdDistErrState int,  @iCreateBatchErrString varchar(255),  @PAQtyQ numeric(19,5),  @PATOTCST numeric(19,5),  @PAACREV numeric(19,5),  @PAREPDT datetime,  @sumtotalqty numeric(19,5),  @sumtotalcost numeric(19,5),  @sumttotaloverhead numeric(19,5),  @sumtotalprofit numeric(19,5),  @sumaccruedrevenues numeric(19,5),  @PAPROJNUMBER char(15),  @PACOSTCATID char(15),  @CUSTNMBR char(17),  @PATU int,  @PACONTNUMBER char (11),  @PADT datetime,  @YEAR1 smallint,  @l_Period smallint,  @fClosed smallint,  @nErr smallint,  @userdate datetime,  @UPSTDTFR int,  @RNDDIFF int,  @PAinactive int,  @l_PSMISCID char(15),  @sCompanyID smallint,     @iGetNextNoteIdxErrState int,    @DBName char(50),     @O_iInitErrorState int,     @oInitErrString varchar(255),    @datediff int,  @PAREPD int,  @yeardiff int,  @origdate datetime,  @daysperperiod int
  
  select @iStatus = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @iError = 0,
         @iCursorError = 0,
         @O_oErrorState = 0,
         @PADOCDT = '',
         @PAYR = 0,
         @USERID = 'eConnect',
         @BCHSOURC = 'PA_ML',
         @TRXSORCE = '',
         @PAPeriodEndDate = '',
         @PACOMM = '',
         @PAUD1 = '',
         @PAUD2 = '',
         @NOTEINDX = 0,
         @PAPD = '',
         @LNITMSEQ = 0,
         @PAML_HDR_Errors = 0,
         @PAML_Dist_Errors = 0,
         @CURRNIDX = 0,
         @EXGTBLID = '',
         @DENXRATE = 0,
         @MCTRXSTT = 0,
         @PAORIGTOTCOSTS = 0,
         @PAORIACCRREV = 0,
         @PAsetupkey = 0,
         @PAallow_1 = 0,
         @PAallow_2 = 0,
         @PAallow_3 = 0,
         @PAallow_4 = 0,
         @PAallow_5 = 0,
         @PAallow_6 = 0,
         @PAallow_7 = 0,
         @PAallow_8 = 0,
         @PAallow_9 = 0,
         @PAallow_10 = 0,
         @PAreportingperiods = 0,
         @PAnumofreportingperiods = 0,
         @PA1stdatereportperiod = '',
         @FUNLCURR = '',
         @ISMCTRX = 0,
         @DECPLCUR = 0,
         @PAQtyQ = 0,
         @PATOTCST = 0,
         @PAACREV = 0,
         @PAREPDT = '',
         @sumtotalqty = 0,
         @sumtotalcost = 0,
         @sumttotaloverhead = 0,
         @sumtotalprofit = 0,
         @sumaccruedrevenues = 0,
         @PAPROJNUMBER = '',
         @PACOSTCATID = '',
         @CUSTNMBR = '',
         @PATU = 0,
         @PACONTNUMBER = '',
         @PADT = '',
         @YEAR1 = 0,
         @l_Period = 0,
         @fClosed = 0,
         @nErr = 0,
         @userdate = '',
         @UPSTDTFR = 0,
         @RNDDIFF = 0,
         @O_iErrorState = 0,
         @PAinactive = 0,
         @l_PSMISCID = '',
         @sCompanyID = 0,
         @iGetNextNoteIdxErrState = 0,
         @DBName = '',
         @O_iInitErrorState = 0,
         @oInitErrString = '',
         @datediff = 0,
         @PAREPD = 0,
         @yeardiff = 0,
         @origdate = '',
         @daysperperiod = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  select @DBName = DB_Name()
  
  exec @iStatus = taPAMiscLogHdrInsertPre
    @I_vPSMISCLTRXTYPE output ,
    @I_vPAMISCLDOCNO output ,
    @I_vPADOCDT output ,
    @I_vBACHNUMB output ,
    @I_vPSMISCID output ,
    @I_vPAREPD output ,
    @I_vPAREPDT output ,
    @I_vCURNCYID output ,
    @I_vPAREFNO output ,
    @I_vPAreptsuff output ,
    @I_vPACOMM output ,
    @I_vPATQTY output ,
    @I_vPAtotcosts output ,
    @I_vPATACRV output ,
    @I_vXCHGRATE output ,
    @I_vRATETPID output ,
    @I_vEXPNDATE output ,
    @I_vEXCHDATE output ,
    @I_vEXGTBDSC output ,
    @I_vEXTBLSRC output ,
    @I_vRATEEXPR output ,
    @I_vDYSTINCR output ,
    @I_vRATEVARC output ,
    @I_vTRXDTDEF output ,
    @I_vRTCLCMTD output ,
    @I_vPRVDSLMT output ,
    @I_vDATELMTS output ,
    @I_vTIME1 output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1010
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPSMISCLTRXTYPE is NULL 
       or @I_vPAMISCLDOCNO is NULL
       or @I_vPADOCDT is NULL
       or @I_vBACHNUMB is NULL
       or @I_vPSMISCID is NULL
       or @I_vPAREPD is NULL
       or @I_vPAREPDT is NULL
       or @I_vCURNCYID is NULL)
    begin
      select @O_iErrorState = 1011
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPSMISCLTRXTYPE = 0
       or @I_vPAMISCLDOCNO = ''
       or @I_vPADOCDT = ''
       or @I_vBACHNUMB = ''
       or @I_vPSMISCID = ''
       or @I_vPAREPD = ''
       or @I_vPAREPDT = ''
       or @I_vCURNCYID = '')
    begin
      select @O_iErrorState = 1012
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vPSMISCID = UPPER(@I_vPSMISCID),
         @I_vCURNCYID = UPPER(@I_vCURNCYID),
         @I_vPAMISCLDOCNO = UPPER(@I_vPAMISCLDOCNO),
         @I_vPAREFNO = UPPER(@I_vPAREFNO)
  
  select @PAsetupkey = isnull(PAsetupkey,0),
         @PAreportingperiods = isnull(PAreportingperiods,0),
         @PAnumofreportingperiods = isnull(PAnumofreportingperiods,0),
         @PA1stdatereportperiod = isnull(PA1stdatereportperiod,0),
         @PAallow_1 = isnull(PAallow_1,0),
         @PAallow_2 = isnull(PAallow_2,0),
         @PAallow_3 = isnull(PAallow_3,0),
         @PAallow_4 = isnull(PAallow_4,0),
         @PAallow_5 = isnull(PAallow_5,0),
         @PAallow_6 = isnull(PAallow_6,0),
         @PAallow_7 = isnull(PAallow_7,0),
         @PAallow_8 = isnull(PAallow_8,0),
         @PAallow_9 = isnull(PAallow_9,0),
         @PAallow_10 = isnull(PAallow_10,0)
  from   PA42001 (nolock)
  
  if (@PAsetupkey = 0)
    begin
      select @O_iErrorState = 1014
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPSMISCLTRXTYPE < 1)
       or (@I_vPSMISCLTRXTYPE > 2))
    begin
      select @O_iErrorState = 1015
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPSMISCLTRXTYPE = 1)
    select @I_vPAREFNO = ''
  
  if exists (select 1
             from   PA30301 (nolock)
             where  PAMISCLDOCNO = @I_vPAMISCLDOCNO)
      or exists (select 1
                 from   PA10200 (nolock)
                 where  PAMISCLDOCNO = @I_vPAMISCLDOCNO)
      or exists (select 1
                 from   PA01901 (nolock)
                 where  PADocnumber20 = @I_vPAMISCLDOCNO
                        and PATranType = 3)
    begin
      select @O_iErrorState = 1016
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPSMISCID <> '')
    begin
      select @l_PSMISCID = isnull(PSMISCID,''),
             @PAinactive = isnull(PAinactive,0)
      from   PA00801 (nolock)
      where  PSMISCID = @I_vPSMISCID
      
      if (@l_PSMISCID = '')
        begin
          select @O_iErrorState = 1017
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@PAinactive = 1)
        begin
          select @O_iErrorState = 1018
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      select @O_iErrorState = 1019
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPSMISCLTRXTYPE = 2)
    begin
      if (@I_vPAREFNO = '')
        begin
          select @O_iErrorState = 1020
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      else
        begin
          if not exists (select 1
                         from   PA30300 (nolock)
                         where  PAMISCLDOCNO = @I_vPAREFNO)
            begin
              select @O_iErrorState = 1021
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
    end
  
  select @PAQtyQ = isnull(sum(PAQtyQ),0),
         @PATOTCST = isnull(sum(PATOTCST),0),
         @PAACREV = isnull(sum(PAACREV),0)
  from   PA10201 (nolock)
  where  PAMISCLDOCNO = @I_vPAMISCLDOCNO
  
  if (@I_vPATQTY < 0)
    begin
      if (@PAallow_5 = 0)
        begin
          select @O_iErrorState = 1022
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if ((@I_vPATQTY <> 0)
      and (@PAQtyQ <> @I_vPATQTY))
    begin
      select @O_iErrorState = 1023
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPATQTY = 0)
    select @I_vPATQTY = @PAQtyQ
  
  if (@I_vPAtotcosts < 0)
    begin
      select @O_iErrorState = 1024
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAtotcosts <> 0)
      and (@PATOTCST <> @I_vPAtotcosts))
    begin
      select @O_iErrorState = 1025
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAtotcosts = 0)
    select @I_vPAtotcosts = @PATOTCST
  
  if (@I_vPATACRV < 0)
    begin
      select @O_iErrorState = 1026
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPATACRV <> 0)
      and (@PAACREV <> @I_vPATACRV))
    begin
      if (@PAallow_5 = 0)
        begin
          select @O_iErrorState = 1027
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPATACRV = 0)
    select @I_vPATACRV = @PAACREV
  
  select @PAYR = datepart(year,@I_vPAREPDT)
  
  if ((@I_vPAREPD > @PAnumofreportingperiods)
      and (@PAreportingperiods <> 1))
    begin
      select @O_iErrorState = 1028
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@PAreportingperiods = 1)
    begin
      select @datediff = datediff(day,@PA1stdatereportperiod,@I_vPAREPDT)
      
      select @PAREPD = (@datediff%365) + 1
      
      select @PAREPDT = DATEADD(day,@datediff,@PA1stdatereportperiod),
             @PAPeriodEndDate = DATEADD(day,(@datediff),@PA1stdatereportperiod)
    end
  else
    if (@PAreportingperiods = 2)
      begin
        select @datediff = datediff(day,dateadd(day,-1,@PA1stdatereportperiod),
                                    @I_vPAREPDT)
        
        if @datediff > 7
          begin
            if (@datediff%7 <> 0)
              select @PAREPD = @datediff / 7 + 1
            else
              select @PAREPD = @datediff / 7
          end
        else
          begin
            select @datediff = 1,
                   @PAREPD = 1
          end
        
        SELECT @PAREPDT = DATEADD(week,@PAREPD - 1,@PA1stdatereportperiod),
               @PAPeriodEndDate = DATEADD(day,-1,DATEADD(week,(@PAREPD),@PA1stdatereportperiod)),
               @PAREPD = case 
                           when (@PAREPD > 52) then (@PAREPD%52)
                           else @PAREPD
                         end
      end
    else
      if (@PAreportingperiods = 3)
        begin
          select @datediff = datediff(week,@PA1stdatereportperiod,dateadd(day,1,@I_vPAREPDT))
          
          if (@datediff%2) <> 0
            select @datediff = @datediff + 1
          
          select @PAREPD = ((@datediff / 2)%26)
          
          select @PAREPDT = DATEADD(week,@datediff - 2,@PA1stdatereportperiod),
                 @PAPeriodEndDate = DATEADD(day,-1,DATEADD(week,(@datediff),@PA1stdatereportperiod))
        end
      else
        if (@PAreportingperiods = 4)
          begin
            if ((@I_vPAREPD%2) = 0)
              begin
                select @origdate = @PA1stdatereportperiod,
                       @PA1stdatereportperiod = dateadd(day,15,@PA1stdatereportperiod)
                
                select @datediff = (datediff(month,@PA1stdatereportperiod,@I_vPAREPDT)) + 1
                
                select @yeardiff = (@datediff / 12)
                
                select @PAREPD = ((@datediff%12) * 2)
                
                select @PAREPDT = DATEADD(month,@datediff - 1,@PA1stdatereportperiod),
                       @PAPeriodEndDate = DATEADD(day,-1,DATEADD(month,@datediff,@origdate))
              end
            else
              begin
                select @datediff = (datediff(month,@PA1stdatereportperiod,@I_vPAREPDT)) + 1
                
                select @yeardiff = (@datediff / 12)
                
                select @PAREPD = (@datediff%12) + ((@datediff%12) - 1)
                
                select @PAREPDT = DATEADD(month,@datediff - 1,@PA1stdatereportperiod),
                       @PAPeriodEndDate = DATEADD(day,14,DATEADD(month,@datediff - 1,@PA1stdatereportperiod))
              end
          end
        else
          if (@PAreportingperiods = 5)
            begin
              select @datediff = datediff(month,@PA1stdatereportperiod,@I_vPAREPDT) + 1
              
              select @PAREPD = @datediff%12
              
              select @PAREPDT = DATEADD(month,@datediff - 1,@PA1stdatereportperiod),
                     @PAPeriodEndDate = DATEADD(day,-1,DATEADD(month,(@datediff),@PA1stdatereportperiod))
            end
          else
            if (@PAreportingperiods = 6)
              begin
                select @datediff = datediff(month,@PA1stdatereportperiod,dateadd(day,1,@I_vPAREPDT))
                
                select @yeardiff = (@datediff / 12)
                
                select @PAREPD = (((@datediff%12) / 2) + 1)
                
                select @PAREPDT = DATEADD(month,(@yeardiff * 12) + ((@PAREPD - 1) * 2),
                                          @PA1stdatereportperiod),
                       @PAPeriodEndDate = DATEADD(day,-1,DATEADD(month,((@yeardiff * 12) + (@PAREPD * 2)),
                                                                 @PA1stdatereportperiod))
              end
            else
              if (@PAreportingperiods = 7)
                begin
                  select @datediff = datediff(month,@PA1stdatereportperiod,dateadd(day,1,@I_vPAREPDT))
                  
                  select @yeardiff = (@datediff / 12)
                  
                  select @PAREPD = (((@datediff%12) / 3) + 1)
                  
                  select @PAREPDT = DATEADD(month,(@yeardiff * 12) + ((@PAREPD - 1) * 3),
                                            @PA1stdatereportperiod),
                         @PAPeriodEndDate = DATEADD(day,-1,DATEADD(month,((@yeardiff * 12) + ((@PAREPD - 1) * 3) + 3),
                                                                   @PA1stdatereportperiod))
                end
  
  if (@PAreportingperiods <> 8)
    begin
      if ((@PAREPDT <> @I_vPAREPDT)
          and (@I_vPAREPDT <> ''))
        begin
          select @O_iErrorState = 1029
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (exists (select 1
                  from   PA10201 (nolock)
                  where  PAMISCLDOCNO = @I_vPAMISCLDOCNO
                         and ((PADT < @I_vPAREPDT)
                               or (PADT > @PAPeriodEndDate))))
        begin
          select @O_iErrorState = 1030
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @LNITMSEQ = isnull(max(LNITMSEQ),0)
  from   PA10201 (nolock)
  where  PAMISCLDOCNO = @I_vPAMISCLDOCNO
  
  if (@LNITMSEQ = 0)
    select @LNITMSEQ = 120
  else
    begin
      select @LNITMSEQ = @LNITMSEQ + 100
    end
  
  select @UPSTDTFR = isnull(UPSTDTFR,0)
  from   SY02300 (nolock)
  where  SERIES = 7
         and TRXSOURC = 'Misc. Log Entry'
  
  if (@UPSTDTFR = 0)
    begin
      select @PAPD = isnull(GLPOSTDT,'')
      from   SY00500 (nolock)
      WHERE  BCHSOURC = @BCHSOURC
             and BACHNUMB = @I_vBACHNUMB
    end
  else
    begin
      select @PAPD = @I_vPADOCDT
    end
  
  if (@I_vCURNCYID <> '')
    begin
      if not exists (select 1
                     from   DYNAMICS..MC40200 (nolock)
                     where  CURNCYID = @I_vCURNCYID)
        begin
          select @O_iErrorState = 1031
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vRATETPID <> '')
    begin
      if (not exists (select 1
                      from   MC40100 (nolock)
                      where  RATETPID = @I_vRATETPID))
        begin
          select @O_iErrorState = 1032
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @FUNLCURR = FUNLCURR
  from   MC40000 (nolock)
  
  select @DECPLCUR = DECPLCUR - 1
  from   DYNAMICS..MC40200 (nolock)
  where  CURNCYID = @FUNLCURR
  
  select @PAORIGTOTCOSTS = @I_vPAtotcosts,
         @PAORIACCRREV = @I_vPATACRV
  
  if (@I_vCURNCYID <> '')
     and (@I_vCURNCYID <> @FUNLCURR)
    begin
      select @ISMCTRX = 1
      
      select @CURRNIDX = isnull(CURRNIDX,0)
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @I_vCURNCYID
      
      if (@CURRNIDX = 0)
        begin
          select @O_iErrorState = 1033
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      exec @iStatus = taMCCurrencyValidate
        @I_vMASTERID = @I_vPSMISCID ,
        @I_vDOCDATE = @I_vPADOCDT ,
        @I_vCURNCYID = @I_vCURNCYID ,
        @I_vEXCHDATE = @I_vEXCHDATE ,
        @I_vEXGTBDSC = @I_vEXGTBDSC ,
        @I_vEXTBLSRC = @I_vEXTBLSRC ,
        @I_vRATEEXPR = @I_vRATEEXPR output ,
        @I_vDYSTINCR = @I_vDYSTINCR output ,
        @I_vRATEVARC = @I_vRATEVARC ,
        @I_vTRXDTDEF = @I_vTRXDTDEF ,
        @I_vPRVDSLMT = @I_vPRVDSLMT ,
        @I_vDATELMTS = @I_vDATELMTS ,
        @I_vMODULE = 0 ,
        @I_vTIME1 = @I_vTIME1 output ,
        @I_vXCHGRATE = @I_vXCHGRATE output ,
        @I_vEXPNDATE = @I_vEXPNDATE output ,
        @I_vRATETPID = @I_vRATETPID output ,
        @I_vRTCLCMTD = @I_vRTCLCMTD output ,
        @I_vEXGTBLID = @EXGTBLID output ,
        @oErrString = @oErrString output ,
        @O_iErrorState = @O_iErrorState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        select @iStatus = @iError
      
      if (@iStatus <> 0)
          or (@O_iErrorState <> 0)
        begin
          if (@O_iErrorState <> 0)
            begin
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          select @O_iErrorState = 1034
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 1035
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      select @ISMCTRX = 0,
             @I_vRATEEXPR = 0,
             @I_vDYSTINCR = 0,
             @I_vRTCLCMTD = 0
      
      select @CURRNIDX = isnull(FUNCRIDX,0)
      from   MC40000 (nolock)
      where  MC40000.FUNLCURR = @I_vCURNCYID
      
      select @CURRNIDX = isnull(@CURRNIDX,0)
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_MiscLog' ,
        @I_vINDEX1 = @I_vPAMISCLDOCNO ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 0 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2037
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@ISMCTRX = 1)
    begin
      select @RNDDIFF = isnull(RNDDIFF,0)
      from   MC40301 (nolock)
      where  RATETPID = @I_vRATETPID
             and CURNCYID = @I_vCURNCYID
             and EXGTBLID = @EXGTBLID
      
      if (@RNDDIFF = 0)
        begin
          select @RNDDIFF = isnull(RNDDIFF,0)
          from   MC40201 (nolock)
          where  CURNCYID = @I_vCURNCYID
        end
      
      if (@RNDDIFF = 0)
        begin
          select @RNDDIFF = isnull(ACTINDX,0)
          from   SY01100 (nolock)
          where  SERIES = 2
                 and SEQNUMBR = 900
        end
      
      if @RNDDIFF = 0
        begin
          select @O_iErrorState = 1036
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if @I_vXCHGRATE = 0
        begin
          select @O_iErrorState = 1037
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if @I_vRTCLCMTD = 0
        begin
          select @I_vPAtotcosts = round(@I_vPAtotcosts * @I_vXCHGRATE,@DECPLCUR),
                 @I_vPATACRV = round(@I_vPATACRV * @I_vXCHGRATE,@DECPLCUR)
        end
      else
        if @I_vRTCLCMTD = 1
          begin
            select @I_vPAtotcosts = round(@I_vPAtotcosts / @I_vXCHGRATE,@DECPLCUR),
                   @I_vPATACRV = round(@I_vPATACRV / @I_vXCHGRATE,@DECPLCUR)
          end
      
      if (@I_vRTCLCMTD = 0)
        begin
          update PA10201
          set    PAUNITCOST = round(PAUNITCOST * @I_vXCHGRATE,@DECPLCUR),
                 ORUNTCST = PAUNITCOST,
                 PABase_Unit_Cost = round(PABase_Unit_Cost * @I_vXCHGRATE,@DECPLCUR),
                 PAORGBSUNITCST = PABase_Unit_Cost,
                 PAEXTCOST = round(PAEXTCOST * @I_vXCHGRATE,@DECPLCUR),
                 OREXTCST = PAEXTCOST,
                 PATOTCST = round(PATOTCST * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGTOTCOST = PATOTCST,
                 PAOverhead_Amount = round(PAOverhead_Amount * @I_vXCHGRATE,@DECPLCUR),
                 PABaseOvhdCost = round(PABaseOvhdCost * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGOVHDAMT = PAOverhead_Amount,
                 PAORIGBASEOVRHCST = PABaseOvhdCost,
                 PATOTALOVERH = round(PATOTALOVERH * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGTOTOVRHD = PATOTALOVERH,
                 PABILRATE = round(PABILRATE * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGBILLRATE = PABILRATE,
                 PA_Base_Billing_Rate = round(PA_Base_Billing_Rate * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGBSBILLRTE = PA_Base_Billing_Rate,
                 PAACREV = round(PAACREV * @I_vXCHGRATE,@DECPLCUR),
                 PAORIACCRREV = PAACREV,
                 PAProfitAmount = round(PAProfitAmount * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGPROFAMT = PAProfitAmount,
                 PATotalProfit = round(PATotalProfit * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGTOTPROF = PATotalProfit,
                 RNDDIFF = @RNDDIFF,
                 PAMCCURNCYID = @I_vCURNCYID,
                 CURRNIDX = @CURRNIDX
          where  PAMISCLDOCNO = @I_vPAMISCLDOCNO
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 1038
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        if @I_vRTCLCMTD = 1
          begin
            update PA10201
            set    PAUNITCOST = round(PAUNITCOST / @I_vXCHGRATE,@DECPLCUR),
                   ORUNTCST = PAUNITCOST,
                   PABase_Unit_Cost = round(PABase_Unit_Cost / @I_vXCHGRATE,@DECPLCUR),
                   PAORGBSUNITCST = PABase_Unit_Cost,
                   PAEXTCOST = round(PAEXTCOST / @I_vXCHGRATE,@DECPLCUR),
                   OREXTCST = PAEXTCOST,
                   PATOTCST = round(PATOTCST / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGTOTCOST = PATOTCST,
                   PAOverhead_Amount = round(PAOverhead_Amount / @I_vXCHGRATE,@DECPLCUR),
                   PABaseOvhdCost = round(PABaseOvhdCost / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGOVHDAMT = PAOverhead_Amount,
                   PAORIGBASEOVRHCST = PABaseOvhdCost,
                   PATOTALOVERH = round(PATOTALOVERH / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGTOTOVRHD = PATOTALOVERH,
                   PABILRATE = round(PABILRATE / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGBILLRATE = PABILRATE,
                   PA_Base_Billing_Rate = round(PA_Base_Billing_Rate / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGBSBILLRTE = PA_Base_Billing_Rate,
                   PAACREV = round(PAACREV / @I_vXCHGRATE,@DECPLCUR),
                   PAORIACCRREV = PAACREV,
                   PAProfitAmount = round(PAProfitAmount / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGPROFAMT = PAProfitAmount,
                   PATotalProfit = round(PATotalProfit / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGTOTPROF = PATotalProfit,
                   RNDDIFF = @RNDDIFF,
                   PAMCCURNCYID = @I_vCURNCYID,
                   CURRNIDX = @CURRNIDX
            where  PAMISCLDOCNO = @I_vPAMISCLDOCNO
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 1039
                
                exec @iStatus = taUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
                
                return (@O_iErrorState)
              end
          end
    end
  else
    begin
      update PA10201
      set    PAMCCURNCYID = @I_vCURNCYID,
             CURRNIDX = @CURRNIDX
      where  PAMISCLDOCNO = @I_vPAMISCLDOCNO
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 9546
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  exec @iStatus = taCreateUpdateBatchHeaderRcd
    @I_vBACHNUMB = @I_vBACHNUMB ,
    @I_vSERIES = 7 ,
    @I_vGLPOSTDT = @I_vPADOCDT ,
    @I_vBCHSOURC = 'PA_ML' ,
    @I_vDOCAMT = @I_vPAtotcosts ,
    @I_vORIGIN = 0 ,
    @I_vNUMOFTRX = 1 ,
    @I_vCHEKBKID = '' ,
    @O_iErrorState = @iUpdtBthErrState output ,
    @oErrString = @iCreateBatchErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    select @iStatus = @iError
  
  if (@iStatus <> 0)
      or (@iUpdtBthErrState <> 0)
    begin
      if (@iUpdtBthErrState <> 0)
        begin
          exec @iStatus = taUpdateString
            @iUpdtBthErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 1040
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 c (nolock)
  where  c.INTERID = db_name()
  
  exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
    @I_sCompanyID = @sCompanyID ,
    @I_iSQLSessionID = 0 ,
    @O_mNoteIndex = @NOTEINDX output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iGetNextNoteIdxErrState <> 0)
    begin
      if (@iGetNextNoteIdxErrState <> 0)
        begin
          exec @iStatus = taUpdateString
            @iGetNextNoteIdxErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 1041
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@oErrString <> '')
    return (@O_iErrorState)
  
  declare CustomerSummary INSENSITIVE cursor  for
  select   isnull(b.CUSTNMBR,''),
           isnull(sum(a.PAQtyQ),0),
           isnull(sum(a.PATOTCST),0),
           isnull(sum(a.PATOTALOVERH),0),
           isnull(sum(a.PATotalProfit),0),
           isnull(sum(a.PAACREV),0)
  from     PA10201 a (nolock)
           join PA01201 b (nolock)
             ON a.PAPROJNUMBER = b.PAPROJNUMBER
  where    PAMISCLDOCNO = @I_vPAMISCLDOCNO
  group by b.CUSTNMBR
  
  open CustomerSummary
  
  select @iCursorError = @@cursor_rows
  
  if (@iCursorError > 0)
    begin
      fetch next from CustomerSummary
      into @CUSTNMBR,
           @sumtotalqty,
           @sumtotalcost,
           @sumttotaloverhead,
           @sumtotalprofit,
           @sumaccruedrevenues
      
      while (@@fetch_status <> -1)
        begin
          if (@@fetch_status = -2)
            begin
              select @O_iErrorState = 1042
              
              break
            end
          
          update PA00501
          set    PAUnpostedQty = PAUnpostedQty + @sumtotalqty,
                 PAUnpostedTotalCostN = PAUnpostedTotalCostN + @sumtotalcost,
                 PAUnposted_Overhead = PAUnposted_Overhead + @sumttotaloverhead,
                 PAUnpostedProfitN = PAUnpostedProfitN + @sumtotalprofit,
                 PAUnpostAccrRevN = PAUnpostAccrRevN + @sumaccruedrevenues
          where  CUSTNMBR = @CUSTNMBR
          
          select @iError = @@error
          
          if @iStatus = 0
             and @iError <> 0
            select @iStatus = @iError
          
          if (@iStatus <> 0)
              or (@O_oErrorState <> 0)
            begin
              if (@iStatus <> 0)
                begin
                  select @O_iErrorState = 1043
                end
              else
                begin
                  select @O_iErrorState = @O_oErrorState
                end
              
              deallocate CustomerSummary
              
              return (@O_iErrorState)
            end
          
          fetch next from CustomerSummary
          into @CUSTNMBR,
               @sumtotalqty,
               @sumtotalcost,
               @sumttotaloverhead,
               @sumtotalprofit,
               @sumaccruedrevenues
        end
    end
  
  deallocate CustomerSummary
  
  declare ContractMstrSummary INSENSITIVE cursor  for
  select   a.PACONTNUMBER,
           isnull(sum(a.PAQtyQ),0),
           isnull(sum(a.PATOTCST),0),
           isnull(sum(a.PATOTALOVERH),0),
           isnull(sum(a.PATotalProfit),0),
           isnull(sum(a.PAACREV),0)
  from     PA10201 a (nolock)
  where    PAMISCLDOCNO = @I_vPAMISCLDOCNO
  group by a.PACONTNUMBER
  
  open ContractMstrSummary
  
  select @iCursorError = @@cursor_rows
  
  if (@iCursorError > 0)
    begin
      fetch next from ContractMstrSummary
      into @PACONTNUMBER,
           @sumtotalqty,
           @sumtotalcost,
           @sumttotaloverhead,
           @sumtotalprofit,
           @sumaccruedrevenues
      
      while (@@fetch_status <> -1)
        begin
          if (@@fetch_status = -2)
            begin
              select @O_iErrorState = 1044
              
              break
            end
          
          update PA01101
          set    PAUnpostedQty = PAUnpostedQty + @sumtotalqty,
                 PAUnpostedTotalCostN = PAUnpostedTotalCostN + @sumtotalcost,
                 PAUnposted_Overhead = PAUnposted_Overhead + @sumttotaloverhead,
                 PAUnpostedProfitN = PAUnpostedProfitN + @sumtotalprofit,
                 PAUnpostAccrRevN = PAUnpostAccrRevN + @sumaccruedrevenues
          where  PACONTNUMBER = @PACONTNUMBER
          
          select @iError = @@error
          
          if @iStatus = 0
             and @iError <> 0
            select @iStatus = @iError
          
          if (@iStatus <> 0)
              or (@O_oErrorState <> 0)
            begin
              if (@iStatus <> 0)
                begin
                  select @O_iErrorState = 1045
                end
              else
                begin
                  select @O_iErrorState = @O_oErrorState
                end
              
              deallocate ContractMstrSummary
              
              return (@O_iErrorState)
            end
          
          fetch next from ContractMstrSummary
          into @PACONTNUMBER,
               @sumtotalqty,
               @sumtotalcost,
               @sumttotaloverhead,
               @sumtotalprofit,
               @sumaccruedrevenues
        end
    end
  
  deallocate ContractMstrSummary
  
  declare ProjectMstrSummary INSENSITIVE cursor  for
  select   a.PAPROJNUMBER,
           isnull(sum(a.PAQtyQ),0),
           isnull(sum(a.PATOTCST),0),
           isnull(sum(a.PATOTALOVERH),0),
           isnull(sum(a.PATotalProfit),0),
           isnull(sum(a.PAACREV),0)
  from     PA10201 a (nolock)
  where    PAMISCLDOCNO = @I_vPAMISCLDOCNO
  group by a.PAPROJNUMBER
  
  open ProjectMstrSummary
  
  select @iCursorError = @@cursor_rows
  
  if (@iCursorError > 0)
    begin
      fetch next from ProjectMstrSummary
      into @PAPROJNUMBER,
           @sumtotalqty,
           @sumtotalcost,
           @sumttotaloverhead,
           @sumtotalprofit,
           @sumaccruedrevenues
      
      while (@@fetch_status <> -1)
        begin
          if (@@fetch_status = -2)
            begin
              select @O_iErrorState = 1046
              
              break
            end
          
          update PA01201
          set    PAUnpostedQty = PAUnpostedQty + @sumtotalqty,
                 PAUnpostedTotalCostN = PAUnpostedTotalCostN + @sumtotalcost,
                 PAUnposted_Overhead = PAUnposted_Overhead + @sumttotaloverhead,
                 PAUnpostedProfitN = PAUnpostedProfitN + @sumtotalprofit,
                 PAUnpostAccrRevN = PAUnpostAccrRevN + @sumaccruedrevenues
          where  PAPROJNUMBER = @PAPROJNUMBER
          
          select @iError = @@error
          
          if @iStatus = 0
             and @iError <> 0
            select @iStatus = @iError
          
          if (@iStatus <> 0)
              or (@O_oErrorState <> 0)
            begin
              if (@iStatus <> 0)
                begin
                  select @O_iErrorState = 1047
                end
              else
                begin
                  select @O_iErrorState = @O_oErrorState
                end
              
              deallocate ProjectMstrSummary
              
              return (@O_iErrorState)
            end
          
          fetch next from ProjectMstrSummary
          into @PAPROJNUMBER,
               @sumtotalqty,
               @sumtotalcost,
               @sumttotaloverhead,
               @sumtotalprofit,
               @sumaccruedrevenues
        end
    end
  
  deallocate ProjectMstrSummary
  
  declare BudgetMstrSummary INSENSITIVE cursor  for
  select   a.PAPROJNUMBER,
           a.PACOSTCATID,
           b.PATU,
           isnull(sum(a.PAQtyQ),0),
           isnull(sum(a.PATOTCST),0),
           isnull(sum(a.PATOTALOVERH),0),
           isnull(sum(a.PATotalProfit),0),
           isnull(sum(a.PAACREV),0)
  from     PA10201 a (nolock)
           join PA01001 b (nolock)
             on a.PACOSTCATID = b.PACOSTCATID
  where    PAMISCLDOCNO = @I_vPAMISCLDOCNO
  group by a.PAPROJNUMBER,a.PACOSTCATID,b.PATU
  
  open BudgetMstrSummary
  
  select @iCursorError = @@cursor_rows
  
  if (@iCursorError > 0)
    begin
      fetch next from BudgetMstrSummary
      into @PAPROJNUMBER,
           @PACOSTCATID,
           @PATU,
           @sumtotalqty,
           @sumtotalcost,
           @sumttotaloverhead,
           @sumtotalprofit,
           @sumaccruedrevenues
      
      while (@@fetch_status <> -1)
        begin
          if (@@fetch_status = -2)
            begin
              select @O_iErrorState = 1048
              
              break
            end
          
          update PA01301
          set    PAUnpostedQty = PAUnpostedQty + @sumtotalqty,
                 PAUnpostedTotalCostN = PAUnpostedTotalCostN + @sumtotalcost,
                 PAUnposted_Overhead = PAUnposted_Overhead + @sumttotaloverhead,
                 PAUnpostedProfitN = PAUnpostedProfitN + @sumtotalprofit,
                 PAUnpostAccrRevN = PAUnpostAccrRevN + @sumaccruedrevenues
          where  PAPROJNUMBER = @PAPROJNUMBER
                 and PACOSTCATID = @PACOSTCATID
                 and PATU = @PATU
          
          select @iError = @@error
          
          if @iStatus = 0
             and @iError <> 0
            select @iStatus = @iError
          
          if (@iStatus <> 0)
              or (@O_oErrorState <> 0)
            begin
              if (@iStatus <> 0)
                begin
                  select @O_iErrorState = 1049
                end
              else
                begin
                  select @O_iErrorState = @O_oErrorState
                end
              
              deallocate BudgetMstrSummary
              
              return (@O_iErrorState)
            end
          
          fetch next from BudgetMstrSummary
          into @PAPROJNUMBER,
               @PACOSTCATID,
               @PATU,
               @sumtotalqty,
               @sumtotalcost,
               @sumttotaloverhead,
               @sumtotalprofit,
               @sumaccruedrevenues
        end
    end
  
  deallocate BudgetMstrSummary
  
  declare PeriodicSummary INSENSITIVE cursor  for
  select   a.PAPROJNUMBER,
           a.PACOSTCATID,
           b.PATU,
           a.PADT,
           isnull(sum(a.PAQtyQ),0),
           isnull(sum(a.PATOTCST),0),
           isnull(sum(a.PATOTALOVERH),0),
           isnull(sum(a.PATotalProfit),0),
           isnull(sum(a.PAACREV),0)
  from     PA10201 a (nolock)
           join PA01001 b (nolock)
             on a.PACOSTCATID = b.PACOSTCATID
  where    PAMISCLDOCNO = @I_vPAMISCLDOCNO
  group by a.PAPROJNUMBER,a.PACOSTCATID,b.PATU,a.PADT
  
  open PeriodicSummary
  
  select @iCursorError = @@cursor_rows
  
  if (@iCursorError > 0)
    begin
      fetch next from PeriodicSummary
      into @PAPROJNUMBER,
           @PACOSTCATID,
           @PATU,
           @PADT,
           @sumtotalqty,
           @sumtotalcost,
           @sumttotaloverhead,
           @sumtotalprofit,
           @sumaccruedrevenues
      
      while (@@fetch_status <> -1)
        begin
          if (@@fetch_status = -2)
            begin
              select @O_iErrorState = 1050
              
              break
            end
          
          select @userdate = getdate()
          
          execute @iStatus = glCalculateGLPeriod
            7 ,
            'Misc. Log Entry' ,
            @PADT ,
            @userdate ,
            @l_Period output ,
            @fClosed output ,
            @YEAR1 output ,
            @nErr output ,
            @O_iErrorState output
          
          select @iError = @@error
          
          if @iStatus = 0
             and @iError <> 0
            select @iStatus = @iError
          
          if (@iStatus <> 0)
              or (@O_iErrorState <> 0)
            begin
              if (@O_iErrorState <> 0)
                begin
                  exec @iStatus = taUpdateString
                    @iUpdDistErrState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
              
              select @O_iErrorState = 946
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          update PA01304
          set    PAUnpostedQty = PAUnpostedQty + @sumtotalqty,
                 PAUnpostedTotalCostN = PAUnpostedTotalCostN + @sumtotalcost,
                 PAUnposted_Overhead = PAUnposted_Overhead + @sumttotaloverhead,
                 PAUnpostedProfitN = PAUnpostedProfitN + @sumtotalprofit,
                 PAUnpostAccrRevN = PAUnpostAccrRevN + @sumaccruedrevenues
          where  PAPROJNUMBER = @PAPROJNUMBER
                 and PACOSTCATID = @PACOSTCATID
                 and PATU = @PATU
                 and YEAR1 = @YEAR1
                 and ((PERIODID = 0)
                       or (PERIODID = @l_Period))
          
          update PA01221
          set    PAUnpostedQty = PAUnpostedQty + @sumtotalqty,
                 PAUnpostedTotalCostN = PAUnpostedTotalCostN + @sumtotalcost,
                 PAUnposted_Overhead = PAUnposted_Overhead + @sumttotaloverhead,
                 PAUnpostedProfitN = PAUnpostedProfitN + @sumtotalprofit,
                 PAUnpostAccrRevN = PAUnpostAccrRevN + @sumaccruedrevenues
          where  PAPROJNUMBER = @PAPROJNUMBER
                 and YEAR1 = @YEAR1
                 and ((PERIODID = 0)
                       or (PERIODID = @l_Period))
          
          update PA01121
          set    PAUnpostedQty = PAUnpostedQty + @sumtotalqty,
                 PAUnpostedTotalCostN = PAUnpostedTotalCostN + @sumtotalcost,
                 PAUnposted_Overhead = PAUnposted_Overhead + @sumttotaloverhead,
                 PAUnpostedProfitN = PAUnpostedProfitN + @sumtotalprofit,
                 PAUnpostAccrRevN = PAUnpostAccrRevN + @sumaccruedrevenues
          where  PACONTNUMBER = @PACONTNUMBER
                 and YEAR1 = @YEAR1
                 and ((PERIODID = 0)
                       or (PERIODID = @l_Period))
          
          update PA00511
          set    PAUnpostedQty = PAUnpostedQty + @sumtotalqty,
                 PAUnpostedTotalCostN = PAUnpostedTotalCostN + @sumtotalcost,
                 PAUnposted_Overhead = PAUnposted_Overhead + @sumttotaloverhead,
                 PAUnpostedProfitN = PAUnpostedProfitN + @sumtotalprofit,
                 PAUnpostAccrRevN = PAUnpostAccrRevN + @sumaccruedrevenues
          where  CUSTNMBR = @CUSTNMBR
                 and YEAR1 = @YEAR1
                 and ((PERIODID = 0)
                       or (PERIODID = @l_Period))
          
          fetch next from PeriodicSummary
          into @PAPROJNUMBER,
               @PACOSTCATID,
               @PATU,
               @PADT,
               @sumtotalqty,
               @sumtotalcost,
               @sumttotaloverhead,
               @sumtotalprofit,
               @sumaccruedrevenues
        end
    end
  
  deallocate PeriodicSummary
  
  insert PA01901
        (PATranType,
         DOCNUMBR,
         CUSTNMBR,
         PADOCDT,
         PACOSTOWNER,
         RMDTYPAL,
         PABILLTRXT,
         PADocnumber20,
         DCSTATUS)
  select 3,
         '',
         '',
         @I_vPADOCDT,
         @I_vPSMISCID,
         0,
         0,
         @I_vPAMISCLDOCNO,
         1
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 1503
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  insert into PA10200
             (PSMISCLTRXTYPE,
              PAMISCLDOCNO,
              PADOCDT,
              PAYR,
              USERID,
              BACHNUMB,
              BCHSOURC,
              TRXSORCE,
              PAREFNO,
              PSMISCID,
              PAREPD,
              PAREPDT,
              PAPeriodEndDate,
              PACOMM,
              PAUD1,
              PAUD2,
              PATQTY,
              PAtotcosts,
              PATACRV,
              NOTEINDX,
              PAreptsuff,
              PAPD,
              LNITMSEQ,
              PAML_HDR_Errors,
              PAML_Dist_Errors,
              CURNCYID,
              CURRNIDX,
              RATETPID,
              EXGTBLID,
              XCHGRATE,
              EXCHDATE,
              TIME1,
              RTCLCMTD,
              DENXRATE,
              MCTRXSTT,
              PAORIGTOTCOSTS,
              PAORIACCRREV,
              Correcting_Trx_Type,
              CREATDDT,
              CRUSRID,
              PAORIGINLDOCNUM)
  select @I_vPSMISCLTRXTYPE,
         @I_vPAMISCLDOCNO,
         @I_vPADOCDT,
         @PAYR,
         @USERID,
         @I_vBACHNUMB,
         @BCHSOURC,
         @TRXSORCE,
         @I_vPAREFNO,
         @I_vPSMISCID,
         @I_vPAREPD,
         @I_vPAREPDT,
         @PAPeriodEndDate,
         @I_vPACOMM,
         @PAUD1,
         @PAUD2,
         @I_vPATQTY,
         @I_vPAtotcosts,
         @I_vPATACRV,
         @NOTEINDX,
         @I_vPAreptsuff,
         @PAPD,
         @LNITMSEQ,
         @PAML_HDR_Errors,
         @PAML_Dist_Errors,
         @I_vCURNCYID,
         @CURRNIDX,
         @I_vRATETPID,
         @EXGTBLID,
         @I_vXCHGRATE,
         @I_vEXCHDATE,
         @I_vTIME1,
         @I_vRTCLCMTD,
         @DENXRATE,
         @MCTRXSTT,
         @PAORIGTOTCOSTS,
         @PAORIACCRREV,
         0,
         '1/1/1900',
         '',
         ''
  
  if @@error <> 0
    begin
      select @O_iErrorState = 1051
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAMLCreateDistributions
    @I_vCURNCYID ,
    @CURRNIDX ,
    @I_vPAMISCLDOCNO ,
    @I_vPSMISCID ,
    @O_iErrorState output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    select @iStatus = @iError
  
  if (@iStatus <> 0)
      or (@O_iErrorState <> 0)
    begin
      if (@O_iErrorState <> 0)
        begin
          exec @iStatus = taUpdateString
            @iUpdDistErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 1052
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAMiscLogHdrInsertPost
    @I_vPSMISCLTRXTYPE ,
    @I_vPAMISCLDOCNO ,
    @I_vPADOCDT ,
    @I_vBACHNUMB ,
    @I_vPSMISCID ,
    @I_vPAREPD ,
    @I_vPAREPDT ,
    @I_vCURNCYID ,
    @I_vPAREFNO ,
    @I_vPAreptsuff ,
    @I_vPACOMM ,
    @I_vPATQTY ,
    @I_vPAtotcosts ,
    @I_vPATACRV ,
    @I_vXCHGRATE ,
    @I_vRATETPID ,
    @I_vEXPNDATE ,
    @I_vEXCHDATE ,
    @I_vEXGTBDSC ,
    @I_vEXTBLSRC ,
    @I_vRATEEXPR ,
    @I_vDYSTINCR ,
    @I_vRATEVARC ,
    @I_vTRXDTDEF ,
    @I_vRTCLCMTD ,
    @I_vPRVDSLMT ,
    @I_vDATELMTS ,
    @I_vTIME1 ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1053
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_MiscLog' ,
        @I_vINDEX1 = @I_vPAMISCLDOCNO ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 1 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2038
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  return (@O_iErrorState)

