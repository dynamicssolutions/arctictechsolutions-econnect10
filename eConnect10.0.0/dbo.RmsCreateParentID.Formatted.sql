

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsCreateParentID]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsCreateParentID]
  
create procedure [dbo].[RmsCreateParentID]
                @I_vCPRCSTNM        char(15),
                @I_vNAALLOWRECEIPTS tinyint  = 1,
                @I_vNACREDITCHECK   tinyint  = 0,
                @I_vNAFINANCECHARGE tinyint  = 0,
                @I_vNAHOLDINACTIVE  tinyint  = 0,
                @I_vNADEFPARENTVEN  tinyint  = 0,
                @I_vUpdateIfExists  tinyint  = 0,
                @I_vRequesterTrx    smallint  = 0,
                @I_vUSRDEFND1       char(50)  = '',
                @I_vUSRDEFND2       char(50)  = '',
                @I_vUSRDEFND3       char(50)  = '',
                @I_vUSRDEFND4       varchar(8000)  = '',
                @I_vUSRDEFND5       varchar(8000)  = '',
                @O_iErrorState      int  output,
                @oErrString         varchar(255)  output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  declare  @sCompanyID              smallint,
           @NOTEINDX                numeric(19,5),
           @iGetNextNoteIdxErrState int,
           @iCustomState            int,
           @iCustomErrString        varchar(255),
           @iStatus                 int,
           @iError                  int,
           @O_oErrorState           int
  
  select @sCompanyID = 0,
         @NOTEINDX = 0,
         @iGetNextNoteIdxErrState = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @O_iErrorState = 0,
         @iStatus = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  select @I_vCPRCSTNM = UPPER(@I_vCPRCSTNM)
  
  if (@I_vCPRCSTNM is NULL 
       or @I_vNAALLOWRECEIPTS is NULL
       or @I_vNACREDITCHECK is NULL
       or @I_vNAFINANCECHARGE is NULL
       or @I_vNAHOLDINACTIVE is NULL
       or @I_vNADEFPARENTVEN is NULL
       or @I_vUpdateIfExists is NULL
       or @I_vRequesterTrx is NULL)
    begin
      select @O_iErrorState = 1350
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vCPRCSTNM = '')
    begin
      select @O_iErrorState = 1351
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vCPRCSTNM <> '')
    begin
      if (not exists (select 1
                      from   RM00101 (nolock)
                      where  CUSTNMBR = @I_vCPRCSTNM))
        begin
          select @O_iErrorState = 7041
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vNAALLOWRECEIPTS not in (0,1))
    begin
      select @O_iErrorState = 6313
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vNACREDITCHECK not in (0,1))
    begin
      select @O_iErrorState = 6314
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vNAFINANCECHARGE not in (0,1))
    begin
      select @O_iErrorState = 6315
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vNAHOLDINACTIVE not in (0,1))
    begin
      select @O_iErrorState = 6316
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vNADEFPARENTVEN not in (0,1))
    begin
      select @O_iErrorState = 6317
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vUpdateIfExists not in (0,1))
    begin
      select @O_iErrorState = 6318
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vRequesterTrx not in (0,1))
    begin
      select @O_iErrorState = 6319
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @iCustomErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@O_iErrorState <> 0)
    begin
      return (@O_iErrorState)
    end
  
  if (@I_vUpdateIfExists = 0)
    begin
      if (exists (select 1
                  from   RM00105 (nolock)
                  where  CPRCSTNM = @I_vCPRCSTNM))
        begin
          select @O_iErrorState = 6323
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (exists (select top 1 CUSTNMBR
              from   RM00101 (nolock)
              where  CUSTNMBR = @I_vCPRCSTNM
                     and BALNCTYP = 1))
    begin
      select @O_iErrorState = 7875
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @iCustomErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (exists (select top 1 CUSTNMBR
              from   RM00101 (nolock)
              where  CUSTNMBR = @I_vCPRCSTNM
                     and CPRCSTNM <> CUSTNMBR
                     and CPRCSTNM <> ''))
    begin
      select @O_iErrorState = 7893
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @iCustomErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@O_iErrorState = 0)
    begin
      if (not exists (select 1
                      from   RM00105 (nolock)
                      where  CPRCSTNM = @I_vCPRCSTNM))
        begin
          select @sCompanyID = CMPANYID
          from   DYNAMICS..SY01500 (nolock)
          where  INTERID = db_name()
          
          exec @iStatus = DYNAMICS..RmssmGetNextNoteIndex
            @I_sCompanyID = @sCompanyID ,
            @I_iSQLSessionID = 0 ,
            @I_noteincrement = 1 ,
            @O_mNoteIndex = @NOTEINDX output ,
            @O_iErrorState = @iGetNextNoteIdxErrState output
          
          select @iError = @@error
          
          if ((@iStatus <> 0)
               or (@iGetNextNoteIdxErrState <> 0)
               or (@iError <> 0))
            begin
              select @oErrString = rtrim(@oErrString) + ' ' + @iGetNextNoteIdxErrState
              
              select @O_iErrorState = 6321
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          insert RM00105
                (CPRCSTNM,
                 NAALLOWRECEIPTS,
                 NACREDITCHECK,
                 NAFINANCECHARGE,
                 NAHOLDINACTIVE,
                 NADEFPARENTVEN,
                 NOTEINDX,
                 CREATDDT,
                 MODIFDT)
          select @I_vCPRCSTNM,
                 @I_vNAALLOWRECEIPTS,
                 @I_vNACREDITCHECK,
                 @I_vNAFINANCECHARGE,
                 @I_vNAHOLDINACTIVE,
                 @I_vNADEFPARENTVEN,
                 @NOTEINDX,
                 convert(varchar(12),getdate()),
                 convert(varchar(12),getdate())
          
          if @@error <> 0
            begin
              select @O_iErrorState = 1411
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          update RM00101
          set    CPRCSTNM = @I_vCPRCSTNM
          where  CUSTNMBR = @I_vCPRCSTNM
          
          if @@error <> 0
            begin
              select @O_iErrorState = 1412
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        if (@I_vUpdateIfExists = 1)
          begin
            update RM00105
            set    NAALLOWRECEIPTS = @I_vNAALLOWRECEIPTS,
                   NACREDITCHECK = @I_vNACREDITCHECK,
                   NAFINANCECHARGE = @I_vNAFINANCECHARGE,
                   NAHOLDINACTIVE = @I_vNAHOLDINACTIVE,
                   NADEFPARENTVEN = @I_vNADEFPARENTVEN,
                   MODIFDT = convert(varchar(12),getdate())
            where  CPRCSTNM = @I_vCPRCSTNM
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 6322
                
                exec @iStatus = RmsUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
                
                return (@O_iErrorState)
              end
          end
    end
  
  return (@O_iErrorState)

