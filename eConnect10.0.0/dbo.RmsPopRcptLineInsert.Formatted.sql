

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsPopRcptLineInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsPopRcptLineInsert]
  
create procedure [dbo].[RmsPopRcptLineInsert]
                @I_vPOPTYPE                  smallint,
                @I_vPOPRCTNM                 char(17),
                @I_vPONUMBER                 char(17)  = '',
                @I_vITEMNMBR                 char(30)  = '',
                @I_vITEMDESC                 char(100)  = '',
                @I_vVENDORID                 char(15),
                @I_vRCPTLNNM                 int  = 0,
                @I_vVNDITNUM                 char(30)  = '',
                @I_vVNDITDSC                 char(100)  = '',
                @I_vACTLSHIP                 datetime  = '',
                @I_vINVINDX                  int  = 0,
                @I_vInventoryAccount         varchar(75)  = '',
                @I_vUOFM                     char(8)  = '',
                @I_vUNITCOST                 numeric(19,5)  = null,
                @I_vEXTDCOST                 numeric(19,5)  = null,
                @I_vNONINVEN                 smallint  = 0,
                @I_vJOBNUMBR                 char(17)  = '',
                @I_vBOLPRONUMBER             char(30)  = '',
                @I_vQTYSHPPD                 numeric(19,5),
                @I_vQTYINVCD                 numeric(19,5)  = 0,
                @I_vAUTOCOST                 int  = 0,
                @I_vPurchase_IV_Item_Taxable smallint  = 2,
                @I_vPurchase_Item_Tax_Schedu char(15)  = '',
                @I_vPurchase_Site_Tax_Schedu char(15)  = '',
                @I_vTAXAMNT                  numeric(19,5)  = 0,
                @I_vLanded_Cost_Group_ID     char(15)  = null,
                @I_vLOCNCODE                 char(10)  = '',
                @I_vPOLNENUM                 int  = 0,
                @I_vreceiptdate              datetime  = '',
                @I_vCURNCYID                 char(15)  = '',
                @I_vProjNum                  char(15)  = null,
                @I_vCostCatID                char(15)  = null,
                @I_vAutoAssignBin            smallint  = 1,
                @I_vCMMTTEXT                 varchar(500)  = null,
                @I_vRequesterTrx             smallint  = 0,
                @I_vUSRDEFND1                char(50)  = '',
                @I_vUSRDEFND2                char(50)  = '',
                @I_vUSRDEFND3                char(50)  = '',
                @I_vUSRDEFND4                varchar(8000)  = '',
                @I_vUSRDEFND5                varchar(8000)  = '',
                @O_iErrorState               int  output,
                @oErrString                  varchar(255)  output     /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @PACONTNUMBER              char(11),
           @LCCOST                    numeric(19,5),
           @Total_Landed_Cost_Amount  numeric(19,5),
           @LCLINENUMBER              int,
           @Landed_Cost_ID            char(15),
           @LOFSGMNT                  int,
           @LOFSGMNTALL               int,
           @LOFSGMNTEND               int,
           @ACCNT_STRING              char(100),
           @NEW_ACCNT_STRING          varchar(100),
           @MAXSEG                    int,
           @ACTINDX                   int,
           @ACSGFLOC                  smallint,
           @Location_Segment          char(67),
           @RcptLineNoteIDArray_1     numeric(19,5),
           @RcptLineNoteIDArray_2     numeric(19,5),
           @RcptLineNoteIDArray_3     numeric(19,5),
           @RcptLineNoteIDArray_4     numeric(19,5),
           @RcptLineNoteIDArray_5     numeric(19,5),
           @RcptLineNoteIDArray_6     numeric(19,5),
           @RcptLineNoteIDArray_7     numeric(19,5),
           @RcptLineNoteIDArray_8     numeric(19,5),
           @DECPLCUR                  smallint,
           @DECPLQTY                  smallint,
           @ODECPLCU                  smallint,
           @JOBNUMBR                  char(17),
           @INVINDX                   int,
           @VCTNMTHD                  smallint,
           @ITMTRKOP                  smallint,
           @UOFM                      char(8),
           @PRCHSUOM                  char(8),
           @UMQTYINB                  numeric(19,5),
           @ITEMNMBR                  char(30),
           @ITEMDESC                  char(100),
           @VNDITNUM                  char(30),
           @VNDITDSC                  char(100),
           @Capital_Item              tinyint,
           @LOCNCODE                  char(10),
           @LOCNCODEREG               char(10),
           @POLNENUM                  int,
           @POSTATUS                  smallint,
           @POLNESTA                  smallint,
           @POTYPE                    smallint,
           @CURNCYID                  char(15),
           @CURRNIDX                  int,
           @iStatus                   int,
           @iError                    int,
           @iCustomState              int,
           @iCustomErrString          varchar(255),
           @iCursorError              int,
           @O_oErrorState             int,
           @O_iUpdCrtItemErrState     int,
           @O_iPOLineInsrErrState     int,
           @O_iPOHdrErrState          int,
           @O_iCommentMstErrState     int,
           @O_iLineIvcInsrtErrState   int,
           @UNITCOST                  numeric(19,5),
           @ORUNTCST                  numeric(19,5),
           @iGetNextNoteIdxErrState   int,
           @sCompanyID                smallint,
           @RCPTLNNM                  smallint,
           @SHIPMTHD                  char(15),
           @VADCDPAD                  char(15),
           @PURPVIDX                  int,
           @Revalue_Inventory         int,
           @Tolerance_Percentage      int,
           @POPALWOP_2                tinyint,
           @SERLTQTY                  numeric(19,5),
           @VENDORID                  char(15),
           @STNDCOST                  numeric(19,5),
           @UPPVIDX                   int,
           @RUPPVAMT                  numeric(19,5),
           @OLDCUCST                  numeric(19,5),
           @ACPURIDX                  int,
           @ISMCTRX                   tinyint,
           @FUNLCURR                  char(15),
           @DECPLCURItem              smallint,
           @EDITDECPLCUR              smallint,
           @FUNDECPLCUR               smallint,
           @MCINSTALLED               smallint,
           @LandedCostErrState        int,
           @LandedCostErrString       varchar(255),
           @projectexists             smallint,
           @PASTAT                    smallint,
           @PAcloseProjcosts          smallint,
           @costcatexists             smallint,
           @PATU                      smallint,
           @PAinactive                smallint,
           @CUSTNMBR                  char(15),
           @ENABLEMULTIBIN            smallint,
           @ITEMTYPE                  tinyint,
           @count                     int,
           @PassedExtCost             tinyint,
           @PAProjectType             smallint,
           @PAAcctgMethod             smallint,
           @PACOSTCATID               char(15),
           @PATMProfitType            int,
           @l_ProjNum                 char(15),
           @PAbllngtype               int,
           @PAOverhead_Amount         numeric(19,5),
           @PAOverheaPercentage       numeric(19,5),
           @PABILRATE                 numeric(19,5),
           @PAProfitType              smallint,
           @PABaselinePTaxOptions     smallint,
           @PAPurchase_Tax_Options    smallint,
           @PAProfitAmount            numeric(19,5),
           @PAProfitPercent           numeric(19,5),
           @PA_Variance_QTY_Accrued_  numeric(19,5),
           @PA_Variance_Accrued_Reve  numeric(19,5),
           @PO_UOMSCHDL               char(11),
           @PO_UOFM                   char(9),
           @PO_ITMTSHID               char(15),
           @PO_TRDISAMT               numeric(19,5),
           @PO_PALineItemSeq          int,
           @PO_PAIV_Item_Checkbox     smallint,
           @PO_PATU                   smallint,
           @PO_PAPurchase_Tax_Options smallint,
           @PO_PRICELVL               char(15),
           @PO_PACHGORDNO             char(17),
           @PACOSTCATNME              char(31),
           @PA_Variance_Cost          tinyint,
           @Status                    smallint,
           @PABilling_StatusN         numeric(19,5),
           @PAMARKPERCENT             numeric(19,5),
           @PAviprofittypefrom        smallint,
           @use_billing_rate          smallint,
           @use_markup_percentage     smallint,
           @PAINCPRCHTXPRJCST         smallint,
           @PA_Base_Billing_Rate      numeric(19,5),
           @PABaseOvhdCost            numeric(19,5),
           @PAShipmentExtCost         numeric(19,5),
           @tax_amount                numeric(19,5),
           @PAOverhead_IDX            int,
           @PATotalProfit             numeric(19,5),
           @PATOTALOVERH              numeric(19,5),
           @PAACREV                   numeric(19,5),
           @PA00901_PATMProfitType    int,
           @PA01001_PATMProfitType    smallint,
           @PAsetupkey                smallint,
           @PO_PACogs_Idx             integer,
           @PO_PACGBWIPIDX            integer,
           @PABase_Unit_Cost          numeric(19,5),
           @PABase_Qty                numeric(19,5),
           @PAPostedBillingsN         numeric(19,5),
           @PACogs_Idx                integer,
           @PACGBWIPIDX               integer,
           @PAUnbilled_AR_Idx         integer,
           @PAUnbilled_Proj_Rev_Idx   integer,
           @PAContra_Account_IDX      integer,
           @PO_INVINDX                integer,
           @PA_MC_Accrued_Revenue     numeric(19,5),
           @PROJECTLOADED             integer,
           @PA_MC_Base_Billing_Rate   numeric(19,5),
           @PA_MC_Billing_Rate        numeric(19,5),
           @PA_MC_VarianceAccruedRev  numeric(19,5),
           @UNITPRCE                  numeric(19,5),
           @PRICMTHD                  smallint,
           @LISTPRCE                  numeric(19,5),
           @QTYBSUOM                  numeric(19,5),
           @UOMSCHDL                  char(10),
           @UOFMBASE                  char(8),
           @CURRCOST                  numeric(19,5),
           @ROUNDTO                   smallint,
           @ROUNDHOW                  smallint,
           @RNDGAMNT                  numeric(19,5),
           @PADECPLQTY                smallint,
           @NOTEINDX                  numeric(19,5),
           @POLNEARY_3                numeric(19,5),
           @DATE1                     datetime,
           @TIME1                     datetime
  
  select @PACONTNUMBER = '',
         @LCCOST = 0,
         @Total_Landed_Cost_Amount = 0,
         @LCLINENUMBER = 0,
         @Landed_Cost_ID = '',
         @LOFSGMNT = 0,
         @LOFSGMNTALL = 0,
         @LOFSGMNTEND = 0,
         @ACCNT_STRING = '',
         @NEW_ACCNT_STRING = '',
         @MAXSEG = 0,
         @ACTINDX = 0,
         @ACSGFLOC = 0,
         @Location_Segment = '',
         @UNITCOST = 0,
         @ORUNTCST = 0,
         @ITMTRKOP = 1,
         @LOCNCODE = '',
         @DECPLCUR = 0,
         @DECPLQTY = 0,
         @iGetNextNoteIdxErrState = 0,
         @sCompanyID = '',
         @RcptLineNoteIDArray_1 = 0,
         @RcptLineNoteIDArray_2 = 0,
         @RcptLineNoteIDArray_3 = 0,
         @RcptLineNoteIDArray_4 = 0,
         @RcptLineNoteIDArray_5 = 0,
         @RcptLineNoteIDArray_6 = 0,
         @RcptLineNoteIDArray_7 = 0,
         @RcptLineNoteIDArray_8 = 0,
         @UMQTYINB = 0,
         @UOFM = '',
         @POLNENUM = 0,
         @POSTATUS = 0,
         @POLNESTA = 0,
         @POTYPE = 0,
         @DECPLQTY = 0,
         @VCTNMTHD = 0,
         @DECPLCUR = 0,
         @JOBNUMBR = '',
         @INVINDX = 0,
         @VNDITNUM = '',
         @VNDITDSC = '',
         @Capital_Item = 0,
         @O_oErrorState = 0,
         @CURRNIDX = 0,
         @O_iErrorState = 0,
         @iStatus = 0,
         @O_iUpdCrtItemErrState = 0,
         @O_iPOLineInsrErrState = 0,
         @O_iPOHdrErrState = 0,
         @O_iCommentMstErrState = 0,
         @O_iLineIvcInsrtErrState = 0,
         @iGetNextNoteIdxErrState = 0,
         @RCPTLNNM = 0,
         @SHIPMTHD = '',
         @VADCDPAD = '',
         @PURPVIDX = 0,
         @Revalue_Inventory = 0,
         @Tolerance_Percentage = 0,
         @POPALWOP_2 = 0,
         @SERLTQTY = 0,
         @VENDORID = '',
         @ITEMNMBR = '',
         @PRCHSUOM = '',
         @LOCNCODEREG = '',
         @ITEMDESC = '',
         @STNDCOST = 0,
         @UPPVIDX = 0,
         @RUPPVAMT = 0,
         @OLDCUCST = 0,
         @ACPURIDX = 0,
         @ISMCTRX = 0,
         @FUNLCURR = '',
         @DECPLCURItem = 0,
         @EDITDECPLCUR = 0,
         @FUNDECPLCUR = 0,
         @MCINSTALLED = 1,
         @LandedCostErrState = 0,
         @LandedCostErrString = '',
         @projectexists = 0,
         @PASTAT = 0,
         @PAcloseProjcosts = 0,
         @costcatexists = 0,
         @PATU = 0,
         @PAinactive = 0,
         @CUSTNMBR = '',
         @ENABLEMULTIBIN = 0,
         @ITEMTYPE = 0,
         @count = 0,
         @PassedExtCost = 0,
         @PAProjectType = 0,
         @PAAcctgMethod = 0,
         @PACOSTCATID = '',
         @PATMProfitType = 0,
         @l_ProjNum = '',
         @PAbllngtype = 0,
         @PAOverhead_Amount = 0,
         @PAOverheaPercentage = 0,
         @PABILRATE = 0,
         @PAProfitType = 0,
         @PABaselinePTaxOptions = 0,
         @PAPurchase_Tax_Options = 0,
         @PAProfitAmount = 0,
         @PAProfitPercent = 0,
         @PA_Variance_QTY_Accrued_ = 0,
         @PA_Variance_Accrued_Reve = 0,
         @PO_UOMSCHDL = '',
         @PO_UOFM = '',
         @PO_ITMTSHID = '',
         @PO_TRDISAMT = 0,
         @PO_PALineItemSeq = 0,
         @PO_PAIV_Item_Checkbox = 0,
         @PO_PATU = 4,
         @PO_PAPurchase_Tax_Options = 0,
         @PO_PRICELVL = '',
         @PO_PACHGORDNO = '',
         @PACOSTCATNME = '',
         @PA_Variance_Cost = 0,
         @Status = 0,
         @PABilling_StatusN = 0,
         @PAMARKPERCENT = 0,
         @PAviprofittypefrom = 0,
         @use_billing_rate = 0,
         @use_markup_percentage = 0,
         @PAINCPRCHTXPRJCST = 0,
         @PA_Base_Billing_Rate = 0,
         @PABaseOvhdCost = 0,
         @PAShipmentExtCost = 0,
         @tax_amount = 0,
         @PAOverhead_IDX = 0,
         @PATotalProfit = 0,
         @PATOTALOVERH = 0,
         @PAACREV = 0,
         @PA00901_PATMProfitType = 0,
         @PA01001_PATMProfitType = 0,
         @PAsetupkey = 0,
         @PO_PACogs_Idx = 0,
         @PO_PACGBWIPIDX = 0,
         @PABase_Unit_Cost = 0,
         @PABase_Qty = 0,
         @PAPostedBillingsN = 0,
         @PACogs_Idx = 0,
         @PACGBWIPIDX = 0,
         @PAUnbilled_AR_Idx = 0,
         @PAUnbilled_Proj_Rev_Idx = 0,
         @PAContra_Account_IDX = 0,
         @PO_INVINDX = 0,
         @PA_MC_Accrued_Revenue = 0,
         @PROJECTLOADED = 0,
         @PA_MC_Base_Billing_Rate = 0,
         @PA_MC_Billing_Rate = 0,
         @PA_MC_VarianceAccruedRev = 0,
         @UNITPRCE = 0,
         @PRICMTHD = 0,
         @LISTPRCE = 0,
         @QTYBSUOM = 1,
         @UOMSCHDL = '',
         @UOFMBASE = '',
         @CURRCOST = 0,
         @ROUNDTO = 0,
         @ROUNDHOW = 0,
         @RNDGAMNT = 0,
         @PADECPLQTY = 0,
         @NOTEINDX = 0,
         @POLNEARY_3 = 0,
         @DATE1 = '',
         @TIME1 = ''
  
  if (@oErrString is null)
    begin
      select @oErrString = ''
    end
  
  if (@I_vACTLSHIP is null 
       or @I_vBOLPRONUMBER is null
       or @I_vINVINDX is null
       or @I_vInventoryAccount is null
       or @I_vITEMDESC is null
       or @I_vITEMNMBR is null
       or @I_vJOBNUMBR is null
       or @I_vNONINVEN is null
       or @I_vPONUMBER is null
       or @I_vPOPRCTNM is null
       or @I_vRCPTLNNM is null
       or @I_vUOFM is null
       or @I_vVNDITDSC is null
       or @I_vVNDITNUM is null
       or @I_vPOPTYPE is null
       or @I_vCURNCYID is null
       or @I_vAutoAssignBin is null)
    begin
      select @O_iErrorState = 2050
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPOPRCTNM = ''
       or (@I_vITEMNMBR = ''
           and @I_vVNDITNUM = ''))
    begin
      select @O_iErrorState = 2051
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vPONUMBER = UPPER(@I_vPONUMBER),
         @I_vITEMNMBR = UPPER(@I_vITEMNMBR),
         @I_vVENDORID = UPPER(@I_vVENDORID),
         @I_vPOPRCTNM = UPPER(@I_vPOPRCTNM),
         @I_vCURNCYID = UPPER(@I_vCURNCYID)
  
  if exists (select 1
             from   POP10300 (nolock)
             where  POPRCTNM = @I_vPOPRCTNM)
      or exists (select 1
                 from   POP30300 (nolock)
                 where  POPRCTNM = @I_vPOPRCTNM)
    begin
      select @O_iErrorState = 8053
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vAUTOCOST = 1)
      and ((@I_vUNITCOST > 0)
            or (@I_vEXTDCOST > 0)))
    begin
      select @O_iErrorState = 594
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPOPTYPE not in (1,3))
    begin
      select @O_iErrorState = 5455
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPONUMBER <> '')
    begin
      if not exists (select 1
                     from   POP10100 (nolock)
                     where  PONUMBER = @I_vPONUMBER)
        begin
          select @O_iErrorState = 2052
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @POSTATUS = POSTATUS,
         @CUSTNMBR = CUSTNMBR
  from   POP10100 (nolock)
  where  PONUMBER = @I_vPONUMBER
  
  if (@POSTATUS in (4,5,6))
    begin
      select @O_iErrorState = 589
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPOLNENUM < 0)
    begin
      select @O_iErrorState = 3810
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @FUNLCURR = FUNLCURR
  from   MC40000 (nolock)
  
  if (@O_iErrorState <> 0)
    begin
      return (@O_iErrorState)
    end
  
  if (@I_vNONINVEN = 0)
    begin
      if (@I_vVNDITNUM = '')
        begin
          select @I_vVNDITNUM = VNDITNUM
          from   IV00103 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and VENDORID = @I_vVENDORID
        end
      
      if (@I_vITEMNMBR = '')
        begin
          select @count = count(ITEMNMBR)
          from   IV00103 (nolock)
          where  VNDITNUM = @I_vVNDITNUM
                 and VENDORID = @I_vVENDORID
          
          if (@count > 1)
            begin
              select @O_iErrorState = 9008
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          else
            begin
              select @I_vITEMNMBR = ITEMNMBR
              from   IV00103 (nolock)
              where  VNDITNUM = @I_vVNDITNUM
                     and VENDORID = @I_vVENDORID
            end
        end
    end
  
  if ((@I_vNONINVEN = 1)
      and (@I_vVNDITNUM = ''))
    begin
      select @I_vVNDITNUM = @I_vITEMNMBR
    end
  
  if (@I_vPONUMBER <> '')
    begin
      if (@I_vPOLNENUM <> 0)
        begin
          if not exists (select 1
                         from   POP10110 (nolock)
                         where  PONUMBER = @I_vPONUMBER
                                and ORD = @I_vPOLNENUM)
            begin
              select @O_iErrorState = 3808
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          select @ITEMDESC = ITEMDESC,
                 @VNDITDSC = VNDITDSC,
                 @VNDITNUM = VNDITNUM,
                 @UOFM = UOFM,
                 @INVINDX = INVINDX,
                 @JOBNUMBR = JOBNUMBR,
                 @Capital_Item = Capital_Item,
                 @DECPLCUR = DECPLCUR,
                 @DECPLQTY = DECPLQTY,
                 @ODECPLCU = ODECPLCU,
                 @LOCNCODE = LOCNCODE,
                 @UMQTYINB = UMQTYINB,
                 @UNITCOST = UNITCOST,
                 @ORUNTCST = ORUNTCST,
                 @POLNESTA = POLNESTA,
                 @POTYPE = POTYPE,
                 @VENDORID = VENDORID,
                 @ITEMNMBR = ITEMNMBR,
                 @I_vPurchase_IV_Item_Taxable = Purchase_IV_Item_Taxable,
                 @I_vLanded_Cost_Group_ID = case 
                                              when @I_vLanded_Cost_Group_ID is null then Landed_Cost_Group_ID
                                              else @I_vLanded_Cost_Group_ID
                                            end,
                 @I_vProjNum = case 
                                 when @I_vProjNum is null then ProjNum
                                 else @I_vProjNum
                               end,
                 @I_vCostCatID = case 
                                   when @I_vCostCatID is null then CostCatID
                                   else @I_vCostCatID
                                 end,
                 @POLNEARY_3 = POLNEARY_3
          from   POP10110 (nolock)
          where  PONUMBER = @I_vPONUMBER
                 and ORD = @I_vPOLNENUM
          
          if (@I_vITEMNMBR <> @ITEMNMBR)
            begin
              select @O_iErrorState = 3809
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if (@I_vUOFM <> '')
            begin
              if (@I_vUOFM <> @UOFM)
                begin
                  select @O_iErrorState = 6663
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
          
          if (@I_vVENDORID <> @VENDORID)
            begin
              select @O_iErrorState = 6662
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      else
        begin
          if (@I_vLOCNCODE <> '')
            begin
              select @count = count(ORD)
              from   POP10110 (nolock)
              where  PONUMBER = @I_vPONUMBER
                     and ITEMNMBR = @I_vITEMNMBR
                     and VNDITNUM = @I_vVNDITNUM
                     and VENDORID = @I_vVENDORID
                     and LOCNCODE = @I_vLOCNCODE
              
              select @ITEMDESC = ITEMDESC,
                     @VNDITDSC = VNDITDSC,
                     @VNDITNUM = VNDITNUM,
                     @UOFM = UOFM,
                     @INVINDX = INVINDX,
                     @JOBNUMBR = JOBNUMBR,
                     @Capital_Item = Capital_Item,
                     @DECPLCUR = DECPLCUR,
                     @DECPLQTY = DECPLQTY,
                     @ODECPLCU = ODECPLCU,
                     @POLNENUM = ORD,
                     @LOCNCODE = LOCNCODE,
                     @UMQTYINB = UMQTYINB,
                     @UNITCOST = UNITCOST,
                     @ORUNTCST = ORUNTCST,
                     @POLNESTA = POLNESTA,
                     @POTYPE = POTYPE,
                     @VENDORID = VENDORID,
                     @I_vPurchase_IV_Item_Taxable = Purchase_IV_Item_Taxable,
                     @I_vLanded_Cost_Group_ID = case 
                                                  when @I_vLanded_Cost_Group_ID is null then Landed_Cost_Group_ID
                                                  else @I_vLanded_Cost_Group_ID
                                                end,
                     @I_vProjNum = case 
                                     when @I_vProjNum is null then ProjNum
                                     else @I_vProjNum
                                   end,
                     @I_vCostCatID = case 
                                       when @I_vCostCatID is null then CostCatID
                                       else @I_vCostCatID
                                     end,
                     @POLNEARY_3 = POLNEARY_3
              from   POP10110 (nolock)
              where  PONUMBER = @I_vPONUMBER
                     and ITEMNMBR = @I_vITEMNMBR
                     and VNDITNUM = @I_vVNDITNUM
                     and VENDORID = @I_vVENDORID
                     and LOCNCODE = @I_vLOCNCODE
            end
          else
            begin
              select @count = count(ORD)
              from   POP10110 (nolock)
              where  PONUMBER = @I_vPONUMBER
                     and ITEMNMBR = @I_vITEMNMBR
                     and VNDITNUM = @I_vVNDITNUM
                     and VENDORID = @I_vVENDORID
              
              select @ITEMDESC = ITEMDESC,
                     @VNDITDSC = VNDITDSC,
                     @VNDITNUM = VNDITNUM,
                     @UOFM = UOFM,
                     @INVINDX = INVINDX,
                     @JOBNUMBR = JOBNUMBR,
                     @Capital_Item = Capital_Item,
                     @DECPLCUR = DECPLCUR,
                     @DECPLQTY = DECPLQTY,
                     @ODECPLCU = ODECPLCU,
                     @POLNENUM = ORD,
                     @LOCNCODE = LOCNCODE,
                     @UMQTYINB = UMQTYINB,
                     @UNITCOST = UNITCOST,
                     @ORUNTCST = ORUNTCST,
                     @POLNESTA = POLNESTA,
                     @POTYPE = POTYPE,
                     @VENDORID = VENDORID,
                     @I_vPurchase_IV_Item_Taxable = Purchase_IV_Item_Taxable,
                     @I_vLanded_Cost_Group_ID = case 
                                                  when @I_vLanded_Cost_Group_ID is null then Landed_Cost_Group_ID
                                                  else @I_vLanded_Cost_Group_ID
                                                end,
                     @I_vProjNum = case 
                                     when @I_vProjNum is null then ProjNum
                                     else @I_vProjNum
                                   end,
                     @I_vCostCatID = case 
                                       when @I_vCostCatID is null then CostCatID
                                       else @I_vCostCatID
                                     end,
                     @POLNEARY_3 = POLNEARY_3
              from   POP10110 (nolock)
              where  PONUMBER = @I_vPONUMBER
                     and ITEMNMBR = @I_vITEMNMBR
                     and VNDITNUM = @I_vVNDITNUM
                     and VENDORID = @I_vVENDORID
            end
          
          if (@count = 1)
            begin
              select @I_vPOLNENUM = @POLNENUM
            end
          else
            begin
              if (@count > 1)
                begin
                  select @O_iErrorState = 9344
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
              
              if (@count = 0)
                begin
                  select @O_iErrorState = 9343
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
        end
    end
  else
    begin
      select @POTYPE = case 
                         when (@PROJECTLOADED = 1)
                              and (@I_vPOPTYPE = 3) then 1
                         else 0
                       end,
             @I_vUOFM = case 
                          when (@I_vNONINVEN = 1)
                               and (@I_vUOFM = '') then 'Each'
                          else @I_vUOFM
                        end,
             @UMQTYINB = case 
                           when @I_vNONINVEN = 1 then 1
                           else 0
                         end,
             @I_vLanded_Cost_Group_ID = case 
                                          when @I_vLanded_Cost_Group_ID is null then ''
                                          else @I_vLanded_Cost_Group_ID
                                        end,
             @I_vProjNum = case 
                             when @I_vProjNum is null then ''
                             else @I_vProjNum
                           end,
             @I_vCostCatID = case 
                               when @I_vCostCatID is null then ''
                               else @I_vCostCatID
                             end,
             @I_vCMMTTEXT = ''
    end
  
  select @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 (nolock)
  where  INTERID = db_name()
  
  select top 1 @LOCNCODEREG = LOCNCODE
  from   IV40700 (nolock)
  where  LOCNCODE <> ''
  
  if ((@LOCNCODEREG = '')
      and (@I_vLOCNCODE <> ''))
    begin
      select @O_iErrorState = 4605
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@LOCNCODEREG <> '')
      and (@I_vLOCNCODE <> ''))
    begin
      if ((@I_vPONUMBER <> '')
          and (not exists (select 1
                           from   IV40700 (nolock)
                           where  LOCNCODE = @I_vLOCNCODE)))
        begin
          select @O_iErrorState = 1277
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@I_vNONINVEN = 0)
          and (not exists (select 1
                           from   IV00102 (nolock)
                           where  ITEMNMBR = @I_vITEMNMBR
                                  and LOCNCODE = @I_vLOCNCODE)))
        begin
          select @O_iErrorState = 4604
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vNONINVEN = 0)
    begin
      if (@I_vPONUMBER = '')
        begin
          if (not exists (select 1
                          from   IV00103 (nolock)
                          where  ITEMNMBR = @I_vITEMNMBR
                                 and VNDITNUM = @I_vVNDITNUM
                                 and VENDORID = @I_vVENDORID))
            begin
              select @O_iErrorState = 1779
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      else
        begin
          if (@I_vPOLNENUM <> 0)
             and (not exists (select 1
                              from   POP10110 (nolock)
                              where  ITEMNMBR = @I_vITEMNMBR
                                     and VNDITNUM = @I_vVNDITNUM
                                     and VENDORID = @I_vVENDORID
                                     and PONUMBER = @I_vPONUMBER
                                     and ORD = @I_vPOLNENUM))
            begin
              select @O_iErrorState = 7922
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
    end
  
  if (@I_vPONUMBER = '')
    begin
      if (@I_vNONINVEN = 0)
        begin
          select @DECPLCUR = ((DECPLCUR - 1) + 7),
                 @ODECPLCU = (DECPLCUR - 1),
                 @DECPLQTY = ((DECPLQTY - 1) + 1)
          from   IV00101 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
          
          if (@I_vUOFM <> '')
            begin
              if not exists (select 1
                             from   IV00106 (nolock)
                             where  ITEMNMBR = @I_vITEMNMBR
                                    and UOFM = @I_vUOFM)
                begin
                  select @O_iErrorState = 9373
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
              
              select @UOFM = @I_vUOFM
            end
          else
            begin
              select @PRCHSUOM = PRCHSUOM
              from   IV00103 (nolock)
              where  VENDORID = @I_vVENDORID
                     and ITEMNMBR = @I_vITEMNMBR
                     and VNDITNUM = @I_vVNDITNUM
              
              if (@PRCHSUOM = '')
                begin
                  select @PRCHSUOM = PRCHSUOM
                  from   IV00101 (nolock)
                  where  ITEMNMBR = @I_vITEMNMBR
                end
              
              if (@PRCHSUOM = '')
                begin
                  select @O_iErrorState = 9374
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
              
              select @UOFM = @PRCHSUOM
            end
          
          select @UMQTYINB = QTYBSUOM
          from   IV00106 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and UOFM = @UOFM
          
          if (@I_vVNDITDSC = '')
            begin
              select @I_vVNDITDSC = VNDITDSC
              from   IV00103 (nolock)
              where  VENDORID = @I_vVENDORID
                     and ITEMNMBR = @I_vITEMNMBR
            end
        end
      else
        begin
          select @DECPLQTY = DECPLQTY,
                 @DECPLCUR = ((DECPLCUR - 1) + 7),
                 @ODECPLCU = (DECPLCUR - 1)
          from   POP40100 (nolock)
          where  INDEX1 = 1
          
          select @UOFM = @I_vUOFM
        end
      
      if (@I_vCURNCYID = '')
        begin
          select @I_vCURNCYID = isnull(CURNCYID,'')
          from   PM00200 (nolock)
          where  VENDORID = @I_vVENDORID
        end
      
      if (@I_vCURNCYID <> '')
        begin
          select @CURRNIDX = isnull(CURRNIDX,0)
          from   DYNAMICS..MC40200 (nolock)
          where  CURNCYID = @I_vCURNCYID
        end
      else
        begin
          select @I_vCURNCYID = isnull(FUNLCURR,''),
                 @CURRNIDX = isnull(FUNCRIDX,0)
          from   MC40000 (nolock)
        end
      
      if ((@I_vLOCNCODE = '')
          and (@LOCNCODEREG <> ''))
        begin
          select @O_iErrorState = 4601
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@I_vAUTOCOST = 1)
        begin
          select @ORUNTCST = Last_Originating_Cost
          from   IV00103 (nolock)
          where  VENDORID = @I_vVENDORID
                 and ITEMNMBR = @I_vITEMNMBR
                 and Last_Currency_ID = @I_vCURNCYID
        end
    end
  
  if (@POLNESTA in (4,5,6))
    begin
      select @O_iErrorState = 591
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@POTYPE = 2)
    begin
      select @O_iErrorState = 590
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@VENDORID <> @I_vVENDORID)
      and (@I_vPONUMBER <> ''))
    begin
      select @O_iErrorState = 3799
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @POPALWOP_2 = POPALWOP_2
  from   POP40100 (nolock)
  where  INDEX1 = 1
  
  if (@I_vJOBNUMBR = '')
    begin
      select @I_vJOBNUMBR = @JOBNUMBR
    end
  
  if (@I_vUOFM = '')
    begin
      select @I_vUOFM = @UOFM
    end
  
  if (@I_vPOLNENUM = 0)
    begin
      select @I_vPOLNENUM = @POLNENUM
    end
  
  if (@I_vLOCNCODE = '')
    begin
      select @I_vLOCNCODE = @LOCNCODE
    end
  else
    begin
      if ((@I_vLOCNCODE <> @LOCNCODE)
          and (@I_vPONUMBER <> ''))
        begin
          if (@POPALWOP_2 = 0)
            begin
              select @O_iErrorState = 1276
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
    end
  
  if ((@I_vPONUMBER <> '')
      and (@I_vUOFM <> ''))
    begin
      if (@I_vUOFM <> @UOFM)
        begin
          select @O_iErrorState = 4606
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (((@VNDITNUM = '')
        or (@VNDITNUM is null))
      and (@I_vPONUMBER <> ''))
    begin
      select @O_iErrorState = 2053
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vNONINVEN = 0)
      and (not exists (select 1
                       from   IV00101 (nolock)
                       where  ITEMNMBR = @I_vITEMNMBR)))
    begin
      select @O_iErrorState = 2054
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPONUMBER <> '')
    begin
      select @CURNCYID = isnull(CURNCYID,0)
      from   POP10100 (nolock)
      where  PONUMBER = @I_vPONUMBER
      
      select @CURRNIDX = isnull(CURRNIDX,0)
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @CURNCYID
      
      if ((@I_vCURNCYID <> '')
          and (@I_vCURNCYID <> @CURNCYID))
        begin
          select @O_iErrorState = 7320
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@I_vCURNCYID = '')
        begin
          select @I_vCURNCYID = @CURNCYID
        end
    end
  
  if ((@I_vCURNCYID <> '')
      and (@I_vCURNCYID <> @FUNLCURR))
    begin
      select @ISMCTRX = 1
    end
  
  if @I_vNONINVEN = 0
    begin
      select @I_vITEMDESC = ITEMDESC,
             @ITMTRKOP = ITMTRKOP,
             @ITEMTYPE = ITEMTYPE,
             @VCTNMTHD = VCTNMTHD,
             @INVINDX = IVIVINDX,
             @STNDCOST = STNDCOST,
             @PURPVIDX = PURPVIDX,
             @UPPVIDX = UPPVIDX,
             @Revalue_Inventory = Revalue_Inventory,
             @Tolerance_Percentage = Tolerance_Percentage,
             @PO_UOMSCHDL = UOMSCHDL,
             @PRICMTHD = PRICMTHD,
             @UOMSCHDL = UOMSCHDL,
             @CURRCOST = CURRCOST
      from   IV00101 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
      
      if (@VCTNMTHD not in (4,5))
        begin
          select @UPPVIDX = 0
        end
      else
        begin
          if (@UPPVIDX = 0)
            begin
              select @UPPVIDX = ACTINDX
              from   SY01100 (nolock)
              where  SERIES = 5
                     and SEQNUMBR = 1250
            end
        end
      
      if @I_vVNDITDSC = ''
        begin
          select @I_vVNDITDSC = @VNDITDSC
        end
      
      if @I_vINVINDX = 0
        begin
          select @I_vINVINDX = @INVINDX
          
          if @I_vINVINDX = 0
            begin
              select @I_vINVINDX = ACTINDX
              from   SY01100 (nolock)
              where  SERIES = 5
                     and SEQNUMBR = 100
            end
        end
    end
  else
    begin
      if @I_vITEMDESC = ''
        begin
          select @I_vITEMDESC = @ITEMDESC
        end
      
      if @I_vITEMDESC = ''
        begin
          select @I_vITEMDESC = @I_vVNDITDSC
        end
      
      if @I_vVNDITDSC = ''
        begin
          select @I_vVNDITDSC = @VNDITDSC
        end
      
      if @I_vINVINDX = 0
        begin
          select @I_vINVINDX = @INVINDX
          
          if (@I_vINVINDX = 0)
             and (@I_vCostCatID = '')
            begin
              select @I_vINVINDX = PMPRCHIX
              from   PM00200 (nolock)
              where  VENDORID = @I_vVENDORID
            end
        end
    end
  
  if (@ITEMTYPE = 3)
    begin
      select @O_iErrorState = 9337
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vInventoryAccount <> '')
    begin
      select @I_vINVINDX = 0
      
      select @I_vINVINDX = ACTINDX
      from   GL00105 (nolock)
      where  ACTNUMST = @I_vInventoryAccount
      
      if (@I_vINVINDX = 0)
        begin
          select @O_iErrorState = 446
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if not exists (select 1
                 from   GL00105 (nolock)
                 where  ACTINDX = @I_vINVINDX)
    begin
      select @O_iErrorState = 4612
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vUNITCOST < 0)
      or (@I_vUNITCOST is null 
          and @I_vAUTOCOST = 0)
    begin
      select @O_iErrorState = 8052
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@ISMCTRX = 0)
    begin
      if (exists (select distinct 1
                  from   IV00105 (nolock)
                  where  ITEMNMBR = @I_vITEMNMBR))
        begin
          if (not exists (select 1
                          from   IV00105 (nolock)
                          where  ITEMNMBR = @I_vITEMNMBR
                                 and CURNCYID <> ''))
            begin
              select @MCINSTALLED = 0
            end
        end
      else
        begin
          if ((not exists (select top 1 CURNCYID
                           from   IV00105 (nolock)
                           where  CURNCYID <> ''))
              and (not exists (select top 1 CURNCYID
                               from   CM00100 (nolock)
                               where  CURNCYID <> '')))
            begin
              select @MCINSTALLED = 0
            end
        end
    end
  
  if (@I_vNONINVEN = 0)
    begin
      if (@ISMCTRX = 1)
        begin
          select @DECPLCURItem = DECPLCUR - 1,
                 @ODECPLCU = DECPLCUR - 1,
                 @LISTPRCE = LISTPRCE
          from   IV00105 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and CURNCYID = @I_vCURNCYID
        end
      else
        begin
          select @DECPLCURItem = DECPLCUR - 1,
                 @ODECPLCU = DECPLCUR - 1,
                 @LISTPRCE = LISTPRCE
          from   IV00105 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and (CURNCYID = ''
                       or CURNCYID = @I_vCURNCYID)
        end
    end
  else
    begin
      if (@MCINSTALLED = 0)
        begin
          select @DECPLCURItem = DECPLCUR - 1
          from   POP40100 (nolock)
          where  INDEX1 = 1
        end
      else
        begin
          select @DECPLCURItem = DECPLCUR - 1
          from   POP40600 (nolock)
          where  CURNCYID = @I_vCURNCYID
        end
    end
  
  select @FUNDECPLCUR = DECPLCUR - 1
  from   MC40000 a (nolock),
         DYNAMICS..MC40200 b (nolock)
  where  a.FUNCRIDX = b.CURRNIDX
         and a.FUNLCURR = b.CURNCYID
  
  if (@ISMCTRX = 1)
    begin
      select @EDITDECPLCUR = DECPLCUR - 1
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @I_vCURNCYID
    end
  else
    begin
      select @EDITDECPLCUR = @FUNDECPLCUR
    end
  
  if (@I_vPONUMBER = '')
    begin
      select @ORUNTCST = round((@ORUNTCST * @UMQTYINB),@DECPLCURItem)
    end
  
  if (@I_vUNITCOST is null 
       or @I_vUNITCOST = 0)
     and (@I_vAUTOCOST = 1)
    begin
      select @I_vUNITCOST = @ORUNTCST
    end
  
  if (@I_vAUTOCOST = 0)
    begin
      if @I_vEXTDCOST is null
        begin
          select @I_vEXTDCOST = case 
                                  when @I_vPOPTYPE = 1 then round((@I_vUNITCOST) * @I_vQTYSHPPD,@EDITDECPLCUR)
                                  else round((@I_vUNITCOST) * @I_vQTYINVCD,@EDITDECPLCUR)
                                end
        end
      else
        begin
          select @PassedExtCost = 1
        end
      
      if ((@I_vQTYSHPPD = 0)
          and (@I_vPOPTYPE = 1))
        begin
          if (@I_vEXTDCOST > 0)
            begin
              select @O_iErrorState = 5446
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      
      if ((@I_vQTYINVCD = 0)
          and (@I_vPOPTYPE = 3))
        begin
          if (@I_vEXTDCOST > 0)
            begin
              select @O_iErrorState = 7993
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      
      if ((@I_vPOPTYPE = 1)
          and (@I_vQTYSHPPD <> 0))
        begin
          if ((round(abs((@I_vUNITCOST) - round((@I_vEXTDCOST / @I_vQTYSHPPD),@DECPLCURItem)) * @I_vQTYSHPPD,
                     @EDITDECPLCUR)) <> 0)
            begin
              if ((round(abs(round((@I_vUNITCOST) * @I_vQTYSHPPD,@EDITDECPLCUR) - @I_vEXTDCOST) * @I_vQTYSHPPD,
                         @EDITDECPLCUR)) <> 0)
                begin
                  select @O_iErrorState = 2057
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
        end
      
      if ((@I_vPOPTYPE = 3)
          and (@I_vQTYINVCD <> 0))
        begin
          if ((round(abs((@I_vUNITCOST) - round((@I_vEXTDCOST / @I_vQTYINVCD),@DECPLCURItem)) * @I_vQTYINVCD,
                     @EDITDECPLCUR)) <> 0)
            begin
              if ((round(abs(round((@I_vUNITCOST) * @I_vQTYINVCD,@EDITDECPLCUR) - @I_vEXTDCOST) * @I_vQTYINVCD,
                         @EDITDECPLCUR)) <> 0)
                begin
                  select @O_iErrorState = 8051
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
        end
    end
  
  if (@I_vRCPTLNNM = 0)
    begin
      select @I_vRCPTLNNM = isnull(max(RCPTLNNM),0) + 16384
      from   POP10310 (nolock)
      where  POPRCTNM = @I_vPOPRCTNM
    end
  
  if exists (select 1
             from   POP10310 (nolock)
             where  POPRCTNM = @I_vPOPRCTNM
                    and RCPTLNNM = @I_vRCPTLNNM)
    begin
      select @O_iErrorState = 2061
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vQTYINVCD > @I_vQTYSHPPD)
      and (@I_vPOPTYPE = 3))
    begin
      select @O_iErrorState = 2062
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vQTYINVCD > 0)
      and (@I_vPOPTYPE = 1))
    begin
      select @O_iErrorState = 734
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPurchase_IV_Item_Taxable < 1)
       or (@I_vPurchase_IV_Item_Taxable > 3))
    begin
      select @O_iErrorState = 2045
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if exists (select 1
             from   POP10360 (nolock)
             where  POPRCTNM = @I_vPOPRCTNM
                    and RCPTLNNM = @I_vRCPTLNNM)
    begin
      select @I_vPurchase_IV_Item_Taxable = 1
    end
  
  if (@I_vPurchase_IV_Item_Taxable <> 1)
    begin
      select @I_vPurchase_Item_Tax_Schedu = ''
    end
  
  if (@I_vPurchase_Item_Tax_Schedu <> '')
    begin
      if (not exists (select 1
                      from   TX00101 (nolock)
                      where  TAXSCHID = @I_vPurchase_Item_Tax_Schedu))
        begin
          select @O_iErrorState = 833
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPurchase_Site_Tax_Schedu <> '')
    begin
      if (not exists (select 1
                      from   TX00101 (nolock)
                      where  TAXSCHID = @I_vPurchase_Site_Tax_Schedu))
        begin
          select @O_iErrorState = 834
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vLanded_Cost_Group_ID <> '')
    begin
      if (not exists (select 1
                      from   IV41101 (nolock)
                      where  Landed_Cost_Group_ID = @I_vLanded_Cost_Group_ID))
        begin
          select @O_iErrorState = 837
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPONUMBER <> '')
    begin
      select @SHIPMTHD = isnull(SHIPMTHD,'')
      from   POP10100 (nolock)
      where  PONUMBER = @I_vPONUMBER
    end
  else
    begin
      select @VADCDPAD = isnull(VADCDPAD,'')
      from   PM00200 (nolock)
      where  VENDORID = @I_vVENDORID
      
      select @SHIPMTHD = isnull(SHIPMTHD,'')
      from   PM00300 (nolock)
      where  VENDORID = @I_vVENDORID
             and ADRSCODE = @VADCDPAD
    end
  
  if (@I_vNONINVEN = 1)
    begin
      select @PURPVIDX = isnull(PURPVIDX,0)
      from   PM00200 (nolock)
      where  VENDORID = @I_vVENDORID
      
      if (@PURPVIDX = 0)
        begin
          select @PURPVIDX = ACTINDX
          from   SY01100 (nolock)
          where  SERIES = 4
                 and SEQNUMBR = 1400
        end
    end
  else
    begin
      select @PURPVIDX = isnull(PURPVIDX,0)
      from   IV00101 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
      
      if (@PURPVIDX = 0)
        begin
          select @PURPVIDX = ACTINDX
          from   SY01100 (nolock)
          where  SERIES = 5
                 and SEQNUMBR = 1200
        end
    end
  
  if (@I_vNONINVEN = 1)
    begin
      if (exists (select ITEMNMBR
                  from   IV00101 (nolock)
                  where  ITEMNMBR = @I_vITEMNMBR))
        begin
          select @O_iErrorState = 8162
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (round(@I_vUNITCOST,@DECPLCURItem) <> (@I_vUNITCOST))
    begin
      select @O_iErrorState = 7321
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (round(@I_vEXTDCOST,@EDITDECPLCUR) <> (@I_vEXTDCOST))
    begin
      select @O_iErrorState = 7322
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @MAXSEG = max(SGMTNUMB)
  from   SY00300 (nolock)
  
  select @Location_Segment = isnull(Location_Segment,'')
  from   IV40700 (nolock)
  where  LOCNCODE = @I_vLOCNCODE
  
  select @ACSGFLOC = ACSGFLOC,
         @ENABLEMULTIBIN = ENABLEMULTIBIN
  from   IV40100 (nolock)
  where  SETUPKEY = 1
  
  if ((@I_vINVINDX <> 0)
      and (@Location_Segment <> '')
      and (@ACSGFLOC <> 0))
    begin
      select @ACCNT_STRING = ''
      
      select @ACCNT_STRING = ACTNUMST
      from   GL00105 (nolock)
      where  ACTINDX = @I_vINVINDX
      
      if (@ACSGFLOC < @MAXSEG)
        begin
          select @LOFSGMNT = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
          from   SY00300 (nolock)
          where  SGMTNUMB >= @ACSGFLOC
          
          select @LOFSGMNTEND = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
          from   SY00300 (nolock)
          where  SGMTNUMB > @ACSGFLOC
        end
      else
        begin
          select @LOFSGMNT = sum(LOFSGMNT)
          from   SY00300 (nolock)
          where  SGMTNUMB = @ACSGFLOC
        end
      
      select @LOFSGMNTALL = sum(LOFSGMNT) + (@MAXSEG - 1)
      from   SY00300 (nolock)
      
      if (@ACSGFLOC = @MAXSEG)
        begin
          select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment))
        end
      else
        begin
          select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment)) + substring(@ACCNT_STRING,@LOFSGMNTALL - @LOFSGMNTEND + 1,
                                                                                                                                       @LOFSGMNTALL)
        end
      
      select @ACTINDX = isnull(ACTINDX,0)
      from   GL00105 (nolock)
      where  ACTNUMST = @NEW_ACCNT_STRING
      
      if (@ACTINDX <> 0)
        begin
          select @I_vINVINDX = @ACTINDX
        end
    end
  
  select @RcptLineNoteIDArray_1 = NOTEINDX
  from   IV00101 (nolock)
  where  ITEMNMBR = @I_vITEMNMBR
  
  select @RcptLineNoteIDArray_2 = NOTEINDX
  from   IV40700 (nolock)
  where  LOCNCODE = @I_vLOCNCODE
  
  select @RcptLineNoteIDArray_3 = isnull(NOTEINDX,0)
  from   GL00100 (nolock)
  where  ACTINDX = @I_vINVINDX
  
  select @RcptLineNoteIDArray_4 = 0
  
  select @RcptLineNoteIDArray_5 = 0
  
  select @RcptLineNoteIDArray_6 = NOTEINDX
  from   TX00101 (nolock)
  where  TAXSCHID = @I_vPurchase_Item_Tax_Schedu
  
  select @RcptLineNoteIDArray_7 = NOTEINDX
  from   TX00101 (nolock)
  where  TAXSCHID = @I_vPurchase_Site_Tax_Schedu
  
  select @RcptLineNoteIDArray_8 = NOTEINDX
  from   IV41101 (nolock)
  where  Landed_Cost_Group_ID = @I_vLanded_Cost_Group_ID
  
  if (@O_iErrorState <> 0)
    begin
      return (@O_iErrorState)
    end
  
  while (1 = 1)
    begin
      if (@I_vPONUMBER <> '')
        begin
          update POP10100
          set    REMSUBTO = round(OREMSUBT - round((@ORUNTCST * @I_vQTYINVCD),@EDITDECPLCUR),
                                  @EDITDECPLCUR),
                 OREMSUBT = round(OREMSUBT - round((@ORUNTCST * @I_vQTYINVCD),@EDITDECPLCUR),
                                  @EDITDECPLCUR)
          where  PONUMBER = @I_vPONUMBER
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 9379
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      insert POP10310
            (POPRCTNM,
             RCPTLNNM,
             PONUMBER,
             ITEMNMBR,
             ITEMDESC,
             VNDITNUM,
             VNDITDSC,
             UMQTYINB,
             ACTLSHIP,
             COMMNTID,
             INVINDX,
             UOFM,
             UNITCOST,
             EXTDCOST,
             LOCNCODE,
             RcptLineNoteIDArray_1,
             RcptLineNoteIDArray_2,
             RcptLineNoteIDArray_3,
             RcptLineNoteIDArray_4,
             RcptLineNoteIDArray_5,
             RcptLineNoteIDArray_6,
             RcptLineNoteIDArray_7,
             RcptLineNoteIDArray_8,
             NONINVEN,
             DECPLCUR,
             DECPLQTY,
             ITMTRKOP,
             VCTNMTHD,
             TRXSORCE,
             JOBNUMBR,
             COSTCODE,
             COSTTYPE,
             CURNCYID,
             CURRNIDX,
             RATETPID,
             XCHGRATE,
             RATECALC,
             DENXRATE,
             ORUNTCST,
             OREXTCST,
             ODECPLCU,
             BOLPRONUMBER,
             Capital_Item,
             Product_Indicator,
             Purchase_IV_Item_Taxable,
             Purchase_Item_Tax_Schedu,
             Purchase_Site_Tax_Schedu,
             BSIVCTTL,
             TAXAMNT,
             ORTAXAMT,
             BCKTXAMT,
             OBTAXAMT,
             Revalue_Inventory,
             Tolerance_Percentage,
             PURPVIDX,
             Remaining_AP_Amount,
             SHIPMTHD,
             Landed_Cost_Group_ID,
             Landed_Cost_Warnings,
             BackoutTradeDiscTax,
             OrigBackoutTradeDiscTax,
             Landed_Cost,
             Invoice_Match,
             RCPTRETNUM,
             RCPTRETLNNUM,
             INVRETNUM,
             INVRETLNNUM,
             ISLINEINTRA,
             ProjNum,
             CostCatID)
      select @I_vPOPRCTNM,
             @I_vRCPTLNNM,
             @I_vPONUMBER,
             @I_vITEMNMBR,
             @I_vITEMDESC,
             @I_vVNDITNUM,
             @I_vVNDITDSC,
             @UMQTYINB,
             @I_vACTLSHIP,
             '',
             @I_vINVINDX,
             @I_vUOFM,
             case 
               when (@I_vAUTOCOST = 0) then @I_vUNITCOST
               else @ORUNTCST
             end,
             case 
               when (@I_vAUTOCOST = 0
                     and @I_vPOPTYPE = 1)
                     or (@PassedExtCost = 1) then @I_vEXTDCOST
               when (@I_vAUTOCOST = 1
                     and @I_vPOPTYPE = 1) then round(@ORUNTCST * @I_vQTYSHPPD,@EDITDECPLCUR)
               when (@I_vAUTOCOST = 0
                     and @I_vPOPTYPE = 3) then round(@I_vUNITCOST * @I_vQTYINVCD,@EDITDECPLCUR)
               when (@I_vAUTOCOST = 1
                     and @I_vPOPTYPE = 3) then round(@ORUNTCST * @I_vQTYINVCD,@EDITDECPLCUR)
               else 0
             end,
             @I_vLOCNCODE,
             @RcptLineNoteIDArray_1,
             @RcptLineNoteIDArray_2,
             @RcptLineNoteIDArray_3,
             @RcptLineNoteIDArray_4,
             @RcptLineNoteIDArray_5,
             @RcptLineNoteIDArray_6,
             @RcptLineNoteIDArray_7,
             @RcptLineNoteIDArray_8,
             @I_vNONINVEN,
             @DECPLCUR,
             case 
               when (@I_vNONINVEN = 1)
                    and (@PROJECTLOADED = 1) then @PADECPLQTY
               else @DECPLQTY
             end,
             @ITMTRKOP,
             @VCTNMTHD,
             '',
             @I_vJOBNUMBR,
             '',
             0,
             case 
               when @MCINSTALLED = 1 then @I_vCURNCYID
               else ''
             end,
             case 
               when @MCINSTALLED = 1 then @CURRNIDX
               else 0
             end,
             '',
             0,
             0,
             0,
             case 
               when (@I_vAUTOCOST = 0) then @I_vUNITCOST
               else @ORUNTCST
             end,
             case 
               when (@I_vAUTOCOST = 0
                     and @I_vPOPTYPE = 1)
                     or (@PassedExtCost = 1) then @I_vEXTDCOST
               when (@I_vAUTOCOST = 1
                     and @I_vPOPTYPE = 1) then round(@ORUNTCST * @I_vQTYSHPPD,@EDITDECPLCUR)
               when (@I_vAUTOCOST = 0
                     and @I_vPOPTYPE = 3) then round(@I_vUNITCOST * @I_vQTYINVCD,@EDITDECPLCUR)
               when (@I_vAUTOCOST = 1
                     and @I_vPOPTYPE = 3) then round(@ORUNTCST * @I_vQTYINVCD,@EDITDECPLCUR)
               else 0
             end,
             @ODECPLCU,
             @I_vBOLPRONUMBER,
             @Capital_Item,
             0,
             @I_vPurchase_IV_Item_Taxable,
             @I_vPurchase_Item_Tax_Schedu,
             @I_vPurchase_Site_Tax_Schedu,
             0,
             @I_vTAXAMNT,
             @I_vTAXAMNT,
             0,
             0,
             @Revalue_Inventory,
             @Tolerance_Percentage,
             @PURPVIDX,
             case 
               when (@I_vAUTOCOST = 0) then @I_vEXTDCOST
               when (@I_vAUTOCOST = 1
                     and @I_vPOPTYPE = 1) then round(@ORUNTCST * @I_vQTYSHPPD,@EDITDECPLCUR)
               when (@I_vAUTOCOST = 1
                     and @I_vPOPTYPE = 3) then round(@ORUNTCST * @I_vQTYINVCD,@EDITDECPLCUR)
               else 0
             end,
             @SHIPMTHD,
             @I_vLanded_Cost_Group_ID,
             0,
             0,
             0,
             0,
             0,
             '',
             0,
             '',
             0,
             0,
             @I_vProjNum,
             @I_vCostCatID
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 2075
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          break
        end
      
      insert POP10500
            (PONUMBER,
             POLNENUM,
             POPRCTNM,
             RCPTLNNM,
             QTYSHPPD,
             QTYINVCD,
             QTYREJ,
             QTYMATCH,
             QTYRESERVED,
             QTYINVRESERVE,
             Status,
             UMQTYINB,
             OLDCUCST,
             JOBNUMBR,
             COSTCODE,
             COSTTYPE,
             ORCPTCOST,
             OSTDCOST,
             APPYTYPE,
             POPTYPE,
             VENDORID,
             ITEMNMBR,
             UOFM,
             TRXLOCTN,
             DATERECD,
             RCTSEQNM,
             SPRCTSEQ,
             PCHRPTCT,
             SPRCPTCT,
             OREXTCST,
             RUPPVAMT,
             ACPURIDX,
             INVINDX,
             UPPVIDX,
             NOTEINDX,
             CURNCYID,
             CURRNIDX,
             XCHGRATE,
             RATECALC,
             DENXRATE,
             RATETPID,
             EXGTBLID,
             Capital_Item,
             Product_Indicator,
             Total_Landed_Cost_Amount,
             QTYTYPE,
             Posted_LC_PPV_Amount)
      select @I_vPONUMBER,
             case 
               when (@I_vPONUMBER = '') then 0
               else @I_vPOLNENUM
             end,
             @I_vPOPRCTNM,
             @I_vRCPTLNNM,
             @I_vQTYSHPPD,
             @I_vQTYINVCD,
             0,
             0,
             0,
             0,
             0,
             @UMQTYINB,
             @OLDCUCST,
             @JOBNUMBR,
             '',
             0,
             case 
               when (@I_vAUTOCOST = 0) then @I_vUNITCOST
               else @ORUNTCST
             end,
             @STNDCOST,
             1,
             @I_vPOPTYPE,
             @I_vVENDORID,
             @I_vITEMNMBR,
             @UOFM,
             @I_vLOCNCODE,
             '',
             0,
             0,
             0,
             0,
             case 
               when (@I_vAUTOCOST = 0) then round(@I_vUNITCOST * (@I_vQTYSHPPD - @I_vQTYINVCD),
                                                  @EDITDECPLCUR)
               when (@I_vAUTOCOST = 1) then round(@ORUNTCST * (@I_vQTYSHPPD - @I_vQTYINVCD),
                                                  @EDITDECPLCUR)
               else 0
             end,
             @RUPPVAMT,
             @ACPURIDX,
             @I_vINVINDX,
             @UPPVIDX,
             0,
             @I_vCURNCYID,
             @CURRNIDX,
             0,
             0,
             0,
             '',
             '',
             @Capital_Item,
             0,
             0,
             1,
             0
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 2076
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          break
        end
      
      if (@ITMTRKOP in (2,3))
        begin
          select @SERLTQTY = isnull(sum(SERLTQTY),0)
          from   POP10330 (nolock)
          where  POPRCTNM = @I_vPOPRCTNM
                 and RCPTLNNM = @I_vRCPTLNNM
          
          if ((@I_vQTYSHPPD * @UMQTYINB) <> @SERLTQTY)
            begin
              select @O_iErrorState = 3389
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      
      select @I_vEXTDCOST = case 
                              when (@I_vAUTOCOST = 0
                                    and @I_vPOPTYPE = 1) then @I_vEXTDCOST
                              when (@I_vAUTOCOST = 1
                                    and @I_vPOPTYPE = 1) then round(@ORUNTCST * @I_vQTYSHPPD,@EDITDECPLCUR)
                              when (@I_vAUTOCOST = 0
                                    and @I_vPOPTYPE = 3) then round(@I_vUNITCOST * @I_vQTYINVCD,@EDITDECPLCUR)
                              when (@I_vAUTOCOST = 1
                                    and @I_vPOPTYPE = 3) then round(@ORUNTCST * @I_vQTYINVCD,@EDITDECPLCUR)
                              else 0
                            end
      
      if (@I_vPONUMBER <> '')
        begin
          if (@I_vCMMTTEXT <> '')
            begin
              if (exists (select NOTEINDX
                          from   SY03900 (nolock)
                          where  NOTEINDX = @POLNEARY_3))
                begin
                  update SY03900
                  set    TXTFIELD = @I_vCMMTTEXT,
                         DATE1 = convert(varchar(12),getdate()),
                         TIME1 = substring(convert(varchar(25),getdate()),12,12)
                  where  NOTEINDX = @POLNEARY_3
                  
                  if (@@error <> 0)
                    begin
                      select @O_iErrorState = 8272
                      
                      exec @iStatus = RmsUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      break
                    end
                end
              else
                begin
                  insert SY03900
                        (NOTEINDX,
                         DATE1,
                         TIME1,
                         TXTFIELD)
                  select @POLNEARY_3,
                         convert(varchar(12),getdate()),
                         substring(convert(varchar(25),getdate()),12,12),
                         @I_vCMMTTEXT
                  
                  if (@@error <> 0)
                    begin
                      select @O_iErrorState = 8271
                      
                      exec @iStatus = RmsUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      break
                    end
                end
            end
          else
            begin
              if (@I_vCMMTTEXT = '')
                begin
                  delete SY03900
                  where  NOTEINDX = @POLNEARY_3
                  
                  if (@@error <> 0)
                    begin
                      select @O_iErrorState = 8273
                      
                      exec @iStatus = RmsUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      break
                    end
                end
            end
        end
      
      break
    end
  
  return (@O_iErrorState)

