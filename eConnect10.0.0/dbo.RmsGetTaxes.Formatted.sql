

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetTaxes]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetTaxes]
  
create procedure [dbo].[RmsGetTaxes]
                @iTaxTempTable  VARCHAR(255),
                @iRmsServerName VARCHAR(255),
                @iHqDbName      VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lTaxTable VARCHAR(255)
  
  SELECT @lTaxTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Tax'
  
  EXEC( 'INSERT INTO ' + @iTaxTempTable + ' SELECT  [ID],  Code,  [Description]  FROM ' + @lTaxTable)

