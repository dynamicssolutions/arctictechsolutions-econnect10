

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taUpdateCreatePAVendorRcd]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taUpdateCreatePAVendorRcd]
  
create procedure [dbo].[taUpdateCreatePAVendorRcd]
                @I_vVENDORID             char(15),
                @I_vVNDCLSID             char(10)  = '',
                @I_vPAddlDefpoformatouse smallint  = 0,
                @I_vPAUnit_of_Measure    char(9)  = '',
                @I_vPAUNITCOST           numeric(19,5)  = 0,
                @I_vPATMProfitType       smallint  = 0,
                @I_vPATMProfitAmount     numeric(19,5)  = 0,
                @I_vPATMProfitPercent    numeric(19,5)  = 0,
                @I_vPAFFProfitType       smallint  = 0,
                @I_vPAFFProfitAmount     numeric(19,5)  = 0,
                @I_vPAFFProfitPercent    numeric(19,5)  = 0,
                @I_vPAProfit_Type__CP    smallint  = 0,
                @I_vPAProfitAmountCP     numeric(19,5)  = 0,
                @I_vPAProfitPercentCP    numeric(19,5)  = 0,
                @I_vPAUD1                char(21)  = '',
                @I_vPAUD2                char(21)  = '',
                @I_vUpdateIfExists       tinyint  = 1,
                @O_iErrorState           int  output,
                @oErrString              varchar(255)  output   /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @CurrentVNDCLSID      char(11),
           @iStatus              int,
           @iAddCodeErrState     int,
           @iCustomState         int,
           @iCustomErrString     varchar(255),
           @O_oErrorState        int,
           @iError               int,
           @PAddlDefpoformatouse smallint,
           @PAUnit_of_Measure    char(9),
           @PAUNITCOST           numeric(19,5),
           @PATMProfitType       smallint,
           @PATMProfitAmount     numeric(19,5),
           @PATMProfitPercent    numeric(19,5),
           @PAFFProfitType       smallint,
           @PAFFProfitAmount     numeric(19,5),
           @PAFFProfitPercent    numeric(19,5),
           @PAProfit_Type__CP    smallint,
           @PAProfitAmountCP     numeric(19,5),
           @PAProfitPercentCP    numeric(19,5),
           @PAUD1                char(21),
           @PAUD2                char(21),
           @RecordExists         tinyint,
           @VendorClassExists    tinyint
  
  select @O_iErrorState = 0,
         @iStatus = 0,
         @CurrentVNDCLSID = '',
         @iAddCodeErrState = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @O_oErrorState = 0,
         @iError = 0,
         @PAddlDefpoformatouse = 0,
         @PAUnit_of_Measure = '',
         @PAUNITCOST = 0,
         @PATMProfitType = 0,
         @PATMProfitAmount = 0,
         @PATMProfitPercent = 0,
         @PAFFProfitType = 0,
         @PAFFProfitAmount = 0,
         @PAFFProfitPercent = 0,
         @PAProfit_Type__CP = 0,
         @PAProfitAmountCP = 0,
         @PAProfitPercentCP = 0,
         @PAUD1 = '',
         @PAUD2 = '',
         @RecordExists = 0,
         @VendorClassExists = 1
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  if (@I_vVENDORID is NULL)
    begin
      select @O_iErrorState = 2701
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
      
      return (@O_iErrorState)
    end
  
  select @I_vVENDORID = UPPER(@I_vVENDORID)
  
  if (@I_vVENDORID = '')
    begin
      select @O_iErrorState = 2702
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
      
      return (@O_iErrorState)
    end
  
  if (not exists (select 1
                  from   PM00200 (nolock)
                  where  VENDORID = @I_vVENDORID))
    begin
      select @O_iErrorState = 2703
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vUpdateIfExists < 0
       or @I_vUpdateIfExists > 1)
    begin
      select @O_iErrorState = 3718
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if @I_vVNDCLSID <> ''
    begin
      if (not exists (select 1
                      from   PM00100 (nolock)
                      where  VNDCLSID = @I_vVNDCLSID))
        begin
          select @O_iErrorState = 2704
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
        end
    end
  else
    begin
      if @I_vVNDCLSID = ''
        select @I_vVNDCLSID = VNDCLSID
        from   PM00100 (nolock)
        where  DEFLTCLS = 1
    end
  
  if (@I_vVNDCLSID = '')
    begin
      select @O_iErrorState = 2705
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  select @CurrentVNDCLSID = VNDCLSID
  from   PM00200 (nolock)
  where  VENDORID = @I_vVENDORID
  
  if (exists (select 1
              from   PA00901 (nolock)
              where  VENDORID = @I_vVENDORID))
    select @RecordExists = 1
  
  if (exists (select 1
              from   PA40901 (nolock)
              where  VNDCLSID = @I_vVNDCLSID))
    select @VendorClassExists = 1
  
  if (((@RecordExists = 0)
       and exists (select 1
                   from   PA40901 (nolock)
                   where  VNDCLSID = @I_vVNDCLSID))
       or ((@RecordExists = 1)
           and (@VendorClassExists = 1)
           and (@CurrentVNDCLSID <> @I_vVNDCLSID)))
    begin
      select @I_vPAddlDefpoformatouse = case 
                                          WHEN @I_vPAddlDefpoformatouse = '' THEN PAddlDefpoformatouse
                                          ELSE @I_vPAddlDefpoformatouse
                                        end,
             @I_vPAUnit_of_Measure = case 
                                       WHEN @I_vPAUnit_of_Measure = '' THEN PAUnit_of_Measure
                                       ELSE @I_vPAUnit_of_Measure
                                     end,
             @I_vPAUNITCOST = case 
                                WHEN @I_vPAUNITCOST = 0 THEN PAUNITCOST
                                ELSE @I_vPAUNITCOST
                              end,
             @I_vPATMProfitType = case 
                                    WHEN @I_vPATMProfitType = 0 THEN PATMProfitType
                                    ELSE @I_vPATMProfitType
                                  end,
             @I_vPATMProfitAmount = case 
                                      WHEN @I_vPATMProfitAmount = 0 THEN PATMProfitAmount
                                      ELSE @I_vPATMProfitAmount
                                    end,
             @I_vPATMProfitPercent = case 
                                       WHEN @I_vPATMProfitPercent = 0 THEN PATMProfitPercent
                                       ELSE @I_vPATMProfitPercent
                                     end,
             @I_vPAFFProfitType = case 
                                    WHEN @I_vPAFFProfitType = 0 THEN PAFFProfitType
                                    ELSE @I_vPAFFProfitType
                                  end,
             @I_vPAFFProfitAmount = case 
                                      WHEN @I_vPAFFProfitAmount = 0 THEN PAFFProfitAmount
                                      ELSE @I_vPAFFProfitAmount
                                    end,
             @I_vPAFFProfitPercent = case 
                                       WHEN @I_vPAFFProfitPercent = 0 THEN PAFFProfitPercent
                                       ELSE @I_vPAFFProfitPercent
                                     end,
             @I_vPAProfit_Type__CP = case 
                                       WHEN @I_vPAProfit_Type__CP = 0 THEN PAProfit_Type__CP
                                       ELSE @I_vPAProfit_Type__CP
                                     end,
             @I_vPAProfitAmountCP = case 
                                      WHEN @I_vPAProfitAmountCP = 0 THEN PAProfitAmountCP
                                      ELSE @I_vPAProfitAmountCP
                                    end,
             @I_vPAProfitPercentCP = case 
                                       WHEN @I_vPAProfitPercentCP = 0 THEN PAProfitPercentCP
                                       ELSE @I_vPAProfitPercentCP
                                     end
      from   PA40901 (nolock)
      where  VNDCLSID = @I_vVNDCLSID
    end
  
  if (@I_vPATMProfitType = 0)
    select @I_vPATMProfitType = 1
  
  if (@I_vPAFFProfitType = 0)
    select @I_vPAFFProfitType = 3
  
  if (@I_vPAProfit_Type__CP = 0)
    select @I_vPAProfit_Type__CP = 3
  
  if (@I_vPAUNITCOST < 0)
    begin
      select @O_iErrorState = 2706
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPATMProfitType <> 1
      and @I_vPATMProfitType <> 2
      and @I_vPATMProfitType <> 8)
    begin
      select @O_iErrorState = 2707
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPATMProfitAmount < 0)
    begin
      select @O_iErrorState = 2708
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if ((@I_vPATMProfitAmount > 0)
      and ((@I_vPATMProfitType = 2)
            or (@I_vPATMProfitType = 8)))
    begin
      select @O_iErrorState = 2709
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPATMProfitPercent < 0)
    begin
      select @O_iErrorState = 2710
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if ((@I_vPATMProfitPercent > 0)
      and ((@I_vPATMProfitType = 1)
            or (@I_vPATMProfitType = 8)))
    begin
      select @O_iErrorState = 2711
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPAFFProfitType <> 3
      and @I_vPAFFProfitType <> 5
      and @I_vPAFFProfitType <> 6
      and @I_vPAFFProfitType <> 8)
    begin
      select @O_iErrorState = 2712
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPAFFProfitAmount < 0)
    begin
      select @O_iErrorState = 2713
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if ((@I_vPAFFProfitAmount > 0)
      and (@I_vPAFFProfitType = 6
            or @I_vPAFFProfitType = 8))
    begin
      select @O_iErrorState = 2714
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPAFFProfitPercent < 0)
    begin
      select @O_iErrorState = 2715
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPAFFProfitPercent > 0)
     and ((@I_vPAFFProfitType <> 6))
    begin
      select @O_iErrorState = 2718
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPAProfit_Type__CP < 3)
      or (@I_vPAProfit_Type__CP > 8)
    begin
      select @O_iErrorState = 2719
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPAProfitAmountCP < 0)
    begin
      select @O_iErrorState = 2720
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPAProfitAmountCP > 0)
     and ((@I_vPAProfit_Type__CP = 6)
           or (@I_vPAProfit_Type__CP = 7)
           or (@I_vPAProfit_Type__CP = 8))
    begin
      select @O_iErrorState = 2721
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPAProfitPercentCP < 0)
    begin
      select @O_iErrorState = 2722
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPAProfitPercentCP > 0)
     and ((@I_vPAProfit_Type__CP <> 6)
           or (@I_vPAProfit_Type__CP <> 7))
    begin
      select @O_iErrorState = 2723
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vPAddlDefpoformatouse < 1
       or @I_vPAddlDefpoformatouse > 3)
    begin
      select @O_iErrorState = 2724
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  if (@RecordExists = 0)
    begin
      insert into PA00901
                 (VENDORID,
                  PAfromemployee,
                  PAddlDefpoformatouse,
                  PAUnit_of_Measure,
                  PAUNITCOST,
                  PATMProfitType,
                  PATMProfitAmount,
                  PATMProfitPercent,
                  PAFFProfitType,
                  PAProfit_Type__CP,
                  PAProfitAmountCP,
                  PAProfitPercentCP,
                  PAFFProfitAmount,
                  PAFFProfitPercent,
                  PAUD1,
                  PAUD2,
                  PA_Allow_Vendor_For_PO)
      select @I_vVENDORID,
             0,
             case 
               WHEN @I_vPAddlDefpoformatouse = 0 THEN @PAddlDefpoformatouse
               ELSE @I_vPAddlDefpoformatouse
             end,
             case 
               WHEN @I_vPAUnit_of_Measure = '' THEN @PAUnit_of_Measure
               ELSE @I_vPAUnit_of_Measure
             end,
             case 
               WHEN @I_vPAUNITCOST = 0 THEN @PAUNITCOST
               ELSE @I_vPAUNITCOST
             end,
             case 
               WHEN @I_vPATMProfitType = 0 THEN @PATMProfitType
               ELSE @I_vPATMProfitType
             end,
             case 
               WHEN @I_vPATMProfitAmount = 0 THEN @PATMProfitAmount
               ELSE @I_vPATMProfitAmount
             end,
             case 
               WHEN @I_vPATMProfitPercent = 0 THEN @PATMProfitPercent
               ELSE @I_vPATMProfitPercent
             end,
             case 
               WHEN @I_vPAFFProfitType = 0 THEN @PAFFProfitType
               ELSE @I_vPAFFProfitType
             end,
             case 
               WHEN @I_vPAProfit_Type__CP = 0 THEN @PAProfit_Type__CP
               ELSE @I_vPAProfit_Type__CP
             end,
             case 
               WHEN @I_vPAProfitAmountCP = 0 THEN @PAProfitAmountCP
               ELSE @I_vPAProfitAmountCP
             end,
             case 
               WHEN @I_vPAProfitPercentCP = 0 THEN @PAProfitPercentCP
               ELSE @I_vPAProfitPercentCP
             end,
             case 
               WHEN @I_vPAFFProfitAmount = 0 THEN @PAFFProfitAmount
               ELSE @I_vPAFFProfitAmount
             end,
             case 
               WHEN @I_vPAFFProfitPercent = 0 THEN @PAFFProfitPercent
               ELSE @I_vPAFFProfitPercent
             end,
             @I_vPAUD1,
             @I_vPAUD2,
             0
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 2725
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
          
          return (@O_iErrorState)
        end
      
      insert into PA43001
                 (PAsfid,
                  PArecordid,
                  PAcosttrxid,
                  PAaccttype,
                  PAACTINDX)
      select 25,
             @I_vVENDORID,
             PAcosttrxid,
             PAaccttype,
             PAACTINDX
      from   PA43001 (nolock)
      where  PArecordid = @I_vVNDCLSID
             and PAsfid = 15
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 2726
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      if (@I_vUpdateIfExists > 0)
        begin
          update PA00901
          set    VENDORID = @I_vVENDORID,
                 PAfromemployee = PAfromemployee,
                 PAddlDefpoformatouse = @I_vPAddlDefpoformatouse,
                 PAUnit_of_Measure = @I_vPAUnit_of_Measure,
                 PAUNITCOST = @I_vPAUNITCOST,
                 PATMProfitType = @I_vPATMProfitType,
                 PATMProfitAmount = @I_vPATMProfitAmount,
                 PATMProfitPercent = @I_vPATMProfitPercent,
                 PAFFProfitType = @I_vPAFFProfitType,
                 PAProfit_Type__CP = @I_vPAProfit_Type__CP,
                 PAProfitAmountCP = @I_vPAProfitAmountCP,
                 PAProfitPercentCP = @I_vPAProfitPercentCP,
                 PAFFProfitAmount = @I_vPAFFProfitAmount,
                 PAFFProfitPercent = @I_vPAFFProfitPercent,
                 PAUD1 = @I_vPAUD1,
                 PAUD2 = @I_vPAUD2,
                 PA_Allow_Vendor_For_PO = PA_Allow_Vendor_For_PO
          where  VENDORID = @I_vVENDORID
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 2727
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @iAddCodeErrState output
              
              return (@O_iErrorState)
            end
          
          if (@CurrentVNDCLSID <> @I_vVNDCLSID)
            begin
              delete PA43001
              where  PArecordid = @I_vVNDCLSID
                     and PAsfid = 25
              
              insert into PA43001
                         (PAsfid,
                          PArecordid,
                          PAcosttrxid,
                          PAaccttype,
                          PAACTINDX)
              select 25,
                     @I_vVENDORID,
                     PAcosttrxid,
                     PAaccttype,
                     PAACTINDX
              from   PA43001 (nolock)
              where  PArecordid = @I_vVNDCLSID
                     and PAsfid = 15
            end
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 2728
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @iAddCodeErrState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  return (@O_iErrorState)

