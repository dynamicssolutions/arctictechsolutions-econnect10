

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetPOReceipts]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetPOReceipts]
  
create procedure [dbo].[RmsGetPOReceipts]
                @iPOReceiptsTempTable VARCHAR(255),
                @iFromDate            DateTime,
                @iToDate              DateTime,
                @iRmsServerName       VARCHAR(255),
                @iHqDbName            VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lInventoryTransferLogTable VARCHAR(255)
  
  DECLARE  @lItemTable VARCHAR(255)
  
  DECLARE  @lPurchaseOrderTable VARCHAR(255)
  
  DECLARE  @lPurchaseOrderEntryTable VARCHAR(255)
  
  DECLARE  @lSupplierTable VARCHAR(255)
  
  SELECT @lInventoryTransferLogTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.InventoryTransferLog as InventoryTransferLog'
  
  SELECT @lItemTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Item as Item'
  
  SELECT @lPurchaseOrderTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.PurchaseOrder as PurchaseOrder'
  
  SELECT @lPurchaseOrderEntryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.PurchaseOrderEntry as PurchaseOrderEntry'
  
  SELECT @lSupplierTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Supplier as Supplier'
  
  EXEC( 'INSERT INTO ' + @iPOReceiptsTempTable + '(RmsTransferLogID,  RmsStoreID,  ITEMNMBR,  QTYRECVD,  COSTAMNT,  RmsPOID,  RmsPONumber,  DATE1,  VENDORID,  RmsCurrencyID,  XCHGRATE)  SELECT  InventoryTransferLog.[ID],  InventoryTransferLog.StoreID,  ISNULL(UPPER(Item.ItemLookupCode),0),  InventoryTransferLog.Quantity,  InventoryTransferLog.Cost * PurchaseOrder.ExchangeRate,  InventoryTransferLog.ReferenceID,  PurchaseOrder.PONumber,  CONVERT(datetime,CONVERT(VARCHAR,InventoryTransferLog.DateTransferred,111)),  ISNULL(LEFT(UPPER(Supplier.Code), 15),''''),  PurchaseOrder.CurrencyID,  PurchaseOrder.ExchangeRate  FROM ' + @lItemTable + ' RIGHT OUTER JOIN ' + @lInventoryTransferLogTable + ' ON ' + ' Item.[ID] = InventoryTransferLog.ItemID JOIN ' + @lPurchaseOrderTable + ' ON ' + ' InventoryTransferLog.ReferenceID = PurchaseOrder.[ID]  AND  InventoryTransferLog.StoreID = PurchaseOrder.StoreID LEFT OUTER JOIN ' + @lSupplierTable + ' ON ' + ' PurchaseOrder.SupplierID = Supplier.[ID]  WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,InventoryTransferLog.DateTransferred,111)) BETWEEN ' + '''' + @iFromDate + '''' + ' AND ' + '''' + @iToDate + '''' + ' AND   InventoryTransferLog.Type = 1  AND  PurchaseOrder.POType in (0,1)  AND  NOT EXISTS(SELECT  1  FROM  RMS30600  WHERE  InventoryTransferLog.StoreID = RMS30600.RmsStoreID  AND  InventoryTransferLog.[ID] = RMS30600.RmsTransferLogID)')

