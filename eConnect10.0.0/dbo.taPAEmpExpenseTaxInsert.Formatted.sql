

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAEmpExpenseTaxInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAEmpExpenseTaxInsert]
  
create procedure [dbo].[taPAEmpExpenseTaxInsert]
                @I_vPAertrxtype      smallint,
                @I_vPAerdocnumber    char(17),
                @I_vLNITMSEQ         int,
                @I_vTAXDTLID         char(15),
                @I_vTAXTYPE          smallint,
                @I_vEMPLOYID         char(15),
                @I_vTAXAMNT          numeric(19,5)  = 0,
                @I_vPAREIMBURSTAXAMT numeric(19,5)  = 0,
                @I_vBKOUTTAX         tinyint  = 0,
                @I_vPCTAXAMT         numeric(19,5)  = 0,
                @I_vFRTTXAMT         numeric(19,5)  = 0,
                @I_vMSCTXAMT         numeric(19,5)  = 0,
                @I_vTXDTTPUR         numeric(19,5)  = 0,
                @I_vTDTTXPUR         numeric(19,5)  = 0,
                @I_vACTINDX          int  = 0,
                @I_vRequesterTrx     smallint  = 0,
                @I_vUSRDEFND1        char(50)  = '',
                @I_vUSRDEFND2        char(50)  = '',
                @I_vUSRDEFND3        char(50)  = '',
                @I_vUSRDEFND4        varchar(8000)  = '',
                @I_vUSRDEFND5        varchar(8000)  = '',
                @O_iErrorState       int  output,
                @oErrString          varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @CURRNIDX         int,
           @iStatus          int,
           @iCustomState     int,
           @iCustomErrString varchar(255),
           @iError           int,
           @O_oErrorState    int,
           @ORTAXAMT         numeric(19,5),
           @PAORIGREIMTAXAMT numeric(19,5),
           @ORPURTAX         numeric(19,5),
           @ORFRTTAX         numeric(19,5),
           @ORMSCTAX         numeric(19,5),
           @ORTOTPUR         numeric(19,5),
           @ORTXBPUR         numeric(19,5),
           @l_LNITMSEQ       int,
           @iAddCodeErrState int
  
  select @CURRNIDX = 0,
         @O_oErrorState = 0,
         @O_iErrorState = 0,
         @iStatus = 0,
         @ORTAXAMT = 0,
         @PAORIGREIMTAXAMT = 0,
         @ORPURTAX = 0,
         @ORFRTTAX = 0,
         @ORMSCTAX = 0,
         @ORTOTPUR = 0,
         @ORTXBPUR = 0,
         @l_LNITMSEQ = 0,
         @iAddCodeErrState = 0,
         @iCustomState = 0,
         @iCustomErrString = ''
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taPAEmpExpenseTaxInsertPre
    @I_vPAertrxtype output ,
    @I_vPAerdocnumber output ,
    @I_vLNITMSEQ output ,
    @I_vTAXDTLID output ,
    @I_vTAXTYPE output ,
    @I_vEMPLOYID output ,
    @I_vTAXAMNT output ,
    @I_vPAREIMBURSTAXAMT output ,
    @I_vBKOUTTAX output ,
    @I_vPCTAXAMT output ,
    @I_vFRTTXAMT output ,
    @I_vMSCTXAMT output ,
    @I_vTXDTTPUR output ,
    @I_vTDTTXPUR output ,
    @I_vACTINDX output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1816
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAertrxtype is NULL 
       or @I_vPAerdocnumber is NULL
       or @I_vLNITMSEQ is NULL
       or @I_vTAXDTLID is NULL
       or @I_vACTINDX is NULL
       or @I_vTAXAMNT is NULL
       or @I_vPAREIMBURSTAXAMT is NULL
       or @I_vTXDTTPUR is NULL
       or @I_vTDTTXPUR is NULL
       or @I_vFRTTXAMT is NULL
       or @I_vMSCTXAMT is NULL
       or @I_vPCTAXAMT is NULL)
    begin
      select @O_iErrorState = 1817
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAerdocnumber = ''
      and @I_vTAXDTLID = '')
    begin
      select @O_iErrorState = 1818
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vEMPLOYID = UPPER(@I_vEMPLOYID),
         @I_vPAerdocnumber = UPPER(@I_vPAerdocnumber),
         @I_vTAXDTLID = UPPER(@I_vTAXDTLID)
  
  select @CURRNIDX = isnull(FUNCRIDX,0)
  from   MC40000 (nolock)
  
  if not exists (select 1
                 from   TX00201 (nolock)
                 where  TAXDTLID = @I_vTAXDTLID)
    begin
      select @O_iErrorState = 1819
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if not exists (select 1
                 from   UPR00100 (nolock)
                 where  EMPLOYID = @I_vEMPLOYID)
    begin
      select @O_iErrorState = 8691
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vACTINDX <> 0
    begin
      select @I_vACTINDX = isnull(ACTINDX,0)
      from   GL00105 (nolock)
      where  ACTINDX = @I_vACTINDX
      
      if (@I_vACTINDX = 0)
        begin
          select @O_iErrorState = 1820
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      select @l_LNITMSEQ = @I_vLNITMSEQ
      
      if (@l_LNITMSEQ = 0)
        select @l_LNITMSEQ = isnull(min(LNITMSEQ),0)
        from   PA10501 (nolock)
        where  PAerdocnumber = @I_vPAerdocnumber
      
      select @I_vACTINDX = isnull(PACGBWIPIDX,0)
      from   PA10501 (nolock)
      where  PAerdocnumber = @I_vPAerdocnumber
             and LNITMSEQ = @l_LNITMSEQ
      
      if @I_vACTINDX = 0
        begin
          select @I_vACTINDX = isnull(PACogs_Idx,0)
          from   PA10501 (nolock)
          where  PAerdocnumber = @I_vPAerdocnumber
                 and LNITMSEQ = @l_LNITMSEQ
          
          if @I_vACTINDX = 0
            begin
              select @O_iErrorState = 1821
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if @I_vTAXTYPE = 0
    begin
      if exists (select 1
                 from   PA10502 (nolock)
                 where  PAerdocnumber = @I_vPAerdocnumber
                        and LNITMSEQ = @I_vLNITMSEQ
                        and @I_vTAXDTLID = TAXDTLID)
        begin
          select @O_iErrorState = 1822
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      if @I_vTAXTYPE = 1
        begin
          if exists (select 1
                     from   PA10502 (nolock)
                     where  LNITMSEQ = 2147483646
                            and PAerdocnumber = @I_vPAerdocnumber
                            and TAXDTLID = @I_vTAXDTLID)
            begin
              select @O_iErrorState = 1823
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          if exists (select 1
                     from   PA10502 (nolock)
                     where  LNITMSEQ = 2147483645
                            and PAerdocnumber = @I_vPAerdocnumber
                            and TAXDTLID = @I_vTAXDTLID)
            begin
              select @O_iErrorState = 1824
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if @I_vTAXTYPE = 1
    begin
      select @I_vLNITMSEQ = 2147483646
    end
  
  if @I_vTAXTYPE = 2
    begin
      select @I_vLNITMSEQ = 2147483645
    end
  
  select @ORTAXAMT = @I_vTAXAMNT,
         @PAORIGREIMTAXAMT = @I_vPAREIMBURSTAXAMT,
         @ORPURTAX = @I_vPCTAXAMT,
         @ORFRTTAX = @I_vFRTTXAMT,
         @ORMSCTAX = @I_vMSCTXAMT,
         @ORTOTPUR = @I_vTXDTTPUR,
         @ORTXBPUR = @I_vTDTTXPUR
  
  if (@oErrString <> '')
    return (@O_iErrorState)
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_EmployeeExpense' ,
        @I_vINDEX1 = @I_vPAerdocnumber ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 0 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2035
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  insert into PA10502
             (PAertrxtype,
              PAerdocnumber,
              LNITMSEQ,
              TAXDTLID,
              EMPLOYID,
              TAXAMNT,
              PAREIMBURSTAXAMT,
              BKOUTTAX,
              ORTAXAMT,
              PAORIGREIMTAXAMT,
              PCTAXAMT,
              ORPURTAX,
              FRTTXAMT,
              ORFRTTAX,
              MSCTXAMT,
              ORMSCTAX,
              TXDTTPUR,
              ORTOTPUR,
              TDTTXPUR,
              ORTXBPUR,
              ACTINDX,
              CURRNIDX)
  select @I_vPAertrxtype,
         @I_vPAerdocnumber,
         @I_vLNITMSEQ,
         @I_vTAXDTLID,
         @I_vEMPLOYID,
         @I_vTAXAMNT,
         @I_vPAREIMBURSTAXAMT,
         @I_vBKOUTTAX,
         @ORTAXAMT,
         @PAORIGREIMTAXAMT,
         @I_vPCTAXAMT,
         @ORPURTAX,
         @I_vFRTTXAMT,
         @ORFRTTAX,
         @I_vMSCTXAMT,
         @ORMSCTAX,
         @I_vTXDTTPUR,
         @ORTOTPUR,
         @I_vTDTTXPUR,
         @ORTXBPUR,
         @I_vACTINDX,
         @CURRNIDX
  
  if @@error <> 0
    begin
      select @O_iErrorState = 1825
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @O_iErrorState <> 0
    return (@O_iErrorState)
  
  exec @iStatus = taPAEmpExpenseTaxInsertPost
    @I_vPAertrxtype ,
    @I_vPAerdocnumber ,
    @I_vLNITMSEQ ,
    @I_vTAXDTLID ,
    @I_vTAXTYPE ,
    @I_vEMPLOYID ,
    @I_vTAXAMNT ,
    @I_vPAREIMBURSTAXAMT ,
    @I_vBKOUTTAX ,
    @I_vPCTAXAMT ,
    @I_vFRTTXAMT ,
    @I_vMSCTXAMT ,
    @I_vTXDTTPUR ,
    @I_vTDTTXPUR ,
    @I_vACTINDX ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1826
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_EmployeeExpense' ,
        @I_vINDEX1 = @I_vPAerdocnumber ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 1 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2036
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  return (@O_iErrorState)

