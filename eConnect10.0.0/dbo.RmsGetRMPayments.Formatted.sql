

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetRMPayments]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetRMPayments]
  
create procedure [dbo].[RmsGetRMPayments]
                @iRMPaymentsTempTable VARCHAR(255),
                @iRMTenderTotals      VARCHAR(255),
                @iStartDate           DATETIME,
                @iEndDate             DATETIME,
                @iRmsServerName       VARCHAR(255),
                @iHqDbName            VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lPaymentTable VARCHAR(255)
  
  DECLARE  @lTenderEntryTable VARCHAR(255)
  
  DECLARE  @lCustomerTable VARCHAR(255)
  
  SELECT @lPaymentTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Payment as Payment'
  
  SELECT @lTenderEntryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.TenderEntry as TenderEntry'
  
  SELECT @lCustomerTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Customer as Customer'
  
  EXEC( 'INSERT INTO ' + @iRMPaymentsTempTable + ' (RmsPaymentID,  RmsStoreID,  Total_Sales,  CUSTNMBR,  DATE1)  SELECT  Payment.[ID],  Payment.StoreID,  Payment.Amount,  LEFT(ISNULL(UPPER(Customer.AccountNumber),''''),15),  CONVERT(DATETIME,CONVERT(VARCHAR,Payment.[Time],111))  FROM ' + @lPaymentTable + ' LEFT OUTER JOIN ' + @lCustomerTable + ' ON ' + ' Payment.CustomerID = Customer.ID  WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,Payment.Time,111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  NOT EXISTS(SELECT  1  FROM  RMS31000  WHERE   Payment.StoreID = RMS31000.RmsStoreID  AND   Payment.ID = RMS31000.RmsPaymentID)')
  
  EXEC( 'INSERT INTO ' + @iRMTenderTotals + ' (RmsTransactionNumber,  RmsStoreID,  IVCLINNO,  RmsTenderID,  SLSAMNT)  SELECT  TenderEntry.PaymentID,  TenderEntry.StoreID,  0,  TenderEntry.TenderID,  SUM(Amount)  FROM ' + @lTenderEntryTable + ' WHERE  EXISTS(SELECT  1  FROM ' + @iRMPaymentsTempTable + ' WHERE   TenderEntry.PaymentID = ' + @iRMPaymentsTempTable + '.RmsPaymentID  AND   TenderEntry.StoreID = ' + @iRMPaymentsTempTable + '.RmsStoreID)   GROUP BY  TenderEntry.PaymentID,  TenderEntry.PaymentID,  TenderEntry.StoreID,  TenderEntry.TenderID')

