

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsRMDistributionWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsRMDistributionWrapper]
  
create procedure [dbo].[RmsRMDistributionWrapper]
                @iDocNum     char(20),
                @iDocType    smallint,
                @iCustNum    char(15),
                @iDistType   smallint,
                @iDistRef    char(30),
                @iActIndx    int,
                @iCreditAmnt numeric(19,5),
                @iDebitAmnt  numeric(19,5),
                @oErrorState int  output,
                @oErrString  varchar(255)  output   /*with encryption*/
AS
  EXEC RmsRMDistribution
    @I_vRMDTYPAL = @iDocType ,
    @I_vDOCNUMBR = @iDocNum ,
    @I_vCUSTNMBR = @iCustNum ,
    @I_vDISTTYPE = @iDistType ,
    @I_vDistRef = @iDistRef ,
    @I_vDSTINDX = @iActIndx ,
    @I_vCRDTAMNT = @iCreditAmnt ,
    @I_vDEBITAMT = @iDebitAmnt ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

