

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetCurrencies]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetCurrencies]
  
create procedure [dbo].[RmsGetCurrencies]
                @iCurrencyTempTable VARCHAR(255),
                @iRmsServerName     VARCHAR(255),
                @iHqDbName          VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lCurrencyTable VARCHAR(255)
  
  SELECT @lCurrencyTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Currency'
  
  EXEC( 'INSERT INTO ' + @iCurrencyTempTable + ' SELECT  [ID],  Code,  [Description]  FROM ' + @lCurrencyTable)

