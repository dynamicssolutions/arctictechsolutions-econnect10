

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetRMArAdjustments]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetRMArAdjustments]
  
create procedure [dbo].[RmsGetRMArAdjustments]
                @iRMArAdjTempTable     VARCHAR(255),
                @iRmArHistAdjTempTable VARCHAR(255),
                @iStartDate            DateTime,
                @iEndDate              DateTime,
                @iRmsServerName        VARCHAR(255),
                @iHqDbName             VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lAccountReceivableHistoryTable VARCHAR(255)
  
  DECLARE  @lAccountReceivableTable VARCHAR(255)
  
  DECLARE  @lCustomerTable VARCHAR(255)
  
  SELECT @lAccountReceivableHistoryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.AccountReceivableHistory as AccountReceivableHistory'
  
  SELECT @lAccountReceivableTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.AccountReceivable as AccountReceivable'
  
  SELECT @lCustomerTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Customer as Customer'
  
  EXEC( 'INSERT INTO ' + @iRMArAdjTempTable + ' (RmsStoreID,  RmsArID,  CUSTNMBR,  Total_Sales,  DATE1)  SELECT  AccountReceivable.StoreID,  AccountReceivable.[ID],  LEFT(ISNULL(UPPER(Customer.AccountNumber),''''),15),  AccountReceivable.OriginalAmount,  AccountReceivable.Date   FROM ' + @lAccountReceivableHistoryTable + ' JOIN ' + @lAccountReceivableTable + ' ON ' + ' AccountReceivableHistory.StoreID = AccountReceivable.StoreID  AND  AccountReceivableHistory.AccountReceivableID = AccountReceivable.[ID] LEFT OUTER JOIN ' + @lCustomerTable + ' ON ' + 'AccountReceivable.CustomerID = Customer.[ID]  WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,AccountReceivable.Date,111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  AccountReceivable.Type = 2  AND  AccountReceivableHistory.HistoryType in (5,6)  AND  NOT EXISTS(SELECT  1  FROM  RMS32000  WHERE  AccountReceivable.StoreID = RMS32000.RmsStoreID  AND  AccountReceivable.[ID] = RMS32000.RmsArID)')
  
  EXEC( 'INSERT INTO ' + @iRmArHistAdjTempTable + ' (RmsStoreID,  RmsArHistID,  CUSTNMBR,  Total_Sales,  DATE1)  SELECT  AccountReceivableHistory.StoreID,  AccountReceivableHistory.[ID],  LEFT(ISNULL(UPPER(Customer.AccountNumber),''''),15),  AccountReceivableHistory.Amount,  AccountReceivableHistory.Date   FROM ' + @lAccountReceivableHistoryTable + ' JOIN ' + @lAccountReceivableTable + ' ON ' + ' AccountReceivableHistory.StoreID = AccountReceivable.StoreID  AND  AccountReceivableHistory.AccountReceivableID = AccountReceivable.[ID] LEFT OUTER JOIN ' + @lCustomerTable + ' ON ' + 'AccountReceivable.CustomerID = Customer.[ID]  WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,AccountReceivableHistory.Date,111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  AccountReceivableHistory.HistoryType in (4,5,6)  AND  AccountReceivable.Type in (0,1)  AND  NOT EXISTS(SELECT  1  FROM  RMS32050  WHERE  AccountReceivableHistory.StoreID = RMS32050.RmsStoreID  AND  AccountReceivableHistory.[ID] = RMS32050.RmsArHistID)')

