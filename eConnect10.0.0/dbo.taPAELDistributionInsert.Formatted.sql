

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAELDistributionInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAELDistributionInsert]
  
create procedure [dbo].[taPAELDistributionInsert]
                @I_vCURNCYID   char(15),
                @I_vCURRNIDX   int,
                @I_vORCRDAMT   numeric(19,5),
                @I_vORDBTAMT   numeric(19,5),
                @I_vPAEQLOGNO  char(17),
                @I_vTRXSORCE   char(13),
                @I_vCNTRLTYP   smallint,
                @I_vCRDTAMNT   numeric(19,5),
                @I_vDEBITAMT   numeric(19,5),
                @I_vDSTINDX    int,
                @I_vDISTTYPE   smallint,
                @I_vDistRef    char(31),
                @I_vUSERID     char(15),
                @I_vPAEQUIPTID char(15),
                @O_iErrorState int  = NULL output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @DSTSQNUM int,
           @iStatus  int,
           @iError   int,
           @CURNCYID char(15)
  
  select @O_iErrorState = 0,
         @iStatus = 0,
         @iError = 0,
         @DSTSQNUM = 0
  
  if (@I_vCURNCYID is NULL 
       or @I_vCURRNIDX is NULL
       or @I_vORCRDAMT is NULL
       or @I_vORDBTAMT is NULL
       or @I_vPAEQLOGNO is NULL
       or @I_vTRXSORCE is NULL
       or @I_vCNTRLTYP is NULL
       or @I_vCRDTAMNT is NULL
       or @I_vDEBITAMT is NULL
       or @I_vDSTINDX is NULL
       or @I_vDISTTYPE is NULL
       or @I_vDistRef is NULL
       or @I_vUSERID is NULL
       or @I_vPAEQUIPTID is NULL)
    begin
      select @O_iErrorState = 6009
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAEQLOGNO = ''
       or @I_vDISTTYPE = 0)
    begin
      select @O_iErrorState = 6010
      
      return (@O_iErrorState)
    end
  
  select @I_vPAEQLOGNO = UPPER(@I_vPAEQLOGNO),
         @I_vCURNCYID = UPPER(@I_vCURNCYID)
  
  select @DSTSQNUM = isnull(max(DSTSQNUM),0) + 10
  from   PA10103 (nolock)
  where  PAEQLOGNO = @I_vPAEQLOGNO
  
  if (@DSTSQNUM = 10)
    select @DSTSQNUM = 1
  
  insert into PA10103
             (CURNCYID,
              CURRNIDX,
              ORCRDAMT,
              ORDBTAMT,
              PAEQLOGNO,
              TRXSORCE,
              DSTSQNUM,
              CNTRLTYP,
              CRDTAMNT,
              DEBITAMT,
              DSTINDX,
              DISTTYPE,
              DistRef,
              USERID,
              PAEQUIPTID)
  select @I_vCURNCYID,
         @I_vCURRNIDX,
         @I_vORCRDAMT,
         @I_vORDBTAMT,
         @I_vPAEQLOGNO,
         @I_vTRXSORCE,
         @DSTSQNUM,
         @I_vCNTRLTYP,
         @I_vCRDTAMNT,
         @I_vDEBITAMT,
         @I_vDSTINDX,
         @I_vDISTTYPE,
         @I_vDistRef,
         @I_vUSERID,
         @I_vPAEQUIPTID
  
  if @@error <> 0
    begin
      select @O_iErrorState = 6011
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

