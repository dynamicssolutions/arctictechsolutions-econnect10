

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taCreateDeduction]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taCreateDeduction]
  
create procedure [dbo].[taCreateDeduction]
                @I_vDEDUCTON       char(6),
                @I_vDSCRIPTN       char(30),
                @I_vINACTIVE       tinyint  = 0,
                @I_vDEDBEGDT       datetime,
                @I_vDEDENDDT       datetime  = '',
                @I_vVARDEDTN       tinyint  = 0,
                @I_vDEDNFREQ       smallint  = 1,
                @I_vSFRFEDTX       tinyint  = 0,
                @I_vSHFRFICA       tinyint  = 0,
                @I_vSHFRSTTX       tinyint  = 0,
                @I_vSFRLCLTX       tinyint  = 0,
                @I_vBSDORCDS       smallint  = 0,
                @I_vDEDNMTHD       smallint  = 1,
                @I_vDEDFRMLA       smallint  = 0,
                @I_vDEDCAMNT_1     numeric(19,5)  = 0,
                @I_vDEDCAMNT_2     numeric(19,5)  = 0,
                @I_vDEDCAMNT_3     numeric(19,5)  = 0,
                @I_vDEDCAMNT_4     numeric(19,5)  = 0,
                @I_vDEDCAMNT_5     numeric(19,5)  = 0,
                @I_vDEDNPRCT_1     numeric(19,5)  = 0,
                @I_vDEDNPRCT_2     numeric(19,5)  = 0,
                @I_vDEDNPRCT_3     numeric(19,5)  = 0,
                @I_vDEDNPRCT_4     numeric(19,5)  = 0,
                @I_vDEDNPRCT_5     numeric(19,5)  = 0,
                @I_vDEDTRMAX_1     numeric(19,5)  = 0,
                @I_vDEDTRMAX_2     numeric(19,5)  = 0,
                @I_vDEDTRMAX_3     numeric(19,5)  = 0,
                @I_vDEDTRMAX_4     numeric(19,5)  = 0,
                @I_vDEDTRMAX_5     numeric(19,5)  = 0,
                @I_vDETRMXUN_1     numeric(19,5)  = 0,
                @I_vDETRMXUN_2     numeric(19,5)  = 0,
                @I_vDETRMXUN_3     numeric(19,5)  = 0,
                @I_vDETRMXUN_4     numeric(19,5)  = 0,
                @I_vDETRMXUN_5     numeric(19,5)  = 0,
                @I_vDEPYPRMX       numeric(19,5)  = 0,
                @I_vDEDYRMAX       numeric(19,5)  = 0,
                @I_vDEDLTMAX       numeric(19,5)  = 0,
                @I_vW2BXNMBR       smallint  = 0,
                @I_vW2BXLABL       char(6)  = '',
                @I_vDATAENTDFLT    tinyint  = 0,
                @I_vUpdateIfExists tinyint  = 1,
                @I_vRequesterTrx   smallint  = 0,
                @I_vUSRDEFND1      char(50)  = '',
                @I_vUSRDEFND2      char(50)  = '',
                @I_vUSRDEFND3      char(50)  = '',
                @I_vUSRDEFND4      varchar(8000)  = '',
                @I_vUSRDEFND5      varchar(8000)  = '',
                @O_iErrorState     int  output,
                @oErrString        varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @sCompanyID              int,
           @iStatus                 int,
           @iCustomState            int,
           @iCustomErrString        varchar(255),
           @nNextNoteIndex          numeric(19,5),
           @iGetNextNoteIdxErrState int,
           @iAddCodeErrState        int,
           @O_oErrorState           int,
           @iError                  int
  
  select @O_iErrorState = 0,
         @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500
  where  INTERID = db_name()
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taCreateDeductionPre
    @I_vDEDUCTON output ,
    @I_vDSCRIPTN output ,
    @I_vINACTIVE output ,
    @I_vDEDBEGDT output ,
    @I_vDEDENDDT output ,
    @I_vVARDEDTN output ,
    @I_vDEDNFREQ output ,
    @I_vSFRFEDTX output ,
    @I_vSHFRFICA output ,
    @I_vSHFRSTTX output ,
    @I_vSFRLCLTX output ,
    @I_vBSDORCDS output ,
    @I_vDEDNMTHD output ,
    @I_vDEDFRMLA output ,
    @I_vDEDCAMNT_1 output ,
    @I_vDEDCAMNT_2 output ,
    @I_vDEDCAMNT_3 output ,
    @I_vDEDCAMNT_4 output ,
    @I_vDEDCAMNT_5 output ,
    @I_vDEDNPRCT_1 output ,
    @I_vDEDNPRCT_2 output ,
    @I_vDEDNPRCT_3 output ,
    @I_vDEDNPRCT_4 output ,
    @I_vDEDNPRCT_5 output ,
    @I_vDEDTRMAX_1 output ,
    @I_vDEDTRMAX_2 output ,
    @I_vDEDTRMAX_3 output ,
    @I_vDEDTRMAX_4 output ,
    @I_vDEDTRMAX_5 output ,
    @I_vDETRMXUN_1 output ,
    @I_vDETRMXUN_2 output ,
    @I_vDETRMXUN_3 output ,
    @I_vDETRMXUN_4 output ,
    @I_vDETRMXUN_5 output ,
    @I_vDEPYPRMX output ,
    @I_vDEDYRMAX output ,
    @I_vDEDLTMAX output ,
    @I_vW2BXNMBR output ,
    @I_vW2BXLABL output ,
    @I_vDATAENTDFLT output ,
    @I_vUpdateIfExists output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @O_iErrorState = 3883
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vDEDUCTON is NULL 
       or @I_vDSCRIPTN is NULL
       or @I_vINACTIVE is NULL
       or @I_vDEDBEGDT is NULL
       or @I_vDEDENDDT is NULL
       or @I_vVARDEDTN is NULL
       or @I_vDEDNFREQ is NULL
       or @I_vSFRFEDTX is NULL
       or @I_vSHFRFICA is NULL
       or @I_vSHFRSTTX is NULL
       or @I_vSFRLCLTX is NULL
       or @I_vBSDORCDS is NULL
       or @I_vDEDNMTHD is NULL
       or @I_vDEDFRMLA is NULL
       or @I_vDEDCAMNT_1 is NULL
       or @I_vDEDCAMNT_2 is NULL
       or @I_vDEDCAMNT_3 is NULL
       or @I_vDEDCAMNT_4 is NULL
       or @I_vDEDCAMNT_5 is NULL
       or @I_vDEDNPRCT_1 is NULL
       or @I_vDEDNPRCT_2 is NULL
       or @I_vDEDNPRCT_3 is NULL
       or @I_vDEDNPRCT_4 is NULL
       or @I_vDEDNPRCT_5 is NULL
       or @I_vDEDTRMAX_1 is NULL
       or @I_vDEDTRMAX_2 is NULL
       or @I_vDEDTRMAX_3 is NULL
       or @I_vDEDTRMAX_4 is NULL
       or @I_vDEDTRMAX_5 is NULL
       or @I_vDETRMXUN_1 is NULL
       or @I_vDETRMXUN_2 is NULL
       or @I_vDETRMXUN_3 is NULL
       or @I_vDETRMXUN_4 is NULL
       or @I_vDETRMXUN_5 is NULL
       or @I_vDEPYPRMX is NULL
       or @I_vDEDYRMAX is NULL
       or @I_vDEDLTMAX is NULL
       or @I_vW2BXNMBR is NULL
       or @I_vW2BXLABL is NULL
       or @I_vDATAENTDFLT is NULL
       or @I_vUpdateIfExists is NULL
       or @I_vRequesterTrx is NULL
       or @I_vUSRDEFND1 is NULL
       or @I_vUSRDEFND2 is NULL
       or @I_vUSRDEFND3 is NULL
       or @I_vUSRDEFND4 is NULL
       or @I_vUSRDEFND5 is NULL)
    begin
      select @O_iErrorState = 3884
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  select @I_vDEDUCTON = UPPER(@I_vDEDUCTON)
  
  if (@I_vDEDUCTON = '')
    begin
      select @O_iErrorState = 3885
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDSCRIPTN = '')
    begin
      select @O_iErrorState = 3886
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDBEGDT = '')
    begin
      select @O_iErrorState = 3887
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vINACTIVE < 0
       or @I_vINACTIVE > 1)
    begin
      select @O_iErrorState = 3888
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vBSDORCDS < 0
       or @I_vBSDORCDS > 1)
    begin
      select @O_iErrorState = 3889
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vSFRFEDTX < 0
       or @I_vSFRFEDTX > 1)
    begin
      select @O_iErrorState = 3890
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vSHFRFICA < 0
       or @I_vSHFRFICA > 1)
    begin
      select @O_iErrorState = 3891
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vSHFRSTTX < 0
       or @I_vSHFRSTTX > 1)
    begin
      select @O_iErrorState = 3892
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vSFRLCLTX < 0
       or @I_vSFRLCLTX > 1)
    begin
      select @O_iErrorState = 3893
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDNFREQ < 1
       or @I_vDEDNFREQ > 8)
    begin
      select @O_iErrorState = 3894
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDNMTHD < 1
       or @I_vDEDNMTHD > 4)
    begin
      select @O_iErrorState = 3895
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDFRMLA < 0
       or @I_vDEDFRMLA > 1)
    begin
      select @O_iErrorState = 3896
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDNPRCT_1 < 0)
    begin
      select @O_iErrorState = 3530
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDNPRCT_2 < 0)
    begin
      select @O_iErrorState = 4231
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDNPRCT_3 < 0)
    begin
      select @O_iErrorState = 4232
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDNPRCT_4 < 0)
    begin
      select @O_iErrorState = 4233
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDNPRCT_5 < 0)
    begin
      select @O_iErrorState = 4234
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEPYPRMX > @I_vDEDYRMAX)
     and (@I_vDEDYRMAX <> 0)
    begin
      select @O_iErrorState = 4240
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEPYPRMX > @I_vDEDLTMAX)
     and (@I_vDEDLTMAX <> 0)
    begin
      select @O_iErrorState = 3642
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDYRMAX > @I_vDEDLTMAX)
     and (@I_vDEDLTMAX <> 0)
    begin
      select @O_iErrorState = 3643
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vW2BXNMBR > 99)
    begin
      select @O_iErrorState = 3897
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDATAENTDFLT < 0
       or @I_vDATAENTDFLT > 1)
    begin
      select @O_iErrorState = 4085
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vUpdateIfExists < 0
       or @I_vUpdateIfExists > 1)
    begin
      select @O_iErrorState = 4086
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vRequesterTrx < 0
       or @I_vRequesterTrx > 1)
    begin
      select @O_iErrorState = 3663
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vVARDEDTN = 0
      and @I_vDATAENTDFLT = 1)
    begin
      select @O_iErrorState = 3898
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDNMTHD = 1
       or @I_vDEDNMTHD = 2)
     and (@I_vDEDCAMNT_1 <> 0
           or @I_vDEDCAMNT_2 <> 0
           or @I_vDEDCAMNT_3 <> 0
           or @I_vDEDCAMNT_4 <> 0
           or @I_vDEDCAMNT_5 <> 0)
    begin
      select @O_iErrorState = 3899
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDNMTHD = 3
       or @I_vDEDNMTHD = 4)
     and (@I_vDEDNPRCT_1 <> 0
           or @I_vDEDNPRCT_2 <> 0
           or @I_vDEDNPRCT_3 <> 0
           or @I_vDEDNPRCT_4 <> 0
           or @I_vDEDNPRCT_5 <> 0)
    begin
      select @O_iErrorState = 3900
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDNMTHD <> 4)
     and (@I_vDETRMXUN_1 <> 0
           or @I_vDETRMXUN_2 <> 0
           or @I_vDETRMXUN_3 <> 0
           or @I_vDETRMXUN_4 <> 0
           or @I_vDETRMXUN_5 <> 0)
    begin
      select @O_iErrorState = 3901
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDFRMLA = 0)
     and (@I_vDEDCAMNT_2 <> 0
           or @I_vDEDCAMNT_3 <> 0
           or @I_vDEDCAMNT_4 <> 0
           or @I_vDEDCAMNT_5 <> 0
           or @I_vDEDTRMAX_1 <> 0
           or @I_vDEDTRMAX_2 <> 0
           or @I_vDEDTRMAX_3 <> 0
           or @I_vDEDTRMAX_4 <> 0
           or @I_vDEDTRMAX_5 <> 0)
    begin
      select @O_iErrorState = 3922
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDNMTHD = 2)
     and (@I_vSFRFEDTX <> 0
           or @I_vSHFRFICA <> 0
           or @I_vSHFRSTTX <> 0
           or @I_vSFRLCLTX <> 0)
    begin
      select @O_iErrorState = 3923
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDFRMLA = 1)
     and (@I_vDEDTRMAX_2 <> 0)
     and (@I_vDEDTRMAX_1 > @I_vDEDTRMAX_2)
    begin
      select @O_iErrorState = 4229
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDFRMLA = 1)
     and (@I_vDEDTRMAX_3 <> 0)
     and (@I_vDEDTRMAX_2 > @I_vDEDTRMAX_3)
    begin
      select @O_iErrorState = 4230
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDFRMLA = 1)
     and (@I_vDEDTRMAX_4 <> 0)
     and (@I_vDEDTRMAX_3 > @I_vDEDTRMAX_4)
    begin
      select @O_iErrorState = 4235
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDFRMLA = 1)
     and (@I_vDEDTRMAX_5 <> 0)
     and (@I_vDEDTRMAX_4 > @I_vDEDTRMAX_5)
    begin
      select @O_iErrorState = 4236
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDFRMLA = 1)
     and (@I_vDETRMXUN_2 <> 0)
     and (@I_vDETRMXUN_1 > @I_vDETRMXUN_2)
    begin
      select @O_iErrorState = 4237
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDFRMLA = 1)
     and (@I_vDETRMXUN_3 <> 0)
     and (@I_vDETRMXUN_2 > @I_vDETRMXUN_3)
    begin
      select @O_iErrorState = 4238
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDFRMLA = 1)
     and (@I_vDETRMXUN_4 <> 0)
     and (@I_vDETRMXUN_3 > @I_vDETRMXUN_4)
    begin
      select @O_iErrorState = 4239
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vDEDFRMLA = 1)
     and (@I_vDETRMXUN_5 <> 0)
     and (@I_vDETRMXUN_4 > @I_vDETRMXUN_5)
    begin
      select @O_iErrorState = 4241
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if @O_iErrorState <> 0
    return (@O_iErrorState)
  
  exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
    @I_sCompanyID = @sCompanyID ,
    @I_iSQLSessionID = 0 ,
    @I_noteincrement = 1 ,
    @O_mNoteIndex = @nNextNoteIndex output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iGetNextNoteIdxErrState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iGetNextNoteIdxErrState))
      
      select @O_iErrorState = 3921
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if not exists (select 1
                 from   UPR40900 (nolock)
                 where  DEDUCTON = @I_vDEDUCTON)
    begin
      insert UPR40900
            (DEDUCTON,
             DSCRIPTN,
             INACTIVE,
             DEDBEGDT,
             DEDENDDT,
             VARDEDTN,
             DEDNFREQ,
             TXSHANTY,
             SFRFEDTX,
             SHFRFICA,
             SHFRSTTX,
             SFRLCLTX,
             BSDORCDS,
             DEDNMTHD,
             DEDFRMLA,
             DEDCAMNT_1,
             DEDCAMNT_2,
             DEDCAMNT_3,
             DEDCAMNT_4,
             DEDCAMNT_5,
             DEDNPRCT_1,
             DEDNPRCT_2,
             DEDNPRCT_3,
             DEDNPRCT_4,
             DEDNPRCT_5,
             DEDTRMAX_1,
             DEDTRMAX_2,
             DEDTRMAX_3,
             DEDTRMAX_4,
             DEDTRMAX_5,
             DETRMXUN_1,
             DETRMXUN_2,
             DETRMXUN_3,
             DETRMXUN_4,
             DETRMXUN_5,
             DEPYPRMX,
             DEDYRMAX,
             DEDLTMAX,
             W2BXNMBR,
             W2BXLABL,
             NOTEINDX,
             DATAENTDFLT,
             DEDTYPE,
             GARNCAT,
             EARNINGSCODE)
      select @I_vDEDUCTON,
             @I_vDSCRIPTN,
             @I_vINACTIVE,
             @I_vDEDBEGDT,
             @I_vDEDENDDT,
             @I_vVARDEDTN,
             @I_vDEDNFREQ,
             case 
               WHEN @I_vSFRFEDTX = 1
                     or @I_vSHFRFICA = 1
                     or @I_vSHFRSTTX = 1
                     or @I_vSFRLCLTX = 1 THEN 1
               ELSE 0
             end,
             @I_vSFRFEDTX,
             @I_vSHFRFICA,
             @I_vSHFRSTTX,
             @I_vSFRLCLTX,
             @I_vBSDORCDS,
             @I_vDEDNMTHD,
             @I_vDEDFRMLA,
             @I_vDEDCAMNT_1,
             @I_vDEDCAMNT_2,
             @I_vDEDCAMNT_3,
             @I_vDEDCAMNT_4,
             @I_vDEDCAMNT_5,
             @I_vDEDNPRCT_1,
             @I_vDEDNPRCT_2,
             @I_vDEDNPRCT_3,
             @I_vDEDNPRCT_4,
             @I_vDEDNPRCT_5,
             @I_vDEDTRMAX_1,
             @I_vDEDTRMAX_2,
             @I_vDEDTRMAX_3,
             @I_vDEDTRMAX_4,
             @I_vDEDTRMAX_5,
             @I_vDETRMXUN_1 * 100,
             @I_vDETRMXUN_2 * 100,
             @I_vDETRMXUN_3 * 100,
             @I_vDETRMXUN_4 * 100,
             @I_vDETRMXUN_5 * 100,
             @I_vDEPYPRMX,
             @I_vDEDYRMAX,
             @I_vDEDLTMAX,
             @I_vW2BXNMBR,
             @I_vW2BXLABL,
             @nNextNoteIndex,
             @I_vDATAENTDFLT,
             1,
             0,
             ''
      
      if @@error <> 0
        begin
          select @O_iErrorState = 3902
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
          
          return (@O_iErrorState)
        end
    end
  else
    if (@I_vUpdateIfExists = 1)
      begin
        update UPR40900
        set    DSCRIPTN = case 
                            WHEN @I_vDSCRIPTN = '' THEN DSCRIPTN
                            ELSE @I_vDSCRIPTN
                          end,
               INACTIVE = @I_vINACTIVE,
               DEDBEGDT = case 
                            WHEN @I_vDEDBEGDT = '' THEN DEDBEGDT
                            ELSE @I_vDEDBEGDT
                          end,
               DEDENDDT = case 
                            WHEN @I_vDEDENDDT = '' THEN DEDENDDT
                            ELSE @I_vDEDENDDT
                          end,
               VARDEDTN = @I_vVARDEDTN,
               DEDNFREQ = @I_vDEDNFREQ,
               SFRFEDTX = @I_vSFRFEDTX,
               SHFRFICA = @I_vSHFRFICA,
               SHFRSTTX = @I_vSHFRSTTX,
               SFRLCLTX = @I_vSFRLCLTX,
               BSDORCDS = @I_vBSDORCDS,
               DEDNMTHD = @I_vDEDNMTHD,
               DEDFRMLA = @I_vDEDFRMLA,
               DEDCAMNT_1 = case 
                              WHEN @I_vDEDNMTHD > 0
                                   and @I_vDEDNMTHD < 3 THEN 0
                              ELSE @I_vDEDCAMNT_1
                            end,
               DEDCAMNT_2 = case 
                              WHEN @I_vDEDNMTHD > 0
                                   and @I_vDEDNMTHD < 3 THEN 0
                              ELSE @I_vDEDCAMNT_2
                            end,
               DEDCAMNT_3 = case 
                              WHEN @I_vDEDNMTHD > 0
                                   and @I_vDEDNMTHD < 3 THEN 0
                              ELSE @I_vDEDCAMNT_3
                            end,
               DEDCAMNT_4 = case 
                              WHEN @I_vDEDNMTHD > 0
                                   and @I_vDEDNMTHD < 3 THEN 0
                              ELSE @I_vDEDCAMNT_4
                            end,
               DEDCAMNT_5 = case 
                              WHEN @I_vDEDNMTHD > 0
                                   and @I_vDEDNMTHD < 3 THEN 0
                              ELSE @I_vDEDCAMNT_5
                            end,
               DEDNPRCT_1 = case 
                              WHEN @I_vDEDNMTHD > 2
                                   and @I_vDEDNMTHD < 5 THEN 0
                              ELSE @I_vDEDNPRCT_1
                            end,
               DEDNPRCT_2 = case 
                              WHEN @I_vDEDNMTHD > 2
                                   and @I_vDEDNMTHD < 5 THEN 0
                              ELSE @I_vDEDNPRCT_2
                            end,
               DEDNPRCT_3 = case 
                              WHEN @I_vDEDNMTHD > 2
                                   and @I_vDEDNMTHD < 5 THEN 0
                              ELSE @I_vDEDNPRCT_3
                            end,
               DEDNPRCT_4 = case 
                              WHEN @I_vDEDNMTHD > 2
                                   and @I_vDEDNMTHD < 5 THEN 0
                              ELSE @I_vDEDNPRCT_4
                            end,
               DEDNPRCT_5 = case 
                              WHEN @I_vDEDNMTHD > 2
                                   and @I_vDEDNMTHD < 5 THEN 0
                              ELSE @I_vDEDNPRCT_5
                            end,
               DEDTRMAX_1 = case 
                              WHEN @I_vDEDFRMLA = 0 THEN 0
                              ELSE @I_vDEDTRMAX_1
                            end,
               DEDTRMAX_2 = case 
                              WHEN @I_vDEDFRMLA = 0 THEN 0
                              ELSE @I_vDEDTRMAX_2
                            end,
               DEDTRMAX_3 = case 
                              WHEN @I_vDEDFRMLA = 0 THEN 0
                              ELSE @I_vDEDTRMAX_3
                            end,
               DEDTRMAX_4 = case 
                              WHEN @I_vDEDFRMLA = 0 THEN 0
                              ELSE @I_vDEDTRMAX_4
                            end,
               DEDTRMAX_5 = case 
                              WHEN @I_vDEDFRMLA = 0 THEN 0
                              ELSE @I_vDEDTRMAX_5
                            end,
               DETRMXUN_1 = case 
                              WHEN @I_vDEDNMTHD <> 4 THEN 0
                              ELSE @I_vDETRMXUN_1 * 100
                            end,
               DETRMXUN_2 = case 
                              WHEN @I_vDEDNMTHD <> 4 THEN 0
                              ELSE @I_vDETRMXUN_2 * 100
                            end,
               DETRMXUN_3 = case 
                              WHEN @I_vDEDNMTHD <> 4 THEN 0
                              ELSE @I_vDETRMXUN_3 * 100
                            end,
               DETRMXUN_4 = case 
                              WHEN @I_vDEDNMTHD <> 4 THEN 0
                              ELSE @I_vDETRMXUN_4 * 100
                            end,
               DETRMXUN_5 = case 
                              WHEN @I_vDEDNMTHD <> 4 THEN 0
                              ELSE @I_vDETRMXUN_5 * 100
                            end,
               DEPYPRMX = @I_vDEPYPRMX,
               DEDYRMAX = @I_vDEDYRMAX,
               DEDLTMAX = @I_vDEDLTMAX,
               W2BXNMBR = @I_vW2BXNMBR,
               W2BXLABL = case 
                            WHEN @I_vW2BXLABL = '' THEN W2BXLABL
                            ELSE @I_vW2BXLABL
                          end
        where  DEDUCTON = @I_vDEDUCTON
        
        if @@error <> 0
          begin
            select @O_iErrorState = 3903
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @iAddCodeErrState output
            
            return (@O_iErrorState)
          end
      end
  
  exec @iStatus = taCreateDeductionPost
    @I_vDEDUCTON ,
    @I_vDSCRIPTN ,
    @I_vINACTIVE ,
    @I_vDEDBEGDT ,
    @I_vDEDENDDT ,
    @I_vVARDEDTN ,
    @I_vDEDNFREQ ,
    @I_vSFRFEDTX ,
    @I_vSHFRFICA ,
    @I_vSHFRSTTX ,
    @I_vSFRLCLTX ,
    @I_vBSDORCDS ,
    @I_vDEDNMTHD ,
    @I_vDEDFRMLA ,
    @I_vDEDCAMNT_1 ,
    @I_vDEDCAMNT_2 ,
    @I_vDEDCAMNT_3 ,
    @I_vDEDCAMNT_4 ,
    @I_vDEDCAMNT_5 ,
    @I_vDEDNPRCT_1 ,
    @I_vDEDNPRCT_2 ,
    @I_vDEDNPRCT_3 ,
    @I_vDEDNPRCT_4 ,
    @I_vDEDNPRCT_5 ,
    @I_vDEDTRMAX_1 ,
    @I_vDEDTRMAX_2 ,
    @I_vDEDTRMAX_3 ,
    @I_vDEDTRMAX_4 ,
    @I_vDEDTRMAX_5 ,
    @I_vDETRMXUN_1 ,
    @I_vDETRMXUN_2 ,
    @I_vDETRMXUN_3 ,
    @I_vDETRMXUN_4 ,
    @I_vDETRMXUN_5 ,
    @I_vDEPYPRMX ,
    @I_vDEDYRMAX ,
    @I_vDEDLTMAX ,
    @I_vW2BXNMBR ,
    @I_vW2BXLABL ,
    @I_vDATAENTDFLT ,
    @I_vUpdateIfExists ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @O_iErrorState = 3904
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

