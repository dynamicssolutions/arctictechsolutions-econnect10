

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPATSCreateDistributions]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPATSCreateDistributions]
  
create procedure [dbo].[taPATSCreateDistributions]
                @I_vCURNCYID   char(15),
                @I_vCURRNIDX   smallint,
                @I_vPATSNO     char(17),
                @I_vEMPLOYID   char(15),
                @O_iErrorState int  = NULL output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @O_oErrorState int,
           @cDTYPE        int,
           @cDTAMT        numeric(19,5),
           @cCTAMT        numeric(19,5),
           @iCursorError  int,
           @cINDEX        int,
           @cStatement    varchar(255),
           @iStatus       int,
           @iError        int,
           @ORDTAMT       numeric(19,5),
           @ORCTAMT       numeric(19,5)
  
  select @O_iErrorState = 0,
         @iStatus = 0,
         @O_oErrorState = 0,
         @iCursorError = 0,
         @iError = 0
  
  if (@I_vCURNCYID is NULL 
       or @I_vCURRNIDX is NULL
       or @I_vPATSNO is NULL
       or @I_vEMPLOYID is NULL)
    begin
      select @O_iErrorState = 1810
      
      return (@O_iErrorState)
    end
  
  if (@I_vCURNCYID = ''
       or @I_vCURRNIDX = 0
       or @I_vPATSNO = ''
       or @I_vEMPLOYID = '')
    begin
      select @O_iErrorState = 1811
      
      return (@O_iErrorState)
    end
  
  select @I_vCURNCYID = UPPER(@I_vCURNCYID),
         @I_vPATSNO = UPPER(@I_vPATSNO),
         @I_vEMPLOYID = UPPER(@I_vEMPLOYID)
  
  delete PA10003
  where  PATSNO = @I_vPATSNO
  
  if @@error <> 0
    begin
      select @O_iErrorState = 744
      
      return (@O_iErrorState)
    end
  
  create table #temp (
    DTYPE   int,
    DTAMT   numeric(19,5),
    CTAMT   numeric(19,5),
    ORDTAMT numeric(19,5),
    ORCTAMT numeric(19,5),
    DTINDEX int)
  
  create table #temp1 (
    DTYPE   int,
    DTAMT   numeric(19,5),
    CTAMT   numeric(19,5),
    ORDTAMT numeric(19,5),
    ORCTAMT numeric(19,5),
    DTINDEX int)
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   1,
           isnull(sum(PATOTCST),0),
           0,
           isnull(sum(PAORIGTOTCOST),0),
           0,
           PACGBWIPIDX
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PACGBWIPIDX <> 0
           and PAOverhead_IDX <> 0
           and PAQtyQ >= 0
  group by PACGBWIPIDX
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   1,
           0,
           isnull(sum(PATOTCST),0) * (-1),
           0,
           isnull(sum(PAORIGTOTCOST),0) * (-1),
           PACGBWIPIDX
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PACGBWIPIDX <> 0
           and PAOverhead_IDX <> 0
           and PAQtyQ <= 0
  group by PACGBWIPIDX
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   1,
           isnull(sum(PAEXTCOST),0),
           0,
           isnull(sum(OREXTCST),0),
           0,
           PACGBWIPIDX
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PACGBWIPIDX <> 0
           and PAOverhead_IDX = 0
           and PAQtyQ >= 0
  group by PACGBWIPIDX
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   1,
           0,
           isnull(sum(PAEXTCOST),0) * (-1),
           0,
           isnull(sum(OREXTCST),0) * (-1),
           PACGBWIPIDX
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PACGBWIPIDX <> 0
           and PAOverhead_IDX = 0
           and PAQtyQ <= 0
  group by PACGBWIPIDX
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   2,
           isnull(sum(PATOTCST),0),
           0,
           isnull(sum(PAORIGTOTCOST),0),
           0,
           PACogs_Idx
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PACogs_Idx <> 0
           and PAOverhead_IDX <> 0
           and PAQtyQ >= 0
  group by PACogs_Idx
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   2,
           0,
           isnull(sum(PATOTCST),0) * (-1),
           0,
           isnull(sum(PAORIGTOTCOST),0) * (-1),
           PACogs_Idx
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PACogs_Idx <> 0
           and PAOverhead_IDX <> 0
           and PAQtyQ <= 0
  group by PACogs_Idx
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   2,
           isnull(sum(PAEXTCOST),0),
           0,
           isnull(sum(OREXTCST),0),
           0,
           PACogs_Idx
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PACogs_Idx <> 0
           and PAOverhead_IDX = 0
           and PAQtyQ >= 0
  group by PACogs_Idx
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   2,
           0,
           isnull(sum(PAEXTCOST),0) * (-1),
           0,
           isnull(sum(OREXTCST),0) * (-1),
           PACogs_Idx
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PACogs_Idx <> 0
           and PAOverhead_IDX = 0
           and PAQtyQ <= 0
  group by PACogs_Idx
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   3,
           0,
           isnull(sum(PAEXTCOST),0),
           0,
           isnull(sum(OREXTCST),0),
           PAContra_Account_IDX
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PAContra_Account_IDX <> 0
           and PAQtyQ >= 0
  group by PAContra_Account_IDX
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   3,
           isnull(sum(PAEXTCOST),0) * (-1),
           0,
           isnull(sum(OREXTCST),0) * (-1),
           0,
           PAContra_Account_IDX
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PAContra_Account_IDX <> 0
           and PAQtyQ <= 0
  group by PAContra_Account_IDX
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   4,
           isnull(sum(PAACREV),0),
           0,
           isnull(sum(PAORIACCRREV),0),
           0,
           PAUnbilled_AR_Idx
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PAUnbilled_AR_Idx <> 0
           and PAQtyQ >= 0
  group by PAUnbilled_AR_Idx
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   4,
           0,
           isnull(sum(PAACREV),0) * (-1),
           0,
           isnull(sum(PAORIACCRREV),0) * (-1),
           PAUnbilled_AR_Idx
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PAUnbilled_AR_Idx <> 0
           and PAQtyQ <= 0
  group by PAUnbilled_AR_Idx
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   5,
           0,
           isnull(sum(PAACREV),0),
           0,
           isnull(sum(PAORIACCRREV),0),
           PAUnbilled_Proj_Rev_Idx
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PAUnbilled_Proj_Rev_Idx <> 0
           and PAQtyQ >= 0
  group by PAUnbilled_Proj_Rev_Idx
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   5,
           isnull(sum(PAACREV),0) * (-1),
           0,
           isnull(sum(PAORIACCRREV),0) * (-1),
           0,
           PAUnbilled_Proj_Rev_Idx
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PAUnbilled_Proj_Rev_Idx <> 0
           and PAQtyQ <= 0
  group by PAUnbilled_Proj_Rev_Idx
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   6,
           0,
           isnull(sum(PATOTALOVERH),0),
           0,
           isnull(sum(PAORIGTOTOVRHD),0),
           PAOverhead_IDX
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PAOverhead_IDX <> 0
           and PAQtyQ >= 0
  group by PAOverhead_IDX
  
  insert into #temp1
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   6,
           isnull(sum(PATOTALOVERH),0) * (-1),
           0,
           isnull(sum(PAORIGTOTOVRHD),0) * (-1),
           0,
           PAOverhead_IDX
  from     PA10001 (nolock)
  where    PATSNO = @I_vPATSNO
           and PAOverhead_IDX <> 0
           and PAQtyQ <= 0
  group by PAOverhead_IDX
  
  insert into #temp
             (DTYPE,
              DTAMT,
              CTAMT,
              ORDTAMT,
              ORCTAMT,
              DTINDEX)
  select   DTYPE,
           isnull(sum(DTAMT - CTAMT),0),
           isnull(sum(CTAMT - DTAMT),0),
           isnull(sum(ORDTAMT - ORCTAMT),0),
           isnull(sum(ORCTAMT - ORDTAMT),0),
           DTINDEX
  from     #temp1 (nolock)
  where    DTINDEX <> 0
  group by DTYPE,DTINDEX
  
  update #temp
  set    CTAMT = 0.00,
         ORCTAMT = 0.00
  where  CTAMT < 0
  
  update #temp
  set    DTAMT = 0.00,
         ORDTAMT = 0.00
  where  DTAMT < 0
  
  delete #temp
  where  DTAMT + CTAMT = 0.00
          or DTAMT - CTAMT = 0.00
  
  declare DistLine INSENSITIVE cursor  for
  select DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX
  from   #temp
  
  open DistLine
  
  select @iCursorError = @@cursor_rows
  
  if (@iCursorError > 0)
    begin
      fetch next from DistLine
      into @cDTYPE,
           @cDTAMT,
           @cCTAMT,
           @ORDTAMT,
           @ORCTAMT,
           @cINDEX
      
      while (@@fetch_status <> -1)
        begin
          if (@@fetch_status = -2)
            begin
              select @O_iErrorState = 1812
              
              break
            end
          
          exec @iStatus = taPATSDistributionInsert
            @I_vCURNCYID ,
            @I_vCURRNIDX ,
            @ORCTAMT ,
            @ORDTAMT ,
            @I_vPATSNO ,
            '' ,
            0 ,
            @cCTAMT ,
            @cDTAMT ,
            @cINDEX ,
            @cDTYPE ,
            '' ,
            '' ,
            @I_vEMPLOYID ,
            @O_oErrorState output
          
          select @iError = @@error
          
          if @iStatus = 0
             and @iError <> 0
            select @iStatus = @iError
          
          if (@iStatus <> 0)
              or (@O_oErrorState <> 0)
            begin
              if (@iStatus <> 0)
                begin
                  select @O_iErrorState = 1813
                end
              else
                begin
                  select @O_iErrorState = @O_oErrorState
                end
              
              deallocate DistLine
              
              return (@O_iErrorState)
            end
          
          fetch next from DistLine
          into @cDTYPE,
               @cDTAMT,
               @cCTAMT,
               @ORDTAMT,
               @ORCTAMT,
               @cINDEX
        end
    end
  
  deallocate DistLine
  
  return (@O_iErrorState)

