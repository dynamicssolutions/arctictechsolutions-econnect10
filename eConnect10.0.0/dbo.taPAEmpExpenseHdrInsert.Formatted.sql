

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAEmpExpenseHdrInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAEmpExpenseHdrInsert]
  
create procedure [dbo].[taPAEmpExpenseHdrInsert]
                @I_vPAertrxtype           smallint,
                @I_vPAerdocnumber         char(17),
                @I_vPADOCDT               datetime,
                @I_vBACHNUMB              char(15),
                @I_vEMPLOYID              char(15),
                @I_vPAVENADDRESSID        char(15),
                @I_vPAStartDate           datetime,
                @I_vPAEnDate              datetime,
                @I_vCURNCYID              char(15),
                @I_vUSERID                char(15)  = '',
                @I_vPAREFNO               char(17)  = '',
                @I_vSHIPMTHD              char(15)  = '',
                @I_vPAUD1                 char(21)  = '',
                @I_vPAUD2                 char(21)  = '',
                @I_vPAreptsuff            char(5)  = '',
                @I_vPACOMM                char(51)  = '',
                @I_vPATQTY                numeric(19,5)  = 0,
                @I_vPAREIMBURSTAXAMT      numeric(19,5)  = 0,
                @I_vBKTPURAM              numeric(19,5)  = 0,
                @I_vPATACRV               numeric(19,5)  = 0,
                @I_vTRDISAMT              numeric(19,5)  = 0,
                @I_vPAFreight_Taxable_P   smallint  = 0,
                @I_vFRTSCHID              char(15)  = '',
                @I_vFRTAMNT               numeric(19,5)  = 0,
                @I_vFRTTXAMT              numeric(19,5)  = 0,
                @I_vPAMisc_Taxable_P      smallint  = 0,
                @I_vMSCSCHID              char(15)  = '',
                @I_vMSCCHAMT              numeric(19,5)  = 0,
                @I_vMSCTXAMT              numeric(19,5)  = 0,
                @I_vDOCAMNT               numeric(19,5)  = 0,
                @I_vTEN99AMNT             numeric(19,5)  = 0,
                @I_vUN1099AM              numeric(19,5)  = 0,
                @I_vCASHAMNT              numeric(19,5)  = 0,
                @I_vCAMCBKID              char(15)  = '',
                @I_vCDOCNMBR              char(21)  = '',
                @I_vCAMTDATE              datetime  = '',
                @I_vCAMPMTNM              char(21)  = '',
                @I_vCHRGAMNT              numeric(19,5)  = 0,
                @I_vCHEKAMNT              numeric(19,5)  = 0,
                @I_vCHEKNMBR              char(21)  = '',
                @I_vCHEKDATE              datetime  = '',
                @I_vCAMPYNBR              char(21)  = '',
                @I_vCHAMCBID              char(15)  = '',
                @I_vCARDNAME              char(15)  = '',
                @I_vCRCRDAMT              numeric(19,5)  = 0,
                @I_vCRCARDDT              datetime  = '',
                @I_vCCAMPYNM              char(21)  = '',
                @I_vCCRCTNUM              char(21)  = '',
                @I_vTAXSCHID              char(15)  = '',
                @I_vTAXAMNT               numeric(19,5)  = 0,
                @I_vBCKTXAMT              numeric(19,5)  = 0,
                @I_vPAReimbursableAmount  numeric(19,5)  = 0,
                @I_vPAPD                  datetime  = '',
                @I_vPApostoDynPM          tinyint  = 1,
                @I_vTax_Date              datetime  = '',
                @I_vTaxInvRecvd           tinyint  = 0,
                @I_vTaxInvReqd            tinyint  = 0,
                @I_vUSINGHEADERLEVELTAXES smallint  = 0,
                @I_vXCHGRATE              numeric(19,7)  = 0,
                @I_vRATETPID              char(15)  = '',
                @I_vEXPNDATE              datetime  = '',
                @I_vEXCHDATE              datetime  = '',
                @I_vEXGTBDSC              char(30)  = '',
                @I_vEXTBLSRC              char(50)  = '',
                @I_vRATEEXPR              smallint  = -1,
                @I_vDYSTINCR              smallint  = -1,
                @I_vRATEVARC              numeric(19,7)  = 0,
                @I_vTRXDTDEF              smallint  = -1,
                @I_vRTCLCMTD              smallint  = -1,
                @I_vPRVDSLMT              smallint  = 0,
                @I_vDATELMTS              smallint  = 0,
                @I_vTIME1                 datetime  = '',
                @I_vPDK_EE_No             char(31)  = '',
                @I_vRequesterTrx          smallint  = 0,
                @I_vUSRDEFND1             char(50)  = '',
                @I_vUSRDEFND2             char(50)  = '',
                @I_vUSRDEFND3             char(50)  = '',
                @I_vUSRDEFND4             varchar(8000)  = '',
                @I_vUSRDEFND5             varchar(8000)  = '',
                @O_iErrorState            int  output,
                @oErrString               varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @PAertrxtype smallint,  @PAerdocnumber char (17),  @PDK_EE_No char (31),  @PADOCDT datetime ,  @USERID char (15),  @BACHNUMB char (15),  @BCHSOURC char (15),  @TRXSORCE char (13),  @PAREFNO char (17),  @SHIPMTHD char (15),  @PAUD1 char (21),  @PAUD2 char (21),  @EMPLOYID char (15),  @PAVENADDRESSID char (15),  @PAStartDate datetime ,  @PAEnDate datetime ,  @PAreptsuff char (5),  @PAYR smallint,  @PYMTRMID char (21),  @DUEDATE datetime ,  @DSCDLRAM numeric (19,5),  @ORDDLRAT numeric (19,5),  @DISCDATE datetime ,  @DISAMTAV numeric (19,5),  @ODISAMTAV numeric (19,5),  @PRCTDISC smallint,  @PACOMM char (51),  @CURNCYID char (15),  @CURRNIDX smallint,  @PATQTY numeric (19,5),  @PAEXTCOST numeric (19,5),  @OREXTCST numeric (19,5),  @PAtotcosts numeric (19,5),  @PAORIGTOTCOSTS numeric (19,5),  @PAREIMBURSTAXAMT numeric (19,5),  @PAORIGREIMTAXAMT numeric (19,5),  @BKTPURAM numeric (19,5),  @PATACRV numeric (19,5),  @PAORIACCRREV numeric (19,5),  @TRDISAMT numeric (19,5),  @ORTDISAM numeric (19,5),  @PAFreight_Taxable_P smallint,  @FRTSCHID char (15),  @FRTAMNT numeric (19,5),  @ORFRTAMT numeric (19,5),  @FRTTXAMT numeric (19,5),  @ORFRTTAX numeric (19,5),  @PAMisc_Taxable_P smallint,  @MSCSCHID char (15),  @MSCCHAMT numeric (19,5),  @OMISCAMT numeric (19,5),  @MSCTXAMT numeric (19,5),  @ORMSCTAX numeric (19,5),  @DOCAMNT numeric (19,5),  @ORDOCAMT numeric (19,5),  @TEN99AMNT numeric (19,5),  @OR1099AM numeric (19,5),  @UN1099AM numeric (19,5),  @CASHAMNT numeric (19,5),  @ORCASAMT numeric (19,5),  @CAMCBKID char (15),  @CDOCNMBR char (21),  @CAMTDATE datetime ,  @CAMPMTNM char (21),  @CHRGAMNT numeric (19,5),  @OCHGAMT numeric (19,5),  @CHEKAMNT numeric (19,5),  @ORCHKAMT numeric (19,5),  @CHEKNMBR char (21),  @CHEKDATE datetime ,  @CAMPYNBR char (21),  @CHAMCBID char (15),  @CARDNAME char (15),  @CRCRDAMT numeric (19,5),  @ORCCDAMT numeric (19,5),  @CRCARDDT datetime ,  @CCAMPYNM char (21),  @CCRCTNUM char (21),  @DISTKNAM numeric (19,5),  @ORDISTKN numeric (19,5),  @ORDAVFRT numeric (19,5),  @ODAVPUR numeric (19,5),  @ORDAVMSC numeric (19,5),  @TAXSCHID char (15),  @TAXAMNT numeric (19,5),  @ORTAXAMT numeric (19,5),  @BCKTXAMT numeric (19,5),  @OBTAXAMT numeric (19,5),  @PAReimbursableAmount numeric (19,5),  @PAOrigReimbursableAmt numeric (19,5),  @PAPD datetime ,  @RATETPID char (15),  @EXGTBLID char (15),  @XCHGRATE numeric (19,5),  @EXCHDATE datetime ,  @TIME1 datetime ,  @RTCLCMTD smallint,  @DENXRATE numeric (19,5),  @MCTRXSTT smallint,  @PA_EE_HDR_Errors binary (4),  @PA_EE_Dist_Errors binary (4),  @PA_MC_EE_Posting_Errors binary (4),  @PApostoDynPM tinyint ,  @Tax_Date datetime ,  @NOTEINDX numeric (19,5),  @TaxInvRecvd tinyint ,  @TaxInvReqd tinyint,  @iCustomState int,  @iCustomErrString varchar(255),  @iStatement int,     @iStatus int,      @iAddBatchErrState int,     @iAddShippingErrState int,    @iAddShippingErrString varchar(255),   @iCreateBatchErrString varchar(255),   @iAddCodeErrState int,     @iError int,       @iUpdDistErrState int,     @iUpdtBthErrState int,  @O_oErrorState int,  @DBName char(50),     @CreditCardVendorID char(15),    @O_iInitErrorState int,     @oInitErrString varchar(255),    @iAddErrState int,  @userdate datetime,  @l_Period int,  @fClosed int,  @nErr int,  @TAXAMNTL numeric(19,5),  @TAXAMNTH numeric(19,5),  @YEAR1 int,  @iGetNextNoteIdxErrState int,   @sCompanyID int,  @FUNLCURR char(15),  @DECPLCUR int,  @ISMCTRX int,  @iCursorError int,  @CUSTNMBR char(15),  @sumtotalqty numeric(19,5),  @sumtotalcost numeric(19,5),  @sumttotaloverhead numeric(19,5),  @sumtotalprofit numeric(19,5),  @sumaccruedrevenues numeric(19,5),  @sumtax numeric(19,5),  @PACONTNUMBER char(11),  @PAPROJNUMBER char(15),  @PACOSTCATID char(15),  @PATU smallint,  @PADT datetime,  @PAProjectType smallint,  @PAAcctgMethod smallint,  @RNDDIFF int,  @ADUPINNM smallint,  @O_glCalculatePeriodiErrorState int,  @CHEKBKID char(15),  @VENDORID char(15),  @linecount int,  @tempNOTEINDX int,  @UPSTDTFR smallint,  @iCalculateGLPeriodErrState int,  @PAINCPRCHTXPRJCST int,  @PAQtyQ numeric(19,5),  @PATotalProfit  numeric(19,5),  @PAACREV numeric(19,5),  @UOMSCHDL char(11),  @PAUnit_of_Measure  char(9),  @PAitemnumber  char(31),  @PAbllngtype smallint,  @PA_Line_Update_Totals_TablesErrorState int,  @PATempTotCosts numeric(19,5),   @TEN99TYPE smallint,     @TEN99BOXNUMBER smallint,    @BATCHTOTAL numeric(19,5)
  
  select @PAertrxtype = 0,
         @PAerdocnumber = '',
         @PDK_EE_No = '',
         @PADOCDT = '',
         @USERID = '',
         @BACHNUMB = '',
         @BCHSOURC = 'PA_EE',
         @TRXSORCE = '',
         @PAREFNO = '',
         @SHIPMTHD = '',
         @PAUD1 = '',
         @PAUD2 = '',
         @EMPLOYID = '',
         @PAVENADDRESSID = '',
         @PAStartDate = '',
         @PAEnDate = '',
         @PAreptsuff = '',
         @PAYR = 0,
         @PYMTRMID = '',
         @DUEDATE = '',
         @DSCDLRAM = 0,
         @ORDDLRAT = 0,
         @DISCDATE = '',
         @DISAMTAV = 0,
         @ODISAMTAV = 0,
         @PRCTDISC = 0,
         @PACOMM = '',
         @CURNCYID = '',
         @CURRNIDX = 0,
         @PATQTY = 0,
         @PAEXTCOST = 0,
         @OREXTCST = 0,
         @PAtotcosts = 0,
         @PAORIGTOTCOSTS = 0,
         @PAREIMBURSTAXAMT = 0,
         @PAORIGREIMTAXAMT = 0,
         @BKTPURAM = 0,
         @PATACRV = 0,
         @PAORIACCRREV = 0,
         @TRDISAMT = 0,
         @ORTDISAM = 0,
         @PAFreight_Taxable_P = 0,
         @FRTSCHID = '',
         @FRTAMNT = 0,
         @ORFRTAMT = 0,
         @FRTTXAMT = 0,
         @ORFRTTAX = 0,
         @PAMisc_Taxable_P = 0,
         @MSCSCHID = '',
         @MSCCHAMT = 0,
         @OMISCAMT = 0,
         @MSCTXAMT = 0,
         @ORMSCTAX = 0,
         @DOCAMNT = 0,
         @ORDOCAMT = 0,
         @TEN99AMNT = 0,
         @OR1099AM = 0,
         @UN1099AM = 0,
         @CASHAMNT = 0,
         @ORCASAMT = 0,
         @CAMCBKID = '',
         @CDOCNMBR = '',
         @CAMTDATE = '',
         @CAMPMTNM = '',
         @CHRGAMNT = 0,
         @OCHGAMT = 0,
         @CHEKAMNT = 0,
         @ORCHKAMT = 0,
         @CHEKNMBR = '',
         @CHEKDATE = '',
         @CAMPYNBR = '',
         @CHAMCBID = '',
         @CARDNAME = '',
         @CRCRDAMT = 0,
         @ORCCDAMT = 0,
         @CRCARDDT = '',
         @CCAMPYNM = '',
         @CCRCTNUM = '',
         @DISTKNAM = 0,
         @ORDISTKN = 0,
         @ORDAVFRT = 0,
         @ODAVPUR = 0,
         @ORDAVMSC = 0,
         @TAXSCHID = '',
         @TAXAMNT = 0,
         @ORTAXAMT = 0,
         @BCKTXAMT = 0,
         @OBTAXAMT = 0,
         @PAReimbursableAmount = 0,
         @PAOrigReimbursableAmt = 0,
         @PAPD = '',
         @RATETPID = '',
         @EXGTBLID = '',
         @XCHGRATE = 0,
         @EXCHDATE = '',
         @TIME1 = '',
         @RTCLCMTD = 0,
         @DENXRATE = 0,
         @MCTRXSTT = 0,
         @PA_EE_HDR_Errors = 0,
         @PA_EE_Dist_Errors = 0,
         @PA_MC_EE_Posting_Errors = 0,
         @PApostoDynPM = 1,
         @Tax_Date = '',
         @NOTEINDX = 0,
         @TaxInvRecvd = 0,
         @TaxInvReqd = 0,
         @iStatus = 0,
         @oErrString = '',
         @O_oErrorState = 0,
         @iStatement = 0,
         @O_iErrorState = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @iAddBatchErrState = 0,
         @iAddShippingErrState = 0,
         @iAddShippingErrString = '',
         @iCreateBatchErrString = '',
         @CURRNIDX = 0,
         @iAddCodeErrState = 0,
         @iError = 0,
         @iUpdDistErrState = 0,
         @DBName = '',
         @CreditCardVendorID = '',
         @O_iInitErrorState = 0,
         @oInitErrString = '',
         @iUpdtBthErrState = 0,
         @iAddErrState = 0,
         @userdate = getdate(),
         @l_Period = 0,
         @fClosed = 0,
         @nErr = 0,
         @TAXAMNTL = 0,
         @TAXAMNTH = 0,
         @YEAR1 = 0,
         @iGetNextNoteIdxErrState = 0,
         @sCompanyID = 0,
         @FUNLCURR = '',
         @DECPLCUR = 0,
         @ISMCTRX = 0,
         @iCursorError = 0,
         @CUSTNMBR = '',
         @sumtotalqty = 0,
         @sumtotalcost = 0,
         @sumttotaloverhead = 0,
         @sumtotalprofit = 0,
         @sumaccruedrevenues = 0,
         @sumtax = 0,
         @PACONTNUMBER = '',
         @PAPROJNUMBER = '',
         @PACOSTCATID = '',
         @PATU = 0,
         @PADT = '',
         @PAProjectType = 0,
         @PAAcctgMethod = 0,
         @RNDDIFF = 0,
         @ADUPINNM = 0,
         @O_glCalculatePeriodiErrorState = 0,
         @CHEKBKID = '',
         @VENDORID = '',
         @linecount = 0,
         @tempNOTEINDX = 0,
         @UPSTDTFR = 0,
         @iCalculateGLPeriodErrState = 0,
         @PAINCPRCHTXPRJCST = 0,
         @PAQtyQ = 0,
         @PATotalProfit = 0,
         @PAACREV = 0,
         @UOMSCHDL = '',
         @PAUnit_of_Measure = '',
         @PAitemnumber = '',
         @PAbllngtype = 0,
         @PA_Line_Update_Totals_TablesErrorState = 0,
         @TEN99TYPE = 0,
         @TEN99BOXNUMBER = 0,
         @BATCHTOTAL = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taPAEmpExpenseHdrInsertPre
    @I_vPAertrxtype output ,
    @I_vPAerdocnumber output ,
    @I_vPADOCDT output ,
    @I_vBACHNUMB output ,
    @I_vEMPLOYID output ,
    @I_vPAVENADDRESSID output ,
    @I_vPAStartDate output ,
    @I_vPAEnDate output ,
    @I_vCURNCYID output ,
    @I_vUSERID output ,
    @I_vPAREFNO output ,
    @I_vSHIPMTHD output ,
    @I_vPAUD1 output ,
    @I_vPAUD2 output ,
    @I_vPAreptsuff output ,
    @I_vPACOMM output ,
    @I_vPATQTY output ,
    @I_vPAREIMBURSTAXAMT output ,
    @I_vBKTPURAM output ,
    @I_vPATACRV output ,
    @I_vTRDISAMT output ,
    @I_vPAFreight_Taxable_P output ,
    @I_vFRTSCHID output ,
    @I_vFRTAMNT output ,
    @I_vFRTTXAMT output ,
    @I_vPAMisc_Taxable_P output ,
    @I_vMSCSCHID output ,
    @I_vMSCCHAMT output ,
    @I_vMSCTXAMT output ,
    @I_vDOCAMNT output ,
    @I_vTEN99AMNT output ,
    @I_vUN1099AM output ,
    @I_vCASHAMNT output ,
    @I_vCAMCBKID output ,
    @I_vCDOCNMBR output ,
    @I_vCAMTDATE output ,
    @I_vCAMPMTNM output ,
    @I_vCHRGAMNT output ,
    @I_vCHEKAMNT output ,
    @I_vCHEKNMBR output ,
    @I_vCHEKDATE output ,
    @I_vCAMPYNBR output ,
    @I_vCHAMCBID output ,
    @I_vCARDNAME output ,
    @I_vCRCRDAMT output ,
    @I_vCRCARDDT output ,
    @I_vCCAMPYNM output ,
    @I_vCCRCTNUM output ,
    @I_vTAXSCHID output ,
    @I_vTAXAMNT output ,
    @I_vBCKTXAMT output ,
    @I_vPAReimbursableAmount output ,
    @I_vPAPD output ,
    @I_vPApostoDynPM output ,
    @I_vTax_Date output ,
    @I_vTaxInvRecvd output ,
    @I_vTaxInvReqd output ,
    @I_vUSINGHEADERLEVELTAXES output ,
    @I_vXCHGRATE output ,
    @I_vRATETPID output ,
    @I_vEXPNDATE output ,
    @I_vEXCHDATE output ,
    @I_vEXGTBDSC output ,
    @I_vEXTBLSRC output ,
    @I_vRATEEXPR output ,
    @I_vDYSTINCR output ,
    @I_vRATEVARC output ,
    @I_vTRXDTDEF output ,
    @I_vRTCLCMTD output ,
    @I_vPRVDSLMT output ,
    @I_vDATELMTS output ,
    @I_vTIME1 output ,
    @I_vPDK_EE_No output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1827
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAertrxtype is NULL 
       or @I_vPAerdocnumber is NULL
       or @I_vPADOCDT is NULL
       or @I_vBACHNUMB is NULL
       or @I_vEMPLOYID is NULL
       or @I_vPAVENADDRESSID is NULL
       or @I_vPAStartDate is NULL
       or @I_vPAEnDate is NULL
       or @I_vCURNCYID is NULL)
    begin
      select @O_iErrorState = 1828
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAertrxtype = 0
       or @I_vPAerdocnumber = ''
       or @I_vPADOCDT = ''
       or @I_vBACHNUMB = ''
       or @I_vEMPLOYID = ''
       or @I_vPAVENADDRESSID = ''
       or @I_vPAStartDate = ''
       or @I_vPAEnDate = ''
       or @I_vCURNCYID = '')
    begin
      select @O_iErrorState = 1829
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPATQTY < 0
       or @I_vPAREIMBURSTAXAMT < 0
       or @I_vBKTPURAM < 0
       or @I_vPATACRV < 0
       or @I_vTRDISAMT < 0
       or @I_vFRTAMNT < 0
       or @I_vFRTTXAMT < 0
       or @I_vMSCCHAMT < 0
       or @I_vMSCTXAMT < 0
       or @I_vDOCAMNT < 0
       or @I_vTEN99AMNT < 0
       or @I_vUN1099AM < 0
       or @I_vCASHAMNT < 0
       or @I_vCHRGAMNT < 0
       or @I_vCHEKAMNT < 0
       or @I_vCRCRDAMT < 0
       or @I_vTAXAMNT < 0
       or @I_vBCKTXAMT < 0
       or @I_vPAReimbursableAmount < 0)
    begin
      select @O_iErrorState = 1830
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vEMPLOYID = UPPER(@I_vEMPLOYID),
         @I_vCURNCYID = UPPER(@I_vCURNCYID),
         @I_vPAerdocnumber = UPPER(@I_vPAerdocnumber),
         @I_vPAREFNO = UPPER(@I_vPAREFNO),
         @I_vPAVENADDRESSID = UPPER(@I_vPAVENADDRESSID),
         @I_vSHIPMTHD = UPPER(@I_vSHIPMTHD),
         @I_vFRTSCHID = UPPER(@I_vFRTSCHID),
         @I_vMSCSCHID = UPPER(@I_vMSCSCHID),
         @I_vCAMCBKID = UPPER(@I_vCAMCBKID),
         @I_vCHAMCBID = UPPER(@I_vCHAMCBID),
         @I_vTAXSCHID = UPPER(@I_vTAXSCHID)
  
  if ((@I_vPAertrxtype < 1)
       or (@I_vPAertrxtype > 2))
    begin
      select @O_iErrorState = 1831
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAertrxtype = 1)
    select @I_vPAREFNO = ''
  
  select @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 c (nolock)
  where  c.INTERID = db_name()
  
  if (@I_vUSERID = '')
    select @I_vUSERID = 'eConnect'
  
  if (@I_vTax_Date = '')
    select @I_vTax_Date = @I_vPADOCDT
  
  select @UPSTDTFR = UPSTDTFR
  from   SY02300 (nolock)
  WHERE  SERIES = 7
         AND TRXSOURC = 'Employee Expense Entry'
  
  if (@UPSTDTFR = 0)
    begin
      select @I_vPAPD = GLPOSTDT
      from   SY00500 (nolock)
      where  SERIES = 7
             and BACHNUMB = @I_vBACHNUMB
             and BCHSOURC = 'PA_EE'
    end
  else
    begin
      select @I_vPAPD = @I_vPADOCDT
    end
  
  select @ADUPINNM = isnull(ADUPINNM,0),
         @CHEKBKID = isnull(CHEKBKID,'')
  from   PM40100 (nolock)
  where  UNIQKEY = '1'
  
  select @PAMisc_Taxable_P = PAMisc_Taxable_P,
         @MSCSCHID = MSCSCHID,
         @PAFreight_Taxable_P = PAFreight_Taxable_P,
         @FRTSCHID = FRTSCHID
  from   PA42401
  where  PAsetupkey = 1
  
  if (@I_vPAFreight_Taxable_P = 0)
    select @I_vPAFreight_Taxable_P = @PAFreight_Taxable_P,
           @I_vFRTSCHID = @FRTSCHID
  
  if (@I_vPAMisc_Taxable_P = 0)
    select @I_vPAMisc_Taxable_P = @PAMisc_Taxable_P,
           @I_vMSCSCHID = @MSCSCHID
  
  if (@I_vEMPLOYID <> '')
    begin
      if not exists (select 1
                     from   UPR00100 (nolock)
                     where  EMPLOYID = @I_vEMPLOYID)
        begin
          select @O_iErrorState = 1832
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if not exists (select 1
                     from   PM00200 (nolock)
                     where  VENDORID = @I_vEMPLOYID)
        begin
          select @O_iErrorState = 1833
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if not exists (select 1
                     from   PA00601 (nolock)
                     where  EMPLOYID = @I_vEMPLOYID)
        begin
          select @O_iErrorState = 1834
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if exists (select 1
                 from   PA00601 (nolock)
                 where  EMPLOYID = @I_vEMPLOYID
                        and PAfileemplreim = 0)
        begin
          select @O_iErrorState = 6457
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if exists (select 1
                 from   PA10501 (nolock)
                 where  PAerdocnumber = @I_vPAerdocnumber
                        and EMPLOYID <> @I_vEMPLOYID)
        begin
          select @O_iErrorState = 6475
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @SHIPMTHD = isnull(SHIPMTHD,''),
         @TEN99TYPE = TEN99TYPE,
         @TEN99BOXNUMBER = TEN99BOXNUMBER
  from   PM00200 (nolock)
  where  VENDORID = @I_vEMPLOYID
  
  if (@I_vSHIPMTHD = '')
    select @I_vSHIPMTHD = @SHIPMTHD
  
  if (@I_vPAertrxtype = 2)
    begin
      if (@I_vPAREFNO = '')
        begin
          select @O_iErrorState = 1835
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if not exists (select 1
                         from   PA30501 (nolock)
                         where  PAerdocnumber = @I_vPAREFNO)
            begin
              select @O_iErrorState = 1836
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if (@I_vCURNCYID <> '')
    begin
      select @CURRNIDX = isnull(CURRNIDX,0)
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @I_vCURNCYID
      
      if (@CURRNIDX = 0)
        begin
          select @O_iErrorState = 1837
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vRATETPID <> '')
    begin
      if (not exists (select 1
                      from   MC40100 (nolock)
                      where  RATETPID = @I_vRATETPID))
        begin
          select @O_iErrorState = 1838
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAVENADDRESSID <> '')
    begin
      if (not exists (select 1
                      from   PM00300 (nolock)
                      where  VENDORID = @I_vEMPLOYID
                             and ADRSCODE = @I_vPAVENADDRESSID))
        begin
          select @O_iErrorState = 1839
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAStartDate <> '')
    begin
      execute @iStatus = glCalculateGLPeriod
        7 ,
        'Employee Expense Entry' ,
        @I_vPAStartDate ,
        @userdate ,
        @l_Period output ,
        @fClosed output ,
        @PAYR output ,
        @nErr output ,
        @O_iErrorState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        select @iStatus = @iError
      
      if (@iStatus <> 0)
          or (@O_iErrorState <> 0)
        begin
          if (@O_iErrorState <> 0)
            begin
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          select @O_iErrorState = 1840
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@nErr <> 0)
        begin
          select @O_iErrorState = 1841
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (exists (select 1
              from   PA10500 (nolock)
              where  EMPLOYID = @I_vEMPLOYID
                     and PAStartDate = @I_vPAStartDate
                     and PAEnDate = @I_vPAEnDate
                     and PAYR = @PAYR
                     and PAreptsuff = @I_vPAreptsuff))
    begin
      select @O_iErrorState = 1842
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (exists (select 1
              from   PA30500 (nolock)
              where  PAerdocnumber = @I_vPAerdocnumber
                     and EMPLOYID = @I_vEMPLOYID
                     and PAStartDate = @I_vPAStartDate
                     and PAEnDate = @I_vPAEnDate
                     and PAYR = @PAYR
                     and PAreptsuff = @I_vPAreptsuff))
    begin
      select @O_iErrorState = 1843
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if exists (select 1
             from   PA01901 (nolock)
             where  PADocnumber20 = @I_vPAerdocnumber
                    and PATranType = 5)
    begin
      select @O_iErrorState = 4004
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @ADUPINNM = 1
      or @ADUPINNM = 2
    begin
      if exists (select 1
                 from   PM00400 a (nolock)
                 where  a.CNTRLTYP = 0
                        and a.CNTRLNUM = @I_vPAerdocnumber)
        begin
          select @O_iErrorState = 5002
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vSHIPMTHD <> '')
    begin
      if (not exists (select 1
                      from   SY03000 (nolock)
                      where  SHIPMTHD = @I_vSHIPMTHD))
        begin
          select @O_iErrorState = 6460
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vTAXSCHID <> '')
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vTAXSCHID)
        begin
          select @O_iErrorState = 1845
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      if @I_vTAXAMNT > 0.00
        begin
          select @O_iErrorState = 1846
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @TAXAMNTL = (ISNULL(sum(TAXAMNT),0.00))
  from   PA10502 a (nolock)
  where  a.PAerdocnumber = @I_vPAerdocnumber
         and LNITMSEQ = 0
  
  if (@TAXAMNTL <> @I_vTAXAMNT)
    begin
      select @O_iErrorState = 1847
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vUSINGHEADERLEVELTAXES = 0
    begin
      select @TAXAMNTH = (ISNULL(sum(TAXAMNT),0.00))
      from   PA10502 a (nolock)
      where  a.PAerdocnumber = @I_vPAerdocnumber
             and LNITMSEQ <> 0
      
      if @TAXAMNTH <> @I_vTAXAMNT
        begin
          select @O_iErrorState = 1848
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAMisc_Taxable_P < 0
       or @I_vPAMisc_Taxable_P > 3)
    begin
      select @O_iErrorState = 1849
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    begin
      if (@I_vPAMisc_Taxable_P <> 1)
        select @I_vMSCSCHID = ''
    end
  
  if (@I_vPAFreight_Taxable_P < 0
       or @I_vPAFreight_Taxable_P > 3)
    begin
      select @O_iErrorState = 1850
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    begin
      if (@I_vPAFreight_Taxable_P <> 1)
        select @I_vFRTSCHID = ''
    end
  
  if (@I_vFRTSCHID <> '')
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vFRTSCHID)
        begin
          select @O_iErrorState = 1851
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      if @I_vFRTTXAMT > 0.00
        begin
          select @O_iErrorState = 1852
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @FRTTXAMT = ISNULL(sum(FRTTXAMT),0),
         @ORFRTTAX = ISNULL(sum(ORFRTTAX),0)
  from   PA10502 a (nolock)
  where  a.PAerdocnumber = @I_vPAerdocnumber
         and LNITMSEQ = 2147483646
  
  if ((@I_vFRTTXAMT <> @FRTTXAMT)
       or (@I_vFRTTXAMT <> @ORFRTTAX))
    begin
      select @O_iErrorState = 1853
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vMSCSCHID <> '')
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vMSCSCHID)
        begin
          select @O_iErrorState = 1854
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      if @I_vMSCTXAMT > 0.00
        begin
          select @O_iErrorState = 1855
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @MSCTXAMT = ISNULL(sum(MSCTXAMT),0),
         @ORMSCTAX = ISNULL(sum(ORMSCTAX),0)
  from   PA10502 a (nolock)
  where  a.PAerdocnumber = @I_vPAerdocnumber
         and LNITMSEQ = 2147483645
  
  if ((@I_vMSCTXAMT <> @MSCTXAMT)
       or (@I_vMSCTXAMT <> @ORMSCTAX))
    begin
      select @O_iErrorState = 1856
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vCAMCBKID <> '')
    begin
      if (not exists (select 1
                      from   CM00100 (nolock)
                      where  CHEKBKID = @I_vCAMCBKID))
        begin
          select @O_iErrorState = 1857
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vCHAMCBID <> '')
    begin
      if (not exists (select 1
                      from   CM00100 (nolock)
                      where  CHEKBKID = @I_vCHAMCBID))
        begin
          select @O_iErrorState = 1858
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if ((@I_vCASHAMNT > 0)
      and (@I_vCAMCBKID = ''))
    begin
      select @O_iErrorState = 1859
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCASHAMNT > 0)
      and (@I_vCAMTDATE = ''))
    begin
      select @O_iErrorState = 1860
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCASHAMNT > 0)
      and (@I_vCDOCNMBR = ''))
    begin
      select @O_iErrorState = 1861
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCASHAMNT > 0)
      and (@I_vCAMPMTNM = ''))
    begin
      select @O_iErrorState = 1862
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCASHAMNT > 0)
      and (@I_vCAMPMTNM <> ''))
    begin
      if (exists (select 1
                  from   PM00400 (nolock)
                  where  CNTRLTYP = 1
                         and CNTRLNUM = @I_vCAMPMTNM))
        begin
          select @O_iErrorState = 1863
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if ((@I_vCASHAMNT = 0)
      and (@I_vCAMCBKID <> ''
            or @I_vCAMPMTNM <> ''
            or @I_vCAMTDATE <> ''
            or @I_vCDOCNMBR <> ''))
    begin
      select @O_iErrorState = 1864
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCHEKAMNT > 0)
      and (@I_vCHAMCBID = ''))
    begin
      select @O_iErrorState = 1865
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCHEKAMNT > 0)
      and (@I_vCHEKDATE = ''))
    begin
      select @O_iErrorState = 1866
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCHEKAMNT > 0)
      and (@I_vCAMPYNBR = ''))
    begin
      select @O_iErrorState = 1867
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCHEKAMNT > 0)
      and (@I_vCHEKNMBR = ''))
    begin
      select @O_iErrorState = 1868
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCHEKAMNT > 0)
      and (@I_vCAMPYNBR <> ''))
    begin
      if (exists (select 1
                  from   PM00400 (nolock)
                  where  CNTRLTYP = 1
                         and CNTRLNUM = @I_vCAMPYNBR))
        begin
          select @O_iErrorState = 1869
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if ((@I_vCHEKAMNT = 0)
      and (@I_vCHAMCBID <> ''
            or @I_vCHEKNMBR <> ''
            or @I_vCHEKDATE <> ''
            or @I_vCAMPYNBR <> ''))
    begin
      select @O_iErrorState = 1870
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCRCRDAMT > 0)
      and (@I_vCARDNAME = ''))
    begin
      select @O_iErrorState = 1871
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCRCRDAMT > 0)
      and (@I_vCRCARDDT = ''))
    begin
      select @O_iErrorState = 1872
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCRCRDAMT > 0)
      and (@I_vCCRCTNUM = ''))
    begin
      select @O_iErrorState = 1873
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCRCRDAMT > 0)
      and (@I_vCCAMPYNM = ''))
    begin
      select @O_iErrorState = 1874
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vCRCRDAMT > 0)
      and (@I_vCCAMPYNM <> ''))
    begin
      if (exists (select 1
                  from   PM00400 (nolock)
                  where  CNTRLTYP = 0
                         and CNTRLNUM = @I_vCCAMPYNM))
        begin
          select @O_iErrorState = 1875
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vCARDNAME <> '')
    begin
      select @CARDNAME = CARDNAME,
             @VENDORID = VENDORID
      from   SY03100 (nolock)
      where  CARDNAME = @I_vCARDNAME
      
      if (@CARDNAME = '')
        begin
          select @O_iErrorState = 1876
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @PATQTY = isnull(sum(PAQtyQ),0),
         @PAEXTCOST = isnull(sum(PAEXTCOST),0),
         @PAtotcosts = isnull(sum(PAtotcosts),0),
         @PAReimbursableAmount = isnull(sum(PAReimbursableAmount),0),
         @TEN99AMNT = case 
                        when (@TEN99TYPE <> 1
                              and @TEN99TYPE <> 5) then isnull((sum(PAReimbursableAmount) - sum(TAXAMNT)),
                                                               0)
                        else 0
                      end
  from   PA10501 (nolock)
  where  PAerdocnumber = @I_vPAerdocnumber
  
  select @DOCAMNT = ((@PAReimbursableAmount - @I_vTAXAMNT) + (@I_vFRTAMNT + @I_vMSCCHAMT + @I_vTAXAMNT)),
         @CHRGAMNT = (((@PAReimbursableAmount - @I_vTAXAMNT) + (@I_vFRTAMNT + @I_vMSCCHAMT + @I_vTAXAMNT)) - @I_vCASHAMNT) - (@I_vCHEKAMNT + @I_vCRCRDAMT),
         @BKTPURAM = (@PAReimbursableAmount - @I_vTAXAMNT - @I_vBCKTXAMT)
  
  if ((@I_vCASHAMNT > 0
        or @I_vCHEKAMNT > 0
        or @I_vCRCRDAMT > 0)
      and (@PAEXTCOST < 0))
    begin
      select @O_iErrorState = 9547
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPATQTY > 0)
      and (@I_vPATQTY <> @PATQTY))
    begin
      select @O_iErrorState = 1877
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    begin
      if ((@I_vPATQTY = 0)
          and (@PATQTY <> 0))
        select @I_vPATQTY = @PATQTY
    end
  
  if ((@I_vPAReimbursableAmount > 0)
      and (@I_vPAReimbursableAmount <> @PAReimbursableAmount))
    begin
      select @O_iErrorState = 743
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    begin
      if ((@I_vPAReimbursableAmount = 0)
          and (@PAReimbursableAmount <> 0))
        select @I_vPAReimbursableAmount = @PAReimbursableAmount
    end
  
  if ((@I_vDOCAMNT > 0)
      and (@I_vDOCAMNT <> @DOCAMNT))
    begin
      select @O_iErrorState = 1879
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    begin
      if ((@I_vDOCAMNT = 0)
          and (@DOCAMNT <> 0))
        select @I_vDOCAMNT = @DOCAMNT
    end
  
  if ((@I_vTEN99AMNT > 0)
      and (@I_vTEN99AMNT <> @TEN99AMNT))
    begin
      select @O_iErrorState = 1880
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    begin
      if ((@I_vTEN99AMNT = 0)
          and (@TEN99AMNT <> 0))
        select @I_vTEN99AMNT = @TEN99AMNT
    end
  
  if ((@I_vCHRGAMNT > 0)
      and (@I_vCHRGAMNT <> @CHRGAMNT))
    begin
      select @O_iErrorState = 1882
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    begin
      if ((@I_vCHRGAMNT = 0)
          and (@CHRGAMNT <> 0))
        select @I_vCHRGAMNT = @CHRGAMNT
    end
  
  if ((@I_vBKTPURAM > 0)
      and (@I_vBKTPURAM <> @BKTPURAM))
    begin
      select @O_iErrorState = 1998
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    begin
      if ((@I_vBKTPURAM = 0)
          and (@BKTPURAM <> 0))
        select @I_vBKTPURAM = @BKTPURAM
    end
  
  if (@I_vPAStartDate > @I_vPAEnDate)
    begin
      select @O_iErrorState = 6485
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if exists (select 1
             from   PA10501 (nolock)
             where  PAerdocnumber = @I_vPAerdocnumber
                    and ((PADT < @I_vPAStartDate)
                          or (PADT > @I_vPAEnDate)))
    begin
      select @O_iErrorState = 6476
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPADOCDT <> '')
    begin
      execute @iStatus = glCalculateGLPeriod
        7 ,
        'Timesheet Entry' ,
        @I_vPADOCDT ,
        @I_vPADOCDT ,
        @l_Period output ,
        @fClosed output ,
        @YEAR1 output ,
        @nErr output ,
        @iCalculateGLPeriodErrState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iCalculateGLPeriodErrState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCalculateGLPeriodErrState))
          
          select @O_iErrorState = 6458
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@fClosed = 1)
        begin
          select @O_iErrorState = 6459
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAPD <> '')
    begin
      select @l_Period = 0,
             @fClosed = 0,
             @YEAR1 = 0,
             @nErr = 0,
             @iCalculateGLPeriodErrState = 0
      
      execute @iStatus = glCalculateGLPeriod
        7 ,
        'Timesheet Entry' ,
        @I_vPAPD ,
        @I_vPAPD ,
        @l_Period output ,
        @fClosed output ,
        @YEAR1 output ,
        @nErr output ,
        @iCalculateGLPeriodErrState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iCalculateGLPeriodErrState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCalculateGLPeriodErrState))
          
          select @O_iErrorState = 6461
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@fClosed = 1)
        begin
          select @O_iErrorState = 6462
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vEXCHDATE <> '')
    begin
      select @l_Period = 0,
             @fClosed = 0,
             @YEAR1 = 0,
             @nErr = 0,
             @iCalculateGLPeriodErrState = 0
      
      execute @iStatus = glCalculateGLPeriod
        7 ,
        'Timesheet Entry' ,
        @I_vEXCHDATE ,
        @I_vEXCHDATE ,
        @l_Period output ,
        @fClosed output ,
        @YEAR1 output ,
        @nErr output ,
        @iCalculateGLPeriodErrState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iCalculateGLPeriodErrState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCalculateGLPeriodErrState))
          
          select @O_iErrorState = 6463
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@fClosed = 1)
        begin
          select @O_iErrorState = 6464
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@oErrString <> '')
      or (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  select @FUNLCURR = FUNLCURR
  from   MC40000 (nolock)
  
  select @DECPLCUR = DECPLCUR - 1
  from   DYNAMICS..MC40200 (nolock)
  where  CURNCYID = @FUNLCURR
  
  select @OREXTCST = @PAEXTCOST,
         @PAORIGTOTCOSTS = @PAtotcosts,
         @PAORIGREIMTAXAMT = @I_vPAREIMBURSTAXAMT,
         @ORTDISAM = @I_vTRDISAMT,
         @ORFRTAMT = @I_vFRTAMNT,
         @ORFRTTAX = @I_vFRTTXAMT,
         @OMISCAMT = @I_vMSCCHAMT,
         @ORMSCTAX = @I_vMSCTXAMT,
         @ORDOCAMT = @I_vDOCAMNT,
         @OR1099AM = @I_vTEN99AMNT,
         @ORCASAMT = @I_vCASHAMNT,
         @OCHGAMT = @I_vCHRGAMNT,
         @ORCHKAMT = @I_vCHEKAMNT,
         @ORCCDAMT = @I_vCRCRDAMT,
         @ORTAXAMT = @I_vTAXAMNT,
         @OBTAXAMT = @I_vBCKTXAMT,
         @PAOrigReimbursableAmt = @I_vPAReimbursableAmount
  
  if (@I_vCURNCYID <> '')
     and (@I_vCURNCYID <> @FUNLCURR)
    begin
      select @ISMCTRX = 1
      
      select @CURRNIDX = isnull(CURRNIDX,0)
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @I_vCURNCYID
      
      if (@CURRNIDX = 0)
        begin
          select @O_iErrorState = 1883
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      exec @iStatus = taMCCurrencyValidate
        @I_vMASTERID = @I_vEMPLOYID ,
        @I_vDOCDATE = @I_vPADOCDT ,
        @I_vCURNCYID = @I_vCURNCYID ,
        @I_vEXCHDATE = @I_vEXCHDATE ,
        @I_vEXGTBDSC = @I_vEXGTBDSC ,
        @I_vEXTBLSRC = @I_vEXTBLSRC ,
        @I_vRATEEXPR = @I_vRATEEXPR output ,
        @I_vDYSTINCR = @I_vDYSTINCR output ,
        @I_vRATEVARC = @I_vRATEVARC ,
        @I_vTRXDTDEF = @I_vTRXDTDEF ,
        @I_vPRVDSLMT = @I_vPRVDSLMT ,
        @I_vDATELMTS = @I_vDATELMTS ,
        @I_vMODULE = 0 ,
        @I_vTIME1 = @I_vTIME1 output ,
        @I_vXCHGRATE = @I_vXCHGRATE output ,
        @I_vEXPNDATE = @I_vEXPNDATE output ,
        @I_vRATETPID = @I_vRATETPID output ,
        @I_vRTCLCMTD = @I_vRTCLCMTD output ,
        @I_vEXGTBLID = @EXGTBLID output ,
        @oErrString = @oErrString output ,
        @O_iErrorState = @O_iErrorState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        select @iStatus = @iError
      
      if (@iStatus <> 0)
          or (@O_iErrorState <> 0)
        begin
          if (@O_iErrorState <> 0)
            begin
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          select @O_iErrorState = 1884
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 1885
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vEXPNDATE < @I_vPADOCDT)
        begin
          select @O_iErrorState = 6487
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      select @ISMCTRX = 0,
             @I_vRATEEXPR = 0,
             @I_vDYSTINCR = 0,
             @I_vRTCLCMTD = 0
      
      select @CURRNIDX = isnull(FUNCRIDX,0)
      from   MC40000 (nolock)
      where  MC40000.FUNLCURR = @I_vCURNCYID
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_EmployeeExpense' ,
        @I_vINDEX1 = @I_vPAerdocnumber ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 0 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iCustomState <> 0)
           or (@iError <> 0))
        begin
          select @O_iErrorState = 2031
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@ISMCTRX = 1)
    begin
      select @RNDDIFF = isnull(RNDDIFF,0)
      from   MC40301 (nolock)
      where  RATETPID = @I_vRATETPID
             and CURNCYID = @I_vCURNCYID
             and EXGTBLID = @EXGTBLID
      
      if (@RNDDIFF = 0)
        begin
          select @RNDDIFF = isnull(RNDDIFF,0)
          from   MC40201 (nolock)
          where  CURNCYID = @I_vCURNCYID
        end
      
      if (@RNDDIFF = 0)
        begin
          select @RNDDIFF = isnull(ACTINDX,0)
          from   SY01100 (nolock)
          where  SERIES = 2
                 and SEQNUMBR = 900
        end
      
      if @RNDDIFF = 0
        begin
          select @O_iErrorState = 1301
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if @I_vXCHGRATE = 0
        begin
          select @O_iErrorState = 1886
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if @I_vRTCLCMTD = 0
        begin
          select @PAEXTCOST = round(@PAEXTCOST * @I_vXCHGRATE,@DECPLCUR),
                 @PAtotcosts = round(@PAtotcosts * @I_vXCHGRATE,@DECPLCUR),
                 @I_vPAREIMBURSTAXAMT = round(@I_vPAREIMBURSTAXAMT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vPATACRV = round(@I_vPATACRV * @I_vXCHGRATE,@DECPLCUR),
                 @I_vTRDISAMT = round(@I_vTRDISAMT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vFRTAMNT = round(@I_vFRTAMNT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vFRTTXAMT = round(@I_vFRTTXAMT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vMSCCHAMT = round(@I_vMSCCHAMT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vMSCTXAMT = round(@I_vMSCTXAMT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vDOCAMNT = round(@I_vDOCAMNT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vTEN99AMNT = round(@I_vTEN99AMNT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vCASHAMNT = round(@I_vCASHAMNT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vCHRGAMNT = round(@I_vCHRGAMNT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vCHEKAMNT = round(@I_vCHEKAMNT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vCRCRDAMT = round(@I_vCRCRDAMT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vTAXAMNT = round(@I_vTAXAMNT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vBCKTXAMT = round(@I_vBCKTXAMT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vPAReimbursableAmount = round(@I_vPAReimbursableAmount * @I_vXCHGRATE,@DECPLCUR),
                 @I_vBKTPURAM = round(@I_vBKTPURAM * @I_vXCHGRATE,@DECPLCUR)
        end
      else
        if @I_vRTCLCMTD = 1
          begin
            select @PAEXTCOST = round(@PAEXTCOST / @I_vXCHGRATE,@DECPLCUR),
                   @PAtotcosts = round(@PAtotcosts / @I_vXCHGRATE,@DECPLCUR),
                   @I_vPAREIMBURSTAXAMT = round(@I_vPAREIMBURSTAXAMT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vPATACRV = round(@I_vPATACRV / @I_vXCHGRATE,@DECPLCUR),
                   @I_vTRDISAMT = round(@I_vTRDISAMT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vFRTAMNT = round(@I_vFRTAMNT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vFRTTXAMT = round(@I_vFRTTXAMT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vMSCCHAMT = round(@I_vMSCCHAMT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vMSCTXAMT = round(@I_vMSCTXAMT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vDOCAMNT = round(@I_vDOCAMNT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vTEN99AMNT = round(@I_vTEN99AMNT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vCASHAMNT = round(@I_vCASHAMNT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vCHRGAMNT = round(@I_vCHRGAMNT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vCHEKAMNT = round(@I_vCHEKAMNT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vCRCRDAMT = round(@I_vCRCRDAMT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vTAXAMNT = round(@I_vTAXAMNT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vBCKTXAMT = round(@I_vBCKTXAMT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vPAReimbursableAmount = round(@I_vPAReimbursableAmount / @I_vXCHGRATE,@DECPLCUR),
                   @I_vBKTPURAM = round(@I_vBKTPURAM / @I_vXCHGRATE,@DECPLCUR)
          end
      
      if (@I_vRTCLCMTD = 0)
        begin
          update PA10501
          set    PAUNITCOST = round(PAUNITCOST * @I_vXCHGRATE,@DECPLCUR),
                 ORUNTCST = PAUNITCOST,
                 PABase_Unit_Cost = round(PABase_Unit_Cost * @I_vXCHGRATE,@DECPLCUR),
                 PAORGBSUNITCST = PABase_Unit_Cost,
                 PAEXTCOST = round(PAEXTCOST * @I_vXCHGRATE,@DECPLCUR),
                 OREXTCST = PAEXTCOST,
                 PAtotcosts = round(PAtotcosts * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGTOTCOSTS = PAtotcosts,
                 PABILRATE = PABILRATE,
                 PAORIGBILLRATE = round(PABILRATE / @I_vXCHGRATE,@DECPLCUR),
                 PA_Base_Billing_Rate = PA_Base_Billing_Rate,
                 PAORIGBSBILLRTE = round(PA_Base_Billing_Rate / @I_vXCHGRATE,@DECPLCUR),
                 PAOverhead_Amount = round(PAOverhead_Amount * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGOVHDAMT = PAOverhead_Amount,
                 PABaseOvhdCost = round(PABaseOvhdCost * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGBASEOVRHCST = PABaseOvhdCost,
                 PATOTALOVERH = round(PATOTALOVERH * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGTOTOVRHD = PATOTALOVERH,
                 PAProfitAmount = round(PAProfitAmount * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGPROFAMT = PAProfitAmount,
                 PATotalProfit = round(PATotalProfit * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGTOTPROF = PATotalProfit,
                 PAACREV = case 
                             when PAMARKPERCENT <> 0 then round(PAACREV * @I_vXCHGRATE,@DECPLCUR)
                             else PAACREV
                           end,
                 PAORIACCRREV = case 
                                  when PAMARKPERCENT = 0 then round(PAACREV / @I_vXCHGRATE,@DECPLCUR)
                                  else PAACREV
                                end,
                 TAXAMNT = round(TAXAMNT * @I_vXCHGRATE,@DECPLCUR),
                 ORTAXAMT = TAXAMNT,
                 BCKTXAMT = round(BCKTXAMT * @I_vXCHGRATE,@DECPLCUR),
                 OBTAXAMT = BCKTXAMT,
                 PAReimbursableAmount = round(PAReimbursableAmount * @I_vXCHGRATE,@DECPLCUR),
                 PAOrigReimbursableAmt = PAReimbursableAmount,
                 RNDDIFF = @RNDDIFF
          where  PAerdocnumber = @I_vPAerdocnumber
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 1887
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          update PA10502
          set    TAXAMNT = round(TAXAMNT * @I_vXCHGRATE,@DECPLCUR),
                 ORTAXAMT = TAXAMNT,
                 PAREIMBURSTAXAMT = round(PAREIMBURSTAXAMT * @I_vXCHGRATE,@DECPLCUR),
                 PAORIGREIMTAXAMT = PAREIMBURSTAXAMT,
                 PCTAXAMT = round(PCTAXAMT * @I_vXCHGRATE,@DECPLCUR),
                 ORPURTAX = PCTAXAMT,
                 FRTTXAMT = round(FRTTXAMT * @I_vXCHGRATE,@DECPLCUR),
                 ORFRTTAX = FRTTXAMT,
                 MSCTXAMT = round(MSCTXAMT * @I_vXCHGRATE,@DECPLCUR),
                 ORMSCTAX = MSCTXAMT,
                 TXDTTPUR = round(TXDTTPUR * @I_vXCHGRATE,@DECPLCUR),
                 ORTOTPUR = TXDTTPUR,
                 TDTTXPUR = round(TDTTXPUR * @I_vXCHGRATE,@DECPLCUR),
                 ORTXBPUR = TDTTXPUR
          where  PAerdocnumber = @I_vPAerdocnumber
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 1888
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          update PA10504
          set    CRDTAMNT = round(CRDTAMNT * @I_vXCHGRATE,@DECPLCUR),
                 ORCRDAMT = CRDTAMNT,
                 DEBITAMT = round(DEBITAMT * @I_vXCHGRATE,@DECPLCUR),
                 ORDBTAMT = DEBITAMT,
                 CURNCYID = @I_vCURNCYID,
                 CURRNIDX = @CURRNIDX
          where  PAerdocnumber = @I_vPAerdocnumber
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 1889
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        if @I_vRTCLCMTD = 1
          begin
            update PA10501
            set    PAUNITCOST = round(PAUNITCOST / @I_vXCHGRATE,@DECPLCUR),
                   ORUNTCST = PAUNITCOST,
                   PABase_Unit_Cost = round(PABase_Unit_Cost / @I_vXCHGRATE,@DECPLCUR),
                   PAORGBSUNITCST = PABase_Unit_Cost,
                   PAEXTCOST = round(PAEXTCOST / @I_vXCHGRATE,@DECPLCUR),
                   OREXTCST = PAEXTCOST,
                   PAtotcosts = round(PAtotcosts / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGTOTCOSTS = PAtotcosts,
                   PABILRATE = PABILRATE,
                   PAORIGBILLRATE = round(PABILRATE * @I_vXCHGRATE,@DECPLCUR),
                   PA_Base_Billing_Rate = PA_Base_Billing_Rate,
                   PAORIGBSBILLRTE = round(PA_Base_Billing_Rate * @I_vXCHGRATE,@DECPLCUR),
                   PAOverhead_Amount = round(PAOverhead_Amount / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGOVHDAMT = PAOverhead_Amount,
                   PABaseOvhdCost = round(PABaseOvhdCost / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGBASEOVRHCST = PABaseOvhdCost,
                   PATOTALOVERH = round(PATOTALOVERH / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGTOTOVRHD = PATOTALOVERH,
                   PAProfitAmount = round(PAProfitAmount / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGPROFAMT = PAProfitAmount,
                   PATotalProfit = round(PATotalProfit / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGTOTPROF = PATotalProfit,
                   PAACREV = case 
                               when PAMARKPERCENT <> 0 then round(PAACREV / @I_vXCHGRATE,@DECPLCUR)
                               else PAACREV
                             end,
                   PAORIACCRREV = case 
                                    when PAMARKPERCENT = 0 then round(PAACREV * @I_vXCHGRATE,@DECPLCUR)
                                    else PAACREV
                                  end,
                   TAXAMNT = round(TAXAMNT / @I_vXCHGRATE,@DECPLCUR),
                   ORTAXAMT = TAXAMNT,
                   BCKTXAMT = round(BCKTXAMT / @I_vXCHGRATE,@DECPLCUR),
                   OBTAXAMT = BCKTXAMT,
                   PAReimbursableAmount = round(PAReimbursableAmount / @I_vXCHGRATE,@DECPLCUR),
                   PAOrigReimbursableAmt = PAReimbursableAmount
            where  PAerdocnumber = @I_vPAerdocnumber
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 1890
                
                exec @iStatus = taUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
                
                return (@O_iErrorState)
              end
            
            update PA10502
            set    TAXAMNT = round(TAXAMNT / @I_vXCHGRATE,@DECPLCUR),
                   ORTAXAMT = TAXAMNT,
                   PAREIMBURSTAXAMT = round(PAREIMBURSTAXAMT / @I_vXCHGRATE,@DECPLCUR),
                   PAORIGREIMTAXAMT = PAREIMBURSTAXAMT,
                   PCTAXAMT = round(PCTAXAMT / @I_vXCHGRATE,@DECPLCUR),
                   ORPURTAX = PCTAXAMT,
                   FRTTXAMT = round(FRTTXAMT / @I_vXCHGRATE,@DECPLCUR),
                   ORFRTTAX = FRTTXAMT,
                   MSCTXAMT = round(MSCTXAMT / @I_vXCHGRATE,@DECPLCUR),
                   ORMSCTAX = MSCTXAMT,
                   TXDTTPUR = round(TXDTTPUR / @I_vXCHGRATE,@DECPLCUR),
                   ORTOTPUR = TXDTTPUR,
                   TDTTXPUR = round(TDTTXPUR / @I_vXCHGRATE,@DECPLCUR),
                   ORTXBPUR = TDTTXPUR
            where  PAerdocnumber = @I_vPAerdocnumber
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 1891
                
                exec @iStatus = taUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
                
                return (@O_iErrorState)
              end
            
            update PA10504
            set    CRDTAMNT = round(CRDTAMNT / @I_vXCHGRATE,@DECPLCUR),
                   ORCRDAMT = CRDTAMNT,
                   DEBITAMT = round(DEBITAMT / @I_vXCHGRATE,@DECPLCUR),
                   ORDBTAMT = DEBITAMT,
                   CURNCYID = @I_vCURNCYID,
                   CURRNIDX = @CURRNIDX
            where  PAerdocnumber = @I_vPAerdocnumber
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 1892
                
                exec @iStatus = taUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
                
                return (@O_iErrorState)
              end
          end
    end
  
  select @PATACRV = isnull(sum(PAACREV),0),
         @PAORIACCRREV = isnull(sum(PAORIACCRREV),0),
         @UN1099AM = case 
                       when (@TEN99TYPE <> 1
                             and @TEN99TYPE <> 5) then (sum(PAReimbursableAmount) - sum(TAXAMNT))
                       else 0
                     end
  from   PA10501 (nolock)
  where  PAerdocnumber = @I_vPAerdocnumber
  
  if ((@I_vUN1099AM > 0)
      and (@I_vUN1099AM <> @UN1099AM))
    begin
      select @O_iErrorState = 1881
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    begin
      if ((@I_vUN1099AM = 0)
          and (@UN1099AM <> 0))
        select @I_vUN1099AM = @UN1099AM
    end
  
  if ((@I_vPATACRV = 0)
      and (@PATACRV <> 0))
    select @I_vPATACRV = @PATACRV
  
  if ((@I_vPATACRV > 0)
      and (@I_vPATACRV <> @PATACRV))
    begin
      select @O_iErrorState = 7334
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @BATCHTOTAL = isnull(@PAtotcosts,0) + isnull(@I_vFRTAMNT,0) + isnull(@I_vMSCCHAMT,0)
  
  exec @iStatus = taCreateUpdateBatchHeaderRcd
    @I_vBACHNUMB = @I_vBACHNUMB ,
    @I_vSERIES = 7 ,
    @I_vGLPOSTDT = @I_vPADOCDT ,
    @I_vBCHSOURC = 'PA_EE' ,
    @I_vDOCAMT = @BATCHTOTAL ,
    @I_vORIGIN = 0 ,
    @I_vNUMOFTRX = 1 ,
    @I_vCHEKBKID = @CHEKBKID ,
    @O_iErrorState = @iUpdtBthErrState output ,
    @oErrString = @iCreateBatchErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    select @iStatus = @iError
  
  if (@iStatus <> 0)
      or (@iUpdtBthErrState <> 0)
    begin
      if (@iUpdtBthErrState <> 0)
        begin
          exec @iStatus = taUpdateString
            @iUpdtBthErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      select @O_iErrorState = 1893
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @linecount = count(PAerdocnumber)
  from   PA10501 (nolock)
  where  PAerdocnumber = @I_vPAerdocnumber
  
  select @linecount = @linecount + 1
  
  exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
    @I_sCompanyID = @sCompanyID ,
    @I_iSQLSessionID = 0 ,
    @I_noteincrement = @linecount ,
    @O_mNoteIndex = @NOTEINDX output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iGetNextNoteIdxErrState <> 0)
       or (@iError <> 0))
    begin
      if @iGetNextNoteIdxErrState <> 0
        select @oErrString = rtrim(@oErrString) + ' ' + @iGetNextNoteIdxErrState
      
      select @O_iErrorState = 1894
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @tempNOTEINDX = @NOTEINDX
  
  update PA10501
  set    PAbillnoteidx = @tempNOTEINDX,
         @tempNOTEINDX = @tempNOTEINDX + 1
  where  PAerdocnumber = @I_vPAerdocnumber
  
  if @@error <> 0
    begin
      select @O_iErrorState = 5262
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@oErrString <> '')
       or (@O_iErrorState <> 0))
    return (@O_iErrorState)
  
  insert PM00400
        (CNTRLNUM,
         CNTRLTYP,
         DCSTATUS,
         DOCTYPE,
         VENDORID,
         DOCNUMBR,
         CHEKBKID,
         DUEDATE,
         DISCDATE,
         BCHSOURC,
         DOCDATE)
  select @I_vPAerdocnumber,
         0,
         1,
         1,
         @I_vEMPLOYID,
         @I_vPAerdocnumber,
         '',
         '',
         '',
         '',
         ''
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 7019
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vCARDNAME <> '')
    begin
      insert PM00400
            (CNTRLNUM,
             CNTRLTYP,
             DCSTATUS,
             DOCTYPE,
             VENDORID,
             DOCNUMBR,
             CHEKBKID,
             DUEDATE,
             DISCDATE,
             BCHSOURC,
             DOCDATE)
      select @I_vCCAMPYNM,
             0,
             1,
             1,
             @VENDORID,
             @I_vCCRCTNUM,
             '',
             '',
             '',
             '',
             ''
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 5255
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vCDOCNMBR <> '')
    begin
      insert PM00400
            (CNTRLNUM,
             CNTRLTYP,
             DCSTATUS,
             DOCTYPE,
             VENDORID,
             DOCNUMBR,
             CHEKBKID,
             DUEDATE,
             DISCDATE,
             BCHSOURC,
             DOCDATE)
      select @I_vCAMPMTNM,
             1,
             1,
             6,
             @I_vEMPLOYID,
             @I_vCDOCNMBR,
             @I_vCAMCBKID,
             '',
             '',
             'PM_Trxent',
             @I_vCAMTDATE
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 7318
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vCHEKNMBR <> '')
    begin
      insert PM00400
            (CNTRLNUM,
             CNTRLTYP,
             DCSTATUS,
             DOCTYPE,
             VENDORID,
             DOCNUMBR,
             CHEKBKID,
             DUEDATE,
             DISCDATE,
             BCHSOURC,
             DOCDATE)
      select @I_vCAMPYNBR,
             1,
             1,
             6,
             @I_vEMPLOYID,
             @I_vCHEKNMBR,
             @I_vCHAMCBID,
             '',
             '',
             'PM_Trxent',
             @I_vCHEKDATE
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 5399
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  insert PA01901
        (PATranType,
         DOCNUMBR,
         CUSTNMBR,
         PADOCDT,
         PACOSTOWNER,
         RMDTYPAL,
         PABILLTRXT,
         PADocnumber20,
         DCSTATUS)
  select 5,
         '',
         '',
         @I_vPADOCDT,
         @I_vEMPLOYID,
         0,
         0,
         @I_vPAerdocnumber,
         1
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 4002
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @PAINCPRCHTXPRJCST = isnull(PAINCPRCHTXPRJCST,0)
  from   PA41701 (nolock)
  
  declare EELineSummary INSENSITIVE cursor  for
  select PACONTNUMBER,
         PAPROJNUMBER,
         PACOSTCATID,
         PADT,
         PAQtyQ,
         PAtotcosts,
         TAXAMNT,
         BCKTXAMT,
         PATotalProfit,
         PAACREV,
         UOMSCHDL,
         PAUnit_of_Measure,
         PAitemnumber,
         PAbllngtype
  from   PA10501 (nolock)
  where  PAerdocnumber = @I_vPAerdocnumber
  
  open EELineSummary
  
  select @iCursorError = @@cursor_rows
  
  if (@iCursorError > 0)
    begin
      fetch next from EELineSummary
      into @PACONTNUMBER,
           @PAPROJNUMBER,
           @PACOSTCATID,
           @PADT,
           @PAQtyQ,
           @PATempTotCosts,
           @TAXAMNT,
           @BCKTXAMT,
           @PATotalProfit,
           @PAACREV,
           @UOMSCHDL,
           @PAUnit_of_Measure,
           @PAitemnumber,
           @PAbllngtype
      
      while (@@fetch_status <> -1)
        begin
          if (@@fetch_status = -2)
            begin
              select @O_iErrorState = 5398
              
              break
            end
          
          if (@PAPROJNUMBER <> '<NONE>')
            begin
              select @CUSTNMBR = isnull(CUSTNMBR,'')
              from   PA01201 (nolock)
              where  PAPROJNUMBER = @PAPROJNUMBER
              
              execute @iStatus = taPA_Line_Update_Totals_Tables
                @I_Cust = @CUSTNMBR ,
                @I_Contract = @PACONTNUMBER ,
                @I_Proj = @PAPROJNUMBER ,
                @I_CostCat = @PACOSTCATID ,
                @I_Date = @PADT ,
                @I_Qty = @PAQtyQ ,
                @IN_Qty_canceled = 0 ,
                @I_Cost = @PATempTotCosts ,
                @I_Tax = @TAXAMNT ,
                @IN_total_backout_amount = @BCKTXAMT ,
                @I_Profit = @PATotalProfit ,
                @I_Accrued = @PAACREV ,
                @I_Update_Accrued_Revenues = 1 ,
                @IN_non_iv = 1 ,
                @IN_uofm_schedule = @UOMSCHDL ,
                @IN_uofm = @PAUnit_of_Measure ,
                @includePurchTaxInCost = @PAINCPRCHTXPRJCST ,
                @I_itemnumber = @PAitemnumber ,
                @PALineItemSeq = 0 ,
                @IN_billingtype = @PAbllngtype ,
                @PATU = 5 ,
                @I_Overhead = 0 ,
                @I_err = @PA_Line_Update_Totals_TablesErrorState output
              
              select @iError = @@error
              
              if ((@iStatus <> 0)
                   or (@PA_Line_Update_Totals_TablesErrorState <> 0)
                   or (@iError <> 0))
                begin
                  select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@PA_Line_Update_Totals_TablesErrorState))
                  
                  select @O_iErrorState = 6416
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  deallocate CustomerSummary
                  
                  return (@O_iErrorState)
                end
            end
          
          fetch next from EELineSummary
          into @PACONTNUMBER,
               @PAPROJNUMBER,
               @PACOSTCATID,
               @PADT,
               @PAQtyQ,
               @PATempTotCosts,
               @TAXAMNT,
               @BCKTXAMT,
               @PATotalProfit,
               @PAACREV,
               @UOMSCHDL,
               @PAUnit_of_Measure,
               @PAitemnumber,
               @PAbllngtype
        end
    end
  
  deallocate EELineSummary
  
  insert PA10500
        (PAertrxtype,
         PAerdocnumber,
         PDK_EE_No,
         PADOCDT,
         USERID,
         BACHNUMB,
         BCHSOURC,
         TRXSORCE,
         PAREFNO,
         SHIPMTHD,
         PAUD1,
         PAUD2,
         EMPLOYID,
         PAVENADDRESSID,
         PAStartDate,
         PAEnDate,
         PAreptsuff,
         PAYR,
         PYMTRMID,
         DUEDATE,
         DSCDLRAM,
         ORDDLRAT,
         DISCDATE,
         DISAMTAV,
         ODISAMTAV,
         PRCTDISC,
         PACOMM,
         CURNCYID,
         CURRNIDX,
         PATQTY,
         PAEXTCOST,
         OREXTCST,
         PAtotcosts,
         PAORIGTOTCOSTS,
         PAREIMBURSTAXAMT,
         PAORIGREIMTAXAMT,
         BKTPURAM,
         PATACRV,
         PAORIACCRREV,
         TRDISAMT,
         ORTDISAM,
         PAFreight_Taxable_P,
         FRTSCHID,
         FRTAMNT,
         ORFRTAMT,
         FRTTXAMT,
         ORFRTTAX,
         PAMisc_Taxable_P,
         MSCSCHID,
         MSCCHAMT,
         OMISCAMT,
         MSCTXAMT,
         ORMSCTAX,
         DOCAMNT,
         ORDOCAMT,
         TEN99AMNT,
         OR1099AM,
         UN1099AM,
         CASHAMNT,
         ORCASAMT,
         CAMCBKID,
         CDOCNMBR,
         CAMTDATE,
         CAMPMTNM,
         CHRGAMNT,
         OCHGAMT,
         CHEKAMNT,
         ORCHKAMT,
         CHEKNMBR,
         CHEKDATE,
         CAMPYNBR,
         CHAMCBID,
         CARDNAME,
         CRCRDAMT,
         ORCCDAMT,
         CRCARDDT,
         CCAMPYNM,
         CCRCTNUM,
         DISTKNAM,
         ORDISTKN,
         ORDAVFRT,
         ODAVPUR,
         ORDAVMSC,
         TAXSCHID,
         TAXAMNT,
         ORTAXAMT,
         BCKTXAMT,
         OBTAXAMT,
         PAReimbursableAmount,
         PAOrigReimbursableAmt,
         PAPD,
         RATETPID,
         EXGTBLID,
         XCHGRATE,
         EXCHDATE,
         TIME1,
         RTCLCMTD,
         DENXRATE,
         MCTRXSTT,
         PA_EE_HDR_Errors,
         PA_EE_Dist_Errors,
         PA_MC_EE_Posting_Errors,
         PApostoDynPM,
         Tax_Date,
         NOTEINDX,
         TaxInvRecvd,
         TaxInvReqd,
         Correcting_Trx_Type,
         PAORIGINLDOCNUM,
         TEN99TYPE,
         TEN99BOXNUMBER,
         CREATDDT,
         CRUSRID)
  select @I_vPAertrxtype,
         @I_vPAerdocnumber,
         @I_vPDK_EE_No,
         @I_vPADOCDT,
         @I_vUSERID,
         @I_vBACHNUMB,
         @BCHSOURC,
         @TRXSORCE,
         @I_vPAREFNO,
         @I_vSHIPMTHD,
         @I_vPAUD1,
         @I_vPAUD2,
         @I_vEMPLOYID,
         @I_vPAVENADDRESSID,
         @I_vPAStartDate,
         @I_vPAEnDate,
         @I_vPAreptsuff,
         @PAYR,
         @PYMTRMID,
         @DUEDATE,
         @DSCDLRAM,
         @ORDDLRAT,
         @DISCDATE,
         @DISAMTAV,
         @ODISAMTAV,
         @PRCTDISC,
         @I_vPACOMM,
         @I_vCURNCYID,
         @CURRNIDX,
         @I_vPATQTY,
         @PAEXTCOST,
         @OREXTCST,
         @PAtotcosts,
         @PAORIGTOTCOSTS,
         @I_vPAREIMBURSTAXAMT,
         @PAORIGREIMTAXAMT,
         @I_vBKTPURAM,
         @I_vPATACRV,
         @PAORIACCRREV,
         @I_vTRDISAMT,
         @ORTDISAM,
         @I_vPAFreight_Taxable_P,
         @I_vFRTSCHID,
         @I_vFRTAMNT,
         @ORFRTAMT,
         @I_vFRTTXAMT,
         @ORFRTTAX,
         @I_vPAMisc_Taxable_P,
         @I_vMSCSCHID,
         @I_vMSCCHAMT,
         @OMISCAMT,
         @I_vMSCTXAMT,
         @ORMSCTAX,
         @I_vDOCAMNT,
         @ORDOCAMT,
         @I_vTEN99AMNT,
         @OR1099AM,
         @I_vUN1099AM,
         @I_vCASHAMNT,
         @ORCASAMT,
         @I_vCAMCBKID,
         @I_vCDOCNMBR,
         @I_vCAMTDATE,
         @I_vCAMPMTNM,
         @I_vCHRGAMNT,
         @OCHGAMT,
         @I_vCHEKAMNT,
         @ORCHKAMT,
         @I_vCHEKNMBR,
         @I_vCHEKDATE,
         @I_vCAMPYNBR,
         @I_vCHAMCBID,
         @I_vCARDNAME,
         @I_vCRCRDAMT,
         @ORCCDAMT,
         @I_vCRCARDDT,
         @I_vCCAMPYNM,
         @I_vCCRCTNUM,
         @DISTKNAM,
         @ORDISTKN,
         @ORDAVFRT,
         @ODAVPUR,
         @ORDAVMSC,
         @I_vTAXSCHID,
         @I_vTAXAMNT,
         @ORTAXAMT,
         @I_vBCKTXAMT,
         @OBTAXAMT,
         @I_vPAReimbursableAmount,
         @PAOrigReimbursableAmt,
         @I_vPAPD,
         @I_vRATETPID,
         @EXGTBLID,
         @I_vXCHGRATE,
         @I_vEXCHDATE,
         @I_vTIME1,
         @I_vRTCLCMTD,
         @DENXRATE,
         @MCTRXSTT,
         @PA_EE_HDR_Errors,
         @PA_EE_Dist_Errors,
         @PA_MC_EE_Posting_Errors,
         @I_vPApostoDynPM,
         @I_vTax_Date,
         @NOTEINDX,
         @I_vTaxInvRecvd,
         @I_vTaxInvReqd,
         0,
         '',
         @TEN99TYPE,
         @TEN99BOXNUMBER,
         '1/1/1900',
         ''
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 1903
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAEECreateDistributions
    @I_vCURNCYID ,
    @CURRNIDX ,
    @I_vPAerdocnumber ,
    @I_vEMPLOYID ,
    @PAProjectType ,
    @PAAcctgMethod ,
    @I_vCHAMCBID ,
    @I_vFRTAMNT ,
    @I_vMSCCHAMT ,
    @I_vCASHAMNT ,
    @I_vCHEKAMNT ,
    @I_vCRCRDAMT ,
    @ORCASAMT ,
    @ORCHKAMT ,
    @ORCCDAMT ,
    @OMISCAMT ,
    @ORFRTAMT ,
    @I_vCARDNAME ,
    @I_vCAMCBKID ,
    @O_iErrorState output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    select @iStatus = @iError
  
  if (@iStatus <> 0)
      or (@O_iErrorState <> 0)
    begin
      if (@O_iErrorState <> 0)
        begin
          exec @iStatus = taUpdateString
            @iUpdDistErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 1904
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAEmpExpenseHdrInsertPost
    @I_vPAertrxtype ,
    @I_vPAerdocnumber ,
    @I_vPADOCDT ,
    @I_vBACHNUMB ,
    @I_vEMPLOYID ,
    @I_vPAVENADDRESSID ,
    @I_vPAStartDate ,
    @I_vPAEnDate ,
    @I_vCURNCYID ,
    @I_vUSERID ,
    @I_vPAREFNO ,
    @I_vSHIPMTHD ,
    @I_vPAUD1 ,
    @I_vPAUD2 ,
    @I_vPAreptsuff ,
    @I_vPACOMM ,
    @I_vPATQTY ,
    @I_vPAREIMBURSTAXAMT ,
    @I_vBKTPURAM ,
    @I_vPATACRV ,
    @I_vTRDISAMT ,
    @I_vPAFreight_Taxable_P ,
    @I_vFRTSCHID ,
    @I_vFRTAMNT ,
    @I_vFRTTXAMT ,
    @I_vPAMisc_Taxable_P ,
    @I_vMSCSCHID ,
    @I_vMSCCHAMT ,
    @I_vMSCTXAMT ,
    @I_vDOCAMNT ,
    @I_vTEN99AMNT ,
    @I_vUN1099AM ,
    @I_vCASHAMNT ,
    @I_vCAMCBKID ,
    @I_vCDOCNMBR ,
    @I_vCAMTDATE ,
    @I_vCAMPMTNM ,
    @I_vCHRGAMNT ,
    @I_vCHEKAMNT ,
    @I_vCHEKNMBR ,
    @I_vCHEKDATE ,
    @I_vCAMPYNBR ,
    @I_vCHAMCBID ,
    @I_vCARDNAME ,
    @I_vCRCRDAMT ,
    @I_vCRCARDDT ,
    @I_vCCAMPYNM ,
    @I_vCCRCTNUM ,
    @I_vTAXSCHID ,
    @I_vTAXAMNT ,
    @I_vBCKTXAMT ,
    @I_vPAReimbursableAmount ,
    @I_vPAPD ,
    @I_vPApostoDynPM ,
    @I_vTax_Date ,
    @I_vTaxInvRecvd ,
    @I_vTaxInvReqd ,
    @I_vUSINGHEADERLEVELTAXES ,
    @I_vXCHGRATE ,
    @I_vRATETPID ,
    @I_vEXPNDATE ,
    @I_vEXCHDATE ,
    @I_vEXGTBDSC ,
    @I_vEXTBLSRC ,
    @I_vRATEEXPR ,
    @I_vDYSTINCR ,
    @I_vRATEVARC ,
    @I_vTRXDTDEF ,
    @I_vRTCLCMTD ,
    @I_vPRVDSLMT ,
    @I_vDATELMTS ,
    @I_vTIME1 ,
    @I_vPDK_EE_No ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1905
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @@error = 0
     and @O_iErrorState = 0
     and @O_oErrorState = 0
    select @O_iErrorState = 0
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_EmployeeExpense' ,
        @I_vINDEX1 = @I_vPAerdocnumber ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 1 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iCustomState <> 0)
           or (@iError <> 0))
        begin
          select @O_iErrorState = 2032
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  return (@O_iErrorState)

