

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsUpdateCreateCustomerRcdWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsUpdateCreateCustomerRcdWrapper]
  
create procedure [dbo].[RmsUpdateCreateCustomerRcdWrapper]
                @iCustNum     char(15),
                @iCustName    char(64),
                @iCustClass   char(15),
                @iAddress1    char(60),
                @iAddress2    char(60),
                @iCity        char(35),
                @iState       char(29),
                @iZipCode     char(10),
                @iCountry     char(60),
                @iPhoneNumber char(14),
                @iFax         char(14),
                @oErrorState  int  output,
                @oErrString   varchar(255)  output   /*with encryption*/
AS
  EXEC RmsUpdateCreateCustomerRcd
    @I_vCUSTNMBR = @iCustNum ,
    @I_vCUSTNAME = @iCustName ,
    @I_vCUSTCLAS = @iCustClass ,
    @I_vADDRESS1 = @iAddress1 ,
    @I_vADDRESS2 = @iAddress2 ,
    @I_vCITY = @iCity ,
    @I_vSTATE = @iState ,
    @I_vZIPCODE = @iZipCode ,
    @I_vCOUNTRY = @iCountry ,
    @I_vPHNUMBR1 = @iPhoneNumber ,
    @I_vFAX = @iFax ,
    @I_vADRSCODE = 'PRIMARY' ,
    @I_vPRBTADCD = 'PRIMARY' ,
    @I_vPRSTADCD = 'PRIMARY' ,
    @I_vSTADDRCD = 'PRIMARY' ,
    @I_vUseCustomerClass = 1 ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

