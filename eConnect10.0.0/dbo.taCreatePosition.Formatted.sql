

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taCreatePosition]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taCreatePosition]
  
create procedure [dbo].[taCreatePosition]
                @I_vJOBTITLE       char(6),
                @I_vDSCRIPTN       char(30),
                @I_vCHANGEBY_I     char(15)  = '',
                @I_vCHANGEDATE_I   datetime  = null,
                @I_vUpdateIfExists tinyint  = 1,
                @I_vRequesterTrx   smallint  = 0,
                @I_vUSRDEFND1      char(50)  = '',
                @I_vUSRDEFND2      char(50)  = '',
                @I_vUSRDEFND3      char(50)  = '',
                @I_vUSRDEFND4      varchar(8000)  = '',
                @I_vUSRDEFND5      varchar(8000)  = '',
                @O_iErrorState     int  output,
                @oErrString        varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @sCompanyID              int,
           @iStatus                 int,
           @iCustomState            int,
           @iCustomErrString        varchar(255),
           @nNextNoteIndex          numeric(19,5),
           @iGetNextNoteIdxErrState int,
           @iAddCodeErrState        int,
           @O_oErrorState           int,
           @iError                  int
  
  select @O_iErrorState = 0,
         @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500
  where  INTERID = db_name()
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taCreatePositionPre
    @I_vJOBTITLE output ,
    @I_vDSCRIPTN output ,
    @I_vCHANGEBY_I output ,
    @I_vCHANGEDATE_I output ,
    @I_vUpdateIfExists output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4464
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vJOBTITLE is NULL 
       or @I_vDSCRIPTN is NULL
       or @I_vCHANGEBY_I is NULL
       or @I_vUpdateIfExists is NULL
       or @I_vRequesterTrx is NULL
       or @I_vUSRDEFND1 is NULL
       or @I_vUSRDEFND2 is NULL
       or @I_vUSRDEFND3 is NULL
       or @I_vUSRDEFND4 is NULL
       or @I_vUSRDEFND5 is NULL)
    begin
      select @O_iErrorState = 4465
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  select @I_vJOBTITLE = UPPER(@I_vJOBTITLE)
  
  if (@I_vJOBTITLE = '')
    begin
      select @O_iErrorState = 4466
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vDSCRIPTN = '')
    begin
      select @O_iErrorState = 4467
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vUpdateIfExists < 0
       or @I_vUpdateIfExists > 1)
    begin
      select @O_iErrorState = 3671
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vRequesterTrx < 0
       or @I_vRequesterTrx > 1)
    begin
      select @O_iErrorState = 3672
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if @O_iErrorState <> 0
    return (@O_iErrorState)
  
  exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
    @I_sCompanyID = @sCompanyID ,
    @I_iSQLSessionID = 0 ,
    @I_noteincrement = 1 ,
    @O_mNoteIndex = @nNextNoteIndex output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iGetNextNoteIdxErrState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iGetNextNoteIdxErrState))
      
      select @O_iErrorState = 4513
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  begin
    if not exists (select 1
                   from   UPR40301 (nolock)
                   where  JOBTITLE = @I_vJOBTITLE)
      begin
        insert UPR40301
              (JOBTITLE,
               DSCRIPTN,
               EEOCLASS_I,
               FLSASTATUS,
               REPORTSTOPOS,
               REVIEWSETUPCODE_I,
               SKILLSETNUMBER_I,
               CHANGEBY_I,
               CHANGEDATE_I,
               NOTEINDX,
               NOTEINDX2,
               TXTFIELD)
        select @I_vJOBTITLE,
               @I_vDSCRIPTN,
               0,
               0,
               '',
               '',
               0,
               @I_vCHANGEBY_I,
               case 
                 WHEN @I_vCHANGEDATE_I is null THEN convert(varchar(12),getdate())
                 ELSE @I_vCHANGEDATE_I
               end,
               @nNextNoteIndex,
               0,
               ''
        
        if @@error <> 0
          begin
            select @O_iErrorState = 4468
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @iAddCodeErrState output
            
            return (@O_iErrorState)
          end
      end
    else
      if (@I_vUpdateIfExists = 1)
        begin
          update UPR40301
          set    DSCRIPTN = case 
                              WHEN @I_vDSCRIPTN = '' THEN DSCRIPTN
                              ELSE @I_vDSCRIPTN
                            end,
                 CHANGEBY_I = case 
                                WHEN @I_vCHANGEBY_I = '' THEN CHANGEBY_I
                                ELSE @I_vCHANGEBY_I
                              end,
                 CHANGEDATE_I = case 
                                  WHEN @I_vCHANGEDATE_I is null THEN CHANGEDATE_I
                                  ELSE @I_vCHANGEDATE_I
                                end
          where  JOBTITLE = @I_vJOBTITLE
                 and DSCRIPTN <> @I_vDSCRIPTN
          
          if @@error <> 0
            begin
              select @O_iErrorState = 4469
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @iAddCodeErrState output
              
              return (@O_iErrorState)
            end
        end
  end
  
  exec @iStatus = taCreatePositionPost
    @I_vJOBTITLE ,
    @I_vDSCRIPTN ,
    @I_vCHANGEBY_I ,
    @I_vCHANGEDATE_I ,
    @I_vUpdateIfExists ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4470
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

