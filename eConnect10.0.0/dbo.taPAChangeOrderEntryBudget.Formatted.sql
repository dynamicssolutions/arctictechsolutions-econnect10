

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAChangeOrderEntryBudget]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAChangeOrderEntryBudget]
  
create procedure [dbo].[taPAChangeOrderEntryBudget]
                @I_vPACHGORDNO             char(17),
                @I_vPACONTNUMBER           CHAR(11),
                @I_vPAPROJNUMBER           char(15),
                @I_vPACOSTCATID            char(15),
                @I_vLNITMSEQ               int  = 0,
                @I_vPAbegindate            datetime  = '',
                @I_vPAEnDate               datetime  = '',
                @I_vPAVarianceQty          numeric(19,5)  = 0,
                @I_vPAQtyQ                 numeric(19,5)  = 0,
                @I_vPAVarianceCosts        numeric(19,5)  = 0,
                @I_vPAUNITCOST             numeric(19,5)  = 0,
                @I_vPAVARMUPRCNT           numeric(19,5)  = 0,
                @I_vPAProfitPercent        numeric(19,5)  = 0,
                @I_vPAVARPROFAMT           numeric(19,5)  = 0,
                @I_vPAProfitAmount         numeric(19,5)  = 0,
                @I_vPAOvhdAmtPerUnit       numeric(19,5)  = 0,
                @I_vPAOverheaPercentage    numeric(19,5)  = 0,
                @I_vPAVAROVRHDUNIT         numeric(19,5)  = 0,
                @I_vPAVAROVERHDPRCNT       numeric(19,5)  = 0,
                @I_vPACOSTATUS             smallint  = 0,
                @I_vPAGBTRKCHG             smallint,
                @I_vPASales_Tax_Options    smallint  = 0,
                @I_vPAbilltaxscheduleid    char(15)  = '',
                @I_vPAPurchase_Tax_Options smallint  = 0,
                @I_vPAPURCHTAXSCHED        char(15)  = '',
                @I_vRequesterTrx           smallint  = 0,
                @I_vUSRDEFND1              char(50)  = '',
                @I_vUSRDEFND2              char(50)  = '',
                @I_vUSRDEFND3              char(50)  = '',
                @I_vUSRDEFND4              varchar(8000)  = '',
                @I_vUSRDEFND5              varchar(8000)  = '',
                @O_iErrorState             int  output,
                @oErrString                varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @PACOSTCATID              char(15),
           @PAIV_Item_Checkbox       smallint,
           @l_PAPROJNUMBER           char(15),
           @PASTAT                   smallint,
           @PACHGORDNO               char(17),
           @DECPLQTY                 smallint,
           @DECPLCUR                 smallint,
           @PAUnit_of_Measure        char(9),
           @UOMSCHDL                 char(11),
           @PAProfitType             smallint,
           @PAbllngtype              smallint,
           @PATU                     smallint,
           @PAProject_Amount         numeric(19,5),
           @PAPREVPROJAMT            numeric(19,5),
           @PAPay_Code_Hourly        char(7),
           @PAPay_Code_Salary        char(7),
           @PAPREVPYCDESLY           char(7),
           @PAPREVPYCODEHOURLY       char(7),
           @PAORIGBUDGTAMT           numeric(19,5),
           @PABQuantity              numeric(19,5),
           @PABUnitCost              numeric(19,5),
           @PABTotalCost             numeric(19,5),
           @PABBeginDate             datetime,
           @PABEndDate               datetime,
           @PABProfitAmt             numeric(19,5),
           @PABProfitPcnt            numeric(19,5),
           @PABBillings              numeric(19,5),
           @PABProfit                numeric(19,5),
           @PABaselineOvhdAmtPerUnit numeric(19,5),
           @PABaselineOvrhdPct       numeric(19,5),
           @PABaseOvhdCost           numeric(19,5),
           @PABaselinePTaxOptions    smallint,
           @PABaselineCTaxSchedID    char(15),
           @PABaselineSTaxOptions    smallint,
           @PABaselineBTaxSchedID    char(15),
           @PABase_Qty               numeric(19,5),
           @PABase_Unit_Cost         numeric(19,5),
           @PAFQuantity              numeric(19,5),
           @PAFUnitCost              numeric(19,5),
           @PAFTotalCost             numeric(19,5),
           @PAFBeginDate             datetime,
           @PAFEndDate               datetime,
           @PAFProfitAmt             numeric(19,5),
           @PAFProfitPcnt            numeric(19,5),
           @PAFBillings              numeric(19,5),
           @PAFProfit                numeric(19,5),
           @PAForecastOvrhdAmtPerUnt numeric(19,5),
           @PAForecastOvrhdPct       numeric(19,5),
           @PAForecastOvhdCost       numeric(19,5),
           @PAForecastPTaxOptions    smallint,
           @PAForecastCTaxSchedID    char(15),
           @PAForecastSTaxOptions    smallint,
           @PAForecastBTaxSchedID    char(15),
           @PAForecastBaseOvrhdAmt   numeric(19,5),
           @PAForecastBaseProfitAmt  numeric(19,5),
           @PAForecastBaseQty        numeric(19,5),
           @PAForecastBaseUnitCost   numeric(19,5),
           @PAQtyQ                   numeric(19,5),
           @PAUNITCOST               numeric(19,5),
           @PATOTCST                 numeric(19,5),
           @PAbegindate              datetime,
           @PAPREVBEGINDT            datetime,
           @PAEnDate                 datetime,
           @PAPREVENDDT              datetime,
           @PAProfitAmount           numeric(19,5),
           @PAProfitPercent          numeric(19,5),
           @PAtotbillings            numeric(19,5),
           @PATotalProfit            numeric(19,5),
           @PAOvhdAmtPerUnit         numeric(19,5),
           @PAOverheaPercentage      numeric(19,5),
           @PATOTALOVERH             numeric(19,5),
           @PAPurchase_Tax_Options   smallint,
           @PAPURCHTAXSCHED          char(15),
           @PASales_Tax_Options      smallint,
           @PAbilltaxscheduleid      char(15),
           @PABaseProfitAmount       numeric(19,5),
           @PAPREVBSOVERHDCST        numeric(19,5),
           @PAPREVBSPROFAMT          numeric(19,5),
           @PAPREVBASEQTY            numeric(19,5),
           @PAPREVBSEUNTCST          numeric(19,5),
           @PAPREVBILLTXSCHED        char(15),
           @PAPREVCOSTXSCHED         char(15),
           @PAPREVPURCHTXOPT         smallint,
           @PAPREVSALESTXOPT         smallint,
           @PAPREVOVEHDUNIT          numeric(19,5),
           @PAPREVOVRHDPRCNT         numeric(19,5),
           @PAPRVPROFITAMT           numeric(19,5),
           @PAPREVPROFPRCNT          numeric(19,5),
           @PAPREVQTY                numeric(19,5),
           @PAPREVTOTBILL            numeric(19,5),
           @PAPREVTOTCOST            numeric(19,5),
           @PAPREVTOTOVEHD           numeric(19,5),
           @PAPREVTOTPROF            numeric(19,5),
           @PAPREVUNITCOST           numeric(19,5),
           @iCustomState             int,
           @iCustomErrString         varchar(255),
           @iStatement               int,
           @iStatus                  int,
           @iAddCodeErrState         int,
           @iError                   int,
           @O_oErrorState            int,
           @DBName                   char(50),
           @O_iInitErrorState        int,
           @oInitErrString           varchar(255),
           @PATRKCHGORDS             smallint,
           @PACOTRKCHGBY             smallint,
           @PAPROJNUMBER             char(15),
           @PAProjectType            smallint,
           @PAAcctgMethod            smallint,
           @PACONTNUMBER             char(11),
           @CUSTNMBR                 char(15),
           @PAVAROVERHDAMT           numeric(19,5),
           @PAVARTOTCOST             numeric(19,5),
           @l_old_billings           numeric(19,5),
           @l_new_billing            numeric(19,5),
           @l_Billings               numeric(19,5),
           @l_Profit                 numeric(19,5),
           @PAVARTOTBILLINGS         numeric(19,5),
           @l_Proj                   numeric(19,5),
           @PAVARTOTLPROF            numeric(19,5),
           @PADOCSTATUS              smallint,
           @PALineItemSeq            integer,
           @PACOTYPE                 smallint,
           @PAVARPROJAMT             numeric(19,5),
           @PACOSTATUS               smallint,
           @PAROGPROJAMT             numeric(19,5)
  
  select @PACOSTCATID = '',
         @PAIV_Item_Checkbox = 0,
         @l_PAPROJNUMBER = '',
         @PASTAT = 0,
         @PACHGORDNO = '',
         @DECPLQTY = 0,
         @DECPLCUR = 0,
         @PAUnit_of_Measure = '',
         @UOMSCHDL = '',
         @PAProfitType = 0,
         @PAbllngtype = 0,
         @PATU = 0,
         @PAProject_Amount = 0,
         @PAPREVPROJAMT = 0,
         @PAPay_Code_Hourly = '',
         @PAPay_Code_Salary = '',
         @PAPREVPYCDESLY = '',
         @PAPREVPYCODEHOURLY = '',
         @PAORIGBUDGTAMT = 0,
         @PABQuantity = 0,
         @PABUnitCost = 0,
         @PABTotalCost = 0,
         @PABBeginDate = '',
         @PABEndDate = '',
         @PABProfitAmt = 0,
         @PABProfitPcnt = 0,
         @PABBillings = 0,
         @PABProfit = 0,
         @PABaselineOvhdAmtPerUnit = 0,
         @PABaselineOvrhdPct = 0,
         @PABaseOvhdCost = 0,
         @PABaselinePTaxOptions = 0,
         @PABaselineCTaxSchedID = '',
         @PABaselineSTaxOptions = 0,
         @PABaselineBTaxSchedID = '',
         @PABase_Qty = 0,
         @PABase_Unit_Cost = 0,
         @PAFQuantity = 0,
         @PAFUnitCost = 0,
         @PAFTotalCost = 0,
         @PAFBeginDate = '',
         @PAFEndDate = '',
         @PAFProfitAmt = 0,
         @PAFProfitPcnt = 0,
         @PAFBillings = 0,
         @PAFProfit = 0,
         @PAForecastOvrhdAmtPerUnt = 0,
         @PAForecastOvrhdPct = 0,
         @PAForecastOvhdCost = 0,
         @PAForecastPTaxOptions = 0,
         @PAForecastCTaxSchedID = '',
         @PAForecastSTaxOptions = 0,
         @PAForecastBTaxSchedID = '',
         @PAForecastBaseOvrhdAmt = 0,
         @PAForecastBaseProfitAmt = 0,
         @PAForecastBaseQty = 0,
         @PAForecastBaseUnitCost = 0,
         @PAQtyQ = 0,
         @PAUNITCOST = 0,
         @PATOTCST = 0,
         @PAbegindate = '',
         @PAPREVBEGINDT = '',
         @PAEnDate = '',
         @PAPREVENDDT = '',
         @PAProfitAmount = 0,
         @PAProfitPercent = 0,
         @PAtotbillings = 0,
         @PATotalProfit = 0,
         @PAOvhdAmtPerUnit = 0,
         @PAOverheaPercentage = 0,
         @PATOTALOVERH = 0,
         @PAPurchase_Tax_Options = 0,
         @PAPURCHTAXSCHED = '',
         @PASales_Tax_Options = 0,
         @PAbilltaxscheduleid = '',
         @PABaseProfitAmount = 0,
         @PAPREVBSOVERHDCST = 0,
         @PAPREVBSPROFAMT = 0,
         @PAPREVBASEQTY = 0,
         @PAPREVBSEUNTCST = 0,
         @PAPREVBILLTXSCHED = '',
         @PAPREVCOSTXSCHED = '',
         @PAPREVPURCHTXOPT = 0,
         @PAPREVSALESTXOPT = 0,
         @PAPREVOVEHDUNIT = 0,
         @PAPREVOVRHDPRCNT = 0,
         @PAPRVPROFITAMT = 0,
         @PAPREVPROFPRCNT = 0,
         @PAPREVQTY = 0,
         @PAPREVTOTBILL = 0,
         @PAPREVTOTCOST = 0,
         @PAPREVTOTOVEHD = 0,
         @PAPREVTOTPROF = 0,
         @PAPREVUNITCOST = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @iStatement = 0,
         @iStatus = 0,
         @iAddCodeErrState = 0,
         @iError = 0,
         @O_oErrorState = 0,
         @DBName = '',
         @O_iInitErrorState = 0,
         @oInitErrString = '',
         @PATRKCHGORDS = 0,
         @PACOTRKCHGBY = 0,
         @PAPROJNUMBER = '',
         @PAProjectType = 0,
         @PAAcctgMethod = 0,
         @PACONTNUMBER = '',
         @CUSTNMBR = '',
         @PAVAROVERHDAMT = 0,
         @PAVARTOTCOST = 0,
         @l_old_billings = 0,
         @l_new_billing = 0,
         @l_Billings = 0,
         @l_Profit = 0,
         @PAVARTOTBILLINGS = 0,
         @l_Proj = 0,
         @PAVARTOTLPROF = 0,
         @PADOCSTATUS = 0,
         @PALineItemSeq = 0,
         @PACOTYPE = 0,
         @PAVARPROJAMT = 0,
         @PACOSTATUS = 0,
         @O_iErrorState = 0,
         @PAROGPROJAMT = 0
  
  select @DBName = DB_Name()
  
  exec @iStatus = taPAChangeOrderEntryBudgetPre
    @I_vPACHGORDNO output ,
    @I_vPACONTNUMBER output ,
    @I_vPAPROJNUMBER output ,
    @I_vPACOSTCATID output ,
    @I_vLNITMSEQ output ,
    @I_vPAbegindate output ,
    @I_vPAEnDate output ,
    @I_vPAVarianceQty output ,
    @I_vPAQtyQ output ,
    @I_vPAVarianceCosts output ,
    @I_vPAUNITCOST output ,
    @I_vPAVARMUPRCNT output ,
    @I_vPAProfitPercent output ,
    @I_vPAVARPROFAMT output ,
    @I_vPAProfitAmount output ,
    @I_vPAOvhdAmtPerUnit output ,
    @I_vPAOverheaPercentage output ,
    @I_vPAVAROVRHDUNIT output ,
    @I_vPAVAROVERHDPRCNT output ,
    @I_vPACOSTATUS output ,
    @I_vPAGBTRKCHG output ,
    @I_vPASales_Tax_Options output ,
    @I_vPAbilltaxscheduleid output ,
    @I_vPAPurchase_Tax_Options output ,
    @I_vPAPURCHTAXSCHED output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@oErrString))
      
      select @O_iErrorState = 2448
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPACHGORDNO is null 
       or @I_vPACONTNUMBER is null
       or @I_vLNITMSEQ is null
       or @I_vPAPROJNUMBER is null
       or @I_vPACOSTCATID is null
       or @I_vPAbegindate is null
       or @I_vPAEnDate is null
       or @I_vPAVarianceQty is null
       or @I_vPAQtyQ is null
       or @I_vPAVarianceCosts is null
       or @I_vPAUNITCOST is null
       or @I_vPAVARMUPRCNT is null
       or @I_vPAProfitPercent is null
       or @I_vPAVARPROFAMT is null
       or @I_vPAProfitAmount is null
       or @I_vPACOSTATUS is null
       or @I_vPAGBTRKCHG is null
       or @I_vRequesterTrx is null)
    begin
      select @O_iErrorState = 2449
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPACHGORDNO = ''
       or @I_vPACONTNUMBER = ''
       or @I_vPAPROJNUMBER = ''
       or @I_vPACOSTCATID = '')
    begin
      select @O_iErrorState = 2450
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vPACHGORDNO = upper(@I_vPACHGORDNO),
         @I_vPACONTNUMBER = upper(@I_vPACONTNUMBER),
         @I_vPAPROJNUMBER = upper(@I_vPAPROJNUMBER),
         @I_vPACOSTCATID = upper(@I_vPACOSTCATID)
  
  select @PACOTRKCHGBY = @I_vPAGBTRKCHG + 1
  
  if (@I_vPAVarianceQty > 0)
     and (@I_vPAQtyQ > 0)
    begin
      select @O_iErrorState = 2451
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAVarianceCosts > 0)
     and (@I_vPAUNITCOST > 0)
    begin
      select @O_iErrorState = 2452
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAVARPROFAMT > 0)
     and (@I_vPAProfitAmount > 0)
    begin
      select @O_iErrorState = 2453
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAGBTRKCHG not in (0,1))
    begin
      select @O_iErrorState = 8689
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAPROJNUMBER <> '')
    begin
      select @PAPROJNUMBER = PAPROJNUMBER,
             @PAProjectType = PAProjectType,
             @PAAcctgMethod = PAAcctgMethod,
             @PACONTNUMBER = PACONTNUMBER,
             @CUSTNMBR = CUSTNMBR,
             @PAbllngtype = PAbllngtype,
             @PAROGPROJAMT = PAProject_Amount
      from   PA01201 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@PAPROJNUMBER = '')
        begin
          select @O_iErrorState = 2454
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@PASTAT = 3)
           or (@PASTAT = 5))
        begin
          select @O_iErrorState = 2455
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @PAPROJNUMBER = PAPROJNUMBER,
             @PATRKCHGORDS = PATRKCHGORDS
      from   PA01241 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@PATRKCHGORDS = 0)
        begin
          select @O_iErrorState = 2456
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPACOSTCATID <> '')
    begin
      select @PACOSTCATID = PACOSTCATID,
             @PATU = PATU,
             @PAIV_Item_Checkbox = PAIV_Item_Checkbox
      from   PA01001 (nolock)
      where  PACOSTCATID = @I_vPACOSTCATID
      
      if (@PACOSTCATID = '')
        begin
          select @O_iErrorState = 2457
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @l_PAPROJNUMBER = PAPROJNUMBER,
             @PASTAT = PASTAT,
             @DECPLQTY = DECPLQTY,
             @DECPLCUR = DECPLCUR,
             @PAUnit_of_Measure = PAUnit_of_Measure,
             @UOMSCHDL = UOMSCHDL,
             @PAProfitType = PAProfitType,
             @PAbllngtype = PAbllngtype,
             @PATU = PATU,
             @PAProject_Amount = PAProject_Amount,
             @PAPREVPROJAMT = PAProject_Amount,
             @PAPay_Code_Hourly = PAPay_Code_Hourly,
             @PAPay_Code_Salary = PAPay_Code_Salary,
             @PAPREVPYCDESLY = PAPay_Code_Salary,
             @PAPREVPYCODEHOURLY = PAPay_Code_Hourly,
             @PAORIGBUDGTAMT = PAProject_Amount,
             @PABQuantity = PABQuantity,
             @PABUnitCost = PABUnitCost,
             @PABTotalCost = PABTotalCost,
             @PABBeginDate = PABBeginDate,
             @PABEndDate = PABEndDate,
             @PABProfitAmt = PABProfitAmt,
             @PABProfitPcnt = PABProfitPcnt,
             @PABBillings = PABBillings,
             @PABProfit = PABProfit,
             @PABaselineOvhdAmtPerUnit = PABaselineOvhdAmtPerUnit,
             @PABaselineOvrhdPct = PABaselineOvrhdPct,
             @PABaseOvhdCost = PABaseOvhdCost,
             @PABaselinePTaxOptions = PABaselinePTaxOptions,
             @PABaselineCTaxSchedID = PABaselineCTaxSchedID,
             @PABaselineSTaxOptions = PABaselineSTaxOptions,
             @PABaselineBTaxSchedID = PABaselineBTaxSchedID,
             @PABaseOvhdCost = PABaseOvhdCost,
             @PABProfitAmt = PABProfitAmt,
             @PABase_Qty = PABase_Qty,
             @PABase_Unit_Cost = PABase_Unit_Cost,
             @PAFQuantity = PAFQuantity,
             @PAFUnitCost = PAFUnitCost,
             @PAFTotalCost = PAFTotalCost,
             @PAFBeginDate = PAFBeginDate,
             @PAFEndDate = PAFEndDate,
             @PAFProfitAmt = PAFProfitAmt,
             @PAFProfitPcnt = PAFProfitPcnt,
             @PAFBillings = PAFBillings,
             @PAFProfit = PAFProfit,
             @PAForecastOvrhdAmtPerUnt = PAForecastOvrhdAmtPerUnt,
             @PAForecastOvrhdPct = PAForecastOvrhdPct,
             @PAForecastOvhdCost = PAForecastOvhdCost,
             @PAForecastPTaxOptions = PAForecastPTaxOptions,
             @PAForecastCTaxSchedID = PAForecastCTaxSchedID,
             @PAForecastSTaxOptions = PAForecastSTaxOptions,
             @PAForecastBTaxSchedID = PAForecastBTaxSchedID,
             @PAForecastBaseOvrhdAmt = PAForecastBaseOvrhdAmt,
             @PAForecastBaseProfitAmt = PAForecastBaseProfitAmt,
             @PAForecastBaseQty = PAForecastBaseQty,
             @PAForecastBaseUnitCost = PAForecastBaseUnitCost
      from   PA01301 (nolock)
      where  PACOSTCATID = @I_vPACOSTCATID
             and PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@I_vPAGBTRKCHG = 0)
        begin
          select @PAQtyQ = @PABQuantity,
                 @PAUNITCOST = @PABUnitCost,
                 @PATOTCST = @PABTotalCost,
                 @PAbegindate = @PABBeginDate,
                 @PAPREVBEGINDT = @PABBeginDate,
                 @PAEnDate = @PABEndDate,
                 @PAPREVENDDT = @PABEndDate,
                 @PAProfitAmount = @PABProfitAmt,
                 @PAProfitPercent = @PABProfitPcnt,
                 @PAtotbillings = @PABBillings,
                 @PATotalProfit = @PABProfit,
                 @PAOvhdAmtPerUnit = @PABaselineOvhdAmtPerUnit,
                 @PAOverheaPercentage = @PABaselineOvrhdPct,
                 @PATOTALOVERH = @PABaseOvhdCost,
                 @PAPurchase_Tax_Options = @PABaselinePTaxOptions,
                 @PAPURCHTAXSCHED = @PABaselineCTaxSchedID,
                 @PASales_Tax_Options = @PABaselineSTaxOptions,
                 @PAbilltaxscheduleid = @PABaselineBTaxSchedID,
                 @PABaseOvhdCost = @PABaseOvhdCost,
                 @PABaseProfitAmount = @PABProfitAmt,
                 @PABase_Qty = @PABase_Qty,
                 @PABase_Unit_Cost = @PABase_Unit_Cost,
                 @PAPREVBSOVERHDCST = @PABaseOvhdCost,
                 @PAPREVBSPROFAMT = @PABProfitAmt,
                 @PAPREVBASEQTY = @PABase_Qty,
                 @PAPREVBSEUNTCST = @PABase_Unit_Cost,
                 @PAPREVBILLTXSCHED = @PABaselineBTaxSchedID,
                 @PAPREVCOSTXSCHED = @PABaselineCTaxSchedID,
                 @PAPREVPURCHTXOPT = @PABaselinePTaxOptions,
                 @PAPREVSALESTXOPT = @PABaselineSTaxOptions,
                 @PAPREVOVEHDUNIT = @PABaselineOvhdAmtPerUnit,
                 @PAPREVOVRHDPRCNT = @PABaselineOvrhdPct,
                 @PAPRVPROFITAMT = @PABProfitAmt,
                 @PAPREVPROFPRCNT = @PABProfitPcnt,
                 @PAPREVQTY = @PABase_Qty,
                 @PAPREVTOTBILL = @PABBillings,
                 @PAPREVTOTCOST = @PABTotalCost,
                 @PAPREVTOTOVEHD = @PABaseOvhdCost,
                 @PAPREVTOTPROF = @PABProfit,
                 @PAPREVUNITCOST = @PABase_Unit_Cost
        end
      else
        begin
          select @PAQtyQ = @PAFQuantity,
                 @PAUNITCOST = @PAFUnitCost,
                 @PATOTCST = @PAFTotalCost,
                 @PAbegindate = @PAFBeginDate,
                 @PAPREVBEGINDT = @PAFBeginDate,
                 @PAEnDate = @PAFEndDate,
                 @PAPREVENDDT = @PAFEndDate,
                 @PAProfitAmount = @PAFProfitAmt,
                 @PAProfitPercent = @PAFProfitPcnt,
                 @PAtotbillings = @PAFBillings,
                 @PATotalProfit = @PAFProfit,
                 @PAOvhdAmtPerUnit = @PAForecastOvrhdAmtPerUnt,
                 @PAOverheaPercentage = @PAForecastOvrhdPct,
                 @PATOTALOVERH = @PAForecastOvhdCost,
                 @PAPurchase_Tax_Options = @PAForecastPTaxOptions,
                 @PAPURCHTAXSCHED = @PAForecastCTaxSchedID,
                 @PASales_Tax_Options = @PAForecastSTaxOptions,
                 @PAbilltaxscheduleid = @PAForecastBTaxSchedID,
                 @PABaseOvhdCost = @PAForecastBaseOvrhdAmt,
                 @PABaseProfitAmount = @PAForecastBaseProfitAmt,
                 @PABase_Qty = @PAForecastBaseQty,
                 @PABase_Unit_Cost = @PAForecastBaseUnitCost,
                 @PAPREVBSOVERHDCST = @PAForecastBaseOvrhdAmt,
                 @PAPREVBSPROFAMT = @PAForecastBaseProfitAmt,
                 @PAPREVBASEQTY = @PAForecastBaseQty,
                 @PAPREVBSEUNTCST = @PAForecastBaseUnitCost,
                 @PAPREVBILLTXSCHED = @PAForecastBTaxSchedID,
                 @PAPREVCOSTXSCHED = @PAForecastCTaxSchedID,
                 @PAPREVPURCHTXOPT = @PAForecastPTaxOptions,
                 @PAPREVSALESTXOPT = @PAForecastSTaxOptions,
                 @PAPREVOVEHDUNIT = @PAForecastOvrhdAmtPerUnt,
                 @PAPREVOVRHDPRCNT = @PAForecastOvrhdPct,
                 @PAPRVPROFITAMT = @PAFProfitAmt,
                 @PAPREVPROFPRCNT = @PAFProfitPcnt,
                 @PAPREVQTY = @PAFQuantity,
                 @PAPREVTOTBILL = @PAFBillings,
                 @PAPREVTOTCOST = @PAFTotalCost,
                 @PAPREVTOTOVEHD = @PAForecastOvhdCost,
                 @PAPREVTOTPROF = @PAFProfit,
                 @PAPREVUNITCOST = @PAFUnitCost
        end
      
      if (@I_vPAQtyQ = 0)
         and (@PAQtyQ > 0)
        select @I_vPAQtyQ = @PAQtyQ
      
      if (@I_vPAVarianceQty = 0)
         and (@I_vPAQtyQ > 0)
        select @I_vPAVarianceQty = (@I_vPAQtyQ - @PAQtyQ)
      else
        select @I_vPAQtyQ = (@I_vPAVarianceQty + @PAQtyQ)
      
      if (@I_vPAUNITCOST = 0)
         and (@PAUNITCOST > 0)
        select @I_vPAUNITCOST = @PAUNITCOST
      
      if (@I_vPAVarianceCosts = 0)
         and (@I_vPAUNITCOST > 0)
        select @I_vPAVarianceCosts = (@I_vPAUNITCOST - @PAUNITCOST)
      else
        select @I_vPAUNITCOST = (@I_vPAVarianceCosts + @PAUNITCOST)
      
      if (@I_vPAProfitAmount = 0)
         and (@PAProfitAmount > 0)
        select @I_vPAProfitAmount = @PAProfitAmount
      
      if (@I_vPAVARPROFAMT = 0)
         and (@I_vPAProfitAmount > 0)
        select @I_vPAVARPROFAMT = (@I_vPAProfitAmount - @PAProfitAmount)
      else
        select @I_vPAProfitAmount = (@I_vPAVARPROFAMT + @PAProfitAmount)
      
      if (@I_vPAbegindate = '')
         and (@PAbegindate <> '')
        select @I_vPAbegindate = @PAbegindate
      
      if (@I_vPAEnDate = '')
         and (@PAEnDate <> '')
        select @I_vPAEnDate = @PAEnDate
      
      select @PATOTALOVERH = round(((@I_vPAQtyQ * @I_vPAOvhdAmtPerUnit) + (@I_vPAQtyQ * @I_vPAUNITCOST * @PAOverheaPercentage / 100)),
                                   @DECPLCUR),
             @PAVAROVERHDAMT = @PATOTALOVERH - round(((@I_vPAQtyQ - @I_vPAVarianceQty) * (@I_vPAOvhdAmtPerUnit - @I_vPAVAROVRHDUNIT)) + ((@I_vPAQtyQ - @I_vPAVarianceQty) * (@I_vPAUNITCOST - @I_vPAVarianceCosts) * (@PAOverheaPercentage - @I_vPAVAROVERHDPRCNT) / 100),
                                                     @DECPLCUR),
             @PATOTCST = round(@I_vPAQtyQ * @I_vPAUNITCOST,@DECPLCUR) + @PATOTALOVERH,
             @PAVARTOTCOST = @PATOTCST - round((@I_vPAQtyQ - @I_vPAVarianceQty) * (@I_vPAUNITCOST - @I_vPAVarianceCosts),
                                               @DECPLCUR) - @PATOTALOVERH + @PAVAROVERHDAMT
      
      if (@l_PAPROJNUMBER = '')
        begin
          select @O_iErrorState = 2458
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      else
        begin
          if (@PASTAT = 3)
              or (@PASTAT = 5)
            begin
              select @O_iErrorState = 2459
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      
      if (@PACHGORDNO <> '')
        begin
          select @PACOTYPE = PACOTYPE,
                 @PACOSTATUS = PACOSTATUS,
                 @PADOCSTATUS = PADOCSTATUS
          from   PA12000 (nolock)
          where  PACHGORDNO = @I_vPACHGORDNO
                 and PACONTNUMBER = @I_vPACONTNUMBER
          
          if (@PADOCSTATUS < 5)
            begin
              select @O_iErrorState = 2460
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      
      if (@I_vPACOSTATUS = 1)
        begin
          if exists (select 1
                     from   PA10901 (nolock)
                     where  PAPROJNUMBER = @I_vPAPROJNUMBER
                            and PACOSTCATID = @I_vPACOSTCATID)
            begin
              select @O_iErrorState = 2461
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if exists (select 1
                     from   PA10901 (nolock)
                     where  PAPROJNUMBER = @I_vPAPROJNUMBER
                            and PACOSTCATID = @I_vPACOSTCATID)
            begin
              select @O_iErrorState = 2462
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if exists (select 1
                     from   PA30901 (nolock)
                     where  PAPROJNUMBER = @I_vPAPROJNUMBER
                            and PACOSTCATID = @I_vPACOSTCATID)
            begin
              select @O_iErrorState = 2463
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      
      if (@PATU = 4)
         and (@PAIV_Item_Checkbox = 1)
        begin
          if (@I_vPAVarianceQty > 0)
            begin
              select @O_iErrorState = 2464
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if (@I_vPAQtyQ > 0)
            begin
              select @O_iErrorState = 2465
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if (@I_vPAVarianceCosts > 0)
            begin
              select @O_iErrorState = 2466
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if (@I_vPAUNITCOST > 0)
            begin
              select @O_iErrorState = 2467
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      
      select @l_Proj = @PAProject_Amount,
             @l_Billings = @PAtotbillings,
             @l_Profit = @PATotalProfit
      
      if @PACOTRKCHGBY = 1
        begin
          if (@PATU <> 4)
              or (@PAIV_Item_Checkbox <> 1)
            begin
              select @PAtotbillings = case 
                                        when @PAProfitType = 1
                                             and @PACOTRKCHGBY = 2 then round(@PAFQuantity * @PAFProfitAmt,@DECPLCUR)
                                        when @PAProfitType = 1
                                             and @PACOTRKCHGBY = 1 then round(@I_vPAQtyQ * @I_vPAProfitAmount,@DECPLCUR)
                                        when @PAProfitType = 3
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 2 then round((@PAFQuantity * @PAFUnitCost) + (@I_vPAQtyQ * @I_vPAProfitAmount) + @PAForecastOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 3
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 1 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 3
                                             and @PAProjectType = 3 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 3
                                             and @PAProjectType = 1
                                             and @PACOTRKCHGBY = 2 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 3
                                             and @PAProjectType = 1
                                             and @PACOTRKCHGBY = 1 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 4
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 2 then round(@PAFQuantity * (@PAFUnitCost + @I_vPAProfitAmount) + @PAForecastOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 4
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 1 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 4
                                             and @PAProjectType <> 2
                                             and @PACOTRKCHGBY = 2 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 4
                                             and @PAProjectType <> 2
                                             and @PACOTRKCHGBY = 1 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 5
                                             and @PACOTRKCHGBY = 2
                                             and @PAProjectType = 2 then round((@PAFQuantity * @PAFUnitCost) + @I_vPAProfitAmount + @PAForecastOvhdCost,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 5
                                             and @PACOTRKCHGBY = 2
                                             and @PAProjectType = 1 then round((@I_vPAQtyQ * @I_vPAUNITCOST) + @I_vPAProfitAmount + @PATOTALOVERH,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 5
                                             and @PACOTRKCHGBY = 2
                                             and @PAProjectType = 1 then round((@I_vPAQtyQ * @I_vPAUNITCOST) + @I_vPAProfitAmount + @PATOTALOVERH,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 5
                                             and @PAProjectType = 2 then round((@I_vPAQtyQ * @I_vPAUNITCOST) + @I_vPAProfitAmount + @PATOTALOVERH,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 5
                                             and @PAProjectType <> 2 then round((@I_vPAQtyQ * @I_vPAUNITCOST) + @I_vPAProfitAmount + @PATOTALOVERH,
                                                                                @DECPLCUR)
                                        when @PAProfitType = 8 then 0
                                        when @PAProfitType = 2
                                             and @PACOTRKCHGBY = 2 then round(@PAFQuantity * (1 + @PAFProfitPcnt / 100) * @PAFUnitCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 2
                                             and @PACOTRKCHGBY = 1 then round(@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 6
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 2 then round((@PAFQuantity * (1 + @I_vPAProfitPercent / 100) * @PAFUnitCost) + @PAForecastOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 6
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 1 then round((@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 6
                                             and @PAProjectType = 3 then round((@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 6
                                             and @PAProjectType = 1
                                             and @PACOTRKCHGBY = 2 then round((@PAFQuantity * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 6
                                             and @PAProjectType = 1
                                             and @PACOTRKCHGBY = 1 then round((@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 7
                                             and @PACOTRKCHGBY = 2 then round((@PAFQuantity * (1 + @PAFProfitPcnt / 100) * @PAFUnitCost) + @PAForecastOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 7
                                             and @PACOTRKCHGBY = 1 then round((@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 9
                                             and @PACOTRKCHGBY = 2 then round(@PAFProfitAmt * @PAFQuantity,@DECPLCUR)
                                        when @PAProfitType = 9
                                             and @PACOTRKCHGBY = 1 then round(@I_vPAProfitAmount * @I_vPAQtyQ,@DECPLCUR)
                                      end
            end
          
          if @PAProjectType = 3
            select @PAProject_Amount = @PAtotbillings
          
          select @l_old_billings = @PAFBillings
          
          select @l_new_billing = case 
                                    when @PAProfitType = 1
                                         and @PACOTRKCHGBY + 1 = 2 then round(@PAFQuantity * @PAFProfitAmt,@DECPLCUR)
                                    when @PAProfitType = 1
                                         and @PACOTRKCHGBY + 1 = 1 then round(@I_vPAQtyQ * @I_vPAProfitAmount,@DECPLCUR)
                                    when @PAProfitType = 3
                                         and @PAProjectType = 2
                                         and @PACOTRKCHGBY + 1 = 2 then round((@PAFQuantity * @PAFUnitCost) + (@I_vPAQtyQ * @I_vPAProfitAmount) + @PAForecastOvhdCost,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 3
                                         and @PAProjectType = 2
                                         and @PACOTRKCHGBY + 1 = 1 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 3
                                         and @PAProjectType = 3 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                           @DECPLCUR)
                                    when @PAProfitType = 3
                                         and @PAProjectType = 1
                                         and @PACOTRKCHGBY + 1 = 2 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 3
                                         and @PAProjectType = 1
                                         and @PACOTRKCHGBY + 1 = 1 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 4
                                         and @PAProjectType = 2
                                         and @PACOTRKCHGBY + 1 = 2 then round(@PAFQuantity * (@PAFUnitCost + @I_vPAProfitAmount) + @PAForecastOvhdCost,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 4
                                         and @PAProjectType = 2
                                         and @PACOTRKCHGBY + 1 = 1 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 4
                                         and @PAProjectType <> 2
                                         and @PACOTRKCHGBY + 1 = 2 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 4
                                         and @PAProjectType <> 2
                                         and @PACOTRKCHGBY + 1 = 1 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @I_vPAProfitAmount) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 5
                                         and @PACOTRKCHGBY + 1 = 2
                                         and @PAProjectType = 2 then round((@PAFQuantity * @PAFUnitCost) + @I_vPAProfitAmount + @PAForecastOvhdCost,
                                                                           @DECPLCUR)
                                    when @PAProfitType = 5
                                         and @PACOTRKCHGBY + 1 = 2
                                         and @PAProjectType = 1 then round((@I_vPAQtyQ * @I_vPAUNITCOST) + @I_vPAProfitAmount + @PATOTALOVERH,
                                                                           @DECPLCUR)
                                    when @PAProfitType = 5
                                         and @PACOTRKCHGBY + 1 = 2
                                         and @PAProjectType = 1 then round((@I_vPAQtyQ * @I_vPAUNITCOST) + @I_vPAProfitAmount + @PATOTALOVERH,
                                                                           @DECPLCUR)
                                    when @PAProfitType = 5
                                         and @PAProjectType = 2 then round((@I_vPAQtyQ * @I_vPAUNITCOST) + @I_vPAProfitAmount + @PATOTALOVERH,
                                                                           @DECPLCUR)
                                    when @PAProfitType = 5
                                         and @PAProjectType <> 2 then round((@I_vPAQtyQ * @I_vPAUNITCOST) + @I_vPAProfitAmount + @PATOTALOVERH,
                                                                            @DECPLCUR)
                                    when @PAProfitType = 8 then 0
                                    when @PAProfitType = 2
                                         and @PACOTRKCHGBY + 1 = 2 then round(@PAFQuantity * (1 + @PAFProfitPcnt / 100) * @PAFUnitCost,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 2
                                         and @PACOTRKCHGBY + 1 = 1 then round(@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 6
                                         and @PAProjectType = 2
                                         and @PACOTRKCHGBY + 1 = 2 then round((@PAFQuantity * (1 + @I_vPAProfitPercent / 100) * @PAFUnitCost) + @PAForecastOvhdCost,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 6
                                         and @PAProjectType = 2
                                         and @PACOTRKCHGBY + 1 = 1 then round((@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 6
                                         and @PAProjectType = 3 then round((@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                           @DECPLCUR)
                                    when @PAProfitType = 6
                                         and @PAProjectType = 1
                                         and @PACOTRKCHGBY + 1 = 2 then round((@PAFQuantity * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 6
                                         and @PAProjectType = 1
                                         and @PACOTRKCHGBY + 1 = 1 then round((@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 7
                                         and @PACOTRKCHGBY + 1 = 2 then round((@PAFQuantity * (1 + @PAFProfitPcnt / 100) * @PAFUnitCost) + @PAForecastOvhdCost,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 7
                                         and @PACOTRKCHGBY + 1 = 1 then round((@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                    when @PAProfitType = 9
                                         and @PACOTRKCHGBY + 1 = 2 then round(@PAFProfitAmt * @PAFQuantity,@DECPLCUR)
                                    when @PAProfitType = 9
                                         and @PACOTRKCHGBY + 1 = 1 then round(@I_vPAProfitAmount * @I_vPAQtyQ,@DECPLCUR)
                                  end
          
          if @PAProjectType <> 3
            select @PAProject_Amount = @l_new_billing
        end
      else
        begin
          select @l_old_billings = @PAFBillings
          
          if (@PATU <> 4)
              or (@PAIV_Item_Checkbox <> 1)
            begin
              select @PAtotbillings = case 
                                        when @PAProfitType = 1
                                             and @PACOTRKCHGBY = 2 then round(@I_vPAQtyQ * @I_vPAProfitAmount,@DECPLCUR)
                                        when @PAProfitType = 1
                                             and @PACOTRKCHGBY = 1 then round(@PABase_Qty * @PABProfitAmt,@DECPLCUR)
                                        when @PAProfitType = 3
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 2 then round((@I_vPAQtyQ * @I_vPAUNITCOST) + (@PABase_Qty * @PABProfitAmt) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 3
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 1 then round(@PABase_Qty * (@PABase_Unit_Cost + @PABProfitAmt) + @PABaseOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 3
                                             and @PAProjectType = 3 then round(@PABase_Qty * (@PABase_Unit_Cost + @PABProfitAmt) + @PABaseOvhdCost,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 3
                                             and @PAProjectType = 1
                                             and @PACOTRKCHGBY = 2 then round(@PABase_Qty * (@PABase_Unit_Cost + @PABProfitAmt) + @PABaseOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 3
                                             and @PAProjectType = 1
                                             and @PACOTRKCHGBY = 1 then round(@PABase_Qty * (@PABase_Unit_Cost + @PABProfitAmt) + @PABaseOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 4
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 2 then round(@I_vPAQtyQ * (@I_vPAUNITCOST + @PABProfitAmt) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 4
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 1 then round(@PABase_Qty * (@PABase_Unit_Cost + @PABProfitAmt) + @PABaseOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 4
                                             and @PAProjectType <> 2
                                             and @PACOTRKCHGBY = 2 then round(@PABase_Qty * (@PABase_Unit_Cost + @PABProfitAmt) + @PABaseOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 4
                                             and @PAProjectType <> 2
                                             and @PACOTRKCHGBY = 1 then round(@PABase_Qty * (@PABase_Unit_Cost + @PABProfitAmt) + @PABaseOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 5
                                             and @PACOTRKCHGBY = 2
                                             and @PAProjectType = 2 then round((@I_vPAQtyQ * @I_vPAUNITCOST) + @PABProfitAmt + @PATOTALOVERH,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 5
                                             and @PACOTRKCHGBY = 2
                                             and @PAProjectType = 1 then round((@PABase_Qty * @PABase_Unit_Cost) + @PABProfitAmt + @PABaseOvhdCost,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 5
                                             and @PACOTRKCHGBY = 2
                                             and @PAProjectType = 1 then round((@PABase_Qty * @PABase_Unit_Cost) + @PABProfitAmt + @PABaseOvhdCost,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 5
                                             and @PAProjectType = 2 then round((@PABase_Qty * @PABase_Unit_Cost) + @PABProfitAmt + @PABaseOvhdCost,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 5
                                             and @PAProjectType <> 2 then round((@PABase_Qty * @PABase_Unit_Cost) + @PABProfitAmt + @PABaseOvhdCost,
                                                                                @DECPLCUR)
                                        when @PAProfitType = 8 then 0
                                        when @PAProfitType = 2
                                             and @PACOTRKCHGBY = 2 then round(@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 2
                                             and @PACOTRKCHGBY = 1 then round(@PABase_Qty * (1 + @PABProfitPcnt / 100) * @PABase_Unit_Cost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 6
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 2 then round((@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 6
                                             and @PAProjectType = 2
                                             and @PACOTRKCHGBY = 1 then round((@PABase_Qty * (1 + @I_vPAProfitPercent / 100) * @PABase_Unit_Cost) + @PABaseOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 6
                                             and @PAProjectType = 3 then round((@PABase_Qty * (1 + @PABProfitPcnt / 100) * @PABase_Unit_Cost) + @PABaseOvhdCost,
                                                                               @DECPLCUR)
                                        when @PAProfitType = 6
                                             and @PAProjectType = 1
                                             and @PACOTRKCHGBY = 2 then round((@I_vPAQtyQ * (1 + @PABProfitPcnt / 100) * @PABase_Unit_Cost) + @PABaseOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 6
                                             and @PAProjectType = 1
                                             and @PACOTRKCHGBY = 1 then round((@PABase_Qty * (1 + @PABProfitPcnt / 100) * @PABase_Unit_Cost) + @PABaseOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 7
                                             and @PACOTRKCHGBY = 2 then round((@I_vPAQtyQ * (1 + @I_vPAProfitPercent / 100) * @I_vPAUNITCOST) + @PATOTALOVERH,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 7
                                             and @PACOTRKCHGBY = 1 then round((@PABase_Qty * (1 + @PABProfitPcnt / 100) * @PABase_Unit_Cost) + @PABaseOvhdCost,
                                                                              @DECPLCUR)
                                        when @PAProfitType = 9
                                             and @PACOTRKCHGBY = 2 then round(@I_vPAProfitAmount * @I_vPAQtyQ,@DECPLCUR)
                                        when @PAProfitType = 9
                                             and @PACOTRKCHGBY = 1 then round(@PABProfitAmt * @PABase_Qty,@DECPLCUR)
                                        else @PAtotbillings
                                      end
            end
          
          if @PAProjectType = 3
            select @PAtotbillings = @l_old_billings
          
          if @PAProjectType <> 3
            select @PAProject_Amount = @PAtotbillings
        end
      
      select @PATotalProfit = @PAtotbillings - @PATOTCST,
             @PAVARTOTLPROF = @PATotalProfit - @l_Profit,
             @PAVARTOTBILLINGS = @PAtotbillings - @l_Billings,
             @PAVARPROJAMT = @PAProject_Amount - @l_Proj
    end
  
  if @I_vPAPurchase_Tax_Options = 0
    select @I_vPAPurchase_Tax_Options = 3
  
  if ((@I_vPAPurchase_Tax_Options < 1)
       or (@I_vPAPurchase_Tax_Options > 3))
    begin
      select @O_iErrorState = 2468
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPAPurchase_Tax_Options <> 1)
      and (@I_vPAPURCHTAXSCHED <> ''))
    begin
      select @O_iErrorState = 2469
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAPURCHTAXSCHED <> '')
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vPAPURCHTAXSCHED)
        begin
          select @O_iErrorState = 2470
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if @I_vPASales_Tax_Options = 0
    select @I_vPASales_Tax_Options = 3
  
  if ((@I_vPASales_Tax_Options < 1)
       or (@I_vPASales_Tax_Options > 3))
    begin
      select @O_iErrorState = 2471
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPASales_Tax_Options <> 1)
      and (@I_vPAbilltaxscheduleid <> ''))
    begin
      select @O_iErrorState = 2472
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAbilltaxscheduleid <> '')
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vPAbilltaxscheduleid)
        begin
          select @O_iErrorState = 3953
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  if not exists (select 1
                 from   PA12001 (nolock)
                 where  PACONTNUMBER = @I_vPACONTNUMBER
                        and PAPROJNUMBER = @I_vPAPROJNUMBER
                        and PACHGORDNO = @I_vPACHGORDNO)
     and not exists (select 1
                     from   PA12101 (nolock)
                     where  PACONTNUMBER = @I_vPACONTNUMBER
                            and PAPROJNUMBER = @I_vPAPROJNUMBER
                            and PACHGORDNO = @I_vPACHGORDNO)
    begin
      update PA01241
      set    PANOPENDCO = PANOPENDCO + 1,
             PANUMCO = PANUMCO + 1
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 5395
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_oErrorState)
        end
    end
  
  update PA01301
  set    PACHGORDNO = @I_vPACHGORDNO
  where  PAPROJNUMBER = @I_vPAPROJNUMBER
         and PACOSTCATID = @I_vPACOSTCATID
         and PATU = @PATU
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 9346
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_oErrorState)
    end
  
  insert into PA12001
             (PACONTNUMBER,
              PACHGORDNO,
              PACOTRKCHGBY,
              LNITMSEQ,
              PAPROJNUMBER,
              PATU,
              PACOSTCATID,
              PALineItemSeq,
              PAUnit_of_Measure,
              UOMSCHDL,
              PAPREVBEGINDT,
              PAbegindate,
              PAPREVENDDT,
              PAEnDate,
              PAPREVPYCDESLY,
              PAPay_Code_Salary,
              PAPREVPYCODEHOURLY,
              PAPay_Code_Hourly,
              PAProfitType,
              PAbllngtype,
              PAPREVPURCHTXOPT,
              PAPurchase_Tax_Options,
              PAPREVCOSTXSCHED,
              PAPURCHTAXSCHED,
              DECPLQTY,
              DECPLCUR,
              PAPREVSALESTXOPT,
              PASales_Tax_Options,
              PAPREVBILLTXSCHED,
              PAbilltaxscheduleid,
              PAVarianceQty,
              PAQtyQ,
              PAPREVQTY,
              PAVarianceCosts,
              PAUNITCOST,
              PAPREVUNITCOST,
              PAVARTOTCOST,
              PAVAROVERHDAMT,
              PATOTALOVERH,
              PAVAROVERHDPRCNT,
              PAOverheaPercentage,
              PAPREVOVRHDPRCNT,
              PAPREVTOTOVEHD,
              PAVAROVRHDUNIT,
              PAOvhdAmtPerUnit,
              PAPREVOVEHDUNIT,
              PATOTCST,
              PAPREVTOTCOST,
              PAVARMUPRCNT,
              PAProfitPercent,
              PAPREVPROFPRCNT,
              PAVARPROFAMT,
              PAProfitAmount,
              PAPRVPROFITAMT,
              PAVARTOTLPROF,
              PATotalProfit,
              PAPREVTOTPROF,
              PAVARTOTBILLINGS,
              PAtotbillings,
              PAPREVTOTBILL,
              PAORIGBUDGTAMT,
              PAROGPROJAMT,
              PAProject_Amount,
              PAPREVPROJAMT,
              PAVARPROJAMT,
              PAPREVBASEQTY,
              PABase_Qty,
              PAPREVBSEUNTCST,
              PABase_Unit_Cost,
              PAPREVBSOVERHDCST,
              PABaseOvhdCost,
              PAPREVBSPROFAMT,
              PABaseProfitAmount,
              PAPROPQUOTEAMT,
              PAFINQUOTEAMT,
              PAQUOTEPREPBY,
              PAQUOTEAPPRVBY,
              PAREVBUDGTAMT,
              PAREVPROJAMT,
              PAsequencenumber,
              APPRVLDT,
              PAClose_CB,
              PAPostedQty,
              PAPostedTotalCostN,
              PAPosted_Overhead,
              PAPostedProfitN,
              PAPosted_Tax_Amount,
              PAPosted_Accr_RevN,
              PAPostRecogRevN,
              PAPOSBIEEAMOUNT,
              PAPOSEIEBAMOUNT,
              PApostbillamt,
              PA_Actual_Receipts_Amoun,
              PAPostedBillingsN,
              PAPostedDiscDolAmtN,
              PAPosted_Sales_Tax_Amou,
              ActualWriteOffAmount,
              ActualDiscTakenAmount,
              PAPostedTaxPaidN,
              PAPOSTRETAMT,
              PAActualWOTaxAmt,
              PAActualTermsTakenTax)
  select @I_vPACONTNUMBER,
         @I_vPACHGORDNO,
         @PACOTRKCHGBY,
         @I_vLNITMSEQ,
         @I_vPAPROJNUMBER,
         @PATU,
         @I_vPACOSTCATID,
         @PALineItemSeq,
         @PAUnit_of_Measure,
         @UOMSCHDL,
         @PAPREVBEGINDT,
         @I_vPAbegindate,
         @PAPREVENDDT,
         @I_vPAEnDate,
         @PAPREVPYCDESLY,
         @PAPay_Code_Salary,
         @PAPREVPYCODEHOURLY,
         @PAPay_Code_Hourly,
         @PAProfitType,
         @PAbllngtype,
         @PAPREVPURCHTXOPT,
         @PAPurchase_Tax_Options,
         @PAPREVCOSTXSCHED,
         @PAPURCHTAXSCHED,
         @DECPLQTY,
         @DECPLCUR,
         @PAPREVSALESTXOPT,
         @PASales_Tax_Options,
         @PAPREVBILLTXSCHED,
         @PAbilltaxscheduleid,
         @I_vPAVarianceQty,
         @I_vPAQtyQ,
         @PAPREVQTY,
         @I_vPAVarianceCosts,
         @I_vPAUNITCOST,
         @PAPREVUNITCOST,
         @PAVARTOTCOST,
         @PAVAROVERHDAMT,
         @PATOTALOVERH,
         @I_vPAVAROVERHDPRCNT,
         @PAOverheaPercentage,
         @PAPREVOVRHDPRCNT,
         @PAPREVTOTOVEHD,
         @I_vPAVAROVRHDUNIT,
         @PAOvhdAmtPerUnit,
         @PAPREVOVEHDUNIT,
         @PATOTCST,
         @PAPREVTOTCOST,
         @I_vPAVARMUPRCNT,
         @I_vPAProfitPercent,
         @PAPREVPROFPRCNT,
         @I_vPAVARPROFAMT,
         @I_vPAProfitAmount,
         @PAPRVPROFITAMT,
         case 
           when @PAVARTOTLPROF = 0 then @I_vPAVARPROFAMT
           else @PAVARTOTLPROF
         end,
         @PATotalProfit + isnull(@I_vPAVARPROFAMT,0),
         @PAPREVTOTPROF,
         case 
           when @PAVARTOTBILLINGS = 0 then @I_vPAVARPROFAMT
           else @PAVARTOTBILLINGS
         end,
         @PAtotbillings + isnull(@I_vPAVARPROFAMT,0),
         @PAPREVTOTBILL,
         @PAORIGBUDGTAMT,
         @PAROGPROJAMT,
         @PAProject_Amount + isnull(@I_vPAVARPROFAMT,0),
         @PAPREVPROJAMT,
         @PAVARPROJAMT + isnull(@I_vPAVARPROFAMT,0),
         @PAPREVBASEQTY,
         @PABase_Qty,
         @PAPREVBSEUNTCST,
         @PABase_Unit_Cost,
         @PAPREVBSOVERHDCST,
         @PABaseOvhdCost,
         @PAPREVBSPROFAMT,
         @PABaseProfitAmount,
         0,
         0,
         '',
         '',
         @PAProject_Amount + isnull(@I_vPAVARPROFAMT,0),
         @PAROGPROJAMT + @PAVARPROJAMT + isnull(@I_vPAVARPROFAMT,0),
         1,
         '',
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0,
         0
  
  if @@error <> 0
    begin
      select @O_iErrorState = 2473
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  exec @iStatus = taPAChangeOrderEntryBudgetPost
    @I_vPACHGORDNO ,
    @I_vPACONTNUMBER ,
    @I_vPAPROJNUMBER ,
    @I_vPACOSTCATID ,
    @I_vLNITMSEQ ,
    @I_vPAbegindate ,
    @I_vPAEnDate ,
    @I_vPAVarianceQty ,
    @I_vPAQtyQ ,
    @I_vPAVarianceCosts ,
    @I_vPAUNITCOST ,
    @I_vPAVARMUPRCNT ,
    @I_vPAProfitPercent ,
    @I_vPAVARPROFAMT ,
    @I_vPAProfitAmount ,
    @I_vPAOvhdAmtPerUnit ,
    @I_vPAOverheaPercentage ,
    @I_vPAVAROVRHDUNIT ,
    @I_vPAVAROVERHDPRCNT ,
    @I_vPACOSTATUS ,
    @I_vPAGBTRKCHG ,
    @I_vPASales_Tax_Options ,
    @I_vPAbilltaxscheduleid ,
    @I_vPAPurchase_Tax_Options ,
    @I_vPAPURCHTAXSCHED ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@oErrString))
      
      select @O_iErrorState = 2474
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

