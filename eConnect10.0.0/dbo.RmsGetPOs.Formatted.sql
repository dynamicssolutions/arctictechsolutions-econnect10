

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetPOs]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetPOs]
  
create procedure [dbo].[RmsGetPOs]
                @iPOHdrTempTable  VARCHAR(255),
                @iPOLineTempTable VARCHAR(255),
                @iStartDate       DATETIME,
                @iEndDate         DATETIME,
                @iRmsServerName   VARCHAR(255),
                @iHqDbName        VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lPurchaseOrderTable VARCHAR(255)
  
  DECLARE  @lPurchaseOrderEntryTable VARCHAR(255)
  
  DECLARE  @lPOHdrTable VARCHAR(255)
  
  DECLARE  @lItemTable VARCHAR(255)
  
  DECLARE  @lSupplierTable VARCHAR(255)
  
  SELECT @lPurchaseOrderTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.PurchaseOrder as PurchaseOrder'
  
  SELECT @lPurchaseOrderEntryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.PurchaseOrderEntry as PurchaseOrderEntry'
  
  SELECT @lPOHdrTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.PurchaseOrder as PurchaseOrder'
  
  SELECT @lItemTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Item as Item'
  
  SELECT @lSupplierTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Supplier as Supplier'
  
  EXEC( 'INSERT INTO ' + @iPOHdrTempTable + ' (RmsPOID,  RmsStoreID,  RmsPONumber,  VENDORID,  DATE1,  FRTAMNT,  RmsCurrencyID,  XCHGRATE)  SELECT  PurchaseOrder.[ID],  PurchaseOrder.StoreID,  PurchaseOrder.PONumber,  ISNULL(LEFT(UPPER(Supplier.Code), 15),''''),  CONVERT(datetime,CONVERT(VARCHAR,PurchaseOrder.DateCreated,111)),  PurchaseOrder.Shipping,  ISNULL(Supplier.CurrencyID, ''''),  PurchaseOrder.ExchangeRate  FROM ' + @lPurchaseOrderTable + ' LEFT JOIN ' + @lSupplierTable + ' ON ' + ' PurchaseOrder.SupplierID = Supplier.ID    WHERE  (PurchaseOrder.POType = 0  OR  PurchaseOrder.POType = 1)  AND  CONVERT(DATETIME,CONVERT(VARCHAR,PurchaseOrder.DateCreated,111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  NOT EXISTS (SELECT  1  FROM  RMS30500  WHERE  PurchaseOrder.StoreID = RMS30500.RmsStoreID  AND  PurchaseOrder.[ID] = RMS30500.RmsPOID)')
  
  SELECT @lSqlError = @@ERROR
  
  IF @lSqlError <> 0
    RETURN
  
  IF @lSqlError = 0
    BEGIN
      EXEC( 'INSERT INTO ' + @iPOLineTempTable + ' (RmsStoreID,  RmsPOID,  RmsTransferLogID,  QTYORDER,  ITEMNMBR,  UNITCOST,  TAXAMNT)  SELECT  PurchaseOrderEntry.StoreID,  PurchaseOrderEntry.PurchaseOrderID,  PurchaseOrderEntry.[ID],  PurchaseOrderEntry.QuantityOrdered,  ISNULL(UPPER(Item.ItemLookupCode),0),  PurchaseOrderEntry.Price,  PurchaseOrderEntry.QuantityOrdered * PurchaseOrderEntry.Price * (PurchaseOrderEntry.TaxRate / 100)  FROM ' + @lItemTable + ' RIGHT OUTER JOIN ' + @lPurchaseOrderEntryTable + ' ON ' + ' Item.[ID] = PurchaseOrderEntry.ItemID  WHERE  EXISTS(SELECT  1  FROM ' + @iPOHdrTempTable + '  WHERE   PurchaseOrderEntry.[PurchaseOrderID] = ' + @iPOHdrTempTable + '.[RmsPOID]  AND   PurchaseOrderEntry.StoreID = ' + @iPOHdrTempTable + '.RmsStoreID)')
    END

