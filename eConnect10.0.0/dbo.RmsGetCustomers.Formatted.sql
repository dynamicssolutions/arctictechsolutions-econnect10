

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetCustomers]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetCustomers]
  
create procedure [dbo].[RmsGetCustomers]
                @iCustomerTempTable VARCHAR(255),
                @iRmsServerName     VARCHAR(255),
                @iHqDbName          VARCHAR(255),
                @iReImport          TINYINT  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lCustomerTable VARCHAR(255)
  
  DECLARE  @lShipToTable VARCHAR(255)
  
  SELECT @lCustomerTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Customer as Cust'
  
  SELECT @lShipToTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.ShipTo as Ship'
  
  EXEC( 'INSERT INTO ' + @iCustomerTempTable + '(CUSTNMBR,  RmsCustomerID,  CUSTNAME,  RmsPrimaryAddress1,  RmsPrimaryAddress2,  RmsPrimaryCity,  RmsPrimaryState,  RmsPrimaryZip,  RmsPrimaryCountry,  RmsPrimaryPhone1,  RmsPrimaryFax,  INET1,  RmsShippingAddress1,  RmsShippingAddress2,  RmsShippingCity,  RmsShippingState,  RmsShippingZip,  RmsShippingCountry,  RmsShippingPhone1,  RmsShippingFax,  MARKED,  RmsIntegrationNotes)  SELECT  LEFT(ISNULL(UPPER(Cust.AccountNumber),''''),15),  Cust.[ID],  LEFT(ISNULL(Cust.FirstName + '' '' + Cust.LastName, ''''),64),  LEFT(ISNULL(Cust.Address,''''),30),  LEFT(ISNULL(Cust.Address2,''''),30),  LEFT(ISNULL(Cust.City,''''),30),  ISNULL(Cust.State,''''),  LEFT(ISNULL(Cust.Zip,''''),10),  ISNULL(Cust.Country,''''),  LEFT(ISNULL(replace(replace(replace(replace(Cust.PhoneNumber,''('',''''),'')'',''''),''-'',''''),'' '',''''),''''),14),  LEFT(ISNULL(replace(replace(replace(replace(Cust.FaxNumber,''('',''''),'')'',''''),''-'',''''),'' '',''''),''''),14),  LEFT(ISNULL(Cust.EmailAddress,''''),200),  LEFT(ISNULL(Ship.Address,''''),30),  LEFT(ISNULL(Ship.Address2,''''),30),  LEFT(ISNULL(Ship.City,''''),30),  ISNULL(Ship.State,''''),   LEFT(ISNULL(Ship.Zip,''''),10),  ISNULL(Ship.Country,''''),  LEFT(ISNULL(replace(replace(replace(replace(Ship.PhoneNumber,''('',''''),'')'',''''),''-'',''''),'' '',''''),''''),14),  LEFT(ISNULL(replace(replace(replace(replace(Ship.FaxNumber,''('',''''),'')'',''''),''-'',''''),'' '',''''),''''),14),  1,  ''''  FROM ' + @lCustomerTable + ' LEFT OUTER JOIN ' + @lShipToTable + ' ON ' + 'Cust.PrimaryShipToID = Ship.[ID]')
  
  IF @iReImport = 0
    BEGIN
      EXEC( 'DELETE ' + @iCustomerTempTable + ' FROM   RMS30000 JOIN ' + @iCustomerTempTable + ' ON ' + @iCustomerTempTable + '.RmsCustomerID = RMS30000.RmsCustomerID')
    END

