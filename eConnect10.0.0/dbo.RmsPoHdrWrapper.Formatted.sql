

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsPoHdrWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsPoHdrWrapper]
  
create procedure [dbo].[RmsPoHdrWrapper]
                @iPOType       smallint,
                @iPONumber     char(17),
                @iVendID       char(15),
                @iDate         datetime,
                @iFrieght      numeric(19,5),
                @iTaxAmnt      numeric(19,5),
                @iCurrencyID   char(15),
                @iExchangeRate numeric(19,7),
                @oErrorState   int  output,
                @oErrString    varchar(255)  output   /*with encryption*/
AS
  EXEC RmsPoHdr
    @I_vPOTYPE = @iPOType ,
    @I_vPONUMBER = @iPONumber ,
    @I_vVENDORID = @iVendID ,
    @I_vDOCDATE = @iDate ,
    @I_vFRTAMNT = @iFrieght ,
    @I_vTAXAMNT = @iTaxAmnt ,
    @I_vCURNCYID = @iCurrencyID ,
    @I_vXCHGRATE = @iExchangeRate ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

