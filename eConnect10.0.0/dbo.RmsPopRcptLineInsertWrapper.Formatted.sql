

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsPopRcptLineInsertWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsPopRcptLineInsertWrapper]
  
create procedure [dbo].[RmsPopRcptLineInsertWrapper]
                @iPoType      smallint,
                @iReceiptNum  char(17),
                @iPoNum       char(17),
                @iItemNum     char(30),
                @iQtyShipped  numeric(19,5),
                @iVendID      char(15),
                @iVendItemNum char(30),
                @iUnitCost    numeric(19,5),
                @iExtCost     numeric(19,5),
                @iCurrencyID  char(15),
                @oErrorState  int  output,
                @oErrString   varchar(255)  output   /*with encryption*/
AS
  EXEC RmsPopRcptLineInsert
    @I_vPOPTYPE = @iPoType ,
    @I_vPOPRCTNM = @iReceiptNum ,
    @I_vPONUMBER = @iPoNum ,
    @I_vITEMNMBR = @iItemNum ,
    @I_vQTYSHPPD = @iQtyShipped ,
    @I_vVENDORID = @iVendID ,
    @I_vVNDITNUM = @iVendItemNum ,
    @I_vUNITCOST = @iUnitCost ,
    @I_vEXTDCOST = @iExtCost ,
    @I_vCURNCYID = @iCurrencyID ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

