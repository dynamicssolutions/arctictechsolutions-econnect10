

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsCreateSalesperson]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsCreateSalesperson]
  
create procedure [dbo].[RmsCreateSalesperson]
                @I_vSLPRSNID       char(15),
                @I_vSALSTERR       char(15),
                @I_vEMPLOYID       char(15)  = '',
                @I_vVENDORID       char(15)  = '',
                @I_vSLPRSNFN       char(15)  = '',
                @I_vSPRSNSMN       char(15)  = '',
                @I_vSPRSNSLN       char(20)  = '',
                @I_vADDRESS1       char(60)  = '',
                @I_vADDRESS2       char(60)  = '',
                @I_vADDRESS3       char(60)  = '',
                @I_vCITY           char(35)  = '',
                @I_vSTATE          char(29)  = '',
                @I_vZIP            char(10)  = '',
                @I_vCOUNTRY        char(60)  = '',
                @I_vPHONE1         char(21)  = '',
                @I_vPHONE2         char(21)  = '',
                @I_vPHONE3         char(21)  = '',
                @I_vFAX            char(21)  = '',
                @I_vINACTIVE       tinyint  = 0,
                @I_vCOMMCODE       char(15)  = '',
                @I_vCOMPRCNT       numeric(19,2)  = 0,
                @I_vSTDCPRCT       numeric(19,2)  = 0,
                @I_vCOMAPPTO       smallint  = 0,
                @I_vCOSTTODT       numeric(19,5)  = 0,
                @I_vCSTLSTYR       numeric(19,5)  = 0,
                @I_vTTLCOMTD       numeric(19,5)  = 0,
                @I_vTTLCOMLY       numeric(19,5)  = 0,
                @I_vCOMSLTDT       numeric(19,5)  = 0,
                @I_vCOMSLLYR       numeric(19,5)  = 0,
                @I_vNCOMSLTD       numeric(19,5)  = 0,
                @I_vNCOMSLYR       numeric(19,5)  = 0,
                @I_vKPCALHST       tinyint  = 1,
                @I_vKPERHIST       tinyint  = 1,
                @I_vMODIFDT        datetime  = '',
                @I_vCREATDDT       datetime  = '',
                @I_vCOMMDEST       smallint  = 0,
                @I_vUpdateIfExists tinyint  = 0,
                @I_vRequesterTrx   smallint  = 0,
                @I_vUSRDEFND1      char(50)  = '',
                @I_vUSRDEFND2      char(50)  = '',
                @I_vUSRDEFND3      char(50)  = '',
                @I_vUSRDEFND4      varchar(8000)  = '',
                @I_vUSRDEFND5      varchar(8000)  = '',
                @O_iErrorState     int  output,
                @oErrString        varchar(255)  output   /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iAddCodeErrState        int,
           @iStatus                 int,
           @iCustomState            int,
           @iCustomErrString        varchar(255),
           @iGetNextNoteIdxErrState int,
           @NOTEINDX                decimal(19,5),
           @CMPANYID                smallint,
           @iError                  int,
           @O_oErrorState           int
  
  select @NOTEINDX = 0,
         @CMPANYID = 0,
         @O_iErrorState = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  if (@I_vSLPRSNID is null 
       or @I_vSALSTERR is null
       or @I_vEMPLOYID is null
       or @I_vVENDORID is null
       or @I_vSLPRSNFN is null
       or @I_vSPRSNSMN is null
       or @I_vSPRSNSLN is null
       or @I_vADDRESS1 is null
       or @I_vADDRESS2 is null
       or @I_vADDRESS3 is null
       or @I_vCITY is null
       or @I_vSTATE is null
       or @I_vZIP is null
       or @I_vCOUNTRY is null
       or @I_vPHONE1 is null
       or @I_vPHONE2 is null
       or @I_vPHONE3 is null
       or @I_vFAX is null
       or @I_vINACTIVE is null
       or @I_vCOMMCODE is null
       or @I_vCOMPRCNT is null
       or @I_vSTDCPRCT is null
       or @I_vCOMAPPTO is null
       or @I_vCOSTTODT is null
       or @I_vCSTLSTYR is null
       or @I_vTTLCOMTD is null
       or @I_vTTLCOMLY is null
       or @I_vCOMSLTDT is null
       or @I_vCOMSLLYR is null
       or @I_vNCOMSLTD is null
       or @I_vNCOMSLYR is null
       or @I_vKPCALHST is null
       or @I_vKPERHIST is null
       or @I_vMODIFDT is null
       or @I_vCREATDDT is null
       or @I_vCOMMDEST is null
       or @I_vRequesterTrx is null)
    begin
      select @O_iErrorState = 4000
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  select @I_vSLPRSNID = UPPER(@I_vSLPRSNID),
         @I_vSALSTERR = UPPER(@I_vSALSTERR),
         @I_vEMPLOYID = UPPER(@I_vEMPLOYID),
         @I_vVENDORID = UPPER(@I_vVENDORID)
  
  select @CMPANYID = CMPANYID
  from   DYNAMICS..SY01500 (nolock)
  where  INTERID = db_name()
  
  exec @iStatus = DYNAMICS..RmssmGetNextNoteIndex
    @I_sCompanyID = @CMPANYID ,
    @I_iSQLSessionID = 0 ,
    @I_noteincrement = 1 ,
    @O_mNoteIndex = @NOTEINDX output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iGetNextNoteIdxErrState <> 0)
    begin
      if (@iGetNextNoteIdxErrState <> 0)
        begin
          exec @iStatus = RmsUpdateString
            @iGetNextNoteIdxErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 467
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vINACTIVE <> 0
      and @I_vINACTIVE <> 1)
    begin
      select @O_iErrorState = 561
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vCOMPRCNT < 0
       or @I_vCOMPRCNT > 100.00)
    begin
      select @O_iErrorState = 681
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  select @I_vCOMPRCNT = @I_vCOMPRCNT * 100
  
  if (@I_vSTDCPRCT < 0
       or @I_vSTDCPRCT > 100.00)
    begin
      select @O_iErrorState = 79
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  select @I_vSTDCPRCT = @I_vSTDCPRCT * 100
  
  if (@I_vCOMAPPTO < 0
       or @I_vCOMAPPTO > 1)
    begin
      select @O_iErrorState = 80
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vKPCALHST < 0
       or @I_vKPCALHST > 1)
    begin
      select @O_iErrorState = 83
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vKPERHIST < 0
       or @I_vKPERHIST > 1)
    begin
      select @O_iErrorState = 84
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vSLPRSNID = '')
    begin
      select @O_iErrorState = 4001
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vSALSTERR = '')
    begin
      select @O_iErrorState = 4003
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (not exists (select SALSTERR
                  from   RM00303 (nolock)
                  where  SALSTERR = @I_vSALSTERR))
    begin
      select @O_iErrorState = 699
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vVENDORID <> ''
      and not exists (select VENDORID
                      from   PM00200 (nolock)
                      where  VENDORID = @I_vVENDORID))
    begin
      select @O_iErrorState = 2314
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vEMPLOYID <> ''
      and not exists (select EMPLOYID
                      from   UPR00100 (nolock)
                      where  EMPLOYID = @I_vEMPLOYID))
    begin
      select @O_iErrorState = 2315
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vUpdateIfExists < 0
       or @I_vUpdateIfExists > 1)
    begin
      select @O_iErrorState = 3721
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@I_vRequesterTrx < 0
       or @I_vRequesterTrx > 1)
    begin
      select @O_iErrorState = 3720
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
    end
  
  if (@O_iErrorState = 0)
    begin
      if (not exists (select 1
                      from   RM00301 (nolock)
                      where  SLPRSNID = @I_vSLPRSNID))
        begin
          insert RM00301
                (SLPRSNID,
                 EMPLOYID,
                 VENDORID,
                 SLPRSNFN,
                 SPRSNSMN,
                 SPRSNSLN,
                 ADDRESS1,
                 ADDRESS2,
                 ADDRESS3,
                 CITY,
                 STATE,
                 ZIP,
                 COUNTRY,
                 PHONE1,
                 PHONE2,
                 PHONE3,
                 FAX,
                 INACTIVE,
                 SALSTERR,
                 COMMCODE,
                 COMPRCNT,
                 STDCPRCT,
                 COMAPPTO,
                 COSTTODT,
                 CSTLSTYR,
                 TTLCOMTD,
                 TTLCOMLY,
                 COMSLTDT,
                 COMSLLYR,
                 NCOMSLTD,
                 NCOMSLYR,
                 KPCALHST,
                 KPERHIST,
                 NOTEINDX,
                 MODIFDT,
                 CREATDDT,
                 COMMDEST)
          select @I_vSLPRSNID,
                 @I_vEMPLOYID,
                 @I_vVENDORID,
                 @I_vSLPRSNFN,
                 @I_vSPRSNSMN,
                 @I_vSPRSNSLN,
                 @I_vADDRESS1,
                 @I_vADDRESS2,
                 @I_vADDRESS3,
                 @I_vCITY,
                 @I_vSTATE,
                 @I_vZIP,
                 @I_vCOUNTRY,
                 @I_vPHONE1,
                 @I_vPHONE2,
                 @I_vPHONE3,
                 @I_vFAX,
                 @I_vINACTIVE,
                 @I_vSALSTERR,
                 @I_vCOMMCODE,
                 @I_vCOMPRCNT,
                 @I_vSTDCPRCT,
                 @I_vCOMAPPTO,
                 @I_vCOSTTODT,
                 @I_vCSTLSTYR,
                 @I_vTTLCOMTD,
                 @I_vTTLCOMLY,
                 @I_vCOMSLTDT,
                 @I_vCOMSLLYR,
                 @I_vNCOMSLTD,
                 @I_vNCOMSLYR,
                 @I_vKPCALHST,
                 @I_vKPERHIST,
                 @NOTEINDX,
                 case 
                   WHEN @I_vMODIFDT = '' THEN convert(varchar(12),getdate())
                   ELSE @I_vMODIFDT
                 end,
                 case 
                   WHEN @I_vCREATDDT = '' THEN convert(varchar(12),getdate())
                   ELSE @I_vCREATDDT
                 end,
                 @I_vCOMMDEST
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 4005
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @iAddCodeErrState output
            end
        end
      else
        if @I_vUpdateIfExists = 1
          begin
            update RM00301
            set    EMPLOYID = @I_vEMPLOYID,
                   VENDORID = @I_vVENDORID,
                   SLPRSNFN = @I_vSLPRSNFN,
                   SPRSNSMN = @I_vSPRSNSMN,
                   SPRSNSLN = @I_vSPRSNSLN,
                   ADDRESS1 = @I_vADDRESS1,
                   ADDRESS2 = @I_vADDRESS2,
                   ADDRESS3 = @I_vADDRESS3,
                   CITY = @I_vCITY,
                   STATE = @I_vSTATE,
                   ZIP = @I_vZIP,
                   COUNTRY = @I_vCOUNTRY,
                   PHONE1 = @I_vPHONE1,
                   PHONE2 = @I_vPHONE2,
                   PHONE3 = @I_vPHONE3,
                   FAX = @I_vFAX,
                   INACTIVE = @I_vINACTIVE,
                   SALSTERR = @I_vSALSTERR,
                   COMMCODE = @I_vCOMMCODE,
                   COMPRCNT = @I_vCOMPRCNT,
                   STDCPRCT = @I_vSTDCPRCT,
                   COMAPPTO = @I_vCOMAPPTO,
                   KPCALHST = @I_vKPCALHST,
                   KPERHIST = @I_vKPERHIST,
                   MODIFDT = case 
                               WHEN @I_vMODIFDT = '' THEN convert(varchar(12),getdate())
                               ELSE @I_vMODIFDT
                             end
            where  SLPRSNID = @I_vSLPRSNID
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 466
                
                exec @iStatus = RmsUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @iAddCodeErrState output
              end
          end
    end
  
  return (@O_iErrorState)

