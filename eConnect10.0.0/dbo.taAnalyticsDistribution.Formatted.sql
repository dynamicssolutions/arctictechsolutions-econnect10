

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taAnalyticsDistribution]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taAnalyticsDistribution]
  
create procedure [dbo].[taAnalyticsDistribution]
                @I_vDOCNMBR             varchar(50),
                @I_vDOCTYPE             smallint  = 0,
                @I_vAMOUNT              numeric(19,5)  = null,
                @I_vaaAssignedPercent   numeric(9,4)  = null,
                @I_vDistSequence        int  = null,
                @I_vACTNUMST            varchar(75)  = null,
                @I_vACTINDX             int  = null,
                @I_vDistRef             char(31)  = null,
                @I_vNOTETEXT            varchar(8000)  = null,
                @I_vaaTrxDim            char(31)  = null,
                @I_vaaTrxDimCode        char(31)  = null,
                @I_vaaTrxDimCodeNum     numeric(19,5)  = null,
                @I_vaaTrxDimCodeBool    smallint  = null,
                @I_vaaTrxDimCodeDate    datetime  = null,
                @I_vaaTrxDimID          int  = null,
                @I_vaaTrxCodeID         int  = null,
                @I_vaaSubLedgerHdrID    int  = null,
                @I_vaaSubLedgerDistID   int  = null,
                @I_vaaSubLedgerAssignID int  = 1,
                @I_vUpdateIfExists      smallint  = 1,
                @I_vRequesterTrx        smallint  = 0,
                @I_vUSRDEFND1           char(50)  = null,
                @I_vUSRDEFND2           char(50)  = null,
                @I_vUSRDEFND3           char(50)  = null,
                @I_vUSRDEFND4           varchar(8000)  = null,
                @I_vUSRDEFND5           varchar(8000)  = null,
                @O_iErrorState          int  output,
                @oErrString             varchar(255)  output   /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iPercentRunningSum      numeric(19,5),
           @DECPLFUN                int,
           @XCHGRATE                numeric(19,7),
           @RTCLCMTD                smallint,
           @iORAMOUNT               numeric(19,5),
           @iSERIES                 int,
           @i                       int,
           @AAG10000exists          int,
           @AAG10002exists          int,
           @AAG20000exists          int,
           @AAG20002exists          int,
           @UPDATE20002             int,
           @SqlSessionID            int,
           @Flag                    smallint,
           @iStatus                 int,
           @iaaAssignedPercent      numeric(9,4),
           @O_oErrorState           smallint,
           @iAMOUNT                 numeric(19,5),
           @noteindex               int,
           @ORDBTAMT                numeric(19,5),
           @ORCRDAMT                numeric(19,5),
           @NOTEINDX                numeric(19,5),
           @iCustomState            smallint,
           @iError                  smallint,
           @iCustomErrString        varchar(255),
           @INTERID                 char(5),
           @sCompanyID              smallint,
           @iGetNextNoteIdxErrState smallint,
           @CURNCYID                char(15),
           @AMOUNT                  numeric(19,5),
           @DEBITAMT                numeric(19,5),
           @CRDTAMNT                numeric(19,5),
           @DECPLUSED               smallint,
           @ClassID                 int,
           @aaBrowseType            int,
           @aaDataType              smallint,
           @DECPLQTY                smallint,
           @RowID                   int,
           @ismctrx                 smallint,
           @FUNLCURR                char(15)
  
  select @iPercentRunningSum = 0,
         @DECPLFUN = 0,
         @iORAMOUNT = @I_vAMOUNT,
         @iSERIES = 0,
         @O_iErrorState = 0,
         @UPDATE20002 = 0,
         @Flag = 0,
         @AAG20000exists = 0,
         @AAG20002exists = 0,
         @AAG10000exists = 0,
         @AAG10002exists = 0,
         @SqlSessionID = 0,
         @Flag = 0,
         @iStatus = 0,
         @iaaAssignedPercent = 0.00,
         @O_oErrorState = 0,
         @iAMOUNT = 0,
         @noteindex = 0,
         @ORDBTAMT = 0,
         @ORCRDAMT = 0,
         @NOTEINDX = 0,
         @iCustomState = 0,
         @iError = 0,
         @iCustomErrString = '',
         @sCompanyID = 0,
         @iGetNextNoteIdxErrState = 0,
         @CURNCYID = '',
         @AMOUNT = 0,
         @DEBITAMT = 0,
         @CRDTAMNT = 0,
         @DECPLUSED = 0,
         @ClassID = 0,
         @aaBrowseType = 0,
         @aaDataType = 0,
         @DECPLQTY = 0,
         @RowID = 0,
         @ismctrx = 0,
         @FUNLCURR = ''
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  if @I_vUpdateIfExists in (0,1)
    begin
      if not exists (select 1
                     from   tempdb..sysobjects
                     where  name = '##taAnalyticsDistribution')
        begin
          create table ##taAnalyticsDistribution (
            DOCNMBR             varchar(50),
            uKey                varchar(50),
            DOCTYPE             smallint,
            AMOUNT              numeric(19,5),
            aaAssignedPercent   numeric(5,2),
            DistSequence        int,
            ACTNUMST            varchar(75),
            ACTINDX             int,
            DistRef             char(31),
            aaTrxDim            char(31),
            aaTrxDimCode        char(31),
            aaTrxDimCodeNum     numeric(19,5),
            aaTrxDimCodeBool    smallint,
            aaTrxDimCodeDate    datetime,
            aaTrxDimID          int,
            aaTrxCodeID         int,
            aaSubLedgerHdrID    int,
            aaSubLedgerDistID   int,
            aaSubLedgerAssignID int,
            UpdateIfExists      smallint,
            RequesterTrx        smallint,
            USRDEFND1           char(50),
            USRDEFND2           char(50),
            USRDEFND3           char(50),
            RowID               int,
            Spid                int)
          
          create table ##taAnalyticsDistributionNOTETEXT (
            RowID    int,
            NOTETEXT varchar(8000))
          
          create table ##taAnalyticsDistributionUSRDEFND4 (
            RowID     int,
            USRDEFND4 varchar(8000))
          
          create table ##taAnalyticsDistributionUSRDEFND5 (
            RowID     int,
            USRDEFND5 varchar(8000))
        end
      else
        begin
          select @RowID = isnull(max(RowID),0) + 1
          from   ##taAnalyticsDistribution
        end
      
      if @I_vaaAssignedPercent > 999.99
        begin
          select @O_iErrorState = 9467
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      insert into ##taAnalyticsDistribution
                 (DOCNMBR,
                  uKey,
                  DOCTYPE,
                  AMOUNT,
                  aaAssignedPercent,
                  DistSequence,
                  ACTNUMST,
                  ACTINDX,
                  DistRef,
                  aaTrxDim,
                  aaTrxDimCode,
                  aaTrxDimCodeNum,
                  aaTrxDimCodeBool,
                  aaTrxDimCodeDate,
                  aaTrxDimID,
                  aaTrxCodeID,
                  aaSubLedgerHdrID,
                  aaSubLedgerDistID,
                  aaSubLedgerAssignID,
                  UpdateIfExists,
                  RequesterTrx,
                  USRDEFND1,
                  USRDEFND2,
                  USRDEFND3,
                  RowID,
                  Spid)
      select @I_vDOCNMBR,
             @I_vDOCNMBR,
             @I_vDOCTYPE,
             @I_vAMOUNT,
             @I_vaaAssignedPercent,
             @I_vDistSequence,
             @I_vACTNUMST,
             @I_vACTINDX,
             @I_vDistRef,
             @I_vaaTrxDim,
             @I_vaaTrxDimCode,
             @I_vaaTrxDimCodeNum,
             @I_vaaTrxDimCodeBool,
             @I_vaaTrxDimCodeDate,
             @I_vaaTrxDimID,
             @I_vaaTrxCodeID,
             @I_vaaSubLedgerHdrID,
             @I_vaaSubLedgerDistID,
             @I_vaaSubLedgerAssignID,
             @I_vUpdateIfExists + 1000,
             @I_vRequesterTrx,
             @I_vUSRDEFND1,
             @I_vUSRDEFND2,
             @I_vUSRDEFND3,
             @RowID,
             @@Spid
      
      if @@error <> 0
        begin
          select @O_iErrorState = 9399
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      insert into ##taAnalyticsDistributionNOTETEXT
                 (RowID,
                  NOTETEXT)
      select @RowID,
             @I_vNOTETEXT
      
      if @@error <> 0
        begin
          select @O_iErrorState = 9400
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      insert into ##taAnalyticsDistributionUSRDEFND4
                 (RowID,
                  USRDEFND4)
      select @RowID,
             @I_vUSRDEFND4
      
      if @@error <> 0
        begin
          select @O_iErrorState = 9401
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      insert into ##taAnalyticsDistributionUSRDEFND5
                 (RowID,
                  USRDEFND5)
      select @RowID,
             @I_vUSRDEFND5
      
      if @@error <> 0
        begin
          select @O_iErrorState = 9402
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      return (@O_iErrorState)
    end
  else
    if @I_vUpdateIfExists in (1000,1001)
      begin
        select @I_vUpdateIfExists = case 
                                      when @I_vUpdateIfExists = 1000 then 0
                                      else 1
                                    end
      end
  
  select @AAG20000exists = 1,
         @I_vaaSubLedgerHdrID = case 
                                  when @I_vaaSubLedgerHdrID is null then aaSubLedgerHdrID
                                  else @I_vaaSubLedgerHdrID
                                end
  from   AAG20000 (nolock)
  where  DOCNUMBR = @I_vDOCNMBR
         and DOCTYPE = @I_vDOCTYPE
         and aaSubLedgerHdrID like case 
                                     when @I_vaaSubLedgerHdrID is not null then @I_vaaSubLedgerHdrID
                                     else aaSubLedgerHdrID
                                   end
  
  if @AAG20000exists <> 1
    begin
      select @i = 1
      
      while @i <= len(@I_vDOCNMBR)
        begin
          if substring(@I_vDOCNMBR,@i,1) not in ('0','1','2','3',
                                                 '4','5','6','7',
                                                 '8','9')
            begin
              select @O_iErrorState = 9468
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          select @i = @i + 1
        end
      
      select @AAG10000exists = 1,
             @I_vaaSubLedgerHdrID = case 
                                      when @I_vaaSubLedgerHdrID is null then aaGLWorkHdrID
                                      else @I_vaaSubLedgerHdrID
                                    end
      from   AAG10000 (nolock)
      where  JRNENTRY = cast(@I_vDOCNMBR as numeric(19,5))
             and RCTRXSEQ = @I_vDOCTYPE
             and aaGLWorkHdrID like case 
                                      when @I_vaaSubLedgerHdrID is not null then @I_vaaSubLedgerHdrID
                                      else aaGLWorkHdrID
                                    end
      
      if @AAG10000exists <> 1
        begin
          select @O_iErrorState = 9403
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if @I_vAMOUNT is not null 
      or @I_vaaAssignedPercent is not null
      or @I_vDistRef is not null
      or ((@I_vaaTrxDim is not null 
            or @I_vaaTrxDimID is not null)
          and (@I_vaaTrxDimCode is not null 
                or @I_vaaTrxDimCodeNum is not null
                or @I_vaaTrxDimCodeBool is not null
                or @I_vaaTrxDimCodeDate is not null))
    begin
      if @I_vaaAssignedPercent > 999.99
        begin
          select @O_iErrorState = 9475
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if @I_vAMOUNT is not null 
          or @I_vaaAssignedPercent is not null
          or @I_vDistRef is not null
        select @UPDATE20002 = 1
      
      if (@I_vACTINDX is not null 
           or @I_vACTNUMST is not null)
         and @I_vaaSubLedgerDistID is null
        begin
          if @I_vACTINDX is null
            select @I_vACTINDX = ACTINDX
            from   GL00105 (nolock)
            where  ACTNUMST = @I_vACTNUMST
          
          if @AAG20000exists = 1
            select @I_vaaSubLedgerDistID = aaSubLedgerDistID,
                   @XCHGRATE = XCHGRATE,
                   @RTCLCMTD = RTCLCMTD
            from   AAG20001 (nolock)
            where  aaSubLedgerHdrID = @I_vaaSubLedgerHdrID
                   and ACTINDX = @I_vACTINDX
          else
            select @I_vaaSubLedgerDistID = aaGLWorkDistID,
                   @XCHGRATE = XCHGRATE,
                   @RTCLCMTD = RTCLCMTD
            from   AAG10001 (nolock)
            where  aaGLWorkHdrID = @I_vaaSubLedgerHdrID
                   and ACTINDX = @I_vACTINDX
        end
      
      if @I_vDistSequence is not null 
         and @I_vaaSubLedgerDistID is null
        begin
          if @AAG20000exists = 1
            select @I_vaaSubLedgerDistID = aaSubLedgerDistID,
                   @XCHGRATE = XCHGRATE,
                   @RTCLCMTD = RTCLCMTD
            from   AAG20001 (nolock)
            where  aaSubLedgerHdrID = @I_vaaSubLedgerHdrID
                   and SEQNUMBR = @I_vDistSequence
          else
            select @I_vaaSubLedgerDistID = aaGLWorkDistID,
                   @XCHGRATE = XCHGRATE,
                   @RTCLCMTD = RTCLCMTD
            from   AAG10001 (nolock)
            where  aaGLWorkHdrID = @I_vaaSubLedgerHdrID
                   and SQNCLINE = @I_vDistSequence
        end
      
      if @AAG20000exists = 1
        select @AAG20002exists = 1,
               @I_vDistRef = case 
                               when @I_vDistRef is null then DistRef
                               else @I_vDistRef
                             end,
               @iaaAssignedPercent = aaAssignedPercent,
               @I_vaaSubLedgerAssignID = case 
                                           when @I_vaaSubLedgerAssignID is null then aaSubLedgerAssignID
                                           else @I_vaaSubLedgerAssignID
                                         end,
               @iAMOUNT = case 
                            when DEBITAMT > 0 then DEBITAMT
                            else CRDTAMNT
                          end,
               @I_vAMOUNT = case 
                              when @I_vAMOUNT is null 
                                   and DEBITAMT > 0 then DEBITAMT
                              when @I_vAMOUNT is null 
                                   and CRDTAMNT > 0 then CRDTAMNT
                              else @I_vAMOUNT
                            end,
               @ORDBTAMT = ORDBTAMT,
               @ORCRDAMT = ORCRDAMT,
               @NOTEINDX = NOTEINDX
        from   AAG20002 (UPDLOCK)
        where  aaSubLedgerHdrID = @I_vaaSubLedgerHdrID
               and aaSubLedgerDistID = @I_vaaSubLedgerDistID
               and aaSubLedgerAssignID = @I_vaaSubLedgerAssignID
      else
        if @AAG10000exists = 1
          select @AAG10002exists = 1,
                 @I_vDistRef = case 
                                 when @I_vDistRef is null then DistRef
                                 else @I_vDistRef
                               end,
                 @iaaAssignedPercent = aaAssignedPercent,
                 @I_vaaSubLedgerAssignID = case 
                                             when @I_vaaSubLedgerAssignID is null then aaGLWorkAssignID
                                             else @I_vaaSubLedgerAssignID
                                           end,
                 @iAMOUNT = case 
                              when DEBITAMT > 0 then ORDBTAMT
                              else ORCRDAMT
                            end,
                 @I_vAMOUNT = case 
                                when @I_vAMOUNT is null 
                                     and DEBITAMT > 0 then ORDBTAMT
                                when @I_vAMOUNT is null 
                                     and CRDTAMNT > 0 then ORCRDAMT
                                else @I_vAMOUNT
                              end,
                 @ORDBTAMT = ORDBTAMT,
                 @ORCRDAMT = ORCRDAMT,
                 @NOTEINDX = NOTEINDX
          from   AAG10002 (UPDLOCK)
          where  aaGLWorkHdrID = @I_vaaSubLedgerHdrID
                 and aaGLWorkDistID = @I_vaaSubLedgerDistID
                 and aaGLWorkAssignID = @I_vaaSubLedgerAssignID
      
      if (@I_vNOTETEXT is null)
        begin
          select @I_vNOTETEXT = TXTFIELD
          from   SY03900 (nolock)
          where  NOTEINDX = @noteindex
        end
      
      select @iaaAssignedPercent = @iaaAssignedPercent / 100
      
      select @I_vaaAssignedPercent = case 
                                       when @I_vaaAssignedPercent is null then @iaaAssignedPercent
                                       else @I_vaaAssignedPercent
                                     end
    end
  
  if @I_vUpdateIfExists = 0
      or (@AAG20002exists = 0
          and @AAG10002exists = 0)
    begin
      if @I_vDistRef is null
        select @I_vDistRef = ''
    end
  
  exec @iStatus = taAnalyticsDistributionPre
    @I_vDOCNMBR output ,
    @I_vDOCTYPE output ,
    @I_vAMOUNT output ,
    @I_vaaAssignedPercent output ,
    @I_vDistSequence output ,
    @I_vACTNUMST output ,
    @I_vACTINDX output ,
    @I_vDistRef output ,
    @I_vNOTETEXT output ,
    @I_vaaTrxDim output ,
    @I_vaaTrxDimCode output ,
    @I_vaaTrxDimCodeNum output ,
    @I_vaaTrxDimCodeBool output ,
    @I_vaaTrxDimCodeDate output ,
    @I_vaaTrxDimID output ,
    @I_vaaTrxCodeID output ,
    @I_vaaSubLedgerHdrID output ,
    @I_vaaSubLedgerDistID output ,
    @I_vaaSubLedgerAssignID output ,
    @I_vUpdateIfExists output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus = 0)
      and (@iError <> 0))
    begin
      select @iStatus = @iError
    end
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 9404
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vDOCNMBR is NULL 
       or @I_vDOCTYPE is NULL
       or @I_vaaSubLedgerAssignID is NULL)
    begin
      select @O_iErrorState = 9405
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vaaTrxDim = UPPER(@I_vaaTrxDim),
         @I_vaaTrxDimCode = UPPER(@I_vaaTrxDimCode)
  
  select @INTERID = INTERID,
         @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 c (nolock)
  where  c.INTERID = db_name()
  
  if @NOTEINDX = 0
    begin
      exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
        @I_sCompanyID = @sCompanyID ,
        @I_iSQLSessionID = 0 ,
        @I_noteincrement = 1 ,
        @O_mNoteIndex = @NOTEINDX output ,
        @O_iErrorState = @iGetNextNoteIdxErrState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iGetNextNoteIdxErrState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iGetNextNoteIdxErrState))
          
          select @O_iErrorState = 9406
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @FUNLCURR = isnull(FUNLCURR,'')
  from   MC40000 (nolock)
  
  if @AAG20000exists = 1
    begin
      select @I_vACTINDX = case 
                             when @I_vACTINDX is null then ACTINDX
                             else @I_vACTINDX
                           end,
             @CURNCYID = CURNCYID,
             @AMOUNT = case 
                         when @FUNLCURR = CURNCYID then DEBITAMT + CRDTAMNT
                         else ORDBTAMT + ORCRDAMT
                       end,
             @DEBITAMT = case 
                           when @FUNLCURR = CURNCYID then DEBITAMT
                           else ORDBTAMT
                         end,
             @CRDTAMNT = case 
                           when @FUNLCURR = CURNCYID then CRDTAMNT
                           else ORCRDAMT
                         end,
             @iSERIES = 5,
             @XCHGRATE = XCHGRATE,
             @RTCLCMTD = RTCLCMTD
      from   AAG20001 (nolock)
      where  aaSubLedgerHdrID = @I_vaaSubLedgerHdrID
             and aaSubLedgerDistID = @I_vaaSubLedgerDistID
    end
  else
    begin
      select @I_vACTINDX = case 
                             when @I_vACTINDX is null then ACTINDX
                             else @I_vACTINDX
                           end,
             @CURNCYID = CURNCYID,
             @AMOUNT = ORDBTAMT + ORCRDAMT,
             @DEBITAMT = ORDBTAMT,
             @CRDTAMNT = ORCRDAMT,
             @XCHGRATE = XCHGRATE,
             @RTCLCMTD = RTCLCMTD
      from   AAG10001 (nolock)
      where  aaGLWorkHdrID = @I_vaaSubLedgerHdrID
             and aaGLWorkDistID = @I_vaaSubLedgerDistID
    end
  
  if @CURNCYID = ''
    select @CURNCYID = @FUNLCURR
  
  if (@FUNLCURR <> @CURNCYID)
     and len(rtrim(ltrim(@CURNCYID))) <> 0
    select @ismctrx = 1
  
  select @DECPLUSED = DECPLCUR - 1
  from   DYNAMICS..MC40200 (nolock)
  where  CURNCYID = @CURNCYID
  
  select @DECPLFUN = DECPLCUR - 1
  from   DYNAMICS..MC40200 (nolock)
  where  CURNCYID = @FUNLCURR
  
  if (@I_vAMOUNT <> round(@I_vAMOUNT,@DECPLFUN))
    begin
      select @O_iErrorState = 9407
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vAMOUNT > 0
     and @iAMOUNT <> @I_vAMOUNT
     and @AMOUNT > 0
    begin
      select @I_vaaAssignedPercent = round(@I_vAMOUNT / @AMOUNT * 100,5)
      
      if (@AAG20000exists = 1)
        begin
          select @iPercentRunningSum = sum(aaAssignedPercent)
          from   AAG20002 (nolock)
          where  aaSubLedgerHdrID = @I_vaaSubLedgerHdrID
                 and aaSubLedgerDistID = @I_vaaSubLedgerDistID
        end
      else
        begin
          select @iPercentRunningSum = sum(aaAssignedPercent)
          from   AAG10002 (nolock)
          where  aaGLWorkHdrID = @I_vaaSubLedgerHdrID
                 and aaGLWorkDistID = @I_vaaSubLedgerDistID
        end
      
      if @iPercentRunningSum <> 10000
         and @I_vaaAssignedPercent <> 100
         and (@iPercentRunningSum + (@I_vaaAssignedPercent * 100)) > 10000
        begin
          select @I_vaaAssignedPercent = 100 - (@iPercentRunningSum / 100)
        end
    end
  else
    begin
      if @I_vaaAssignedPercent > 0
         and (@iaaAssignedPercent <> @I_vaaAssignedPercent
               or @I_vaaAssignedPercent = 100)
         and @AMOUNT > 0
        begin
          select @I_vAMOUNT = round(@I_vaaAssignedPercent / 100 * @AMOUNT,@DECPLFUN)
        end
      else
        begin
          select @I_vAMOUNT = @ORDBTAMT + @ORCRDAMT
        end
    end
  
  if @I_vAMOUNT <> 0
     and @ismctrx = 1
    begin
      if @XCHGRATE <> 0
        begin
          if @RTCLCMTD = 0
            select @iORAMOUNT = round(@I_vAMOUNT * @XCHGRATE,@DECPLFUN)
          else
            select @iORAMOUNT = round(@I_vAMOUNT / @XCHGRATE,@DECPLFUN)
        end
    end
  else
    select @iORAMOUNT = @I_vAMOUNT
  
  if (@I_vAMOUNT is null 
      and @iSERIES <> 5)
      or (@I_vaaSubLedgerDistID is null)
    begin
      select @O_iErrorState = 9469
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vaaAssignedPercent > 999.99
    begin
      select @O_iErrorState = 9408
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vRequesterTrx < 0)
       or (@I_vRequesterTrx > 1))
    begin
      select @O_iErrorState = 9409
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if @AAG20002exists <> 1
     and @AAG10002exists <> 1
    begin
      if (@I_vAMOUNT is null 
           or @I_vAMOUNT <= 0)
         and (@I_vaaAssignedPercent <= 0
               or @I_vaaAssignedPercent is null)
        begin
          select @O_iErrorState = 9410
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if @AAG20000exists = 1
        begin
          exec aagGetClassIDBrowseType
            @I_vACTINDX ,
            @ClassID output ,
            @aaBrowseType output
          
          insert AAG20002
                (DistRef,
                 aaAssignedPercent,
                 aaSubLedgerHdrID,
                 aaSubLedgerDistID,
                 aaSubLedgerAssignID,
                 ORCRDAMT,
                 NOTEINDX,
                 DEBITAMT,
                 CRDTAMNT,
                 ORDBTAMT)
          select @I_vDistRef,
                 round(round(@I_vaaAssignedPercent * 100,1),0),
                 @I_vaaSubLedgerHdrID,
                 @I_vaaSubLedgerDistID,
                 @I_vaaSubLedgerAssignID,
                 case 
                   when @CRDTAMNT > 0 then @I_vAMOUNT
                   else 0
                 end,
                 @NOTEINDX,
                 case 
                   when @DEBITAMT > 0 then @iORAMOUNT
                   else 0
                 end,
                 case 
                   when @CRDTAMNT > 0 then @iORAMOUNT
                   else 0
                 end,
                 case 
                   when @DEBITAMT > 0 then @I_vAMOUNT
                   else 0
                 end
          
          if @@error <> 0
            begin
              select @O_iErrorState = 9411
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          exec aagSubWorkCodeUpdate
            @I_vaaSubLedgerHdrID ,
            @I_vaaSubLedgerDistID ,
            @I_vaaSubLedgerAssignID ,
            @ClassID
          
          if @@error <> 0
            begin
              select @O_iErrorState = 9412
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          exec aagGetClassIDBrowseType
            @I_vACTINDX ,
            @ClassID output ,
            @aaBrowseType output
          
          insert AAG10002
                (DistRef,
                 aaAssignedPercent,
                 aaGLWorkHdrID,
                 aaGLWorkDistID,
                 aaGLWorkAssignID,
                 ORCRDAMT,
                 NOTEINDX,
                 DEBITAMT,
                 CRDTAMNT,
                 ORDBTAMT)
          select @I_vDistRef,
                 round(round(@I_vaaAssignedPercent * 100,1),0),
                 @I_vaaSubLedgerHdrID,
                 @I_vaaSubLedgerDistID,
                 @I_vaaSubLedgerAssignID,
                 case 
                   when @CRDTAMNT > 0 then @I_vAMOUNT
                   else 0
                 end,
                 @NOTEINDX,
                 case 
                   when @DEBITAMT > 0 then @iORAMOUNT
                   else 0
                 end,
                 case 
                   when @CRDTAMNT > 0 then @iORAMOUNT
                   else 0
                 end,
                 case 
                   when @DEBITAMT > 0 then @I_vAMOUNT
                   else 0
                 end
          
          if @@error <> 0
            begin
              select @O_iErrorState = 9429
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          exec aagGLWorkCodeUpdate
            @I_vaaSubLedgerHdrID ,
            @I_vaaSubLedgerDistID ,
            @I_vaaSubLedgerAssignID ,
            @ClassID
          
          if @@error <> 0
            begin
              select @O_iErrorState = 9430
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if @UPDATE20002 = 1
    begin
      if @AAG20000exists = 1
        begin
          update AAG20002
          set    DistRef = @I_vDistRef,
                 aaAssignedPercent = round(round(@I_vaaAssignedPercent * 100,1),0),
                 DEBITAMT = case 
                              when @DEBITAMT > 0 then @iORAMOUNT
                              else DEBITAMT
                            end,
                 CRDTAMNT = case 
                              when @CRDTAMNT > 0 then @iORAMOUNT
                              else CRDTAMNT
                            end,
                 ORDBTAMT = case 
                              when @DEBITAMT > 0 then @I_vAMOUNT
                              else DEBITAMT
                            end,
                 ORCRDAMT = case 
                              when @CRDTAMNT > 0 then @I_vAMOUNT
                              else CRDTAMNT
                            end
          where  aaSubLedgerHdrID = @I_vaaSubLedgerHdrID
                 and aaSubLedgerDistID = @I_vaaSubLedgerDistID
                 and aaSubLedgerAssignID = @I_vaaSubLedgerAssignID
          
          if @@error <> 0
            begin
              select @O_iErrorState = 9413
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          update AAG20001
          set    aaWinWasOpen = 1
          where  aaSubLedgerHdrID = @I_vaaSubLedgerHdrID
          
          if @@error <> 0
            begin
              select @O_iErrorState = 9426
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          update AAG10002
          set    DistRef = @I_vDistRef,
                 aaAssignedPercent = round(round(@I_vaaAssignedPercent * 100,1),0),
                 DEBITAMT = case 
                              when @DEBITAMT > 0 then @iORAMOUNT
                              else DEBITAMT
                            end,
                 CRDTAMNT = case 
                              when @CRDTAMNT > 0 then @iORAMOUNT
                              else CRDTAMNT
                            end,
                 ORDBTAMT = case 
                              when @DEBITAMT > 0 then @I_vAMOUNT
                              else DEBITAMT
                            end,
                 ORCRDAMT = case 
                              when @CRDTAMNT > 0 then @I_vAMOUNT
                              else CRDTAMNT
                            end
          where  aaGLWorkHdrID = @I_vaaSubLedgerHdrID
                 and aaGLWorkDistID = @I_vaaSubLedgerDistID
                 and aaGLWorkAssignID = @I_vaaSubLedgerAssignID
          
          if @@error <> 0
            begin
              select @O_iErrorState = 9431
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          update AAG10001
          set    aaWinWasOpen = 1
          where  aaGLWorkHdrID = @I_vaaSubLedgerHdrID
          
          if @@error <> 0
            begin
              select @O_iErrorState = 9432
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if (@I_vaaTrxCodeID is not null 
      and @I_vaaTrxCodeID <> '')
     and (@I_vaaTrxDim is null 
          and @I_vaaTrxDimID is null
          and @I_vaaTrxDimCode is null
          and @I_vaaTrxDimCodeNum is null
          and @I_vaaTrxDimCodeBool is null
          and @I_vaaTrxDimCodeDate is null)
    begin
      select @O_iErrorState = 9480
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vaaTrxDim is not null 
       or @I_vaaTrxDimID is not null)
     and (@I_vaaTrxDimCode is not null 
           or @I_vaaTrxDimCodeNum is not null
           or @I_vaaTrxDimCodeBool is not null
           or @I_vaaTrxDimCodeDate is not null
           or @I_vaaTrxCodeID is not null)
    begin
      if @I_vaaTrxDimID is null
        select @I_vaaTrxDimID = aaTrxDimID,
               @aaDataType = aaDataType,
               @DECPLQTY = DECPLQTY
        from   AAG00400 (nolock)
        where  aaTrxDim = @I_vaaTrxDim
      else
        select @aaDataType = aaDataType,
               @I_vaaTrxDim = aaTrxDim,
               @DECPLQTY = DECPLQTY
        from   AAG00400 (nolock)
        where  aaTrxDimID = @I_vaaTrxDimID
      
      if @AAG20000exists = 1
        begin
          if not exists (select aaTrxDimID
                         from   AAG20003 (nolock)
                         where  aaSubLedgerHdrID = @I_vaaSubLedgerHdrID
                                and aaSubLedgerDistID = @I_vaaSubLedgerDistID
                                and aaSubLedgerAssignID = @I_vaaSubLedgerAssignID
                                and aaTrxDimID = @I_vaaTrxDimID)
            begin
              select @O_iErrorState = 9414
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          if not exists (select aaTrxDimID
                         from   AAG10003 (nolock)
                         where  aaGLWorkHdrID = @I_vaaSubLedgerHdrID
                                and aaGLWorkDistID = @I_vaaSubLedgerDistID
                                and aaGLWorkAssignID = @I_vaaSubLedgerAssignID
                                and aaTrxDimID = @I_vaaTrxDimID)
            begin
              select @O_iErrorState = 9433
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      if @aaDataType = 1
         and (@I_vaaTrxDimCode is not null 
               or @I_vaaTrxCodeID is not null)
        begin
          if @I_vaaTrxCodeID is null
            select @I_vaaTrxCodeID = aaTrxDimCodeID
            from   AAG00401 (nolock)
            where  aaTrxDimID = @I_vaaTrxDimID
                   and aaTrxDimCode = @I_vaaTrxDimCode
          
          if not exists (select 1
                         from   AAG00401 (nolock)
                         where  aaTrxDimID = @I_vaaTrxDimID
                                and aaTrxDimCodeID = @I_vaaTrxCodeID)
            begin
              select @O_iErrorState = 9415
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      if @aaDataType = 2
         and (@I_vaaTrxDimCodeNum is not null 
               or @I_vaaTrxCodeID is not null)
        begin
          if @I_vaaTrxCodeID is null
            select @I_vaaTrxCodeID = aaTrxDimCodeNumID
            from   AAG00402 (nolock)
            where  aaTrxDimID = aaTrxDimID
                   and aaTrxDimCodeNum = @I_vaaTrxDimCodeNum
          
          if @I_vaaTrxDimCodeNum <> round(@I_vaaTrxDimCodeNum,@DECPLQTY)
            begin
              select @O_iErrorState = 9416
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          if @I_vaaTrxCodeID is null
            begin
              select @I_vaaTrxCodeID = isnull(max(aaTrxDimCodeNumID),0) + 1
              from   AAG00402 (nolock)
              where  aaTrxDimID = aaTrxDimID
              
              insert AAG00402
                    (aaTrxDimID,
                     aaTrxDimCodeNumID,
                     aaTrxDimCodeNum)
              select @I_vaaTrxDimID,
                     @I_vaaTrxCodeID,
                     @I_vaaTrxDimCodeNum
              
              if @@error <> 0
                begin
                  select @O_iErrorState = 9417
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
        end
      
      if @aaDataType = 3
         and (@I_vaaTrxDimCodeBool is not null 
               or @I_vaaTrxCodeID is not null)
        begin
          if @I_vaaTrxCodeID is null
            select @I_vaaTrxCodeID = aaTrxDimCodeBoolID
            from   AAG00403
            where  aaTrxDimID = aaTrxDimID
                   and aaTrxDimCodeBool = @I_vaaTrxDimCodeBool
          
          if @I_vaaTrxCodeID is null
            begin
              select @I_vaaTrxCodeID = isnull(max(aaTrxDimCodeBoolID),0) + 1
              from   AAG00403 (nolock)
              where  aaTrxDimID = aaTrxDimID
              
              insert AAG00403
                    (aaTrxDimID,
                     aaTrxDimCodeBoolID,
                     aaTrxDimCodeBool)
              select @I_vaaTrxDimID,
                     @I_vaaTrxCodeID,
                     @I_vaaTrxDimCodeBool
              
              if @@error <> 0
                begin
                  select @O_iErrorState = 9418
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
        end
      
      if @aaDataType = 4
         and (@I_vaaTrxDimCodeDate is not null 
               or @I_vaaTrxCodeID is not null)
        begin
          if @I_vaaTrxCodeID is null
            select @I_vaaTrxCodeID = aaTrxDimCodeDateID
            from   AAG00404
            where  aaTrxDimID = aaTrxDimID
                   and aaTrxDimCodeDate = @I_vaaTrxDimCodeDate
          
          if @I_vaaTrxCodeID is null
            begin
              select @I_vaaTrxCodeID = isnull(max(aaTrxDimCodeDateID),0) + 1
              from   AAG00404 (nolock)
              where  aaTrxDimID = aaTrxDimID
              
              insert AAG00404
                    (aaTrxDimID,
                     aaTrxDimCodeDateID,
                     aaTrxDimCodeDate)
              select @I_vaaTrxDimID,
                     @I_vaaTrxCodeID,
                     @I_vaaTrxDimCodeDate
              
              if @@error <> 0
                begin
                  select @O_iErrorState = 9419
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
        end
      
      exec aagGetClassIDBrowseType
        @I_vACTINDX ,
        @ClassID output ,
        @aaBrowseType output
      
      if exists (select 1
                 from   AAG00202 (nolock)
                 where  aaAcctClassID = @ClassID
                        and aaTrxDimID = @I_vaaTrxDimID
                        and aaTrxDimCodeIDDflt <> @I_vaaTrxCodeID
                        and aaDataEntry = 4)
        begin
          select @O_iErrorState = 9466
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if @I_vaaTrxCodeID is null
        begin
          select @O_iErrorState = 9477
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if @AAG20000exists = 1
        begin
          update AAG20003
          set    aaTrxCodeID = @I_vaaTrxCodeID
          where  aaSubLedgerHdrID = @I_vaaSubLedgerHdrID
                 and aaSubLedgerDistID = @I_vaaSubLedgerDistID
                 and aaSubLedgerAssignID = @I_vaaSubLedgerAssignID
                 and aaTrxDimID = @I_vaaTrxDimID
          
          if @@error <> 0
            begin
              select @O_iErrorState = 9420
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          update AAG10003
          set    aaTrxCodeID = @I_vaaTrxCodeID
          where  aaGLWorkHdrID = @I_vaaSubLedgerHdrID
                 and aaGLWorkDistID = @I_vaaSubLedgerDistID
                 and aaGLWorkAssignID = @I_vaaSubLedgerAssignID
                 and aaTrxDimID = @I_vaaTrxDimID
          
          if @@error <> 0
            begin
              select @O_iErrorState = 9434
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      if @UPDATE20002 <> 1
        begin
          if @AAG20000exists = 1
            begin
              update AAG20001
              set    aaWinWasOpen = 1
              where  aaSubLedgerHdrID = @I_vaaSubLedgerHdrID
              
              if @@error <> 0
                begin
                  select @O_iErrorState = 9425
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
          else
            begin
              update AAG10001
              set    aaWinWasOpen = 1
              where  aaGLWorkHdrID = @I_vaaSubLedgerHdrID
              
              if @@error <> 0
                begin
                  select @O_iErrorState = 9435
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
        end
    end
  
  if (@I_vNOTETEXT <> '')
    begin
      if not exists (select 1
                     from   SY03900 (nolock)
                     where  NOTEINDX = @NOTEINDX)
        begin
          insert SY03900
                (NOTEINDX,
                 DATE1,
                 TIME1,
                 TXTFIELD)
          select @NOTEINDX,
                 convert(varchar(12),getdate()),
                 substring(convert(varchar(25),getdate()),12,12),
                 @I_vNOTETEXT
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 9421
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          update SY03900
          set    DATE1 = convert(varchar(12),getdate()),
                 TIME1 = substring(convert(varchar(25),getdate()),12,12),
                 TXTFIELD = @I_vNOTETEXT
          where  NOTEINDX = @NOTEINDX
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 9422
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  else
    if (@I_vNOTETEXT = '')
      begin
        delete SY03900
        where  NOTEINDX = @NOTEINDX
        
        if (@@error <> 0)
          begin
            select @O_iErrorState = 9423
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @O_oErrorState output
            
            return (@O_iErrorState)
          end
      end
  
  exec @iStatus = taAnalyticsDistributionPost
    @I_vDOCNMBR ,
    @I_vDOCTYPE ,
    @I_vAMOUNT ,
    @I_vaaAssignedPercent ,
    @I_vDistSequence ,
    @I_vACTNUMST ,
    @I_vACTINDX ,
    @I_vDistRef ,
    @I_vNOTETEXT ,
    @I_vaaTrxDim ,
    @I_vaaTrxDimCode ,
    @I_vaaTrxDimCodeNum ,
    @I_vaaTrxDimCodeBool ,
    @I_vaaTrxDimCodeDate ,
    @I_vaaTrxDimID ,
    @I_vaaTrxCodeID ,
    @I_vaaSubLedgerHdrID ,
    @I_vaaSubLedgerDistID ,
    @I_vaaSubLedgerAssignID ,
    @I_vUpdateIfExists ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus = 0)
      and (@iError <> 0))
    begin
      select @iStatus = @iError
    end
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 9424
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

