

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetTenders]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetTenders]
  
create procedure [dbo].[RmsGetTenders]
                @iTenderTempTable VARCHAR(255),
                @iRmsServerName   VARCHAR(255),
                @iHqDbName        VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lTenderTable VARCHAR(255)
  
  SELECT @lTenderTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Tender'
  
  EXEC( 'INSERT INTO ' + @iTenderTempTable + ' SELECT  [ID],  [Description],  AdditionalDetailType  FROM ' + @lTenderTable)

