

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetDepartments]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetDepartments]
  
create procedure [dbo].[RmsGetDepartments]
                @iDeptTempTable VARCHAR(255),
                @iRmsServerName VARCHAR(255),
                @iHqDbName      VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lDeptTable VARCHAR(255)
  
  SELECT @lDeptTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Department'
  
  EXEC( 'INSERT INTO ' + @iDeptTempTable + ' SELECT  [ID],  code,  [Name]  FROM ' + @lDeptTable)

