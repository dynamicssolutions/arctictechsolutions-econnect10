

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPATSDistributionInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPATSDistributionInsert]
  
create procedure [dbo].[taPATSDistributionInsert]
                @I_vCURNCYID   char(15),
                @I_vCURRNIDX   int,
                @I_vORCRDAMT   numeric(19,5),
                @I_vORDBTAMT   numeric(19,5),
                @I_vPATSNO     char(17),
                @I_vTRXSORCE   char(13),
                @I_vCNTRLTYP   smallint,
                @I_vCRDTAMNT   numeric(19,5),
                @I_vDEBITAMT   numeric(19,5),
                @I_vDSTINDX    int,
                @I_vDISTTYPE   smallint,
                @I_vDistRef    char(31),
                @I_vUSERID     char(15),
                @I_vEMPLOYID   char(15),
                @O_iErrorState int  = NULL output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @DSTSQNUM int,
           @iStatus  int,
           @iError   int,
           @CURNCYID char(15)
  
  select @O_iErrorState = 0,
         @iStatus = 0,
         @iError = 0,
         @DSTSQNUM = 0
  
  if (@I_vCURNCYID is NULL 
       or @I_vCURRNIDX is NULL
       or @I_vORCRDAMT is NULL
       or @I_vORDBTAMT is NULL
       or @I_vPATSNO is NULL
       or @I_vTRXSORCE is NULL
       or @I_vCNTRLTYP is NULL
       or @I_vCRDTAMNT is NULL
       or @I_vDEBITAMT is NULL
       or @I_vDSTINDX is NULL
       or @I_vDISTTYPE is NULL
       or @I_vDistRef is NULL
       or @I_vUSERID is NULL
       or @I_vEMPLOYID is NULL)
    begin
      select @O_iErrorState = 1814
      
      return (@O_iErrorState)
    end
  
  if (@I_vPATSNO = ''
       or @I_vDISTTYPE = 0)
    begin
      select @O_iErrorState = 742
      
      return (@O_iErrorState)
    end
  
  select @I_vPATSNO = UPPER(@I_vPATSNO),
         @I_vEMPLOYID = UPPER(@I_vEMPLOYID),
         @I_vCURNCYID = UPPER(@I_vCURNCYID)
  
  select @DSTSQNUM = isnull(max(DSTSQNUM),0) + 10
  from   PA10003 nolock
  where  PATSNO = @I_vPATSNO
  
  if (@DSTSQNUM = 10)
    select @DSTSQNUM = 1
  
  insert into PA10003
             (CURNCYID,
              CURRNIDX,
              ORCRDAMT,
              ORDBTAMT,
              PATSNO,
              TRXSORCE,
              DSTSQNUM,
              CNTRLTYP,
              CRDTAMNT,
              DEBITAMT,
              DSTINDX,
              DISTTYPE,
              DistRef,
              USERID,
              EMPLOYID)
  select @I_vCURNCYID,
         @I_vCURRNIDX,
         @I_vORCRDAMT,
         @I_vORDBTAMT,
         @I_vPATSNO,
         @I_vTRXSORCE,
         @DSTSQNUM,
         @I_vCNTRLTYP,
         @I_vCRDTAMNT,
         @I_vDEBITAMT,
         @I_vDSTINDX,
         @I_vDISTTYPE,
         @I_vDistRef,
         @I_vUSERID,
         @I_vEMPLOYID
  
  if @@error <> 0
    begin
      select @O_iErrorState = 1815
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

