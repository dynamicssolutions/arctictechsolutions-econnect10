

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsRMCashReceiptInsertWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsRMCashReceiptInsertWrapper]
  
create procedure [dbo].[RmsRMCashReceiptInsertWrapper]
                @iCustNum       char(15),
                @iDocNum        char(20),
                @iDocDate       datetime,
                @iOrigTrxAmnt   numeric(19,5),
                @iGLPostDate    datetime,
                @iBatchNum      char(15),
                @iCashRecptType smallint,
                @iCkbkID        char(15)  = '',
                @iTrxDescrip    char(30)  = '',
                @iCrCardID      char(15),
                @iBatchCkbkID   char(15)  = '',
                @oErrorState    int  output,
                @oErrString     varchar(255)  output   /*with encryption*/
AS
  EXEC RmsRMCashReceiptInsert
    @I_vCUSTNMBR = @iCustNum ,
    @I_vDOCNUMBR = @iDocNum ,
    @I_vDOCDATE = @iDocDate ,
    @I_vORTRXAMT = @iOrigTrxAmnt ,
    @I_vGLPOSTDT = @iGLPostDate ,
    @I_vBACHNUMB = @iBatchNum ,
    @I_vCSHRCTYP = @iCashRecptType ,
    @I_vCHEKBKID = @iCkbkID ,
    @I_vTRXDSCRN = @iTrxDescrip ,
    @I_vCRCARDID = @iCrCardID ,
    @I_vBatchCHEKBKID = @iBatchCkbkID ,
    @I_vCREATEDIST = 0 ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

