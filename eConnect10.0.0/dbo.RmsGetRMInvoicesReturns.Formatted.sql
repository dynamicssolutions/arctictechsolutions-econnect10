

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetRMInvoicesReturns]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetRMInvoicesReturns]
  
create procedure [dbo].[RmsGetRMInvoicesReturns]
                @iRMInvoiceTempTable VARCHAR(255),
                @iRMTaxTotalTemp     VARCHAR(255),
                @iRMDeptTotals       VARCHAR(255),
                @iRMTenderTotals     VARCHAR(255),
                @iStartDate          DATETIME,
                @iEndDate            DATETIME,
                @iSelectType         int,
                @iRmsServerName      VARCHAR(255),
                @iHqDbName           VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lTransactionTable VARCHAR(255)
  
  DECLARE  @lTransactionEntryTable VARCHAR(255)
  
  DECLARE  @lShippingTable VARCHAR(255)
  
  DECLARE  @lTenderEntryTable VARCHAR(255)
  
  DECLARE  @lTenderTable VARCHAR(255)
  
  DECLARE  @lTenderTotalsTable VARCHAR(255)
  
  DECLARE  @lTaxEntryTable VARCHAR(255)
  
  DECLARE  @lItemTable VARCHAR(255)
  
  DECLARE  @lCustomerTable VARCHAR(255)
  
  DECLARE  @lWhereClause VARCHAR(1000)
  
  DECLARE  @lOrderHistoryTable VARCHAR(255)
  
  DECLARE  @lOrderTable VARCHAR(255)
  
  DECLARE  @lDocType INT
  
  SELECT @lTransactionTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.[Transaction] as Transactions'
  
  SELECT @lTransactionEntryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.TransactionEntry as TransactionEntry'
  
  SELECT @lShippingTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Shipping as Shipping'
  
  SELECT @lTenderEntryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.TenderEntry as TenderEntry'
  
  SELECT @lTenderTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Tender as Tender'
  
  SELECT @lTenderTotalsTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.TenderTotals as TendorTotals'
  
  SELECT @lTaxEntryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.TaxEntry as TaxEntry'
  
  SELECT @lItemTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Item as Item'
  
  SELECT @lCustomerTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Customer as Customer'
  
  SELECT @lOrderHistoryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.OrderHistory as OrderHistory'
  
  SELECT @lOrderTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.[Order] as Orders'
  
  IF @iSelectType = 1
    BEGIN
      SELECT @lDocType = 1
      
      SELECT @lWhereClause = ' WHERE  RecallType IN (0,2,4,5)  AND  Transactions.Total >= 0  AND  NOT EXISTS(SELECT  1  FROM  RMS30700  WHERE  Transactions.StoreID = RMS30700.RmsStoreID  AND  Transactions.TransactionNumber = RMS30700.RmsTransactionNumber)'
    END
  
  IF @iSelectType = 2
    BEGIN
      SELECT @lDocType = 8
      
      SELECT @lWhereClause = ' WHERE  ((RecallType = 1 OR RecallType = 3)  OR  Transactions.Total < 0)  AND  NOT EXISTS(SELECT  1  FROM  RMS30900  WHERE  Transactions.StoreID = RMS30900.RmsStoreID  AND  Transactions.TransactionNumber = RMS30900.RmsTransactionNumber)'
    END
  
  EXEC( 'INSERT INTO ' + @iRMInvoiceTempTable + ' (RmsTransactionNumber,  RmsStoreID,  IVCLINNO,  CUSTNMBR,  Total_Sales,  DATE1,  COMMAMNT,  FRTAMNT,  TAXAMNT,  CASHAMNT,  CRCRDAMT,  CHEKAMNT,  ACCTAMNT,  RmsRecallType,  DOCTYPE,  RmsMixedDocumentError)  SELECT  Transactions.TransactionNumber,  Transactions.StoreID,  0,  LEFT(ISNULL(UPPER(Customer.AccountNumber),''''),15),  Transactions.Total,  CONVERT(DATETIME,CONVERT(VARCHAR,Transactions.Time,111)),  0,  0,  0,  0,  0,  0,  0,  Transactions.RecallType, ' + @lDocType + ',  0  FROM ' + @lTransactionTable + ' LEFT OUTER JOIN ' + @lCustomerTable + ' ON ' + 'Transactions.CustomerID = Customer.[ID] ' + @lWhereClause + ' AND  CONVERT(DATETIME,CONVERT(VARCHAR,Transactions.Time,111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''')
  
  EXEC( 'UPDATE ' + @iRMInvoiceTempTable + ' SET  COMMAMNT =  isnull((SELECT  SUM(TransactionEntry.Commission)  FROM ' + @lTransactionEntryTable + ' WHERE  TransactionEntry.TransactionNumber = ' + @iRMInvoiceTempTable + '.RmsTransactionNumber  AND  TransactionEntry.StoreID = ' + @iRMInvoiceTempTable + '.RmsStoreID   GROUP BY   TransactionEntry.StoreID,  TransactionEntry.TransactionNumber),0) ,   TAXAMNT =  isnull((SELECT  SUM(TransactionEntry.SalesTax)  FROM ' + @lTransactionEntryTable + ' WHERE  TransactionEntry.TransactionNumber = ' + @iRMInvoiceTempTable + '.RmsTransactionNumber  AND  TransactionEntry.StoreID = ' + @iRMInvoiceTempTable + '.RmsStoreID   GROUP BY   TransactionEntry.StoreID,  TransactionEntry.TransactionNumber),0),  RmsMixedDocumentError = isnull((SELECT  DISTINCT(1)  FROM ' + @lTransactionEntryTable + ' WHERE  TransactionEntry.TransactionNumber = ' + @iRMInvoiceTempTable + '.RmsTransactionNumber  AND  TransactionEntry.StoreID = ' + @iRMInvoiceTempTable + '.RmsStoreID   AND  ((TransactionEntry.Quantity < 0 AND ' + @iSelectType + ' = 1)OR(TransactionEntry.Quantity > 0 AND ' + @iSelectType + ' = 2))   ),0),  CHEKAMNT =  ISNULL((SELECT  SUM(Amount)  FROM ' + @lTenderEntryTable + ',' + @lTenderTable + ' WHERE ' + @iRMInvoiceTempTable + '.RmsStoreID = TenderEntry.StoreID  AND ' + @iRMInvoiceTempTable + '.RmsTransactionNumber = TenderEntry.TransactionNumber  AND  TenderEntry.TenderID = Tender.[ID]  AND  Tender.AdditionalDetailType = 2  GROUP BY  TenderEntry.StoreID,  TenderEntry.TransactionNumber),0),   CASHAMNT = ISNULL((SELECT  SUM(Amount)   FROM ' + @lTenderEntryTable + ',' + @lTenderTable + ' WHERE ' + @iRMInvoiceTempTable + '.RmsStoreID = TenderEntry.StoreID   AND ' + @iRMInvoiceTempTable + '.RmsTransactionNumber = TenderEntry.TransactionNumber  AND  TenderEntry.TenderID = Tender.[ID]  AND  (Tender.AdditionalDetailType = 0  OR  Tender.AdditionalDetailType = 1  OR  Tender.AdditionalDetailType = 6  OR  Tender.AdditionalDetailType = 8)  GROUP BY  TenderEntry.StoreID,  TenderEntry.TransactionNumber),0),   CRCRDAMT =  ISNULL((SELECT   SUM(Amount)  FROM ' + @lTenderEntryTable + ',' + @lTenderTable + ' WHERE ' + @iRMInvoiceTempTable + '.RmsStoreID = TenderEntry.StoreID  AND ' + @iRMInvoiceTempTable + '.RmsTransactionNumber = TenderEntry.TransactionNumber  AND  TenderEntry.TenderID = Tender.[ID]  AND  (Tender.AdditionalDetailType = 3  OR  Tender.AdditionalDetailType = 7  OR  Tender.AdditionalDetailType = 9)  GROUP BY  TenderEntry.StoreID,  TenderEntry.TransactionNumber),0),  ACCTAMNT =  ISNULL((SELECT   SUM(Amount)  FROM ' + @lTenderEntryTable + ',' + @lTenderTable + ' WHERE ' + @iRMInvoiceTempTable + '.RmsStoreID = TenderEntry.StoreID  AND ' + @iRMInvoiceTempTable + '.RmsTransactionNumber = TenderEntry.TransactionNumber  AND  TenderEntry.TenderID = Tender.[ID]  AND  Tender.AdditionalDetailType = 4  GROUP BY  TenderEntry.StoreID,  TenderEntry.TransactionNumber),0),  FRTAMNT =  ISNULL((SELECT   SUM(Shipping.Charge)  FROM ' + @lShippingTable + ' WHERE  Shipping.TransactionNumber = ' + @iRMInvoiceTempTable + '.RmsTransactionNumber  AND  Shipping.StoreID = ' + @iRMInvoiceTempTable + '.RmsStoreID  GROUP BY  Shipping.StoreID,  Shipping.TransactionNumber),0)')
  
  EXEC RmsGetSplitRMInvoicesReturns
    @iRMInvoiceTempTable ,
    @iRMTaxTotalTemp ,
    @iRMDeptTotals ,
    @iRMTenderTotals ,
    @iRmsServerName ,
    @iHqDbName
  
  EXEC( 'UPDATE ' + @iRMInvoiceTempTable + ' SET  CHEKAMNT = 0,  CASHAMNT = 0,  ACCTAMNT = Total_Sales,  CRCRDAMT = 0  FROM ' + @lTransactionTable + ',' + @lOrderTable + ',' + @lOrderHistoryTable + ' WHERE ' + @iRMInvoiceTempTable + '.RmsStoreID = Transactions.StoreID  AND ' + @iRMInvoiceTempTable + '.RmsTransactionNumber = Transactions.TransactionNumber  AND ' + 'Transactions.RecallID = 0  AND ' + @iRMInvoiceTempTable + '.IVCLINNO = 0  AND   Transactions.StoreID = OrderHistory.StoreID  AND  Transactions.TransactionNumber = OrderHistory.TransactionNumber  AND  OrderHistory.StoreID = Orders.StoreID  AND   OrderHistory.OrderID = Orders.[ID]  AND  Orders.Type = 4')
  
  EXEC( 'INSERT INTO ' + @iRMTaxTotalTemp + '  (RmsTransactionNumber,  RmsStoreID,  RmsTaxID,  IVCLINNO,  TAXAMNT,  Taxable_Amount,  Total_Sales)  SELECT  TransactionNumber,  StoreID,  TaxID,  0,  SUM(Tax),  SUM(TaxableAmount),  0  FROM ' + @lTaxEntryTable + ' WHERE  TaxID <> 0  AND  EXISTS(SELECT  DISTINCT(1)  FROM ' + @iRMInvoiceTempTable + ' WHERE  TaxEntry.TransactionNumber = ' + @iRMInvoiceTempTable + '.RmsTransactionNumber  AND  TaxEntry.StoreID = ' + @iRMInvoiceTempTable + '.RmsStoreID  AND ' + @iRMInvoiceTempTable + '.IVCLINNO = 0)  GROUP BY  TransactionNumber,  StoreID,  TaxID')
  
  EXEC( 'UPDATE ' + @iRMTaxTotalTemp + ' SET  Total_Sales = ISNULL((SELECT   SUM(ROUND(CAST(Quantity * Price as MONEY), 2))  FROM ' + @lTransactionEntryTable + ' WHERE ' + @iRMTaxTotalTemp + '.RmsStoreID = TransactionEntry.StoreID  AND ' + @iRMTaxTotalTemp + '.RmsTransactionNumber = TransactionEntry.TransactionNumber  GROUP BY  TransactionEntry.StoreID,  TransactionEntry.TransactionNumber),0)  WHERE ' + @iRMTaxTotalTemp + '.IVCLINNO = 0')
  
  EXEC( 'INSERT INTO ' + @iRMDeptTotals + ' (RmsTransactionNumber,  RmsStoreID,  RmsDeptID,  IVCLINNO,  SLSAMNT,  COSTAMNT,  TAXAMNT)  SELECT  TransactionEntry.TransactionNumber,  TransactionEntry.StoreID,  ISNULL(Item.DepartmentID,0),  0,  SUM(ROUND(CAST(TransactionEntry.Price * TransactionEntry.Quantity as MONEY), 2)),  SUM(ROUND(CAST(TransactionEntry.Cost * TransactionEntry.Quantity as MONEY), 2)),  SUM(TransactionEntry.SalesTax)  FROM ' + @lTransactionEntryTable + ' LEFT OUTER JOIN ' + @lItemTable + ' ON ' + ' TransactionEntry.ItemID = Item.[ID]  WHERE  EXISTS(SELECT  DISTINCT(1)  FROM ' + @iRMInvoiceTempTable + ' WHERE  TransactionEntry.TransactionNumber = ' + @iRMInvoiceTempTable + '.RmsTransactionNumber  AND  TransactionEntry.StoreID = ' + @iRMInvoiceTempTable + '.RmsStoreID  AND ' + @iRMInvoiceTempTable + '.IVCLINNO = 0)  GROUP BY  TransactionEntry.TransactionNumber,  TransactionEntry.StoreID,  Item.DepartmentID')
  
  EXEC( 'INSERT INTO ' + @iRMTenderTotals + ' (RmsTransactionNumber,  RmsStoreID,  RmsTenderID,  IVCLINNO,  SLSAMNT)  SELECT  TenderEntry.TransactionNumber,  TenderEntry.StoreID,  TenderEntry.TenderID,  0,  SUM(Amount)  FROM ' + @lTenderEntryTable + ' WHERE  EXISTS(SELECT  DISTINCT(1)  FROM ' + @iRMInvoiceTempTable + ' WHERE  TenderEntry.TransactionNumber = ' + @iRMInvoiceTempTable + '.RmsTransactionNumber  AND  TenderEntry.StoreID = ' + @iRMInvoiceTempTable + '.RmsStoreID  AND ' + @iRMInvoiceTempTable + '.IVCLINNO = 0)  GROUP BY  TenderEntry.TransactionNumber,  TenderEntry.StoreID,  TenderEntry.TenderID')
  
  EXEC( 'DELETE ' + @iRMTenderTotals + ' FROM ' + @lTransactionTable + ',' + @lOrderTable + ',' + @lOrderHistoryTable + ' WHERE ' + @iRMTenderTotals + '.RmsStoreID = Transactions.StoreID  AND ' + @iRMTenderTotals + '.RmsTransactionNumber = Transactions.TransactionNumber  AND ' + @iRMTenderTotals + '.IVCLINNO = 0  AND ' + 'Transactions.RecallID = 0  AND   Transactions.StoreID = OrderHistory.StoreID  AND  Transactions.TransactionNumber = OrderHistory.TransactionNumber  AND  OrderHistory.StoreID = Orders.StoreID  AND   OrderHistory.OrderID = Orders.[ID]  AND  Orders.Type = 4')

