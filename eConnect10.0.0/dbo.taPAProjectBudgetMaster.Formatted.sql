

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAProjectBudgetMaster]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAProjectBudgetMaster]
  
create procedure [dbo].[taPAProjectBudgetMaster]
                @I_vPAPROJNUMBER             char(15),
                @I_vPACOSTCATID              char(15),
                @I_vPASTAT                   smallint  = null,
                @I_vPAProfitType             smallint  = null,
                @I_vPAbllngtype              smallint  = null,
                @I_vPABBeginDate             datetime  = null,
                @I_vPABEndDate               datetime  = null,
                @I_vLNITMSEQ                 int  = null,
                @I_vPALineItemSeq            int  = null,
                @I_vPABQuantity              numeric(19,5)  = null,
                @I_vPABUnitCost              numeric(19,5)  = null,
                @I_vPABProfitAmt             numeric(19,5)  = null,
                @I_vPABProfitPcnt            numeric(19,5)  = null,
                @I_vPABTotalCost             numeric(19,5)  = null,
                @I_vPABaselineOvhdAmtPerUnit numeric(19,5)  = null,
                @I_vPABaselineOvrhdPct       numeric(19,5)  = null,
                @I_vPABaselinePTaxOptions    smallint  = null,
                @I_vPABaselineCTaxSchedID    char(15)  = null,
                @I_vPABaselineSTaxOptions    smallint  = null,
                @I_vPABaselineBTaxSchedID    char(15)  = null,
                @I_vPAFQuantity              numeric(19,5)  = null,
                @I_vPAFUnitCost              numeric(19,5)  = null,
                @I_vPAFTotalCost             numeric(19,5)  = null,
                @I_vPAFProfitAmt             numeric(19,5)  = null,
                @I_vPAFProfitPcnt            numeric(19,5)  = null,
                @I_vPAForecastOvrhdAmtPerUnt numeric(19,5)  = null,
                @I_vPAForecastOvrhdPct       numeric(19,5)  = null,
                @I_vPAForecastPTaxOptions    smallint  = null,
                @I_vPAForecastCTaxSchedID    char(15)  = null,
                @I_vPAForecastSTaxOptions    smallint  = null,
                @I_vPAForecastBTaxSchedID    char(15)  = null,
                @I_vPAFBeginDate             datetime  = null,
                @I_vPAFEndDate               datetime  = null,
                @I_vPAUnit_of_Measure        char(9)  = null,
                @I_vUOMSCHDL                 char(11)  = null,
                @I_vPAPay_Code_Hourly        char(7)  = null,
                @I_vPAPay_Code_Salary        char(7)  = null,
                @I_vUpdateExisting           tinyint  = null,
                @I_vRollUpEst_to_Forecast    tinyint  = null,
                @I_vRequesterTrx             smallint  = 0,
                @I_vUSRDEFND1                char(50)  = '',
                @I_vUSRDEFND2                char(50)  = '',
                @I_vUSRDEFND3                char(50)  = '',
                @I_vUSRDEFND4                varchar(8000)  = '',
                @I_vUSRDEFND5                varchar(8000)  = '',
                @O_iErrorState               int  output,
                @oErrString                  varchar(255)  output     /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iStatus                                     int,
           @iCustomState                                int,
           @iCustomErrString                            varchar(255),
           @O_oErrorState                               int,
           @iError                                      int,
           @PAIV_Item_Checkbox                          tinyint,
           @PATU                                        smallint,
           @PASTAT                                      smallint,
           @PABase_Qty                                  numeric(19,5),
           @PABase_Unit_Cost                            numeric(19,5),
           @PABaseProfitAmount                          numeric(19,5),
           @PABaseOvhdCost                              numeric(19,5),
           @PAForecastBaseOvrhdAmt                      numeric(19,5),
           @PAForecastBaseQty                           numeric(19,5),
           @PAForecastBaseUnitCost                      numeric(19,5),
           @PAForecastBaseProfitAmt                     numeric(19,5),
           @PABBillings                                 numeric(19,5),
           @PABProfit                                   numeric(19,5),
           @PABProfitAmt                                numeric(19,5),
           @PABProfitPcnt                               numeric(19,5),
           @PABTaxPaidAmt                               numeric(19,5),
           @PABTaxChargedAmt                            numeric(19,5),
           @PABaselineOvhdCost                          numeric(19,5),
           @PAACTUALBEGDATE                             datetime,
           @PA_Actual_End_Date                          datetime,
           @PAFQuantity                                 numeric(19,5),
           @PAFUnitCost                                 numeric(19,5),
           @PAFTotalCost                                numeric(19,5),
           @PAFBillings                                 numeric(19,5),
           @PAFProfit                                   numeric(19,5),
           @PAFTaxPaidAmt                               numeric(19,5),
           @PAFTaxChargedAmt                            numeric(19,5),
           @PAFProfitAmt                                numeric(19,5),
           @PAFProfitPcnt                               numeric(19,5),
           @PAForecastOvhdCost                          numeric(19,5),
           @PAForecastPTaxOptions                       smallint,
           @PAForecastCTaxSchedID                       char(15),
           @PAForecastSTaxOptions                       smallint,
           @PAForecastBTaxSchedID                       char(15),
           @PAUOfMMult                                  numeric(19,5),
           @DECPLQTY                                    smallint,
           @DECPLCUR                                    smallint,
           @PALabor_Rate_Table_ID                       char(31),
           @PALabor_RateTable_Type                      smallint,
           @PALabor_Rate_Table_Acc                      tinyint,
           @PAEquip_Rate_Table_ID                       char(31),
           @PAEquip_Rate_Table_Acc                      tinyint,
           @TM_WIP_SRC                                  smallint,
           @TM_COGS_SRC                                 smallint,
           @TM_Cost_Contra_SRC                          smallint,
           @TM_Unbilled_AR_SRC                          smallint,
           @TM_Unbilled_Rev_SRC                         smallint,
           @TM_AR_SRC                                   smallint,
           @TM_Prj_Rev_SRC                              smallint,
           @TM_OVHD_SRC                                 smallint,
           @FF_WIP_SRC                                  smallint,
           @FF_Cost_Contra_SRC                          smallint,
           @FF_AR_SRC                                   smallint,
           @FF_Prj_Billings_SRC                         smallint,
           @FF_Prj_Expense_SRC                          smallint,
           @FF_Prj_Revenue_SRC                          smallint,
           @FF_Prj_Loss_SRC                             smallint,
           @FF_BIEE_SRC                                 smallint,
           @FF_EIEB_SRC                                 smallint,
           @FF_OVHD_SRC                                 smallint,
           @PAProject_Amount                            numeric(19,5),
           @PAOverheadRateMethod                        smallint,
           @PAForecastOvrhdAmtPerUnt                    numeric(19,5),
           @PAForecastOvrhdPct                          numeric(19,5),
           @PAUBeginDate                                datetime,
           @PAUEndDate                                  datetime,
           @PAUnpostedQty                               numeric(19,5),
           @PAUnpostedTotalCostN                        numeric(19,5),
           @PAUnposted_Overhead                         numeric(19,5),
           @PAUnpostedProfitN                           numeric(19,5),
           @PAUnposted_Tax_Amount                       numeric(19,5),
           @PAUnpostAccrRevN                            numeric(19,5),
           @PAUnpostedCommitedQty                       numeric(19,5),
           @PAUnpostedCommitedCost                      numeric(19,5),
           @PAUnpostedCommitedTaxAmt                    numeric(19,5),
           @PAUNPOSTBIEEAMOUNT                          numeric(19,5),
           @PAUNPEIEBAMOUNT                             numeric(19,5),
           @PAunpostbillamt                             numeric(19,5),
           @PAPostedQty                                 numeric(19,5),
           @PAPostedTotalCostN                          numeric(19,5),
           @PAPosted_Overhead                           numeric(19,5),
           @PAPostedProfitN                             numeric(19,5),
           @PAPosted_Tax_Amount                         numeric(19,5),
           @PAPosted_Accr_RevN                          numeric(19,5),
           @PAPostedCommitedQty                         numeric(19,5),
           @PAPostedCommitedCost                        numeric(19,5),
           @PAPostedCommitedTaxAmt                      numeric(19,5),
           @PAPostRecogRevN                             numeric(19,5),
           @PAPOSBIEEAMOUNT                             numeric(19,5),
           @PAPOSEIEBAMOUNT                             numeric(19,5),
           @PApostbillamt                               numeric(19,5),
           @PAWrite_UpDown_Amount                       numeric(19,5),
           @PABilled_QtyN                               numeric(19,5),
           @PABilled_Cost                               numeric(19,5),
           @PABilled_Accrued_Revenu                     numeric(19,5),
           @PACostPcntCompleted                         numeric(19,5),
           @PAQuantityPcntCompleted                     numeric(19,5),
           @PA_Receipts_Amount                          numeric(19,5),
           @PA_Actual_Receipts_Amoun                    numeric(19,5),
           @PA_Earnings                                 numeric(19,5),
           @PA_Cost_of_Earnings                         numeric(19,5),
           @PAUnpostBillN                               numeric(19,5),
           @PAUnpostDiscDolAmtN                         numeric(19,5),
           @PAUnposted_Sales_Tax_Am                     numeric(19,5),
           @PAPostedBillingsN                           numeric(19,5),
           @PAPostedDiscDolAmtN                         numeric(19,5),
           @PAPosted_Sales_Tax_Amou                     numeric(19,5),
           @PABCWPAMT                                   numeric(19,5),
           @PABCWSAMT                                   numeric(19,5),
           @PAACWPAMT                                   numeric(19,5),
           @PAbillnoteidx                               numeric(19,5),
           @PAUnpostedRecogRevN                         numeric(19,5),
           @PAApproved_Accrued_Reve                     numeric(19,5),
           @PAApproved_Cost                             numeric(19,5),
           @PAApproved_Quantity                         numeric(19,5),
           @PACommitted_Costs                           numeric(19,5),
           @PACommitted_Qty                             numeric(19,5),
           @PAPOQty                                     numeric(19,5),
           @PAPOCost                                    numeric(19,5),
           @PAPOPostedQty                               numeric(19,5),
           @PAPOPostedCost                              numeric(19,5),
           @WROFAMNT                                    numeric(19,5),
           @ActualWriteOffAmount                        numeric(19,5),
           @DISTKNAM                                    numeric(19,5),
           @ActualDiscTakenAmount                       numeric(19,5),
           @PAsfid                                      smallint,
           @PAPreviouslyOpen                            tinyint,
           @PAtaxpaidamt                                numeric(19,5),
           @PAPostedTaxPaidN                            numeric(19,5),
           @PAPOSTRETAMT                                numeric(19,5),
           @PAUNPOSTRETAMT                              numeric(19,5),
           @PA_Write_Off_Tax_Amount                     numeric(19,5),
           @PAActualWOTaxAmt                            numeric(19,5),
           @PA_Terms_Taken_Tax_Amt                      numeric(19,5),
           @PAActualTermsTakenTax                       numeric(19,5),
           @PACHGORDNO                                  char(17),
           @PAUNAPPRVPROJAMT                            numeric(19,5),
           @PAUNAPFRTOTBILL                             numeric(19,5),
           @PAUNAPPFRTOTCST                             numeric(19,5),
           @PAUNFRCTOTPROF                              numeric(19,5),
           @PAUNPFRTOTQTY                               numeric(19,5),
           @PAUNABSTOTBILL                              numeric(19,5),
           @PAUNPBSTOTCST                               numeric(19,5),
           @PAUNPBSLTOTPROF                             numeric(19,5),
           @PAUNPBSTOTQTY                               numeric(19,5),
           @PATOTCHGORDAMT                              numeric(19,5),
           @PAPosted_Earnings                           numeric(19,5),
           @PAActualCostofEarnings                      numeric(19,5),
           @PAUnpostedLossAmount                        numeric(19,5),
           @PAActualLossAmount                          numeric(19,5),
           @DBName                                      char(50),
           @O_iInitErrorState                           int,
           @oInitErrString                              varchar(255),
           @SLPRSNID                                    char(15),
           @COMPRCNT                                    int,
           @COMAPPTO                                    smallint,
           @SALSTERR                                    char(15),
           @PAPROJNUMBER                                char(17),
           @PABBeginDate                                datetime,
           @PABEndDate                                  datetime,
           @PACOSTCATID                                 char(17),
           @PAPurchase_Tax_Options                      smallint,
           @PAcostaxscheduleid                          char(17),
           @PASales_Tax_Options                         smallint,
           @PAbilltaxscheduleid                         char(17),
           @PAUnit_of_Measure                           char(17),
           @UOMSCHDL                                    char(17),
           @PAPay_Code_Hourly                           char(17),
           @PAPay_Code_Salary                           char(17),
           @PAOvhdAmtPerUnit                            numeric(19,5),
           @PAOverheaPercentage                         numeric(19,5),
           @PAUNITCOST                                  numeric(19,5),
           @PAProjectType                               smallint,
           @PATMProfitType                              smallint,
           @PATMProfitAmount                            numeric(19,5),
           @PATMProfitPercent                           numeric(19,5),
           @PAProfit_Type__CP                           smallint,
           @PAProfitAmountCP                            numeric(19,5),
           @PAProfitPercentCP                           numeric(19,5),
           @PAFFProfitType                              smallint,
           @PAFFProfitAmount                            numeric(19,5),
           @PAFFProfitPercent                           numeric(19,5),
           @PACOSTCATCLASID                             char(17),
           @PAinactive                                  smallint,
           @l_tmp_cost_trx_id                           char(2),
           @PAFBeginDate                                datetime,
           @PAFEndDate                                  datetime,
           @userdate                                    datetime,
           @iUpdDistErrState                            integer,
           @fClosed                                     smallint,
           @YEAR1                                       integer,
           @l_Period                                    integer,
           @nErr                                        integer,
           @PACONTNUMBER                                char(17),
           @CUSTNMBR                                    char(17),
           @O_glCalculateGLPeriodErrorState             int,
           @Existing_PACOSTCATID                        char(15),
           @Existing_PAPROJNUMBER                       char(15),
           @Existing_PASTAT                             smallint,
           @Existing_PABProfitAmt                       numeric(19,5),
           @Existing_PAFProfitAmt                       numeric(19,5),
           @Existing_PABQuantity                        numeric(19,5),
           @Existing_PABUnitCost                        numeric(19,5),
           @Existing_PABTotalCost                       numeric(19,5),
           @Existing_PABProfit                          numeric(19,5),
           @Existing_PABBillings                        numeric(19,5),
           @Existing_PABaselineOvhdCost                 numeric(19,5),
           @Existing_PAFQuantity                        numeric(19,5),
           @Existing_PAFUnitCost                        numeric(19,5),
           @Existing_PAFTotalCost                       numeric(19,5),
           @Existing_PAFProfit                          numeric(19,5),
           @Existing_PAFBillings                        numeric(19,5),
           @Existing_PAForecastOvhdCost                 numeric(19,5),
           @Existing_PAProfitType                       smallint,
           @Existing_PAbllngtype                        smallint,
           @PAbllngtype                                 smallint,
           @Existing_PAPreviouslyOpen                   smallint,
           @PAFEndDate_Period                           int,
           @PABEndDate_Period                           int,
           @i_startyearupdate                           int,
           @i_endyearupdate                             int,
           @i_baselinestartyearupdate                   int,
           @i_forecaststartyearupdate                   int,
           @PA_UpdateProjectDatesErrorState             int,
           @PA_UpdateContractDatesErrorState            int,
           @PA_Create_Periodic_Budget_RecordsErrorState int,
           @O_glCalculateGLPeriodErrorState1            int,
           @O_glCalculateGLPeriodErrorState2            int,
           @O_glCalculateGLPeriodErrorState3            int,
           @O_glCalculateGLPeriodErrorState4            int,
           @PABBeginDate_Period                         int,
           @PABEndDate_YEAR                             int,
           @PAFBeginDate_Period                         int,
           @PAFEndDate_YEAR                             int,
           @PABBeginDate_YEAR                           int,
           @PAFBeginDate_YEAR                           int,
           @NUMOFPER                                    int,
           @l_fore_ctr                                  int,
           @l_base_ctr                                  int,
           @Existing_PABBeginDate                       datetime,
           @Existing_PABEndDate                         datetime,
           @Existing_PABaselinePTaxOptions              int,
           @Existing_PABaselineCTaxSchedID              char(15),
           @Existing_PABaselineSTaxOptions              int,
           @Existing_PABaselineBTaxSchedID              char(15),
           @Existing_PAForecastPTaxOptions              int,
           @Existing_PAForecastCTaxSchedID              char(15),
           @Existing_PAForecastSTaxOptions              int,
           @Existing_PAForecastBTaxSchedID              char(15),
           @Existing_PAFBeginDate                       datetime,
           @Existing_PAFEndDate                         datetime,
           @Existing_PAUnit_of_Measure                  char(9),
           @Existing_UOMSCHDL                           char(11),
           @Existing_PAPay_Code_Hourly                  char(7),
           @Existing_PAPay_Code_Salary                  char(7),
           @Existing_LNITMSEQ                           int,
           @Existing_PALineItemSeq                      int,
           @Existing_PABProfitPcnt                      numeric(19,5),
           @Existing_PABaselineOvhdAmtPerUnit           numeric(19,5),
           @Existing_PABaselineOvrhdPct                 numeric(19,5),
           @Existing_PAFProfitPcnt                      numeric(19,5),
           @Existing_PAForecastOvrhdAmtPerUnt           numeric(19,5),
           @Existing_PAForecastOvrhdPct                 numeric(19,5),
           @Existing_PATU                               smallint,
           @PAMCBProfitAmt                              numeric(19,5),
           @PAMCBBillings                               numeric(19,5),
           @PAMCFProfitAmt                              numeric(19,5),
           @PAMCFBillings                               numeric(19,5),
           @PAMCBaseProfitAmount                        numeric(19,5),
           @PAMCForecastBProfitAmt                      numeric(19,5),
           @PAMCProjectAmount                           numeric(19,5),
           @PAMCActualBillings                          numeric(19,5),
           @PAMCActualDiscountAmt                       numeric(19,5),
           @PAMCActualSalesTaxAmt                       numeric(19,5),
           @PAMCUnpostBillN                             numeric(19,5),
           @PAMCUnpostedDiscount                        numeric(19,5),
           @PAMCUnpostedSalesTax                        numeric(19,5),
           @sCompanyID                                  smallint,
           @iGetNextNoteIdxErrState                     int,
           @OPENYEAR                                    smallint
  
  select @iStatus = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @iError = 0,
         @O_oErrorState = 0,
         @PAIV_Item_Checkbox = 0,
         @PATU = 0,
         @PASTAT = 4,
         @PABase_Qty = 0,
         @PABase_Unit_Cost = 0,
         @PABaseProfitAmount = 0,
         @PABaseOvhdCost = 0,
         @PAForecastBaseOvrhdAmt = 0,
         @PAForecastBaseQty = 0,
         @PAForecastBaseUnitCost = 0,
         @PAForecastBaseProfitAmt = 0,
         @PABBillings = 0,
         @PABProfit = 0,
         @PABProfitAmt = 0,
         @PABProfitPcnt = 0,
         @PABTaxPaidAmt = 0,
         @PABTaxChargedAmt = 0,
         @PABaselineOvhdCost = 0,
         @PAACTUALBEGDATE = '',
         @PA_Actual_End_Date = '',
         @PAFQuantity = 0,
         @PAFUnitCost = 0,
         @PAFTotalCost = 0,
         @PAFBillings = 0,
         @PAFProfit = 0,
         @PAFTaxPaidAmt = 0,
         @PAFTaxChargedAmt = 0,
         @PAFProfitAmt = 0,
         @PAFProfitPcnt = 0,
         @PAForecastOvhdCost = 0,
         @PAForecastPTaxOptions = 0,
         @PAForecastCTaxSchedID = '',
         @PAForecastSTaxOptions = 0,
         @PAForecastBTaxSchedID = '',
         @PAUOfMMult = 0,
         @DECPLQTY = 0,
         @DECPLCUR = 0,
         @PALabor_Rate_Table_ID = '',
         @PALabor_RateTable_Type = 0,
         @PALabor_Rate_Table_Acc = 0,
         @PAEquip_Rate_Table_ID = '',
         @PAEquip_Rate_Table_Acc = 0,
         @TM_WIP_SRC = 0,
         @TM_COGS_SRC = 0,
         @TM_Cost_Contra_SRC = 0,
         @TM_Unbilled_AR_SRC = 0,
         @TM_Unbilled_Rev_SRC = 0,
         @TM_AR_SRC = 0,
         @TM_Prj_Rev_SRC = 0,
         @TM_OVHD_SRC = 0,
         @FF_WIP_SRC = 0,
         @FF_Cost_Contra_SRC = 0,
         @FF_AR_SRC = 0,
         @FF_Prj_Billings_SRC = 0,
         @FF_Prj_Expense_SRC = 0,
         @FF_Prj_Revenue_SRC = 0,
         @FF_Prj_Loss_SRC = 0,
         @FF_BIEE_SRC = 0,
         @FF_EIEB_SRC = 0,
         @FF_OVHD_SRC = 0,
         @PAProject_Amount = 0,
         @PAOverheadRateMethod = 0,
         @PAForecastOvrhdAmtPerUnt = 0,
         @PAForecastOvrhdPct = 0,
         @PAUBeginDate = '',
         @PAUEndDate = '',
         @PAUnpostedQty = 0,
         @PAUnpostedTotalCostN = 0,
         @PAUnposted_Overhead = 0,
         @PAUnpostedProfitN = 0,
         @PAUnposted_Tax_Amount = 0,
         @PAUnpostAccrRevN = 0,
         @PAUnpostedCommitedQty = 0,
         @PAUnpostedCommitedCost = 0,
         @PAUnpostedCommitedTaxAmt = 0,
         @PAUNPOSTBIEEAMOUNT = 0,
         @PAUNPEIEBAMOUNT = 0,
         @PAunpostbillamt = 0,
         @PAPostedQty = 0,
         @PAPostedTotalCostN = 0,
         @PAPosted_Overhead = 0,
         @PAPostedProfitN = 0,
         @PAPosted_Tax_Amount = 0,
         @PAPosted_Accr_RevN = 0,
         @PAPostedCommitedQty = 0,
         @PAPostedCommitedCost = 0,
         @PAPostedCommitedTaxAmt = 0,
         @PAPostRecogRevN = 0,
         @PAPOSBIEEAMOUNT = 0,
         @PAPOSEIEBAMOUNT = 0,
         @PApostbillamt = 0,
         @PAWrite_UpDown_Amount = 0,
         @PABilled_QtyN = 0,
         @PABilled_Cost = 0,
         @PABilled_Accrued_Revenu = 0,
         @PACostPcntCompleted = 0,
         @PAQuantityPcntCompleted = 0,
         @PA_Receipts_Amount = 0,
         @PA_Actual_Receipts_Amoun = 0,
         @PA_Earnings = 0,
         @PA_Cost_of_Earnings = 0,
         @PAUnpostBillN = 0,
         @PAUnpostDiscDolAmtN = 0,
         @PAUnposted_Sales_Tax_Am = 0,
         @PAPostedBillingsN = 0,
         @PAPostedDiscDolAmtN = 0,
         @PAPosted_Sales_Tax_Amou = 0,
         @PABCWPAMT = 0,
         @PABCWSAMT = 0,
         @PAACWPAMT = 0,
         @PAbillnoteidx = 0,
         @PAUnpostedRecogRevN = 0,
         @PAApproved_Accrued_Reve = 0,
         @PAApproved_Cost = 0,
         @PAApproved_Quantity = 0,
         @PACommitted_Costs = 0,
         @PACommitted_Qty = 0,
         @PAPOQty = 0,
         @PAPOCost = 0,
         @PAPOPostedQty = 0,
         @PAPOPostedCost = 0,
         @WROFAMNT = 0,
         @ActualWriteOffAmount = 0,
         @DISTKNAM = 0,
         @ActualDiscTakenAmount = 0,
         @PAsfid = 0,
         @PAPreviouslyOpen = 0,
         @PAtaxpaidamt = 0,
         @PAPostedTaxPaidN = 0,
         @PAPOSTRETAMT = 0,
         @PAUNPOSTRETAMT = 0,
         @PA_Write_Off_Tax_Amount = 0,
         @PAActualWOTaxAmt = 0,
         @PA_Terms_Taken_Tax_Amt = 0,
         @PAActualTermsTakenTax = 0,
         @PACHGORDNO = '',
         @PAUNAPPRVPROJAMT = 0,
         @PAUNAPFRTOTBILL = 0,
         @PAUNAPPFRTOTCST = 0,
         @PAUNFRCTOTPROF = 0,
         @PAUNPFRTOTQTY = 0,
         @PAUNABSTOTBILL = 0,
         @PAUNPBSTOTCST = 0,
         @PAUNPBSLTOTPROF = 0,
         @PAUNPBSTOTQTY = 0,
         @PATOTCHGORDAMT = 0,
         @PAPosted_Earnings = 0,
         @PAActualCostofEarnings = 0,
         @PAUnpostedLossAmount = 0,
         @PAActualLossAmount = 0,
         @DBName = '',
         @O_iInitErrorState = 0,
         @oInitErrString = '',
         @SLPRSNID = '',
         @COMPRCNT = 0,
         @COMAPPTO = 0,
         @SALSTERR = '',
         @PAPROJNUMBER = '',
         @PABBeginDate = '',
         @PABEndDate = '',
         @PACOSTCATID = '',
         @PAPurchase_Tax_Options = 0,
         @PAcostaxscheduleid = '',
         @PASales_Tax_Options = 0,
         @PAbilltaxscheduleid = '',
         @PAUnit_of_Measure = '',
         @UOMSCHDL = '',
         @PAPay_Code_Hourly = '',
         @PAPay_Code_Salary = '',
         @PAOvhdAmtPerUnit = 0,
         @PAOverheaPercentage = 0,
         @PAUNITCOST = 0,
         @PAProjectType = 0,
         @PATMProfitType = 0,
         @PATMProfitAmount = 0,
         @PATMProfitPercent = 0,
         @PAProfit_Type__CP = 0,
         @PAProfitAmountCP = 0,
         @PAProfitPercentCP = 0,
         @PAFFProfitType = 0,
         @PAFFProfitAmount = 0,
         @PAFFProfitPercent = 0,
         @PACOSTCATCLASID = '',
         @PAinactive = 0,
         @l_tmp_cost_trx_id = '',
         @PAFBeginDate = '',
         @PAFEndDate = '',
         @userdate = '',
         @iUpdDistErrState = 0,
         @fClosed = 0,
         @YEAR1 = 0,
         @l_Period = 0,
         @nErr = 0,
         @PACONTNUMBER = '',
         @CUSTNMBR = '',
         @O_glCalculateGLPeriodErrorState = 0,
         @Existing_PACOSTCATID = '',
         @Existing_PAPROJNUMBER = '',
         @Existing_PASTAT = 0,
         @Existing_PABProfitAmt = 0,
         @Existing_PAFProfitAmt = 0,
         @Existing_PABQuantity = 0,
         @Existing_PABUnitCost = 0,
         @Existing_PABTotalCost = 0,
         @Existing_PABProfit = 0,
         @Existing_PABBillings = 0,
         @Existing_PABaselineOvhdCost = 0,
         @Existing_PAFQuantity = 0,
         @Existing_PAFUnitCost = 0,
         @Existing_PAFTotalCost = 0,
         @Existing_PAFProfit = 0,
         @Existing_PAFBillings = 0,
         @Existing_PAForecastOvhdCost = 0,
         @Existing_PAProfitType = 0,
         @Existing_PAbllngtype = 0,
         @PAbllngtype = 0,
         @Existing_PAPreviouslyOpen = 0,
         @PAFEndDate_Period = 0,
         @PABEndDate_Period = 0,
         @i_startyearupdate = '',
         @i_endyearupdate = '',
         @i_baselinestartyearupdate = '',
         @i_forecaststartyearupdate = '',
         @PA_UpdateProjectDatesErrorState = 0,
         @PA_UpdateContractDatesErrorState = 0,
         @PA_Create_Periodic_Budget_RecordsErrorState = 0,
         @O_glCalculateGLPeriodErrorState1 = 0,
         @O_glCalculateGLPeriodErrorState2 = 0,
         @O_glCalculateGLPeriodErrorState3 = 0,
         @O_glCalculateGLPeriodErrorState4 = 0,
         @PABBeginDate_Period = 0,
         @PABEndDate_YEAR = 0,
         @PAFBeginDate_Period = 0,
         @PAFEndDate_YEAR = 0,
         @PABBeginDate_YEAR = 0,
         @PAFBeginDate_YEAR = 0,
         @NUMOFPER = 0,
         @l_fore_ctr = 0,
         @l_base_ctr = 0,
         @Existing_PABBeginDate = '',
         @Existing_PABEndDate = '',
         @Existing_PABaselinePTaxOptions = 0,
         @Existing_PABaselineCTaxSchedID = '',
         @Existing_PABaselineSTaxOptions = 0,
         @Existing_PABaselineBTaxSchedID = '',
         @Existing_PAForecastPTaxOptions = 0,
         @Existing_PAForecastCTaxSchedID = '',
         @Existing_PAForecastSTaxOptions = 0,
         @Existing_PAForecastBTaxSchedID = '',
         @Existing_PAFBeginDate = '',
         @Existing_PAFEndDate = '',
         @Existing_PAUnit_of_Measure = '',
         @Existing_UOMSCHDL = '',
         @Existing_PAPay_Code_Hourly = '',
         @Existing_PAPay_Code_Salary = '',
         @Existing_LNITMSEQ = 0,
         @Existing_PALineItemSeq = 0,
         @Existing_PABProfitPcnt = 0,
         @Existing_PABaselineOvhdAmtPerUnit = 0,
         @Existing_PABaselineOvrhdPct = 0,
         @Existing_PAFQuantity = 0,
         @Existing_PAFUnitCost = 0,
         @Existing_PAFTotalCost = 0,
         @Existing_PAFProfitAmt = 0,
         @Existing_PAFProfitPcnt = 0,
         @Existing_PAForecastOvrhdAmtPerUnt = 0,
         @Existing_PAForecastOvrhdPct = 0,
         @O_iErrorState = 0,
         @Existing_PATU = 0,
         @PAMCBProfitAmt = 0,
         @PAMCBBillings = 0,
         @PAMCFProfitAmt = 0,
         @PAMCFBillings = 0,
         @PAMCBaseProfitAmount = 0,
         @PAMCForecastBProfitAmt = 0,
         @PAMCProjectAmount = 0,
         @PAMCActualBillings = 0,
         @PAMCActualDiscountAmt = 0,
         @PAMCActualSalesTaxAmt = 0,
         @PAMCUnpostBillN = 0,
         @PAMCUnpostedDiscount = 0,
         @PAMCUnpostedSalesTax = 0,
         @sCompanyID = 0,
         @iGetNextNoteIdxErrState = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  select @DBName = DB_Name()
  
  exec @iStatus = taPAProjectBudgetMasterPre
    @I_vPAPROJNUMBER output ,
    @I_vPACOSTCATID output ,
    @I_vPASTAT output ,
    @I_vPAProfitType output ,
    @I_vPAbllngtype output ,
    @I_vPABBeginDate output ,
    @I_vPABEndDate output ,
    @I_vLNITMSEQ output ,
    @I_vPALineItemSeq output ,
    @I_vPABQuantity output ,
    @I_vPABUnitCost output ,
    @I_vPABProfitAmt output ,
    @I_vPABProfitPcnt output ,
    @I_vPABTotalCost output ,
    @I_vPABaselinePTaxOptions output ,
    @I_vPABaselineCTaxSchedID output ,
    @I_vPABaselineSTaxOptions output ,
    @I_vPABaselineBTaxSchedID output ,
    @I_vPAFQuantity output ,
    @I_vPAFUnitCost output ,
    @I_vPAFTotalCost output ,
    @I_vPAFProfitAmt output ,
    @I_vPAFProfitPcnt output ,
    @I_vPAForecastOvrhdAmtPerUnt output ,
    @I_vPAForecastOvrhdPct output ,
    @I_vPAForecastPTaxOptions output ,
    @I_vPAForecastCTaxSchedID output ,
    @I_vPAForecastSTaxOptions output ,
    @I_vPAForecastBTaxSchedID output ,
    @I_vPAFBeginDate output ,
    @I_vPAFEndDate output ,
    @I_vPAUnit_of_Measure output ,
    @I_vUOMSCHDL output ,
    @I_vPAPay_Code_Hourly output ,
    @I_vPAPay_Code_Salary output ,
    @I_vPABaselineOvhdAmtPerUnit output ,
    @I_vPABaselineOvrhdPct output ,
    @I_vUpdateExisting output ,
    @I_vRollUpEst_to_Forecast output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 5172
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAPROJNUMBER is null 
       or @I_vPACOSTCATID is null)
    begin
      select @O_iErrorState = 5173
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAPROJNUMBER = ''
       or @I_vPACOSTCATID = '')
    begin
      select @O_iErrorState = 5174
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @userdate = cast(getdate() as varchar(12))
  
  if (@I_vPABQuantity < 0
       or @I_vPABUnitCost < 0
       or @I_vPABTotalCost < 0
       or @I_vPABProfitAmt < 0
       or @I_vPABProfitPcnt < 0
       or @I_vPABaselineOvhdAmtPerUnit < 0
       or @I_vPABaselineOvrhdPct < 0)
    begin
      select @O_iErrorState = 5175
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vPAPROJNUMBER = UPPER(@I_vPAPROJNUMBER)
  
  if (@I_vUpdateExisting = 1)
    begin
      select @Existing_PACOSTCATID = PACOSTCATID,
             @Existing_PAPROJNUMBER = PAPROJNUMBER,
             @Existing_PAProfitType = PAProfitType,
             @Existing_PAbllngtype = PAbllngtype,
             @Existing_PASTAT = PASTAT,
             @Existing_PAFProfitAmt = PAFProfitAmt,
             @Existing_PABQuantity = PABQuantity,
             @Existing_PABUnitCost = PABUnitCost,
             @Existing_PABTotalCost = PABTotalCost,
             @Existing_PABProfit = PABProfit,
             @Existing_PABProfitAmt = PABProfitAmt,
             @Existing_PABBillings = PABBillings,
             @Existing_PABaselineOvhdCost = PABaselineOvhdCost,
             @Existing_PAFQuantity = PAFQuantity,
             @Existing_PAFUnitCost = PAFUnitCost,
             @Existing_PAFTotalCost = PAFTotalCost,
             @Existing_PAFProfit = PAFProfit,
             @Existing_PAFBillings = PAFBillings,
             @Existing_PAForecastOvhdCost = PAForecastOvhdCost,
             @Existing_PAPreviouslyOpen = PAPreviouslyOpen,
             @PAACTUALBEGDATE = PAACTUALBEGDATE,
             @PA_Actual_End_Date = PA_Actual_End_Date,
             @DECPLQTY = DECPLQTY,
             @Existing_PABBeginDate = PABBeginDate,
             @Existing_PABEndDate = PABEndDate,
             @Existing_PABaselinePTaxOptions = PABaselinePTaxOptions,
             @Existing_PABaselineCTaxSchedID = PABaselineCTaxSchedID,
             @Existing_PABaselineSTaxOptions = PABaselineSTaxOptions,
             @Existing_PABaselineBTaxSchedID = PABaselineBTaxSchedID,
             @Existing_PAForecastPTaxOptions = PAForecastPTaxOptions,
             @Existing_PAForecastCTaxSchedID = PAForecastCTaxSchedID,
             @Existing_PAForecastSTaxOptions = PAForecastSTaxOptions,
             @Existing_PAForecastBTaxSchedID = PAForecastBTaxSchedID,
             @Existing_PAFBeginDate = PAFBeginDate,
             @Existing_PAFEndDate = PAFEndDate,
             @Existing_PAUnit_of_Measure = PAUnit_of_Measure,
             @Existing_UOMSCHDL = UOMSCHDL,
             @Existing_PAPay_Code_Hourly = PAPay_Code_Hourly,
             @Existing_PAPay_Code_Salary = PAPay_Code_Salary,
             @Existing_LNITMSEQ = LNITMSEQ,
             @Existing_PALineItemSeq = PALineItemSeq,
             @Existing_PABProfitPcnt = PABProfitPcnt,
             @Existing_PABaselineOvhdAmtPerUnit = PABaselineOvhdAmtPerUnit,
             @Existing_PABaselineOvrhdPct = PABaselineOvrhdPct,
             @Existing_PAFUnitCost = PAFUnitCost,
             @Existing_PAFProfitPcnt = PAFProfitPcnt,
             @Existing_PAForecastOvrhdAmtPerUnt = PAForecastOvrhdAmtPerUnt,
             @Existing_PAForecastOvrhdPct = PAForecastOvrhdPct,
             @Existing_PATU = PATU
      from   PA01301 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PACOSTCATID = @I_vPACOSTCATID
      
      select @I_vPABBeginDate = case 
                                  when (@I_vPABBeginDate is null)
                                       and (@Existing_PACOSTCATID <> '') then @Existing_PABBeginDate
                                  when (@I_vPABBeginDate is null)
                                       and (@Existing_PACOSTCATID = '') then ''
                                  else @I_vPABBeginDate
                                end,
             @I_vPABEndDate = case 
                                when (@I_vPABEndDate is null)
                                     and (@Existing_PACOSTCATID <> '') then @Existing_PABEndDate
                                when (@I_vPABEndDate is null)
                                     and (@Existing_PACOSTCATID = '') then ''
                                else @I_vPABEndDate
                              end,
             @I_vPABaselinePTaxOptions = case 
                                           when (@I_vPABaselinePTaxOptions is null)
                                                and (@Existing_PACOSTCATID <> '') then @Existing_PABaselinePTaxOptions
                                           when (@I_vPABaselinePTaxOptions is null)
                                                and (@Existing_PACOSTCATID = '') then 0
                                           else @I_vPABaselinePTaxOptions
                                         end,
             @I_vPABaselineCTaxSchedID = case 
                                           when (@I_vPABaselineCTaxSchedID is null)
                                                and (@Existing_PACOSTCATID <> '') then @Existing_PABaselineCTaxSchedID
                                           when (@I_vPABaselineCTaxSchedID is null)
                                                and (@Existing_PACOSTCATID = '') then ''
                                           else @I_vPABaselineCTaxSchedID
                                         end,
             @I_vPABaselineSTaxOptions = case 
                                           when (@I_vPABaselineSTaxOptions is null)
                                                and (@Existing_PACOSTCATID <> '') then @Existing_PABaselineSTaxOptions
                                           when (@I_vPABaselineSTaxOptions is null)
                                                and (@Existing_PACOSTCATID = '') then 0
                                           else @I_vPABaselineSTaxOptions
                                         end,
             @I_vPABaselineBTaxSchedID = case 
                                           when (@I_vPABaselineBTaxSchedID is null)
                                                and (@Existing_PACOSTCATID <> '') then @Existing_PABaselineBTaxSchedID
                                           when (@I_vPABaselineBTaxSchedID is null)
                                                and (@Existing_PACOSTCATID = '') then ''
                                           else @I_vPABaselineBTaxSchedID
                                         end,
             @I_vPAForecastPTaxOptions = case 
                                           when (@I_vPAForecastPTaxOptions is null)
                                                and (@Existing_PACOSTCATID <> '') then @Existing_PAForecastPTaxOptions
                                           when (@I_vPAForecastPTaxOptions is null)
                                                and (@Existing_PACOSTCATID = '') then 0
                                           else @I_vPAForecastPTaxOptions
                                         end,
             @I_vPAForecastCTaxSchedID = case 
                                           when (@I_vPAForecastCTaxSchedID is null)
                                                and (@Existing_PACOSTCATID <> '') then @Existing_PAForecastCTaxSchedID
                                           when (@I_vPAForecastCTaxSchedID is null)
                                                and (@Existing_PACOSTCATID = '') then ''
                                           else @I_vPAForecastCTaxSchedID
                                         end,
             @I_vPAForecastSTaxOptions = case 
                                           when (@I_vPAForecastSTaxOptions is null)
                                                and (@Existing_PACOSTCATID <> '') then @Existing_PAForecastSTaxOptions
                                           when (@I_vPAForecastSTaxOptions is null)
                                                and (@Existing_PACOSTCATID = '') then 0
                                           else @I_vPAForecastSTaxOptions
                                         end,
             @I_vPAForecastBTaxSchedID = case 
                                           when (@I_vPAForecastBTaxSchedID is null)
                                                and (@Existing_PACOSTCATID <> '') then @Existing_PAForecastBTaxSchedID
                                           when (@I_vPAForecastBTaxSchedID is null)
                                                and (@Existing_PACOSTCATID = '') then ''
                                           else @I_vPAForecastBTaxSchedID
                                         end,
             @I_vPAFBeginDate = case 
                                  when (@I_vPAFBeginDate is null)
                                       and (@Existing_PACOSTCATID <> '') then @Existing_PAFBeginDate
                                  when (@I_vPAFBeginDate is null)
                                       and (@Existing_PACOSTCATID = '') then ''
                                  else @I_vPAFBeginDate
                                end,
             @I_vPAFEndDate = case 
                                when (@I_vPAFEndDate is null)
                                     and (@Existing_PACOSTCATID <> '') then @Existing_PAFEndDate
                                when (@I_vPAFEndDate is null)
                                     and (@Existing_PACOSTCATID = '') then ''
                                else @I_vPAFEndDate
                              end,
             @I_vPAUnit_of_Measure = case 
                                       when (@I_vPAUnit_of_Measure is null)
                                            and (@Existing_PACOSTCATID <> '') then @Existing_PAUnit_of_Measure
                                       when (@I_vPAUnit_of_Measure is null)
                                            and (@Existing_PACOSTCATID = '') then ''
                                       else @I_vPAUnit_of_Measure
                                     end,
             @I_vUOMSCHDL = case 
                              when (@I_vUOMSCHDL is null)
                                   and (@Existing_PACOSTCATID <> '') then @Existing_UOMSCHDL
                              when (@I_vUOMSCHDL is null)
                                   and (@Existing_PACOSTCATID = '') then ''
                              else @I_vUOMSCHDL
                            end,
             @I_vPAPay_Code_Hourly = case 
                                       when (@I_vPAPay_Code_Hourly is null)
                                            and (@Existing_PACOSTCATID <> '') then @Existing_PAPay_Code_Hourly
                                       when (@I_vPAPay_Code_Hourly is null)
                                            and (@Existing_PACOSTCATID = '') then ''
                                       else @I_vPAPay_Code_Hourly
                                     end,
             @I_vPAPay_Code_Salary = case 
                                       when (@I_vPAPay_Code_Salary is null)
                                            and (@Existing_PACOSTCATID <> '') then @Existing_PAPay_Code_Salary
                                       when (@I_vPAPay_Code_Salary is null)
                                            and (@Existing_PACOSTCATID = '') then ''
                                       else @I_vPAPay_Code_Salary
                                     end,
             @I_vPAPay_Code_Salary = case 
                                       when (@I_vPAPay_Code_Salary is null)
                                            and (@Existing_PACOSTCATID <> '') then @Existing_PAPay_Code_Salary
                                       when (@I_vPAPay_Code_Salary is null)
                                            and (@Existing_PACOSTCATID = '') then ''
                                       else @I_vPAPay_Code_Salary
                                     end,
             @I_vLNITMSEQ = case 
                              when (@I_vLNITMSEQ is null)
                                   and (@Existing_PACOSTCATID <> '') then @Existing_LNITMSEQ
                              when (@I_vLNITMSEQ is null)
                                   and (@Existing_PACOSTCATID = '') then 0
                              else @I_vLNITMSEQ
                            end,
             @I_vPALineItemSeq = case 
                                   when (@I_vPALineItemSeq is null)
                                        and (@Existing_PACOSTCATID <> '') then @Existing_PALineItemSeq
                                   when (@I_vPALineItemSeq is null)
                                        and (@Existing_PACOSTCATID = '') then 0
                                   else @I_vPALineItemSeq
                                 end,
             @I_vPABProfitPcnt = case 
                                   when (@I_vPABProfitPcnt is null)
                                        and (@Existing_PACOSTCATID <> '') then @Existing_PABProfitPcnt
                                   when (@I_vPABProfitPcnt is null)
                                        and (@Existing_PACOSTCATID = '') then 0
                                   else @I_vPABProfitPcnt
                                 end,
             @I_vPABaselineOvhdAmtPerUnit = case 
                                              when (@I_vPABaselineOvhdAmtPerUnit is null)
                                                   and (@Existing_PACOSTCATID <> '') then @Existing_PABaselineOvhdAmtPerUnit
                                              when (@I_vPABaselineOvhdAmtPerUnit is null)
                                                   and (@Existing_PACOSTCATID = '') then 0
                                              else @I_vPABaselineOvhdAmtPerUnit
                                            end,
             @I_vPABaselineOvrhdPct = case 
                                        when (@I_vPABaselineOvrhdPct is null)
                                             and (@Existing_PACOSTCATID <> '') then @Existing_PABaselineOvrhdPct
                                        when (@I_vPABaselineOvrhdPct is null)
                                             and (@Existing_PACOSTCATID = '') then 0
                                        else @I_vPABaselineOvrhdPct
                                      end,
             @I_vPAFQuantity = case 
                                 when (@I_vPAFQuantity is null)
                                      and (@Existing_PACOSTCATID <> '') then @Existing_PAFQuantity
                                 when (@I_vPAFQuantity is null)
                                      and (@Existing_PACOSTCATID = '') then 0
                                 else @I_vPAFQuantity
                               end,
             @I_vPAFUnitCost = case 
                                 when (@I_vPAFUnitCost is null)
                                      and (@Existing_PACOSTCATID <> '') then @Existing_PAFUnitCost
                                 when (@I_vPAFUnitCost is null)
                                      and (@Existing_PACOSTCATID = '') then 0
                                 else @I_vPAFUnitCost
                               end,
             @I_vPAFTotalCost = case 
                                  when (@I_vPAFTotalCost is null)
                                       and (@Existing_PACOSTCATID <> '') then @Existing_PAFTotalCost
                                  when (@I_vPAFTotalCost is null)
                                       and (@Existing_PACOSTCATID = '') then 0
                                  else @I_vPAFTotalCost
                                end,
             @I_vPAFProfitAmt = case 
                                  when (@I_vPAFProfitAmt is null)
                                       and (@Existing_PACOSTCATID <> '') then @Existing_PAFProfitAmt
                                  when (@I_vPAFProfitAmt is null)
                                       and (@Existing_PACOSTCATID = '') then 0
                                  else @I_vPAFProfitAmt
                                end,
             @I_vPAFProfitPcnt = case 
                                   when (@I_vPAFProfitPcnt is null)
                                        and (@Existing_PACOSTCATID <> '') then @Existing_PAFProfitPcnt
                                   when (@I_vPAFProfitPcnt is null)
                                        and (@Existing_PACOSTCATID = '') then 0
                                   else @I_vPAFProfitPcnt
                                 end,
             @I_vPAForecastOvrhdAmtPerUnt = case 
                                              when (@I_vPAForecastOvrhdAmtPerUnt is null)
                                                   and (@Existing_PACOSTCATID <> '') then @Existing_PAForecastOvrhdAmtPerUnt
                                              when (@I_vPAForecastOvrhdAmtPerUnt is null)
                                                   and (@Existing_PACOSTCATID = '') then 0
                                              else @I_vPAForecastOvrhdAmtPerUnt
                                            end,
             @I_vPAForecastOvrhdPct = case 
                                        when (@I_vPAForecastOvrhdPct is null)
                                             and (@Existing_PACOSTCATID <> '') then @Existing_PAForecastOvrhdPct
                                        when (@I_vPAForecastOvrhdPct is null)
                                             and (@Existing_PACOSTCATID = '') then 0
                                        else @I_vPAForecastOvrhdPct
                                      end,
             @I_vPAProfitType = case 
                                  when (@I_vPAProfitType is null)
                                       and (@Existing_PACOSTCATID <> '') then @Existing_PAProfitType
                                  when (@I_vPAProfitType is null)
                                       and (@Existing_PACOSTCATID = '') then 0
                                  else @I_vPAProfitType
                                end,
             @I_vPAbllngtype = case 
                                 when (@I_vPAbllngtype is null)
                                      and (@Existing_PACOSTCATID <> '') then @Existing_PAbllngtype
                                 when (@I_vPAbllngtype is null)
                                      and (@Existing_PACOSTCATID = '') then 0
                                 else @I_vPAbllngtype
                               end,
             @I_vPASTAT = case 
                            when (@I_vPASTAT is null)
                                 and (@Existing_PACOSTCATID <> '') then @Existing_PASTAT
                            when (@I_vPASTAT is null)
                                 and (@Existing_PACOSTCATID = '') then 4
                            else @I_vPASTAT
                          end,
             @I_vPABQuantity = case 
                                 when (@I_vPABQuantity is null)
                                      and (@Existing_PACOSTCATID <> '') then @Existing_PABQuantity
                                 when (@I_vPABQuantity is null)
                                      and (@Existing_PACOSTCATID = '') then 0
                                 else @I_vPABQuantity
                               end,
             @I_vPABUnitCost = case 
                                 when (@I_vPABUnitCost is null)
                                      and (@Existing_PACOSTCATID <> '') then @Existing_PABUnitCost
                                 when (@I_vPABUnitCost is null)
                                      and (@Existing_PACOSTCATID = '') then 0
                                 else @I_vPABUnitCost
                               end,
             @I_vPABTotalCost = case 
                                  when (@I_vPABTotalCost is null)
                                       and (@Existing_PACOSTCATID <> '') then @Existing_PABTotalCost
                                  when (@I_vPABTotalCost is null)
                                       and (@Existing_PACOSTCATID = '') then 0
                                  else @I_vPABTotalCost
                                end,
             @I_vPABProfitAmt = case 
                                  when (@I_vPABProfitAmt is null)
                                       and (@Existing_PACOSTCATID <> '') then @Existing_PABProfitAmt
                                  when (@I_vPABProfitAmt is null)
                                       and (@Existing_PACOSTCATID = '') then 0
                                  else @I_vPABProfitAmt
                                end,
             @I_vPABProfitPcnt = case 
                                   when (@I_vPABProfitPcnt is null)
                                        and (@Existing_PACOSTCATID <> '') then @Existing_PABProfitPcnt
                                   when (@I_vPABProfitPcnt is null)
                                        and (@Existing_PACOSTCATID = '') then 0
                                   else @I_vPABProfitPcnt
                                 end,
             @I_vPABaselineOvhdAmtPerUnit = case 
                                              when (@I_vPABaselineOvhdAmtPerUnit is null)
                                                   and (@Existing_PACOSTCATID <> '') then @Existing_PABaselineOvhdAmtPerUnit
                                              when (@I_vPABaselineOvhdAmtPerUnit is null)
                                                   and (@Existing_PACOSTCATID = '') then 0
                                              else @I_vPABaselineOvhdAmtPerUnit
                                            end,
             @I_vPABaselineOvrhdPct = case 
                                        when (@I_vPABaselineOvrhdPct is null)
                                             and (@Existing_PACOSTCATID <> '') then @Existing_PABaselineOvrhdPct
                                        when (@I_vPABaselineOvrhdPct is null)
                                             and (@Existing_PACOSTCATID = '') then 0
                                        else @I_vPABaselineOvrhdPct
                                      end
      
      if (@I_vLNITMSEQ = 0)
        begin
          select @I_vLNITMSEQ = isnull(max(LNITMSEQ),0) + 1
          from   PA01301 (nolock)
          where  PAPROJNUMBER = @I_vPAPROJNUMBER
        end
      
      if (@I_vPALineItemSeq = 0)
        begin
          select @I_vPALineItemSeq = (@I_vLNITMSEQ * 100)
        end
      
      if (@Existing_PATU <> 0)
        begin
          select @PATU = @Existing_PATU
        end
    end
  else
    begin
      if (@I_vPASTAT is null)
        select @I_vPASTAT = 4
      
      if (@I_vPAProfitType is null)
        select @I_vPAProfitType = 0
      
      if (@I_vPAbllngtype is null)
        select @I_vPAbllngtype = 0
      
      if (@I_vPABBeginDate is null)
        select @I_vPABBeginDate = ''
      
      if (@I_vPABEndDate is null)
        select @I_vPABEndDate = ''
      
      if (@I_vLNITMSEQ is null)
        select @I_vLNITMSEQ = 0
      
      if (@I_vPALineItemSeq is null)
        select @I_vPALineItemSeq = 0
      
      if (@I_vPABQuantity is null)
        select @I_vPABQuantity = 0
      
      if (@I_vPABUnitCost is null)
        select @I_vPABUnitCost = 0
      
      if (@I_vPABProfitAmt is null)
        select @I_vPABProfitAmt = 0
      
      if (@I_vPABProfitPcnt is null)
        select @I_vPABProfitPcnt = 0
      
      if (@I_vPABTotalCost is null)
        select @I_vPABTotalCost = 0
      
      if (@I_vPABaselineOvhdAmtPerUnit is null)
        select @I_vPABaselineOvhdAmtPerUnit = 0
      
      if (@I_vPABaselineOvrhdPct is null)
        select @I_vPABaselineOvrhdPct = 0
      
      if (@I_vPABaselinePTaxOptions is null)
        select @I_vPABaselinePTaxOptions = 0
      
      if (@I_vPABaselineCTaxSchedID is null)
        select @I_vPABaselineCTaxSchedID = ''
      
      if (@I_vPABaselineSTaxOptions is null)
        select @I_vPABaselineSTaxOptions = 0
      
      if (@I_vPABaselineBTaxSchedID is null)
        select @I_vPABaselineBTaxSchedID = ''
      
      if (@I_vPAFQuantity is null)
        select @I_vPAFQuantity = 0
      
      if (@I_vPAFUnitCost is null)
        select @I_vPAFUnitCost = 0
      
      if (@I_vPAFTotalCost is null)
        select @I_vPAFTotalCost = 0
      
      if (@I_vPAFProfitAmt is null)
        select @I_vPAFProfitAmt = 0
      
      if (@I_vPAFProfitPcnt is null)
        select @I_vPAFProfitPcnt = 0
      
      if (@I_vPAForecastOvrhdAmtPerUnt is null)
        select @I_vPAForecastOvrhdAmtPerUnt = 0
      
      if (@I_vPAForecastOvrhdPct is null)
        select @I_vPAForecastOvrhdPct = 0
      
      if (@I_vPAForecastPTaxOptions is null)
        select @I_vPAForecastPTaxOptions = 0
      
      if (@I_vPAForecastCTaxSchedID is null)
        select @I_vPAForecastCTaxSchedID = ''
      
      if (@I_vPAForecastSTaxOptions is null)
        select @I_vPAForecastSTaxOptions = 0
      
      if (@I_vPAForecastBTaxSchedID is null)
        select @I_vPAForecastBTaxSchedID = ''
      
      if (@I_vPAFBeginDate is null)
        select @I_vPAFBeginDate = ''
      
      if (@I_vPAFEndDate is null)
        select @I_vPAFEndDate = ''
      
      if (@I_vPAUnit_of_Measure is null)
        select @I_vPAUnit_of_Measure = ''
      
      if (@I_vUOMSCHDL is null)
        select @I_vUOMSCHDL = ''
      
      if (@I_vPAPay_Code_Hourly is null)
        select @I_vPAPay_Code_Hourly = ''
      
      if (@I_vPAPay_Code_Salary is null)
        select @I_vPAPay_Code_Salary = ''
      
      if (@I_vUpdateExisting is null)
        select @I_vUpdateExisting = 0
      
      if (@I_vRollUpEst_to_Forecast is null)
        select @I_vRollUpEst_to_Forecast = 0
      
      if (@I_vLNITMSEQ = 0)
        begin
          select @I_vLNITMSEQ = isnull(max(LNITMSEQ),0) + 1
          from   PA01301 (nolock)
          where  PAPROJNUMBER = @I_vPAPROJNUMBER
        end
      
      if (@I_vPALineItemSeq = 0)
        begin
          select @I_vPALineItemSeq = (@I_vLNITMSEQ * 100)
        end
      
      if exists (select 1
                 from   PA01301 (nolock)
                 where  PAPROJNUMBER = @I_vPAPROJNUMBER
                        and PACOSTCATID = @I_vPACOSTCATID)
        begin
          select @O_iErrorState = 5178
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if exists (select 1
                 from   PA01301 (nolock)
                 where  PAPROJNUMBER = @I_vPAPROJNUMBER
                        and PACOSTCATID = @I_vPACOSTCATID
                        and LNITMSEQ = @I_vLNITMSEQ)
        begin
          select @O_iErrorState = 5186
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @PAPROJNUMBER = PAPROJNUMBER,
         @PABBeginDate = PABBeginDate,
         @PABEndDate = PABEndDate,
         @PAProjectType = PAProjectType,
         @PACONTNUMBER = PACONTNUMBER,
         @CUSTNMBR = CUSTNMBR,
         @PAbllngtype = PAbllngtype,
         @PASTAT = PASTAT
  from   PA01201 (nolock)
  where  PAPROJNUMBER = @I_vPAPROJNUMBER
  
  if (@PAPROJNUMBER = '')
    begin
      select @O_iErrorState = 5176
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPABBeginDate = '')
    select @I_vPABBeginDate = @PABBeginDate
  
  if (@I_vPABBeginDate = '')
      or (@I_vPABBeginDate is null)
    select @I_vPABBeginDate = @userdate
  
  if (@I_vPABEndDate = '')
    select @I_vPABEndDate = @PABEndDate
  
  if (@I_vPABEndDate = '')
      or (@I_vPABEndDate is null)
    select @I_vPABEndDate = @userdate
  
  if (@I_vPABBeginDate < @PABBeginDate)
    select @PABBeginDate = @I_vPABBeginDate
  
  if (@I_vPABEndDate < @PABEndDate)
    select @PABEndDate = @I_vPABEndDate
  
  select @OPENYEAR = YEAR1
  from   SY40101 a (nolock)
  where  @I_vPABBeginDate BETWEEN a.FSTFSCDY and a.LSTFSCDY
  
  if (@I_vPABBeginDate <> '')
    begin
      if (not exists (select 1
                      from   SY40101 (nolock)
                      where  @I_vPABBeginDate BETWEEN FSTFSCDY and LSTFSCDY))
        begin
          select @O_iErrorState = 9348
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@PASTAT = 5)
    begin
      select @O_iErrorState = 9364
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if not exists (select 1
                 from   PA01001 (nolock)
                 where  PACOSTCATID = @I_vPACOSTCATID)
    begin
      select @O_iErrorState = 5177
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    begin
      if (@Existing_PACOSTCATID = '')
        begin
          select @PACOSTCATID = PACOSTCATID,
                 @PACOSTCATCLASID = PACOSTCATCLASID,
                 @PAinactive = PAinactive,
                 @PATU = PATU,
                 @PAIV_Item_Checkbox = PAIV_Item_Checkbox,
                 @PAUnit_of_Measure = PAUnit_of_Measure,
                 @UOMSCHDL = UOMSCHDL,
                 @DECPLQTY = DECPLQTY,
                 @PAUNITCOST = PAUNITCOST,
                 @PAPay_Code_Hourly = PAPay_Code_Hourly,
                 @PAPay_Code_Salary = PAPay_Code_Salary,
                 @PATMProfitType = PATMProfitType,
                 @PATMProfitAmount = PATMProfitAmount,
                 @PATMProfitPercent = PATMProfitPercent,
                 @PAProfit_Type__CP = PAProfit_Type__CP,
                 @PAProfitAmountCP = PAProfitAmountCP,
                 @PAProfitPercentCP = PAProfitPercentCP,
                 @PAFFProfitType = PAFFProfitType,
                 @PAFFProfitAmount = PAFFProfitAmount,
                 @PAFFProfitPercent = PAFFProfitPercent,
                 @DECPLCUR = DECPLCUR,
                 @PAcostaxscheduleid = PAcostaxscheduleid,
                 @PAPurchase_Tax_Options = PAPurchase_Tax_Options,
                 @PAbilltaxscheduleid = PAbilltaxscheduleid,
                 @PASales_Tax_Options = PASales_Tax_Options,
                 @PAOverheadRateMethod = PAOverheadRateMethod,
                 @PAOvhdAmtPerUnit = PAOvhdAmtPerUnit,
                 @PAOverheaPercentage = PAOverheaPercentage
          from   PA01001 (nolock)
          where  PACOSTCATID = @I_vPACOSTCATID
          
          if (@I_vPABaselineOvhdAmtPerUnit = 0)
            select @I_vPABaselineOvhdAmtPerUnit = @PAOvhdAmtPerUnit
          
          if (@I_vPABaselineOvrhdPct = 0)
            select @I_vPABaselineOvrhdPct = @PAOverheaPercentage
          
          if (@I_vPABUnitCost = 0)
            select @I_vPABUnitCost = @PAUNITCOST
          
          if (@I_vPABaselineCTaxSchedID = '')
             and (@I_vPABaselinePTaxOptions = 0)
            select @I_vPABaselineCTaxSchedID = @PAcostaxscheduleid
          
          if (@I_vPABaselinePTaxOptions = 0)
            select @I_vPABaselinePTaxOptions = @PAPurchase_Tax_Options
          
          if (@I_vPABaselineBTaxSchedID = '')
             and (@I_vPABaselineSTaxOptions = 0)
            select @I_vPABaselineBTaxSchedID = @PAbilltaxscheduleid
          
          if (@I_vPABaselineSTaxOptions = 0)
            select @I_vPABaselineSTaxOptions = @PASales_Tax_Options
          
          if (@PAProjectType = 1)
            begin
              if (@I_vPAProfitType = 0)
                select @I_vPAProfitType = @PATMProfitType
              
              if (@PATMProfitAmount > 0)
                select @PABProfitAmt = @PATMProfitAmount
              
              if (@PATMProfitPercent > 0)
                select @PABProfitPcnt = @PATMProfitPercent
            end
          else
            if (@PAProjectType = 2)
              begin
                if (@I_vPAProfitType = 0)
                  select @I_vPAProfitType = @PAProfit_Type__CP
                
                if (@PAProfitAmountCP > 0)
                  select @PABProfitAmt = @PAProfitAmountCP
                
                if (@PAProfitPercentCP > 0)
                  select @PABProfitPcnt = @PAProfitPercentCP
              end
            else
              if (@PAProjectType = 3)
                begin
                  if (@I_vPAProfitType = 0)
                    select @I_vPAProfitType = @PAFFProfitType
                  
                  if (@PAFFProfitAmount > 0)
                    select @PABProfitAmt = @PAFFProfitAmount
                  
                  if (@PAFFProfitPercent > 0)
                    select @PABProfitPcnt = @PAFFProfitPercent
                end
          
          if (@I_vPAbllngtype = 0)
            begin
              select @I_vPAbllngtype = @PAbllngtype
            end
        end
      else
        begin
          select @DECPLCUR = DECPLCUR
          from   PA01001 (nolock)
          where  PACOSTCATID = @I_vPACOSTCATID
          
          if (@I_vPASTAT <> 4)
            begin
              select @I_vPAProfitType = @Existing_PAProfitType,
                     @I_vPAbllngtype = @Existing_PAbllngtype
            end
          else
            begin
              if (@I_vPAProfitType = 0)
                select @I_vPAProfitType = @Existing_PAProfitType
              
              if (@I_vPAbllngtype = 0)
                select @I_vPAbllngtype = @Existing_PAbllngtype
            end
        end
    end
  
  if (@PATU = 4)
      OR (@PATU = 5)
    begin
      select @I_vPABaselineOvhdAmtPerUnit = 0,
             @I_vPABaselineOvrhdPct = 0,
             @I_vPAForecastOvrhdAmtPerUnt = 0,
             @I_vPAForecastOvrhdPct = 0
    end
  
  if (@I_vPASTAT < 1)
      or (@I_vPASTAT > 5)
    begin
      select @O_iErrorState = 6400
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPASTAT <> 4)
     and (@Existing_PAPROJNUMBER = '')
    begin
      select @O_iErrorState = 6401
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vPASTAT = 3
    begin
      select @O_iErrorState = 6402
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@PAinactive = 1)
    begin
      select @O_iErrorState = 5179
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAProfitType < 1)
     and (@I_vPAProfitType > 9)
    begin
      select @O_iErrorState = 5180
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@PAProjectType = 1
       and @I_vPAProfitType > 2
       and @I_vPAProfitType <> 8
       and @I_vPAProfitType <> 9)
       or (@PAProjectType = 2
           and @I_vPAProfitType < 3)
       or (@PAProjectType = 3
           and (@I_vPAProfitType = 1
                 or @I_vPAProfitType = 2
                 or @I_vPAProfitType = 4
                 or @I_vPAProfitType = 7)))
    begin
      select @O_iErrorState = 5181
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @PAIV_Item_Checkbox = 1
     and @I_vPAProfitType <> 9
    begin
      select @O_iErrorState = 5182
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @PAIV_Item_Checkbox = 0
     and @I_vPAProfitType = 9
    begin
      select @O_iErrorState = 5183
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAbllngtype < 1)
      or (@I_vPAbllngtype > 3)
    begin
      select @O_iErrorState = 5184
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAbllngtype > 1)
     and (@PAProjectType > 1)
    begin
      select @O_iErrorState = 5185
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 (nolock)
  where  INTERID = db_name()
  
  exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
    @I_sCompanyID = @sCompanyID ,
    @I_iSQLSessionID = 0 ,
    @O_mNoteIndex = @PAbillnoteidx output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iGetNextNoteIdxErrState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + @iGetNextNoteIdxErrState
      
      select @O_iErrorState = 9347
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPASTAT = 4)
    begin
      if (@I_vPABUnitCost > 0)
        begin
          select @I_vPABTotalCost = 0
        end
      else
        if (@I_vPABTotalCost > 0)
          begin
            select @I_vPABUnitCost = round((@I_vPABTotalCost / @I_vPABQuantity),@DECPLCUR)
          end
      
      if (@I_vPABaselineOvhdAmtPerUnit > 0)
        select @PABaselineOvhdCost = round(@I_vPABQuantity * @I_vPABaselineOvhdAmtPerUnit,
                                           @DECPLCUR)
      
      if (@I_vPABaselineOvrhdPct > 0)
        select @PABaselineOvhdCost = @PABaselineOvhdCost + round(((@I_vPABQuantity * @I_vPABUnitCost) * (@I_vPABaselineOvrhdPct / 100)),
                                                                 @DECPLCUR)
      
      select @I_vPABTotalCost = round(@I_vPABQuantity * @I_vPABUnitCost,@DECPLCUR) + @PABaselineOvhdCost
      
      if (@I_vPABProfitAmt = 0)
          or (@I_vPABProfitAmt is null)
        select @I_vPABProfitAmt = @PABProfitAmt
      
      if (@I_vPABProfitPcnt = 0)
          or (@I_vPABProfitPcnt is null)
        select @I_vPABProfitPcnt = @PABProfitPcnt
      
      if (@I_vPAProfitType = 1)
        select @PABBillings = round(@I_vPABQuantity * @I_vPABProfitAmt,@DECPLCUR),
               @I_vPABProfitPcnt = 0
      
      if (@I_vPAProfitType = 3)
        select @PABBillings = round(@I_vPABQuantity * (@I_vPABUnitCost + @I_vPABProfitAmt) + @PABaselineOvhdCost,
                                    @DECPLCUR),
               @I_vPABProfitPcnt = 0
      
      if (@I_vPAProfitType = 4)
        select @PABBillings = round(@I_vPABQuantity * (@I_vPABUnitCost + @I_vPABProfitAmt) + @PABaselineOvhdCost,
                                    @DECPLCUR),
               @I_vPABProfitPcnt = 0
      
      if (@I_vPAProfitType = 5)
        select @PABBillings = round((@I_vPABQuantity * @I_vPABUnitCost) + @I_vPABProfitAmt + @PABaselineOvhdCost,
                                    @DECPLCUR),
               @I_vPABProfitPcnt = 0
      
      if (@I_vPAProfitType = 8)
        select @PABBillings = 0,
               @I_vPABProfitPcnt = 0,
               @I_vPABProfitAmt = 0
      
      if (@I_vPAProfitType = 2)
        select @PABBillings = round(@I_vPABQuantity * (1 + @I_vPABProfitPcnt / 100) * @I_vPABUnitCost,
                                    @DECPLCUR),
               @I_vPABProfitAmt = 0
      
      if (@I_vPAProfitType = 6)
        select @PABBillings = round((@I_vPABQuantity * (1 + @I_vPABProfitPcnt / 100) * @I_vPABUnitCost) + @PABaselineOvhdCost,
                                    @DECPLCUR),
               @I_vPABProfitAmt = 0
      
      if (@I_vPAProfitType = 7)
        select @PABBillings = round((@I_vPABQuantity * (1 + @I_vPABProfitPcnt / 100) * @I_vPABUnitCost) + @PABaselineOvhdCost,
                                    @DECPLCUR),
               @I_vPABProfitAmt = 0
      
      if (@I_vPAProfitType = 9)
        select @PABBillings = round(@I_vPABProfitAmt * @I_vPABQuantity,@DECPLCUR),
               @I_vPABProfitPcnt = 0
      
      select @PABProfit = @PABBillings - @I_vPABTotalCost
      
      if (@I_vPABBeginDate = '')
         and (@I_vPABEndDate <> '')
        begin
          select @O_iErrorState = 5187
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vPABBeginDate <> '')
         and (@I_vPABEndDate = '')
        begin
          select @O_iErrorState = 5188
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vPABaselinePTaxOptions < 1)
          or (@I_vPABaselinePTaxOptions > 3)
        begin
          select @O_iErrorState = 5189
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vPABaselinePTaxOptions <> 1)
        begin
          if (@I_vPABaselineCTaxSchedID <> '')
            begin
              select @O_iErrorState = 5190
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          if (@I_vPABaselineCTaxSchedID <> '')
            begin
              if not exists (select 1
                             from   TX00101 (nolock)
                             where  TAXSCHID = @I_vPABaselineCTaxSchedID)
                begin
                  select @O_iErrorState = 5191
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
        end
      
      if (@I_vPABaselineSTaxOptions < 1)
          or (@I_vPABaselineSTaxOptions > 3)
        begin
          select @O_iErrorState = 5192
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vPABaselineSTaxOptions <> 1)
        begin
          if (@I_vPABaselineBTaxSchedID <> '')
            begin
              select @O_iErrorState = 5193
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          if (@I_vPABaselineBTaxSchedID <> '')
            begin
              if not exists (select 1
                             from   TX00101 (nolock)
                             where  TAXSCHID = @I_vPABaselineBTaxSchedID)
                begin
                  select @O_iErrorState = 5194
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
        end
      
      if (@I_vUOMSCHDL <> '')
        begin
          if not exists (select 1
                         from   PA40101 (nolock)
                         where  UOMSCHDL = @I_vUOMSCHDL)
            begin
              select @O_iErrorState = 5195
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      if (@I_vPAPay_Code_Salary <> '')
        begin
          if not exists (select 1
                         from   UPR40600 (nolock)
                         where  PAYRCORD = @I_vPAPay_Code_Salary)
            begin
              select @O_iErrorState = 5196
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      if (@I_vPAPay_Code_Hourly <> '')
        begin
          if not exists (select 1
                         from   UPR40600 (nolock)
                         where  PAYRCORD = @I_vPAPay_Code_Hourly)
            begin
              select @O_iErrorState = 5197
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      if (@PATU <> 1
          and ((@I_vPAPay_Code_Hourly <> '')
                or (@I_vPAPay_Code_Salary <> '')))
        begin
          select @O_iErrorState = 5198
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      select @PABase_Qty = @I_vPABQuantity,
             @PABase_Unit_Cost = @I_vPABUnitCost,
             @PAProject_Amount = @PABBillings,
             @PABaseProfitAmount = @I_vPABProfitAmt,
             @PABaseOvhdCost = @I_vPABaselineOvhdAmtPerUnit
      
      if (@I_vRollUpEst_to_Forecast = 1)
          or (@Existing_PACOSTCATID = '')
          or (@Existing_PAPreviouslyOpen = 0)
        begin
          select @PAForecastBaseQty = @PABase_Qty,
                 @PAForecastBaseOvrhdAmt = @PABaseOvhdCost,
                 @PAForecastBaseUnitCost = @PABase_Unit_Cost,
                 @PAForecastBaseProfitAmt = @PABaseProfitAmount,
                 @I_vPAFBeginDate = @I_vPABBeginDate,
                 @I_vPAFEndDate = @I_vPABEndDate,
                 @I_vPAFQuantity = @I_vPABQuantity,
                 @I_vPAFUnitCost = @I_vPABUnitCost,
                 @I_vPAFTotalCost = @I_vPABTotalCost,
                 @PAFBillings = @PABBillings,
                 @PAFProfit = @PABProfit,
                 @I_vPAFProfitAmt = @I_vPABProfitAmt,
                 @I_vPAFProfitPcnt = @I_vPABProfitPcnt,
                 @PAForecastOvhdCost = @PABaselineOvhdCost,
                 @I_vPAForecastOvrhdAmtPerUnt = @I_vPABaselineOvhdAmtPerUnit,
                 @I_vPAForecastOvrhdPct = @I_vPABaselineOvrhdPct,
                 @I_vPAForecastPTaxOptions = @I_vPABaselinePTaxOptions,
                 @I_vPAForecastCTaxSchedID = @I_vPABaselineCTaxSchedID,
                 @I_vPAForecastSTaxOptions = @I_vPABaselineSTaxOptions,
                 @I_vPAForecastBTaxSchedID = @I_vPABaselineBTaxSchedID
        end
    end
  
  if (@I_vPASTAT = 1)
      or (@I_vPASTAT = 2)
    begin
      if (@I_vPAFUnitCost > 0)
        begin
          select @I_vPAFTotalCost = 0
        end
      else
        if (@I_vPAFTotalCost > 0)
          begin
            select @I_vPAFUnitCost = round((@I_vPAFTotalCost / @I_vPAFQuantity),@DECPLCUR)
          end
      
      if (@I_vPAForecastOvrhdAmtPerUnt > 0)
        select @PAForecastOvhdCost = round(@I_vPAFQuantity * @I_vPAForecastOvrhdAmtPerUnt,
                                           @DECPLCUR)
      
      if (@I_vPAForecastOvrhdPct > 0)
        select @PAForecastOvhdCost = @PAForecastOvhdCost + round(((@I_vPAFQuantity * @I_vPAFUnitCost) * (@I_vPAForecastOvrhdPct / 100)),
                                                                 @DECPLCUR)
      
      select @I_vPAFTotalCost = round(@I_vPAFQuantity * @I_vPAFUnitCost,@DECPLCUR) + @PABaselineOvhdCost
      
      if (@I_vPAFProfitAmt = 0)
        select @I_vPAFProfitAmt = @PAFProfitAmt
      
      if (@I_vPAFProfitPcnt = 0)
        select @I_vPABProfitPcnt = @I_vPAFProfitPcnt
      
      if (@I_vPAProfitType = 1)
        select @PAFBillings = round(@I_vPAFQuantity * @I_vPAFProfitAmt,@DECPLCUR),
               @I_vPAFProfitPcnt = 0
      
      if (@I_vPAProfitType = 3)
        select @PAFBillings = round(@I_vPAFQuantity * (@I_vPAFUnitCost + @I_vPAFProfitAmt) + @PABaselineOvhdCost,
                                    @DECPLCUR),
               @I_vPAFProfitPcnt = 0
      
      if (@I_vPAProfitType = 4)
        select @PAFBillings = round(@I_vPAFQuantity * (@I_vPAFUnitCost + @I_vPAFProfitAmt) + @PABaselineOvhdCost,
                                    @DECPLCUR),
               @I_vPAFProfitPcnt = 0
      
      if (@I_vPAProfitType = 5)
        select @PAFBillings = round((@I_vPAFQuantity * @I_vPAFUnitCost) + @I_vPAFProfitAmt + @PABaselineOvhdCost,
                                    @DECPLCUR),
               @I_vPAFProfitPcnt = 0
      
      if (@I_vPAProfitType = 8)
        select @PAFBillings = 0,
               @I_vPABProfitPcnt = 0,
               @I_vPABProfitAmt = 0
      
      if (@I_vPAProfitType = 2)
        select @PAFBillings = round(@I_vPAFQuantity * (1 + @I_vPAFProfitPcnt / 100) * @I_vPAFUnitCost,
                                    @DECPLCUR),
               @I_vPAFProfitAmt = 0
      
      if (@I_vPAProfitType = 6)
        select @PAFBillings = round((@I_vPAFQuantity * (1 + @I_vPAFProfitPcnt / 100) * @I_vPAFUnitCost) + @PABaselineOvhdCost,
                                    @DECPLCUR),
               @I_vPAFProfitAmt = 0
      
      if (@I_vPAProfitType = 7)
        select @PAFBillings = round((@I_vPAFQuantity * (1 + @I_vPAFProfitPcnt / 100) * @I_vPAFUnitCost) + @PABaselineOvhdCost,
                                    @DECPLCUR),
               @I_vPAFProfitAmt = 0
      
      if (@I_vPAProfitType = 9)
        select @PAFBillings = round(@I_vPAFProfitAmt * @I_vPAFQuantity,@DECPLCUR),
               @I_vPAFProfitPcnt = 0
      
      select @PAFProfit = @PAFBillings - @I_vPAFTotalCost,
             @PAProject_Amount = @PAFBillings,
             @PAForecastBaseQty = @I_vPAFQuantity,
             @PAForecastBaseOvrhdAmt = @I_vPAForecastOvrhdAmtPerUnt,
             @PAForecastBaseUnitCost = @I_vPAFUnitCost,
             @PAForecastBaseProfitAmt = @I_vPAFProfitAmt
      
      if (@I_vPAFBeginDate = '')
         and (@I_vPAFEndDate <> '')
        begin
          select @O_iErrorState = 6403
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vPAFBeginDate <> '')
         and (@I_vPAFEndDate = '')
        begin
          select @O_iErrorState = 6404
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vPAForecastPTaxOptions < 1)
          or (@I_vPAForecastPTaxOptions > 3)
        begin
          select @O_iErrorState = 6405
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vPAForecastPTaxOptions <> 1)
        begin
          if (@I_vPAForecastCTaxSchedID <> '')
            begin
              select @O_iErrorState = 6406
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          if (@I_vPAForecastCTaxSchedID <> '')
            begin
              if not exists (select 1
                             from   TX00101 (nolock)
                             where  TAXSCHID = @I_vPAForecastCTaxSchedID)
                begin
                  select @O_iErrorState = 6407
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
        end
      
      if (@I_vPAForecastSTaxOptions < 1)
          or (@I_vPAForecastSTaxOptions > 3)
        begin
          select @O_iErrorState = 6408
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vPAForecastSTaxOptions <> 1)
        begin
          if (@I_vPAForecastBTaxSchedID <> '')
            begin
              select @O_iErrorState = 6409
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          if (@I_vPAForecastBTaxSchedID <> '')
            begin
              if not exists (select 1
                             from   TX00101 (nolock)
                             where  TAXSCHID = @I_vPAForecastBTaxSchedID)
                begin
                  select @O_iErrorState = 6410
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
            end
        end
    end
  
  if (@Existing_PACOSTCATID = '')
    begin
      if (@I_vPABaselinePTaxOptions = 0)
        select @I_vPABaselinePTaxOptions = @PAPurchase_Tax_Options
      
      if (@I_vPABaselineCTaxSchedID = '')
        select @I_vPABaselineCTaxSchedID = @PAcostaxscheduleid
      
      if (@I_vPABaselineSTaxOptions = 0)
        select @I_vPABaselineSTaxOptions = @PASales_Tax_Options
      
      if (@I_vPABaselineBTaxSchedID = '')
        select @I_vPABaselineBTaxSchedID = @PAbilltaxscheduleid
      
      if (@I_vPAUnit_of_Measure = '')
        select @I_vPAUnit_of_Measure = @PAUnit_of_Measure
      
      if (@I_vUOMSCHDL = '')
        select @I_vUOMSCHDL = @UOMSCHDL
      
      if (@I_vPAPay_Code_Hourly = '')
        select @I_vPAPay_Code_Hourly = @PAPay_Code_Hourly
      
      if (@I_vPAPay_Code_Salary = '')
        select @I_vPAPay_Code_Salary = @PAPay_Code_Salary
      
      select @PAUBeginDate = @I_vPABBeginDate,
             @PAUEndDate = @I_vPABEndDate
      
      select @l_tmp_cost_trx_id = case 
                                    when @PATU = 1 then 'TS'
                                    when @PATU = 2 then 'EL'
                                    when @PATU = 3 then 'ML'
                                    when @PATU = 4 then 'VI'
                                    when @PATU = 5 then 'EE'
                                  end
      
      if exists (select 1
                 from   PA41701A (nolock)
                 where  PAcosttrxid = @l_tmp_cost_trx_id)
        begin
          select @TM_WIP_SRC = TM_WIP_SRC,
                 @TM_COGS_SRC = TM_COGS_SRC,
                 @TM_Cost_Contra_SRC = TM_Cost_Contra_SRC,
                 @TM_Unbilled_AR_SRC = TM_Unbilled_AR_SRC,
                 @TM_Unbilled_Rev_SRC = TM_Unbilled_Rev_SRC,
                 @TM_AR_SRC = TM_AR_SRC,
                 @TM_Prj_Rev_SRC = TM_Prj_Rev_SRC,
                 @TM_OVHD_SRC = TM_OVHD_SRC,
                 @FF_WIP_SRC = FF_WIP_SRC,
                 @FF_Cost_Contra_SRC = FF_Cost_Contra_SRC,
                 @FF_AR_SRC = FF_AR_SRC,
                 @FF_Prj_Billings_SRC = FF_Prj_Billings_SRC,
                 @FF_Prj_Expense_SRC = FF_Prj_Expense_SRC,
                 @FF_Prj_Revenue_SRC = FF_Prj_Revenue_SRC,
                 @FF_Prj_Loss_SRC = FF_Prj_Loss_SRC,
                 @FF_BIEE_SRC = FF_BIEE_SRC,
                 @FF_EIEB_SRC = FF_EIEB_SRC,
                 @FF_OVHD_SRC = FF_OVHD_SRC
          from   PA41701A (nolock)
          where  PAcosttrxid = @l_tmp_cost_trx_id
          
          if (@PAIV_Item_Checkbox = 1)
            begin
              select @TM_Cost_Contra_SRC = 8,
                     @FF_Cost_Contra_SRC = 8
            end
          
          if (@PAIV_Item_Checkbox = 1)
             and (@TM_COGS_SRC = 1)
            begin
              select @TM_COGS_SRC = 5
            end
          
          if (@PAIV_Item_Checkbox = 1)
             and (@FF_WIP_SRC = 1)
            begin
              select @FF_WIP_SRC = 5
            end
        end
      else
        begin
          select @TM_WIP_SRC = 5,
                 @TM_COGS_SRC = 5,
                 @TM_Cost_Contra_SRC = case 
                                         when @PAIV_Item_Checkbox = 1 then 8
                                         else 5
                                       end,
                 @TM_Unbilled_AR_SRC = 5,
                 @TM_Unbilled_Rev_SRC = 5,
                 @TM_AR_SRC = 5,
                 @TM_Prj_Rev_SRC = 5,
                 @TM_OVHD_SRC = 5,
                 @FF_WIP_SRC = case 
                                 when @PAIV_Item_Checkbox = 1 then 8
                                 else 5
                               end,
                 @FF_Cost_Contra_SRC = 5,
                 @FF_AR_SRC = 5,
                 @FF_Prj_Billings_SRC = 5,
                 @FF_Prj_Expense_SRC = 5,
                 @FF_Prj_Revenue_SRC = 5,
                 @FF_Prj_Loss_SRC = 5,
                 @FF_BIEE_SRC = 5,
                 @FF_EIEB_SRC = 5,
                 @FF_OVHD_SRC = 5
        end
    end
  
  execute @iStatus = glCalculateGLPeriod
    7 ,
    'Timesheet Entry' ,
    @I_vPABBeginDate ,
    @I_vPABBeginDate ,
    @PABBeginDate_Period output ,
    @fClosed output ,
    @PABBeginDate_YEAR output ,
    @nErr output ,
    @O_glCalculateGLPeriodErrorState1 output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@O_glCalculateGLPeriodErrorState1 <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@O_glCalculateGLPeriodErrorState1))
      
      select @O_iErrorState = 6537
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  execute @iStatus = glCalculateGLPeriod
    7 ,
    'Timesheet Entry' ,
    @I_vPABEndDate ,
    @I_vPABEndDate ,
    @PABEndDate_Period output ,
    @fClosed output ,
    @PABEndDate_YEAR output ,
    @nErr output ,
    @O_glCalculateGLPeriodErrorState2 output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@O_glCalculateGLPeriodErrorState2 <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@O_glCalculateGLPeriodErrorState2))
      
      select @O_iErrorState = 6530
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  execute @iStatus = glCalculateGLPeriod
    7 ,
    'Timesheet Entry' ,
    @I_vPAFBeginDate ,
    @I_vPAFBeginDate ,
    @PAFBeginDate_Period output ,
    @fClosed output ,
    @PAFBeginDate_YEAR output ,
    @nErr output ,
    @O_glCalculateGLPeriodErrorState3 output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@O_glCalculateGLPeriodErrorState3 <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@O_glCalculateGLPeriodErrorState3))
      
      select @O_iErrorState = 6531
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  execute @iStatus = glCalculateGLPeriod
    7 ,
    'Timesheet Entry' ,
    @I_vPAFEndDate ,
    @I_vPAFEndDate ,
    @PAFEndDate_Period output ,
    @fClosed output ,
    @PAFEndDate_YEAR output ,
    @nErr output ,
    @O_glCalculateGLPeriodErrorState4 output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@O_glCalculateGLPeriodErrorState4 <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@O_glCalculateGLPeriodErrorState4))
      
      select @O_iErrorState = 6536
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @i_startyearupdate = DATEPART(year,@I_vPABBeginDate),
         @i_endyearupdate = DATEPART(year,@I_vPABEndDate),
         @i_baselinestartyearupdate = DATEPART(year,@I_vPABEndDate),
         @i_forecaststartyearupdate = DATEPART(year,@I_vPAFEndDate)
  
  select @NUMOFPER = NUMOFPER
  from   SY40101 (nolock)
  where  YEAR1 = @PABEndDate_YEAR
  
  if (@PABEndDate_YEAR - @PABBeginDate_YEAR) = 1
    begin
      set @l_base_ctr = (@NUMOFPER - @PABBeginDate_Period) + (@PABEndDate_Period) + 1
    end
  else
    if (@PABEndDate_YEAR = @PABBeginDate_YEAR)
      begin
        set @l_base_ctr = (@PABEndDate_Period - @PABBeginDate_Period) + 1
      end
    else
      begin
        set @l_base_ctr = ((@PABEndDate_YEAR - @PABBeginDate_YEAR) * @NUMOFPER) + (@NUMOFPER - @PABBeginDate_Period) + (@NUMOFPER - @PABBeginDate_Period)
      end
  
  if (@PABEndDate_YEAR <> @PABBeginDate_YEAR)
    begin
      set @l_fore_ctr = (@NUMOFPER - @PAFBeginDate_Period) + (@PAFEndDate_Period) + 1
    end
  else
    if (@PABEndDate_YEAR = @PABBeginDate_YEAR)
      begin
        set @l_fore_ctr = (@PAFEndDate_Period - @PAFBeginDate_Period) + 1
      end
    else
      begin
        set @l_fore_ctr = ((@PAFEndDate_YEAR - @PAFBeginDate_YEAR - 1) * @NUMOFPER) + (@NUMOFPER - @PAFBeginDate_Period) + (@NUMOFPER - @PABBeginDate_Period)
      end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Projects' ,
        @I_vINDEX1 = @I_vPAPROJNUMBER ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 0 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 6131
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  update PA01201
  set    PABQuantity = PABQuantity + (@I_vPABQuantity - @Existing_PABQuantity),
         PABTotalCost = PABTotalCost + (@I_vPABTotalCost - @Existing_PABTotalCost),
         PABProfit = PABProfit + (@PABProfit - @Existing_PABProfit),
         PABBillings = PABBillings + (@PABBillings - @Existing_PABBillings),
         PAFQuantity = PAFQuantity + (@I_vPAFQuantity - @Existing_PAFQuantity),
         PAFTotalCost = PAFTotalCost + (@I_vPAFTotalCost - @Existing_PAFTotalCost),
         PAFProfit = PAFProfit + (@PAFProfit - @Existing_PAFProfit),
         PAFBillings = PAFBillings + (@PAFBillings - @Existing_PAFBillings),
         PAProject_Amount = PAProject_Amount + (@PAProject_Amount - @Existing_PABBillings)
  where  PAPROJNUMBER = @I_vPAPROJNUMBER
  
  if @@error <> 0
    begin
      select @O_iErrorState = 5450
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  update PA01101
  set    PABQuantity = PABQuantity + (@I_vPABQuantity - @Existing_PABQuantity),
         PABTotalCost = PABTotalCost + (@I_vPABTotalCost - @Existing_PABTotalCost),
         PABProfit = PABProfit + (@PABProfit - @Existing_PABProfit),
         PABBillings = PABBillings + (@PABBillings - @Existing_PABBillings),
         PAFQuantity = PAFQuantity + (@I_vPAFQuantity - @Existing_PAFQuantity),
         PAFTotalCost = PAFTotalCost + (@I_vPAFTotalCost - @Existing_PAFTotalCost),
         PAFProfit = PAFProfit + (@PAFProfit - @Existing_PAFProfit),
         PAFBillings = PAFBillings + (@PAFBillings - @Existing_PAFBillings),
         PAProject_Amount = PAProject_Amount + (@PAProject_Amount - @Existing_PABBillings)
  where  PACONTNUMBER = @PACONTNUMBER
  
  if @@error <> 0
    begin
      select @O_iErrorState = 5451
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  update PA00501
  set    PABQuantity = PABQuantity + (@I_vPABQuantity - @Existing_PABQuantity),
         PABTotalCost = PABTotalCost + (@I_vPABTotalCost - @Existing_PABTotalCost),
         PABProfit = PABProfit + (@PABProfit - @Existing_PABProfit),
         PABBillings = PABBillings + (@PABBillings - @Existing_PABBillings),
         PAFQuantity = PAFQuantity + (@I_vPAFQuantity - @Existing_PAFQuantity),
         PAFTotalCost = PAFTotalCost + (@I_vPAFTotalCost - @Existing_PAFTotalCost),
         PAFProfit = PAFProfit + (@PAFProfit - @Existing_PAFProfit),
         PAFBillings = PAFBillings + (@PAFBillings - @Existing_PAFBillings),
         PAProject_Amount = PAProject_Amount + (@PAProject_Amount - @Existing_PABBillings)
  where  CUSTNMBR = @CUSTNMBR
  
  if @@error <> 0
    begin
      select @O_iErrorState = 5452
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  update PA01221
  set    PABQuantity = PABQuantity + (@I_vPABQuantity - @Existing_PABQuantity),
         PABTotalCost = PABTotalCost + (@I_vPABTotalCost - @Existing_PABTotalCost),
         PABProfit = PABProfit + (@PABProfit - @Existing_PABProfit),
         PABBillings = PABBillings + (@PABBillings - @Existing_PABBillings),
         PABaselineOvhdCost = PABaselineOvhdCost + (@PABaselineOvhdCost - @Existing_PABaselineOvhdCost),
         PAFQuantity = PAFQuantity + (@I_vPAFQuantity - @Existing_PAFQuantity),
         PAFTotalCost = PAFTotalCost + (@I_vPAFTotalCost - @Existing_PAFTotalCost),
         PAFProfit = PAFProfit + (@PAFProfit - @Existing_PAFProfit),
         PAFBillings = PAFBillings + (@PAFBillings - @Existing_PAFBillings),
         PAForecastOvhdCost = PAForecastOvhdCost + (@PAForecastOvhdCost - @Existing_PAForecastOvhdCost)
  where  PAPROJNUMBER = @I_vPAPROJNUMBER
         and YEAR1 = @YEAR1
         and ((PERIODID = 0)
               or (PERIODID = @l_Period))
  
  if @@error <> 0
    begin
      select @O_iErrorState = 6138
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  update PA01121
  set    PABQuantity = PABQuantity + (@I_vPABQuantity - @Existing_PABQuantity),
         PABTotalCost = PABTotalCost + (@I_vPABTotalCost - @Existing_PABTotalCost),
         PABProfit = PABProfit + (@PABProfit - @Existing_PABProfit),
         PABBillings = PABBillings + (@PABBillings - @Existing_PABBillings),
         PABaselineOvhdCost = PABaselineOvhdCost + (@PABaselineOvhdCost - @Existing_PABaselineOvhdCost),
         PAFQuantity = PAFQuantity + (@I_vPAFQuantity - @Existing_PAFQuantity),
         PAFTotalCost = PAFTotalCost + (@I_vPAFTotalCost - @Existing_PAFTotalCost),
         PAFProfit = PAFProfit + (@PAFProfit - @Existing_PAFProfit),
         PAFBillings = PAFBillings + (@PAFBillings - @Existing_PAFBillings),
         PAForecastOvhdCost = PAForecastOvhdCost + (@PAForecastOvhdCost - @Existing_PAForecastOvhdCost)
  where  PACONTNUMBER = @PACONTNUMBER
         and YEAR1 = @YEAR1
         and ((PERIODID = 0)
               or (PERIODID = @l_Period))
  
  if @@error <> 0
    begin
      select @O_iErrorState = 6139
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  update PA00511
  set    PABQuantity = PABQuantity + (@I_vPABQuantity - @Existing_PABQuantity),
         PABTotalCost = PABTotalCost + (@I_vPABTotalCost - @Existing_PABTotalCost),
         PABProfit = PABProfit + (@PABProfit - @Existing_PABProfit),
         PABBillings = PABBillings + (@PABBillings - @Existing_PABBillings),
         PABaselineOvhdCost = PABaselineOvhdCost + (@PABaselineOvhdCost - @Existing_PABaselineOvhdCost),
         PAFQuantity = PAFQuantity + (@I_vPAFQuantity - @Existing_PAFQuantity),
         PAFTotalCost = PAFTotalCost + (@I_vPAFTotalCost - @Existing_PAFTotalCost),
         PAFProfit = PAFProfit + (@PAFProfit - @Existing_PAFProfit),
         PAFBillings = PAFBillings + (@PAFBillings - @Existing_PAFBillings),
         PAForecastOvhdCost = PAForecastOvhdCost + (@PAForecastOvhdCost - @Existing_PAForecastOvhdCost)
  where  CUSTNMBR = @CUSTNMBR
         and YEAR1 = @YEAR1
         and ((PERIODID = 0)
               or (PERIODID = @l_Period))
  
  if @@error <> 0
    begin
      select @O_iErrorState = 6140
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@Existing_PACOSTCATID = '')
    begin
      insert into PA43001
                 (PAsfid,
                  PArecordid,
                  PAcosttrxid,
                  PAaccttype,
                  PAACTINDX)
      select 29,
             @I_vPAPROJNUMBER,
             @I_vPACOSTCATID,
             PAaccttype,
             PAACTINDX
      from   PA43001
      where  PAcosttrxid = @l_tmp_cost_trx_id
             and PArecordid = ''
      
      if @@error <> 0
        begin
          select @O_iErrorState = 7733
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      insert into PA01301
                 (PAPROJNUMBER,
                  PACOSTCATID,
                  PATU,
                  PAIV_Item_Checkbox,
                  PASTAT,
                  PAProfitType,
                  PAbllngtype,
                  PABase_Qty,
                  PABase_Unit_Cost,
                  PABaseProfitAmount,
                  PABaseOvhdCost,
                  PAForecastBaseOvrhdAmt,
                  PAForecastBaseQty,
                  PAForecastBaseUnitCost,
                  PAForecastBaseProfitAmt,
                  PABBeginDate,
                  PABEndDate,
                  PABQuantity,
                  PABUnitCost,
                  PABTotalCost,
                  PABBillings,
                  PABProfit,
                  PABProfitAmt,
                  PABProfitPcnt,
                  PABTaxPaidAmt,
                  PABTaxChargedAmt,
                  PABaselineOvhdCost,
                  PABaselinePTaxOptions,
                  PABaselineCTaxSchedID,
                  PABaselineSTaxOptions,
                  PABaselineBTaxSchedID,
                  PAACTUALBEGDATE,
                  PA_Actual_End_Date,
                  PAFBeginDate,
                  PAFEndDate,
                  PAFQuantity,
                  PAFUnitCost,
                  PAFTotalCost,
                  PAFBillings,
                  PAFProfit,
                  PAFTaxPaidAmt,
                  PAFTaxChargedAmt,
                  PAFProfitAmt,
                  PAFProfitPcnt,
                  PAForecastOvhdCost,
                  PAForecastPTaxOptions,
                  PAForecastCTaxSchedID,
                  PAForecastSTaxOptions,
                  PAForecastBTaxSchedID,
                  PAUnit_of_Measure,
                  UOMSCHDL,
                  PAUOfMMult,
                  DECPLQTY,
                  PAPay_Code_Hourly,
                  PAPay_Code_Salary,
                  DECPLCUR,
                  PALabor_Rate_Table_ID,
                  PALabor_RateTable_Type,
                  PALabor_Rate_Table_Acc,
                  PAEquip_Rate_Table_ID,
                  PAEquip_Rate_Table_Acc,
                  TM_WIP_SRC,
                  TM_COGS_SRC,
                  TM_Cost_Contra_SRC,
                  TM_Unbilled_AR_SRC,
                  TM_Unbilled_Rev_SRC,
                  TM_AR_SRC,
                  TM_Prj_Rev_SRC,
                  TM_OVHD_SRC,
                  FF_WIP_SRC,
                  FF_Cost_Contra_SRC,
                  FF_AR_SRC,
                  FF_Prj_Billings_SRC,
                  FF_Prj_Expense_SRC,
                  FF_Prj_Revenue_SRC,
                  FF_Prj_Loss_SRC,
                  FF_BIEE_SRC,
                  FF_EIEB_SRC,
                  FF_OVHD_SRC,
                  PAProject_Amount,
                  PAOverheadRateMethod,
                  PABaselineOvhdAmtPerUnit,
                  PABaselineOvrhdPct,
                  PAForecastOvrhdAmtPerUnt,
                  PAForecastOvrhdPct,
                  PAUBeginDate,
                  PAUEndDate,
                  PAUnpostedQty,
                  PAUnpostedTotalCostN,
                  PAUnposted_Overhead,
                  PAUnpostedProfitN,
                  PAUnposted_Tax_Amount,
                  PAUnpostAccrRevN,
                  PAUnpostedCommitedQty,
                  PAUnpostedCommitedCost,
                  PAUnpostedCommitedTaxAmt,
                  PAUNPOSTBIEEAMOUNT,
                  PAUNPEIEBAMOUNT,
                  PAunpostbillamt,
                  PAPostedQty,
                  PAPostedTotalCostN,
                  PAPosted_Overhead,
                  PAPostedProfitN,
                  PAPosted_Tax_Amount,
                  PAPosted_Accr_RevN,
                  PAPostedCommitedQty,
                  PAPostedCommitedCost,
                  PAPostedCommitedTaxAmt,
                  PAPostRecogRevN,
                  PAPOSBIEEAMOUNT,
                  PAPOSEIEBAMOUNT,
                  PApostbillamt,
                  PAWrite_UpDown_Amount,
                  PABilled_QtyN,
                  PABilled_Cost,
                  PABilled_Accrued_Revenu,
                  PACostPcntCompleted,
                  PAQuantityPcntCompleted,
                  PA_Receipts_Amount,
                  PA_Actual_Receipts_Amoun,
                  PA_Earnings,
                  PA_Cost_of_Earnings,
                  PAUnpostBillN,
                  PAUnpostDiscDolAmtN,
                  PAUnposted_Sales_Tax_Am,
                  PAPostedBillingsN,
                  PAPostedDiscDolAmtN,
                  PAPosted_Sales_Tax_Amou,
                  PABCWPAMT,
                  PABCWSAMT,
                  PAACWPAMT,
                  LNITMSEQ,
                  PALineItemSeq,
                  PAbillnoteidx,
                  PAUnpostedRecogRevN,
                  PAApproved_Accrued_Reve,
                  PAApproved_Cost,
                  PAApproved_Quantity,
                  PACommitted_Costs,
                  PACommitted_Qty,
                  PAPOQty,
                  PAPOCost,
                  PAPOPostedQty,
                  PAPOPostedCost,
                  WROFAMNT,
                  ActualWriteOffAmount,
                  DISTKNAM,
                  ActualDiscTakenAmount,
                  PAsfid,
                  PAPreviouslyOpen,
                  PAtaxpaidamt,
                  PAPostedTaxPaidN,
                  PAPOSTRETAMT,
                  PAUNPOSTRETAMT,
                  PA_Write_Off_Tax_Amount,
                  PAActualWOTaxAmt,
                  PA_Terms_Taken_Tax_Amt,
                  PAActualTermsTakenTax,
                  PACHGORDNO,
                  PAUNAPPRVPROJAMT,
                  PAUNAPFRTOTBILL,
                  PAUNAPPFRTOTCST,
                  PAUNFRCTOTPROF,
                  PAUNPFRTOTQTY,
                  PAUNABSTOTBILL,
                  PAUNPBSTOTCST,
                  PAUNPBSLTOTPROF,
                  PAUNPBSTOTQTY,
                  PATOTCHGORDAMT,
                  PAPosted_Earnings,
                  PAActualCostofEarnings,
                  PAUnpostedLossAmount,
                  PAActualLossAmount,
                  PAMCBProfitAmt,
                  PAMCBBillings,
                  PAMCFProfitAmt,
                  PAMCFBillings,
                  PAMCBaseProfitAmount,
                  PAMCForecastBProfitAmt,
                  PAMCProjectAmount,
                  PAMCActualBillings,
                  PAMCActualDiscountAmt,
                  PAMCActualSalesTaxAmt,
                  PAMCUnpostBillN,
                  PAMCUnpostedDiscount,
                  PAMCUnpostedSalesTax)
      select @I_vPAPROJNUMBER,
             @I_vPACOSTCATID,
             @PATU,
             @PAIV_Item_Checkbox,
             @I_vPASTAT,
             @I_vPAProfitType,
             @I_vPAbllngtype,
             @PABase_Qty,
             @PABase_Unit_Cost,
             @PABaseProfitAmount,
             @PABaseOvhdCost,
             @PAForecastBaseOvrhdAmt,
             @PAForecastBaseQty,
             @PAForecastBaseUnitCost,
             @PAForecastBaseProfitAmt,
             @I_vPABBeginDate,
             @I_vPABEndDate,
             @I_vPABQuantity,
             @I_vPABUnitCost,
             @I_vPABTotalCost,
             @PABBillings,
             @PABProfit,
             @I_vPABProfitAmt,
             @I_vPABProfitPcnt,
             @PABTaxPaidAmt,
             @PABTaxChargedAmt,
             @PABaselineOvhdCost,
             @I_vPABaselinePTaxOptions,
             @I_vPABaselineCTaxSchedID,
             @I_vPABaselineSTaxOptions,
             @I_vPABaselineBTaxSchedID,
             @PAACTUALBEGDATE,
             @PA_Actual_End_Date,
             @I_vPAFBeginDate,
             @I_vPAFEndDate,
             @I_vPAFQuantity,
             @I_vPAFUnitCost,
             @I_vPAFTotalCost,
             @PAFBillings,
             @PAFProfit,
             @PAFTaxPaidAmt,
             @PAFTaxChargedAmt,
             @I_vPAFProfitAmt,
             @I_vPAFProfitPcnt,
             @PAForecastOvhdCost,
             @I_vPAForecastPTaxOptions,
             @I_vPAForecastCTaxSchedID,
             @I_vPAForecastSTaxOptions,
             @I_vPAForecastBTaxSchedID,
             @I_vPAUnit_of_Measure,
             @I_vUOMSCHDL,
             @PAUOfMMult,
             @DECPLQTY,
             @I_vPAPay_Code_Hourly,
             @I_vPAPay_Code_Salary,
             @DECPLCUR,
             @PALabor_Rate_Table_ID,
             @PALabor_RateTable_Type,
             @PALabor_Rate_Table_Acc,
             @PAEquip_Rate_Table_ID,
             @PAEquip_Rate_Table_Acc,
             @TM_WIP_SRC,
             @TM_COGS_SRC,
             @TM_Cost_Contra_SRC,
             @TM_Unbilled_AR_SRC,
             @TM_Unbilled_Rev_SRC,
             @TM_AR_SRC,
             @TM_Prj_Rev_SRC,
             @TM_OVHD_SRC,
             @FF_WIP_SRC,
             @FF_Cost_Contra_SRC,
             @FF_AR_SRC,
             @FF_Prj_Billings_SRC,
             @FF_Prj_Expense_SRC,
             @FF_Prj_Revenue_SRC,
             @FF_Prj_Loss_SRC,
             @FF_BIEE_SRC,
             @FF_EIEB_SRC,
             @FF_OVHD_SRC,
             @PAProject_Amount,
             @PAOverheadRateMethod,
             @I_vPABaselineOvhdAmtPerUnit,
             @I_vPABaselineOvrhdPct,
             @I_vPAForecastOvrhdAmtPerUnt,
             @I_vPAForecastOvrhdPct,
             @PAUBeginDate,
             @PAUEndDate,
             @PAUnpostedQty,
             @PAUnpostedTotalCostN,
             @PAUnposted_Overhead,
             @PAUnpostedProfitN,
             @PAUnposted_Tax_Amount,
             @PAUnpostAccrRevN,
             @PAUnpostedCommitedQty,
             @PAUnpostedCommitedCost,
             @PAUnpostedCommitedTaxAmt,
             @PAUNPOSTBIEEAMOUNT,
             @PAUNPEIEBAMOUNT,
             @PAunpostbillamt,
             @PAPostedQty,
             @PAPostedTotalCostN,
             @PAPosted_Overhead,
             @PAPostedProfitN,
             @PAPosted_Tax_Amount,
             @PAPosted_Accr_RevN,
             @PAPostedCommitedQty,
             @PAPostedCommitedCost,
             @PAPostedCommitedTaxAmt,
             @PAPostRecogRevN,
             @PAPOSBIEEAMOUNT,
             @PAPOSEIEBAMOUNT,
             @PApostbillamt,
             @PAWrite_UpDown_Amount,
             @PABilled_QtyN,
             @PABilled_Cost,
             @PABilled_Accrued_Revenu,
             @PACostPcntCompleted,
             @PAQuantityPcntCompleted,
             @PA_Receipts_Amount,
             @PA_Actual_Receipts_Amoun,
             @PA_Earnings,
             @PA_Cost_of_Earnings,
             @PAUnpostBillN,
             @PAUnpostDiscDolAmtN,
             @PAUnposted_Sales_Tax_Am,
             @PAPostedBillingsN,
             @PAPostedDiscDolAmtN,
             @PAPosted_Sales_Tax_Amou,
             @PABCWPAMT,
             @PABCWSAMT,
             @PAACWPAMT,
             @I_vLNITMSEQ,
             @I_vPALineItemSeq,
             @PAbillnoteidx,
             @PAUnpostedRecogRevN,
             @PAApproved_Accrued_Reve,
             @PAApproved_Cost,
             @PAApproved_Quantity,
             @PACommitted_Costs,
             @PACommitted_Qty,
             @PAPOQty,
             @PAPOCost,
             @PAPOPostedQty,
             @PAPOPostedCost,
             @WROFAMNT,
             @ActualWriteOffAmount,
             @DISTKNAM,
             @ActualDiscTakenAmount,
             @PAsfid,
             @PAPreviouslyOpen,
             @PAtaxpaidamt,
             @PAPostedTaxPaidN,
             @PAPOSTRETAMT,
             @PAUNPOSTRETAMT,
             @PA_Write_Off_Tax_Amount,
             @PAActualWOTaxAmt,
             @PA_Terms_Taken_Tax_Amt,
             @PAActualTermsTakenTax,
             @PACHGORDNO,
             @PAUNAPPRVPROJAMT,
             @PAUNAPFRTOTBILL,
             @PAUNAPPFRTOTCST,
             @PAUNFRCTOTPROF,
             @PAUNPFRTOTQTY,
             @PAUNABSTOTBILL,
             @PAUNPBSTOTCST,
             @PAUNPBSLTOTPROF,
             @PAUNPBSTOTQTY,
             @PATOTCHGORDAMT,
             @PAPosted_Earnings,
             @PAActualCostofEarnings,
             @PAUnpostedLossAmount,
             @PAActualLossAmount,
             @PAMCBProfitAmt,
             @PAMCBBillings,
             @PAMCFProfitAmt,
             @PAMCFBillings,
             @PAMCBaseProfitAmount,
             @PAMCForecastBProfitAmt,
             @PAMCProjectAmount,
             @PAMCActualBillings,
             @PAMCActualDiscountAmt,
             @PAMCActualSalesTaxAmt,
             @PAMCUnpostBillN,
             @PAMCUnpostedDiscount,
             @PAMCUnpostedSalesTax
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6135
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      update PA01301
      set    PASTAT = @I_vPASTAT,
             PAProfitType = case 
                              when @I_vPASTAT = 4 then @I_vPAProfitType
                              else PAProfitType
                            end,
             PAbllngtype = case 
                             when @I_vPASTAT = 4 then @I_vPAbllngtype
                             else PAbllngtype
                           end,
             PABase_Qty = case 
                            when @I_vPASTAT = 4 then @PABase_Qty
                            else PABase_Qty
                          end,
             PABase_Unit_Cost = case 
                                  when @I_vPASTAT = 4 then @PABase_Unit_Cost
                                  else PABase_Unit_Cost
                                end,
             PABaseProfitAmount = case 
                                    when @I_vPASTAT = 4 then @PABaseProfitAmount
                                    else PABaseProfitAmount
                                  end,
             PABaseOvhdCost = case 
                                when @I_vPASTAT = 4 then @PABaseOvhdCost
                                else PABaseOvhdCost
                              end,
             PAForecastBaseOvrhdAmt = case 
                                        when (@I_vPASTAT = 1)
                                              or (@I_vPASTAT = 2) then @PAForecastBaseOvrhdAmt
                                        when (@I_vPASTAT = 4)
                                             and ((@I_vRollUpEst_to_Forecast = 1)
                                                   or (@Existing_PAPreviouslyOpen = 0)) then @PAForecastBaseOvrhdAmt
                                        else PAForecastBaseOvrhdAmt
                                      end,
             PAForecastBaseQty = case 
                                   when (@I_vPASTAT = 1)
                                         or (@I_vPASTAT = 2) then @PAForecastBaseQty
                                   when (@I_vPASTAT = 4)
                                        and ((@I_vRollUpEst_to_Forecast = 1)
                                              or (@Existing_PAPreviouslyOpen = 0)) then @PAForecastBaseQty
                                   else PAForecastBaseQty
                                 end,
             PAForecastBaseUnitCost = case 
                                        when (@I_vPASTAT = 1)
                                              or (@I_vPASTAT = 2) then @PAForecastBaseUnitCost
                                        when (@I_vPASTAT = 4)
                                             and ((@I_vRollUpEst_to_Forecast = 1)
                                                   or (@Existing_PAPreviouslyOpen = 0)) then @PAForecastBaseUnitCost
                                        else PAForecastBaseUnitCost
                                      end,
             PAForecastBaseProfitAmt = case 
                                         when (@I_vPASTAT = 1)
                                               or (@I_vPASTAT = 2) then @PAForecastBaseProfitAmt
                                         when (@I_vPASTAT = 4)
                                              and ((@I_vRollUpEst_to_Forecast = 1)
                                                    or (@Existing_PAPreviouslyOpen = 0)) then @PAForecastBaseProfitAmt
                                         else PAForecastBaseProfitAmt
                                       end,
             PABBeginDate = case 
                              when (@I_vPASTAT = 4)
                                   and (@I_vPABBeginDate <> '') then @I_vPABBeginDate
                              else PABBeginDate
                            end,
             PABEndDate = case 
                            when (@I_vPASTAT = 4)
                                 and (@I_vPABEndDate <> '') then @I_vPABEndDate
                            else PABEndDate
                          end,
             PABQuantity = case 
                             when @I_vPASTAT = 4 then @I_vPABQuantity
                             else PABQuantity
                           end,
             PABUnitCost = case 
                             when @I_vPASTAT = 4 then @I_vPABUnitCost
                             else PABUnitCost
                           end,
             PABTotalCost = case 
                              when @I_vPASTAT = 4 then @I_vPABTotalCost
                              else PABTotalCost
                            end,
             PABBillings = case 
                             when @I_vPASTAT = 4 then @PABBillings
                             else PABBillings
                           end,
             PABProfit = case 
                           when @I_vPASTAT = 4 then @PABProfit
                           else PABProfit
                         end,
             PABProfitAmt = case 
                              when @I_vPASTAT = 4 then @I_vPABProfitAmt
                              else PABProfitAmt
                            end,
             PABProfitPcnt = case 
                               when @I_vPASTAT = 4 then @I_vPABProfitPcnt
                               else PABProfitPcnt
                             end,
             PABaselineOvhdCost = case 
                                    when @I_vPASTAT = 4 then @PABaselineOvhdCost
                                    else PABaselineOvhdCost
                                  end,
             PABaselinePTaxOptions = case 
                                       when @I_vPASTAT = 4 then @I_vPABaselinePTaxOptions
                                       else PABaselinePTaxOptions
                                     end,
             PABaselineCTaxSchedID = case 
                                       when @I_vPASTAT = 4 then @I_vPABaselineCTaxSchedID
                                       else PABaselineCTaxSchedID
                                     end,
             PABaselineSTaxOptions = case 
                                       when @I_vPASTAT = 4 then @I_vPABaselineSTaxOptions
                                       else PABaselineSTaxOptions
                                     end,
             PABaselineBTaxSchedID = case 
                                       when @I_vPASTAT = 4 then @I_vPABaselineBTaxSchedID
                                       else PABaselineBTaxSchedID
                                     end,
             PAFBeginDate = case 
                              when ((@I_vPASTAT = 1)
                                     or (@I_vPASTAT = 2))
                                   and (@I_vPAFBeginDate <> '') then @I_vPAFBeginDate
                              when ((@I_vPASTAT = 4)
                                    and ((@I_vRollUpEst_to_Forecast = 1)
                                          or (@Existing_PAPreviouslyOpen = 0)))
                                   and (@I_vPAFBeginDate <> '') then @I_vPAFBeginDate
                              else PAFBeginDate
                            end,
             PAFEndDate = case 
                            when ((@I_vPASTAT = 1)
                                   or (@I_vPASTAT = 2))
                                 and (@I_vPAFEndDate <> '') then @I_vPAFEndDate
                            when ((@I_vPASTAT = 4)
                                  and ((@I_vRollUpEst_to_Forecast = 1)
                                        or (@Existing_PAPreviouslyOpen = 0)))
                                 and (@I_vPAFEndDate <> '') then @I_vPAFEndDate
                            else PAFEndDate
                          end,
             PAFQuantity = case 
                             when (@I_vPASTAT = 1)
                                   or (@I_vPASTAT = 2) then @I_vPAFQuantity
                             when (@I_vPASTAT = 4)
                                  and ((@I_vRollUpEst_to_Forecast = 1)
                                        or (@Existing_PAPreviouslyOpen = 0)) then @I_vPAFQuantity
                             else PAFQuantity
                           end,
             PAFUnitCost = case 
                             when (@I_vPASTAT = 1)
                                   or (@I_vPASTAT = 2) then @I_vPAFUnitCost
                             when (@I_vPASTAT = 4)
                                  and ((@I_vRollUpEst_to_Forecast = 1)
                                        or (@Existing_PAPreviouslyOpen = 0)) then @I_vPAFUnitCost
                             else PAFUnitCost
                           end,
             PAFTotalCost = case 
                              when (@I_vPASTAT = 1)
                                    or (@I_vPASTAT = 2) then @I_vPAFTotalCost
                              when (@I_vPASTAT = 4)
                                   and ((@I_vRollUpEst_to_Forecast = 1)
                                         or (@Existing_PAPreviouslyOpen = 0)) then @I_vPAFTotalCost
                              else PAFTotalCost
                            end,
             PAFBillings = case 
                             when (@I_vPASTAT = 1)
                                   or (@I_vPASTAT = 2) then @PAFBillings
                             when (@I_vPASTAT = 4)
                                  and ((@I_vRollUpEst_to_Forecast = 1)
                                        or (@Existing_PAPreviouslyOpen = 0)) then @PAFBillings
                             else PAFBillings
                           end,
             PAFProfit = case 
                           when (@I_vPASTAT = 1)
                                 or (@I_vPASTAT = 2) then @PAFProfit
                           when (@I_vPASTAT = 4)
                                and ((@I_vRollUpEst_to_Forecast = 1)
                                      or (@Existing_PAPreviouslyOpen = 0)) then @PAFProfit
                           else PAFProfit
                         end,
             PAFProfitAmt = case 
                              when (@I_vPASTAT = 1)
                                    or (@I_vPASTAT = 2) then @I_vPAFProfitAmt
                              when (@I_vPASTAT = 4)
                                   and ((@I_vRollUpEst_to_Forecast = 1)
                                         or (@Existing_PAPreviouslyOpen = 0)) then @I_vPAFProfitAmt
                              else PAFProfitAmt
                            end,
             PAFProfitPcnt = case 
                               when (@I_vPASTAT = 1)
                                     or (@I_vPASTAT = 2) then @I_vPAFProfitPcnt
                               when (@I_vPASTAT = 4)
                                    and ((@I_vRollUpEst_to_Forecast = 1)
                                          or (@Existing_PAPreviouslyOpen = 0)) then @I_vPAFProfitPcnt
                               else PAFProfitPcnt
                             end,
             PAForecastOvhdCost = case 
                                    when (@I_vPASTAT = 1)
                                          or (@I_vPASTAT = 2) then @PAForecastOvhdCost
                                    when (@I_vPASTAT = 4)
                                         and ((@I_vRollUpEst_to_Forecast = 1)
                                               or (@Existing_PAPreviouslyOpen = 0)) then @PAForecastOvhdCost
                                    else PAForecastOvhdCost
                                  end,
             PAForecastPTaxOptions = case 
                                       when (@I_vPASTAT = 1)
                                             or (@I_vPASTAT = 2) then @I_vPAForecastPTaxOptions
                                       when (@I_vPASTAT = 4)
                                            and ((@I_vRollUpEst_to_Forecast = 1)
                                                  or (@Existing_PAPreviouslyOpen = 0)) then @I_vPAForecastPTaxOptions
                                       else PAForecastPTaxOptions
                                     end,
             PAForecastCTaxSchedID = case 
                                       when (@I_vPASTAT = 1)
                                             or (@I_vPASTAT = 2) then @I_vPAForecastCTaxSchedID
                                       when (@I_vPASTAT = 4)
                                            and ((@I_vRollUpEst_to_Forecast = 1)
                                                  or (@Existing_PAPreviouslyOpen = 0)) then @I_vPAForecastCTaxSchedID
                                       else PAForecastCTaxSchedID
                                     end,
             PAForecastSTaxOptions = case 
                                       when (@I_vPASTAT = 1)
                                             or (@I_vPASTAT = 2) then @I_vPAForecastSTaxOptions
                                       when (@I_vPASTAT = 4)
                                            and ((@I_vRollUpEst_to_Forecast = 1)
                                                  or (@Existing_PAPreviouslyOpen = 0)) then @I_vPAForecastSTaxOptions
                                       else PAForecastSTaxOptions
                                     end,
             PAForecastBTaxSchedID = case 
                                       when (@I_vPASTAT = 1)
                                             or (@I_vPASTAT = 2) then @I_vPAForecastBTaxSchedID
                                       when (@I_vPASTAT = 4)
                                            and ((@I_vRollUpEst_to_Forecast = 1)
                                                  or (@Existing_PAPreviouslyOpen = 0)) then @I_vPAForecastBTaxSchedID
                                       else PAForecastBTaxSchedID
                                     end,
             PAUnit_of_Measure = case 
                                   when @I_vPASTAT = 4 then @I_vPAUnit_of_Measure
                                   else PAUnit_of_Measure
                                 end,
             UOMSCHDL = case 
                          when @I_vPASTAT = 4 then @I_vUOMSCHDL
                          else UOMSCHDL
                        end,
             PAPay_Code_Hourly = case 
                                   when @I_vPASTAT = 4 then @I_vPAPay_Code_Hourly
                                   else PAPay_Code_Hourly
                                 end,
             PAPay_Code_Salary = case 
                                   when @I_vPASTAT = 4 then @I_vPAPay_Code_Salary
                                   else PAPay_Code_Salary
                                 end,
             @PAProject_Amount = case 
                                   when @I_vPASTAT = 1 then @PABBillings
                                   when @I_vPASTAT = 2
                                         or @I_vPASTAT = 4 then @PAFBillings
                                   else PAProject_Amount
                                 end,
             PABaselineOvhdAmtPerUnit = case 
                                          when @I_vPASTAT = 4 then @I_vPABaselineOvhdAmtPerUnit
                                          else PABaselineOvhdAmtPerUnit
                                        end,
             PABaselineOvrhdPct = case 
                                    when @I_vPASTAT = 4 then @I_vPABaselineOvrhdPct
                                    else PABaselineOvrhdPct
                                  end,
             PAForecastOvrhdAmtPerUnt = case 
                                          when (@I_vPASTAT = 1)
                                                or (@I_vPASTAT = 2) then @I_vPAForecastOvrhdAmtPerUnt
                                          when (@I_vPASTAT = 4)
                                               and ((@I_vRollUpEst_to_Forecast = 1)
                                                     or (@Existing_PAPreviouslyOpen = 0)) then @I_vPAForecastOvrhdAmtPerUnt
                                          else PAForecastOvrhdAmtPerUnt
                                        end,
             PAForecastOvrhdPct = case 
                                    when (@I_vPASTAT = 1)
                                          or (@I_vPASTAT = 2) then @I_vPAForecastOvrhdPct
                                    when (@I_vPASTAT = 4)
                                         and ((@I_vRollUpEst_to_Forecast = 1)
                                               or (@Existing_PAPreviouslyOpen = 0)) then @I_vPAForecastOvrhdPct
                                    else PAForecastOvrhdPct
                                  end,
             PAPreviouslyOpen = case 
                                  when (@I_vPASTAT <> 4) then 1
                                  else PAPreviouslyOpen
                                end
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PACOSTCATID = @I_vPACOSTCATID
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6411
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @DECPLQTY = @DECPLQTY - 1
  
  execute @iStatus = PA_Create_Periodic_Budget_Records
    @I_projectnumber = @I_vPAPROJNUMBER ,
    @I_transactionusage = @PATU ,
    @I_costcategory = @I_vPACOSTCATID ,
    @I_contractnumber = @PACONTNUMBER ,
    @I_customernumber = @CUSTNMBR ,
    @I_sourcefileID = @PAsfid ,
    @I_basestartdate = @I_vPABBeginDate ,
    @I_baseenddate = @I_vPABEndDate ,
    @I_forestartdate = @I_vPAFBeginDate ,
    @I_foreenddate = @I_vPAFEndDate ,
    @I_actualstartdate = @PAACTUALBEGDATE ,
    @I_actualenddate = @PA_Actual_End_Date ,
    @I_userdate = @userdate ,
    @I_deciqty = @DECPLQTY ,
    @I_functionaldeciglobals = @DECPLQTY ,
    @I_create = 0 ,
    @I_change_baseline = 1 ,
    @I_change_forecast = 1 ,
    @i_firstdate = '' ,
    @i_lastdate = '' ,
    @i_year_start = 0 ,
    @i_year_end = 0 ,
    @i_startyearupdate = @i_startyearupdate ,
    @i_endyearupdate = @i_endyearupdate ,
    @i_baselinestartyearupdate = @i_baselinestartyearupdate ,
    @i_baseline_period = @PABEndDate_Period ,
    @i_forecaststartyearupdate = @i_forecaststartyearupdate ,
    @i_forecast_period = @PAFEndDate_Period ,
    @l_base_ctr = @l_base_ctr ,
    @l_fore_ctr = @l_base_ctr ,
    @I_err = @PA_Create_Periodic_Budget_RecordsErrorState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@PA_Create_Periodic_Budget_RecordsErrorState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@PA_Create_Periodic_Budget_RecordsErrorState))
      
      select @O_iErrorState = 6129
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  execute @iStatus = PA_UpdateProjectDates
    @I_cPAPROJNUMBER = @I_vPAPROJNUMBER ,
    @O_iErrorState = @PA_UpdateProjectDatesErrorState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@PA_UpdateProjectDatesErrorState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@PA_UpdateProjectDatesErrorState))
      
      select @O_iErrorState = 6424
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  execute @iStatus = PA_UpdateContractDates
    @I_cPACONTNUMBER = @I_vPAPROJNUMBER ,
    @O_iErrorState = @PA_UpdateContractDatesErrorState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@PA_UpdateContractDatesErrorState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@PA_UpdateContractDatesErrorState))
      
      select @O_iErrorState = 6425
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAProjectBudgetMasterPost
    @I_vPAPROJNUMBER ,
    @I_vPACOSTCATID ,
    @I_vPASTAT ,
    @I_vPAProfitType ,
    @I_vPAbllngtype ,
    @I_vPABBeginDate ,
    @I_vPABEndDate ,
    @I_vLNITMSEQ ,
    @I_vPALineItemSeq ,
    @I_vPABQuantity ,
    @I_vPABUnitCost ,
    @I_vPABProfitAmt ,
    @I_vPABProfitPcnt ,
    @I_vPABTotalCost ,
    @I_vPABaselinePTaxOptions ,
    @I_vPABaselineCTaxSchedID ,
    @I_vPABaselineSTaxOptions ,
    @I_vPABaselineBTaxSchedID ,
    @I_vPAFQuantity ,
    @I_vPAFUnitCost ,
    @I_vPAFTotalCost ,
    @I_vPAFProfitAmt ,
    @I_vPAFProfitPcnt ,
    @I_vPAForecastOvrhdAmtPerUnt ,
    @I_vPAForecastOvrhdPct ,
    @I_vPAForecastPTaxOptions ,
    @I_vPAForecastCTaxSchedID ,
    @I_vPAForecastSTaxOptions ,
    @I_vPAForecastBTaxSchedID ,
    @I_vPAFBeginDate ,
    @I_vPAFEndDate ,
    @I_vPAUnit_of_Measure ,
    @I_vUOMSCHDL ,
    @I_vPAPay_Code_Hourly ,
    @I_vPAPay_Code_Salary ,
    @I_vPABaselineOvhdAmtPerUnit ,
    @I_vPABaselineOvrhdPct ,
    @I_vUpdateExisting ,
    @I_vRollUpEst_to_Forecast ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 6136
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Projects' ,
        @I_vINDEX1 = @I_vPAPROJNUMBER ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 1 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 6137
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  return (@O_iErrorState)

