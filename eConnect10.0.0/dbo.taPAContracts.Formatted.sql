

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAContracts]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAContracts]
  
create procedure [dbo].[taPAContracts]
                @I_vCUSTNMBR                char(15),
                @I_vPAcontid                char(15),
                @I_vPAcontname              char(31),
                @I_vPACONTNUMBER            char(11),
                @I_vPAcontclassid           char(15)  = null,
                @I_vPAProjectType           smallint,
                @I_vPAAcctgMethod           smallint,
                @I_vPAMCCURNCYID            char(15),
                @I_vPASTAT                  smallint  = null,
                @I_vPApurordnum             char(17)  = null,
                @I_vPAcloseProjcosts        smallint  = null,
                @I_vPAclosetobillings       smallint  = null,
                @I_vPAUD1_Cont              char(51)  = null,
                @I_vPAUD2_Cont              char(51)  = null,
                @I_vPAContMgrID             char(15)  = null,
                @I_vPABusMgrID              char(15)  = null,
                @I_vADRSCODE                char(15)  = null,
                @I_vSLPRSNID                char(15)  = null,
                @I_vSALSTERR                char(15)  = null,
                @I_vCOMPRCNT                int  = null,
                @I_vCOMAPPTO                smallint  = null,
                @I_vPABILLFORMAT            char(15)  = null,
                @I_vPRBTADCD                char(15)  = null,
                @I_vPA_RestrictCustomerList smallint  = null,
                @I_vDSCPCTAM                numeric(19,2)  = null,
                @I_vPASegmentCB             smallint  = null,
                @I_vPABILLCYCLEID1          char(15)  = null,
                @I_vPATRKCHGORDS            smallint  = null,
                @I_vPATRKCOBDGADDFLY        smallint  = null,
                @I_vPAbillnoteTS            char(1000)  = '',
                @I_vPAbillnoteEL            char(1000)  = '',
                @I_vPAbillnoteML            char(1000)  = '',
                @I_vPAbillnoteVI            char(1000)  = '',
                @I_vPAbillnoteEE            char(1000)  = '',
                @I_vPAbillnoteINV           char(1000)  = '',
                @I_vPAbillnoteFEE           char(1000)  = '',
                @I_vUpdateExisting          tinyint  = 0,
                @I_vRequesterTrx            smallint  = 0,
                @I_vUSRDEFND1               char(50)  = '',
                @I_vUSRDEFND2               char(50)  = '',
                @I_vUSRDEFND3               char(50)  = '',
                @I_vUSRDEFND4               varchar(8000)  = '',
                @I_vUSRDEFND5               varchar(8000)  = '',
                @O_iErrorState              int  output,
                @oErrString                 varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iStatus                          int,
           @iCustomState                     int,
           @iCustomErrString                 varchar(255),
           @iError                           int,
           @iCursorError                     int,
           @O_oErrorState                    int,
           @PALONGID_1                       char(9),
           @PALONGID_2                       char(9),
           @PALONGID_3                       char(9),
           @PALONGID_4                       char(9),
           @PALONGID_5                       char(9),
           @PABBeginDate                     datetime,
           @PABEndDate                       datetime,
           @PAFBeginDate                     datetime,
           @PAFEndDate                       datetime,
           @PAACTUALBEGDATE                  datetime,
           @PA_Actual_End_Date               datetime,
           @CNTCPRSN                         char(31),
           @LOCATNID                         char(15),
           @PASegmentCB                      smallint,
           @PAbillnoteidx                    int,
           @PAbillnoteidxts                  int,
           @PAELbillniteidx                  int,
           @PAbillnoteidxML                  int,
           @PAbillnoteidxvi                  int,
           @PAbillnoteidxee                  int,
           @PAbillnoteidxinv                 int,
           @PAbillnoteidxfee                 int,
           @PABQuantity                      numeric(19,5),
           @PABTotalCost                     numeric(19,5),
           @PABBillings                      numeric(19,5),
           @PABProfit                        numeric(19,5),
           @PABTaxPaidAmt                    numeric(19,5),
           @PABTaxChargedAmt                 numeric(19,5),
           @PABaselineOvhdCost               numeric(19,5),
           @PAFQuantity                      numeric(19,5),
           @PAFTotalCost                     numeric(19,5),
           @PAFBillings                      numeric(19,5),
           @PAFProfit                        numeric(19,5),
           @PAFTaxPaidAmt                    numeric(19,5),
           @PAFTaxChargedAmt                 numeric(19,5),
           @PAForecastOvhdCost               numeric(19,5),
           @PAProject_Amount                 numeric(19,5),
           @PAUnpostedQty                    numeric(19,5),
           @PAUnpostedTotalCostN             numeric(19,5),
           @PAUnposted_Overhead              numeric(19,5),
           @PAUnpostedProfitN                numeric(19,5),
           @PAUnposted_Tax_Amount            numeric(19,5),
           @PAUnpostAccrRevN                 numeric(19,5),
           @PAUnpostedCommitedQty            numeric(19,5),
           @PAUnpostedCommitedCost           numeric(19,5),
           @PAUnpostedCommitedTaxAmt         numeric(19,5),
           @PAUnpostedRecogRevN              numeric(19,5),
           @PAUnposted_Project_Fee           numeric(19,5),
           @PAUnposted_Retainer_Fee          numeric(19,5),
           @PAUnposted_Service_Fee           numeric(19,5),
           @PAUNPOSTRETAMT                   numeric(19,5),
           @PAUNPOSTBIEEAMOUNT               numeric(19,5),
           @PAUNPEIEBAMOUNT                  numeric(19,5),
           @PA_Unposted_Billed_Reten         numeric(19,5),
           @PAPostedQty                      numeric(19,5),
           @PAPostedTotalCostN               numeric(19,5),
           @PAPosted_Overhead                numeric(19,5),
           @PAPostedProfitN                  numeric(19,5),
           @PAPosted_Tax_Amount              numeric(19,5),
           @PAPosted_Accr_RevN               numeric(19,5),
           @PAPostedCommitedQty              numeric(19,5),
           @PAPostedCommitedCost             numeric(19,5),
           @PAPostedCommitedTaxAmt           numeric(19,5),
           @PAPostRecogRevN                  numeric(19,5),
           @PAPosted_Project_Fee             numeric(19,5),
           @PAPosted_Retainer_Fee            numeric(19,5),
           @PAPosted_Service_Fee             numeric(19,5),
           @PAPOSTRETAMT                     numeric(19,5),
           @PAPOSBIEEAMOUNT                  numeric(19,5),
           @PAPOSEIEBAMOUNT                  numeric(19,5),
           @PA_Actual_Billed_Retenti         numeric(19,5),
           @PAWrite_UpDown_Amount            numeric(19,5),
           @PABilled_QtyN                    numeric(19,5),
           @PABilled_Cost                    numeric(19,5),
           @PABilled_Accrued_Revenu          numeric(19,5),
           @PACostPcntCompleted              numeric(19,5),
           @PAQuantityPcntCompleted          numeric(19,5),
           @PA_Receipts_Amount               numeric(19,5),
           @PA_Actual_Receipts_Amoun         numeric(19,5),
           @PA_Earnings                      numeric(19,5),
           @PA_Cost_of_Earnings              numeric(19,5),
           @PAUnpostBillN                    numeric(19,5),
           @PAUnpostDiscDolAmtN              numeric(19,5),
           @PAUnposted_Sales_Tax_Am          numeric(19,5),
           @PAPostedBillingsN                numeric(19,5),
           @PAPostedDiscDolAmtN              numeric(19,5),
           @PAPosted_Sales_Tax_Amou          numeric(19,5),
           @PAService_Fee_Amount             numeric(19,5),
           @PARetainer_Fee_Amount            numeric(19,5),
           @PAProject_Fee_Amount             numeric(19,5),
           @PARetentionFeeAmount             numeric(19,5),
           @PABCWPAMT                        numeric(19,5),
           @PABCWSAMT                        numeric(19,5),
           @PAACWPAMT                        numeric(19,5),
           @NOTEINDX                         int,
           @PAApproved_Accrued_Reve          numeric(19,5),
           @PAApproved_Cost                  numeric(19,5),
           @PAApproved_Quantity              numeric(19,5),
           @WROFAMNT                         numeric(19,5),
           @ActualWriteOffAmount             numeric(19,5),
           @DISTKNAM                         numeric(19,5),
           @ActualDiscTakenAmount            numeric(19,5),
           @PACommitted_Costs                numeric(19,5),
           @PACommitted_Qty                  numeric(19,5),
           @PAPOCost                         numeric(19,5),
           @PAPOQty                          numeric(19,5),
           @PAPOPostedCost                   numeric(19,5),
           @PAPOPostedQty                    numeric(19,5),
           @PAtaxpaidamt                     numeric(19,5),
           @PAPostedTaxPaidN                 numeric(19,5),
           @PApretainage                     numeric(19,5),
           @PAunpretainage                   numeric(19,5),
           @PA_Write_Off_Tax_Amount          numeric(19,5),
           @PAActualWOTaxAmt                 numeric(19,5),
           @PA_Terms_Taken_Tax_Amt           numeric(19,5),
           @PAActualTermsTakenTax            numeric(19,5),
           @PAPosted_Earnings                numeric(19,5),
           @PAActualCostofEarnings           numeric(19,5),
           @PAUnpostedLossAmount             numeric(19,5),
           @PAActualLossAmount               numeric(19,5),
           @PAFQLaborOnly                    numeric(19,5),
           @PAAQLaborOnly                    numeric(19,5),
           @DBName                           char(50),
           @O_iInitErrorState                int,
           @oInitErrString                   varchar(255),
           @SLPRSNID                         char(15),
           @COMPRCNT                         int,
           @COMAPPTO                         smallint,
           @SALSTERR                         char(15),
           @iGetNextNoteIdxErrState          int,
           @sCompanyID                       int,
           @userdate                         datetime,
           @iCalculateGLPeriodErrState       int,
           @fClosed                          smallint,
           @YEAR1                            int,
           @l_Period                         int,
           @nErr                             int,
           @Existing_CUSTNMBR                char(15),
           @Existing_PAcontid                char(15),
           @Existing_PAcontname              char(31),
           @Existing_PACONTNUMBER            char(11),
           @Existing_PAcontclassid           char(15),
           @Existing_PAProjectType           smallint,
           @Existing_PAAcctgMethod           smallint,
           @Existing_PASTAT                  smallint,
           @Existing_PApurordnum             char(17),
           @Existing_PAcloseProjcosts        smallint,
           @Existing_PAclosetobillings       smallint,
           @Existing_PAContMgrID             char(15),
           @Existing_PABusMgrID              char(15),
           @Existing_SLPRSNID                char(15),
           @Existing_SALSTERR                char(15),
           @Existing_COMPRCNT                int,
           @Existing_COMAPPTO                smallint,
           @Existing_PABILLFORMAT            char(15),
           @Existing_ADRSCODE                char(15),
           @Existing_PRBTADCD                char(15),
           @Existing_PAUD1_Cont              char(51),
           @Existing_PAUD2_Cont              char(51),
           @Existing_PASegmentCB             smallint,
           @Cursor_PAPROJNUMBER              char(15),
           @fStatus                          smallint,
           @Update_Budget_Lines              smallint,
           @Existing_PAbillnoteidxts         int,
           @Existing_PAELbillniteidx         int,
           @Existing_PAbillnoteidxML         int,
           @Existing_PAbillnoteidxvi         int,
           @Existing_PAbillnoteidxee         int,
           @Existing_PAbillnoteidxinv        int,
           @Existing_PAbillnoteidxfee        int,
           @Existing_PA_RestrictCustomerList smallint,
           @Existing_DSCPCTAM                numeric(19,2),
           @Existing_PABILLCYCLEID1          char(15),
           @Existing_PATRKCHGORDS            smallint,
           @Existing_PATRKCOBDGADDFLY        smallint,
           @PAchangeorderdocounter           char(17),
           @PAMCProjectAmount                numeric(19,5),
           @PAMCUnpostBillN                  numeric(19,5),
           @PAMCUnpostedSalesTax             numeric(19,5),
           @PAMCUnpostedDiscount             numeric(19,5),
           @PAMCActualBillings               numeric(19,5),
           @PAMCActualSalesTaxAmt            numeric(19,5),
           @PAMCActualDiscountAmt            numeric(19,5),
           @PAMCBBillings                    numeric(19,5),
           @PAMCFBillings                    numeric(19,5),
           @PA_MC_Project_Fee_Amount         numeric(19,5),
           @PA_MC_Retainer_Fee_Amt           numeric(19,5),
           @PA_MC_Service_Fee_Amount         numeric(19,5),
           @PA_MC_UnpostedProjectFee         numeric(19,5),
           @PA_MC_Unposted_Retainer          numeric(19,5),
           @PA_MC_UnpostedServiceFee         numeric(19,5),
           @PA_MC_Actual_Project_Fee         numeric(19,5),
           @PA_MC_Actual_RetainerFee         numeric(19,5),
           @PA_MC_Actual_Service_Fee         numeric(19,5),
           @CURRNIDX                         int,
           @Existing_PAMCCURNCYID            char(15)
  
  select @iStatus = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @iError = 0,
         @iCursorError = 0,
         @O_oErrorState = 0,
         @PALONGID_1 = '',
         @PALONGID_2 = '',
         @PALONGID_3 = '',
         @PALONGID_4 = '',
         @PALONGID_5 = '',
         @PABBeginDate = '',
         @PABEndDate = '',
         @PAFBeginDate = '',
         @PAFEndDate = '',
         @PAACTUALBEGDATE = '',
         @PA_Actual_End_Date = '',
         @CNTCPRSN = '',
         @LOCATNID = '',
         @PASegmentCB = 0,
         @PAbillnoteidx = 0,
         @PAbillnoteidxts = 0,
         @PAELbillniteidx = 0,
         @PAbillnoteidxML = 0,
         @PAbillnoteidxvi = 0,
         @PAbillnoteidxee = 0,
         @PAbillnoteidxinv = 0,
         @PAbillnoteidxfee = 0,
         @PABQuantity = 0,
         @PABTotalCost = 0,
         @PABBillings = 0,
         @PABProfit = 0,
         @PABTaxPaidAmt = 0,
         @PABTaxChargedAmt = 0,
         @PABaselineOvhdCost = 0,
         @PAFQuantity = 0,
         @PAFTotalCost = 0,
         @PAFBillings = 0,
         @PAFProfit = 0,
         @PAFTaxPaidAmt = 0,
         @PAFTaxChargedAmt = 0,
         @PAForecastOvhdCost = 0,
         @PAProject_Amount = 0,
         @PAUnpostedQty = 0,
         @PAUnpostedTotalCostN = 0,
         @PAUnposted_Overhead = 0,
         @PAUnpostedProfitN = 0,
         @PAUnposted_Tax_Amount = 0,
         @PAUnpostAccrRevN = 0,
         @PAUnpostedCommitedQty = 0,
         @PAUnpostedCommitedCost = 0,
         @PAUnpostedCommitedTaxAmt = 0,
         @PAUnpostedRecogRevN = 0,
         @PAUnposted_Project_Fee = 0,
         @PAUnposted_Retainer_Fee = 0,
         @PAUnposted_Service_Fee = 0,
         @PAUNPOSTRETAMT = 0,
         @PAUNPOSTBIEEAMOUNT = 0,
         @PAUNPEIEBAMOUNT = 0,
         @PA_Unposted_Billed_Reten = 0,
         @PAPostedQty = 0,
         @PAPostedTotalCostN = 0,
         @PAPosted_Overhead = 0,
         @PAPostedProfitN = 0,
         @PAPosted_Tax_Amount = 0,
         @PAPosted_Accr_RevN = 0,
         @PAPostedCommitedQty = 0,
         @PAPostedCommitedCost = 0,
         @PAPostedCommitedTaxAmt = 0,
         @PAPostRecogRevN = 0,
         @PAPosted_Project_Fee = 0,
         @PAPosted_Retainer_Fee = 0,
         @PAPosted_Service_Fee = 0,
         @PAPOSTRETAMT = 0,
         @PAPOSBIEEAMOUNT = 0,
         @PAPOSEIEBAMOUNT = 0,
         @PA_Actual_Billed_Retenti = 0,
         @PAWrite_UpDown_Amount = 0,
         @PABilled_QtyN = 0,
         @PABilled_Cost = 0,
         @PABilled_Accrued_Revenu = 0,
         @PACostPcntCompleted = 0,
         @PAQuantityPcntCompleted = 0,
         @PA_Receipts_Amount = 0,
         @PA_Actual_Receipts_Amoun = 0,
         @PA_Earnings = 0,
         @PA_Cost_of_Earnings = 0,
         @PAUnpostBillN = 0,
         @PAUnpostDiscDolAmtN = 0,
         @PAUnposted_Sales_Tax_Am = 0,
         @PAPostedBillingsN = 0,
         @PAPostedDiscDolAmtN = 0,
         @PAPosted_Sales_Tax_Amou = 0,
         @PAService_Fee_Amount = 0,
         @PARetainer_Fee_Amount = 0,
         @PAProject_Fee_Amount = 0,
         @PARetentionFeeAmount = 0,
         @PABCWPAMT = 0,
         @PABCWSAMT = 0,
         @PAACWPAMT = 0,
         @NOTEINDX = 0,
         @PAApproved_Accrued_Reve = 0,
         @PAApproved_Cost = 0,
         @PAApproved_Quantity = 0,
         @WROFAMNT = 0,
         @ActualWriteOffAmount = 0,
         @DISTKNAM = 0,
         @ActualDiscTakenAmount = 0,
         @PACommitted_Costs = 0,
         @PACommitted_Qty = 0,
         @PAPOCost = 0,
         @PAPOQty = 0,
         @PAPOPostedCost = 0,
         @PAPOPostedQty = 0,
         @PAtaxpaidamt = 0,
         @PAPostedTaxPaidN = 0,
         @PApretainage = 0,
         @PAunpretainage = 0,
         @PA_Write_Off_Tax_Amount = 0,
         @PAActualWOTaxAmt = 0,
         @PA_Terms_Taken_Tax_Amt = 0,
         @PAActualTermsTakenTax = 0,
         @PAPosted_Earnings = 0,
         @PAActualCostofEarnings = 0,
         @PAUnpostedLossAmount = 0,
         @PAActualLossAmount = 0,
         @PAFQLaborOnly = 0,
         @PAAQLaborOnly = 0,
         @DBName = '',
         @O_iInitErrorState = 0,
         @oInitErrString = '',
         @SLPRSNID = '',
         @COMPRCNT = 0,
         @COMAPPTO = 0,
         @SALSTERR = '',
         @iGetNextNoteIdxErrState = 0,
         @sCompanyID = 0,
         @userdate = '',
         @iCalculateGLPeriodErrState = 0,
         @fClosed = 0,
         @YEAR1 = 0,
         @l_Period = 0,
         @nErr = 0,
         @Existing_CUSTNMBR = '',
         @Existing_PAcontid = '',
         @Existing_PAcontname = '',
         @Existing_PACONTNUMBER = '',
         @Existing_PAcontclassid = '',
         @Existing_PAProjectType = 0,
         @Existing_PAAcctgMethod = 0,
         @Existing_PASTAT = 0,
         @Existing_PApurordnum = '',
         @Existing_PAcloseProjcosts = 0,
         @Existing_PAclosetobillings = 0,
         @Existing_PAContMgrID = '',
         @Existing_PABusMgrID = '',
         @Existing_SLPRSNID = '',
         @Existing_SALSTERR = '',
         @Existing_COMPRCNT = 0,
         @Existing_COMAPPTO = 0,
         @Existing_PABILLFORMAT = '',
         @Existing_ADRSCODE = '',
         @Existing_PRBTADCD = '',
         @Existing_PAUD1_Cont = '',
         @Existing_PAUD2_Cont = '',
         @Existing_PASegmentCB = 0,
         @Cursor_PAPROJNUMBER = '',
         @fStatus = 0,
         @Update_Budget_Lines = 0,
         @Existing_PAbillnoteidxts = 0,
         @Existing_PAELbillniteidx = 0,
         @Existing_PAbillnoteidxML = 0,
         @Existing_PAbillnoteidxvi = 0,
         @Existing_PAbillnoteidxee = 0,
         @Existing_PAbillnoteidxinv = 0,
         @Existing_PAbillnoteidxfee = 0,
         @O_iErrorState = 0,
         @Existing_PA_RestrictCustomerList = 0,
         @Existing_DSCPCTAM = 0,
         @Existing_PASegmentCB = 0,
         @Existing_PABILLCYCLEID1 = '',
         @Existing_PATRKCHGORDS = 0,
         @Existing_PATRKCOBDGADDFLY = 0,
         @PAchangeorderdocounter = '',
         @PAMCProjectAmount = 0,
         @PAMCUnpostBillN = 0,
         @PAMCUnpostedSalesTax = 0,
         @PAMCUnpostedDiscount = 0,
         @PAMCActualBillings = 0,
         @PAMCActualSalesTaxAmt = 0,
         @PAMCActualDiscountAmt = 0,
         @PAMCBBillings = 0,
         @PAMCFBillings = 0,
         @PA_MC_Project_Fee_Amount = 0,
         @PA_MC_Retainer_Fee_Amt = 0,
         @PA_MC_Service_Fee_Amount = 0,
         @PA_MC_UnpostedProjectFee = 0,
         @PA_MC_Unposted_Retainer = 0,
         @PA_MC_UnpostedServiceFee = 0,
         @PA_MC_Actual_Project_Fee = 0,
         @PA_MC_Actual_RetainerFee = 0,
         @PA_MC_Actual_Service_Fee = 0,
         @CURRNIDX = 0,
         @Existing_PAMCCURNCYID = ''
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taPAContractsPre
    @I_vCUSTNMBR output ,
    @I_vPAcontid output ,
    @I_vPAcontname output ,
    @I_vPACONTNUMBER output ,
    @I_vPAcontclassid output ,
    @I_vPAProjectType output ,
    @I_vPAAcctgMethod output ,
    @I_vPAMCCURNCYID output ,
    @I_vPASTAT output ,
    @I_vPApurordnum output ,
    @I_vPAcloseProjcosts output ,
    @I_vPAclosetobillings output ,
    @I_vPAUD1_Cont output ,
    @I_vPAUD2_Cont output ,
    @I_vPAContMgrID output ,
    @I_vPABusMgrID output ,
    @I_vADRSCODE output ,
    @I_vSLPRSNID output ,
    @I_vSALSTERR output ,
    @I_vCOMPRCNT output ,
    @I_vCOMAPPTO output ,
    @I_vPABILLFORMAT output ,
    @I_vPRBTADCD output ,
    @I_vPA_RestrictCustomerList output ,
    @I_vDSCPCTAM output ,
    @I_vPASegmentCB output ,
    @I_vPABILLCYCLEID1 output ,
    @I_vPATRKCHGORDS output ,
    @I_vPATRKCOBDGADDFLY output ,
    @I_vPAbillnoteTS output ,
    @I_vPAbillnoteEL output ,
    @I_vPAbillnoteML output ,
    @I_vPAbillnoteVI output ,
    @I_vPAbillnoteEE output ,
    @I_vPAbillnoteINV output ,
    @I_vPAbillnoteFEE output ,
    @I_vUpdateExisting output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4945
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vCUSTNMBR is NULL 
       or @I_vPAcontid is NULL
       or @I_vPAcontname is NULL
       or @I_vPACONTNUMBER is NULL
       or @I_vPAProjectType is NULL
       or @I_vPAAcctgMethod is NULL)
    begin
      select @O_iErrorState = 4946
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vCUSTNMBR = ''
       or @I_vPAcontid = ''
       or @I_vPAcontname = ''
       or @I_vPACONTNUMBER = ''
       or @I_vPAProjectType = 0
       or @I_vPAAcctgMethod = 0
       or @I_vPASTAT = 0)
    begin
      select @O_iErrorState = 4947
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAProjectType < 0
       or @I_vPAAcctgMethod < 0
       or @I_vPASTAT < 0)
    begin
      select @O_iErrorState = 4948
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vCUSTNMBR = UPPER(@I_vCUSTNMBR),
         @I_vPAcontid = UPPER(@I_vPAcontid),
         @I_vPACONTNUMBER = UPPER(@I_vPACONTNUMBER),
         @I_vADRSCODE = UPPER(@I_vADRSCODE),
         @I_vSLPRSNID = UPPER(@I_vSLPRSNID),
         @I_vSALSTERR = UPPER(@I_vSALSTERR),
         @I_vPRBTADCD = UPPER(@I_vPRBTADCD)
  
  if (@I_vPAMCCURNCYID <> '')
    begin
      if not exists (select 1
                     from   DYNAMICS..MC40200 (nolock)
                     where  CURNCYID = @I_vPAMCCURNCYID)
        begin
          select @O_iErrorState = 5878
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          select @CURRNIDX = isnull(CURRNIDX,0)
          from   DYNAMICS..MC40200 (nolock)
          where  CURNCYID = @I_vPAMCCURNCYID
        end
    end
  
  if (@I_vUpdateExisting = 1)
    begin
      select @Existing_CUSTNMBR = CUSTNMBR,
             @Existing_PAcontid = PAcontid,
             @Existing_PAcontname = PAcontname,
             @Existing_PACONTNUMBER = PACONTNUMBER,
             @Existing_PAcontclassid = PAcontclassid,
             @Existing_PAProjectType = PAProjectType,
             @Existing_PAAcctgMethod = PAAcctgMethod,
             @Existing_PASTAT = PASTAT,
             @Existing_PApurordnum = PApurordnum,
             @Existing_PAcloseProjcosts = PAcloseProjcosts,
             @Existing_PAclosetobillings = PAclosetobillings,
             @Existing_PAContMgrID = PAContMgrID,
             @Existing_PABusMgrID = PABusMgrID,
             @Existing_SLPRSNID = SLPRSNID,
             @Existing_SALSTERR = SALSTERR,
             @Existing_COMPRCNT = COMPRCNT,
             @Existing_COMAPPTO = COMAPPTO,
             @Existing_PABILLFORMAT = PABILLFORMAT,
             @Existing_ADRSCODE = ADRSCODE,
             @Existing_PRBTADCD = PRBTADCD,
             @Existing_PAUD1_Cont = PAUD1_Cont,
             @Existing_PAUD2_Cont = PAUD2_Cont,
             @Existing_PASegmentCB = PASegmentCB,
             @Existing_PAbillnoteidxts = PAbillnoteidxts,
             @Existing_PAELbillniteidx = PAELbillniteidx,
             @Existing_PAbillnoteidxML = PAbillnoteidxML,
             @Existing_PAbillnoteidxvi = PAbillnoteidxvi,
             @Existing_PAbillnoteidxee = PAbillnoteidxee,
             @Existing_PAbillnoteidxinv = PAbillnoteidxinv,
             @Existing_PAbillnoteidxfee = PAbillnoteidxfee,
             @Existing_PA_RestrictCustomerList = PA_RestrictCustomerList,
             @Existing_DSCPCTAM = DSCPCTAM,
             @Existing_PASegmentCB = PASegmentCB,
             @Existing_PAMCCURNCYID = PAMCCURNCYID
      from   PA01101 (nolock)
      where  PACONTNUMBER = @I_vPACONTNUMBER
      
      select @Existing_PATRKCHGORDS = PATRKCHGORDS,
             @Existing_PATRKCOBDGADDFLY = PATRKCOBDGADDFLY
      from   PA01141 (nolock)
      where  PACONTNUMBER = @I_vPACONTNUMBER
      
      select @Existing_PABILLCYCLEID1 = isnull(PABILLCYCLEID1,'')
      from   PA02501 (nolock)
      where  PACONTNUMBER = @I_vPACONTNUMBER
      
      select @I_vPAcontclassid = case 
                                   when (@I_vPAcontclassid is null)
                                        and (@Existing_PACONTNUMBER <> '') then @Existing_PAcontclassid
                                   when (@I_vPAcontclassid is null)
                                        and (@Existing_PACONTNUMBER = '') then ''
                                   else @I_vPAcontclassid
                                 end,
             @I_vPASTAT = case 
                            when (@I_vPASTAT is null)
                                 and (@Existing_PACONTNUMBER <> '') then @Existing_PASTAT
                            when (@I_vPASTAT is null)
                                 and (@Existing_PACONTNUMBER = '') then 4
                            else @I_vPASTAT
                          end,
             @I_vPApurordnum = case 
                                 when (@I_vPApurordnum is null)
                                      and (@Existing_PACONTNUMBER <> '') then @Existing_PApurordnum
                                 when (@I_vPApurordnum is null)
                                      and (@Existing_PACONTNUMBER = '') then ''
                                 else @I_vPApurordnum
                               end,
             @I_vPAcloseProjcosts = case 
                                      when (@I_vPAcloseProjcosts is null)
                                           and (@Existing_PACONTNUMBER <> '') then @Existing_PAcloseProjcosts
                                      when (@I_vPAcloseProjcosts is null)
                                           and (@Existing_PACONTNUMBER = '') then 0
                                      else @I_vPAcloseProjcosts
                                    end,
             @I_vPAclosetobillings = case 
                                       when (@I_vPAclosetobillings is null)
                                            and (@Existing_PACONTNUMBER <> '') then @Existing_PAclosetobillings
                                       when (@I_vPAclosetobillings is null)
                                            and (@Existing_PACONTNUMBER = '') then 0
                                       else @I_vPAclosetobillings
                                     end,
             @I_vPAContMgrID = case 
                                 when (@I_vPAContMgrID is null)
                                      and (@Existing_PACONTNUMBER <> '') then @Existing_PAContMgrID
                                 when (@I_vPAContMgrID is null)
                                      and (@Existing_PACONTNUMBER = '') then ''
                                 else @I_vPAContMgrID
                               end,
             @I_vPABusMgrID = case 
                                when (@I_vPABusMgrID is null)
                                     and (@Existing_PACONTNUMBER <> '') then @Existing_PABusMgrID
                                when (@I_vPABusMgrID is null)
                                     and (@Existing_PACONTNUMBER = '') then ''
                                else @I_vPABusMgrID
                              end,
             @I_vSLPRSNID = case 
                              when (@I_vSLPRSNID is null)
                                   and (@Existing_PACONTNUMBER <> '') then @Existing_SLPRSNID
                              when (@I_vSLPRSNID is null)
                                   and (@Existing_PACONTNUMBER = '') then ''
                              else @I_vSLPRSNID
                            end,
             @I_vSALSTERR = case 
                              when (@I_vSALSTERR is null)
                                   and (@Existing_PACONTNUMBER <> '') then @Existing_SALSTERR
                              when (@I_vSALSTERR is null)
                                   and (@Existing_PACONTNUMBER = '') then ''
                              else @I_vSALSTERR
                            end,
             @I_vCOMPRCNT = case 
                              when (@I_vCOMPRCNT is null)
                                   and (@Existing_PACONTNUMBER <> '') then @Existing_COMPRCNT
                              when (@I_vCOMPRCNT is null)
                                   and (@Existing_PACONTNUMBER = '') then 0
                              else @I_vCOMPRCNT
                            end,
             @I_vCOMAPPTO = case 
                              when (@I_vCOMAPPTO is null)
                                   and (@Existing_PACONTNUMBER <> '') then @Existing_COMAPPTO
                              when (@I_vCOMAPPTO is null)
                                   and (@Existing_PACONTNUMBER = '') then 0
                              else @I_vCOMAPPTO
                            end,
             @I_vPABILLFORMAT = case 
                                  when (@I_vPABILLFORMAT is null)
                                       and (@Existing_PACONTNUMBER <> '') then @Existing_PABILLFORMAT
                                  when (@I_vPABILLFORMAT is null)
                                       and (@Existing_PACONTNUMBER = '') then ''
                                  else @I_vPABILLFORMAT
                                end,
             @I_vADRSCODE = case 
                              when (@I_vADRSCODE is null)
                                   and (@Existing_PACONTNUMBER <> '') then @Existing_ADRSCODE
                              when (@I_vADRSCODE is null)
                                   and (@Existing_PACONTNUMBER = '') then ''
                              else @I_vADRSCODE
                            end,
             @I_vPRBTADCD = case 
                              when (@I_vPRBTADCD is null)
                                   and (@Existing_PACONTNUMBER <> '') then @Existing_PRBTADCD
                              when (@I_vPRBTADCD is null)
                                   and (@Existing_PACONTNUMBER = '') then ''
                              else @I_vPRBTADCD
                            end,
             @I_vPAUD1_Cont = case 
                                when (@I_vPAUD1_Cont is null)
                                     and (@Existing_PACONTNUMBER <> '') then @Existing_PAUD1_Cont
                                when (@I_vPAUD1_Cont is null)
                                     and (@Existing_PACONTNUMBER = '') then ''
                                else @I_vPAUD1_Cont
                              end,
             @I_vPAUD2_Cont = case 
                                when (@I_vPAUD2_Cont is null)
                                     and (@Existing_PACONTNUMBER <> '') then @Existing_PAUD2_Cont
                                when (@I_vPAUD2_Cont is null)
                                     and (@Existing_PACONTNUMBER = '') then ''
                                else @I_vPAUD2_Cont
                              end,
             @I_vPASegmentCB = case 
                                 when (@I_vPASegmentCB is null)
                                      and (@Existing_PACONTNUMBER <> '') then @Existing_PASegmentCB
                                 when (@I_vPASegmentCB is null)
                                      and (@Existing_PACONTNUMBER = '') then 0
                                 else @I_vPASegmentCB
                               end,
             @I_vDSCPCTAM = case 
                              when (@I_vDSCPCTAM is null)
                                   and (@Existing_PACONTNUMBER <> '')
                                   and (@Existing_DSCPCTAM > 0) then (@Existing_DSCPCTAM / 100)
                              when (@I_vDSCPCTAM is null)
                                   and (@Existing_PACONTNUMBER <> '')
                                   and (@Existing_DSCPCTAM = 0) then 0
                              when (@I_vDSCPCTAM is null)
                                   and (@Existing_PACONTNUMBER = '') then 0
                              else @I_vDSCPCTAM
                            end,
             @I_vPABILLCYCLEID1 = case 
                                    when (@I_vPABILLCYCLEID1 is null)
                                         and (@Existing_PACONTNUMBER <> '') then @Existing_PABILLCYCLEID1
                                    when (@I_vPABILLCYCLEID1 is null)
                                         and (@Existing_PACONTNUMBER = '') then ''
                                    else @I_vPABILLCYCLEID1
                                  end,
             @I_vPATRKCHGORDS = case 
                                  when (@I_vPATRKCHGORDS is null)
                                       and (@Existing_PACONTNUMBER <> '') then @Existing_PATRKCHGORDS
                                  when (@I_vPATRKCHGORDS is null)
                                       and (@Existing_PACONTNUMBER = '') then 0
                                  else @I_vPATRKCHGORDS
                                end,
             @I_vPATRKCOBDGADDFLY = case 
                                      when (@I_vPATRKCOBDGADDFLY is null)
                                           and (@Existing_PACONTNUMBER <> '') then @Existing_PATRKCOBDGADDFLY
                                      when (@I_vPATRKCOBDGADDFLY is null)
                                           and (@Existing_PACONTNUMBER = '') then 0
                                      else @I_vPATRKCOBDGADDFLY
                                    end,
             @Existing_PA_RestrictCustomerList = case 
                                                   when (@I_vPA_RestrictCustomerList is null)
                                                        and (@Existing_PA_RestrictCustomerList <> '') then @Existing_PA_RestrictCustomerList
                                                   when (@I_vPA_RestrictCustomerList is null)
                                                        and (@Existing_PA_RestrictCustomerList = '') then 0
                                                   else @I_vPA_RestrictCustomerList
                                                 end
      
      if (@Existing_CUSTNMBR <> @I_vCUSTNMBR)
         and (@Existing_CUSTNMBR <> '')
        begin
          select @O_iErrorState = 6417
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@Existing_PAcontid <> @I_vPAcontid)
         and (@Existing_PAcontid <> '')
        begin
          select @O_iErrorState = 6418
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@Existing_PAMCCURNCYID <> @I_vPAMCCURNCYID)
         and (@Existing_PAMCCURNCYID <> '')
        begin
          select @O_iErrorState = 5879
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      if (@I_vPAcontclassid is null)
        select @I_vPAcontclassid = ''
      
      if (@I_vPAProjectType is null)
        select @I_vPAProjectType = 0
      
      if (@I_vPAAcctgMethod is null)
        select @I_vPAAcctgMethod = ''
      
      if (@I_vPASTAT is null)
        select @I_vPASTAT = 4
      
      if (@I_vPApurordnum is null)
        select @I_vPApurordnum = ''
      
      if (@I_vPAcloseProjcosts is null)
        select @I_vPAcloseProjcosts = 0
      
      if (@I_vPAclosetobillings is null)
        select @I_vPAclosetobillings = 0
      
      if (@I_vPAContMgrID is null)
        select @I_vPAContMgrID = ''
      
      if (@I_vPABusMgrID is null)
        select @I_vPABusMgrID = ''
      
      if (@I_vSLPRSNID is null)
        select @I_vSLPRSNID = ''
      
      if (@I_vSALSTERR is null)
        select @I_vSALSTERR = ''
      
      if (@I_vCOMPRCNT is null)
        select @I_vCOMPRCNT = 0
      
      if (@I_vCOMAPPTO is null)
        select @I_vCOMAPPTO = 0
      
      if (@I_vPABILLFORMAT is null)
        select @I_vPABILLFORMAT = ''
      
      if (@I_vADRSCODE is null)
        select @I_vADRSCODE = ''
      
      if (@I_vPRBTADCD is null)
        select @I_vPRBTADCD = ''
      
      if (@I_vPAUD1_Cont is null)
        select @I_vPAUD1_Cont = ''
      
      if (@I_vPAUD2_Cont is null)
        select @I_vPAUD2_Cont = ''
      
      if (@I_vPASegmentCB is null)
        select @I_vPASegmentCB = 0
      
      if (@I_vDSCPCTAM is null)
        select @I_vDSCPCTAM = 0
      
      if (@I_vPASegmentCB is null)
        select @I_vPASegmentCB = 0
      
      if (@I_vPATRKCHGORDS is null)
        select @I_vPATRKCHGORDS = 0
      
      if (@I_vPATRKCOBDGADDFLY is null)
        select @I_vPATRKCOBDGADDFLY = 0
      
      if (@I_vPABILLCYCLEID1 is null)
        select @I_vPABILLCYCLEID1 = ''
      
      if (@I_vPA_RestrictCustomerList is null)
        select @I_vPA_RestrictCustomerList = 0
      
      if exists (select 1
                 from   PA01101 (nolock)
                 where  PACONTNUMBER = @I_vPACONTNUMBER)
        begin
          select @O_iErrorState = 4949
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      select @PAchangeorderdocounter = rtrim(@I_vPACONTNUMBER) + '0000001'
    end
  
  if not exists (select 1
                 from   RM00101 (nolock)
                 where  CUSTNMBR = @I_vCUSTNMBR)
    begin
      select @O_iErrorState = 4950
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAcontclassid <> '')
    begin
      if not exists (select 1
                     from   PA41101 (nolock)
                     where  PAcontclassid = @I_vPAcontclassid)
        begin
          select @O_iErrorState = 4951
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAProjectType < 1)
      or (@I_vPAProjectType > 3)
    begin
      select @O_iErrorState = 4952
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAAcctgMethod < 1)
      or (@I_vPAAcctgMethod > 6)
    begin
      select @O_iErrorState = 4953
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAAcctgMethod < 1)
       or (@I_vPAAcctgMethod > 2))
     and (@I_vPAProjectType = 1)
    begin
      select @O_iErrorState = 5265
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAAcctgMethod < 3)
       or (@I_vPAAcctgMethod > 6))
     and (@I_vPAProjectType = 2)
    begin
      select @O_iErrorState = 5266
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAAcctgMethod < 3)
       or (@I_vPAAcctgMethod > 6))
     and (@I_vPAProjectType = 3)
    begin
      select @O_iErrorState = 5267
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPASTAT < 1)
      or (@I_vPASTAT > 5)
    begin
      select @O_iErrorState = 4954
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPASTAT <> 4)
     and (@Existing_PASTAT = 0)
    begin
      select @O_iErrorState = 4955
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vPASTAT = 3
    begin
      if exists (select 1
                 from   PA01201 (nolock)
                 where  @I_vPACONTNUMBER = @I_vPACONTNUMBER
                        and PASTAT <> 3)
        begin
          select @O_iErrorState = 6419
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAcloseProjcosts < 0)
      or (@I_vPAcloseProjcosts > 1)
    begin
      select @O_iErrorState = 4956
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAclosetobillings < 0)
      or (@I_vPAclosetobillings > 1)
    begin
      select @O_iErrorState = 4957
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPA_RestrictCustomerList < 0)
      or (@I_vPA_RestrictCustomerList > 1)
    begin
      select @O_iErrorState = 4958
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPASegmentCB < 0)
      or (@I_vPASegmentCB > 1)
    begin
      select @O_iErrorState = 4959
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAContMgrID <> '')
    begin
      if not exists (select 1
                     from   UPR00100 (nolock)
                     where  EMPLOYID = @I_vPAContMgrID)
        begin
          select @O_iErrorState = 4960
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        if not exists (select 1
                       from   PA00601 (nolock)
                       where  EMPLOYID = @I_vPAContMgrID
                              and PApmcb = 1)
          begin
            select @O_iErrorState = 4961
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @O_oErrorState output
            
            return (@O_iErrorState)
          end
    end
  
  if (@I_vPABusMgrID <> '')
    begin
      if not exists (select 1
                     from   UPR00100 (nolock)
                     where  EMPLOYID = @I_vPABusMgrID)
        begin
          select @O_iErrorState = 4962
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        if not exists (select 1
                       from   PA00601 (nolock)
                       where  EMPLOYID = @I_vPABusMgrID
                              and PAbmcb = 1)
          begin
            select @O_iErrorState = 4963
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @O_oErrorState output
            
            return (@O_iErrorState)
          end
    end
  
  if (@I_vADRSCODE <> '')
    begin
      if not exists (select 1
                     from   RM00102 (nolock)
                     where  CUSTNMBR = @I_vCUSTNMBR
                            and ADRSCODE = @I_vADRSCODE)
        begin
          select @O_iErrorState = 4964
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPRBTADCD <> '')
    begin
      if not exists (select 1
                     from   RM00102 (nolock)
                     where  CUSTNMBR = @I_vCUSTNMBR
                            and ADRSCODE = @I_vPRBTADCD)
        begin
          select @O_iErrorState = 4965
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vSLPRSNID <> '')
    begin
      select @SLPRSNID = SLPRSNID,
             @COMPRCNT = COMPRCNT,
             @COMAPPTO = @COMAPPTO,
             @SALSTERR = SALSTERR
      from   RM00301 (nolock)
      where  SLPRSNID = @I_vSLPRSNID
      
      if (@SLPRSNID = '')
        begin
          select @O_iErrorState = 4966
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if @I_vCOMPRCNT = 0
             and @COMPRCNT > 0
            select @I_vCOMPRCNT = @COMPRCNT
          
          if @I_vCOMAPPTO = 0
             and @COMAPPTO > 0
            select @I_vCOMAPPTO = @COMAPPTO
          
          if @I_vSALSTERR = ''
             and @SALSTERR <> ''
            select @I_vSALSTERR = @SALSTERR
        end
    end
  
  if (@I_vSALSTERR <> '')
    begin
      if not exists (select 1
                     from   RM00303 (nolock)
                     where  SALSTERR = @I_vSALSTERR)
        begin
          select @O_iErrorState = 4967
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPABILLFORMAT <> '')
    begin
      if not exists (select 1
                     from   PA43101 (nolock)
                     where  PA_Bill_Format_Number = @I_vPABILLFORMAT)
        begin
          select @O_iErrorState = 4968
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vDSCPCTAM < 0)
      or (@I_vDSCPCTAM > 100)
    begin
      select @O_iErrorState = 4969
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  else
    begin
      if (@I_vDSCPCTAM > 0)
        select @I_vDSCPCTAM = @I_vDSCPCTAM * 100
    end
  
  if (@I_vPABILLCYCLEID1 <> '')
    begin
      if not exists (select 1
                     from   PA02010 (nolock)
                     where  PABILLCYCLEID1 = @I_vPABILLCYCLEID1)
        begin
          select @O_iErrorState = 4970
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 (nolock)
  where  INTERID = db_name()
  
  exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
    @I_sCompanyID = @sCompanyID ,
    @I_iSQLSessionID = 0 ,
    @O_mNoteIndex = @NOTEINDX output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iGetNextNoteIdxErrState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + @iGetNextNoteIdxErrState
      
      select @O_iErrorState = 9351
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@Existing_PACONTNUMBER = '')
    begin
      exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
        @I_sCompanyID = @sCompanyID ,
        @I_iSQLSessionID = 0 ,
        @I_noteincrement = 7 ,
        @O_mNoteIndex = @PAbillnoteidx output ,
        @O_iErrorState = @iGetNextNoteIdxErrState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iGetNextNoteIdxErrState <> 0)
           or (@iError <> 0))
        begin
          select @O_iErrorState = 4998
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return @O_oErrorState
        end
      
      select @PAbillnoteidxts = @PAbillnoteidx + 1,
             @PAELbillniteidx = @PAbillnoteidx + 2,
             @PAbillnoteidxML = @PAbillnoteidx + 3,
             @PAbillnoteidxvi = @PAbillnoteidx + 4,
             @PAbillnoteidxee = @PAbillnoteidx + 5,
             @PAbillnoteidxinv = @PAbillnoteidx + 6,
             @PAbillnoteidxfee = @PAbillnoteidx + 7
    end
  else
    begin
      select @PAbillnoteidxts = @Existing_PAbillnoteidxts,
             @PAELbillniteidx = @Existing_PAELbillniteidx,
             @PAbillnoteidxML = @Existing_PAbillnoteidxML,
             @PAbillnoteidxvi = @Existing_PAbillnoteidxvi,
             @PAbillnoteidxee = @Existing_PAbillnoteidxee,
             @PAbillnoteidxinv = @Existing_PAbillnoteidxinv,
             @PAbillnoteidxfee = @Existing_PAbillnoteidxfee
    end
  
  select @userdate = getdate()
  
  execute @iStatus = glCalculateGLPeriod
    7 ,
    'Timesheet Entry' ,
    @userdate ,
    @userdate ,
    @l_Period output ,
    @fClosed output ,
    @YEAR1 output ,
    @nErr output ,
    @iCalculateGLPeriodErrState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCalculateGLPeriodErrState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCalculateGLPeriodErrState))
      
      select @O_iErrorState = 6291
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@fClosed = 1)
    begin
      select @O_iErrorState = 6292
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Contracts' ,
        @I_vINDEX1 = @I_vPACONTNUMBER ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 0 ,
        @O_iErrorState = @iCustomState output
      
      if ((@iStatus <> 0)
           or (@iCustomState <> 0)
           or (@iError <> 0))
        begin
          select @O_iErrorState = 4971
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@Existing_PACONTNUMBER = '')
    begin
      delete PA01601
      where  PAbillnoteidx = @PAbillnoteidxts
              or PAbillnoteidx = @PAELbillniteidx
              or PAbillnoteidx = @PAbillnoteidxML
              or PAbillnoteidx = @PAbillnoteidxvi
              or PAbillnoteidx = @PAbillnoteidxee
              or PAbillnoteidx = @PAbillnoteidxinv
              or PAbillnoteidx = @PAbillnoteidxfee
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6420
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteINV <> '')
     and (@I_vPAbillnoteINV <> NULL)
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidxinv,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteINV
      
      if @@error <> 0
        begin
          select @O_iErrorState = 4999
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteEE <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidxee,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteEE
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5250
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteVI <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidxvi,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteVI
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5251
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteML <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidxML,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteML
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5252
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteEL <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAELbillniteidx,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteEL
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5253
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteTS <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidxts,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteTS
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5254
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  delete PA02501
  where  PACONTNUMBER = @I_vPACONTNUMBER
  
  if @@error <> 0
    begin
      select @O_iErrorState = 6421
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPABILLCYCLEID1 <> '')
    begin
      insert into PA02501
                 (PACONTNUMBER,
                  PABILLCYCLEID1)
      select @I_vPACONTNUMBER,
             @I_vPABILLCYCLEID1
      
      if @@error <> 0
        begin
          select @O_iErrorState = 4972
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if not exists (select 1
                 from   PA01141 (nolock)
                 where  PACONTNUMBER = @I_vPACONTNUMBER)
    begin
      insert into PA01141
                 (PACONTNUMBER,
                  PAchangeorderdocounter,
                  PATRKCHGORDS,
                  PATRKCOBDGADDFLY,
                  PACOTRKBASELINE,
                  PATOTCHGORDAMT,
                  PANOPENDCO,
                  PANUMCO,
                  PACOBASEPROJAMT,
                  PACOBSLNFEEPROJAMT,
                  PAUNPRRTOTPROJAMT,
                  PAUNABSTOTBILL,
                  PAUNPBSTOTCST,
                  PAUNPBSLTOTPROF,
                  PAUNPBSTOTQTY,
                  PAUNAPFRTOTBILL,
                  PAUNAPPFRTOTCST,
                  PAUNFRCTOTPROF,
                  PAUNPFRTOTQTY,
                  PAPostedQty,
                  PAPostedTotalCostN,
                  PAPosted_Overhead,
                  PAPostedProfitN,
                  PAPosted_Tax_Amount,
                  PAPosted_Accr_RevN,
                  PAPostRecogRevN,
                  PAPOSBIEEAMOUNT,
                  PAPOSEIEBAMOUNT,
                  PApostbillamt,
                  PA_Actual_Receipts_Amoun,
                  PAPostedBillingsN,
                  PAPostedDiscDolAmtN,
                  PAPosted_Sales_Tax_Amou,
                  ActualWriteOffAmount,
                  ActualDiscTakenAmount,
                  PAPostedTaxPaidN,
                  PAPOSTRETAMT,
                  PAActualWOTaxAmt,
                  PAActualTermsTakenTax,
                  PAPosted_Project_Fee,
                  PAPosted_Retainer_Fee,
                  PAPosted_Service_Fee,
                  PAGBTRKCHG)
      select @I_vPACONTNUMBER,
             @PAchangeorderdocounter,
             @I_vPATRKCHGORDS,
             @I_vPATRKCOBDGADDFLY,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0
      
      if @@error <> 0
        begin
          select @O_iErrorState = 4974
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      update PA01141
      set    PATRKCHGORDS = @I_vPATRKCHGORDS,
             PATRKCOBDGADDFLY = @I_vPATRKCOBDGADDFLY
      where  PACONTNUMBER = @I_vPACONTNUMBER
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6422
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@Existing_PACONTNUMBER = '')
    begin
      insert into PA01111
                 (PACONTNUMBER,
                  CUSTNMBR)
      select @I_vPACONTNUMBER,
             @I_vCUSTNMBR
      
      if @@error <> 0
        begin
          select @O_iErrorState = 4973
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@Existing_PACONTNUMBER <> '')
     and (@I_vPASTAT <> @Existing_PASTAT)
    begin
      if @Existing_PASTAT = 4
          or @I_vPASTAT = 5
        begin
          if @I_vPASTAT = 5
            begin
              declare PA_Set_Project_Status_To_Contract_Status INSENSITIVE cursor  for
              select PAPROJNUMBER
              from   PA01201 (nolock)
              where  PACONTNUMBER = @I_vPACONTNUMBER
              
              open PA_Set_Project_Status_To_Contract_Status
              
              if (@@cursor_rows > 0)
                begin
                  fetch next from PA_Set_Project_Status_To_Contract_Status
                  into @Cursor_PAPROJNUMBER
                  
                  while (@@fetch_status <> -1)
                    begin
                      if exists (select 1
                                 from   PA12000 a (nolock)
                                 where  (a.PACHGORDNO in (select b.PACHGORDNO
                                                          from   PA12001 b (nolock)
                                                          where  b.PAPROJNUMBER = @Cursor_PAPROJNUMBER)
                                          or a.PACHGORDNO in (select c.PACHGORDNO
                                                              from   PA12101 c (nolock)
                                                              where  c.PAPROJNUMBER = @Cursor_PAPROJNUMBER)
                                          or a.PACHGORDNO in (select d.PACHGORDNO
                                                              from   PA12103 d (nolock)
                                                              where  d.PAPROJNUMBER = @Cursor_PAPROJNUMBER))
                                        and a.PACONTNUMBER = @I_vPACONTNUMBER
                                        and a.PADOCSTATUS < 5)
                        begin
                          select @fStatus = 1
                        end
                      
                      if (@fStatus = 0)
                          or (@I_vPASTAT <> 5)
                        begin
                          if @Update_Budget_Lines = 1
                            begin
                              UPDATE a
                              set    a.PASTAT = case 
                                                  when @I_vPASTAT <> 5
                                                       and (@fStatus = 0) then @I_vPASTAT
                                                  else a.PASTAT
                                                end,
                                     a.PAPreviouslyOpen = case 
                                                            when (@I_vPASTAT <> 5
                                                                  and @I_vPASTAT <> 4)
                                                                 and (@fStatus = 0) then 1
                                                            else a.PAPreviouslyOpen
                                                          end
                              FROM   PA01301 a
                                     INNER JOIN PA01201 c
                                       ON a.PAPROJNUMBER = c.PAPROJNUMBER
                                     INNER JOIN PA01101 b
                                       on b.PACONTNUMBER = c.PACONTNUMBER
                              where  (@I_vPASTAT = 5
                                      and a.PASTAT <> 3)
                                      or (b.PASTAT = 4
                                          and a.PASTAT = b.PASTAT
                                          and @I_vPASTAT <> 5)
                                      or (b.PASTAT = 4
                                          and a.PASTAT = b.PASTAT
                                          and @I_vPASTAT > 5)
                                         and b.PACONTNUMBER = @I_vPACONTNUMBER
                                         and a.PAPROJNUMBER = @Cursor_PAPROJNUMBER
                            end
                          
                          UPDATE a
                          set    a.PASTAT = case 
                                              when ((@I_vPASTAT = 5)
                                                    and (@fStatus = 0)
                                                    and a.PASTAT = 1) then @I_vPASTAT
                                              when ((@I_vPASTAT <> 5)
                                                    and (@fStatus = 0)
                                                    and a.PASTAT <> 1) then @I_vPASTAT
                                              when (((@fStatus = 0)
                                                      or (@I_vPASTAT <> 5))
                                                    and a.PASTAT <> 1) then @I_vPASTAT
                                              else a.PASTAT
                                            end
                          FROM   PA01201 a
                                 INNER JOIN PA01101 b
                                   on a.PACONTNUMBER = b.PACONTNUMBER
                          where  (@I_vPASTAT = 5
                                  and a.PASTAT <> 3)
                                  or (b.PASTAT = 4
                                      and a.PASTAT = b.PASTAT
                                      and @I_vPASTAT <> 5)
                                  or (b.PASTAT = 4
                                      and a.PASTAT = 1
                                      and @I_vPASTAT <> 5)
                                  or (b.PASTAT = 4
                                      and a.PASTAT = b.PASTAT
                                      and @I_vPASTAT > 5)
                                     and a.PACONTNUMBER = @I_vPACONTNUMBER
                                     and a.PAPROJNUMBER = @Cursor_PAPROJNUMBER
                        end
                      
                      fetch next from PA_Set_Project_Status_To_Contract_Status
                      into @Cursor_PAPROJNUMBER
                    end
                end
              
              deallocate PA_Set_Project_Status_To_Contract_Status
            end
          else
            begin
              if @I_vPASTAT = 1
                  or @I_vPASTAT = 2
                begin
                  UPDATE a
                  set    a.PASTAT = case 
                                      when a.PASTAT = 4 then @I_vPASTAT
                                      else a.PASTAT
                                    end,
                         a.PAPreviouslyOpen = case 
                                                when a.PASTAT = 4 then 1
                                                else a.PAPreviouslyOpen
                                              end
                  FROM   PA01301 a
                         join PA01201 b
                           on a.PAPROJNUMBER = b.PAPROJNUMBER
                         join PA01101 c
                           on c.PACONTNUMBER = b.PACONTNUMBER
                  where  c.PACONTNUMBER = @I_vPACONTNUMBER
                         and b.PASTAT = 4
                  
                  UPDATE a
                  set    a.PASTAT = case 
                                      when a.PASTAT = 4 then @I_vPASTAT
                                      else a.PASTAT
                                    end
                  FROM   PA01201 a
                         join PA01101 c
                           on c.PACONTNUMBER = a.PACONTNUMBER
                  where  c.PACONTNUMBER = @I_vPACONTNUMBER
                         and a.PASTAT = 4
                end
              else
                begin
                  select @Update_Budget_Lines = 1
                  
                  declare PA_Set_Project_Status_To_Contract_Status INSENSITIVE cursor  for
                  select PAPROJNUMBER
                  from   PA01201 (nolock)
                  where  PACONTNUMBER = @I_vPACONTNUMBER
                  
                  open PA_Set_Project_Status_To_Contract_Status
                  
                  if (@@cursor_rows > 0)
                    begin
                      fetch next from PA_Set_Project_Status_To_Contract_Status
                      into @Cursor_PAPROJNUMBER
                      
                      while (@@fetch_status <> -1)
                        begin
                          if exists (select 1
                                     from   PA12000 a (nolock)
                                     where  (a.PACHGORDNO in (select b.PACHGORDNO
                                                              from   PA12001 b (nolock)
                                                              where  b.PAPROJNUMBER = @Cursor_PAPROJNUMBER)
                                              or a.PACHGORDNO in (select c.PACHGORDNO
                                                                  from   PA12101 c (nolock)
                                                                  where  c.PAPROJNUMBER = @Cursor_PAPROJNUMBER)
                                              or a.PACHGORDNO in (select d.PACHGORDNO
                                                                  from   PA12103 d (nolock)
                                                                  where  d.PAPROJNUMBER = @Cursor_PAPROJNUMBER))
                                            and a.PACONTNUMBER = @I_vPACONTNUMBER
                                            and a.PADOCSTATUS < 5)
                            begin
                              select @fStatus = 1
                            end
                          
                          if (@fStatus = 0)
                              or (@I_vPASTAT <> 5)
                            begin
                              if @Update_Budget_Lines = 1
                                begin
                                  UPDATE a
                                  set    a.PASTAT = case 
                                                      when @I_vPASTAT <> 5
                                                           and (@fStatus = 0) then @I_vPASTAT
                                                      else a.PASTAT
                                                    end,
                                         a.PAPreviouslyOpen = case 
                                                                when (@I_vPASTAT <> 5
                                                                      and @I_vPASTAT <> 4)
                                                                     and (@fStatus = 0) then 1
                                                                else a.PAPreviouslyOpen
                                                              end
                                  FROM   PA01301 a
                                         INNER JOIN PA01201 c
                                           ON a.PAPROJNUMBER = c.PAPROJNUMBER
                                         INNER JOIN PA01101 b
                                           on b.PACONTNUMBER = c.PACONTNUMBER
                                  where  (@I_vPASTAT = 5
                                          and a.PASTAT <> 3)
                                          or (b.PASTAT = 4
                                              and a.PASTAT = b.PASTAT
                                              and @I_vPASTAT <> 5)
                                          or (b.PASTAT = 4
                                              and a.PASTAT = b.PASTAT
                                              and @I_vPASTAT > 5)
                                             and b.PACONTNUMBER = @I_vPACONTNUMBER
                                             and a.PAPROJNUMBER = @Cursor_PAPROJNUMBER
                                end
                              
                              UPDATE a
                              set    a.PASTAT = case 
                                                  when ((@I_vPASTAT = 5)
                                                        and (@fStatus = 0)
                                                        and a.PASTAT = 1) then @I_vPASTAT
                                                  when ((@I_vPASTAT <> 5)
                                                        and (@fStatus = 0)
                                                        and a.PASTAT <> 1) then @I_vPASTAT
                                                  when (((@fStatus = 0)
                                                          or (@I_vPASTAT <> 5))
                                                        and a.PASTAT <> 1) then @I_vPASTAT
                                                  else a.PASTAT
                                                end
                              FROM   PA01201 a
                                     INNER JOIN PA01101 b
                                       on a.PACONTNUMBER = b.PACONTNUMBER
                              where  (@I_vPASTAT = 5
                                      and a.PASTAT <> 3)
                                      or (b.PASTAT = 4
                                          and a.PASTAT = b.PASTAT
                                          and @I_vPASTAT <> 5)
                                      or (b.PASTAT = 4
                                          and a.PASTAT = 1
                                          and @I_vPASTAT <> 5)
                                      or (b.PASTAT = 4
                                          and a.PASTAT = b.PASTAT
                                          and @I_vPASTAT > 5)
                                         and a.PACONTNUMBER = @I_vPACONTNUMBER
                                         and a.PAPROJNUMBER = @Cursor_PAPROJNUMBER
                            end
                          
                          fetch next from PA_Set_Project_Status_To_Contract_Status
                          into @Cursor_PAPROJNUMBER
                        end
                    end
                  
                  deallocate PA_Set_Project_Status_To_Contract_Status
                end
            end
        end
      
      if @I_vPASTAT = 5
        begin
          declare PA_Set_Project_Status_To_Contract_Status INSENSITIVE cursor  for
          select PAPROJNUMBER
          from   PA01201 (nolock)
          where  PACONTNUMBER = @I_vPACONTNUMBER
          
          open PA_Set_Project_Status_To_Contract_Status
          
          if (@@cursor_rows > 0)
            begin
              fetch next from PA_Set_Project_Status_To_Contract_Status
              into @Cursor_PAPROJNUMBER
              
              while (@@fetch_status <> -1)
                begin
                  if exists (select 1
                             from   PA12000 a (nolock)
                             where  (a.PACHGORDNO in (select b.PACHGORDNO
                                                      from   PA12001 b (nolock)
                                                      where  b.PAPROJNUMBER = @Cursor_PAPROJNUMBER)
                                      or a.PACHGORDNO in (select c.PACHGORDNO
                                                          from   PA12101 c (nolock)
                                                          where  c.PAPROJNUMBER = @Cursor_PAPROJNUMBER)
                                      or a.PACHGORDNO in (select d.PACHGORDNO
                                                          from   PA12103 d (nolock)
                                                          where  d.PAPROJNUMBER = @Cursor_PAPROJNUMBER))
                                    and a.PACONTNUMBER = @I_vPACONTNUMBER
                                    and a.PADOCSTATUS < 5)
                    begin
                      select @fStatus = 1
                    end
                  
                  if (@fStatus = 0)
                      or (@I_vPASTAT <> 5)
                    begin
                      UPDATE a
                      set    a.PASTAT = case 
                                          when ((@I_vPASTAT = 5)
                                                and (@fStatus = 0)
                                                and a.PASTAT = 1) then @I_vPASTAT
                                          when ((@I_vPASTAT <> 5)
                                                and (@fStatus = 0)
                                                and a.PASTAT <> 1) then @I_vPASTAT
                                          when (((@fStatus = 0)
                                                  or (@I_vPASTAT <> 5))
                                                and a.PASTAT <> 1) then @I_vPASTAT
                                          else a.PASTAT
                                        end
                      FROM   PA01201 a
                             INNER JOIN PA01101 b
                               on a.PACONTNUMBER = b.PACONTNUMBER
                      where  (@I_vPASTAT = 5
                              and a.PASTAT <> 3)
                              or (b.PASTAT = 4
                                  and a.PASTAT = b.PASTAT
                                  and @I_vPASTAT <> 5)
                              or (b.PASTAT = 4
                                  and a.PASTAT = 1
                                  and @I_vPASTAT <> 5)
                              or (b.PASTAT = 4
                                  and a.PASTAT = b.PASTAT
                                  and @I_vPASTAT > 5)
                                 and a.PACONTNUMBER = @I_vPACONTNUMBER
                                 and a.PAPROJNUMBER = @Cursor_PAPROJNUMBER
                    end
                  
                  fetch next from PA_Set_Project_Status_To_Contract_Status
                  into @Cursor_PAPROJNUMBER
                end
            end
          
          deallocate PA_Set_Project_Status_To_Contract_Status
        end
    end
  
  if (@Existing_PACONTNUMBER = '')
    begin
      insert into PA01101
                 (CUSTNMBR,
                  PAcontid,
                  PALONGID_1,
                  PALONGID_2,
                  PALONGID_3,
                  PALONGID_4,
                  PALONGID_5,
                  PAcontname,
                  PACONTNUMBER,
                  PAcontclassid,
                  PAProjectType,
                  PAAcctgMethod,
                  PASTAT,
                  CURRNIDX,
                  PApurordnum,
                  PABBeginDate,
                  PABEndDate,
                  PAFBeginDate,
                  PAFEndDate,
                  PAACTUALBEGDATE,
                  PA_Actual_End_Date,
                  PAcloseProjcosts,
                  PAclosetobillings,
                  PAContMgrID,
                  PABusMgrID,
                  LOCATNID,
                  SLPRSNID,
                  SALSTERR,
                  COMPRCNT,
                  COMAPPTO,
                  PABILLFORMAT,
                  ADRSCODE,
                  CNTCPRSN,
                  PRBTADCD,
                  PAUD1_Cont,
                  PAUD2_Cont,
                  PASegmentCB,
                  PAbillnoteidx,
                  PAbillnoteidxts,
                  PAELbillniteidx,
                  PAbillnoteidxML,
                  PAbillnoteidxvi,
                  PAbillnoteidxee,
                  PAbillnoteidxinv,
                  PAbillnoteidxfee,
                  PABQuantity,
                  PABTotalCost,
                  PABBillings,
                  PABProfit,
                  PABTaxPaidAmt,
                  PABTaxChargedAmt,
                  PABaselineOvhdCost,
                  PAFQuantity,
                  PAFTotalCost,
                  PAFBillings,
                  PAFProfit,
                  PAFTaxPaidAmt,
                  PAFTaxChargedAmt,
                  PAForecastOvhdCost,
                  PAProject_Amount,
                  PAUnpostedQty,
                  PAUnpostedTotalCostN,
                  PAUnposted_Overhead,
                  PAUnpostedProfitN,
                  PAUnposted_Tax_Amount,
                  PAUnpostAccrRevN,
                  PAUnpostedCommitedQty,
                  PAUnpostedCommitedCost,
                  PAUnpostedCommitedTaxAmt,
                  PAUnpostedRecogRevN,
                  PAUnposted_Project_Fee,
                  PAUnposted_Retainer_Fee,
                  PAUnposted_Service_Fee,
                  PAUNPOSTRETAMT,
                  PAUNPOSTBIEEAMOUNT,
                  PAUNPEIEBAMOUNT,
                  PA_Unposted_Billed_Reten,
                  PAPostedQty,
                  PAPostedTotalCostN,
                  PAPosted_Overhead,
                  PAPostedProfitN,
                  PAPosted_Tax_Amount,
                  PAPosted_Accr_RevN,
                  PAPostedCommitedQty,
                  PAPostedCommitedCost,
                  PAPostedCommitedTaxAmt,
                  PAPostRecogRevN,
                  PAPosted_Project_Fee,
                  PAPosted_Retainer_Fee,
                  PAPosted_Service_Fee,
                  PAPOSTRETAMT,
                  PAPOSBIEEAMOUNT,
                  PAPOSEIEBAMOUNT,
                  PA_Actual_Billed_Retenti,
                  PAWrite_UpDown_Amount,
                  PABilled_QtyN,
                  PABilled_Cost,
                  PABilled_Accrued_Revenu,
                  PACostPcntCompleted,
                  PAQuantityPcntCompleted,
                  PA_Receipts_Amount,
                  PA_Actual_Receipts_Amoun,
                  PA_Earnings,
                  PA_Cost_of_Earnings,
                  PAUnpostBillN,
                  PAUnpostDiscDolAmtN,
                  PAUnposted_Sales_Tax_Am,
                  PAPostedBillingsN,
                  PAPostedDiscDolAmtN,
                  PAPosted_Sales_Tax_Amou,
                  PAService_Fee_Amount,
                  PARetainer_Fee_Amount,
                  PAProject_Fee_Amount,
                  PARetentionFeeAmount,
                  DSCPCTAM,
                  PABCWPAMT,
                  PABCWSAMT,
                  PAACWPAMT,
                  NOTEINDX,
                  PAApproved_Accrued_Reve,
                  PAApproved_Cost,
                  PAApproved_Quantity,
                  WROFAMNT,
                  ActualWriteOffAmount,
                  DISTKNAM,
                  ActualDiscTakenAmount,
                  PACommitted_Costs,
                  PACommitted_Qty,
                  PAPOCost,
                  PAPOQty,
                  PAPOPostedCost,
                  PAPOPostedQty,
                  PAtaxpaidamt,
                  PAPostedTaxPaidN,
                  PApretainage,
                  PAunpretainage,
                  PA_Write_Off_Tax_Amount,
                  PAActualWOTaxAmt,
                  PA_Terms_Taken_Tax_Amt,
                  PAActualTermsTakenTax,
                  PA_RestrictCustomerList,
                  PAPosted_Earnings,
                  PAActualCostofEarnings,
                  PAUnpostedLossAmount,
                  PAActualLossAmount,
                  PAFQLaborOnly,
                  PAAQLaborOnly,
                  PAMCCURNCYID,
                  PAMCProjectAmount,
                  PAMCUnpostBillN,
                  PAMCUnpostedSalesTax,
                  PAMCUnpostedDiscount,
                  PAMCActualBillings,
                  PAMCActualSalesTaxAmt,
                  PAMCActualDiscountAmt,
                  PAMCBBillings,
                  PAMCFBillings,
                  PA_MC_Project_Fee_Amount,
                  PA_MC_Retainer_Fee_Amt,
                  PA_MC_Service_Fee_Amount,
                  PA_MC_UnpostedProjectFee,
                  PA_MC_Unposted_Retainer,
                  PA_MC_UnpostedServiceFee,
                  PA_MC_Actual_Project_Fee,
                  PA_MC_Actual_RetainerFee,
                  PA_MC_Actual_Service_Fee)
      select @I_vCUSTNMBR,
             @I_vPAcontid,
             @PALONGID_1,
             @PALONGID_2,
             @PALONGID_3,
             @PALONGID_4,
             @PALONGID_5,
             @I_vPAcontname,
             @I_vPACONTNUMBER,
             @I_vPAcontclassid,
             @I_vPAProjectType,
             @I_vPAAcctgMethod,
             @I_vPASTAT,
             @CURRNIDX,
             @I_vPApurordnum,
             @PABBeginDate,
             @PABEndDate,
             @PAFBeginDate,
             @PAFEndDate,
             @PAACTUALBEGDATE,
             @PA_Actual_End_Date,
             @I_vPAcloseProjcosts,
             @I_vPAclosetobillings,
             @I_vPAContMgrID,
             @I_vPABusMgrID,
             @LOCATNID,
             @I_vSLPRSNID,
             @I_vSALSTERR,
             @I_vCOMPRCNT,
             @I_vCOMAPPTO,
             @I_vPABILLFORMAT,
             @I_vADRSCODE,
             @CNTCPRSN,
             @I_vPRBTADCD,
             @I_vPAUD1_Cont,
             @I_vPAUD2_Cont,
             @I_vPASegmentCB,
             @PAbillnoteidx,
             @PAbillnoteidxts,
             @PAELbillniteidx,
             @PAbillnoteidxML,
             @PAbillnoteidxvi,
             @PAbillnoteidxee,
             @PAbillnoteidxinv,
             @PAbillnoteidxfee,
             @PABQuantity,
             @PABTotalCost,
             @PABBillings,
             @PABProfit,
             @PABTaxPaidAmt,
             @PABTaxChargedAmt,
             @PABaselineOvhdCost,
             @PAFQuantity,
             @PAFTotalCost,
             @PAFBillings,
             @PAFProfit,
             @PAFTaxPaidAmt,
             @PAFTaxChargedAmt,
             @PAForecastOvhdCost,
             @PAProject_Amount,
             @PAUnpostedQty,
             @PAUnpostedTotalCostN,
             @PAUnposted_Overhead,
             @PAUnpostedProfitN,
             @PAUnposted_Tax_Amount,
             @PAUnpostAccrRevN,
             @PAUnpostedCommitedQty,
             @PAUnpostedCommitedCost,
             @PAUnpostedCommitedTaxAmt,
             @PAUnpostedRecogRevN,
             @PAUnposted_Project_Fee,
             @PAUnposted_Retainer_Fee,
             @PAUnposted_Service_Fee,
             @PAUNPOSTRETAMT,
             @PAUNPOSTBIEEAMOUNT,
             @PAUNPEIEBAMOUNT,
             @PA_Unposted_Billed_Reten,
             @PAPostedQty,
             @PAPostedTotalCostN,
             @PAPosted_Overhead,
             @PAPostedProfitN,
             @PAPosted_Tax_Amount,
             @PAPosted_Accr_RevN,
             @PAPostedCommitedQty,
             @PAPostedCommitedCost,
             @PAPostedCommitedTaxAmt,
             @PAPostRecogRevN,
             @PAPosted_Project_Fee,
             @PAPosted_Retainer_Fee,
             @PAPosted_Service_Fee,
             @PAPOSTRETAMT,
             @PAPOSBIEEAMOUNT,
             @PAPOSEIEBAMOUNT,
             @PA_Actual_Billed_Retenti,
             @PAWrite_UpDown_Amount,
             @PABilled_QtyN,
             @PABilled_Cost,
             @PABilled_Accrued_Revenu,
             @PACostPcntCompleted,
             @PAQuantityPcntCompleted,
             @PA_Receipts_Amount,
             @PA_Actual_Receipts_Amoun,
             @PA_Earnings,
             @PA_Cost_of_Earnings,
             @PAUnpostBillN,
             @PAUnpostDiscDolAmtN,
             @PAUnposted_Sales_Tax_Am,
             @PAPostedBillingsN,
             @PAPostedDiscDolAmtN,
             @PAPosted_Sales_Tax_Amou,
             @PAService_Fee_Amount,
             @PARetainer_Fee_Amount,
             @PAProject_Fee_Amount,
             @PARetentionFeeAmount,
             @I_vDSCPCTAM,
             @PABCWPAMT,
             @PABCWSAMT,
             @PAACWPAMT,
             @NOTEINDX,
             @PAApproved_Accrued_Reve,
             @PAApproved_Cost,
             @PAApproved_Quantity,
             @WROFAMNT,
             @ActualWriteOffAmount,
             @DISTKNAM,
             @ActualDiscTakenAmount,
             @PACommitted_Costs,
             @PACommitted_Qty,
             @PAPOCost,
             @PAPOQty,
             @PAPOPostedCost,
             @PAPOPostedQty,
             @PAtaxpaidamt,
             @PAPostedTaxPaidN,
             @PApretainage,
             @PAunpretainage,
             @PA_Write_Off_Tax_Amount,
             @PAActualWOTaxAmt,
             @PA_Terms_Taken_Tax_Amt,
             @PAActualTermsTakenTax,
             @I_vPA_RestrictCustomerList,
             @PAPosted_Earnings,
             @PAActualCostofEarnings,
             @PAUnpostedLossAmount,
             @PAActualLossAmount,
             @PAFQLaborOnly,
             @PAAQLaborOnly,
             @I_vPAMCCURNCYID,
             @PAMCProjectAmount,
             @PAMCUnpostBillN,
             @PAMCUnpostedSalesTax,
             @PAMCUnpostedDiscount,
             @PAMCActualBillings,
             @PAMCActualSalesTaxAmt,
             @PAMCActualDiscountAmt,
             @PAMCBBillings,
             @PAMCFBillings,
             @PA_MC_Project_Fee_Amount,
             @PA_MC_Retainer_Fee_Amt,
             @PA_MC_Service_Fee_Amount,
             @PA_MC_UnpostedProjectFee,
             @PA_MC_Unposted_Retainer,
             @PA_MC_UnpostedServiceFee,
             @PA_MC_Actual_Project_Fee,
             @PA_MC_Actual_RetainerFee,
             @PA_MC_Actual_Service_Fee
      
      if @@error <> 0
        begin
          select @O_iErrorState = 4975
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      update PA01101
      set    PAcontname = @I_vPAcontname,
             PAcontclassid = @I_vPAcontclassid,
             PAProjectType = @I_vPAProjectType,
             PAAcctgMethod = @I_vPAAcctgMethod,
             PASTAT = @I_vPASTAT,
             PApurordnum = @I_vPApurordnum,
             PAcloseProjcosts = @I_vPAcloseProjcosts,
             PAclosetobillings = @I_vPAclosetobillings,
             PAContMgrID = @I_vPAContMgrID,
             PABusMgrID = @I_vPABusMgrID,
             SLPRSNID = @I_vSLPRSNID,
             SALSTERR = @I_vSALSTERR,
             COMPRCNT = @I_vCOMPRCNT,
             COMAPPTO = @I_vCOMAPPTO,
             PABILLFORMAT = @I_vPABILLFORMAT,
             ADRSCODE = @I_vADRSCODE,
             CNTCPRSN = @CNTCPRSN,
             PRBTADCD = @I_vPRBTADCD,
             PAUD1_Cont = @I_vPAUD1_Cont,
             PAUD2_Cont = @I_vPAUD2_Cont,
             PASegmentCB = @I_vPASegmentCB,
             DSCPCTAM = @I_vDSCPCTAM,
             PA_RestrictCustomerList = @I_vPA_RestrictCustomerList
      where  PACONTNUMBER = @I_vPACONTNUMBER
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6423
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  exec @iStatus = taPAContractsPost
    @I_vCUSTNMBR ,
    @I_vPAcontid ,
    @I_vPAcontname ,
    @I_vPACONTNUMBER ,
    @I_vPAcontclassid ,
    @I_vPAProjectType ,
    @I_vPAAcctgMethod ,
    @I_vPAMCCURNCYID ,
    @I_vPASTAT ,
    @I_vPApurordnum ,
    @I_vPAcloseProjcosts ,
    @I_vPAclosetobillings ,
    @I_vPAUD1_Cont ,
    @I_vPAUD2_Cont ,
    @I_vPAContMgrID ,
    @I_vPABusMgrID ,
    @I_vADRSCODE ,
    @I_vSLPRSNID ,
    @I_vSALSTERR ,
    @I_vCOMPRCNT ,
    @I_vCOMAPPTO ,
    @I_vPABILLFORMAT ,
    @I_vPRBTADCD ,
    @I_vPA_RestrictCustomerList ,
    @I_vDSCPCTAM ,
    @I_vPASegmentCB ,
    @I_vPABILLCYCLEID1 ,
    @I_vPATRKCHGORDS ,
    @I_vPATRKCOBDGADDFLY ,
    @I_vPAbillnoteTS ,
    @I_vPAbillnoteEL ,
    @I_vPAbillnoteML ,
    @I_vPAbillnoteVI ,
    @I_vPAbillnoteEE ,
    @I_vPAbillnoteINV ,
    @I_vPAbillnoteFEE ,
    @I_vUpdateExisting ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4976
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Contracts' ,
        @I_vINDEX1 = @I_vPACONTNUMBER ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 1 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 4977
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  return (@O_iErrorState)

