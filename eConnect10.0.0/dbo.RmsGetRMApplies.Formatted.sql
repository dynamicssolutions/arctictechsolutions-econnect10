

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetRMApplies]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetRMApplies]
  
create procedure [dbo].[RmsGetRMApplies]
                @iRMPaymentApplyTempTable    VARCHAR(255),
                @iRMDepositApplyTempTable    VARCHAR(255),
                @iRMArAdjApplyTempTable      VARCHAR(255),
                @iRMReturnVoidApplyTempTable VARCHAR(255),
                @iStartDate                  DATETIME,
                @iEndDate                    DATETIME,
                @iRmsServerName              VARCHAR(255),
                @iHqDbName                   VARCHAR(255)  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lAccountReceivableHistoryTable VARCHAR(255)
  
  DECLARE  @lAccountReceivableTable VARCHAR(255)
  
  DECLARE  @lOrderHistoryTable VARCHAR(255)
  
  DECLARE  @lOrderTable VARCHAR(255)
  
  DECLARE  @lApplyFromTable VARCHAR(255)
  
  DECLARE  @lTransactionTable VARCHAR(255)
  
  DECLARE  @lTransactionEntryTable VARCHAR(255)
  
  DECLARE  @lTaxEntryTable VARCHAR(255)
  
  SELECT @lAccountReceivableHistoryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.AccountReceivableHistory as AccountReceivableHistory'
  
  SELECT @lAccountReceivableTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.AccountReceivable as AccountReceivable'
  
  SELECT @lOrderHistoryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.OrderHistory as OrderHistory'
  
  SELECT @lOrderTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.[Order] as Orders'
  
  SELECT @lApplyFromTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.OrderHistory as ApplyFromTable'
  
  SELECT @lTransactionTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.[Transaction] as Transactions'
  
  SELECT @lTransactionEntryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.TransactionEntry as TransactionEntry'
  
  SELECT @lTaxEntryTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.TaxEntry as TaxEntry'
  
  EXEC( 'INSERT INTO ' + @iRMPaymentApplyTempTable + ' (RmsStoreID,  RmsArHistID,  Total_Sales,  RmsApplyToID,  RmsApplyFromID,  RmsApplyDate)  SELECT  AccountReceivableHistory.StoreID,  AccountReceivableHistory.[ID],  case   when   abs(AccountReceivableHistory.Amount) <= abs(AccountReceivable.OriginalAmount)  then  AccountReceivableHistory.Amount  when  abs(AccountReceivableHistory.Amount) > abs(AccountReceivable.OriginalAmount)   then  AccountReceivable.OriginalAmount  end,  AccountReceivable.TransactionNumber,  AccountReceivableHistory.PaymentID,  CONVERT(DATETIME,CONVERT(VARCHAR,AccountReceivableHistory.[Date],111))   FROM ' + @lAccountReceivableHistoryTable + ',' + @lAccountReceivableTable + ' WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,AccountReceivableHistory.[Date],111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  AccountReceivableHistory.AccountReceivableID = AccountReceivable.[ID]  AND  AccountReceivableHistory.StoreID = AccountReceivable.StoreID  AND  AccountReceivableHistory.HistoryType = 2  AND  ((AccountReceivable.Type = 2 AND OriginalAmount > 0) OR AccountReceivable.Type <> 2)  AND  NOT EXISTS(SELECT  1  FROM  RMS31200  WHERE  AccountReceivableHistory.StoreID = RMS31200.RmsStoreID  AND  AccountReceivableHistory.[ID] = RMS31200.RmsArHistID)')
  
  EXEC( 'INSERT INTO ' + @iRMDepositApplyTempTable + ' (RmsStoreID,  RmsOrderHistoryID,  Total_Sales,  RmsApplyToID,  RmsApplyFromID,  RmsApplyDate)  SELECT  OrderHistory.StoreID,  OrderHistory.[ID],  case   when   abs(ApplyFromTable.DeltaDeposit) <= abs(OrderHistory.DeltaDeposit)  then  ApplyFromTable.DeltaDeposit  when  abs(ApplyFromTable.DeltaDeposit) > abs(OrderHistory.DeltaDeposit)   then  OrderHistory.DeltaDeposit  end,  OrderHistory.TransactionNumber,  ApplyFromTable.[ID],  CONVERT(DATETIME,CONVERT(VARCHAR,OrderHistory.[Date],111))   FROM ' + @lOrderHistoryTable + ',' + @lApplyFromTable + ' WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,OrderHistory.[Date],111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  OrderHistory.StoreID = ApplyFromTable.StoreID  AND  OrderHistory.OrderID = ApplyFromTable.OrderID  AND  OrderHistory.[ID] <> ApplyFromTable.[ID]  AND  OrderHistory.DeltaDeposit < 0  AND  ApplyFromTable.DeltaDeposit > 0  AND  NOT EXISTS(SELECT  1  FROM  RMS31100  WHERE  OrderHistory.StoreID = RMS31100.RmsStoreID  AND  OrderHistory.[ID] = RMS31100.RmsOrderHistoryID)')
  
  EXEC( 'INSERT INTO ' + @iRMDepositApplyTempTable + ' (RmsStoreID,  RmsOrderHistoryID,  Total_Sales,  RmsApplyToID,  RmsApplyFromID,  RmsApplyDate)  SELECT  OrderHistory.StoreID,  OrderHistory.[ID],  case   when   abs(OrderHistory.DeltaDeposit) <= abs(Transactions.Total)  then  OrderHistory.DeltaDeposit  when  abs(OrderHistory.DeltaDeposit) > abs(Transactions.Total)   then  Transactions.Total  end,  OrderHistory.TransactionNumber,  OrderHistory.[ID],  CONVERT(DATETIME,CONVERT(VARCHAR,OrderHistory.[Date],111))   FROM ' + @lOrderHistoryTable + ',' + @lTransactionTable + ' WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,OrderHistory.[Date],111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  OrderHistory.TransactionNumber = Transactions.TransactionNumber  AND  OrderHistory.StoreID = Transactions.StoreID  AND  OrderHistory.DeltaDeposit > 0  AND  OrderHistory.TransactionNumber <> 0  AND  NOT EXISTS(SELECT  1  FROM  RMS31100  WHERE  OrderHistory.StoreID = RMS31100.RmsStoreID  AND  OrderHistory.[ID] = RMS31100.RmsOrderHistoryID)')
  
  EXEC( 'INSERT INTO ' + @iRMArAdjApplyTempTable + ' (RmsStoreID,  RmsArHistID,  Total_Sales,  RmsApplyToID,  RmsApplyFromID,  RmsApplyDate,  APTODCTY,  RmsApplyType)  SELECT  AccountReceivableHistory.StoreID,  AccountReceivableHistory.[ID],  case   when   abs(AccountReceivableHistory.Amount) <= abs(AccountReceivable.OriginalAmount)  then  AccountReceivableHistory.Amount  when  abs(AccountReceivableHistory.Amount) > abs(AccountReceivable.OriginalAmount)   then  AccountReceivable.OriginalAmount  end,  AccountReceivable.TransactionNumber,  AccountReceivableHistory.[ID],  CONVERT(DATETIME,CONVERT(VARCHAR,AccountReceivableHistory.[Date],111)),  CASE  WHEN  AccountReceivable.TransactionNumber > 0 AND AccountReceivable.OriginalAmount < 0  THEN  8  WHEN  AccountReceivable.TransactionNumber > 0 AND AccountReceivable.OriginalAmount > 0  THEN  1  WHEN  AccountReceivable.TransactionNumber = 0 AND AccountReceivable.OriginalAmount < 0  THEN  7  WHEN  AccountReceivable.TransactionNumber = 0 AND AccountReceivable.OriginalAmount > 0  THEN  3  END,  1   FROM ' + @lAccountReceivableHistoryTable + ',' + @lAccountReceivableTable + ' WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,AccountReceivableHistory.[Date],111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  AccountReceivableHistory.AccountReceivableID = AccountReceivable.[ID]  AND  AccountReceivableHistory.StoreID = AccountReceivable.StoreID  AND  (AccountReceivableHistory.HistoryType in (5,6)  AND  AccountReceivable.Type in (0,1))  AND  NOT EXISTS(SELECT  1  FROM  RMS32100  WHERE  AccountReceivableHistory.StoreID = RMS32100.RmsStoreID  AND  AccountReceivableHistory.[ID] = RMS32100.RmsArHistID)')
  
  EXEC( 'INSERT INTO ' + @iRMArAdjApplyTempTable + ' (RmsStoreID,  RmsArHistID,  Total_Sales,  RmsApplyToID,  RmsApplyFromID,  RmsApplyDate,  APTODCTY,  RmsApplyType)  SELECT  AccountReceivableHistory.StoreID,  AccountReceivableHistory.[ID],  case   when   abs(AccountReceivableHistory.Amount) <= abs(AccountReceivable.OriginalAmount)  then  AccountReceivableHistory.Amount  when  abs(AccountReceivableHistory.Amount) > abs(AccountReceivable.OriginalAmount)   then  AccountReceivable.OriginalAmount  end,  AccountReceivable.[ID],  AccountReceivableHistory.[ID],  CONVERT(DATETIME,CONVERT(VARCHAR,AccountReceivableHistory.[Date],111)),  CASE  WHEN  AccountReceivable.TransactionNumber > 0 AND AccountReceivable.OriginalAmount < 0  THEN  8  WHEN  AccountReceivable.TransactionNumber > 0 AND AccountReceivable.OriginalAmount > 0  THEN  1  WHEN  AccountReceivable.TransactionNumber = 0 AND AccountReceivable.OriginalAmount < 0  THEN  7  WHEN  AccountReceivable.TransactionNumber = 0 AND AccountReceivable.OriginalAmount > 0  THEN  3  END,  0   FROM ' + @lAccountReceivableHistoryTable + ',' + @lAccountReceivableTable + ' WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,AccountReceivableHistory.[Date],111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  AccountReceivableHistory.AccountReceivableID = AccountReceivable.[ID]  AND  AccountReceivableHistory.StoreID = AccountReceivable.StoreID  AND  (AccountReceivableHistory.HistoryType in (5,6)  AND  AccountReceivable.Type = 2  AND  (AccountReceivableHistory.Date <> AccountReceivable.Date OR AccountReceivableHistory.Amount <> AccountReceivable.OriginalAmount))  AND  NOT EXISTS(SELECT  1  FROM  RMS32100  WHERE  AccountReceivableHistory.StoreID = RMS32100.RmsStoreID  AND  AccountReceivableHistory.[ID] = RMS32100.RmsArHistID)')
  
  EXEC( 'INSERT INTO ' + @iRMReturnVoidApplyTempTable + ' (RmsTransactionNumber,  RmsStoreID,  Total_Sales,  RmsApplyFromID,  RmsApplyToID,  RmsApplyDate)  SELECT  Transactions.TransactionNumber,  Transactions.StoreID,  SUM(Price * Quantity),  Transactions.TransactionNumber,  Transactions.TransactionNumber,  CONVERT(DATETIME,CONVERT(VARCHAR,Transactions.Time,111))  FROM ' + @lTransactionTable + ' JOIN ' + @lTransactionEntryTable + ' ON ' + ' Transactions.TransactionNumber = TransactionEntry.TransactionNumber  AND  Transactions.StoreID = TransactionEntry.StoreID  WHERE  CONVERT(DATETIME,CONVERT(VARCHAR,Transactions.Time,111)) BETWEEN ' + '''' + @iStartDate + '''' + ' AND ' + '''' + @iEndDate + '''' + ' AND  TransactionEntry.Quantity < 0  AND  NOT EXISTS(SELECT  1  FROM  RMS31300  WHERE  Transactions.TransactionNumber = RMS31300.RmsTransactionNumber  AND  Transactions.StoreID = RMS31300.RmsStoreID)  GROUP BY   Transactions.TransactionNumber,  Transactions.StoreID,  Transactions.Time')
  
  EXEC( 'Update ' + @iRMReturnVoidApplyTempTable + ' SET  Total_Sales = Total_Sales + ISNULL((SELECT  SUM(ROUND(Tax,2))  FROM ' + @lTaxEntryTable + ' WHERE  TaxEntry.TransactionNumber = ' + @iRMReturnVoidApplyTempTable + '.RmsTransactionNumber  AND  TaxEntry.StoreID = ' + @iRMReturnVoidApplyTempTable + '.RmsStoreID  AND  TaxEntry.Tax < 0  GROUP BY  TaxEntry.StoreID,  TaxEntry.TransactionNumber),0)')

