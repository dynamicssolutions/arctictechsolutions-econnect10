

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAChangeOrderEntryFeeHeader]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAChangeOrderEntryFeeHeader]
  
create procedure [dbo].[taPAChangeOrderEntryFeeHeader]
                @I_vPACHGORDNO      char(17),
                @I_vPAPROJNUMBER    char(15),
                @I_vPALineItemSeq   smallint,
                @I_vPAbegindate     datetime,
                @I_vPAEnDate        datetime,
                @I_vPATOTPRJFEEAMT  numeric(19,5)  = 0,
                @I_vPATOTRETNFEEAMT numeric(19,5)  = 0,
                @I_vPATOTRETNTNAMT  numeric(19,5)  = 0,
                @I_vPATOTSRVCFEEAMT numeric(19,5)  = 0,
                @I_vRequesterTrx    smallint  = 0,
                @I_vUSRDEFND1       char(50)  = '',
                @I_vUSRDEFND2       char(50)  = '',
                @I_vUSRDEFND3       char(50)  = '',
                @I_vUSRDEFND4       varchar(8000)  = '',
                @I_vUSRDEFND5       varchar(8000)  = '',
                @O_iErrorState      int  output,
                @oErrString         varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iStatus               int,
           @iError                int,
           @PAPROJNUMBER          char(15),
           @PATRKCHGORDS          smallint,
           @PACONTNUMBER          char(11),
           @PATOTPRJFEEAMT        numeric(19,5),
           @PATOTRETNFEEAMT       numeric(19,5),
           @PATOTSRVCFEEAMT       numeric(19,5),
           @PATOTRETNTNAMT        numeric(19,5),
           @O_oErrorState         int,
           @iCustomState          int,
           @PAVARTOTFEEAMT        numeric(19,5),
           @PAsequencenumber      int,
           @PAClose_CB            smallint,
           @PAVARPRJFEEAMT        numeric(19,5),
           @PAVARRETFEEAMT        numeric(19,5),
           @PAVARETNFEEAMT        numeric(19,5),
           @PAVARSERVFEEAMT       numeric(19,5),
           @PAPosted_Project_Fee  numeric(19,5),
           @PAPosted_Retainer_Fee numeric(19,5),
           @PAPOSTRETAMT          numeric(19,5),
           @PAPosted_Service_Fee  numeric(19,5)
  
  select @iStatus = 0,
         @iError = 0,
         @PAPROJNUMBER = '',
         @PATRKCHGORDS = 0,
         @PACONTNUMBER = '',
         @PATOTPRJFEEAMT = 0,
         @PATOTRETNFEEAMT = 0,
         @PATOTSRVCFEEAMT = 0,
         @PATOTRETNTNAMT = 0,
         @O_oErrorState = 0,
         @iCustomState = 0,
         @PAVARTOTFEEAMT = 0,
         @PAsequencenumber = 0,
         @PAClose_CB = 0,
         @PAVARPRJFEEAMT = 0,
         @PAVARRETFEEAMT = 0,
         @PAVARETNFEEAMT = 0,
         @PAVARSERVFEEAMT = 0,
         @PAPosted_Project_Fee = 0,
         @PAPosted_Retainer_Fee = 0,
         @PAPOSTRETAMT = 0,
         @PAPosted_Service_Fee = 0,
         @O_iErrorState = 0
  
  exec @iStatus = taPAChangeOrderEntryFeeHeaderPre
    @I_vPACHGORDNO output ,
    @I_vPAPROJNUMBER output ,
    @I_vPALineItemSeq output ,
    @I_vPAbegindate output ,
    @I_vPAEnDate output ,
    @I_vPATOTPRJFEEAMT output ,
    @I_vPATOTRETNFEEAMT output ,
    @I_vPATOTRETNTNAMT output ,
    @I_vPATOTSRVCFEEAMT output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@oErrString))
      
      select @O_iErrorState = 2437
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPACHGORDNO is null 
       or @I_vPAPROJNUMBER is null
       or @I_vPALineItemSeq is null
       or @I_vPAbegindate is null
       or @I_vPAEnDate is null
       or @I_vPATOTPRJFEEAMT is null
       or @I_vPATOTRETNFEEAMT is null
       or @I_vPATOTRETNTNAMT is null
       or @I_vPATOTSRVCFEEAMT is null)
    begin
      select @O_iErrorState = 2438
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPACHGORDNO = ''
       or @I_vPAPROJNUMBER = '')
    begin
      select @O_iErrorState = 2439
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vPACHGORDNO = upper(@I_vPACHGORDNO),
         @I_vPAPROJNUMBER = upper(@I_vPAPROJNUMBER)
  
  if (@I_vPAPROJNUMBER <> '')
    begin
      select @PAPROJNUMBER = PAPROJNUMBER,
             @PACONTNUMBER = PACONTNUMBER
      from   PA01201 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@PAPROJNUMBER = '')
        begin
          select @O_iErrorState = 2440
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @PAPROJNUMBER = PAPROJNUMBER,
             @PATRKCHGORDS = PATRKCHGORDS
      from   PA01241 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
    end
  
  if exists (select 1
             from   PA12101 (nolock)
             where  PAPROJNUMBER = @I_vPAPROJNUMBER
                    and PACONTNUMBER = @PACONTNUMBER
                    and PACHGORDNO = @I_vPACHGORDNO)
    begin
      select @O_iErrorState = 2441
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @PATOTPRJFEEAMT = isnull(sum(PAFeeAmount),0)
  from   PA12102 (nolock)
  where  PAPROJNUMBER = @I_vPAPROJNUMBER
         and PACONTNUMBER = @PACONTNUMBER
         and PACHGORDNO = @I_vPACHGORDNO
         and PAFeeType = 1
  
  if (@I_vPATOTPRJFEEAMT <> @PATOTPRJFEEAMT)
    begin
      select @O_iErrorState = 2442
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @PATOTRETNFEEAMT = isnull(sum(PAFeeAmount),0)
  from   PA12102 (nolock)
  where  PAPROJNUMBER = @I_vPAPROJNUMBER
         and PACONTNUMBER = @PACONTNUMBER
         and PACHGORDNO = @I_vPACHGORDNO
         and PAFeeType = 2
  
  if (@I_vPATOTRETNFEEAMT <> @PATOTRETNFEEAMT)
    begin
      select @O_iErrorState = 2443
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @PATOTRETNTNAMT = isnull(sum(PAFeeAmount),0)
  from   PA12102 (nolock)
  where  PAPROJNUMBER = @I_vPAPROJNUMBER
         and PACONTNUMBER = @PACONTNUMBER
         and PACHGORDNO = @I_vPACHGORDNO
         and PAFeeType = 3
  
  if (@I_vPATOTRETNTNAMT <> @PATOTRETNTNAMT)
    begin
      select @O_iErrorState = 2444
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @PATOTSRVCFEEAMT = isnull(sum(PAFeeAmount),0)
  from   PA12102 (nolock)
  where  PAPROJNUMBER = @I_vPAPROJNUMBER
         and PACONTNUMBER = @PACONTNUMBER
         and PACHGORDNO = @I_vPACHGORDNO
         and PAFeeType = 4
  
  if (@I_vPATOTSRVCFEEAMT <> @PATOTSRVCFEEAMT)
    begin
      select @O_iErrorState = 2445
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  if not exists (select 1
                 from   PA12001 (nolock)
                 where  PACONTNUMBER = @PACONTNUMBER
                        and PAPROJNUMBER = @I_vPAPROJNUMBER
                        and PACHGORDNO = @I_vPACHGORDNO)
     and not exists (select 1
                     from   PA12101 (nolock)
                     where  PACONTNUMBER = @PACONTNUMBER
                            and PAPROJNUMBER = @I_vPAPROJNUMBER
                            and PACHGORDNO = @I_vPACHGORDNO)
    begin
      update PA01241
      set    PANOPENDCO = PANOPENDCO + 1,
             PANUMCO = PANUMCO + 1
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 5396
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_oErrorState)
        end
    end
  
  insert into PA12101
             (PACONTNUMBER,
              PACHGORDNO,
              PAPROJNUMBER,
              PALineItemSeq,
              PAVARTOTFEEAMT,
              PAsequencenumber,
              PAbegindate,
              PAEnDate,
              PATOTPRJFEEAMT,
              PATOTRETNFEEAMT,
              PATOTRETNTNAMT,
              PATOTSRVCFEEAMT,
              PAClose_CB,
              PAVARPRJFEEAMT,
              PAVARRETFEEAMT,
              PAVARETNFEEAMT,
              PAVARSERVFEEAMT,
              PAPosted_Project_Fee,
              PAPosted_Retainer_Fee,
              PAPOSTRETAMT,
              PAPosted_Service_Fee)
  select @PACONTNUMBER,
         @I_vPACHGORDNO,
         @I_vPAPROJNUMBER,
         @I_vPALineItemSeq,
         @PAVARTOTFEEAMT,
         @PAsequencenumber,
         @I_vPAbegindate,
         @I_vPAEnDate,
         @I_vPATOTPRJFEEAMT,
         @I_vPATOTRETNFEEAMT,
         @I_vPATOTRETNTNAMT,
         @I_vPATOTSRVCFEEAMT,
         @PAClose_CB,
         @PAVARPRJFEEAMT,
         @PAVARRETFEEAMT,
         @PAVARETNFEEAMT,
         @PAVARSERVFEEAMT,
         @PAPosted_Project_Fee,
         @PAPosted_Retainer_Fee,
         @PAPOSTRETAMT,
         @PAPosted_Service_Fee
  
  if @@error <> 0
    begin
      select @O_iErrorState = 2446
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  exec @iStatus = taPAChangeOrderEntryFeeHeaderPost
    @I_vPACHGORDNO ,
    @I_vPAPROJNUMBER ,
    @I_vPALineItemSeq ,
    @I_vPAbegindate ,
    @I_vPAEnDate ,
    @I_vPATOTPRJFEEAMT ,
    @I_vPATOTRETNFEEAMT ,
    @I_vPATOTRETNTNAMT ,
    @I_vPATOTSRVCFEEAMT ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@oErrString))
      
      select @O_iErrorState = 2447
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)
  
  grant
    execute
    on dbo.taPAChangeOrderEntryFeeHeader
  to DYNGRP

