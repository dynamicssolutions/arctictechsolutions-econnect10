

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetItems]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetItems]
  
create procedure [dbo].[RmsGetItems]
                @iItemTempTable VARCHAR(255),
                @iRmsServerName VARCHAR(255),
                @iHqDbName      VARCHAR(255),
                @iReImport      TINYINT  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lItemTable VARCHAR(255)
  
  SELECT @lItemTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Item as Item'
  
  EXEC( 'INSERT INTO ' + @iItemTempTable + '(ITEMNMBR,  RmsItemID,  ITEMDESC,  RmsDeptID,  MARKED,  RmsIntegrationNotes)  SELECT  ISNULL(UPPER(ItemLookupCode),''''),  [ID],  ISNULL(Description,''''),  ISNULL(DepartmentID,''''),  1,  ''''  FROM ' + @lItemTable)
  
  IF @iReImport = 0
    BEGIN
      EXEC( 'DELETE ' + @iItemTempTable + ' FROM  RMS30200 JOIN ' + @iItemTempTable + ' ON RMS30200.RmsItemID = ' + @iItemTempTable + '.RmsItemID')
    END

