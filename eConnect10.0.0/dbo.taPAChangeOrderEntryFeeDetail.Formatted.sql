

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAChangeOrderEntryFeeDetail]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAChangeOrderEntryFeeDetail]
  
create procedure [dbo].[taPAChangeOrderEntryFeeDetail]
                @I_vPACONTNUMBER        char(11),
                @I_vPACHGORDNO          char(17),
                @I_vPAPROJNUMBER        char(15),
                @I_vPAFeeID             char(15),
                @I_vPAFeeAmount         numeric(19,5)  = 0,
                @I_vPAFeePercent        numeric(19,5)  = 0,
                @I_vPAFeeToUse          smallint,
                @I_vPASales_Tax_Options smallint  = 3,
                @I_vPAbilltaxscheduleid char(15)  = '',
                @I_vPAFrequency         smallint,
                @I_vPALineItemSeq       int  = 0,
                @I_vPAbegindate         datetime  = '',
                @I_vPAEnDate            datetime  = '',
                @I_vPARenew             smallint  = 0,
                @I_vPARenewal_Date      datetime  = '',
                @I_vPA_Renew_Day        int  = 0,
                @I_vPA_Renew_Month      int  = 0,
                @I_vPAtotcbts           tinyint  = 0,
                @I_vPAtotcbEL           tinyint  = 0,
                @I_vPAtotcbML           tinyint  = 0,
                @I_vPAtotcbvi           tinyint  = 0,
                @I_vPAtotcber           tinyint  = 0,
                @I_vPAtotcbinv          tinyint  = 0,
                @I_vRequesterTrx        smallint  = 0,
                @I_vUSRDEFND1           char(50)  = '',
                @I_vUSRDEFND2           char(50)  = '',
                @I_vUSRDEFND3           char(50)  = '',
                @I_vUSRDEFND4           varchar(8000)  = '',
                @I_vUSRDEFND5           varchar(8000)  = '',
                @O_iErrorState          int  output,
                @oErrString             varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @PAFeeID                  char(15),
           @PALineItemSeq            int,
           @PAFeeType                smallint,
           @PAFeeAmount              numeric(19,5),
           @PAPercent_Cost           numeric(19,5),
           @PAPercent_Revenue        numeric(19,5),
           @PA_Retention_Percent     numeric(19,5),
           @PAFeeToUse               smallint,
           @PAbilltaxscheduleid      char(15),
           @PASales_Tax_Options      smallint,
           @PAbegindate              datetime,
           @PAEnDate                 datetime,
           @PAFrequency              smallint,
           @STRTDATE                 datetime,
           @ENDDATE                  datetime,
           @PARenew                  smallint,
           @PARenewal_Date           datetime,
           @PATotAmt                 numeric(19,5),
           @TM_AR_Fee_SRC            smallint,
           @TM_Prj_Deferred_Rev_Fee_ smallint,
           @TM_Prj_Rev_Fee_SRC       smallint,
           @PAFF_AR_Fee_SRC          smallint,
           @FF_Prj_Billings_Fee_SRC  smallint,
           @FF_Prj_Rev_Fee_SRC       smallint,
           @FF_Retention_Fee_SRC     smallint,
           @FF_WIP_Fee_SRC           smallint,
           @FF_Prj_Deferred_Rev_Fee_ smallint,
           @FF_BIEE_Fee_SRC          smallint,
           @FF_EIEB_Fee_SRC          smallint,
           @TM_BIEE_Fee_SRC          smallint,
           @TM_EIEB_Fee_SRC          smallint,
           @PAtotcbts                smallint,
           @PAtotcbEL                smallint,
           @PAtotcbML                smallint,
           @PAtotcbvi                smallint,
           @PAtotcber                smallint,
           @PAtotcbinv               smallint,
           @PA_Renew_Day             int,
           @PA_Renew_Month           int,
           @PAService_Fee_Amount     numeric(19,5),
           @PAPROJNUMBER             char(15),
           @PAProjectType            smallint,
           @PAAcctgMethod            smallint,
           @PACONTNUMBER             char(11),
           @iStatus                  int,
           @iError                   int,
           @iCustomState             int,
           @O_oErrorState            int,
           @PASTAT                   smallint,
           @PATRKCHGORDS             smallint,
           @PA_Prev_Fee_Percent      numeric(19,5)
  
  select @PAFeeID = '',
         @PALineItemSeq = 0,
         @PAFeeType = 0,
         @PAFeeAmount = 0,
         @PAPercent_Cost = 0,
         @PAPercent_Revenue = 0,
         @PA_Retention_Percent = 0,
         @PAFeeToUse = 0,
         @PAbilltaxscheduleid = '',
         @PASales_Tax_Options = 0,
         @PAbegindate = '',
         @PAEnDate = '',
         @PAFrequency = 0,
         @STRTDATE = '',
         @ENDDATE = '',
         @PARenew = 0,
         @PARenewal_Date = '',
         @PATotAmt = 0,
         @TM_AR_Fee_SRC = 0,
         @TM_Prj_Deferred_Rev_Fee_ = 0,
         @TM_Prj_Rev_Fee_SRC = 0,
         @PAFF_AR_Fee_SRC = 0,
         @FF_Prj_Billings_Fee_SRC = 0,
         @FF_Prj_Rev_Fee_SRC = 0,
         @FF_Retention_Fee_SRC = 0,
         @FF_WIP_Fee_SRC = 0,
         @FF_Prj_Deferred_Rev_Fee_ = 0,
         @FF_BIEE_Fee_SRC = 0,
         @FF_EIEB_Fee_SRC = 0,
         @TM_BIEE_Fee_SRC = 0,
         @TM_EIEB_Fee_SRC = 0,
         @PAtotcbts = 0,
         @PAtotcbEL = 0,
         @PAtotcbML = 0,
         @PAtotcbvi = 0,
         @PAtotcber = 0,
         @PAtotcbinv = 0,
         @PA_Renew_Day = 0,
         @PA_Renew_Month = 0,
         @PAService_Fee_Amount = 0,
         @PAPROJNUMBER = '',
         @PAProjectType = 0,
         @PAAcctgMethod = 0,
         @PACONTNUMBER = '',
         @iStatus = 0,
         @iError = 0,
         @iCustomState = 0,
         @O_oErrorState = 0,
         @PASTAT = 0,
         @PATRKCHGORDS = 0,
         @PA_Prev_Fee_Percent = 0,
         @O_iErrorState = 0
  
  exec @iStatus = taPAChangeOrderEntryFeeDetailPre
    @I_vPACONTNUMBER output ,
    @I_vPACHGORDNO output ,
    @I_vPAPROJNUMBER output ,
    @I_vPAFeeID output ,
    @I_vPAFeeAmount output ,
    @I_vPAFeePercent output ,
    @I_vPAFeeToUse output ,
    @I_vPASales_Tax_Options output ,
    @I_vPAbilltaxscheduleid output ,
    @I_vPAFrequency output ,
    @I_vPALineItemSeq output ,
    @I_vPAbegindate output ,
    @I_vPAEnDate output ,
    @I_vPARenew output ,
    @I_vPARenewal_Date output ,
    @I_vPA_Renew_Day output ,
    @I_vPA_Renew_Month output ,
    @I_vPAtotcbts output ,
    @I_vPAtotcbEL output ,
    @I_vPAtotcbML output ,
    @I_vPAtotcbvi output ,
    @I_vPAtotcber output ,
    @I_vPAtotcbinv output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@oErrString))
      
      select @O_iErrorState = 4156
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPACONTNUMBER is null 
       or @I_vPACHGORDNO is null
       or @I_vPAPROJNUMBER is null
       or @I_vPAFeeID is null
       or @I_vPAFeeAmount is null
       or @I_vPAFeePercent is null
       or @I_vPAFeeToUse is null
       or @I_vPASales_Tax_Options is null
       or @I_vPAbilltaxscheduleid is null
       or @I_vPAFrequency is null
       or @I_vPALineItemSeq is null
       or @I_vPARenew is null
       or @I_vPARenewal_Date is null
       or @I_vPA_Renew_Day is null
       or @I_vPA_Renew_Month is null
       or @I_vPAtotcbts is null
       or @I_vPAtotcbEL is null
       or @I_vPAtotcbML is null
       or @I_vPAtotcbvi is null
       or @I_vPAtotcber is null
       or @I_vPAtotcbinv is null)
    begin
      select @O_iErrorState = 4157
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPACHGORDNO = ''
       or @I_vPACONTNUMBER = ''
       or @I_vPAPROJNUMBER = ''
       or @I_vPAFeeID = '')
    begin
      select @O_iErrorState = 4158
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vPACHGORDNO = upper(@I_vPACHGORDNO),
         @I_vPACONTNUMBER = upper(@I_vPACONTNUMBER),
         @I_vPAPROJNUMBER = upper(@I_vPAPROJNUMBER),
         @I_vPAFeeID = upper(@I_vPAFeeID)
  
  if (@I_vPACONTNUMBER <> '')
    begin
      if not exists (select 1
                     from   PA01101 (nolock)
                     where  PACONTNUMBER = @I_vPACONTNUMBER)
        begin
          select @O_iErrorState = 4159
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPAPROJNUMBER <> '')
    begin
      select @PAPROJNUMBER = PAPROJNUMBER,
             @PAProjectType = PAProjectType,
             @PAAcctgMethod = PAAcctgMethod,
             @PACONTNUMBER = PACONTNUMBER
      from   PA01201 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@PAPROJNUMBER = '')
        begin
          select @O_iErrorState = 3920
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@PASTAT = 3)
           or (@PASTAT = 5))
        begin
          select @O_iErrorState = 2396
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @PAPROJNUMBER = PAPROJNUMBER,
             @PATRKCHGORDS = PATRKCHGORDS
      from   PA01241 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      select @PAFeeID = PAFeeID,
             @PALineItemSeq = PALineItemSeq,
             @PAFeeType = PAFeeType,
             @PAFeeAmount = PAFeeAmount,
             @PAPercent_Cost = PAPercent_Cost,
             @PAPercent_Revenue = PAPercent_Revenue,
             @PA_Retention_Percent = PA_Retention_Percent,
             @PAFeeToUse = PAFeeToUse,
             @PAbilltaxscheduleid = PAbilltaxscheduleid,
             @PASales_Tax_Options = PASales_Tax_Options,
             @PAbegindate = PAbegindate,
             @PAEnDate = PAEnDate,
             @PAFrequency = PAFrequency,
             @STRTDATE = STRTDATE,
             @ENDDATE = ENDDATE,
             @PARenew = PARenew,
             @PARenewal_Date = PARenewal_Date,
             @PATotAmt = PATotAmt,
             @TM_AR_Fee_SRC = TM_AR_Fee_SRC,
             @TM_Prj_Deferred_Rev_Fee_ = TM_Prj_Deferred_Rev_Fee_,
             @TM_Prj_Rev_Fee_SRC = TM_Prj_Rev_Fee_SRC,
             @PAFF_AR_Fee_SRC = @PAFF_AR_Fee_SRC,
             @FF_Prj_Billings_Fee_SRC = @FF_Prj_Billings_Fee_SRC,
             @FF_Prj_Rev_Fee_SRC = @FF_Prj_Rev_Fee_SRC,
             @FF_Retention_Fee_SRC = @FF_Retention_Fee_SRC,
             @FF_WIP_Fee_SRC = @FF_WIP_Fee_SRC,
             @FF_Prj_Deferred_Rev_Fee_ = @FF_Prj_Deferred_Rev_Fee_,
             @FF_BIEE_Fee_SRC = @FF_BIEE_Fee_SRC,
             @FF_EIEB_Fee_SRC = @FF_EIEB_Fee_SRC,
             @PAtotcbts = @PAtotcbts,
             @PAtotcbEL = @PAtotcbEL,
             @PAtotcbML = @PAtotcbML,
             @PAtotcbvi = @PAtotcbvi,
             @PAtotcber = @PAtotcber,
             @PAtotcbinv = @PAtotcbinv,
             @PA_Renew_Day = @PA_Renew_Day,
             @PA_Renew_Month = @PA_Renew_Month,
             @PAService_Fee_Amount = @PAService_Fee_Amount
      from   PA02101 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PAFeeID = @I_vPAFeeID
    end
  
  if (@I_vPAFeeID <> '')
    begin
      if (@PAFeeID = '')
        begin
          select @PAFeeID = PAFeeID,
                 @PAFeeType = PAFeeType,
                 @PAFeeAmount = PAFeeAmount,
                 @PAPercent_Cost = PAPercent_Cost,
                 @PAPercent_Revenue = PAPercent_Revenue,
                 @PA_Retention_Percent = PA_Retention_Percent,
                 @PAFeeToUse = PAFeeToUse,
                 @PAbilltaxscheduleid = PAbilltaxscheduleid,
                 @PASales_Tax_Options = PASales_Tax_Options,
                 @PAFrequency = PAFrequency,
                 @STRTDATE = STRTDATE,
                 @ENDDATE = ENDDATE,
                 @PARenew = PARenew,
                 @PARenewal_Date = PARenewal_Date,
                 @PAtotcbts = @PAtotcbts,
                 @PAtotcbEL = @PAtotcbEL,
                 @PAtotcbML = @PAtotcbML,
                 @PAtotcbvi = @PAtotcbvi,
                 @PAtotcber = @PAtotcber,
                 @PAtotcbinv = @PAtotcbinv
          from   PA00401 (nolock)
          where  PAFeeID = @I_vPAFeeID
          
          if (@PAFeeID = '')
            begin
              select @O_iErrorState = 2398
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          if (@PAFeeType = 1)
            select @TM_Prj_Rev_Fee_SRC = TM_Prj_Rev_Fee_SRC,
                   @TM_AR_Fee_SRC = TM_AR_Fee_SRC,
                   @FF_WIP_Fee_SRC = FF_WIP_Fee_SRC,
                   @FF_Prj_Billings_Fee_SRC = FF_Prj_Billings_Fee_SRC,
                   @PAFF_AR_Fee_SRC = PAFF_AR_Fee_SRC,
                   @FF_Prj_Rev_Fee_SRC = FF_Prj_Rev_Fee_SRC,
                   @TM_BIEE_Fee_SRC = TM_BIEE_Fee_SRC,
                   @TM_EIEB_Fee_SRC = TM_EIEB_Fee_SRC,
                   @FF_BIEE_Fee_SRC = FF_BIEE_Fee_SRC,
                   @FF_EIEB_Fee_SRC = FF_EIEB_Fee_SRC
            from   PA41701 (nolock)
            where  PAsetupkey = 1
          
          if (@PAFeeType = 2)
            select @TM_AR_Fee_SRC = TM_AR_Fee_SRC,
                   @TM_Prj_Deferred_Rev_Fee_ = TM_Prj_Deferred_Rev_Fee_,
                   @PAFF_AR_Fee_SRC = PAFF_AR_Fee_SRC,
                   @FF_Prj_Deferred_Rev_Fee_ = FF_Prj_Deferred_Rev_Fee_
            from   PA41701 (nolock)
            where  PAsetupkey = 1
          
          if (@PAFeeType = 3)
            select @FF_Retention_Fee_SRC = FF_Retention_Fee_SRC,
                   @PAFF_AR_Fee_SRC = PAFF_AR_Fee_SRC
            from   PA41701 (nolock)
            where  PAsetupkey = 1
          
          if (@PAFeeType = 4)
            select @TM_Prj_Rev_Fee_SRC = TM_Prj_Rev_Fee_SRC,
                   @TM_AR_Fee_SRC = TM_AR_Fee_SRC,
                   @TM_Prj_Deferred_Rev_Fee_ = TM_Prj_Deferred_Rev_Fee_,
                   @TM_BIEE_Fee_SRC = TM_BIEE_Fee_SRC,
                   @TM_EIEB_Fee_SRC = TM_EIEB_Fee_SRC,
                   @FF_BIEE_Fee_SRC = FF_BIEE_Fee_SRC,
                   @FF_EIEB_Fee_SRC = FF_EIEB_Fee_SRC
            from   PA41701 (nolock)
            where  PAsetupkey = 1
        end
    end
  
  if (@PAProjectType = 1)
     and (@PAFeeType = 3)
    begin
      select @O_iErrorState = 2399
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PAProjectType = 2)
     and (@PAFeeType = 4)
    begin
      select @O_iErrorState = 2400
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PAProjectType = 3)
     and (@PAFeeType = 4)
    begin
      select @O_iErrorState = 2401
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if exists (select 1
             from   PA12102 (nolock)
             where  PAPROJNUMBER = @I_vPAPROJNUMBER
                    and PACONTNUMBER = @I_vPACONTNUMBER
                    and PACHGORDNO = @I_vPACHGORDNO
                    and PAFeeID = @I_vPAFeeID)
    begin
      select @O_iErrorState = 2402
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAtotcbts < 0)
      or (@I_vPAtotcbts > 1)
    begin
      select @O_iErrorState = 2403
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAtotcbEL < 0)
      or (@I_vPAtotcbEL > 1)
    begin
      select @O_iErrorState = 2404
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAtotcbML < 0)
      or (@I_vPAtotcbML > 1)
    begin
      select @O_iErrorState = 2405
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAtotcbvi < 0)
      or (@I_vPAtotcbvi > 1)
    begin
      select @O_iErrorState = 2406
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAtotcber < 0)
      or (@I_vPAtotcber > 1)
    begin
      select @O_iErrorState = 2407
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAtotcbinv < 0)
      or (@I_vPAtotcbinv > 1)
    begin
      select @O_iErrorState = 2408
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vRequesterTrx < 0)
      or (@I_vRequesterTrx > 1)
    begin
      select @O_iErrorState = 2409
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPARenew < 0)
      or (@I_vPARenew > 1)
    begin
      select @O_iErrorState = 2410
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPARenew = 1)
     and (@I_vPARenewal_Date = '')
    begin
      select @O_iErrorState = 2422
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@PAFeeType = 4)
     and (@I_vPARenew = 1)
     and ((@I_vPA_Renew_Day = 0)
           or (@I_vPA_Renew_Month = 0))
    begin
      select @O_iErrorState = 2423
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAFrequency < 1)
      or (@I_vPAFrequency > 3)
    begin
      select @O_iErrorState = 2413
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAFrequency <> 1)
     and (@PAFeeType = 2)
    begin
      select @O_iErrorState = 2414
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAFrequency = 1)
     and (@PAFeeType = 3)
    begin
      select @O_iErrorState = 2415
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAFrequency <> 1)
     and (@PAFeeType = 4)
    begin
      select @O_iErrorState = 2416
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAFeeToUse = 0)
     and (@PAFeeType <> 3)
     and ((@I_vPAFrequency = 3)
           or (@I_vPAFrequency = 4))
    begin
      select @O_iErrorState = 2417
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPASales_Tax_Options < 1
       or @I_vPASales_Tax_Options > 3)
    begin
      select @O_iErrorState = 2418
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  else
    begin
      if (@I_vPASales_Tax_Options <> 1)
        select @I_vPAbilltaxscheduleid = ''
    end
  
  if (@I_vPAbilltaxscheduleid <> '')
    begin
      if (not exists (select 1
                      from   TX00101 (nolock)
                      where  TAXSCHID = @I_vPAbilltaxscheduleid))
        begin
          select @O_iErrorState = 2419
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPASales_Tax_Options = 1
      and @I_vPAbilltaxscheduleid = '')
    begin
      select @O_iErrorState = 2424
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPA_Renew_Day = 0)
     and (@I_vPARenewal_Date <> '')
    select @I_vPA_Renew_Day = DATEPART(dd,@I_vPARenewal_Date)
  
  if (@I_vPA_Renew_Month = 0)
     and (@I_vPARenewal_Date <> '')
    select @I_vPA_Renew_Month = DATEPART(mm,@I_vPARenewal_Date)
  
  if (@I_vPAbegindate = '')
     and (@PAbegindate <> '')
    select @I_vPAbegindate = @PAbegindate
  
  if (@I_vPAEnDate = 0)
     and (@PAEnDate <> '')
    select @I_vPAEnDate = @PAEnDate
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  insert PA12102
        (PACONTNUMBER,
         PACHGORDNO,
         PAPROJNUMBER,
         PALineItemSeq,
         PAFeeID,
         PAFeeType,
         PAPREVFEEAMT,
         PAVARFEESAMT,
         PAFeeAmount,
         PA_Prev_Fee_Percent,
         PAVARFEEPRCNT,
         PAFeePercent,
         PAFeeToUse,
         PASales_Tax_Options,
         PAbilltaxscheduleid,
         PAPREVBILLTXSCHED,
         PAbegindate,
         PAPREVBEGINDT,
         PAEnDate,
         PAPREVENDDT,
         PAFrequency,
         PAPREVFREQ,
         STRTDATE,
         ENDDATE,
         PARenew,
         PARenewal_Date,
         PATotAmt,
         PAPREVTOTAMT,
         PA_Renew_Day,
         PA_Renew_Month,
         PAsequencenumber,
         TM_AR_Fee_SRC,
         TM_Prj_Deferred_Rev_Fee_,
         TM_Prj_Rev_Fee_SRC,
         TM_BIEE_Fee_SRC,
         TM_EIEB_Fee_SRC,
         PAFF_AR_Fee_SRC,
         FF_Prj_Billings_Fee_SRC,
         FF_Prj_Rev_Fee_SRC,
         FF_Retention_Fee_SRC,
         FF_WIP_Fee_SRC,
         FF_Prj_Deferred_Rev_Fee_,
         FF_BIEE_Fee_SRC,
         FF_EIEB_Fee_SRC,
         PAtotcbts,
         PAtotcbEL,
         PAtotcbML,
         PAtotcbvi,
         PAtotcber,
         PAtotcbinv,
         PAbillnoteidx,
         PAPosted_Project_Fee,
         PAPosted_Retainer_Fee,
         PAPOSTRETAMT,
         PAPosted_Service_Fee)
  select @I_vPACONTNUMBER,
         @I_vPACHGORDNO,
         @I_vPAPROJNUMBER,
         @I_vPALineItemSeq,
         @I_vPAFeeID,
         @PAFeeType,
         0,
         @I_vPAFeeAmount,
         @I_vPAFeeAmount,
         @PA_Prev_Fee_Percent,
         @PA_Prev_Fee_Percent - @I_vPAFeePercent,
         @I_vPAFeePercent,
         @I_vPAFeeToUse,
         @I_vPASales_Tax_Options,
         @I_vPAbilltaxscheduleid,
         @PAbilltaxscheduleid,
         @I_vPAbegindate,
         '',
         @I_vPAEnDate,
         '',
         @I_vPAFrequency,
         0,
         @STRTDATE,
         @ENDDATE,
         @I_vPARenew,
         @I_vPARenewal_Date,
         @PATotAmt,
         0,
         @I_vPA_Renew_Day,
         @I_vPA_Renew_Month,
         1,
         @TM_AR_Fee_SRC,
         @TM_Prj_Deferred_Rev_Fee_,
         @TM_Prj_Rev_Fee_SRC,
         @TM_BIEE_Fee_SRC,
         @TM_EIEB_Fee_SRC,
         @PAFF_AR_Fee_SRC,
         @FF_Prj_Billings_Fee_SRC,
         @FF_Prj_Rev_Fee_SRC,
         @FF_Retention_Fee_SRC,
         @FF_WIP_Fee_SRC,
         @FF_Prj_Deferred_Rev_Fee_,
         @FF_BIEE_Fee_SRC,
         @FF_EIEB_Fee_SRC,
         @I_vPAtotcbts,
         @I_vPAtotcbEL,
         @I_vPAtotcbML,
         @I_vPAtotcbvi,
         @I_vPAtotcber,
         @I_vPAtotcbinv,
         0,
         0,
         0,
         0,
         0
  
  if @@error <> 0
    begin
      select @O_iErrorState = 2420
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  exec @iStatus = taPAChangeOrderEntryFeeDetailPost
    @I_vPACONTNUMBER ,
    @I_vPACHGORDNO ,
    @I_vPAPROJNUMBER ,
    @I_vPAFeeID ,
    @I_vPAFeeAmount ,
    @I_vPAFeePercent ,
    @I_vPAFeeToUse ,
    @I_vPASales_Tax_Options ,
    @I_vPAbilltaxscheduleid ,
    @I_vPAFrequency ,
    @I_vPALineItemSeq ,
    @I_vPAbegindate ,
    @I_vPAEnDate ,
    @I_vPARenew ,
    @I_vPARenewal_Date ,
    @I_vPA_Renew_Day ,
    @I_vPA_Renew_Month ,
    @I_vPAtotcbts ,
    @I_vPAtotcbEL ,
    @I_vPAtotcbML ,
    @I_vPAtotcbvi ,
    @I_vPAtotcber ,
    @I_vPAtotcbinv ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@oErrString))
      
      select @O_iErrorState = 2421
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)
  
  grant
    execute
    on dbo.taPAChangeOrderEntryFeeDetail
  to DYNGRP

