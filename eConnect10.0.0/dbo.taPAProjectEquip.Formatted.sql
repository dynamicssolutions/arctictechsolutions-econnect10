

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAProjectEquip]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAProjectEquip]
  
create procedure [dbo].[taPAProjectEquip]
                @I_vPAPROJNUMBER char(15),
                @I_vPAEQUIPTID   char(15),
                @I_vAction       smallint  = 1,
                @I_vRequesterTrx smallint  = 0,
                @I_vUSRDEFND1    char(50)  = '',
                @I_vUSRDEFND2    char(50)  = '',
                @I_vUSRDEFND3    char(50)  = '',
                @I_vUSRDEFND4    varchar(8000)  = '',
                @I_vUSRDEFND5    varchar(8000)  = '',
                @O_iErrorState   int  output,
                @oErrString      varchar(255)  output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @CURNCYID         char(15),
           @CURRNIDX         int,
           @iStatus          int,
           @iCustomState     int,
           @iCustomErrString varchar(255),
           @iError           int,
           @O_oErrorState    int
  
  select @CURNCYID = '',
         @CURRNIDX = 0,
         @O_oErrorState = 0,
         @O_iErrorState = 0,
         @iStatus = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  exec @iStatus = taPAProjectEquipPre
    @I_vPAPROJNUMBER output ,
    @I_vPAEQUIPTID output ,
    @I_vAction output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4917
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAPROJNUMBER is NULL 
       or @I_vPAEQUIPTID is NULL
       or @I_vRequesterTrx is NULL)
    begin
      select @O_iErrorState = 4918
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAPROJNUMBER = ''
       OR @I_vPAEQUIPTID = '')
    begin
      select @O_iErrorState = 4919
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vPAPROJNUMBER = UPPER(@I_vPAPROJNUMBER),
         @I_vPAEQUIPTID = UPPER(@I_vPAEQUIPTID)
  
  if not exists (select 1
                 from   PA01201 (nolock)
                 where  PAPROJNUMBER = @I_vPAPROJNUMBER)
    begin
      select @O_iErrorState = 4920
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if not exists (select 1
                 from   PA00701 (nolock)
                 where  PAEQUIPTID = @I_vPAEQUIPTID)
    begin
      select @O_iErrorState = 4921
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vAction = 1)
    begin
      if exists (select 1
                 from   PA01409 (nolock)
                 where  PAPROJNUMBER = @I_vPAPROJNUMBER
                        and PAEQUIPTID = @I_vPAEQUIPTID)
        begin
          select @O_iErrorState = 4922
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vAction < 1)
      or (@I_vAction > 2)
    begin
      select @O_iErrorState = 5256
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@oErrString <> '')
    return (@O_iErrorState)
  
  if (@I_vAction = 1)
    begin
      insert into PA01409
                 (PAPROJNUMBER,
                  PAEQUIPTID)
      select @I_vPAPROJNUMBER,
             @I_vPAEQUIPTID
    end
  else
    begin
      delete PA01409
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PAEQUIPTID = @I_vPAEQUIPTID
    end
  
  if @@error <> 0
    begin
      select @O_iErrorState = 4923
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @O_iErrorState <> 0
    return (@O_iErrorState)
  
  exec @iStatus = taPAProjectEquipPost
    @I_vPAPROJNUMBER ,
    @I_vPAEQUIPTID ,
    @I_vAction ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4924
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

