

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetLinkedServers]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetLinkedServers]
  
create procedure [dbo].[RmsGetLinkedServers]
                @iLinkedServerTempTable VARCHAR(255)  /*with encryption*/
AS
  DECLARE  @lSqlError INT
  
  CREATE TABLE #ServerTemp (
    RmsServerName           char(254),
    RmsServerProvider       char(255),
    RmsServerProduct        char(255),
    RmsServerDataSource     char(255),
    RmsServerProviderString char(151),
    RmsServerLocation       char(255),
    RmsServerCatelog        char(255))
  
  INSERT INTO #ServerTemp
  EXEC sp_linkedservers
  
  EXEC( 'INSERT INTO ' + @iLinkedServerTempTable + ' Select  ISNULL(RmsServerName,''''),  ISNULL(RmsServerProvider,''''),  ISNULL(RmsServerProduct,''''),  ISNULL(RmsServerDataSource,''''),  ISNULL(RmsServerProviderString,''''),  ISNULL(RmsServerLocation,''''),  ISNULL(RmsServerCatelog,'''')  FROM  #ServerTemp ')

