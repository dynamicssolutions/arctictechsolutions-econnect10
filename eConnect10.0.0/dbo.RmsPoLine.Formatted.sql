

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsPoLine]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsPoLine]
  
create procedure [dbo].[RmsPoLine]
                @I_vPOTYPE                   smallint  = null,
                @I_vPONUMBER                 char(17),
                @I_vVENDORID                 char(15),
                @I_vLOCNCODE                 char(10)  = null,
                @I_vVNDITNUM                 char(30)  = null,
                @I_vITEMNMBR                 char(30)  = null,
                @I_vQUANTITY                 numeric(19,5)  = null,
                @I_vQTYCANCE                 numeric(19,5)  = null,
                @I_vFREEONBOARD              smallint  = null,
                @I_vREQSTDBY                 char(20)  = null,
                @I_vCOMMNTID                 char(15)  = null,
                @I_vCOMMENT_1                char(50)  = null,
                @I_vCOMMENT_2                char(50)  = null,
                @I_vCOMMENT_3                char(50)  = null,
                @I_vCOMMENT_4                char(50)  = null,
                @I_vREQDATE                  datetime  = null,
                @I_vRELEASEBYDATE            datetime  = null,
                @I_vPRMDATE                  datetime  = null,
                @I_vPRMSHPDTE                datetime  = null,
                @I_vNONINVEN                 smallint  = 0,
                @I_vIVIVINDX                 int  = null,
                @I_vInventoryAccount         varchar(75)  = '',
                @I_vITEMDESC                 char(100)  = null,
                @I_vUNITCOST                 numeric(19,5)  = null,
                @I_vVNDITDSC                 char(100)  = null,
                @I_vUOFM                     char(8)  = null,
                @I_vPurchase_IV_Item_Taxable smallint  = null,
                @I_vPurchase_Item_Tax_Schedu char(15)  = null,
                @I_vPurchase_Site_Tax_Schedu char(15)  = null,
                @I_vBSIVCTTL                 smallint  = null,
                @I_vTAXAMNT                  numeric(19,5)  = null,
                @I_vBCKTXAMT                 numeric(19,5)  = null,
                @I_vLanded_Cost_Group_ID     char(15)  = null,
                @I_vPLNNDSPPLID              smallint  = null,
                @I_vSHIPMTHD                 char(15)  = null,
                @I_vBackoutTradeDiscTax      numeric(19,5)  = null,
                @I_vPOLNESTA                 smallint  = null,
                @I_vCMMTTEXT                 varchar(500)  = null,
                @I_vORD                      int  = null,
                @I_vCUSTNMBR                 char(15)  = null,
                @I_vADRSCODE                 char(15)  = null,
                @I_vCMPNYNAM                 char(64)  = null,
                @I_vCONTACT                  char(60)  = null,
                @I_vADDRESS1                 char(60)  = null,
                @I_vADDRESS2                 char(60)  = null,
                @I_vADDRESS3                 char(60)  = null,
                @I_vCITY                     char(35)  = null,
                @I_vSTATE                    char(29)  = null,
                @I_vZIPCODE                  char(10)  = null,
                @I_vCCode                    char(6)  = null,
                @I_vCOUNTRY                  char(60)  = null,
                @I_vPHONE1                   char(21)  = null,
                @I_vPHONE2                   char(21)  = null,
                @I_vPHONE3                   char(21)  = null,
                @I_vFAX                      char(21)  = null,
                @I_vCURNCYID                 char(15)  = null,
                @I_vProjNum                  char(15)  = null,
                @I_vCostCatID                char(15)  = null,
                @I_vLineNumber               int  = null,
                @I_vUpdateIfExists           smallint  = 0,
                @I_vNOTETEXT                 varchar(8000)  = null,
                @I_vRequesterTrx             smallint  = 0,
                @I_vUSRDEFND1                char(50)  = null,
                @I_vUSRDEFND2                char(50)  = null,
                @I_vUSRDEFND3                char(50)  = null,
                @I_vUSRDEFND4                varchar(8000)  = null,
                @I_vUSRDEFND5                varchar(8000)  = null,
                @O_iErrorState               int  output,
                @oErrString                  varchar(255)  output     /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @PACONTNUMBER             char(11),
           @POTYPE                   smallint,
           @ITMTRKOP                 smallint,
           @ITEMTYPE                 smallint,
           @INVINDX                  int,
           @UNITCOST                 numeric(19,5),
           @ITEMDESC                 char(100),
           @VNDITDSC                 char(100),
           @CURRNIDX                 int,
           @SEQNUMBR                 int,
           @LINEORIGIN               smallint,
           @VCTNMTHD                 smallint,
           @PLANNINGLEADTIME         smallint,
           @VendItemCount            int,
           @DECPLQTY                 int,
           @DECPLCUR                 int,
           @ODECPLCU                 smallint,
           @FREEONBOARD              smallint,
           @UMQTYINB                 numeric(19,5),
           @cUMQTYINB                numeric(19,5),
           @UOFM                     char(8),
           @PRCHSUOM                 char(8),
           @UOMSCHDL                 char(10),
           @LOCNCODE                 char(10),
           @POLNEARY_1               numeric(19,5),
           @POLNEARY_2               numeric(19,5),
           @POLNEARY_3               numeric(19,5),
           @POLNEARY_4               numeric(19,5),
           @POLNEARY_5               numeric(19,5),
           @POLNEARY_6               numeric(19,5),
           @POLNEARY_7               numeric(19,5),
           @POLNEARY_8               numeric(19,5),
           @POLNEARY_9               numeric(19,5),
           @SHIPTYPE                 tinyint,
           @ADDRSOURCE               tinyint,
           @CMPANYID                 int,
           @MAXSEG                   int,
           @Location_Segment         char(67),
           @ACSGFLOC                 smallint,
           @ACTINDX                  int,
           @ACCNT_STRING             char(100),
           @NEW_ACCNT_STRING         varchar(100),
           @LOFSGMNT                 int,
           @LOFSGMNTALL              int,
           @LOFSGMNTEND              int,
           @iStatus                  int,
           @iError                   int,
           @iCursorError             int,
           @O_oErrorState            int,
           @iInsertComponent         int,
           @O_iCommentMstErrState    int,
           @iCustomState             int,
           @ISMCTRX                  int,
           @FUNLCURR                 char(15),
           @FUNDECPLCUR              int,
           @EDITDECPLCUR             int,
           @ITEMFUNCTDEC             int,
           @MCINSTALLED              smallint,
           @LineCount                int,
           @cPOLNESTA                smallint,
           @cQTYCANCE                numeric(19,5),
           @cUOFM                    char(8),
           @cLOCNCODE                char(10),
           @cQTYSHPPD0               numeric(19,5),
           @cQTYINVCD0               numeric(19,5),
           @cQTYREJ0                 numeric(19,5),
           @cQTYMATCH0               numeric(19,5),
           @cNETQTYSHPP0             numeric(19,5),
           @cQTYSHPPD1               numeric(19,5),
           @cQTYINVCD1               numeric(19,5),
           @cQTYREJ1                 numeric(19,5),
           @cQTYMATCH1               numeric(19,5),
           @cNETQTYSHPP1             numeric(19,5),
           @cQTYCMTBASE              numeric(19,5),
           @cCOMMNTID                char(15),
           @cCMMTTEXT                varchar(500),
           @cUNITCOST                numeric(19,5),
           @cQUANTITY                numeric(19,5),
           @cReleased_Date           datetime,
           @cCOMMENT_1               char(50),
           @cCOMMENT_2               char(50),
           @cCOMMENT_3               char(50),
           @cCOMMENT_4               char(50),
           @existscomment            smallint,
           @projectexists            smallint,
           @PASTAT                   smallint,
           @PAcloseProjcosts         smallint,
           @costcatexists            smallint,
           @PATU                     smallint,
           @PAinactive               smallint,
           @CANCELLED                tinyint,
           @PAsetupkey               tinyint,
           @USEADVTX                 tinyint,
           @TAXSCHID                 char(15),
           @PApounitostfrom          tinyint,
           @PAPROJNUMBER             char(15),
           @PAprojname               char(31),
           @PANONE                   char(6),
           @PAProjectType            tinyint,
           @PAAcctgMethod            tinyint,
           @PAIV_Item_Checkbox       tinyint,
           @Purchase_Tax_Options     tinyint,
           @PABase_Unit_Cost         numeric(19,5),
           @Purchase_Item_Tax_Schedu char(15),
           @PACogs_Idx               int,
           @PACGBWIPIDX              int,
           @PACOSTCATID              char(15),
           @COPTXSCH                 char(15),
           @ITEMNMBR                 char(30),
           @QTYBSUOM                 numeric(19,5),
           @PABase_Qty               numeric(19,5),
           @PRICELVL                 char(15),
           @CUSTNMBR                 char(15),
           @PALineItemSeq            int,
           @UserDate                 datetime,
           @PeriodID                 int,
           @Closed                   int,
           @Year                     int,
           @iCustomGLPeriodErr       int,
           @iCustomGLPeriodState     int,
           @QTYUNCMTBASE             numeric(19,5),
           @Rec_QTYSHPPD             numeric(19,5),
           @Rec_QTYINVCD             numeric(19,5),
           @Rec_QTYREJ               numeric(19,5),
           @sCompanyID               smallint,
           @nNextNoteIndex           numeric(19,5),
           @iGetNextNoteIdxErrState  int,
           @NOTEINDX                 numeric(19,5),
           @POSTATUS                 tinyint
  
  select @PACONTNUMBER = '',
         @ITMTRKOP = 1,
         @ITEMTYPE = 0,
         @INVINDX = 0,
         @UNITCOST = 0,
         @ITEMDESC = '',
         @VNDITDSC = '',
         @CURRNIDX = 0,
         @SEQNUMBR = 16384,
         @LINEORIGIN = 1,
         @VCTNMTHD = 0,
         @PLANNINGLEADTIME = 0,
         @VendItemCount = 0,
         @DECPLQTY = 0,
         @DECPLCUR = 0,
         @ODECPLCU = 0,
         @FREEONBOARD = 0,
         @UMQTYINB = 0,
         @cUMQTYINB = 0,
         @UOFM = '',
         @PRCHSUOM = '',
         @UOMSCHDL = '',
         @LOCNCODE = '',
         @POLNEARY_1 = 0,
         @POLNEARY_2 = 0,
         @POLNEARY_3 = 0,
         @POLNEARY_4 = 0,
         @POLNEARY_5 = 0,
         @POLNEARY_6 = 0,
         @POLNEARY_7 = 0,
         @POLNEARY_8 = 0,
         @POLNEARY_9 = 0,
         @SHIPTYPE = 3,
         @ADDRSOURCE = 0,
         @CMPANYID = 0,
         @MAXSEG = 0,
         @Location_Segment = '',
         @ACSGFLOC = 0,
         @ACTINDX = 0,
         @ACCNT_STRING = '',
         @NEW_ACCNT_STRING = '',
         @LOFSGMNT = 0,
         @LOFSGMNTALL = 0,
         @LOFSGMNTEND = 0,
         @iStatus = 0,
         @O_iErrorState = 0,
         @oErrString = '',
         @O_oErrorState = 0,
         @ISMCTRX = 0,
         @FUNLCURR = '',
         @FUNDECPLCUR = 0,
         @EDITDECPLCUR = 0,
         @ITEMFUNCTDEC = 0,
         @MCINSTALLED = 1,
         @LineCount = 0,
         @cPOLNESTA = 0,
         @cQTYCANCE = 0,
         @cUOFM = '',
         @cLOCNCODE = '',
         @cQTYSHPPD0 = 0,
         @cQTYINVCD0 = 0,
         @cQTYREJ0 = 0,
         @cQTYMATCH0 = 0,
         @cNETQTYSHPP0 = 0,
         @cQTYSHPPD1 = 0,
         @cQTYINVCD1 = 0,
         @cQTYREJ1 = 0,
         @cQTYMATCH1 = 0,
         @cNETQTYSHPP1 = 0,
         @cQTYCMTBASE = 0,
         @cCOMMNTID = '',
         @cCMMTTEXT = '',
         @cUNITCOST = 0,
         @cQUANTITY = 0,
         @cReleased_Date = '',
         @POTYPE = @I_vPOTYPE,
         @cCOMMENT_1 = '',
         @cCOMMENT_2 = '',
         @cCOMMENT_3 = '',
         @cCOMMENT_4 = '',
         @existscomment = 0,
         @projectexists = 0,
         @PASTAT = 0,
         @PAcloseProjcosts = 0,
         @costcatexists = 0,
         @PATU = 4,
         @PAinactive = 0,
         @CANCELLED = 0,
         @PAsetupkey = 0,
         @USEADVTX = 0,
         @TAXSCHID = '',
         @PApounitostfrom = 0,
         @PAPROJNUMBER = '',
         @PAprojname = '',
         @PANONE = '<NONE>',
         @PAProjectType = 0,
         @PAAcctgMethod = 0,
         @PAIV_Item_Checkbox = 0,
         @Purchase_Tax_Options = 0,
         @PABase_Unit_Cost = 0,
         @Purchase_Item_Tax_Schedu = '',
         @PACogs_Idx = 0,
         @PACGBWIPIDX = 0,
         @PACOSTCATID = '',
         @COPTXSCH = '',
         @ITEMNMBR = '',
         @QTYBSUOM = 0,
         @PABase_Qty = 0,
         @PRICELVL = '',
         @CUSTNMBR = '',
         @PALineItemSeq = 0,
         @UserDate = cast(getdate() as varchar(12)),
         @PeriodID = 0,
         @Closed = 0,
         @Year = 0,
         @iCustomGLPeriodErr = 0,
         @iCustomGLPeriodState = 0,
         @QTYUNCMTBASE = 0,
         @Rec_QTYSHPPD = 0,
         @Rec_QTYINVCD = 0,
         @Rec_QTYREJ = 0,
         @sCompanyID = 0,
         @nNextNoteIndex = 0,
         @iGetNextNoteIdxErrState = 0,
         @NOTEINDX = 0,
         @POSTATUS = 0
  
  select @I_vPONUMBER = upper(@I_vPONUMBER),
         @I_vCOMMNTID = upper(@I_vCOMMNTID),
         @I_vITEMNMBR = upper(@I_vITEMNMBR),
         @I_vVENDORID = upper(@I_vVENDORID),
         @I_vLOCNCODE = upper(@I_vLOCNCODE),
         @I_vCUSTNMBR = upper(@I_vCUSTNMBR),
         @I_vSHIPMTHD = upper(@I_vSHIPMTHD),
         @I_vCCode = upper(@I_vCCode),
         @I_vCURNCYID = upper(@I_vCURNCYID),
         @I_vProjNum = upper(@I_vProjNum),
         @I_vCostCatID = upper(@I_vCostCatID)
  
  select @CMPANYID = CMPANYID
  from   DYNAMICS..SY01500 (nolock)
  where  INTERID = db_name()
  
  if (@I_vUpdateIfExists = 1)
    begin
      select @LineCount = isnull(count(PONUMBER),0)
      from   POP10110 (nolock)
      where  PONUMBER = @I_vPONUMBER
             and ITEMNMBR = @I_vITEMNMBR
      
      if @LineCount > 1
         and ((@I_vORD is null 
               and @I_vPOTYPE in (1,2))
               or (@I_vORD is null 
                   and @I_vLineNumber is null
                   and @I_vPOTYPE in (3,4)))
        begin
          select @O_iErrorState = 9178
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (exists (select ORD
                  from   POP10110 (nolock)
                  where  PONUMBER = @I_vPONUMBER
                         and ORD = @I_vORD
                         and ITEMNMBR <> @I_vITEMNMBR))
        begin
          select @O_iErrorState = 9179
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@I_vPOTYPE in (1,2)
          and @I_vORD is null
          and exists (select 1
                      from   POP10110 (nolock)
                      where  PONUMBER = @I_vPONUMBER
                             and ITEMNMBR = @I_vITEMNMBR))
          or (exists (select 1
                      from   POP10110 (nolock)
                      where  PONUMBER = @I_vPONUMBER
                             and ITEMNMBR = @I_vITEMNMBR
                             and ORD = @I_vORD))
          or (exists (select 1
                      from   POP10110 (nolock)
                      where  PONUMBER = @I_vPONUMBER
                             and ITEMNMBR = @I_vITEMNMBR
                             and LineNumber = @I_vLineNumber))
        begin
          if (@I_vORD is null 
              and @I_vPOTYPE in (1,2))
            begin
              select @I_vORD = ORD
              from   POP10110 (nolock)
              where  PONUMBER = @I_vPONUMBER
                     and ITEMNMBR = @I_vITEMNMBR
            end
          else
            if (@I_vORD is null 
                and @I_vPOTYPE in (3,4))
              begin
                select @I_vORD = ORD
                from   POP10110 (nolock)
                where  PONUMBER = @I_vPONUMBER
                       and ITEMNMBR = @I_vITEMNMBR
                       and LineNumber = @I_vLineNumber
              end
          
          select @POTYPE = POTYPE,
                 @I_vPOTYPE = case 
                                when @I_vPOTYPE is null then POTYPE
                                else @I_vPOTYPE
                              end,
                 @I_vProjNum = case 
                                 when @I_vProjNum is null then ProjNum
                                 else @I_vProjNum
                               end,
                 @I_vCostCatID = case 
                                   when @I_vCostCatID is null then CostCatID
                                   else @I_vCostCatID
                                 end,
                 @I_vVENDORID = case 
                                  when @I_vVENDORID is null 
                                        or @I_vVENDORID = '' then VENDORID
                                  else @I_vVENDORID
                                end,
                 @cLOCNCODE = LOCNCODE,
                 @I_vLOCNCODE = case 
                                  when @I_vLOCNCODE is null then LOCNCODE
                                  else @I_vLOCNCODE
                                end,
                 @I_vVNDITNUM = case 
                                  when @I_vVNDITNUM is null then VNDITNUM
                                  else @I_vVNDITNUM
                                end,
                 @I_vQUANTITY = case 
                                  when @I_vQUANTITY is null then QTYORDER
                                  else @I_vQUANTITY
                                end,
                 @I_vQTYCANCE = case 
                                  when @I_vQTYCANCE is null 
                                       and POLNESTA in (6)
                                       and @I_vPOLNESTA in (3) then 0
                                  when @I_vQTYCANCE is null then QTYCANCE
                                  else @I_vQTYCANCE
                                end,
                 @cQTYCANCE = QTYCANCE,
                 @I_vFREEONBOARD = case 
                                     when @I_vFREEONBOARD is null then FREEONBOARD
                                     else @I_vFREEONBOARD
                                   end,
                 @I_vREQSTDBY = case 
                                  when @I_vREQSTDBY is null then REQSTDBY
                                  else @I_vREQSTDBY
                                end,
                 @cCOMMNTID = COMMNTID,
                 @I_vCOMMNTID = case 
                                  when @I_vCOMMNTID is null then COMMNTID
                                  else @I_vCOMMNTID
                                end,
                 @I_vREQDATE = case 
                                 when @I_vREQDATE is null then REQDATE
                                 else @I_vREQDATE
                               end,
                 @I_vRELEASEBYDATE = case 
                                       when @I_vRELEASEBYDATE is null then RELEASEBYDATE
                                       else @I_vRELEASEBYDATE
                                     end,
                 @I_vPRMDATE = case 
                                 when @I_vPRMDATE is null then PRMDATE
                                 else @I_vPRMDATE
                               end,
                 @I_vPRMSHPDTE = case 
                                   when @I_vPRMSHPDTE is null then PRMSHPDTE
                                   else @I_vPRMSHPDTE
                                 end,
                 @I_vIVIVINDX = case 
                                  when @I_vIVIVINDX is null 
                                       and @I_vPOTYPE <> POTYPE then 0
                                  when @I_vIVIVINDX is null 
                                       and @I_vPOTYPE = POTYPE then INVINDX
                                  else @I_vIVIVINDX
                                end,
                 @I_vITEMDESC = case 
                                  when @I_vITEMDESC is null then ITEMDESC
                                  else @I_vITEMDESC
                                end,
                 @I_vUNITCOST = case 
                                  when @I_vUNITCOST is null then ORUNTCST
                                  else @I_vUNITCOST
                                end,
                 @I_vVNDITDSC = case 
                                  when @I_vVNDITDSC is null then VNDITDSC
                                  else @I_vVNDITDSC
                                end,
                 @UOFM = UOFM,
                 @I_vUOFM = case 
                              when @I_vUOFM is null then UOFM
                              else @I_vUOFM
                            end,
                 @I_vPurchase_IV_Item_Taxable = case 
                                                  when @I_vPurchase_IV_Item_Taxable is null then Purchase_IV_Item_Taxable
                                                  else @I_vPurchase_IV_Item_Taxable
                                                end,
                 @I_vPurchase_Item_Tax_Schedu = case 
                                                  when @I_vPurchase_Item_Tax_Schedu is null then Purchase_Item_Tax_Schedu
                                                  else @I_vPurchase_Item_Tax_Schedu
                                                end,
                 @I_vPurchase_Site_Tax_Schedu = case 
                                                  when @I_vPurchase_Site_Tax_Schedu is null then Purchase_Site_Tax_Schedu
                                                  else @I_vPurchase_Site_Tax_Schedu
                                                end,
                 @I_vBSIVCTTL = case 
                                  when @I_vBSIVCTTL is null then BSIVCTTL
                                  else @I_vBSIVCTTL
                                end,
                 @I_vTAXAMNT = case 
                                 when @I_vTAXAMNT is null then ORTAXAMT
                                 else @I_vTAXAMNT
                               end,
                 @I_vBCKTXAMT = case 
                                  when @I_vBCKTXAMT is null then OBTAXAMT
                                  else @I_vBCKTXAMT
                                end,
                 @I_vLanded_Cost_Group_ID = case 
                                              when @I_vLanded_Cost_Group_ID is null then Landed_Cost_Group_ID
                                              else @I_vLanded_Cost_Group_ID
                                            end,
                 @I_vPLNNDSPPLID = case 
                                     when @I_vPLNNDSPPLID is null then PLNNDSPPLID
                                     else @I_vPLNNDSPPLID
                                   end,
                 @I_vSHIPMTHD = case 
                                  when @I_vSHIPMTHD is null then SHIPMTHD
                                  else @I_vSHIPMTHD
                                end,
                 @I_vBackoutTradeDiscTax = case 
                                             when @I_vBackoutTradeDiscTax is null then OrigBackoutTradeDiscTax
                                             else @I_vBackoutTradeDiscTax
                                           end,
                 @I_vPOLNESTA = case 
                                  when @I_vPOLNESTA is null then POLNESTA
                                  else @I_vPOLNESTA
                                end,
                 @cPOLNESTA = POLNESTA,
                 @I_vADRSCODE = case 
                                  when @I_vADRSCODE is null then ADRSCODE
                                  else @I_vADRSCODE
                                end,
                 @I_vCMPNYNAM = case 
                                  when @I_vCMPNYNAM is null then CMPNYNAM
                                  else @I_vCMPNYNAM
                                end,
                 @I_vCONTACT = case 
                                 when @I_vCONTACT is null then CONTACT
                                 else @I_vCONTACT
                               end,
                 @I_vADDRESS1 = case 
                                  when @I_vADDRESS1 is null then ADDRESS1
                                  else @I_vADDRESS1
                                end,
                 @I_vADDRESS2 = case 
                                  when @I_vADDRESS2 is null then ADDRESS2
                                  else @I_vADDRESS2
                                end,
                 @I_vADDRESS3 = case 
                                  when @I_vADDRESS3 is null then ADDRESS3
                                  else @I_vADDRESS3
                                end,
                 @I_vCITY = case 
                              when @I_vCITY is null then CITY
                              else @I_vCITY
                            end,
                 @I_vSTATE = case 
                               when @I_vSTATE is null then STATE
                               else @I_vSTATE
                             end,
                 @I_vZIPCODE = case 
                                 when @I_vZIPCODE is null then ZIPCODE
                                 else @I_vZIPCODE
                               end,
                 @I_vCCode = case 
                               when @I_vCCode is null then CCode
                               else @I_vCCode
                             end,
                 @I_vCOUNTRY = case 
                                 when @I_vCOUNTRY is null then COUNTRY
                                 else @I_vCOUNTRY
                               end,
                 @I_vPHONE1 = case 
                                when @I_vPHONE1 is null then PHONE1
                                else @I_vPHONE1
                              end,
                 @I_vPHONE2 = case 
                                when @I_vPHONE2 is null then PHONE2
                                else @I_vPHONE2
                              end,
                 @I_vPHONE3 = case 
                                when @I_vPHONE3 is null then PHONE3
                                else @I_vPHONE3
                              end,
                 @I_vFAX = case 
                             when @I_vFAX is null then FAX
                             else @I_vFAX
                           end,
                 @I_vCURNCYID = case 
                                  when @I_vCURNCYID is null then CURNCYID
                                  else @I_vCURNCYID
                                end,
                 @cUNITCOST = ORUNTCST,
                 @cQUANTITY = QTYORDER,
                 @cUOFM = UOFM,
                 @cReleased_Date = Released_Date,
                 @POLNEARY_5 = POLNEARY_5,
                 @cUMQTYINB = UMQTYINB,
                 @I_vORD = ORD,
                 @I_vLineNumber = LineNumber
          from   POP10110 (UPDLOCK)
          where  PONUMBER = @I_vPONUMBER
                 and ITEMNMBR = @I_vITEMNMBR
                 and ORD = case 
                             when @I_vORD is not null then @I_vORD
                             else ORD
                           end
                 and LineNumber = case 
                                    when @I_vLineNumber is not null then @I_vLineNumber
                                    else LineNumber
                                  end
          
          if (@I_vCMMTTEXT is null 
               or @I_vCOMMENT_1 is null
               or @I_vCOMMENT_2 is null
               or @I_vCOMMENT_3 is null
               or @I_vCOMMENT_4 is null)
            begin
              select @cCMMTTEXT = convert(varchar(500),CMMTTEXT),
                     @cCOMMENT_1 = COMMENT_1,
                     @cCOMMENT_2 = COMMENT_2,
                     @cCOMMENT_3 = COMMENT_3,
                     @cCOMMENT_4 = COMMENT_4
              from   POP10550 (UPDLOCK)
              where  POPNUMBE = @I_vPONUMBER
                     and ORD = @I_vORD
            end
          
          if @I_vCMMTTEXT is null 
             and @I_vCOMMENT_1 is null
             and @I_vCOMMENT_2 is null
             and @I_vCOMMENT_3 is null
             and @I_vCOMMENT_4 is null
            begin
              select @I_vCMMTTEXT = @cCMMTTEXT
            end
          else
            if @I_vCMMTTEXT is null
              begin
                select @I_vCMMTTEXT = ''
              end
          
          if @I_vCMMTTEXT is null
            begin
              if @I_vCOMMENT_1 is null
                select @I_vCOMMENT_1 = @cCOMMENT_1
              
              if @I_vCOMMENT_2 is null
                select @I_vCOMMENT_2 = @cCOMMENT_2
              
              if @I_vCOMMENT_3 is null
                select @I_vCOMMENT_3 = @cCOMMENT_3
              
              if @I_vCOMMENT_4 is null
                select @I_vCOMMENT_4 = @cCOMMENT_4
            end
          else
            begin
              if @I_vCOMMENT_1 is null
                select @I_vCOMMENT_1 = ''
              
              if @I_vCOMMENT_2 is null
                select @I_vCOMMENT_2 = ''
              
              if @I_vCOMMENT_3 is null
                select @I_vCOMMENT_3 = ''
              
              if @I_vCOMMENT_4 is null
                select @I_vCOMMENT_4 = ''
            end
          
          select @I_vCUSTNMBR = case 
                                  when @I_vCUSTNMBR is null 
                                       and @I_vPOTYPE in (2,4) then CUSTNMBR
                                  when @I_vCUSTNMBR is null 
                                       and @I_vPOTYPE in (1,3) then ''
                                  else @I_vCUSTNMBR
                                end
          from   POP10100 (nolock)
          where  PONUMBER = @I_vPONUMBER
          
          if @cLOCNCODE <> @I_vLOCNCODE
              or @cUOFM <> @I_vUOFM
                 and exists (select 1
                             from   POP10500 (nolock)
                             where  PONUMBER = @I_vPONUMBER
                                    and POLNENUM = @I_vORD)
            begin
              select @O_iErrorState = 9208
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      
      select @POSTATUS = POSTATUS
      from   POP10100 (nolock)
      where  PONUMBER = @I_vPONUMBER
      
      if @POSTATUS in (4,5,6)
        begin
          select @O_iErrorState = 8248
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if @LineCount > 0
        begin
          if @I_vLineNumber is null
            begin
              select @I_vLineNumber = isnull(max(LineNumber),0)
              from   POP10110 (nolock)
              where  PONUMBER = @I_vPONUMBER
              
              select @I_vLineNumber = @I_vLineNumber + 1
            end
        end
      
      if (@I_vNOTETEXT is null)
        begin
          select @I_vNOTETEXT = TXTFIELD
          from   SY03900 (nolock)
          where  NOTEINDX = @POLNEARY_5
        end
    end
  
  if @I_vUpdateIfExists = 0
     and exists (select 1
                 from   POP10100 (nolock)
                 where  PONUMBER = @I_vPONUMBER)
    begin
      select @O_iErrorState = 9127
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vUpdateIfExists = 0
      or @LineCount = 0
      or (@I_vUpdateIfExists = 1
          and @I_vORD is not null
          and not exists (select 1
                          from   POP10110 (nolock)
                          where  PONUMBER = @I_vPONUMBER
                                 and ITEMNMBR = @I_vITEMNMBR
                                 and ORD = @I_vORD))
      or (@I_vUpdateIfExists = 1
          and @I_vLineNumber is not null
          and not exists (select 1
                          from   POP10110 (nolock)
                          where  PONUMBER = @I_vPONUMBER
                                 and ITEMNMBR = @I_vITEMNMBR
                                 and LineNumber = @I_vLineNumber))
    begin
      if @I_vProjNum is NULL
        select @I_vProjNum = ''
      
      if @I_vCostCatID is NULL
        select @I_vCostCatID = ''
      
      if @I_vPOTYPE is NULL
        select @I_vPOTYPE = 1
      
      if @I_vLOCNCODE is NULL
        select @I_vLOCNCODE = ''
      
      if @I_vITEMNMBR is NULL
        select @I_vITEMNMBR = ''
      
      if @I_vREQSTDBY is NULL
        select @I_vREQSTDBY = ''
      
      if @I_vCOMMNTID is NULL
        select @I_vCOMMNTID = ''
      
      if @I_vCOMMENT_1 is NULL
        select @I_vCOMMENT_1 = ''
      
      if @I_vCOMMENT_2 is NULL
        select @I_vCOMMENT_2 = ''
      
      if @I_vCOMMENT_3 is NULL
        select @I_vCOMMENT_3 = ''
      
      if @I_vCOMMENT_4 is NULL
        select @I_vCOMMENT_4 = ''
      
      if @I_vREQDATE is NULL
        select @I_vREQDATE = ''
      
      if @I_vRELEASEBYDATE is NULL
        select @I_vRELEASEBYDATE = ''
      
      if @I_vPRMDATE is NULL
        select @I_vPRMDATE = ''
      
      if @I_vPRMSHPDTE is NULL
        select @I_vPRMSHPDTE = ''
      
      if @I_vNONINVEN is NULL
        select @I_vNONINVEN = 0
      
      if @I_vIVIVINDX is NULL
        select @I_vIVIVINDX = 0
      
      if @I_vInventoryAccount is NULL
        select @I_vInventoryAccount = ''
      
      if @I_vITEMDESC is NULL
        select @I_vITEMDESC = ''
      
      if @I_vVNDITNUM is NULL
        select @I_vVNDITNUM = ''
      
      if @I_vVNDITDSC is NULL
        select @I_vVNDITDSC = ''
      
      if @I_vUOFM is NULL
        select @I_vUOFM = ''
      
      if @I_vPOLNESTA is NULL
        select @I_vPOLNESTA = 1
      
      if @I_vCMMTTEXT is NULL
        select @I_vCMMTTEXT = ''
      
      if @I_vCURNCYID is NULL
        select @I_vCURNCYID = ''
      
      if @I_vBSIVCTTL is NULL
        select @I_vBSIVCTTL = 0
      
      if @I_vTAXAMNT is NULL
        select @I_vTAXAMNT = 0
      
      if @I_vBCKTXAMT is NULL
        select @I_vBCKTXAMT = 0
      
      if @I_vPLNNDSPPLID is NULL
        select @I_vPLNNDSPPLID = 0
      
      if @I_vBackoutTradeDiscTax is NULL
        select @I_vBackoutTradeDiscTax = 0
      
      if @I_vPOLNESTA is NULL
        select @I_vPOLNESTA = 1
      
      if @I_vORD is NULL
        select @I_vORD = 0
      
      if @I_vCUSTNMBR is NULL
        select @I_vCUSTNMBR = ''
      
      if @I_vQTYCANCE is NULL
        select @I_vQTYCANCE = 0
      
      if @I_vQUANTITY is NULL
        select @I_vQUANTITY = 0
      
      if @I_vNOTETEXT is NULL
        select @I_vNOTETEXT = ''
      
      exec @iStatus = DYNAMICS..RmssmGetNextNoteIndex
        @I_sCompanyID = @CMPANYID ,
        @I_iSQLSessionID = 0 ,
        @I_noteincrement = 1 ,
        @O_mNoteIndex = @POLNEARY_5 output ,
        @O_iErrorState = @iGetNextNoteIdxErrState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iGetNextNoteIdxErrState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + @iGetNextNoteIdxErrState
          
          select @O_iErrorState = 8211
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPONUMBER is NULL 
       or @I_vLOCNCODE is NULL
       or @I_vITEMNMBR is NULL
       or @I_vREQSTDBY is NULL
       or @I_vCOMMNTID is NULL
       or @I_vCOMMENT_1 is NULL
       or @I_vCOMMENT_2 is NULL
       or @I_vCOMMENT_3 is NULL
       or @I_vCOMMENT_4 is NULL
       or @I_vREQDATE is NULL
       or @I_vRELEASEBYDATE is NULL
       or @I_vPRMDATE is NULL
       or @I_vPRMSHPDTE is NULL
       or @I_vNONINVEN is NULL
       or @I_vIVIVINDX is NULL
       or @I_vInventoryAccount is NULL
       or @I_vITEMDESC is NULL
       or @I_vVNDITNUM is NULL
       or @I_vVNDITDSC is NULL
       or @I_vUOFM is NULL
       or @I_vQUANTITY is NULL
       or @I_vPOLNESTA is NULL
       or @I_vCMMTTEXT is NULL
       or @I_vCURNCYID is NULL
       or @I_vProjNum is NULL
       or @I_vCostCatID is NULL)
    begin
      select @O_iErrorState = 238
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vCOMMNTID = 'untitled*')
    begin
      select @I_vCOMMNTID = ''
    end
  
  if @I_vLineNumber is null 
     and @I_vPOTYPE in (3,4)
     and @LineCount = 0
    select @I_vLineNumber = 0
  else
    if (@I_vLineNumber is null 
        and @I_vPOTYPE in (3,4)
        and @LineCount > 0)
        or (@LineCount = 0
             or @I_vUpdateIfExists = 0
                and @I_vLineNumber is null)
      begin
        select @I_vLineNumber = isnull(max(LineNumber),0)
        from   POP10110 (nolock)
        where  PONUMBER = @I_vPONUMBER
        
        select @I_vLineNumber = @I_vLineNumber + 1
      end
  
  select @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 (nolock)
  where  INTERID = db_name()
  
  if (@I_vNONINVEN = 1)
    begin
      if (exists (select 1
                  from   IV00101 (nolock)
                  where  ITEMNMBR = @I_vITEMNMBR))
        begin
          select @O_iErrorState = 5466
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vNONINVEN = 0)
    begin
      if (@I_vVNDITNUM = ''
          and @I_vITEMNMBR = '')
        begin
          select @O_iErrorState = 396
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      else
        begin
          select @VendItemCount = count(VENDORID)
          from   IV00103 (nolock)
          where  VENDORID = @I_vVENDORID
                 and (VNDITNUM = @I_vVNDITNUM
                       or @I_vVNDITNUM = '')
                 and (ITEMNMBR = @I_vITEMNMBR
                       or @I_vITEMNMBR = '')
          
          if (@VendItemCount = 1)
            begin
              select @I_vITEMNMBR = ITEMNMBR,
                     @I_vVNDITNUM = VNDITNUM,
                     @VNDITDSC = VNDITDSC,
                     @UNITCOST = Last_Originating_Cost
              from   IV00103 (nolock)
              where  VENDORID = @I_vVENDORID
                     and (VNDITNUM = @I_vVNDITNUM
                           or @I_vVNDITNUM = '')
                     and (ITEMNMBR = @I_vITEMNMBR
                           or @I_vITEMNMBR = '')
              
              if (@I_vVNDITNUM = '')
                begin
                  select @O_iErrorState = 9002
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
          else
            begin
              if (@VendItemCount = 0)
                begin
                  select @O_iErrorState = 397
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
              else
                begin
                  select @O_iErrorState = 234
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                end
            end
        end
    end
  else
    begin
      if ((@I_vITEMNMBR <> '')
          and (@I_vVNDITNUM = ''))
        begin
          select @I_vVNDITNUM = @I_vITEMNMBR,
                 @VNDITDSC = ''
        end
      else
        begin
          if ((@I_vVNDITNUM <> '')
              and (@I_vITEMNMBR = ''))
            begin
              select @I_vITEMNMBR = @I_vVNDITNUM,
                     @I_vVNDITNUM = @I_vVNDITNUM,
                     @VNDITDSC = ''
            end
          else
            begin
              select @I_vITEMNMBR = @I_vITEMNMBR,
                     @I_vVNDITNUM = @I_vVNDITNUM
            end
        end
      
      if ((@I_vITEMNMBR = '')
          and (@I_vVNDITNUM = ''))
        begin
          select @O_iErrorState = 246
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vUpdateIfExists = 1)
    begin
      if (@I_vPOLNESTA = 6)
        begin
          select @I_vQTYCANCE = @I_vQUANTITY
        end
      
      if @cPOLNESTA in (1)
         and @I_vPOLNESTA in (3,4,5)
        begin
          select @O_iErrorState = 9188
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @cPOLNESTA in (2)
         and @I_vPOLNESTA in (1)
        begin
          select @O_iErrorState = 9189
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @cPOLNESTA in (3)
         and @I_vPOLNESTA in (1)
        begin
          select @O_iErrorState = 9223
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @cPOLNESTA in (3)
         and @I_vPOLNESTA in (2)
        begin
          select @O_iErrorState = 9191
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @cPOLNESTA in (4)
         and @I_vPOLNESTA in (1)
        begin
          select @O_iErrorState = 9192
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @cPOLNESTA in (4)
         and @I_vPOLNESTA in (2)
        begin
          select @O_iErrorState = 9193
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @cPOLNESTA in (4)
         and @I_vPOLNESTA in (6)
        begin
          select @O_iErrorState = 9194
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @cPOLNESTA in (5)
        begin
          select @O_iErrorState = 9195
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @cPOLNESTA in (6)
         and @I_vPOLNESTA in (4,5)
        begin
          select @O_iErrorState = 9196
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @I_vPOLNESTA in (5)
         and exists (select 1
                     from   POP10500 (nolock)
                     where  PONUMBER = @I_vPONUMBER
                            and POLNENUM = @I_vORD
                            and Status = 0)
        begin
          select @O_iErrorState = 9228
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @cQTYSHPPD0 = isnull(sum(QTYSHPPD * UMQTYINB),0),
             @cQTYINVCD0 = isnull(sum(QTYINVCD * UMQTYINB),0),
             @cQTYREJ0 = isnull(sum(QTYREJ * UMQTYINB),0),
             @cQTYMATCH0 = isnull(sum(QTYMATCH * UMQTYINB),0),
             @cNETQTYSHPP0 = isnull(sum((QTYSHPPD - QTYREJ) * UMQTYINB),0)
      from   POP10500 (nolock)
      where  PONUMBER = @I_vPONUMBER
             and POLNENUM = @I_vORD
             and Status = 0
      
      select @cQTYSHPPD1 = isnull(sum(QTYSHPPD * UMQTYINB),0),
             @cQTYINVCD1 = isnull(sum(QTYINVCD * UMQTYINB),0),
             @cQTYREJ1 = isnull(sum(QTYREJ * UMQTYINB),0),
             @cQTYMATCH1 = isnull(sum(QTYMATCH * UMQTYINB),0),
             @cNETQTYSHPP1 = isnull(sum((QTYSHPPD - QTYREJ) * UMQTYINB),0)
      from   POP10500 (nolock)
      where  PONUMBER = @I_vPONUMBER
             and POLNENUM = @I_vORD
             and Status = 1
      
      if (@cQTYSHPPD0 + @cQTYINVCD0 + @cQTYSHPPD1 + @cQTYINVCD1 + @cQTYMATCH0 + @cQTYMATCH1) > 0
         and @I_vPOTYPE <> @POTYPE
        begin
          select @O_iErrorState = 7771
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@cQTYSHPPD1 + @cQTYSHPPD0) > @I_vQUANTITY
         and @I_vQUANTITY <> @cQUANTITY
        begin
          select @O_iErrorState = 9230
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @I_vPOLNESTA in (5)
         and (@cQTYSHPPD1 - @cQTYREJ1) > @cQTYMATCH1
         and @cQTYMATCH1 <> 0
        begin
          select @O_iErrorState = 9209
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @I_vPOLNESTA in (6)
         and (@cQTYSHPPD0 > 0
               or @cQTYSHPPD1 > 0)
        begin
          select @O_iErrorState = 9211
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @I_vPOLNESTA in (4,5)
         and (@cQTYSHPPD1 = 0
               or @cQTYINVCD1 = 0)
        begin
          select @O_iErrorState = 9213
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @I_vPOLNESTA in (4,5)
         and (@cQTYSHPPD0 > 0)
        begin
          select @O_iErrorState = 9214
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @I_vPOLNESTA in (4)
         and (@cQTYSHPPD0 = @cQTYINVCD0)
        begin
          select @O_iErrorState = 9216
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if @I_vPOLNESTA in (4)
         and @I_vPOTYPE in (2,4)
        begin
          select @O_iErrorState = 9217
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if @I_vUpdateIfExists = 1
     and (@I_vQTYCANCE <> @cQTYCANCE
           or @cUNITCOST <> @I_vUNITCOST
           or @cQUANTITY <> @I_vQUANTITY)
     and @cPOLNESTA = @I_vPOLNESTA
     and @cPOLNESTA = 2
    begin
      select @I_vPOLNESTA = 3
    end
  
  if (@I_vPOTYPE not in (1,2,3,4))
    begin
      select @O_iErrorState = 377
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (not exists (select 1
                  from   PM00200 (nolock)
                  where  VENDORID = @I_vVENDORID))
    begin
      select @O_iErrorState = 252
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPOTYPE = 3)
     and (@I_vCostCatID <> '')
    begin
      select @O_iErrorState = 8209
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPOTYPE = 4)
     and (@I_vCostCatID <> '')
    begin
      select @O_iErrorState = 8210
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vPurchase_IV_Item_Taxable = case 
                                          when (@I_vPurchase_IV_Item_Taxable > 0) then @I_vPurchase_IV_Item_Taxable
                                          when (@USEADVTX = 0) then @Purchase_Tax_Options
                                          else 3
                                        end
  
  select @I_vPurchase_Item_Tax_Schedu = case 
                                          when @I_vPurchase_Item_Tax_Schedu <> '' then @I_vPurchase_Item_Tax_Schedu
                                          when (@Purchase_Tax_Options = 1)
                                               and (@USEADVTX = 0) then @Purchase_Item_Tax_Schedu
                                          when (@Purchase_Tax_Options = 2)
                                               and (@USEADVTX = 0) then ''
                                          when (@Purchase_Tax_Options = 3)
                                               and (@USEADVTX = 0) then @TAXSCHID
                                          when (@TAXSCHID <> '') then @TAXSCHID
                                          else @COPTXSCH
                                        end
  
  if (@I_vNONINVEN = 0)
    begin
      select @I_vLOCNCODE = case 
                              when @I_vLOCNCODE = '' then LOCNCODE
                              else @I_vLOCNCODE
                            end,
             @ITMTRKOP = ITMTRKOP,
             @ITEMDESC = ITEMDESC,
             @INVINDX = case 
                          when @I_vPOTYPE in (1,3) then IVIVINDX
                          else DPSHPIDX
                        end,
             @ITEMTYPE = ITEMTYPE,
             @VCTNMTHD = VCTNMTHD,
             @DECPLQTY = ((DECPLQTY - 1) + 1),
             @UOMSCHDL = UOMSCHDL,
             @I_vPurchase_IV_Item_Taxable = case 
                                              when @I_vPurchase_IV_Item_Taxable is null 
                                                    or @I_vPurchase_IV_Item_Taxable = 0 then Purchase_Tax_Options
                                              else @I_vPurchase_IV_Item_Taxable
                                            end,
             @I_vPurchase_Item_Tax_Schedu = case 
                                              when @I_vPurchase_Item_Tax_Schedu is null then Purchase_Item_Tax_Schedu
                                              else @I_vPurchase_Item_Tax_Schedu
                                            end
      from   IV00101 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
    end
  else
    begin
      select @UOFM = 'Each',
             @ITMTRKOP = 1,
             @ITEMDESC = '',
             @UNITCOST = 0,
             @INVINDX = 0,
             @ITEMTYPE = 0,
             @VCTNMTHD = 0,
             @UMQTYINB = 1,
             @I_vPurchase_IV_Item_Taxable = case 
                                              when @I_vPurchase_IV_Item_Taxable is null 
                                                    or @I_vPurchase_IV_Item_Taxable = 0 then 2
                                              else @I_vPurchase_IV_Item_Taxable
                                            end,
             @I_vPurchase_Item_Tax_Schedu = case 
                                              when @I_vPurchase_Item_Tax_Schedu is null then ''
                                              else @I_vPurchase_Item_Tax_Schedu
                                            end
      
      select @DECPLQTY = DECPLQTY,
             @DECPLCUR = DECPLCUR,
             @ODECPLCU = (DECPLCUR - 1)
      from   POP40100 (nolock)
      where  INDEX1 = 1
      
      if (@I_vPOTYPE in (2,4))
        select @INVINDX = ACTINDX
        from   SY01100 (nolock)
        where  SERIES = 5
               and SEQNUMBR = 1200
    end
  
  select top 1 @LOCNCODE = LOCNCODE
  from   IV40700 (nolock)
  where  LOCNCODE <> ''
  
  if (@I_vLOCNCODE = '')
    begin
      if (@LOCNCODE <> '')
        begin
          select @O_iErrorState = 254
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      if (not exists (select 1
                      from   IV40700 (nolock)
                      where  LOCNCODE = @I_vLOCNCODE))
        begin
          select @O_iErrorState = 256
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@I_vNONINVEN = 0)
          and (not exists (select 1
                           from   IV00102 (nolock)
                           where  ITEMNMBR = @I_vITEMNMBR
                                  and LOCNCODE = @I_vLOCNCODE)))
        begin
          select @O_iErrorState = 261
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vCURNCYID = '')
    begin
      select @I_vCURNCYID = isnull(CURNCYID,'')
      from   PM00200 (nolock)
      where  VENDORID = @I_vVENDORID
      
      if (@I_vCURNCYID <> '')
        begin
          select @CURRNIDX = isnull(CURRNIDX,0)
          from   DYNAMICS..MC40200 (nolock)
          where  CURNCYID = @I_vCURNCYID
        end
      else
        begin
          select @I_vCURNCYID = isnull(FUNLCURR,''),
                 @CURRNIDX = isnull(FUNCRIDX,0)
          from   MC40000 (nolock)
        end
    end
  else
    begin
      select @CURRNIDX = isnull(CURRNIDX,0)
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @I_vCURNCYID
    end
  
  select @FUNLCURR = FUNLCURR
  from   MC40000 (nolock)
  
  if ((@I_vCURNCYID <> '')
      and (@I_vCURNCYID <> @FUNLCURR))
    begin
      select @ISMCTRX = 1
    end
  
  if ((@CURRNIDX = 0)
       or (@I_vCURNCYID = ''))
    begin
      select @O_iErrorState = 6620
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@ITEMTYPE in (2,3))
    begin
      select @O_iErrorState = 9338
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vNONINVEN = 0)
    begin
      if (@ISMCTRX = 1)
        begin
          select @DECPLCUR = DECPLCUR - 1,
                 @ODECPLCU = DECPLCUR - 1
          from   IV00105 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and CURNCYID = @I_vCURNCYID
          
          select @ITEMFUNCTDEC = DECPLCUR - 1
          from   IV00105 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and CURNCYID = @FUNLCURR
        end
      else
        begin
          select top 1 @DECPLCUR = DECPLCUR - 1,
                       @ODECPLCU = DECPLCUR - 1
          from   IV00105 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and (CURNCYID = ''
                       or CURNCYID = @I_vCURNCYID)
          
          select top 1 @ITEMFUNCTDEC = DECPLCUR - 1
          from   IV00105 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and (CURNCYID = ''
                       or CURNCYID = @FUNLCURR)
        end
    end
  else
    begin
      select @ITEMFUNCTDEC = DECPLCUR - 1,
             @ODECPLCU = DECPLCUR - 1
      from   POP40600 (nolock)
      where  CURNCYID = @FUNLCURR
    end
  
  select @FUNDECPLCUR = DECPLCUR - 1
  from   MC40000 a (nolock),
         DYNAMICS..MC40200 b (nolock)
  where  a.FUNCRIDX = b.CURRNIDX
         and a.FUNLCURR = b.CURNCYID
         and a.FUNLCURR = @FUNLCURR
  
  if (@ISMCTRX = 1)
    begin
      select @EDITDECPLCUR = DECPLCUR - 1
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @I_vCURNCYID
    end
  else
    begin
      select @EDITDECPLCUR = @FUNDECPLCUR
    end
  
  if ((@I_vNONINVEN = 1)
      and exists (select 1
                  from   POP40600 (nolock)
                  where  CURNCYID = @I_vCURNCYID))
    begin
      select @DECPLCUR = DECPLCUR - 1
      from   POP40600 (nolock)
      where  CURNCYID = @I_vCURNCYID
    end
  
  if (round(@I_vUNITCOST,@DECPLCUR) <> (@I_vUNITCOST))
    begin
      select @O_iErrorState = 6621
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (not exists (select 1
                  from   IV00101 (nolock)
                  where  ITEMNMBR = @I_vITEMNMBR)
      and (@I_vNONINVEN = 0))
    begin
      select @O_iErrorState = 304
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vFREEONBOARD is null)
       or (@I_vFREEONBOARD = 0))
    begin
      if (@I_vNONINVEN = 0)
        begin
          select @FREEONBOARD = FREEONBOARD
          from   IV00103 (nolock)
          where  VENDORID = @I_vVENDORID
                 and ITEMNMBR = @I_vITEMNMBR
                 and VNDITNUM = @I_vVNDITNUM
        end
      else
        begin
          select @FREEONBOARD = 1
        end
    end
  else
    begin
      select @FREEONBOARD = @I_vFREEONBOARD
    end
  
  if (@FREEONBOARD not in (1,2,3))
    begin
      select @O_iErrorState = 387
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vREQDATE = '')
    begin
      select @I_vREQDATE = convert(varchar(12),getdate())
    end
  
  if (@I_vNONINVEN = 0)
    begin
      select @PLANNINGLEADTIME = PLANNINGLEADTIME
      from   IV00103 (nolock)
      where  VENDORID = @I_vVENDORID
             and ITEMNMBR = @I_vITEMNMBR
    end
  else
    begin
      select @PLANNINGLEADTIME = 0
    end
  
  if (@I_vREQDATE < @I_vRELEASEBYDATE)
    begin
      select @O_iErrorState = 393
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vCMMTTEXT <> @cCMMTTEXT)
      and (@I_vCMMTTEXT <> '')
      and ((@I_vCOMMENT_1 <> '')
            or (@I_vCOMMENT_2 <> '')
            or (@I_vCOMMENT_3 <> '')
            or (@I_vCOMMENT_4 <> '')))
    begin
      select @O_iErrorState = 2295
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPRMDATE = '')
    begin
      select @I_vPRMDATE = convert(varchar(12),getdate())
    end
  
  if (@I_vPRMSHPDTE = '')
    begin
      select @I_vPRMSHPDTE = convert(varchar(12),getdate())
    end
  
  if (@I_vNONINVEN = 0)
    begin
      select @PRCHSUOM = PRCHSUOM
      from   IV00103 (nolock)
      where  VENDORID = @I_vVENDORID
             and ITEMNMBR = @I_vITEMNMBR
             and VNDITNUM = @I_vVNDITNUM
      
      if (@PRCHSUOM = '')
        begin
          select @PRCHSUOM = PRCHSUOM
          from   IV00101 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
        end
      
      if (@PRCHSUOM <> '')
        begin
          select @UOFM = @PRCHSUOM
        end
    end
  
  if (@I_vUOFM <> '')
    begin
      select @UOFM = @I_vUOFM
    end
  
  if ((@I_vNONINVEN = 0)
      and (@LOCNCODE <> ''))
    begin
      if (not exists (select 1
                      from   IV40202 (nolock)
                      where  UOFM = @UOFM
                             and UOMSCHDL = @UOMSCHDL))
        begin
          select @O_iErrorState = 4599
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@UOFM = '')
    begin
      select @O_iErrorState = 1600
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vNONINVEN = 0)
    begin
      select @UMQTYINB = QTYBSUOM
      from   IV00106 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
             and UOFM = @UOFM
      
      if (@UMQTYINB = 0)
        begin
          select @O_iErrorState = 321
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vVNDITDSC <> '')
    begin
      select @VNDITDSC = @I_vVNDITDSC
    end
  
  if (@I_vITEMDESC <> '')
    begin
      select @ITEMDESC = @I_vITEMDESC
    end
  
  if (@I_vInventoryAccount <> '')
    begin
      select @INVINDX = ACTINDX
      from   GL00105 (nolock)
      where  ACTNUMST = @I_vInventoryAccount
      
      if (@INVINDX = 0)
        begin
          select @O_iErrorState = 442
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      if (@I_vIVIVINDX <> 0)
        begin
          select @INVINDX = ACTINDX
          from   GL00105 (nolock)
          where  ACTINDX = @I_vIVIVINDX
          
          if (@INVINDX = 0)
            begin
              select @O_iErrorState = 443
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      else
        begin
          if (@INVINDX = 0)
            begin
              if (@I_vPOTYPE in (1,3))
                begin
                  if (@I_vNONINVEN = 1)
                    begin
                      select @INVINDX = PMPRCHIX
                      from   PM00200 (nolock)
                      where  VENDORID = @I_vVENDORID
                    end
                  
                  if (@INVINDX = 0)
                    begin
                      select @INVINDX = ACTINDX
                      from   SY01100 (nolock)
                      where  SERIES = 5
                             and SEQNUMBR = 100
                    end
                end
              else
                begin
                  select @INVINDX = ACTINDX
                  from   SY01100 (nolock)
                  where  SERIES = 5
                         and SEQNUMBR = 1100
                end
            end
        end
    end
  
  select @MAXSEG = max(SGMTNUMB)
  from   SY00300 (nolock)
  
  select @Location_Segment = Location_Segment
  from   IV40700 (nolock)
  where  LOCNCODE = @I_vLOCNCODE
  
  select @ACSGFLOC = ACSGFLOC
  from   IV40100 (nolock)
  where  SETUPKEY = 1
  
  if ((@INVINDX <> 0)
      and (@Location_Segment <> '')
      and (@ACSGFLOC <> 0))
    begin
      select @ACTINDX = 0,
             @ACCNT_STRING = '',
             @NEW_ACCNT_STRING = ''
      
      select @ACCNT_STRING = ACTNUMST
      from   GL00105 (nolock)
      where  ACTINDX = @INVINDX
      
      if (@ACSGFLOC < @MAXSEG)
        begin
          select @LOFSGMNT = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
          from   SY00300 (nolock)
          where  SGMTNUMB >= @ACSGFLOC
          
          select @LOFSGMNTEND = sum(LOFSGMNT) + (@MAXSEG - @ACSGFLOC)
          from   SY00300 (nolock)
          where  SGMTNUMB > @ACSGFLOC
        end
      else
        begin
          select @LOFSGMNT = sum(LOFSGMNT)
          from   SY00300 (nolock)
          where  SGMTNUMB = @ACSGFLOC
        end
      
      select @LOFSGMNTALL = sum(LOFSGMNT) + (@MAXSEG - 1)
      from   SY00300 (nolock)
      
      if (@ACSGFLOC = @MAXSEG)
        begin
          select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment))
        end
      else
        begin
          select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@Location_Segment)) + substring(@ACCNT_STRING,@LOFSGMNTALL - @LOFSGMNTEND + 1,
                                                                                                                                       @LOFSGMNTALL)
        end
      
      select @ACTINDX = isnull(ACTINDX,0)
      from   GL00105 (nolock)
      where  ACTNUMST = @NEW_ACCNT_STRING
      
      if (@ACTINDX <> 0)
        select @INVINDX = @ACTINDX
    end
  
  if (@I_vUNITCOST is not null)
    select @UNITCOST = @I_vUNITCOST
  
  if (@UNITCOST < 0)
    begin
      select @O_iErrorState = 395
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vUNITCOST is null)
    select @I_vUNITCOST = @UNITCOST
  
  if ((@I_vNONINVEN <> 0)
      and (@I_vNONINVEN <> 1))
    begin
      select @O_iErrorState = 398
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vQUANTITY < 0)
    begin
      select @O_iErrorState = 253
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vQTYCANCE < 0)
    begin
      select @O_iErrorState = 315
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vQTYCANCE > @I_vQUANTITY)
    begin
      select @O_iErrorState = 323
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vORD < 0)
    begin
      select @O_iErrorState = 8136
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vQUANTITY = @I_vQTYCANCE)
      and (@I_vQUANTITY > 0)
      and (@I_vPOLNESTA = 6))
    begin
      select @CANCELLED = 1
    end
  
  if ((@I_vQTYCANCE * @UMQTYINB) > (@I_vQUANTITY * @UMQTYINB))
    begin
      select @O_iErrorState = 8225
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @QTYUNCMTBASE = (((@I_vQUANTITY - @I_vQTYCANCE) * @UMQTYINB) - (@Rec_QTYSHPPD - @Rec_QTYREJ))
  
  if @QTYUNCMTBASE < 0
    select @QTYUNCMTBASE = 0
  
  if ((@I_vPOLNESTA not in (1,2)))
     and (@I_vUpdateIfExists = 0
           or @LineCount = 0)
     and (@CANCELLED = 0)
    begin
      select @O_iErrorState = 1333
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPOLNESTA < 1)
       or (@I_vPOLNESTA > 6))
    begin
      select @O_iErrorState = 9229
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPOLNESTA = 2)
    begin
      select @I_vRELEASEBYDATE = convert(varchar(12),getdate())
    end
  
  if ((@I_vQUANTITY = @I_vQTYCANCE)
      and (@I_vQUANTITY > 0))
    begin
      select @I_vPOLNESTA = 6
    end
  
  exec @iStatus = glCalculateGLPeriod
    @I_sSeries = 7 ,
    @I_cOrigin = 'PA Receivings Trx Entry' ,
    @I_dDate = @I_vREQDATE ,
    @I_dUserDate = @UserDate ,
    @O_sPeriodID = @PeriodID output ,
    @O_tClosed = @Closed output ,
    @O_sYear = @Year output ,
    @O_iOUTErr = @iCustomGLPeriodErr output ,
    @O_iErrorState = @iCustomGLPeriodState output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomGLPeriodState <> 0)
    begin
      select @O_iErrorState = 8226
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPurchase_IV_Item_Taxable not in (1,2,3))
    begin
      select @O_iErrorState = 880
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  else
    begin
      if (@I_vPurchase_IV_Item_Taxable <> 1)
        begin
          select @I_vPurchase_Item_Tax_Schedu = ''
        end
    end
  
  if (@I_vPurchase_Item_Tax_Schedu <> '')
    begin
      if (not exists (select 1
                      from   TX00101 (nolock)
                      where  TAXSCHID = @I_vPurchase_Item_Tax_Schedu))
        begin
          select @O_iErrorState = 881
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPurchase_Site_Tax_Schedu <> '')
    begin
      if (not exists (select 1
                      from   TX00101 (nolock)
                      where  TAXSCHID = @I_vPurchase_Site_Tax_Schedu))
        begin
          select @O_iErrorState = 882
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPOTYPE in (2,4))
    begin
      if (@I_vCUSTNMBR = '')
        begin
          select @O_iErrorState = 6333
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if not exists (select 1
                     from   RM00101 (nolock)
                     where  CUSTNMBR = @I_vCUSTNMBR)
        begin
          select @O_iErrorState = 6334
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@I_vSHIPMTHD is null)
      and (@I_vPOTYPE in (1,3)))
    begin
      select @I_vSHIPMTHD = SHIPMTHD
      from   PM00200 (nolock)
      where  VENDORID = @I_vVENDORID
      
      if (@I_vSHIPMTHD is null)
        begin
          select @O_iErrorState = 6335
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vLanded_Cost_Group_ID is null)
    begin
      select @I_vLanded_Cost_Group_ID = Landed_Cost_Group_ID
      from   IV00102 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
             and LOCNCODE = @I_vLOCNCODE
      
      if @I_vLanded_Cost_Group_ID is null
        select @I_vLanded_Cost_Group_ID = ''
    end
  else
    begin
      if @I_vLanded_Cost_Group_ID <> ''
         and not exists (select 1
                         from   IV41101
                         where  Landed_Cost_Group_ID = @I_vLanded_Cost_Group_ID)
        begin
          select @O_iErrorState = 9231
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@I_vSHIPMTHD is null)
      and (@I_vPOTYPE in (2,4)))
    begin
      select @I_vSHIPMTHD = SHIPMTHD
      from   RM00101 (nolock)
      where  CUSTNMBR = @I_vCUSTNMBR
      
      if (@I_vSHIPMTHD is null)
        begin
          select @O_iErrorState = 6336
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vSHIPMTHD <> '')
    begin
      select @SHIPTYPE = SHIPTYPE,
             @POLNEARY_9 = NOTEINDX
      from   SY03000 (nolock)
      where  SHIPMTHD = @I_vSHIPMTHD
      
      if (@SHIPTYPE not in (0,1))
        begin
          select @O_iErrorState = 989
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vORD = 0)
    begin
      select @I_vORD = @SEQNUMBR + isnull(max(ORD),0)
      from   POP10110 (nolock)
      where  PONUMBER = @I_vPONUMBER
    end
  else
    begin
      if @I_vUpdateIfExists = 0
         and exists (select 1
                     from   POP10110 (nolock)
                     where  PONUMBER = @I_vPONUMBER
                            and ORD = @I_vORD)
        begin
          select @O_iErrorState = 3806
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vCCode is not null)
     and (@I_vCCode <> '')
    begin
      if (not exists (select 1
                      from   VAT10001 (nolock)
                      where  CCode = @I_vCCode))
        begin
          select @O_iErrorState = 6337
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPOTYPE in (1,3))
    begin
      if (@I_vPurchase_Site_Tax_Schedu is null)
        begin
          if (@SHIPTYPE = 1)
            begin
              select @I_vPurchase_Site_Tax_Schedu = PCTAXSCH
              from   IV40700 (nolock)
              where  LOCNCODE = @I_vLOCNCODE
            end
          else
            if (@SHIPTYPE = 0)
              begin
                select @I_vPurchase_Site_Tax_Schedu = TAXSCHID
                from   PM00200 (nolock)
                where  VENDORID = @I_vVENDORID
              end
            else
              begin
                select @I_vPurchase_Site_Tax_Schedu = ''
              end
        end
      
      if ((@I_vADRSCODE is null)
          and (@SHIPTYPE = 1))
        begin
          select @I_vADRSCODE = @I_vLOCNCODE
        end
      else
        if ((@I_vADRSCODE is null)
            and (@SHIPTYPE = 0))
          begin
            select @I_vADRSCODE = isnull(VADCDPAD,'')
            from   PM00200 (nolock)
            where  VENDORID = @I_vVENDORID
          end
        else
          if ((@I_vADRSCODE is null)
              and (@SHIPTYPE = 3))
            begin
              select @I_vADRSCODE = ''
            end
    end
  else
    if (@I_vPOTYPE in (2,4))
      begin
        if (@I_vPurchase_Site_Tax_Schedu is null)
          begin
            if (@SHIPTYPE = 1)
              begin
                select @I_vPurchase_Site_Tax_Schedu = TAXSCHID
                from   RM00102 (nolock)
                where  CUSTNMBR = @I_vCUSTNMBR
                       and LOCNCODE = @I_vLOCNCODE
              end
            
            if (@SHIPTYPE = 0)
              begin
                select @I_vPurchase_Site_Tax_Schedu = TAXSCHID
                from   PM00200 (nolock)
                where  VENDORID = @I_vVENDORID
              end
            else
              begin
                select @I_vPurchase_Site_Tax_Schedu = ''
              end
          end
        
        if (@I_vADRSCODE is null)
          begin
            select @I_vADRSCODE = isnull(PRSTADCD,'')
            from   RM00101 (nolock)
            where  CUSTNMBR = @I_vCUSTNMBR
            
            if (@I_vADRSCODE = '')
              begin
                select @O_iErrorState = 6338
                
                exec @iStatus = RmsUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
              end
          end
        
        if not exists (select 1
                       from   RM00102
                       where  CUSTNMBR = @I_vCUSTNMBR
                              and ADRSCODE = @I_vADRSCODE)
          begin
            select @O_iErrorState = 6342
            
            exec @iStatus = RmsUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @O_oErrorState output
          end
      end
  
  if (@I_vCCode <> '')
     and (@I_vCOUNTRY is null)
    begin
      select @I_vCOUNTRY = CCodeDesc
      from   VAT10001 (nolock)
      where  CCode = @I_vCCode
    end
  
  select @ADDRSOURCE = case 
                         when @I_vPOTYPE <> @POTYPE then 1
                         when (@I_vSHIPMTHD = '')
                              and (@I_vADRSCODE = '') then 0
                         when (@I_vSHIPMTHD = '')
                              and (@I_vPOTYPE in (2,4)) then 1
                         when (@I_vSHIPMTHD = '')
                              and (@I_vPOTYPE in (1,3)) then 5
                         when (@I_vSHIPMTHD <> '')
                              and (@SHIPTYPE = 0) then 2
                         when (@I_vSHIPMTHD <> '')
                              and (@SHIPTYPE = 1)
                              and (@I_vPOTYPE in (2,4)) then 1
                         when (@I_vSHIPMTHD <> '')
                              and (@SHIPTYPE = 1)
                              and (@I_vPOTYPE in (1,3))
                              and @LOCNCODE <> '' then 3
                         when (@I_vSHIPMTHD <> '')
                              and (@SHIPTYPE = 1)
                              and (@I_vPOTYPE in (1,3))
                              and @LOCNCODE = '' then 4
                         else 0
                       end
  
  if (@ADDRSOURCE = 0)
    begin
      select @I_vADRSCODE = '',
             @I_vCMPNYNAM = case 
                              when (@I_vCMPNYNAM is null) then ''
                              else @I_vCMPNYNAM
                            end,
             @I_vCONTACT = case 
                             when (@I_vCONTACT is null) then ''
                             else @I_vCONTACT
                           end,
             @I_vADDRESS1 = case 
                              when (@I_vADDRESS1 is null) then ''
                              else @I_vADDRESS1
                            end,
             @I_vADDRESS2 = case 
                              when (@I_vADDRESS2 is null) then ''
                              else @I_vADDRESS2
                            end,
             @I_vADDRESS3 = case 
                              when (@I_vADDRESS3 is null) then ''
                              else @I_vADDRESS3
                            end,
             @I_vCITY = case 
                          when (@I_vCITY is null) then ''
                          else @I_vCITY
                        end,
             @I_vSTATE = case 
                           when (@I_vSTATE is null) then ''
                           else @I_vSTATE
                         end,
             @I_vZIPCODE = case 
                             when (@I_vZIPCODE is null) then ''
                             else @I_vZIPCODE
                           end,
             @I_vCCode = case 
                           when (@I_vCCode is null) then ''
                           else @I_vCCode
                         end,
             @I_vCOUNTRY = case 
                             when (@I_vCOUNTRY is null) then ''
                             else @I_vCOUNTRY
                           end,
             @I_vPHONE1 = case 
                            when (@I_vPHONE1 is null) then ''
                            else @I_vPHONE1
                          end,
             @I_vPHONE2 = case 
                            when (@I_vPHONE2 is null) then ''
                            else @I_vPHONE2
                          end,
             @I_vPHONE3 = case 
                            when (@I_vPHONE3 is null) then ''
                            else @I_vPHONE3
                          end,
             @I_vFAX = case 
                         when (@I_vFAX is null) then ''
                         else @I_vFAX
                       end
    end
  else
    if (@ADDRSOURCE = 1)
      begin
        select @I_vCMPNYNAM = case 
                                when (@I_vCMPNYNAM is null) then CUSTNAME
                                else @I_vCMPNYNAM
                              end
        from   RM00101 (nolock)
        where  CUSTNMBR = @I_vCUSTNMBR
        
        select @I_vCONTACT = case 
                               when (@I_vCONTACT is null) then CNTCPRSN
                               else @I_vCONTACT
                             end,
               @I_vADDRESS1 = case 
                                when (@I_vADDRESS1 is null) then ADDRESS1
                                else @I_vADDRESS1
                              end,
               @I_vADDRESS2 = case 
                                when (@I_vADDRESS2 is null) then ADDRESS2
                                else @I_vADDRESS2
                              end,
               @I_vADDRESS3 = case 
                                when (@I_vADDRESS3 is null) then ADDRESS3
                                else @I_vADDRESS3
                              end,
               @I_vCITY = case 
                            when (@I_vCITY is null) then CITY
                            else @I_vCITY
                          end,
               @I_vSTATE = case 
                             when (@I_vSTATE is null) then STATE
                             else @I_vSTATE
                           end,
               @I_vZIPCODE = case 
                               when (@I_vZIPCODE is null) then ZIP
                               else @I_vZIPCODE
                             end,
               @I_vCCode = case 
                             when (@I_vCCode is null) then CCode
                             else @I_vCCode
                           end,
               @I_vCOUNTRY = case 
                               when (@I_vCOUNTRY is null) then COUNTRY
                               else @I_vCOUNTRY
                             end,
               @I_vPHONE1 = case 
                              when (@I_vPHONE1 is null) then PHONE1
                              else @I_vPHONE1
                            end,
               @I_vPHONE2 = case 
                              when (@I_vPHONE2 is null) then PHONE2
                              else @I_vPHONE2
                            end,
               @I_vPHONE3 = case 
                              when (@I_vPHONE3 is null) then PHONE3
                              else @I_vPHONE3
                            end,
               @I_vFAX = case 
                           when (@I_vFAX is null) then FAX
                           else @I_vFAX
                         end
        from   RM00102 (nolock)
        where  CUSTNMBR = @I_vCUSTNMBR
               and ADRSCODE = @I_vADRSCODE
      end
    else
      if (@ADDRSOURCE = 2)
        begin
          select @I_vCMPNYNAM = case 
                                  when (@I_vCMPNYNAM is null) then VENDNAME
                                  else @I_vCMPNYNAM
                                end
          from   PM00200 (nolock)
          where  VENDORID = @I_vVENDORID
          
          select @I_vCONTACT = case 
                                 when (@I_vCONTACT is null) then VNDCNTCT
                                 else @I_vCONTACT
                               end,
                 @I_vADDRESS1 = case 
                                  when (@I_vADDRESS1 is null) then ADDRESS1
                                  else @I_vADDRESS1
                                end,
                 @I_vADDRESS2 = case 
                                  when (@I_vADDRESS2 is null) then ADDRESS2
                                  else @I_vADDRESS2
                                end,
                 @I_vADDRESS3 = case 
                                  when (@I_vADDRESS3 is null) then ADDRESS3
                                  else @I_vADDRESS3
                                end,
                 @I_vCITY = case 
                              when (@I_vCITY is null) then CITY
                              else @I_vCITY
                            end,
                 @I_vSTATE = case 
                               when (@I_vSTATE is null) then STATE
                               else @I_vSTATE
                             end,
                 @I_vZIPCODE = case 
                                 when (@I_vZIPCODE is null) then ZIPCODE
                                 else @I_vZIPCODE
                               end,
                 @I_vCCode = case 
                               when (@I_vCCode is null) then CCode
                               else @I_vCCode
                             end,
                 @I_vCOUNTRY = case 
                                 when (@I_vCOUNTRY is null) then COUNTRY
                                 else @I_vCOUNTRY
                               end,
                 @I_vPHONE1 = case 
                                when (@I_vPHONE1 is null) then PHNUMBR1
                                else @I_vPHONE1
                              end,
                 @I_vPHONE2 = case 
                                when (@I_vPHONE2 is null) then PHNUMBR2
                                else @I_vPHONE2
                              end,
                 @I_vPHONE3 = case 
                                when (@I_vPHONE3 is null) then PHONE3
                                else @I_vPHONE3
                              end,
                 @I_vFAX = case 
                             when (@I_vFAX is null) then FAXNUMBR
                             else @I_vFAX
                           end
          from   PM00300 (nolock)
          where  VENDORID = @I_vVENDORID
                 and ADRSCODE = @I_vADRSCODE
        end
      else
        if (@ADDRSOURCE = 3)
          begin
            select @I_vCMPNYNAM = '',
                   @I_vCONTACT = '',
                   @I_vADDRESS1 = ADDRESS1,
                   @I_vADDRESS2 = ADDRESS2,
                   @I_vADDRESS3 = ADDRESS3,
                   @I_vCITY = CITY,
                   @I_vSTATE = STATE,
                   @I_vZIPCODE = ZIPCODE,
                   @I_vCCode = CCode,
                   @I_vCOUNTRY = COUNTRY,
                   @I_vPHONE1 = PHONE1,
                   @I_vPHONE2 = PHONE2,
                   @I_vPHONE3 = PHONE3,
                   @I_vFAX = FAXNUMBR
            from   IV40700 (nolock)
            where  LOCNCODE = @I_vADRSCODE
          end
        else
          if (@ADDRSOURCE = 4)
            begin
              select @CMPANYID = CMPANYID,
                     @COPTXSCH = COPTXSCH
              from   DYNAMICS..SY01500 (nolock)
              where  INTERID = db_name()
              
              select @I_vCMPNYNAM = LOCATNNM,
                     @I_vCONTACT = '',
                     @I_vADDRESS1 = ADDRESS1,
                     @I_vADDRESS2 = ADDRESS2,
                     @I_vADDRESS3 = ADDRESS3,
                     @I_vCITY = CITY,
                     @I_vSTATE = STATE,
                     @I_vZIPCODE = ZIPCODE,
                     @I_vCCode = CCode,
                     @I_vCOUNTRY = COUNTRY,
                     @I_vPHONE1 = PHONE1,
                     @I_vPHONE2 = PHONE2,
                     @I_vPHONE3 = PHONE3,
                     @I_vFAX = FAXNUMBR
              from   SY00600 (nolock)
              where  LOCATNID = @I_vADRSCODE
                     and CMPANYID = @CMPANYID
            end
          else
            if (@ADDRSOURCE = 5)
              begin
                select @I_vCMPNYNAM = case 
                                        when (@I_vCMPNYNAM is null) then LOCATNNM
                                        else @I_vCMPNYNAM
                                      end,
                       @I_vCONTACT = case 
                                       when (@I_vCONTACT is null) then ADRCNTCT
                                       else @I_vCONTACT
                                     end,
                       @I_vADDRESS1 = case 
                                        when (@I_vADDRESS1 is null) then ADDRESS1
                                        else @I_vADDRESS1
                                      end,
                       @I_vADDRESS2 = case 
                                        when (@I_vADDRESS2 is null) then ADDRESS2
                                        else @I_vADDRESS2
                                      end,
                       @I_vADDRESS3 = case 
                                        when (@I_vADDRESS3 is null) then ADDRESS3
                                        else @I_vADDRESS3
                                      end,
                       @I_vCITY = case 
                                    when (@I_vCITY is null) then CITY
                                    else @I_vCITY
                                  end,
                       @I_vSTATE = case 
                                     when (@I_vSTATE is null) then STATE
                                     else @I_vSTATE
                                   end,
                       @I_vZIPCODE = case 
                                       when (@I_vZIPCODE is null) then ZIPCODE
                                       else @I_vZIPCODE
                                     end,
                       @I_vCCode = case 
                                     when (@I_vCCode is null) then CCode
                                     else @I_vCCode
                                   end,
                       @I_vCOUNTRY = case 
                                       when (@I_vCOUNTRY is null) then COUNTRY
                                       else @I_vCOUNTRY
                                     end,
                       @I_vPHONE1 = case 
                                      when (@I_vPHONE1 is null) then PHONE1
                                      else @I_vPHONE1
                                    end,
                       @I_vPHONE2 = case 
                                      when (@I_vPHONE2 is null) then PHONE2
                                      else @I_vPHONE2
                                    end,
                       @I_vPHONE3 = case 
                                      when (@I_vPHONE3 is null) then PHONE3
                                      else @I_vPHONE3
                                    end,
                       @I_vFAX = case 
                                   when (@I_vFAX is null) then FAXNUMBR
                                   else @I_vFAX
                                 end
                from   SY00600 (nolock)
                where  LOCATNID = @I_vADRSCODE
                       and CMPANYID = @CMPANYID
                
                select @ADDRSOURCE = 4
              end
            else
              begin
                if (@I_vCMPNYNAM is null)
                  select @I_vCMPNYNAM = ''
                
                if (@I_vCONTACT is null)
                  select @I_vCONTACT = ''
                
                if (@I_vADDRESS1 is null)
                  select @I_vADDRESS1 = ''
                
                if (@I_vADDRESS2 is null)
                  select @I_vADDRESS2 = ''
                
                if (@I_vADDRESS3 is null)
                  select @I_vADDRESS3 = ''
                
                if (@I_vCITY is null)
                  select @I_vCITY = ''
                
                if (@I_vSTATE is null)
                  select @I_vSTATE = ''
                
                if (@I_vZIPCODE is null)
                  select @I_vZIPCODE = ''
                
                if (@I_vCCode is null)
                  select @I_vCCode = ''
                
                if (@I_vCOUNTRY is null)
                  select @I_vCOUNTRY = ''
                
                if (@I_vPHONE1 is null)
                  select @I_vPHONE1 = ''
                
                if (@I_vPHONE2 is null)
                  select @I_vPHONE2 = ''
                
                if (@I_vPHONE3 is null)
                  select @I_vPHONE3 = ''
                
                if (@I_vFAX is null)
                  select @I_vFAX = ''
              end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  while 1 = 1
    begin
      select @existscomment = isnull(1,0)
      from   POP10550 (nolock)
      where  POPNUMBE = @I_vPONUMBER
             and ORD = @I_vORD
      
      if (@I_vCOMMENT_1 <> ''
           or @I_vCOMMENT_2 <> ''
           or @I_vCOMMENT_3 <> ''
           or @I_vCOMMENT_4 <> '')
        begin
          if (@I_vCOMMENT_1 <> @cCOMMENT_1
               or @I_vCOMMENT_2 <> @cCOMMENT_2
               or @I_vCOMMENT_3 <> @cCOMMENT_3
               or @I_vCOMMENT_4 <> @cCOMMENT_4)
            begin
              if (DataLength(rtrim(@I_vCOMMENT_1) + rtrim(@I_vCOMMENT_2) + rtrim(@I_vCOMMENT_3) + rtrim(@I_vCOMMENT_4)) > 0)
                begin
                  select @I_vCMMTTEXT = rtrim(@I_vCOMMENT_1) + char(13) + rtrim(@I_vCOMMENT_2) + char(13) + rtrim(@I_vCOMMENT_3) + char(13) + rtrim(@I_vCOMMENT_4)
                end
              else
                begin
                  select @I_vCMMTTEXT = ''
                  
                  select @I_vCOMMENT_1 = ''
                  
                  select @I_vCOMMENT_2 = ''
                  
                  select @I_vCOMMENT_3 = ''
                  
                  select @I_vCOMMENT_4 = ''
                end
            end
          else
            begin
              select @I_vCMMTTEXT = @cCMMTTEXT
            end
        end
      else
        begin
          if (@I_vCMMTTEXT <> '')
            begin
              if (@I_vCMMTTEXT <> @cCMMTTEXT
                   or (@I_vCOMMENT_1 = ''
                       and @I_vCOMMENT_2 = ''
                       and @I_vCOMMENT_3 = ''
                       and @I_vCOMMENT_4 = ''))
                begin
                  exec @iStatus = RmsParseCommentText
                    @I_vCMMTTEXT = @I_vCMMTTEXT ,
                    @O_COMMENT_1 = @I_vCOMMENT_1 output ,
                    @O_COMMENT_2 = @I_vCOMMENT_2 output ,
                    @O_COMMENT_3 = @I_vCOMMENT_3 output ,
                    @O_COMMENT_4 = @I_vCOMMENT_4 output
                  
                  select @iError = @@error
                  
                  if ((@iStatus <> 0)
                       or (@iError <> 0))
                    begin
                      select @O_iErrorState = 2290
                      
                      exec @iStatus = RmsUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                    end
                end
            end
        end
      
      if (@I_vCOMMNTID <> ''
          and (@cCOMMNTID <> @I_vCOMMNTID
                or @I_vCOMMENT_1 <> @cCOMMENT_1
                or @I_vCOMMENT_2 <> @cCOMMENT_2
                or @I_vCOMMENT_3 <> @cCOMMENT_3
                or @I_vCOMMENT_4 <> @cCOMMENT_4))
        begin
          if (not exists (select 1
                          from   SY04200 (nolock)
                          where  COMMNTID = @I_vCOMMNTID))
            begin
              if (isnull(@I_vCMMTTEXT,'') = '')
                select @I_vCMMTTEXT = ''
              
              exec @iStatus = RmsCreateCommentMaster
                4 ,
                @I_vCOMMNTID ,
                @I_vCMMTTEXT ,
                @O_iErrorState = @O_iCommentMstErrState output
              
              select @iError = @@error
              
              if ((@iStatus <> 0)
                   or (@O_iCommentMstErrState <> 0)
                   or (@iError <> 0))
                begin
                  select @oErrString = rtrim(@oErrString) + ' ' + @O_iCommentMstErrState
                  
                  select @O_iErrorState = 2289
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
              
              select @I_vCMMTTEXT = Cast(CMMTTEXT as varchar(500))
              from   SY04200 (nolock)
              where  COMMNTID = @I_vCOMMNTID
            end
          else
            begin
              if (@I_vCMMTTEXT = '')
                begin
                  select @I_vCMMTTEXT = Cast(CMMTTEXT as varchar(500))
                  from   SY04200 (nolock)
                  where  COMMNTID = @I_vCOMMNTID
                end
            end
          
          if (@I_vCMMTTEXT <> '')
            begin
              exec @iStatus = RmsParseCommentText
                @I_vCMMTTEXT = @I_vCMMTTEXT ,
                @O_COMMENT_1 = @I_vCOMMENT_1 output ,
                @O_COMMENT_2 = @I_vCOMMENT_2 output ,
                @O_COMMENT_3 = @I_vCOMMENT_3 output ,
                @O_COMMENT_4 = @I_vCOMMENT_4 output
              
              select @iError = @@error
              
              if ((@iStatus <> 0)
                   or (@iError <> 0))
                begin
                  select @O_iErrorState = 391
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
            end
        end
      
      if (((@cCMMTTEXT <> @I_vCMMTTEXT
             or @I_vCOMMNTID <> @cCOMMNTID)
           and @I_vUpdateIfExists = 1
           and @existscomment = 0)
           or (@I_vUpdateIfExists = 0
               and (@I_vCMMTTEXT <> ''
                     or @I_vCOMMNTID <> ''))
           or ((@I_vUpdateIfExists = 1
                 or @LineCount = 1)
               and (@cCMMTTEXT <> @I_vCMMTTEXT
                     or @I_vCOMMNTID <> @cCOMMNTID)))
        begin
          if @I_vUpdateIfExists = 0
              or @existscomment = 0
            begin
              insert POP10550
                    (DOCTYPE,
                     POPNUMBE,
                     ORD,
                     COMMNTID,
                     COMMENT_1,
                     COMMENT_2,
                     COMMENT_3,
                     COMMENT_4,
                     CMMTTEXT)
              select 1,
                     @I_vPONUMBER,
                     @I_vORD,
                     @I_vCOMMNTID,
                     @I_vCOMMENT_1,
                     @I_vCOMMENT_2,
                     @I_vCOMMENT_3,
                     @I_vCOMMENT_4,
                     @I_vCMMTTEXT
              
              if (@@error <> 0)
                begin
                  select @O_iErrorState = 2293
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
            end
          else
            begin
              update POP10550
              set    COMMNTID = @I_vCOMMNTID,
                     COMMENT_1 = isnull(@I_vCOMMENT_1,''),
                     COMMENT_2 = isnull(@I_vCOMMENT_2,''),
                     COMMENT_3 = isnull(@I_vCOMMENT_3,''),
                     COMMENT_4 = isnull(@I_vCOMMENT_4,''),
                     CMMTTEXT = isnull(@I_vCMMTTEXT,'')
              where  POPNUMBE = @I_vPONUMBER
                     and ORD = @I_vORD
              
              if (@@error <> 0)
                begin
                  select @O_iErrorState = 8207
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
            end
        end
      
      select @POLNEARY_1 = NOTEINDX
      from   IV00101 (nolock)
      where  ITEMNMBR = @I_vITEMNMBR
      
      select @POLNEARY_2 = NOTEINDX
      from   IV40700 (nolock)
      where  LOCNCODE = @I_vLOCNCODE
      
      select @POLNEARY_3 = NOTEINDX
      from   SY04200 (nolock)
      where  COMMNTID = @I_vCOMMNTID
      
      select @POLNEARY_4 = NOTEINDX
      from   GL00100 (nolock)
      where  ACTINDX = @INVINDX
      
      select @POLNEARY_6 = NOTEINDX
      from   TX00101 (nolock)
      where  TAXSCHID = @I_vPurchase_Item_Tax_Schedu
      
      select @POLNEARY_7 = NOTEINDX
      from   TX00101 (nolock)
      where  TAXSCHID = @I_vPurchase_Site_Tax_Schedu
      
      select @POLNEARY_8 = NOTEINDX
      from   IV41101 (nolock)
      where  Landed_Cost_Group_ID = @I_vLanded_Cost_Group_ID
      
      if (@ISMCTRX = 0)
        begin
          if (exists (select distinct 1
                      from   IV00105 a (nolock)
                      where  ITEMNMBR = @I_vITEMNMBR))
            begin
              if (not exists (select 1
                              from   IV00105 a (nolock)
                              where  ITEMNMBR = @I_vITEMNMBR
                                     and a.CURNCYID <> ''))
                begin
                  select @MCINSTALLED = 0
                end
            end
          else
            begin
              if (not exists (select distinct 1
                              from   IV00105 a (nolock)
                              where  CURNCYID <> ''))
                begin
                  select @MCINSTALLED = 0
                end
            end
        end
      
      if @I_vUpdateIfExists = 0
          or @LineCount = 0
          or (@I_vUpdateIfExists = 1
              and @I_vORD is not null
              and not exists (select 1
                              from   POP10110 (nolock)
                              where  PONUMBER = @I_vPONUMBER
                                     and ITEMNMBR = @I_vITEMNMBR
                                     and ORD = @I_vORD))
        begin
          insert POP10110
                (PONUMBER,
                 ORD,
                 POLNESTA,
                 POTYPE,
                 ITEMNMBR,
                 ITEMDESC,
                 VENDORID,
                 VNDITNUM,
                 VNDITDSC,
                 NONINVEN,
                 LOCNCODE,
                 UOFM,
                 UMQTYINB,
                 QTYORDER,
                 QTYCANCE,
                 QTYCMTBASE,
                 QTYUNCMTBASE,
                 UNITCOST,
                 EXTDCOST,
                 INVINDX,
                 REQDATE,
                 PRMDATE,
                 PRMSHPDTE,
                 REQSTDBY,
                 COMMNTID,
                 DOCTYPE,
                 POLNEARY_1,
                 POLNEARY_2,
                 POLNEARY_3,
                 POLNEARY_4,
                 POLNEARY_5,
                 POLNEARY_6,
                 POLNEARY_7,
                 POLNEARY_8,
                 POLNEARY_9,
                 DECPLCUR,
                 DECPLQTY,
                 ITMTRKOP,
                 VCTNMTHD,
                 BRKFLD1,
                 PO_Line_Status_Orig,
                 QTY_Canceled_Orig,
                 OPOSTSUB,
                 JOBNUMBR,
                 COSTCODE,
                 COSTTYPE,
                 CURNCYID,
                 CURRNIDX,
                 XCHGRATE,
                 RATECALC,
                 DENXRATE,
                 ORUNTCST,
                 OREXTCST,
                 LINEORIGIN,
                 FREEONBOARD,
                 ODECPLCU,
                 Capital_Item,
                 Product_Indicator,
                 Source_Document_Number,
                 Source_Document_Line_Num,
                 RELEASEBYDATE,
                 Released_Date,
                 Change_Order_Flag,
                 Purchase_IV_Item_Taxable,
                 Purchase_Item_Tax_Schedu,
                 Purchase_Site_Tax_Schedu,
                 PURCHSITETXSCHSRC,
                 BSIVCTTL,
                 TAXAMNT,
                 ORTAXAMT,
                 BCKTXAMT,
                 OBTAXAMT,
                 Landed_Cost_Group_ID,
                 PLNNDSPPLID,
                 SHIPMTHD,
                 BackoutTradeDiscTax,
                 OrigBackoutTradeDiscTax,
                 LineNumber,
                 ORIGPRMDATE,
                 FSTRCPTDT,
                 LSTRCPTDT,
                 RELEASE,
                 ADRSCODE,
                 CMPNYNAM,
                 CONTACT,
                 ADDRESS1,
                 ADDRESS2,
                 ADDRESS3,
                 CITY,
                 STATE,
                 ZIPCODE,
                 CCode,
                 COUNTRY,
                 PHONE1,
                 PHONE2,
                 PHONE3,
                 FAX,
                 ADDRSOURCE,
                 Flags,
                 ProjNum,
                 CostCatID)
          select @I_vPONUMBER,
                 @I_vORD,
                 @I_vPOLNESTA,
                 @I_vPOTYPE,
                 @I_vITEMNMBR,
                 @ITEMDESC,
                 @I_vVENDORID,
                 @I_vVNDITNUM,
                 @VNDITDSC,
                 @I_vNONINVEN,
                 @I_vLOCNCODE,
                 @UOFM,
                 @UMQTYINB,
                 @I_vQUANTITY,
                 @I_vQTYCANCE,
                 0,
                 (@I_vQUANTITY - @I_vQTYCANCE) * @UMQTYINB,
                 @UNITCOST,
                 round(@UNITCOST * (@I_vQUANTITY - @I_vQTYCANCE),
                       @EDITDECPLCUR),
                 @INVINDX,
                 @I_vREQDATE,
                 @I_vPRMDATE,
                 @I_vPRMSHPDTE,
                 @I_vREQSTDBY,
                 @I_vCOMMNTID,
                 1,
                 @POLNEARY_1,
                 @POLNEARY_2,
                 @POLNEARY_3,
                 @POLNEARY_4,
                 @POLNEARY_5,
                 @POLNEARY_6,
                 @POLNEARY_7,
                 @POLNEARY_8,
                 @POLNEARY_9,
                 @ITEMFUNCTDEC + 7,
                 @DECPLQTY,
                 @ITMTRKOP,
                 @VCTNMTHD,
                 0,
                 0,
                 0,
                 0,
                 '',
                 '',
                 0,
                 case 
                   when @MCINSTALLED = 1 then @I_vCURNCYID
                   else ''
                 end,
                 case 
                   when @MCINSTALLED = 1 then @CURRNIDX
                   else 0
                 end,
                 0,
                 0,
                 0,
                 @UNITCOST,
                 round(@UNITCOST * (@I_vQUANTITY - @I_vQTYCANCE),
                       @EDITDECPLCUR),
                 @LINEORIGIN,
                 @FREEONBOARD,
                 @ODECPLCU,
                 0,
                 0,
                 '',
                 0,
                 @I_vRELEASEBYDATE,
                 case 
                   when @I_vPOLNESTA = 2 then @I_vRELEASEBYDATE
                   else ''
                 end,
                 case 
                   when @I_vPOLNESTA = 2 then 0
                   else 1
                 end,
                 @I_vPurchase_IV_Item_Taxable,
                 @I_vPurchase_Item_Tax_Schedu,
                 @I_vPurchase_Site_Tax_Schedu,
                 0,
                 @I_vBSIVCTTL,
                 @I_vTAXAMNT,
                 @I_vTAXAMNT,
                 @I_vBCKTXAMT,
                 @I_vBCKTXAMT,
                 @I_vLanded_Cost_Group_ID,
                 @I_vPLNNDSPPLID,
                 @I_vSHIPMTHD,
                 @I_vBackoutTradeDiscTax,
                 @I_vBackoutTradeDiscTax,
                 @I_vLineNumber,
                 @I_vPRMDATE,
                 '',
                 '',
                 1,
                 @I_vADRSCODE,
                 @I_vCMPNYNAM,
                 @I_vCONTACT,
                 @I_vADDRESS1,
                 @I_vADDRESS2,
                 @I_vADDRESS3,
                 @I_vCITY,
                 @I_vSTATE,
                 @I_vZIPCODE,
                 @I_vCCode,
                 @I_vCOUNTRY,
                 @I_vPHONE1,
                 @I_vPHONE2,
                 @I_vPHONE3,
                 @I_vFAX,
                 @ADDRSOURCE,
                 case 
                   when (@I_vCOMMNTID = ''
                         and (@I_vCOMMENT_1 <> ''
                               or @I_vCOMMENT_2 <> ''
                               or @I_vCOMMENT_3 <> ''
                               or @I_vCOMMENT_4 <> '')) then 2
                   else 0
                 end,
                 @I_vProjNum,
                 @I_vCostCatID
          
          if @@error <> 0
            begin
              select @O_iErrorState = 649
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      else
        begin
          update POP10110
          set    POTYPE = @I_vPOTYPE,
                 QTY_Canceled_Orig = @cQTYCANCE,
                 PO_Line_Status_Orig = case 
                                         when @cPOLNESTA <> @I_vPOLNESTA
                                              and @cPOLNESTA <> 1 then @cPOLNESTA
                                         else PO_Line_Status_Orig
                                       end,
                 POLNESTA = @I_vPOLNESTA,
                 VENDORID = @I_vVENDORID,
                 VNDITNUM = @I_vVNDITNUM,
                 VNDITDSC = @VNDITDSC,
                 LOCNCODE = @I_vLOCNCODE,
                 UOFM = @UOFM,
                 UMQTYINB = @UMQTYINB,
                 QTYORDER = @I_vQUANTITY,
                 QTYCANCE = @I_vQTYCANCE,
                 QTYUNCMTBASE = (@I_vQUANTITY - @I_vQTYCANCE) * @UMQTYINB,
                 UNITCOST = @UNITCOST,
                 EXTDCOST = round(@UNITCOST * (@I_vQUANTITY - @I_vQTYCANCE),
                                  @EDITDECPLCUR),
                 INVINDX = @INVINDX,
                 REQDATE = @I_vREQDATE,
                 PRMDATE = @I_vPRMDATE,
                 PRMSHPDTE = @I_vPRMSHPDTE,
                 REQSTDBY = @I_vREQSTDBY,
                 COMMNTID = @I_vCOMMNTID,
                 DECPLCUR = @ITEMFUNCTDEC + 7,
                 DECPLQTY = @DECPLQTY,
                 ITMTRKOP = @ITMTRKOP,
                 VCTNMTHD = @VCTNMTHD,
                 CURNCYID = case 
                              when @MCINSTALLED = 1 then @I_vCURNCYID
                              else ''
                            end,
                 CURRNIDX = case 
                              when @MCINSTALLED = 1 then @CURRNIDX
                              else 0
                            end,
                 ORUNTCST = @UNITCOST,
                 OREXTCST = round(@UNITCOST * (@I_vQUANTITY - @I_vQTYCANCE),
                                  @EDITDECPLCUR),
                 LINEORIGIN = @LINEORIGIN,
                 FREEONBOARD = @FREEONBOARD,
                 ODECPLCU = @ODECPLCU,
                 RELEASEBYDATE = @I_vRELEASEBYDATE,
                 Released_Date = case 
                                   when @I_vPOLNESTA = 2 then @I_vRELEASEBYDATE
                                   else ''
                                 end,
                 Change_Order_Flag = case 
                                       when @I_vPOLNESTA = 2 then 0
                                       else 1
                                     end,
                 Purchase_IV_Item_Taxable = @I_vPurchase_IV_Item_Taxable,
                 Purchase_Item_Tax_Schedu = @I_vPurchase_Item_Tax_Schedu,
                 Purchase_Site_Tax_Schedu = @I_vPurchase_Site_Tax_Schedu,
                 BSIVCTTL = @I_vBSIVCTTL,
                 TAXAMNT = @I_vTAXAMNT,
                 ORTAXAMT = @I_vTAXAMNT,
                 BCKTXAMT = @I_vBCKTXAMT,
                 OBTAXAMT = @I_vBCKTXAMT,
                 Landed_Cost_Group_ID = @I_vLanded_Cost_Group_ID,
                 PLNNDSPPLID = @I_vPLNNDSPPLID,
                 SHIPMTHD = @I_vSHIPMTHD,
                 BackoutTradeDiscTax = @I_vBackoutTradeDiscTax,
                 OrigBackoutTradeDiscTax = @I_vBackoutTradeDiscTax,
                 ORIGPRMDATE = @I_vPRMDATE,
                 ADRSCODE = @I_vADRSCODE,
                 CMPNYNAM = @I_vCMPNYNAM,
                 CONTACT = @I_vCONTACT,
                 ADDRESS1 = @I_vADDRESS1,
                 ADDRESS2 = @I_vADDRESS2,
                 ADDRESS3 = @I_vADDRESS3,
                 CITY = @I_vCITY,
                 STATE = @I_vSTATE,
                 ZIPCODE = @I_vZIPCODE,
                 CCode = @I_vCCode,
                 COUNTRY = @I_vCOUNTRY,
                 PHONE1 = @I_vPHONE1,
                 PHONE2 = @I_vPHONE2,
                 PHONE3 = @I_vPHONE3,
                 FAX = @I_vFAX,
                 ADDRSOURCE = @ADDRSOURCE,
                 Flags = case 
                           when (@I_vCOMMNTID = ''
                                 and (@I_vCOMMENT_1 <> ''
                                       or @I_vCOMMENT_2 <> ''
                                       or @I_vCOMMENT_3 <> ''
                                       or @I_vCOMMENT_4 <> '')) then 2
                           else 0
                         end,
                 ProjNum = @I_vProjNum,
                 CostCatID = @I_vCostCatID
          where  PONUMBER = @I_vPONUMBER
                 and ORD = @I_vORD
                 and ITEMNMBR = @I_vITEMNMBR
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 9224
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      
      if (@I_vPOLNESTA in (2,3,6))
        begin
          update IV00102
          set    QTYONORD = case 
                              when @cPOLNESTA in (2,3,6) then QTYONORD + (((@I_vQUANTITY - @I_vQTYCANCE) * @UMQTYINB) - ((@cQUANTITY - @cQTYCANCE) * @cUMQTYINB))
                              else QTYONORD + ((@I_vQUANTITY - @I_vQTYCANCE) * @UMQTYINB)
                            end,
                 LSORDQTY = (@I_vQUANTITY * @UMQTYINB),
                 LSTORDDT = @I_vRELEASEBYDATE,
                 LSORDVND = @I_vVENDORID
          where  ITEMNMBR = @I_vITEMNMBR
                 AND LOCNCODE = @I_vLOCNCODE
                 AND RCRDTYPE = 2
          
          if @@error <> 0
            begin
              select @O_iErrorState = 1334
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
          
          update IV00102
          set    QTYONORD = case 
                              when @cPOLNESTA in (2,3,6) then QTYONORD + (((@I_vQUANTITY - @I_vQTYCANCE) * @UMQTYINB) - ((@cQUANTITY - @cQTYCANCE) * @cUMQTYINB))
                              else QTYONORD + ((@I_vQUANTITY - @I_vQTYCANCE) * @UMQTYINB)
                            end,
                 LSORDQTY = (@I_vQUANTITY * @UMQTYINB),
                 LSTORDDT = @I_vRELEASEBYDATE,
                 LSORDVND = @I_vVENDORID
          where  ITEMNMBR = @I_vITEMNMBR
                 AND LOCNCODE = ''
                 AND RCRDTYPE = 1
          
          if @@error <> 0
            begin
              select @O_iErrorState = 1335
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
          
          update IV00103
          set    QTYONORD = case 
                              when @cPOLNESTA in (2,3,6) then QTYONORD + (((@I_vQUANTITY - @I_vQTYCANCE) * @UMQTYINB) - ((@cQUANTITY - @cQTYCANCE) * @cUMQTYINB))
                              else QTYONORD + ((@I_vQUANTITY - @I_vQTYCANCE) * @UMQTYINB)
                            end,
                 LSORDQTY = (@I_vQUANTITY * @UMQTYINB),
                 LSTORDDT = @I_vRELEASEBYDATE
          where  VENDORID = @I_vVENDORID
                 and ITEMNMBR = @I_vITEMNMBR
          
          if @@error <> 0
            begin
              select @O_iErrorState = 1272
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      
      if (@I_vNOTETEXT <> '')
        begin
          if not exists (select 1
                         from   SY03900 (nolock)
                         where  NOTEINDX = @POLNEARY_5)
            begin
              insert SY03900
                    (NOTEINDX,
                     DATE1,
                     TIME1,
                     TXTFIELD)
              select @POLNEARY_5,
                     convert(varchar(12),getdate()),
                     substring(convert(varchar(25),getdate()),12,12),
                     @I_vNOTETEXT
              
              if (@@error <> 0)
                begin
                  select @O_iErrorState = 8279
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
            end
          else
            begin
              update SY03900
              set    DATE1 = convert(varchar(12),getdate()),
                     TIME1 = substring(convert(varchar(25),getdate()),12,12),
                     TXTFIELD = @I_vNOTETEXT
              where  NOTEINDX = @POLNEARY_5
              
              if (@@error <> 0)
                begin
                  select @O_iErrorState = 8280
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
            end
        end
      else
        begin
          delete SY03900
          where  NOTEINDX = @POLNEARY_5
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 8281
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      break
    end
  
  return (@O_iErrorState)

