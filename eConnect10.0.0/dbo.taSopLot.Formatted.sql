

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taSopLot]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taSopLot]
  
create procedure [dbo].[taSopLot]
                @I_vSOPTYPE    smallint,
                @I_vSOPNUMBE   char(17),
                @I_vLNITMSEQ   int  = 0,
                @I_vCMPNTSEQ   int  = 0,
                @I_vITEMNMBR   char(30),
                @I_vLOCNCODE   char(10),
                @I_vQTYTYPE    smallint  = 1,
                @I_vQUANTITY   numeric(19,5),
                @I_vDOCID      char(15)  = '',
                @I_vDROPSHIP   smallint  = 0,
                @I_vQTYFULFI   numeric(19,5)  = null,
                @I_vALLOCATE   smallint  = 0,
                @O_iErrorState int  output,
                @oErrString    varchar(255)  output /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @LotFound         smallint,
           @QTYRECVD         numeric(19,5),
           @DTSEQNUM         numeric(19,5),
           @SERLTNUM         char(20),
           @DATERECD         datetime,
           @UNITCOST         numeric(19,5),
           @SERLTQTY         numeric(19,5),
           @SLTSQNUM         int,
           @QUANTITY         numeric(19,5),
           @QtyAvail         numeric(19,5),
           @ATYALLOC         numeric(19,5),
           @QTYSOLD          numeric(19,5),
           @QtyDone          numeric(19,5),
           @VCTNMTHD         tinyint,
           @ENABLEMULTIBIN   tinyint,
           @BIN              char(15),
           @iStatus          int,
           @iError           int,
           @iCustomState     int,
           @iCustomErrString varchar(255),
           @O_oErrorState    int,
           @ALLOCABY         smallint,
           @USPFULPR         smallint
  
  select @QTYRECVD = 0,
         @LotFound = 0,
         @DTSEQNUM = 0,
         @SERLTNUM = '',
         @DATERECD = '',
         @UNITCOST = 0,
         @SERLTQTY = 0,
         @QUANTITY = @I_vQUANTITY,
         @QtyAvail = 0,
         @ATYALLOC = 0,
         @QTYSOLD = 0,
         @QtyDone = 0,
         @VCTNMTHD = 0,
         @ENABLEMULTIBIN = 0,
         @BIN = '',
         @iStatus = 0,
         @iError = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @O_iErrorState = 0,
         @ALLOCABY = 0,
         @USPFULPR = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  if (@I_vSOPTYPE is NULL 
       or @I_vSOPNUMBE is NULL
       or @I_vLNITMSEQ is NULL
       or @I_vITEMNMBR is NULL
       or @I_vQUANTITY is NULL
       or @I_vQTYTYPE is NULL
       or @I_vDOCID is NULL
       or @I_vDROPSHIP is NULL
       or @I_vALLOCATE is NULL)
    begin
      select @O_iErrorState = 567
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vSOPTYPE = 0)
       or (@I_vSOPNUMBE = ''))
    begin
      select @O_iErrorState = 779
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vSOPNUMBE = upper(@I_vSOPNUMBE),
         @I_vITEMNMBR = upper(@I_vITEMNMBR),
         @I_vLOCNCODE = upper(@I_vLOCNCODE),
         @I_vDOCID = upper(@I_vDOCID)
  
  if ((@I_vSOPTYPE < 1)
       or (@I_vSOPTYPE > 6))
    begin
      select @O_iErrorState = 821
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @ENABLEMULTIBIN = ENABLEMULTIBIN
  from   IV40100 (nolock)
  where  SETUPKEY = 1
  
  if ((@I_vDOCID <> '')
      and (@I_vSOPTYPE in (1,2,3,5)))
    begin
      select @ALLOCABY = ALLOCABY,
             @USPFULPR = USPFULPR
      from   SOP40200 (nolock)
      where  DOCID = @I_vDOCID
             and SOPTYPE = @I_vSOPTYPE
    end
  else
    begin
      select @ALLOCABY = 1,
             @USPFULPR = 0
    end
  
  if (@I_vDOCID <> '')
     and (@I_vSOPTYPE = 6)
    begin
      select @ALLOCABY = ALLOCABY,
             @USPFULPR = USPFULPR
      from   SOP40200 (nolock)
      where  DOCID = @I_vDOCID
             and SOPTYPE = 3  --this correct they store them as 3's in this table not sure why but they do
    end
  else
    begin
      select @ALLOCABY = 1,
             @USPFULPR = 0
    end
  
  if (@I_vQTYFULFI is null)
    begin
      select @I_vQTYFULFI = 1
    end
  
  while (@LotFound = 0)
    begin
      select @DTSEQNUM = 0
      
      -- Line Sequence Logic
      select @SLTSQNUM = isnull(max(SLTSQNUM),0) + 16384
      from   SOP10201 (nolock)
      where  SOPTYPE = @I_vSOPTYPE
             and SOPNUMBE = @I_vSOPNUMBE
             and LNITMSEQ = @I_vLNITMSEQ
      
      if (@I_vSOPTYPE <> 4)
        begin
          -- Date SEQ Number - Set to 1 greater than the sequence number assigned to the last lot number for the item at the site and date received in the IV_Lot_MSTR file
          
          -- This select will get the top record where the date received is First In and the Date SEQ Number is the lowest - FIFO
          
          -- The column LTNUMSLD is set to 1 when @QTYRECVD = @ATYALLOC + @QtyAvail + @QTYSOLD
          
          -- Get the valuation method for the item 1,3,4=fifo else lifo
          select @VCTNMTHD = isnull(VCTNMTHD,0)
          from   IV00101 (nolock)
          where  ITEMNMBR = @I_vITEMNMBR
          
          --FIFO valuation method
          if ((@VCTNMTHD = 1)
               or (@VCTNMTHD = 3)
               or (@VCTNMTHD = 4))
            begin
              select   TOP 1 @DTSEQNUM = isnull(DTSEQNUM,0),
                             @DATERECD = isnull(DATERECD,''),
                             @I_vQTYTYPE = isnull(QTYTYPE,0)
              from     IV00300 (nolock)
              where    ITEMNMBR = @I_vITEMNMBR
                       and LOCNCODE = @I_vLOCNCODE
                       and QTYTYPE = @I_vQTYTYPE
                       and LTNUMSLD = 0
              order by DATERECD,
                       DTSEQNUM
            end
          else
            begin
              select   TOP 1 @DTSEQNUM = isnull(DTSEQNUM,0),
                             @DATERECD = isnull(DATERECD,''),
                             @I_vQTYTYPE = isnull(QTYTYPE,0)
              from     IV00300 (nolock)
              where    ITEMNMBR = @I_vITEMNMBR
                       and LOCNCODE = @I_vLOCNCODE
                       and QTYTYPE = @I_vQTYTYPE
                       and LTNUMSLD = 0
              order by DATERECD desc,
                       DTSEQNUM desc
            end
          
          if (@@rowcount = 0)
            begin
              select @O_iErrorState = 2621
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
          
          if (@DTSEQNUM > 0)
            begin
              select @QTYRECVD = QTYRECVD,
                     @ATYALLOC = ATYALLOC,
                     @QTYSOLD = QTYSOLD,
                     @SERLTNUM = LOTNUMBR,
                     @UNITCOST = UNITCOST,
                     @BIN = BIN
              from   IV00300 (nolock)
              where  ITEMNMBR = @I_vITEMNMBR
                     and LOCNCODE = @I_vLOCNCODE
                     and DATERECD = @DATERECD
                     and DTSEQNUM = @DTSEQNUM
                     and LTNUMSLD = 0
                     and QTYTYPE = @I_vQTYTYPE
              
              if (@QUANTITY > (@QTYRECVD - @ATYALLOC - @QTYSOLD) + @QtyDone)
                begin
                  select @QtyAvail = @QTYRECVD - @ATYALLOC - @QTYSOLD
                  
                  -- need counter variable to allocate the correct quantity for the second record in the SOP10201, the first record is always set to SERLTQTY = 1
                  select @SERLTQTY = @QtyAvail
                end
              else
                begin
                  select @LotFound = 1
                  
                  select @SERLTQTY = @QUANTITY - @QtyDone
                  
                  -- Need @QtyAvail variable for the update of the IV00300
                  select @QtyAvail = @QUANTITY - @QtyDone
                end
            end
          else
            begin
              select @O_iErrorState = 260
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      else
        if (@I_vSOPTYPE = 4)
          begin
            select @LotFound = 1
            
            select @BIN = SORETURNBIN
            from   IV00102 (nolock)
            where  ITEMNMBR = @I_vITEMNMBR
                   and LOCNCODE = @I_vLOCNCODE
            
            if (@BIN = '')
              begin
                select @BIN = SORETURNBIN
                from   IV40700 (nolock)
                where  LOCNCODE = @I_vLOCNCODE
              end
            
            if (@BIN = '')
              begin
                select @O_iErrorState = 7044
                
                exec @iStatus = taUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
              end
            
            select @SERLTNUM = LOTNUMBR --baj
            from   IV00300 (nolock)
            where  ITEMNMBR = @I_vITEMNMBR
                   and LOCNCODE = @I_vLOCNCODE
                   and QTYTYPE = @I_vQTYTYPE
            
            select @SERLTQTY = @QUANTITY --baj
          end
      
      if (@ENABLEMULTIBIN <> 1)
        begin
          select @BIN = ''
        end
      
      -- insert to SOP10201 for the remaining Serial/Lot QTY
      if (((@ALLOCABY = 1)
            or (@I_vALLOCATE = 1))
          and ((@USPFULPR = 0)
                or (@I_vQTYFULFI > 0))
          and (@I_vQTYFULFI <> 0)
          and (@I_vDROPSHIP = 0))
        begin
          insert SOP10201
                (SOPTYPE,
                 SOPNUMBE,
                 LNITMSEQ,
                 CMPNTSEQ,
                 QTYTYPE,
                 SERLTNUM,
                 SERLTQTY,
                 SLTSQNUM,
                 DATERECD,
                 DTSEQNUM,
                 UNITCOST,
                 ITEMNMBR,
                 TRXSORCE,
                 POSTED,
                 OVRSERLT,
                 BIN,
                 MFGDATE,
                 EXPNDATE)
          select @I_vSOPTYPE,
                 @I_vSOPNUMBE,
                 @I_vLNITMSEQ,
                 @I_vCMPNTSEQ,
                 @I_vQTYTYPE,
                 @SERLTNUM,
                 @SERLTQTY,
                 @SLTSQNUM,
                 @DATERECD,
                 @DTSEQNUM,
                 @UNITCOST,
                 @I_vITEMNMBR,
                 '',
                 0,
                 0,
                 @BIN,
                 '',
                 ''
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 1000
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
        end
      
      -- update IV00300 set LTNUMSLD = 1 when the @QTYRECVD = @ATYALLOC + @QtyAvail + @QTYSOLD else set that column to zero
      if (((@ALLOCABY = 1)
            or (@I_vALLOCATE = 1))
          and ((@USPFULPR = 0)
                or (@I_vQTYFULFI > 0))
          and (@I_vQTYFULFI <> 0)
          and (@I_vDROPSHIP = 0)
          and (@I_vSOPTYPE <> 4))
        begin
          update IV00300
          set    ATYALLOC = ATYALLOC + @QtyAvail,
                 LTNUMSLD = case 
                              when @QTYRECVD = @ATYALLOC + @QtyAvail + @QTYSOLD then 1
                              else 0
                            end
          from   IV00300 (updlock)
          where  ITEMNMBR = @I_vITEMNMBR
                 and LOCNCODE = @I_vLOCNCODE
                 and DATERECD = @DATERECD
                 and DTSEQNUM = @DTSEQNUM
                 and QTYTYPE = @I_vQTYTYPE
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 166
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              break
            end
          
          if (@ENABLEMULTIBIN = 1)
            begin
              update IV00112
              set    ATYALLOC = ATYALLOC + @SERLTQTY
              from   IV00112 (updlock)
              where  ITEMNMBR = @I_vITEMNMBR
                     and LOCNCODE = @I_vLOCNCODE
                     and BIN = @BIN
                     and QTYTYPE = @I_vQTYTYPE
              
              if (@@error <> 0)
                begin
                  select @O_iErrorState = 7027
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
            end
        end
      
      select @QtyDone = @QtyDone + @QtyAvail
      
      if ((@LotFound = 1)
           or (@QtyDone = @I_vQUANTITY))
        begin
          break
        end
    end
  
  return (@O_iErrorState)

