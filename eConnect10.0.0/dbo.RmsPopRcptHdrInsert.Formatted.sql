

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsPopRcptHdrInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsPopRcptHdrInsert]
  
create procedure [dbo].[RmsPopRcptHdrInsert]
                @I_vPOPRCTNM                 char(17),
                @I_vPOPTYPE                  smallint,
                @I_vVNDDOCNM                 char(20)  = '',
                @I_vreceiptdate              datetime,
                @I_vACTLSHIP                 datetime  = '',
                @I_vBACHNUMB                 char(15),
                @I_vVENDORID                 char(15),
                @I_vVENDNAME                 char(64)  = '',
                @I_vSUBTOTAL                 numeric(19,5)  = null,
                @I_vTRDISAMT                 numeric(19,5)  = null,
                @I_vFRTAMNT                  numeric(19,5)  = 0,
                @I_vMISCAMNT                 numeric(19,5)  = 0,
                @I_vTAXAMNT                  numeric(19,5)  = 0,
                @I_vTEN99AMNT                numeric(19,5)  = 0,
                @I_vPYMTRMID                 char(20)  = null,
                @I_vDSCPCTAM                 numeric(19,2)  = null,
                @I_vDSCDLRAM                 numeric(19,5)  = null,
                @I_vDISAVAMT                 numeric(19,5)  = null,
                @I_vREFRENCE                 char(30)  = '',
                @I_vUSER2ENT                 char(15)  = '',
                @I_vVCHRNMBR                 char(20)  = '',
                @I_vTax_Date                 datetime  = '',
                @I_vTIME1                    datetime  = '',
                @I_vWITHHAMT                 numeric(19,5)  = 0,
                @I_vTXRGNNUM                 char(25)  = '',
                @I_vAUTOCOST                 int  = 0,
                @I_vTAXSCHID                 char(15)  = '',
                @I_vPurchase_Freight_Taxable smallint  = 2,
                @I_vPurchase_Misc_Taxable    smallint  = 2,
                @I_vFRTSCHID                 char(15)  = '',
                @I_vMSCSCHID                 char(15)  = '',
                @I_vFRTTXAMT                 numeric(19,5)  = 0,
                @I_vMSCTXAMT                 numeric(19,5)  = 0,
                @I_vBCKTXAMT                 numeric(19,5)  = 0,
                @I_vBackoutTradeDiscTax      numeric(19,5)  = 0,
                @I_vSHIPMTHD                 char(15)  = null,
                @I_vUSINGHEADERLEVELTAXES    smallint  = 0,
                @I_vCREATEDIST               smallint  = 1,
                @I_vCURNCYID                 char(15)  = '',
                @I_vXCHGRATE                 numeric(19,7)  = 0,
                @I_vRATETPID                 char(15)  = '',
                @I_vEXPNDATE                 datetime  = '',
                @I_vEXCHDATE                 datetime  = '',
                @I_vEXGTBDSC                 char(30)  = '',
                @I_vEXTBLSRC                 char(50)  = '',
                @I_vRATEEXPR                 smallint  = -1,
                @I_vDYSTINCR                 smallint  = -1,
                @I_vRATEVARC                 numeric(19,7)  = 0,
                @I_vTRXDTDEF                 smallint  = -1,
                @I_vRTCLCMTD                 smallint  = -1,
                @I_vPRVDSLMT                 smallint  = 0,
                @I_vDATELMTS                 smallint  = 0,
                @I_vDUEDATE                  datetime  = '',
                @I_vDISCDATE                 datetime  = '',
                @I_vNOTETEXT                 varchar(8000)  = '',
                @I_vVADCDTRO                 char(15)  = null,
                @I_vRequesterTrx             smallint  = 0,
                @I_vUSRDEFND1                char(50)  = '',
                @I_vUSRDEFND2                char(50)  = '',
                @I_vUSRDEFND3                char(50)  = '',
                @I_vUSRDEFND4                varchar(8000)  = '',
                @I_vUSRDEFND5                varchar(8000)  = '',
                @O_iErrorState               int  output,
                @oErrString                  varchar(255)  output     /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @RCPTNOTE_1               numeric(19,5),
           @RCPTNOTE_2               numeric(19,5),
           @RCPTNOTE_3               numeric(19,5),
           @RCPTNOTE_4               numeric(19,5),
           @RCPTNOTE_5               numeric(19,5),
           @RCPTNOTE_6               numeric(19,5),
           @RCPTNOTE_7               numeric(19,5),
           @RCPTNOTE_8               numeric(19,5),
           @dDUEDATE                 datetime,
           @dDISCDATE                datetime,
           @VENDNAME                 char(64),
           @PYMTRMID                 char(20),
           @DSCLCTYP                 smallint,
           @DSCDLRAM                 numeric(19,5),
           @DSCPCTAM                 numeric(19,5),
           @SALPURCH                 smallint,
           @DISCNTCB                 smallint,
           @FREIGHT                  smallint,
           @MISC                     smallint,
           @TAX                      smallint,
           @PymtTermAmnt             numeric(19,5),
           @ACCTAMNT                 numeric(19,5),
           @FUNACCTAMNT              numeric(19,5),
           @EXTDCOST                 numeric(19,5),
           @dtDEFAULT                datetime,
           @CURRNIDX                 int,
           @FCURRNIDX                int,
           @BCHSOURC                 char(15),
           @iCursorError             int,
           @iCustomState             int,
           @iCustomErrString         varchar(255),
           @iStatus                  int,
           @iError                   int,
           @O_oErrorState            int,
           @iUpdtBthErrState         int,
           @iCreateBatchErrString    varchar(255),
           @iUpdDistErrState         int,
           @iCalcDueDateErrState     int,
           @iCalcDueDateErrString    varchar(255),
           @iAddCodeErrState         int,
           @iGetNextNoteIdxErrState  int,
           @sCompanyID               smallint,
           @TAXAMNTL                 numeric(19,5),
           @TAXAMNTH                 numeric(19,5),
           @FRTTXAMT                 numeric(19,5),
           @ORFRTTAX                 numeric(19,5),
           @MSCTXAMT                 numeric(19,5),
           @ORMSCTAX                 numeric(19,5),
           @iAddErrState             int,
           @FUNLCURR                 char(15),
           @EXGTBLID                 char(15),
           @ISMCTRX                  int,
           @ORSUBTOT                 numeric(19,5),
           @ORTDISAM                 numeric(19,5),
           @ORFRTAMT                 numeric(19,5),
           @ORMISCAMT                numeric(19,5),
           @ORTAXAMT                 numeric(19,5),
           @OR1099AM                 numeric(19,5),
           @ORDDLRAT                 numeric(19,5),
           @ORDAVAMT                 numeric(19,5),
           @OBTAXAMT                 numeric(19,5),
           @OrigBackoutFreightTaxAmt numeric(19,5),
           @OrigBackoutMiscTaxAmt    numeric(19,5),
           @OrigBackoutTradeDiscTax  numeric(19,5),
           @DECPLCUR                 int,
           @ORDECPLCUR               tinyint,
           @O_iInitErrorState        int,
           @oInitErrString           varchar(255),
           @linecount                int,
           @BCKTXAMT                 numeric(19,5),
           @BackoutFreightTaxAmt     numeric(19,5),
           @BackoutMiscTaxAmt        numeric(19,5),
           @SumLineORTAXAMT          numeric(19,5),
           @TaxAmtTot                numeric(19,5),
           @OrTaxAmtTot              numeric(19,5),
           @TrDisTax                 numeric(19,5),
           @TrDisTaxDif              numeric(19,5),
           @OrTrDisTax               numeric(19,5),
           @OrTrDisTaxDif            numeric(19,5),
           @BckRunTax                numeric(19,5),
           @BckRunFrt                numeric(19,5),
           @BckRunMsc                numeric(19,5),
           @BckRunLn                 numeric(19,5),
           @POPRCTNM                 char(17),
           @RCPTLNNM                 int,
           @TAXDTLID                 char(15),
           @ACTINDX                  int,
           @LocRcptLine              int,
           @DECPLUSED                int,
           @VENDSTTS                 tinyint,
           @MCINSTALLED              smallint,
           @Total_Landed_Cost_Amount numeric(19,5),
           @CURNCYID                 char(15),
           @INTERID                  char(5),
           @TRDDISCT                 numeric(19,5),
           @TRDPCTPR                 numeric(25,0),
           @TRDPCTPRNUM              numeric(25,24),
           @TRDISAMT                 numeric(19,5),
           @POPALWOP_1               tinyint,
           @PONUMBER                 char(17)
  
  select @RCPTNOTE_1 = 0,
         @RCPTNOTE_2 = 0,
         @RCPTNOTE_3 = 0,
         @RCPTNOTE_4 = 0,
         @RCPTNOTE_5 = 0,
         @RCPTNOTE_6 = 0,
         @RCPTNOTE_7 = 0,
         @RCPTNOTE_8 = 0,
         @dDUEDATE = '',
         @dDISCDATE = '',
         @VENDNAME = '',
         @PYMTRMID = '',
         @DSCLCTYP = 0,
         @DSCDLRAM = 0,
         @DSCPCTAM = 0,
         @SALPURCH = 1,
         @DISCNTCB = 1,
         @FREIGHT = 1,
         @MISC = 1,
         @TAX = 1,
         @PymtTermAmnt = 0,
         @ACCTAMNT = 0,
         @FUNACCTAMNT = 0,
         @EXTDCOST = 0,
         @dtDEFAULT = '',
         @CURRNIDX = 0,
         @FCURRNIDX = 0,
         @BCHSOURC = 'Rcvg Trx Entry',
         @iStatus = 0,
         @iError = 0,
         @O_oErrorState = 0,
         @iUpdtBthErrState = 0,
         @iCreateBatchErrString = '',
         @iUpdDistErrState = 0,
         @iCalcDueDateErrState = 0,
         @iCalcDueDateErrString = '',
         @iGetNextNoteIdxErrState = 0,
         @sCompanyID = '',
         @TAXAMNTL = 0,
         @TAXAMNTH = 0,
         @FRTTXAMT = 0,
         @ORFRTTAX = 0,
         @MSCTXAMT = 0,
         @ORMSCTAX = 0,
         @iAddErrState = 0,
         @FUNLCURR = '',
         @EXGTBLID = '',
         @ISMCTRX = 0,
         @ORSUBTOT = 0,
         @ORTDISAM = 0,
         @ORFRTAMT = 0,
         @ORMISCAMT = 0,
         @ORTAXAMT = 0,
         @OR1099AM = 0,
         @ORDDLRAT = 0,
         @ORDAVAMT = 0,
         @OBTAXAMT = 0,
         @OrigBackoutFreightTaxAmt = 0,
         @OrigBackoutMiscTaxAmt = 0,
         @OrigBackoutTradeDiscTax = 0,
         @DECPLCUR = 0,
         @ORDECPLCUR = 0,
         @linecount = 0,
         @BCKTXAMT = 0,
         @BackoutFreightTaxAmt = 0,
         @BackoutMiscTaxAmt = 0,
         @SumLineORTAXAMT = 0,
         @TaxAmtTot = 0,
         @OrTaxAmtTot = 0,
         @TrDisTax = 0,
         @TrDisTaxDif = 0,
         @OrTrDisTax = 0,
         @OrTrDisTaxDif = 0,
         @BckRunTax = 0,
         @BckRunFrt = 0,
         @BckRunMsc = 0,
         @BckRunLn = 0,
         @POPRCTNM = '',
         @RCPTLNNM = 0,
         @TAXDTLID = '',
         @ACTINDX = 0,
         @LocRcptLine = 0,
         @O_iErrorState = 0,
         @DECPLUSED = 0,
         @VENDSTTS = 0,
         @MCINSTALLED = 1,
         @Total_Landed_Cost_Amount = 0,
         @CURNCYID = '',
         @INTERID = '',
         @TRDDISCT = 0,
         @TRDPCTPR = 100000000000000000000,
         @TRDPCTPRNUM = 0,
         @TRDISAMT = 0,
         @POPALWOP_1 = 0,
         @PONUMBER = ''
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  if (@I_vACTLSHIP is NULL 
       or @I_vBACHNUMB is NULL
       or @I_vFRTAMNT is NULL
       or @I_vMISCAMNT is NULL
       or @I_vPOPRCTNM is NULL
       or @I_vPOPTYPE is NULL
       or @I_vreceiptdate is NULL
       or @I_vREFRENCE is NULL
       or @I_vTax_Date is NULL
       or @I_vTAXAMNT is NULL
       or @I_vTEN99AMNT is NULL
       or @I_vTIME1 is NULL
       or @I_vTXRGNNUM is NULL
       or @I_vUSER2ENT is NULL
       or @I_vVCHRNMBR is NULL
       or @I_vVENDNAME is NULL
       or @I_vVENDORID is NULL
       or @I_vVNDDOCNM is NULL
       or @I_vWITHHAMT is NULL)
    begin
      select @O_iErrorState = 2000
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPOPRCTNM = ''
       or @I_vreceiptdate = ''
       or @I_vVENDORID = ''
       or @I_vBACHNUMB = ''
       or @I_vPOPTYPE = 0)
    begin
      select @O_iErrorState = 2001
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vPOPRCTNM = UPPER(@I_vPOPRCTNM),
         @I_vBACHNUMB = UPPER(@I_vBACHNUMB),
         @I_vVENDORID = UPPER(@I_vVENDORID),
         @I_vVNDDOCNM = UPPER(@I_vVNDDOCNM),
         @I_vSHIPMTHD = upper(@I_vSHIPMTHD),
         @I_vTAXSCHID = upper(@I_vTAXSCHID),
         @I_vFRTSCHID = upper(@I_vFRTSCHID),
         @I_vMSCSCHID = upper(@I_vMSCSCHID),
         @I_vVADCDTRO = upper(@I_vVADCDTRO)
  
  if (@I_vACTLSHIP = '')
    begin
      select @I_vACTLSHIP = @dtDEFAULT
    end
  
  if (@I_vTax_Date = '')
    begin
      select @I_vTax_Date = @I_vreceiptdate
    end
  
  if ((@I_vAUTOCOST = 1)
      and (@I_vSUBTOTAL > 0))
    begin
      select @O_iErrorState = 595
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPOPTYPE <> 1)
      and (@I_vPOPTYPE <> 3))
    begin
      select @O_iErrorState = 2002
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPOPTYPE = 1)
    begin
      if exists (select 1
                 from   POP10360 (nolock)
                 where  POPRCTNM = @I_vPOPRCTNM)
        begin
          select @O_iErrorState = 394
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if (@I_vTAXAMNT <> 0
           or @I_vFRTAMNT <> 0
           or @I_vMISCAMNT <> 0
           or @I_vTRDISAMT <> 0)
        begin
          select @O_iErrorState = 995
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPOPTYPE = 3)
    begin
      if (@I_vVNDDOCNM = '')
        begin
          select @O_iErrorState = 6357
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if exists (select 1
                 from   POP10300 (nolock)
                 where  VENDORID = @I_vVENDORID
                        and VNDDOCNM = @I_vVNDDOCNM)
          or exists (select 1
                     from   POP30300 (nolock)
                     where  VENDORID = @I_vVENDORID
                            and VNDDOCNM = @I_vVNDDOCNM)
        begin
          select @O_iErrorState = 996
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @POPALWOP_1 = POPALWOP_1
  from   POP40100 (nolock)
  
  if (@I_vCREATEDIST not in (0,1))
    begin
      select @O_iErrorState = 6170
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if exists (select 1
             from   POP10300 (nolock)
             where  POPRCTNM = @I_vPOPRCTNM)
      or exists (select 1
                 from   POP30300 (nolock)
                 where  POPRCTNM = @I_vPOPRCTNM)
    begin
      select @O_iErrorState = 2003
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vDSCPCTAM > 100)
    begin
      select @O_iErrorState = 2015
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (not exists (select 1
                  from   PM00200 (nolock)
                  where  VENDORID = @I_vVENDORID))
    begin
      select @O_iErrorState = 2005
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vAUTOCOST = 1)
      or @I_vSUBTOTAL is null
    begin
      select @I_vSUBTOTAL = isnull(sum(EXTDCOST),0.00)
      from   POP10310 (nolock)
      where  POPRCTNM = @I_vPOPRCTNM
      
      select @EXTDCOST = @I_vSUBTOTAL
    end
  
  select @PYMTRMID = PYMTRMID,
         @VENDNAME = VENDNAME,
         @RCPTNOTE_3 = NOTEINDX,
         @VENDSTTS = VENDSTTS,
         @CURNCYID = CURNCYID,
         @TRDDISCT = TRDDISCT,
         @I_vVADCDTRO = case 
                          when @I_vVADCDTRO is null then VADCDTRO
                          else @I_vVADCDTRO
                        end,
         @I_vSHIPMTHD = case 
                          when @I_vSHIPMTHD is null then SHIPMTHD
                          else @I_vSHIPMTHD
                        end
  from   PM00200 (nolock)
  where  VENDORID = @I_vVENDORID
  
  if (@VENDSTTS = 2)
    begin
      select @O_iErrorState = 5418
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vVADCDTRO <> ''
    begin
      if not exists (select 1
                     from   PM00300 (nolock)
                     where  VENDORID = @I_vVENDORID
                            and ADRSCODE = @I_vVADCDTRO)
        begin
          select @O_iErrorState = 462
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vVENDNAME = '')
    begin
      select @I_vVENDNAME = @VENDNAME
    end
  
  if (@I_vVENDNAME = '')
    begin
      select @O_iErrorState = 2009
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @FUNLCURR = FUNLCURR,
         @FCURRNIDX = isnull(FUNCRIDX,0)
  from   MC40000 (nolock)
  
  select @DECPLCUR = DECPLCUR - 1
  from   DYNAMICS..MC40200 (nolock)
  where  CURNCYID = @FUNLCURR
  
  if (@I_vCURNCYID = '')
    begin
      select @I_vCURNCYID = @CURNCYID
      
      if (@I_vCURNCYID = '')
        begin
          select @I_vCURNCYID = @FUNLCURR,
                 @CURRNIDX = @FCURRNIDX
        end
    end
  
  if (@CURRNIDX = 0)
    begin
      select @CURRNIDX = isnull(CURRNIDX,0)
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @I_vCURNCYID
    end
  
  if ((@CURRNIDX = 0)
       or (@I_vCURNCYID = ''))
    begin
      select @O_iErrorState = 980
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@FUNLCURR <> '')
       or (@I_vCURNCYID <> ''))
    begin
      if (@I_vCURNCYID <> '')
        select @DECPLUSED = DECPLCUR - 1
        from   DYNAMICS..MC40200 (nolock)
        where  CURNCYID = @I_vCURNCYID
      else
        select @DECPLUSED = DECPLCUR - 1
        from   DYNAMICS..MC40200 (nolock)
        where  CURNCYID = @FUNLCURR
      
      if ((@I_vSUBTOTAL <> round(@I_vSUBTOTAL,@DECPLUSED))
           or (@I_vTRDISAMT <> round(@I_vTRDISAMT,@DECPLUSED))
           or (@I_vFRTAMNT <> round(@I_vFRTAMNT,@DECPLUSED))
           or (@I_vMISCAMNT <> round(@I_vMISCAMNT,@DECPLUSED))
           or (@I_vTAXAMNT <> round(@I_vTAXAMNT,@DECPLUSED))
           or (@I_vTEN99AMNT <> round(@I_vTEN99AMNT,@DECPLUSED))
           or (@I_vDSCDLRAM <> round(@I_vDSCDLRAM,@DECPLUSED))
           or (@I_vDISAVAMT <> round(@I_vDISAVAMT,@DECPLUSED))
           or (@I_vWITHHAMT <> round(@I_vWITHHAMT,@DECPLUSED))
           or (@I_vFRTTXAMT <> round(@I_vFRTTXAMT,@DECPLUSED))
           or (@I_vMSCTXAMT <> round(@I_vMSCTXAMT,@DECPLUSED))
           or (@I_vBCKTXAMT <> round(@I_vBCKTXAMT,@DECPLUSED))
           or (@I_vBackoutTradeDiscTax <> round(@I_vBackoutTradeDiscTax,@DECPLUSED)))
        begin
          select @O_iErrorState = 7323
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @ORDECPLCUR = DECPLCUR - 1,
         @RCPTNOTE_5 = NOTEINDX
  from   DYNAMICS..MC40200 (nolock)
  where  CURNCYID = @I_vCURNCYID
  
  if (@I_vCURNCYID <> '')
     and (@I_vCURNCYID <> @FUNLCURR)
    begin
      select @ISMCTRX = 1
      
      exec @iStatus = RmsMCCurrencyValidate
        @I_vMASTERID = @I_vVENDORID ,
        @I_vDOCDATE = @I_vreceiptdate ,
        @I_vCURNCYID = @I_vCURNCYID ,
        @I_vEXCHDATE = @I_vEXCHDATE output ,
        @I_vEXGTBDSC = @I_vEXGTBDSC ,
        @I_vEXTBLSRC = @I_vEXTBLSRC ,
        @I_vRATEEXPR = @I_vRATEEXPR output ,
        @I_vDYSTINCR = @I_vDYSTINCR output ,
        @I_vRATEVARC = @I_vRATEVARC ,
        @I_vTRXDTDEF = @I_vTRXDTDEF ,
        @I_vPRVDSLMT = @I_vPRVDSLMT ,
        @I_vDATELMTS = @I_vDATELMTS ,
        @I_vMODULE = 0 ,
        @I_vTIME1 = @I_vTIME1 output ,
        @I_vXCHGRATE = @I_vXCHGRATE output ,
        @I_vEXPNDATE = @I_vEXPNDATE output ,
        @I_vRATETPID = @I_vRATETPID output ,
        @I_vRTCLCMTD = @I_vRTCLCMTD output ,
        @I_vEXGTBLID = @EXGTBLID output ,
        @oErrString = @oErrString output ,
        @O_iErrorState = @O_iErrorState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        select @iStatus = @iError
      
      if (@iStatus <> 0)
          or (@O_iErrorState <> 0)
        begin
          if (@O_iErrorState <> 0)
            begin
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
          
          select @O_iErrorState = 981
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 982
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      select @ISMCTRX = 0,
             @I_vRATEEXPR = 0,
             @I_vDYSTINCR = 0,
             @I_vRTCLCMTD = 0
    end
  
  if (@I_vTIME1 = '')
    begin
      select @I_vTIME1 = ' ' + substring(convert(varchar(25),getdate()),12,12)
    end
  
  select @PONUMBER = PONUMBER
  from   POP10310 (nolock)
  where  POPRCTNM = @I_vPOPRCTNM
  
  if (@I_vPYMTRMID is null)
    begin
      select @I_vPYMTRMID = @PYMTRMID
    end
  
  if ((@I_vTRDISAMT is null)
      and (@I_vPOPTYPE = 3))
    begin
      select @TRDISAMT = round(@I_vSUBTOTAL * (@TRDDISCT / 10000),@DECPLUSED)
    end
  else
    begin
      if @I_vTRDISAMT is NULL
        begin
          select @I_vTRDISAMT = 0
        end
    end
  
  if (@I_vTRDISAMT > @I_vSUBTOTAL)
    begin
      select @O_iErrorState = 5300
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vSUBTOTAL <> 0)
    begin
      if (@I_vTRDISAMT is null)
        begin
          select @I_vTRDISAMT = @TRDISAMT
          
          if (@I_vTRDISAMT = 0)
            begin
              select @TRDPCTPR = 0
            end
          else
            begin
              select @TRDPCTPR = 100000000000000000000
            end
        end
      else
        begin
          if ((@I_vTRDISAMT = 0)
              and (@TRDISAMT = 0))
            begin
              select @TRDPCTPR = 0
            end
          else
            begin
              select @TRDPCTPRNUM = round(@I_vTRDISAMT / @I_vSUBTOTAL,15)
              
              if (@TRDPCTPRNUM < 1)
                begin
                  select @TRDPCTPR = cast(substring(cast(@TRDPCTPRNUM as varchar(27)),3,21) as numeric(21,0))
                end
              else
                begin
                  select @TRDPCTPR = cast(substring(cast(@TRDPCTPRNUM as varchar(27)),1,1) + substring(cast(@TRDPCTPRNUM as varchar(27)),3,20) as numeric(23,0))
                end
            end
        end
    end
  
  if (@I_vAUTOCOST = 0)
    begin
      select @EXTDCOST = isnull(sum(EXTDCOST),0.00)
      from   POP10310 (nolock)
      where  POPRCTNM = @I_vPOPRCTNM
      
      if (@EXTDCOST <> @I_vSUBTOTAL)
        begin
          select @O_iErrorState = 2006
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @ACCTAMNT = @EXTDCOST + @I_vTAXAMNT + @I_vFRTAMNT + @I_vMISCAMNT - @I_vTRDISAMT
  
  select @FUNACCTAMNT = @ACCTAMNT
  
  if (@I_vDISAVAMT > (@EXTDCOST + @I_vTAXAMNT + @I_vFRTAMNT + @I_vMISCAMNT - @I_vTRDISAMT))
    begin
      select @O_iErrorState = 2016
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@oErrString <> '')
    return (@O_iErrorState)
  
  select @SALPURCH = SALPURCH,
         @DISCNTCB = DISCNTCB,
         @FREIGHT = FREIGHT,
         @MISC = MISC,
         @TAX = TAX,
         @RCPTNOTE_4 = NOTEINDX
  from   SY03300 (nolock)
  where  PYMTRMID = @I_vPYMTRMID
  
  select @PymtTermAmnt = @PymtTermAmnt + (@I_vSUBTOTAL * @SALPURCH),
         @PymtTermAmnt = @PymtTermAmnt - (@I_vTRDISAMT * @DISCNTCB),
         @PymtTermAmnt = @PymtTermAmnt + (@I_vFRTAMNT * @FREIGHT),
         @PymtTermAmnt = @PymtTermAmnt + (@I_vMISCAMNT * @MISC),
         @PymtTermAmnt = @PymtTermAmnt + ((@I_vTAXAMNT + @I_vFRTTXAMT + @I_vMSCTXAMT) * (@TAX))
  
  if ((@I_vPYMTRMID = '')
      and (@I_vDISAVAMT is null)
      and (@I_vDSCDLRAM is null)
      and (@I_vDSCPCTAM is null))
    begin
      select @I_vDISAVAMT = 0,
             @I_vDSCDLRAM = 0,
             @I_vDSCPCTAM = 0
    end
  
  if ((@I_vDISAVAMT is not null)
      and (@I_vDSCDLRAM is null 
            or @I_vDSCDLRAM <> @I_vDISAVAMT))
    begin
      select @I_vDSCDLRAM = 0
    end
  
  if ((@I_vDISAVAMT is not null)
      and (@I_vDSCPCTAM is null 
            or round((@I_vDSCPCTAM / 100) * @PymtTermAmnt,@ORDECPLCUR) <> @I_vDISAVAMT))
    begin
      select @I_vDSCPCTAM = 0
    end
  
  if ((@I_vDSCDLRAM is not null 
       and @I_vDSCDLRAM <> 0)
      and (@I_vDSCPCTAM is not null 
           and @I_vDSCPCTAM <> 0))
    begin
      select @O_iErrorState = 5380
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vDSCDLRAM is not null)
      and (@I_vDISAVAMT is null))
    begin
      select @I_vDISAVAMT = @I_vDSCDLRAM
      
      select @ORDAVAMT = @I_vDISAVAMT,
             @I_vDSCPCTAM = 0
    end
  
  if ((@I_vDSCPCTAM is not null)
      and (@I_vDISAVAMT is null))
    begin
      select @I_vDISAVAMT = ((@I_vDSCPCTAM / 100) * @PymtTermAmnt)
      
      select @ORDAVAMT = @I_vDISAVAMT,
             @I_vDSCDLRAM = 0
    end
  
  select @DSCLCTYP = DSCLCTYP,
         @DSCDLRAM = DSCDLRAM,
         @DSCPCTAM = DSCPCTAM
  from   SY03300 (nolock)
  where  PYMTRMID = @I_vPYMTRMID
  
  if ((@I_vPYMTRMID <> '')
      and (@I_vDISAVAMT is null)
      and (@I_vDSCDLRAM is null)
      and (@I_vDSCPCTAM is null))
    begin
      if (@DSCLCTYP = 1)
        begin
          select @I_vDSCPCTAM = @DSCPCTAM / 100.0,
                 @I_vDSCDLRAM = 0
          
          if (@ISMCTRX = 1)
            begin
              select @ORDAVAMT = round((@I_vDSCPCTAM / 100.0) * @PymtTermAmnt,@DECPLUSED)
              
              select @ORDDLRAT = 0
              
              select @I_vDISAVAMT = case 
                                      when (@I_vRTCLCMTD = 0) then round(@ORDAVAMT * @I_vXCHGRATE,@DECPLCUR)
                                      when (@I_vRTCLCMTD = 1) then round(@ORDAVAMT / @I_vXCHGRATE,@DECPLCUR)
                                      else 0
                                    end
            end
          else
            begin
              select @I_vDISAVAMT = round((@I_vDSCPCTAM / 100.0) * @PymtTermAmnt,@DECPLCUR)
              
              select @ORDAVAMT = @I_vDISAVAMT,
                     @ORDDLRAT = 0
            end
        end
      else
        if (@DSCLCTYP = 2)
          begin
            select @I_vDSCPCTAM = 0
            
            if (@ISMCTRX = 1)
              begin
                select @I_vDISAVAMT = @DSCDLRAM,
                       @I_vDSCDLRAM = @DSCDLRAM
                
                select @ORDAVAMT = case 
                                     when (@I_vRTCLCMTD = 0) then round(@I_vDISAVAMT / @I_vXCHGRATE,@DECPLUSED)
                                     when (@I_vRTCLCMTD = 1) then round(@I_vDISAVAMT * @I_vXCHGRATE,@DECPLUSED)
                                     else 0
                                   end
                
                select @ORDDLRAT = @ORDAVAMT
              end
            else
              begin
                select @I_vDSCDLRAM = @DSCDLRAM,
                       @I_vDISAVAMT = @DSCDLRAM
                
                select @ORDDLRAT = @I_vDSCDLRAM,
                       @ORDAVAMT = @I_vDISAVAMT
              end
          end
    end
  else
    if (@ISMCTRX = 1)
      begin
        select @ORDAVAMT = @I_vDISAVAMT
        
        select @I_vDISAVAMT = case 
                                when (@I_vRTCLCMTD = 0) then round(@ORDAVAMT * @I_vXCHGRATE,@DECPLCUR)
                                when (@I_vRTCLCMTD = 1) then round(@ORDAVAMT / @I_vXCHGRATE,@DECPLCUR)
                                else 0
                              end
        
        select @ORDAVAMT = round(@ORDAVAMT,@DECPLUSED)
        
        if (@I_vDSCPCTAM = 0)
          begin
            select @ORDDLRAT = @ORDAVAMT,
                   @I_vDSCDLRAM = @I_vDISAVAMT
          end
      end
    else
      begin
        select @I_vDISAVAMT = round(@I_vDISAVAMT,@DECPLCUR),
               @ORDAVAMT = @I_vDISAVAMT
        
        if (@I_vDSCPCTAM = 0)
          begin
            select @I_vDSCDLRAM = @I_vDISAVAMT,
                   @ORDDLRAT = @I_vDISAVAMT
          end
      end
  
  if (@I_vPYMTRMID <> '')
    begin
      if (not exists (select 1
                      from   SY03300 (nolock)
                      where  PYMTRMID = @I_vPYMTRMID))
        begin
          select @O_iErrorState = 7036
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if ((@I_vPYMTRMID <> '')
      and (@I_vDUEDATE = ''
            or @I_vDISCDATE = ''))
    begin
      exec @iStatus = RmsCalcDueDatePM
        @I_vVENDORID = @I_vVENDORID ,
        @I_vPYMTRMID = @I_vPYMTRMID ,
        @I_vDOCDATE = @I_vreceiptdate ,
        @O_dDISCDATE = @dDISCDATE output ,
        @O_dDUEDATE = @dDUEDATE output ,
        @O_iErrorState = @iCalcDueDateErrState output ,
        @oErrString = @iCalcDueDateErrString output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCalcDueDateErrState <> 0
               or @dDISCDATE = NULL
               or @dDUEDATE = NULL)
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCalcDueDateErrString))
          
          select @O_iErrorState = 2010
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @iAddCodeErrState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vDUEDATE = '')
        select @I_vDUEDATE = @dDUEDATE
      
      if (@I_vDISCDATE = '')
        select @I_vDISCDATE = @dDISCDATE
    end
  
  if (@I_vSHIPMTHD <> '')
    begin
      if (not exists (select 1
                      from   SY03000 (nolock)
                      where  SHIPMTHD = @I_vSHIPMTHD))
        begin
          exec @iStatus = RmsCreateShippingMethod
            @I_vSHIPMTHD = @I_vSHIPMTHD ,
            @I_vUpdateIfExists = 0 ,
            @O_iErrorState = @iAddErrState output ,
            @oErrString = @oErrString output
          
          select @iError = @@error
          
          if @iStatus = 0
             and @iError <> 0
            begin
              select @iStatus = @iError
            end
          
          if (@iStatus <> 0)
              or (@iAddErrState <> 0)
            begin
              select @O_iErrorState = 838
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if (@I_vPurchase_Misc_Taxable < 1
       or @I_vPurchase_Misc_Taxable > 3)
    begin
      select @O_iErrorState = 839
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  else
    begin
      if (@I_vPurchase_Misc_Taxable <> 1)
        begin
          select @I_vMSCSCHID = ''
        end
    end
  
  if (@I_vPurchase_Freight_Taxable < 1
       or @I_vPurchase_Freight_Taxable > 3)
    begin
      select @O_iErrorState = 840
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  else
    begin
      if (@I_vPurchase_Freight_Taxable <> 1)
        begin
          select @I_vFRTSCHID = ''
        end
    end
  
  if (@I_vTAXSCHID <> '')
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vTAXSCHID)
        begin
          select @O_iErrorState = 841
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      if (@I_vTAXAMNT > 0.00)
        begin
          select @O_iErrorState = 842
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @TAXAMNTL = ISNULL(sum(ORTAXAMT),0.00) + ISNULL(sum(ORFRTTAX),0.00) + ISNULL(sum(ORMSCTAX),0.00)
  from   POP10360 (nolock)
  where  POPRCTNM = @I_vPOPRCTNM
         and RCPTLNNM = 0
         and BKOUTTAX = 0
  
  if (@TAXAMNTL <> @I_vTAXAMNT + @I_vFRTTXAMT + @I_vMSCTXAMT)
    begin
      select @O_iErrorState = 843
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vUSINGHEADERLEVELTAXES = 0)
    begin
      select @SumLineORTAXAMT = isnull(sum(ORTAXAMT),0.00)
      from   POP10310 (nolock)
      where  POPRCTNM = @I_vPOPRCTNM
      
      if (@SumLineORTAXAMT <> @I_vTAXAMNT)
        begin
          select @O_iErrorState = 7194
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @TAXAMNTH = ISNULL(sum(TXDTOTTX),0.00)
      from   POP10360 (nolock)
      where  POPRCTNM = @I_vPOPRCTNM
             and RCPTLNNM <> 0
             and BKOUTTAX = 0
      
      if (@TAXAMNTH <> @I_vTAXAMNT + @I_vFRTTXAMT + @I_vMSCTXAMT)
        begin
          select @O_iErrorState = 844
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vFRTSCHID <> '')
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vFRTSCHID)
        begin
          select @O_iErrorState = 845
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      if (@I_vFRTTXAMT > 0.00)
        begin
          select @O_iErrorState = 846
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @FRTTXAMT = ISNULL(sum(FRTTXAMT),0),
         @ORFRTTAX = ISNULL(sum(ORFRTTAX),0)
  from   POP10360 (nolock)
  where  POPRCTNM = @I_vPOPRCTNM
         and RCPTLNNM = 2147483646
         and BKOUTTAX = 0
  
  if ((@I_vFRTTXAMT <> @FRTTXAMT)
       or (@I_vFRTTXAMT <> @ORFRTTAX))
    begin
      select @O_iErrorState = 847
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vMSCSCHID <> '')
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vMSCSCHID)
        begin
          select @O_iErrorState = 848
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      if (@I_vMSCTXAMT > 0.00)
        begin
          select @O_iErrorState = 849
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  select @MSCTXAMT = ISNULL(sum(MSCTXAMT),0),
         @ORMSCTAX = ISNULL(sum(ORMSCTAX),0)
  from   POP10360 (nolock)
  where  POPRCTNM = @I_vPOPRCTNM
         and RCPTLNNM = 2147483645
         and BKOUTTAX = 0
  
  if ((@I_vMSCTXAMT <> @MSCTXAMT)
       or (@I_vMSCTXAMT <> @ORMSCTAX))
    begin
      select @O_iErrorState = 836
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @ORSUBTOT = @I_vSUBTOTAL,
         @ORFRTTAX = @I_vFRTTXAMT,
         @ORMSCTAX = @I_vMSCTXAMT,
         @ORTDISAM = @I_vTRDISAMT,
         @ORFRTAMT = @I_vFRTAMNT,
         @ORMISCAMT = @I_vMISCAMNT,
         @ORTAXAMT = @I_vTAXAMNT,
         @OR1099AM = @I_vTEN99AMNT,
         @OBTAXAMT = @I_vBCKTXAMT,
         @OrigBackoutTradeDiscTax = @I_vBackoutTradeDiscTax
  
  if (@I_vBackoutTradeDiscTax > 0)
    begin
      update POP10310
      set    BackoutTradeDiscTax = round((EXTDCOST / @I_vSUBTOTAL) * @I_vBackoutTradeDiscTax,
                                         @DECPLCUR),
             OrigBackoutTradeDiscTax = round((OREXTCST / @ORSUBTOT) * @OrigBackoutTradeDiscTax,
                                             @ORDECPLCUR)
      where  POPRCTNM = @I_vPOPRCTNM
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 2628
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      select @TrDisTax = isnull(sum(BackoutTradeDiscTax),0)
      from   POP10310 (nolock)
      where  POPRCTNM = @I_vPOPRCTNM
      
      select @TrDisTaxDif = @I_vBackoutTradeDiscTax - @TrDisTax
      
      select @OrTrDisTax = isnull(sum(OrigBackoutTradeDiscTax),0)
      from   POP10310 (nolock)
      where  POPRCTNM = @I_vPOPRCTNM
      
      select @OrTrDisTaxDif = @OrigBackoutTradeDiscTax - @OrTrDisTax
      
      if ((@TrDisTaxDif <> 0)
           or (@OrTrDisTaxDif <> 0))
        begin
          update b
          set    BackoutTradeDiscTax = BackoutTradeDiscTax + @TrDisTaxDif,
                 OrigBackoutTradeDiscTax = OrigBackoutTradeDiscTax + @OrTrDisTaxDif
          from   POP10310 b (nolock),
                 (select max(DEX_ROW_ID) as Dex
                  from   POP10310 (nolock)
                  where  POPRCTNM = @I_vPOPRCTNM) as t1
          where  b.POPRCTNM = @I_vPOPRCTNM
                 and b.DEX_ROW_ID = t1.Dex
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 6263
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  select @linecount = count(POPRCTNM) + 2
  from   POP10310 (nolock)
  where  POPRCTNM = @I_vPOPRCTNM
  
  select @INTERID = INTERID,
         @sCompanyID = CMPANYID
  from   DYNAMICS..SY01500 (nolock)
  where  INTERID = db_name()
  
  exec @iStatus = DYNAMICS..RmssmGetNextNoteIndex
    @I_sCompanyID = @sCompanyID ,
    @I_iSQLSessionID = 0 ,
    @I_noteincrement = @linecount ,
    @O_mNoteIndex = @RCPTNOTE_1 output ,
    @O_iErrorState = @iGetNextNoteIdxErrState output
  
  select @iError = @@error
  
  if ((@iStatus = 0)
      and (@iError <> 0))
    begin
      select @iStatus = @iError
    end
  
  if ((@iStatus <> 0)
       or (@iGetNextNoteIdxErrState <> 0))
    begin
      if (@iGetNextNoteIdxErrState <> 0)
        begin
          exec @iStatus = RmsUpdateString
            @iGetNextNoteIdxErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 247
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @RCPTNOTE_2 = NOTEINDX
  from   SY00500 (nolock)
  where  BACHNUMB = @I_vBACHNUMB
         and BCHSOURC = @BCHSOURC
  
  select @RCPTNOTE_6 = NOTEINDX
  from   TX00101 (nolock)
  where  TAXSCHID = @I_vTAXSCHID
  
  select @RCPTNOTE_7 = NOTEINDX
  from   TX00101 (nolock)
  where  TAXSCHID = @I_vFRTSCHID
  
  select @RCPTNOTE_8 = NOTEINDX
  from   TX00101 (nolock)
  where  TAXSCHID = @I_vMSCSCHID
  
  if (exists (select 1
              from   POP10360 (nolock)
              where  POPRCTNM = @I_vPOPRCTNM
                     and BKOUTTAX = 1)
      and (@I_vBackoutTradeDiscTax = 0))
    begin
      select @BckRunTax = @I_vSUBTOTAL + @I_vFRTAMNT + @I_vMISCAMNT,
             @BckRunFrt = @I_vFRTAMNT,
             @BckRunMsc = @I_vMISCAMNT
      
      declare PopTax INSENSITIVE cursor  for
      select   POPRCTNM,
               RCPTLNNM,
               TAXDTLID,
               ACTINDX
      from     POP10360 (nolock)
      where    POPRCTNM = @I_vPOPRCTNM
      order by RCPTLNNM asc,
               BKOUTTAX desc,
               TAXDTLID asc
      
      open PopTax
      
      select @iCursorError = @@cursor_rows
      
      if (@iCursorError > 0)
        begin
          fetch next from PopTax
          into @POPRCTNM,
               @RCPTLNNM,
               @TAXDTLID,
               @ACTINDX
          
          while (@@fetch_status <> -1)
            begin
              if (@@fetch_status = -2)
                begin
                  select @O_iErrorState = 6233
                  
                  break
                end
              
              if ((@RCPTLNNM <> @LocRcptLine)
                  and (@RCPTLNNM not in (0,2147483645,2147483646)))
                begin
                  select @BckRunLn = OREXTCST
                  from   POP10310 (nolock)
                  where  POPRCTNM = @POPRCTNM
                         and RCPTLNNM = @RCPTLNNM
                         and RCPTLNNM not in (0,2147483645,2147483646)
                end
              
              update POP10360
              set    ORGTXPCH = case 
                                  when BKOUTTAX = 1
                                       and @BckRunTax = @I_vSUBTOTAL + @I_vFRTAMNT + @I_vMISCAMNT
                                       and RCPTLNNM = 0 then @BckRunTax - ORTAXAMT - ORFRTTAX - ORMSCTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 0 then @BckRunTax
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483645
                                       and @BckRunMsc = @I_vMISCAMNT then @BckRunMsc - ORMSCTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483645
                                       and @BckRunMsc <> @I_vMISCAMNT then @BckRunMsc
                                  when BKOUTTAX = 0
                                       and RCPTLNNM = 2147483645 then @BckRunMsc
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483646
                                       and @BckRunFrt = @I_vFRTAMNT then @BckRunFrt - ORFRTTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483646
                                       and @BckRunFrt <> @I_vFRTAMNT then @BckRunFrt
                                  when BKOUTTAX = 0
                                       and RCPTLNNM = 2147483646 then @BckRunFrt
                                  when BKOUTTAX = 1
                                       and RCPTLNNM not in (0,2147483645,2147483646) then @BckRunLn
                                  when BKOUTTAX = 0
                                       and RCPTLNNM not in (0,2147483645,2147483646) then @BckRunLn
                                  else @BckRunTax
                                end,
                     TAXPURCH = case 
                                  when BKOUTTAX = 1
                                       and @BckRunTax = @I_vSUBTOTAL + @I_vFRTAMNT + @I_vMISCAMNT
                                       and RCPTLNNM = 0 then @BckRunTax - ORTAXAMT - ORFRTTAX - ORMSCTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 0 then @BckRunTax
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483645
                                       and @BckRunMsc = @I_vMISCAMNT then @BckRunMsc - ORMSCTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483645
                                       and @BckRunMsc <> @I_vMISCAMNT then @BckRunMsc
                                  when BKOUTTAX = 0
                                       and RCPTLNNM = 2147483645 then @BckRunMsc
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483646
                                       and @BckRunFrt = @I_vFRTAMNT then @BckRunFrt - ORFRTTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483646
                                       and @BckRunFrt <> @I_vFRTAMNT then @BckRunFrt
                                  when BKOUTTAX = 0
                                       and RCPTLNNM = 2147483646 then @BckRunFrt
                                  when BKOUTTAX = 1
                                       and RCPTLNNM not in (0,2147483645,2147483646) then @BckRunLn
                                  when BKOUTTAX = 0
                                       and RCPTLNNM not in (0,2147483645,2147483646) then @BckRunLn
                                  else @BckRunTax
                                end,
                     TOTPURCH = case 
                                  when BKOUTTAX = 1
                                       and @BckRunTax = @I_vSUBTOTAL + @I_vFRTAMNT + @I_vMISCAMNT
                                       and RCPTLNNM = 0 then @BckRunTax - ORTAXAMT - ORFRTTAX - ORMSCTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 0 then @BckRunTax
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483645
                                       and @BckRunMsc = @I_vMISCAMNT then @BckRunMsc - ORMSCTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483645
                                       and @BckRunMsc <> @I_vMISCAMNT then @BckRunMsc
                                  when BKOUTTAX = 0
                                       and RCPTLNNM = 2147483645 then @BckRunMsc
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483646
                                       and @BckRunFrt = @I_vFRTAMNT then @BckRunFrt - ORFRTTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483646
                                       and @BckRunFrt <> @I_vFRTAMNT then @BckRunFrt
                                  when BKOUTTAX = 0
                                       and RCPTLNNM = 2147483646 then @BckRunFrt
                                  when BKOUTTAX = 1
                                       and RCPTLNNM not in (0,2147483645,2147483646) then @BckRunLn
                                  when BKOUTTAX = 0
                                       and RCPTLNNM not in (0,2147483645,2147483646) then @BckRunLn
                                  else @BckRunTax
                                end,
                     ORTOTPUR = case 
                                  when BKOUTTAX = 1
                                       and @BckRunTax = @I_vSUBTOTAL + @I_vFRTAMNT + @I_vMISCAMNT
                                       and RCPTLNNM = 0 then @BckRunTax - ORTAXAMT - ORFRTTAX - ORMSCTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 0 then @BckRunTax
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483645
                                       and @BckRunMsc = @I_vMISCAMNT then @BckRunMsc - ORMSCTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483645
                                       and @BckRunMsc <> @I_vMISCAMNT then @BckRunMsc
                                  when BKOUTTAX = 0
                                       and RCPTLNNM = 2147483645 then @BckRunMsc
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483646
                                       and @BckRunFrt = @I_vFRTAMNT then @BckRunFrt - ORFRTTAX
                                  when BKOUTTAX = 1
                                       and RCPTLNNM = 2147483646
                                       and @BckRunFrt <> @I_vFRTAMNT then @BckRunFrt
                                  when BKOUTTAX = 0
                                       and RCPTLNNM = 2147483646 then @BckRunFrt
                                  when BKOUTTAX = 1
                                       and RCPTLNNM not in (0,2147483645,2147483646) then @BckRunLn
                                  when BKOUTTAX = 0
                                       and RCPTLNNM not in (0,2147483645,2147483646) then @BckRunLn
                                  else @BckRunTax
                                end,
                     @BckRunTax = case 
                                    when BKOUTTAX = 1
                                         and RCPTLNNM = 0 then @BckRunTax - ORTAXAMT - ORFRTTAX - ORMSCTAX
                                    else @BckRunTax
                                  end,
                     @BckRunFrt = case 
                                    when BKOUTTAX = 1
                                         and RCPTLNNM = 2147483646 then @BckRunFrt - ORFRTTAX
                                    else @BckRunFrt
                                  end,
                     @BckRunMsc = case 
                                    when BKOUTTAX = 1
                                         and RCPTLNNM = 2147483645 then @BckRunMsc - ORMSCTAX
                                    else @BckRunMsc
                                  end,
                     @BckRunLn = case 
                                   when BKOUTTAX = 1
                                        and RCPTLNNM not in (0,2147483646,2147483645) then @BckRunLn - ORTAXAMT
                                   else @BckRunLn
                                 end
              from   POP10360 (nolock)
              where  POPRCTNM = @POPRCTNM
                     and RCPTLNNM = @RCPTLNNM
                     and TAXDTLID = @TAXDTLID
                     and ACTINDX = @ACTINDX
              
              if (@@error <> 0)
                begin
                  select @O_iErrorState = 2614
                  
                  exec @iStatus = RmsUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  deallocate PopTax
                  
                  return (@O_iErrorState)
                end
              
              select @LocRcptLine = @RCPTLNNM
              
              fetch next from PopTax
              into @POPRCTNM,
                   @RCPTLNNM,
                   @TAXDTLID,
                   @ACTINDX
            end
        end
      
      deallocate PopTax
    end
  
  if (exists (select 1
              from   POP10310 (nolock)
              where  POPRCTNM = @I_vPOPRCTNM
                     and CURNCYID <> @I_vCURNCYID
                     and CURNCYID <> ''
                     and @POPALWOP_1 = 0))
    begin
      select @O_iErrorState = 970
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@ISMCTRX = 1)
    begin
      if (@I_vXCHGRATE = 0)
        begin
          select @O_iErrorState = 983
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vRTCLCMTD = 0)
        begin
          select @I_vSUBTOTAL = round(@I_vSUBTOTAL * @I_vXCHGRATE,@DECPLCUR),
                 @I_vFRTTXAMT = round(@I_vFRTTXAMT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vMSCTXAMT = round(@I_vMSCTXAMT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vTRDISAMT = round(@I_vTRDISAMT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vFRTAMNT = round(@I_vFRTAMNT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vMISCAMNT = round(@I_vMISCAMNT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vTAXAMNT = round(@I_vTAXAMNT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vTEN99AMNT = round(@I_vTEN99AMNT * @I_vXCHGRATE,@DECPLCUR),
                 @I_vBackoutTradeDiscTax = round(@I_vBackoutTradeDiscTax * @I_vXCHGRATE,@DECPLCUR),
                 @FUNACCTAMNT = round(@ACCTAMNT * @I_vXCHGRATE,@DECPLCUR)
        end
      else
        if (@I_vRTCLCMTD = 1)
          begin
            select @I_vSUBTOTAL = round(@I_vSUBTOTAL / @I_vXCHGRATE,@DECPLCUR),
                   @I_vFRTTXAMT = round(@I_vFRTTXAMT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vMSCTXAMT = round(@I_vMSCTXAMT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vTRDISAMT = round(@I_vTRDISAMT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vFRTAMNT = round(@I_vFRTAMNT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vMISCAMNT = round(@I_vMISCAMNT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vTAXAMNT = round(@I_vTAXAMNT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vTEN99AMNT = round(@I_vTEN99AMNT / @I_vXCHGRATE,@DECPLCUR),
                   @I_vBackoutTradeDiscTax = round(@I_vBackoutTradeDiscTax / @I_vXCHGRATE,@DECPLCUR),
                   @FUNACCTAMNT = round(@ACCTAMNT / @I_vXCHGRATE,@DECPLCUR)
          end
      
      if (@I_vRTCLCMTD = 0)
        begin
          update POP10100
          set    REMSUBTO = round(REMSUBTO * @I_vXCHGRATE,@DECPLCUR)
          where  PONUMBER = @PONUMBER
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 9377
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          update POP10310
          set    UNITCOST = round(UNITCOST * @I_vXCHGRATE,(DECPLCUR - 7)),
                 ORUNTCST = UNITCOST,
                 EXTDCOST = round(EXTDCOST * @I_vXCHGRATE,@DECPLCUR),
                 OREXTCST = EXTDCOST,
                 TAXAMNT = round(TAXAMNT * @I_vXCHGRATE,@DECPLCUR),
                 ORTAXAMT = TAXAMNT,
                 BCKTXAMT = round(BCKTXAMT * @I_vXCHGRATE,@DECPLCUR),
                 OBTAXAMT = BCKTXAMT,
                 BackoutTradeDiscTax = round(BackoutTradeDiscTax * @I_vXCHGRATE,@DECPLCUR),
                 OrigBackoutTradeDiscTax = BackoutTradeDiscTax,
                 CURNCYID = @I_vCURNCYID,
                 CURRNIDX = @CURRNIDX,
                 RATETPID = @I_vRATETPID,
                 XCHGRATE = @I_vXCHGRATE,
                 RATECALC = @I_vRTCLCMTD,
                 DENXRATE = 0
          where  POPRCTNM = @I_vPOPRCTNM
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 984
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          update POP10360
          set    TAXAMNT = round(TAXAMNT * @I_vXCHGRATE,@DECPLCUR),
                 ORTAXAMT = TAXAMNT,
                 TAXPURCH = round(TAXPURCH * @I_vXCHGRATE,@DECPLCUR),
                 ORGTXPCH = TAXPURCH,
                 TOTPURCH = round(TOTPURCH * @I_vXCHGRATE,@DECPLCUR),
                 ORTOTPUR = TOTPURCH,
                 FRTTXAMT = round(FRTTXAMT * @I_vXCHGRATE,@DECPLCUR),
                 ORFRTTAX = FRTTXAMT,
                 MSCTXAMT = round(MSCTXAMT * @I_vXCHGRATE,@DECPLCUR),
                 ORMSCTAX = MSCTXAMT,
                 TXDTOTTX = round(TXDTOTTX * @I_vXCHGRATE,@DECPLCUR),
                 OTTAXPON = TXDTOTTX,
                 CURRNIDX = @CURRNIDX
          where  POPRCTNM = @I_vPOPRCTNM
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 985
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          update POP10390
          set    CURRNIDX = @CURRNIDX,
                 XCHGRATE = @I_vXCHGRATE,
                 CURNCYID = @I_vCURNCYID,
                 RATETPID = @I_vRATETPID,
                 EXGTBLID = @EXGTBLID,
                 EXCHDATE = @I_vEXCHDATE,
                 RATECALC = @I_vRTCLCMTD,
                 DENXRATE = 0,
                 MCTRXSTT = 0,
                 DEBITAMT = round(DEBITAMT * @I_vXCHGRATE,@DECPLCUR),
                 ORDBTAMT = DEBITAMT,
                 CRDTAMNT = round(CRDTAMNT * @I_vXCHGRATE,@DECPLCUR),
                 ORCRDAMT = CRDTAMNT
          where  POPRCTNM = @I_vPOPRCTNM
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 7051
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          update POP10500
          set    CURNCYID = @I_vCURNCYID,
                 CURRNIDX = @CURRNIDX,
                 XCHGRATE = @I_vXCHGRATE,
                 RATECALC = @I_vRTCLCMTD,
                 DENXRATE = 0,
                 RATETPID = @I_vRATETPID,
                 EXGTBLID = @EXGTBLID,
                 OSTDCOST = round(OSTDCOST / @I_vXCHGRATE,@DECPLCUR)
          where  POPRCTNM = @I_vPOPRCTNM
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 1999
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        if (@I_vRTCLCMTD = 1)
          begin
            update POP10100
            set    REMSUBTO = round(REMSUBTO / @I_vXCHGRATE,@DECPLCUR)
            where  PONUMBER = @PONUMBER
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 9378
                
                exec @iStatus = RmsUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
                
                return (@O_iErrorState)
              end
            
            update POP10310
            set    UNITCOST = round(UNITCOST / @I_vXCHGRATE,(DECPLCUR - 7)),
                   ORUNTCST = UNITCOST,
                   EXTDCOST = round(EXTDCOST / @I_vXCHGRATE,@DECPLCUR),
                   OREXTCST = EXTDCOST,
                   TAXAMNT = round(TAXAMNT / @I_vXCHGRATE,@DECPLCUR),
                   ORTAXAMT = TAXAMNT,
                   BCKTXAMT = round(BCKTXAMT / @I_vXCHGRATE,@DECPLCUR),
                   OBTAXAMT = BCKTXAMT,
                   BackoutTradeDiscTax = round(BackoutTradeDiscTax / @I_vXCHGRATE,@DECPLCUR),
                   OrigBackoutTradeDiscTax = BackoutTradeDiscTax,
                   CURNCYID = @I_vCURNCYID,
                   CURRNIDX = @CURRNIDX,
                   RATETPID = @I_vRATETPID,
                   XCHGRATE = @I_vXCHGRATE,
                   RATECALC = @I_vRTCLCMTD,
                   DENXRATE = 0
            where  POPRCTNM = @I_vPOPRCTNM
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 986
                
                exec @iStatus = RmsUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
                
                return (@O_iErrorState)
              end
            
            update POP10360
            set    TAXAMNT = round(TAXAMNT / @I_vXCHGRATE,@DECPLCUR),
                   ORTAXAMT = TAXAMNT,
                   TAXPURCH = round(TAXPURCH / @I_vXCHGRATE,@DECPLCUR),
                   ORGTXPCH = TAXPURCH,
                   TOTPURCH = round(TOTPURCH / @I_vXCHGRATE,@DECPLCUR),
                   ORTOTPUR = TOTPURCH,
                   FRTTXAMT = round(FRTTXAMT / @I_vXCHGRATE,@DECPLCUR),
                   ORFRTTAX = FRTTXAMT,
                   MSCTXAMT = round(MSCTXAMT / @I_vXCHGRATE,@DECPLCUR),
                   ORMSCTAX = MSCTXAMT,
                   TXDTOTTX = round(TXDTOTTX / @I_vXCHGRATE,@DECPLCUR),
                   OTTAXPON = TXDTOTTX,
                   CURRNIDX = @CURRNIDX
            where  POPRCTNM = @I_vPOPRCTNM
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 987
                
                exec @iStatus = RmsUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
                
                return (@O_iErrorState)
              end
            
            update POP10390
            set    CURRNIDX = @CURRNIDX,
                   XCHGRATE = @I_vXCHGRATE,
                   CURNCYID = @I_vCURNCYID,
                   RATETPID = @I_vRATETPID,
                   EXGTBLID = @EXGTBLID,
                   EXCHDATE = @I_vEXCHDATE,
                   RATECALC = @I_vRTCLCMTD,
                   DENXRATE = 0,
                   MCTRXSTT = 0,
                   DEBITAMT = round(DEBITAMT / @I_vXCHGRATE,@DECPLCUR),
                   ORDBTAMT = DEBITAMT,
                   CRDTAMNT = round(CRDTAMNT / @I_vXCHGRATE,@DECPLCUR),
                   ORCRDAMT = CRDTAMNT
            where  POPRCTNM = @I_vPOPRCTNM
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 7052
                
                exec @iStatus = RmsUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
                
                return (@O_iErrorState)
              end
            
            update POP10500
            set    CURNCYID = @I_vCURNCYID,
                   CURRNIDX = @CURRNIDX,
                   XCHGRATE = @I_vXCHGRATE,
                   RATECALC = @I_vRTCLCMTD,
                   DENXRATE = 0,
                   RATETPID = @I_vRATETPID,
                   EXGTBLID = @EXGTBLID,
                   OSTDCOST = round(OSTDCOST * @I_vXCHGRATE,@DECPLCUR)
            where  POPRCTNM = @I_vPOPRCTNM
            
            if (@@error <> 0)
              begin
                select @O_iErrorState = 2007
                
                exec @iStatus = RmsUpdateString
                  @O_iErrorState ,
                  @oErrString ,
                  @oErrString output ,
                  @O_oErrorState output
                
                return (@O_iErrorState)
              end
          end
    end
  else
    begin
      if (not exists (select 1
                      from   PM00200 (nolock)
                      where  VENDORID = @I_vVENDORID
                             and (CURNCYID = @I_vCURNCYID
                                   or CURNCYID = '')))
        begin
          update POP10390
          set    CURNCYID = @I_vCURNCYID,
                 CURRNIDX = @CURRNIDX
          where  POPRCTNM = @I_vPOPRCTNM
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 7053
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          update POP10360
          set    CURRNIDX = @CURRNIDX
          where  POPRCTNM = @I_vPOPRCTNM
          
          if (@@error <> 0)
            begin
              select @O_iErrorState = 4023
              
              exec @iStatus = RmsUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  update s
  set    s.RUPPVAMT = (s.ORCPTCOST - s.OSTDCOST) * (((QTYSHPPD - QTYREJ - QTYINVCD) * s.UMQTYINB))
  from   POP10500 s (nolock),
         IV00101 b (nolock)
  where  POPRCTNM = @I_vPOPRCTNM
         and s.ITEMNMBR = b.ITEMNMBR
         and b.VCTNMTHD in (4,5)
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 6661
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vREFRENCE = '')
    begin
      select @I_vREFRENCE = 'Receivings Transaction Entry'
    end
  
  if @ISMCTRX = 1
    begin
      update POP10700
      set    Total_Landed_Cost_Amount = case 
                                          when @I_vRTCLCMTD = 0 then round(Total_Landed_Cost_Amount * XCHGRATE,@DECPLCUR)
                                          else round(Total_Landed_Cost_Amount / XCHGRATE,@DECPLCUR)
                                        end,
             Orig_TotalLandedCostAmt = case 
                                         when @I_vRTCLCMTD = 0 then round(Orig_TotalLandedCostAmt * XCHGRATE,@DECPLCUR)
                                         else round(Orig_TotalLandedCostAmt / XCHGRATE,@DECPLCUR)
                                       end
      where  POPRCTNM = @I_vPOPRCTNM
             and CURNCYID <> @I_vCURNCYID
             and CURNCYID <> @FUNLCURR
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 8054
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      update POP10700
      set    Total_Landed_Cost_Amount = case 
                                          when @I_vRTCLCMTD = 0 then round(Total_Landed_Cost_Amount * XCHGRATE,@DECPLCUR)
                                          else round(Total_Landed_Cost_Amount / XCHGRATE,@DECPLCUR)
                                        end,
             Orig_TotalLandedCostAmt = case 
                                         when @I_vRTCLCMTD = 0 then round(Orig_TotalLandedCostAmt * XCHGRATE,@DECPLCUR)
                                         else round(Orig_TotalLandedCostAmt / XCHGRATE,@DECPLCUR)
                                       end
      where  POPRCTNM = @I_vPOPRCTNM
             and CURNCYID = @I_vCURNCYID
             and CURNCYID <> @FUNLCURR
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 8055
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  update a
  set    a.Total_Landed_Cost_Amount = t1.Total_Landed_Cost_Amount
  from   POP10500 a (nolock),
         (select   sum(b.Total_Landed_Cost_Amount)  Total_Landed_Cost_Amount
          from     POP10500 a (nolock),
                   POP10700 b (nolock)
          where    a.POPRCTNM = @I_vPOPRCTNM
                   and b.POPRCTNM = @I_vPOPRCTNM
                   and a.RCPTLNNM = b.RCPTLNNM
          group by a.POPRCTNM,a.RCPTLNNM) t1
  where  a.POPRCTNM = @I_vPOPRCTNM
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 7906
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @Total_Landed_Cost_Amount = round(isnull(sum(Total_Landed_Cost_Amount),0),@DECPLCUR)
  from   POP10700 (nolock)
  where  POPRCTNM = @I_vPOPRCTNM
  
  select @BCKTXAMT = round(isnull(sum(TXDTOTTX),0),@DECPLCUR),
         @OBTAXAMT = round(isnull(sum(OTTAXPON),0),@ORDECPLCUR)
  from   POP10360 (nolock)
  where  POPRCTNM = @I_vPOPRCTNM
         and RCPTLNNM = 0
         and BKOUTTAX = 1
  
  select @BackoutFreightTaxAmt = round(isnull(sum(TXDTOTTX),0),@DECPLCUR),
         @OrigBackoutFreightTaxAmt = round(isnull(sum(OTTAXPON),0),@ORDECPLCUR)
  from   POP10360 (nolock)
  where  POPRCTNM = @I_vPOPRCTNM
         and RCPTLNNM = 2147483646
         and BKOUTTAX = 1
  
  select @BackoutMiscTaxAmt = round(isnull(sum(TXDTOTTX),0),@DECPLCUR),
         @OrigBackoutMiscTaxAmt = round(isnull(sum(OTTAXPON),0),@ORDECPLCUR)
  from   POP10360 (nolock)
  where  POPRCTNM = @I_vPOPRCTNM
         and RCPTLNNM = 2147483645
         and BKOUTTAX = 1
  
  select @TaxAmtTot = @I_vTAXAMNT + @I_vFRTTXAMT + @I_vMSCTXAMT,
         @OrTaxAmtTot = @ORTAXAMT + @ORFRTTAX + @ORMSCTAX
  
  if (@BCKTXAMT <> 0
       or @OBTAXAMT <> 0)
    begin
      update b
      set    b.BCKTXAMT = t1.TAX + b.BackoutTradeDiscTax,
             b.OBTAXAMT = t1.ORTAX + b.OrigBackoutTradeDiscTax
      from   POP10310 b (nolock),
             (select   isnull(sum(TAXAMNT),0)   as TAX,
                       isnull(sum(ORTAXAMT),0)  as ORTAX,
                       RCPTLNNM,
                       POPRCTNM
              from     POP10360 (nolock)
              where    POPRCTNM = @I_vPOPRCTNM
                       and RCPTLNNM <> 0
                       and BKOUTTAX = 1
              group by POPRCTNM,RCPTLNNM) as t1
      where  b.POPRCTNM = @I_vPOPRCTNM
             and b.RCPTLNNM = t1.RCPTLNNM
             and b.POPRCTNM = t1.POPRCTNM
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 6667
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@ISMCTRX = 0)
    begin
      if (exists (select distinct 1
                  from   IV00105 a (nolock),
                         POP10310 b (nolock)
                  where  b.POPRCTNM = @I_vPOPRCTNM
                         and b.ITEMNMBR = a.ITEMNMBR))
        begin
          if (not exists (select 1
                          from   IV00105 a (nolock),
                                 POP10310 b (nolock)
                          where  b.POPRCTNM = @I_vPOPRCTNM
                                 and b.ITEMNMBR = a.ITEMNMBR
                                 and a.CURNCYID <> ''))
            begin
              select @MCINSTALLED = 0
            end
        end
      else
        begin
          if (not exists (select distinct 1
                          from   IV00105 a (nolock)
                          where  CURNCYID <> ''))
            begin
              select @MCINSTALLED = 0
            end
        end
    end
  
  insert POP10300
        (POPRCTNM,
         POPTYPE,
         VNDDOCNM,
         receiptdate,
         GLPOSTDT,
         ACTLSHIP,
         BCHSOURC,
         BACHNUMB,
         VENDORID,
         VENDNAME,
         SUBTOTAL,
         TRDISAMT,
         TRDPCTPR,
         FRTAMNT,
         MISCAMNT,
         TAXAMNT,
         TEN99AMNT,
         PYMTRMID,
         DSCPCTAM,
         DSCDLRAM,
         DISAVAMT,
         DISCDATE,
         DUEDATE,
         REFRENCE,
         RCPTNOTE_1,
         RCPTNOTE_2,
         RCPTNOTE_3,
         RCPTNOTE_4,
         RCPTNOTE_5,
         RCPTNOTE_6,
         RCPTNOTE_7,
         RCPTNOTE_8,
         POPHDR1,
         POPHDR2,
         POPLNERR,
         POSTEDDT,
         PTDUSRID,
         USER2ENT,
         CREATDDT,
         MODIFDT,
         TRXSORCE,
         VCHRNMBR,
         Tax_Date,
         CURNCYID,
         CURRNIDX,
         RATETPID,
         EXGTBLID,
         XCHGRATE,
         EXCHDATE,
         TIME1,
         RATECALC,
         DENXRATE,
         MCTRXSTT,
         ORSUBTOT,
         ORTDISAM,
         ORFRTAMT,
         ORMISCAMT,
         ORTAXAMT,
         OR1099AM,
         ORDDLRAT,
         ORDAVAMT,
         WITHHAMT,
         SIMPLIFD,
         BNKRCAMT,
         ECTRX,
         TXRGNNUM,
         TAXSCHID,
         TXENGCLD,
         BSIVCTTL,
         Purchase_Freight_Taxable,
         Purchase_Misc_Taxable,
         FRTSCHID,
         MSCSCHID,
         FRTTXAMT,
         ORFRTTAX,
         MSCTXAMT,
         ORMSCTAX,
         BCKTXAMT,
         OBTAXAMT,
         BackoutFreightTaxAmt,
         OrigBackoutFreightTaxAmt,
         BackoutMiscTaxAmt,
         OrigBackoutMiscTaxAmt,
         TaxInvReqd,
         TaxInvRecvd,
         APLYWITH,
         PPSTAXRT,
         SHIPMTHD,
         DocPrinted,
         Total_Landed_Cost_Amount,
         BackoutTradeDiscTax,
         OrigBackoutTradeDiscTax,
         CBVAT,
         VADCDTRO)
  select @I_vPOPRCTNM,
         @I_vPOPTYPE,
         @I_vVNDDOCNM,
         @I_vreceiptdate,
         @I_vreceiptdate,
         @I_vACTLSHIP,
         @BCHSOURC,
         @I_vBACHNUMB,
         @I_vVENDORID,
         @I_vVENDNAME,
         @I_vSUBTOTAL,
         @I_vTRDISAMT,
         @TRDPCTPR,
         @I_vFRTAMNT,
         @I_vMISCAMNT,
         @TaxAmtTot,
         @I_vTEN99AMNT,
         @I_vPYMTRMID,
         @I_vDSCPCTAM * 100,
         @I_vDSCDLRAM,
         case 
           when @I_vPOPTYPE = 1 then 0
           else @I_vDISAVAMT
         end,
         case 
           when @I_vPOPTYPE = 1 then ''
           else @I_vDISCDATE
         end,
         case 
           when @dDUEDATE = @dtDEFAULT then @I_vreceiptdate
           else @I_vDUEDATE
         end,
         @I_vREFRENCE,
         @RCPTNOTE_1,
         @RCPTNOTE_2,
         @RCPTNOTE_3,
         @RCPTNOTE_4,
         @RCPTNOTE_5,
         @RCPTNOTE_6,
         @RCPTNOTE_7,
         @RCPTNOTE_8,
         0,
         0,
         0,
         '',
         '',
         @I_vUSER2ENT,
         convert(varchar(12),getdate()),
         convert(varchar(12),getdate()),
         '',
         '',
         @I_vreceiptdate,
         case 
           when @MCINSTALLED = 1 then @I_vCURNCYID
           else ''
         end,
         case 
           when @MCINSTALLED = 1 then @CURRNIDX
           else 0
         end,
         @I_vRATETPID,
         @EXGTBLID,
         @I_vXCHGRATE,
         @I_vEXCHDATE,
         case 
           when (@ISMCTRX = 1) then @I_vTIME1
           else @dtDEFAULT
         end,
         @I_vRTCLCMTD,
         0,
         0,
         @ORSUBTOT,
         @ORTDISAM,
         @ORFRTAMT,
         @ORMISCAMT,
         @OrTaxAmtTot,
         @OR1099AM,
         @ORDDLRAT,
         case 
           when @I_vPOPTYPE = 1 then 0
           else @ORDAVAMT
         end,
         0,
         0,
         0,
         0,
         @I_vTXRGNNUM,
         @I_vTAXSCHID,
         1,
         0,
         @I_vPurchase_Freight_Taxable,
         @I_vPurchase_Misc_Taxable,
         @I_vFRTSCHID,
         @I_vMSCSCHID,
         @I_vFRTTXAMT,
         @ORFRTTAX,
         @I_vMSCTXAMT,
         @ORMSCTAX,
         @BCKTXAMT,
         @OBTAXAMT,
         @BackoutFreightTaxAmt,
         @OrigBackoutFreightTaxAmt,
         @BackoutMiscTaxAmt,
         @OrigBackoutMiscTaxAmt,
         0,
         0,
         0,
         0,
         @I_vSHIPMTHD,
         0,
         @Total_Landed_Cost_Amount,
         @I_vBackoutTradeDiscTax,
         @OrigBackoutTradeDiscTax,
         0,
         @I_vVADCDTRO
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 2011
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = RmsCreateUpdateBatchHeaderRcd
    @I_vBACHNUMB = @I_vBACHNUMB ,
    @I_vSERIES = 4 ,
    @I_vGLPOSTDT = @I_vreceiptdate ,
    @I_vBCHSOURC = @BCHSOURC ,
    @I_vDOCAMT = @FUNACCTAMNT ,
    @I_vORIGIN = 1 ,
    @I_vNUMOFTRX = 1 ,
    @I_vCHEKBKID = '' ,
    @I_vUSERID = @I_vUSER2ENT ,
    @O_iErrorState = @iUpdtBthErrState output ,
    @oErrString = @iCreateBatchErrString output
  
  select @iError = @@error
  
  if ((@iStatus = 0)
      and (@iError <> 0))
    select @iStatus = @iError
  
  if (@iStatus <> 0)
      or (@iUpdtBthErrState <> 0)
    begin
      if (@iUpdtBthErrState <> 0)
        begin
          exec @iStatus = RmsUpdateString
            @iUpdtBthErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 2008
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPOPTYPE = 1)
    begin
      select @I_vDISAVAMT = 0,
             @ORDAVAMT = 0
    end
  
  exec @iStatus = RmsPopCreateDistributions
    @I_vPOPTYPE ,
    @I_vPOPRCTNM ,
    @I_vVENDORID ,
    @CURRNIDX ,
    @I_vTRDISAMT ,
    @I_vFRTAMNT ,
    @I_vMISCAMNT ,
    @TaxAmtTot ,
    @I_vDISAVAMT ,
    @ORTDISAM ,
    @ORFRTAMT ,
    @ORMISCAMT ,
    @OrTaxAmtTot ,
    @ORDAVAMT ,
    @I_vCURNCYID ,
    @I_vRATETPID ,
    @EXGTBLID ,
    @I_vXCHGRATE ,
    @I_vEXCHDATE ,
    @I_vTIME1 ,
    @I_vRTCLCMTD ,
    @DECPLUSED ,
    @INTERID ,
    @sCompanyID ,
    @I_vCREATEDIST ,
    @iUpdDistErrState output ,
    @oErrString output
  
  select @iError = @@error
  
  if ((@iStatus = 0)
      and (@iError <> 0))
    select @iStatus = @iError
  
  if ((@iStatus <> 0)
       or (@iUpdDistErrState <> 0))
    begin
      if (@iUpdDistErrState <> 0)
        begin
          exec @iStatus = RmsUpdateString
            @iUpdDistErrState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      select @O_iErrorState = 2012
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vNOTETEXT <> '')
    begin
      insert SY03900
            (NOTEINDX,
             DATE1,
             TIME1,
             TXTFIELD)
      select @RCPTNOTE_1,
             convert(varchar(12),getdate()),
             @I_vTIME1,
             @I_vNOTETEXT
      
      if (@@error <> 0)
        begin
          select @O_iErrorState = 6126
          
          exec @iStatus = RmsUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @RCPTNOTE_1 = @RCPTNOTE_1 + 1
  
  update POP10310
  set    RcptLineNoteIDArray_5 = @RCPTNOTE_1,
         @RCPTNOTE_1 = @RCPTNOTE_1 + 1
  where  POPRCTNM = @I_vPOPRCTNM
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 1794
      
      exec @iStatus = RmsUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@O_iErrorState <> 0)
    begin
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

