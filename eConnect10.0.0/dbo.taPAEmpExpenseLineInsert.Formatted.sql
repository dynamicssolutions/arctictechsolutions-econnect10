

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAEmpExpenseLineInsert]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAEmpExpenseLineInsert]
  
create procedure [dbo].[taPAEmpExpenseLineInsert]
                @I_vPAertrxtype            smallint,
                @I_vPAerdocnumber          char(17),
                @I_vEMPLOYID               char(15),
                @I_vPADT                   datetime,
                @I_vPAPROJNUMBER           char(15),
                @I_vPACOSTCATID            char(15),
                @I_vCURNCYID               char(15),
                @I_vLNITMSEQ               int  = 0,
                @I_vPAitemnumber           char(31)  = '',
                @I_vPAREFNO                char(17)  = '',
                @I_vPAQtyQ                 numeric(19,5)  = 0,
                @I_vPAUnit_of_Measure      char(9)  = '',
                @I_vUOMSCHDL               char(11)  = '',
                @I_vPAUNITCOST             numeric(19,5)  = 0,
                @I_vPAPurchase_Tax_Options smallint  = 3,
                @I_vITMTSHID               char(15)  = '',
                @I_vTAXAMNT                numeric(19,5)  = 0,
                @I_vBCKTXAMT               numeric(19,5)  = 0,
                @I_vPAPaymentMethod        smallint  = 3,
                @I_vPAExpenseType          smallint  = 1,
                @I_vPAReference_Line_Seq_N int  = 0,
                @I_vPAbllngtype            smallint  = -1,
                @I_vRequesterTrx           smallint  = 0,
                @I_vUSRDEFND1              char(50)  = '',
                @I_vUSRDEFND2              char(50)  = '',
                @I_vUSRDEFND3              char(50)  = '',
                @I_vUSRDEFND4              varchar(8000)  = '',
                @I_vUSRDEFND5              varchar(8000)  = '',
                @O_iErrorState             int  output,
                @oErrString                varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iStatus int,  @iCustomState int,  @iCustomErrString varchar(255),  @iError int,  @iCursorError int,  @O_oErrorState int,  @sCompanyID smallint,     @PAreportingperiods smallint,  @PAnumofreportingperiods smallint,  @PA1stdatereportperiod datetime,  @PAallow_1 int,  @PAallow_2 int,  @PAallow_3 int,  @PAallow_4 int,  @PAallow_5 int,  @PAallow_6 int,  @PAallow_7 int,  @PAallow_8 int,  @PAallow_9 int,  @PAallow_10 int,  @PACONTNUMBER char (11),  @l_PACONTNUMBER char (11),  @l_PAPROJNUMBER char (15),  @l_PACOSTCATID char (15),  @PAbillnoteidx numeric (19,5),  @PABase_Qty numeric (19,5),  @l_UOMSCHDL char (11),  @ORUNTCST numeric (19,5),  @PABase_Unit_Cost numeric (19,5),  @PAORGBSUNITCST numeric (19,5),  @PAEXTCOST numeric (19,5),  @OREXTCST numeric (19,5),  @PAtotcosts numeric (19,5),  @PAORIGTOTCOSTS numeric (19,5),  @PABILRATE numeric (19,5),  @PAORIGBILLRATE numeric (19,5),  @PA_Base_Billing_Rate numeric (19,5),  @PAORIGBSBILLRTE numeric (19,5),  @PAMARKPERCENT numeric (19,5),  @PAOverhead_Amount numeric (19,5),  @PAORIGOVHDAMT numeric (19,5),  @PABaseOvhdCost numeric (19,5),  @PAORIGBASEOVRHCST numeric (19,5),  @PAOverheaPercentage numeric (19,5),  @PATOTALOVERH numeric (19,5),  @PAORIGTOTOVRHD numeric (19,5),  @PAProfitType smallint,  @PAProfitAmount numeric (19,5),  @PAORIGPROFAMT numeric (19,5),  @PAProfitPercent numeric (19,5),  @PATotalProfit numeric (19,5),  @PAORIGTOTPROF numeric (19,5),  @PAACREV numeric (19,5),  @PAORIACCRREV numeric (19,5),  @PASTD_Qty numeric (19,5),  @PANC_Qty numeric (19,5),  @PANB_Qty numeric (19,5),  @PAPurchase_Tax_Options smallint,  @ITMTSHID char (15),  @TAXAMNT numeric (19,5),  @ORTAXAMT numeric (19,5),  @BCKTXAMT numeric (19,5),  @OBTAXAMT numeric (19,5),  @PAPaymentMethod smallint,  @PAExpenseType smallint,  @PAReimbursableAmount numeric (19,5),  @PAOrigReimbursableAmt numeric (19,5),  @PACGBWIPIDX int,  @PAUnbilled_AR_Idx int,  @PACogs_Idx int,  @PAContra_Account_IDX int,  @PAOverhead_IDX int,  @PAUnbilled_Proj_Rev_Idx int,  @LNITMSEQ int,  @PAReference_Line_Seq_N int,  @PAbllngtype smallint,  @PABilling_StatusN smallint,  @PAexptdate datetime,  @PA_EE_Line_Errors binary (4),  @PA_EE_Line_Errors_2 binary (4),  @PAApproved_Quantity numeric (19,5),  @PAAPPROVBILLRATE numeric (19,5),  @PAAPPROVMKUPPCT numeric (19,5),  @PAApproved_Billing_Amou numeric (19,5),  @PARemaining_Quantity numeric (19,5),  @PARemaining_Total_Cost numeric (19,5),  @PARemaining_Accrued_Rev numeric (19,5),  @PABILLQTY numeric (19,5),  @PA_Billed_Profit_Amount_ numeric (19,5),  @PABilledProfitPercentage numeric (19,5),  @PABilled_Amount numeric (19,5),  @PABilledTax numeric (19,5),  @PAWrite_UpDown_Amount numeric (19,5),  @PAWrite_UpDown_Percenta int,  @PAApprover_ID char (15),  @PAApprover_Type smallint,  @PAPartial_Bill tinyint,  @PAROUNDAMT numeric (19,5),  @RNDDIFF int,  @PACHGORDNO char (17),  @DEX_ROW_ID int,  @PASTAT smallint,  @PAcloseProjcosts smallint,  @PAProjectType smallint,  @PAAcctgMethod smallint,  @CUSTNMBR char(17),  @l_CUSTNMBR char(17),  @add_employee_access smallint,  @PATU smallint,  @DECPLCUR smallint,  @DECPLQTY smallint,  @PAVIDROAer smallint,  @PAinactive smallint,  @PAeeprofittypefrom smallint,  @BASEUOFM char(9),  @EQUOMQTY numeric(19,5),  @FUNCRIDX int,  @CURRNIDX smallint,  @CURNCYID char(15),  @FUNLCURR char(15),  @iGetNextNoteIdxErrState int,   @iAddCodeErrState int,  @PAUnitCost numeric(19,5),  @PAIV_Item_Checkbox smallint,  @PA_Line_Update_Totals_TablesErrorState int,  @l_qty numeric(19,5),  @l_base_qty numeric(19,5),  @l_overhead numeric(19,5),  @l_extended numeric(19,5),  @l_accrued numeric(19,5),  @l_cost numeric(19,5),  @l_ocost numeric(19,5),  @l_scroll_UofM_Qty numeric(19,5),  @l_oqty numeric(19,5),  @O_overhead numeric(19,5),  @O_extended numeric(19,5),  @O_accrued numeric(19,5),  @O_cost numeric(19,5),  @PAPostedQty numeric(19,5),  @PAFQuantity numeric(19,5),  @l_line_UofM_Qty numeric(19,5),  @PAPostedTotalCostN numeric(19,5),  @PAFTotalCost numeric(19,5),  @l_oaccrued numeric(19,5),  @PAFBillings numeric(19,5),  @PAUnpostedQty numeric(19,5),  @PAUnpostedTotalCostN numeric(19,5),  @PAPosted_Accr_RevN numeric(19,5),  @PABilled_Accrued_Revenu numeric(19,5),  @PAUnpostAccrRevN numeric(19,5),  @taPAProjectBudgetMasterErrorState int,  @otaPAProjectBudgetMasterErrString varchar(255),  @temp_PAProfitType smallint,  @temp_PAbllngtype smallint,  @temp_LNITMSEQ int,  @temp_PALineItemSeq int,  @temp_PAFQuantity numeric(19,5),  @temp_PAFUnitCost numeric(19,5),  @temp_PAFTotalCost numeric(19,5),  @temp_PAFProfitAmt numeric(19,5),  @temp_PAFProfitPcnt  numeric(19,5),  @temp_PAForecastOvrhdAmtPerUnt numeric(19,5),  @temp_PAForecastOvrhdPct numeric(19,5),  @temp_PAForecastPTaxOptions smallint,  @temp_PAForecastCTaxSchedID char(15),  @temp_PAForecastSTaxOptions smallint,  @temp_PAForecastBTaxSchedID char(15),  @temp_PAFBeginDate datetime,  @temp_PAFEndDate datetime,  @PAINCPRCHTXPRJCST smallint,  @PAPostedBillingsN numeric(19,5),  @PAUnit_of_Measure char(9),  @PAMCCURNCYID char(15),  @RATETPID char(15),  @EXGTBLID char(15),  @XCHGRATE numeric(19,5),  @EXCHDATE datetime,  @TIME1 datetime,  @RATECALC int,  @DENXRATE numeric(19,5),  @MCTRXSTT int,  @PA_MC_Accrued_Revenue numeric(19,5),  @PA_MC_Base_Billing_Rate numeric(19,5),  @PA_MC_Billing_Rate numeric(19,5),  @IsMC bit,  @MCDECPLCUR smallint,  @PAMCCURRNIDX char(15)
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  select @iStatus = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @iError = 0,
         @iCursorError = 0,
         @O_oErrorState = 0,
         @sCompanyID = 0,
         @PAreportingperiods = 0,
         @PAnumofreportingperiods = 0,
         @PA1stdatereportperiod = '',
         @PAallow_1 = 0,
         @PAallow_2 = 0,
         @PAallow_3 = 0,
         @PAallow_4 = 0,
         @PAallow_5 = 0,
         @PAallow_6 = 0,
         @PAallow_7 = 0,
         @PAallow_8 = 0,
         @PAallow_9 = 0,
         @PAallow_10 = 0,
         @PACONTNUMBER = '',
         @l_PACONTNUMBER = '',
         @l_PAPROJNUMBER = '',
         @l_PACOSTCATID = '',
         @PAbillnoteidx = 0,
         @PABase_Qty = 0,
         @l_UOMSCHDL = '',
         @ORUNTCST = 0,
         @PABase_Unit_Cost = 0,
         @PAORGBSUNITCST = 0,
         @PAEXTCOST = 0,
         @OREXTCST = 0,
         @PAtotcosts = 0,
         @PAORIGTOTCOSTS = 0,
         @PABILRATE = 0,
         @PAORIGBILLRATE = 0,
         @PA_Base_Billing_Rate = 0,
         @PAORIGBSBILLRTE = 0,
         @PAMARKPERCENT = 0,
         @PAOverhead_Amount = 0,
         @PAORIGOVHDAMT = 0,
         @PABaseOvhdCost = 0,
         @PAORIGBASEOVRHCST = 0,
         @PAOverheaPercentage = 0,
         @PATOTALOVERH = 0,
         @PAORIGTOTOVRHD = 0,
         @PAProfitType = 0,
         @PAProfitAmount = 0,
         @PAORIGPROFAMT = 0,
         @PAProfitPercent = 0,
         @PATotalProfit = 0,
         @PAORIGTOTPROF = 0,
         @PAACREV = 0,
         @PAORIACCRREV = 0,
         @PASTD_Qty = 0,
         @PANC_Qty = 0,
         @PANB_Qty = 0,
         @PAPurchase_Tax_Options = 0,
         @ITMTSHID = '',
         @TAXAMNT = 0,
         @ORTAXAMT = 0,
         @BCKTXAMT = 0,
         @OBTAXAMT = 0,
         @PAPaymentMethod = 0,
         @PAExpenseType = 0,
         @PAReimbursableAmount = 0,
         @PAOrigReimbursableAmt = 0,
         @PACGBWIPIDX = 0,
         @PAUnbilled_AR_Idx = 0,
         @PACogs_Idx = 0,
         @PAContra_Account_IDX = 0,
         @PAOverhead_IDX = 0,
         @PAUnbilled_Proj_Rev_Idx = 0,
         @LNITMSEQ = 0,
         @PAReference_Line_Seq_N = 0,
         @PAbllngtype = 0,
         @PABilling_StatusN = 0,
         @PAexptdate = '',
         @PA_EE_Line_Errors = 0,
         @PA_EE_Line_Errors_2 = 0,
         @PAApproved_Quantity = 0,
         @PAAPPROVBILLRATE = 0,
         @PAAPPROVMKUPPCT = 0,
         @PAApproved_Billing_Amou = 0,
         @PARemaining_Quantity = 0,
         @PARemaining_Total_Cost = 0,
         @PARemaining_Accrued_Rev = 0,
         @PABILLQTY = 0,
         @PA_Billed_Profit_Amount_ = 0,
         @PABilledProfitPercentage = 0,
         @PABilled_Amount = 0,
         @PABilledTax = 0,
         @PAWrite_UpDown_Amount = 0,
         @PAWrite_UpDown_Percenta = 0,
         @PAApprover_ID = '',
         @PAApprover_Type = 0,
         @PAPartial_Bill = 0,
         @PAROUNDAMT = 0,
         @RNDDIFF = 0,
         @PACHGORDNO = '',
         @DEX_ROW_ID = 0,
         @PASTAT = 0,
         @PAcloseProjcosts = 0,
         @PAProjectType = 0,
         @PAAcctgMethod = 0,
         @CUSTNMBR = '',
         @l_CUSTNMBR = '',
         @add_employee_access = 0,
         @PATU = 0,
         @DECPLCUR = 0,
         @DECPLQTY = 0,
         @PAVIDROAer = 0,
         @PAinactive = 0,
         @PAeeprofittypefrom = 0,
         @BASEUOFM = '',
         @EQUOMQTY = 0,
         @FUNCRIDX = 0,
         @CURRNIDX = 0,
         @CURNCYID = '',
         @FUNLCURR = '',
         @iGetNextNoteIdxErrState = 0,
         @iAddCodeErrState = 0,
         @O_iErrorState = 0,
         @PAUnitCost = 0,
         @PAIV_Item_Checkbox = 0,
         @PA_Line_Update_Totals_TablesErrorState = 0,
         @l_qty = 0,
         @l_base_qty = 0,
         @O_overhead = 0,
         @O_extended = 0,
         @O_cost = 0,
         @l_UOMSCHDL = '',
         @l_overhead = 0,
         @l_extended = 0,
         @l_accrued = 0,
         @l_cost = 0,
         @l_ocost = 0,
         @l_scroll_UofM_Qty = 0,
         @l_oqty = 0,
         @O_accrued = 0,
         @PAPostedQty = 0,
         @PAFQuantity = 0,
         @l_line_UofM_Qty = 0,
         @PAPostedTotalCostN = 0,
         @PAFTotalCost = 0,
         @l_oaccrued = 0,
         @PAFBillings = 0,
         @PAUnpostedQty = 0,
         @PAUnpostedTotalCostN = 0,
         @PAPosted_Accr_RevN = 0,
         @PABilled_Accrued_Revenu = 0,
         @PAUnpostAccrRevN = 0,
         @taPAProjectBudgetMasterErrorState = 0,
         @otaPAProjectBudgetMasterErrString = '',
         @temp_PAProfitType = 0,
         @temp_PAbllngtype = 0,
         @temp_LNITMSEQ = 0,
         @temp_PALineItemSeq = 0,
         @temp_PAFQuantity = 0,
         @temp_PAFUnitCost = 0,
         @temp_PAFTotalCost = 0,
         @temp_PAFProfitAmt = 0,
         @temp_PAFProfitPcnt = 0,
         @temp_PAForecastOvrhdAmtPerUnt = 0,
         @temp_PAForecastOvrhdPct = 0,
         @temp_PAForecastPTaxOptions = 0,
         @temp_PAForecastCTaxSchedID = '',
         @temp_PAForecastSTaxOptions = 0,
         @temp_PAForecastBTaxSchedID = '',
         @temp_PAFBeginDate = '',
         @temp_PAFEndDate = '',
         @PAINCPRCHTXPRJCST = 0,
         @PAPostedBillingsN = 0,
         @PAUnit_of_Measure = '',
         @PAMCCURNCYID = '',
         @RATETPID = 0,
         @EXGTBLID = '',
         @XCHGRATE = 0,
         @EXCHDATE = '1/1/1900',
         @TIME1 = '1/1/1900',
         @RATECALC = 0,
         @DENXRATE = 0,
         @MCTRXSTT = 0,
         @PA_MC_Accrued_Revenue = 0,
         @PA_MC_Base_Billing_Rate = 0,
         @PA_MC_Billing_Rate = 0,
         @IsMC = 0,
         @MCDECPLCUR = 0,
         @PAMCCURRNIDX = ''
  
  exec @iStatus = taPAEmpExpenseLineInsertPre
    @I_vPAertrxtype output ,
    @I_vPAerdocnumber output ,
    @I_vEMPLOYID output ,
    @I_vPADT output ,
    @I_vPAPROJNUMBER output ,
    @I_vPACOSTCATID output ,
    @I_vCURNCYID output ,
    @I_vLNITMSEQ output ,
    @I_vPAitemnumber output ,
    @I_vPAREFNO output ,
    @I_vPAQtyQ output ,
    @I_vPAUnit_of_Measure output ,
    @I_vUOMSCHDL output ,
    @I_vPAUNITCOST output ,
    @I_vPAPurchase_Tax_Options output ,
    @I_vITMTSHID output ,
    @I_vTAXAMNT output ,
    @I_vBCKTXAMT output ,
    @I_vPAPaymentMethod output ,
    @I_vPAExpenseType output ,
    @I_vPAReference_Line_Seq_N output ,
    @I_vPAbllngtype output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1906
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAertrxtype is NULL 
       or @I_vPAerdocnumber is NULL
       or @I_vEMPLOYID is NULL
       or @I_vPADT is NULL
       or @I_vPAPROJNUMBER is NULL
       or @I_vPACOSTCATID is NULL)
    begin
      select @O_iErrorState = 1907
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAertrxtype = 0
       or @I_vPAerdocnumber = ''
       or @I_vEMPLOYID = ''
       or @I_vPADT = ''
       or @I_vPAPROJNUMBER = ''
       or @I_vPACOSTCATID = '')
    begin
      select @O_iErrorState = 1908
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAUNITCOST < 0
       or @I_vTAXAMNT < 0
       or @I_vBCKTXAMT < 0)
    begin
      select @O_iErrorState = 1909
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vEMPLOYID = UPPER(@I_vEMPLOYID),
         @I_vPAPROJNUMBER = UPPER(@I_vPAPROJNUMBER),
         @I_vPACOSTCATID = UPPER(@I_vPACOSTCATID),
         @I_vPAerdocnumber = UPPER(@I_vPAerdocnumber),
         @I_vPAUnit_of_Measure = UPPER(@I_vPAUnit_of_Measure),
         @I_vUOMSCHDL = UPPER(@I_vUOMSCHDL),
         @I_vITMTSHID = UPPER(@I_vITMTSHID),
         @I_vCURNCYID = UPPER(@I_vCURNCYID)
  
  select @DEX_ROW_ID = DEX_ROW_ID,
         @PAVIDROAer = isnull(PAVIDROAer,0),
         @PAeeprofittypefrom = isnull(PAeeprofittypefrom,0),
         @PAallow_1 = isnull(PAallow_1,0),
         @PAallow_2 = isnull(PAallow_2,0),
         @PAallow_3 = isnull(PAallow_3,0),
         @PAallow_4 = isnull(PAallow_4,0),
         @PAallow_5 = isnull(PAallow_5,0),
         @PAallow_6 = isnull(PAallow_6,0),
         @PAallow_7 = isnull(PAallow_7,0)
  from   PA42401 (nolock)
  
  if (@DEX_ROW_ID IS NULL)
    begin
      select @O_iErrorState = 1911
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @PAINCPRCHTXPRJCST = isnull(PAINCPRCHTXPRJCST,0)
  from   PA41701 (nolock)
  
  if (@I_vCURNCYID <> '')
    begin
      select @CURRNIDX = isnull(CURRNIDX,0),
             @MCDECPLCUR = isnull(DECPLCUR - 1,0)
      from   DYNAMICS..MC40200 (nolock)
      where  CURNCYID = @I_vCURNCYID
      
      if (@CURRNIDX = 0)
        begin
          select @O_iErrorState = 5877
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if ((@I_vPAUNITCOST = 0)
      and (@PAallow_3 = 0))
    begin
      select @O_iErrorState = 6474
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAertrxtype < 1)
       or (@I_vPAertrxtype > 2))
    begin
      select @O_iErrorState = 1912
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAertrxtype = 1)
    select @I_vPAREFNO = ''
  
  if @I_vEMPLOYID <> ''
    begin
      if not exists (select 1
                     from   UPR00100 (nolock)
                     where  EMPLOYID = @I_vEMPLOYID)
        begin
          select @O_iErrorState = 1913
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if not exists (select 1
                     from   PM00200 (nolock)
                     where  VENDORID = @I_vEMPLOYID)
        begin
          select @O_iErrorState = 1914
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if not exists (select 1
                     from   PA00601 (nolock)
                     where  EMPLOYID = @I_vEMPLOYID)
        begin
          select @O_iErrorState = 1915
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @FUNLCURR = isnull(FUNLCURR,''),
         @FUNCRIDX = isnull(FUNCRIDX,0)
  from   MC40000 (nolock)
  
  if (@CURNCYID = '')
    select @CURNCYID = isnull(FUNLCURR,''),
           @CURRNIDX = isnull(FUNCRIDX,0)
    from   MC40000 (nolock)
  
  if ((@I_vPAPROJNUMBER <> '')
      and (@I_vPAPROJNUMBER <> '<NONE>'))
    begin
      select @l_PAPROJNUMBER = PAPROJNUMBER,
             @PASTAT = isnull(PASTAT,0),
             @PAcloseProjcosts = isnull(PAcloseProjcosts,0),
             @PACONTNUMBER = isnull(PACONTNUMBER,''),
             @PAProjectType = isnull(PAProjectType,0),
             @PAAcctgMethod = isnull(PAAcctgMethod,0),
             @CUSTNMBR = isnull(CUSTNMBR,'')
      from   PA01201 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@l_PAPROJNUMBER = '')
        begin
          select @O_iErrorState = 1916
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if ((@PASTAT = 2)
               or (@PASTAT = 3)
               or (@PASTAT = 4))
            begin
              select @O_iErrorState = 1917
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          if (@PAcloseProjcosts = 1)
            begin
              select @O_iErrorState = 6465
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  else
    begin
      select @PAProjectType = 1,
             @PACONTNUMBER = '<NONE>',
             @PAAcctgMethod = 2
    end
  
  if ((@PACONTNUMBER <> '')
      and (@PACONTNUMBER <> '<NONE>'))
    begin
      select @l_PACONTNUMBER = PACONTNUMBER,
             @PASTAT = isnull(PASTAT,0),
             @PAcloseProjcosts = isnull(PAcloseProjcosts,0),
             @PAMCCURNCYID = ISNULL(PAMCCURNCYID,'')
      from   PA01101 (nolock)
      where  PACONTNUMBER = @PACONTNUMBER
      
      if (@l_PACONTNUMBER IS NULL)
        begin
          select @O_iErrorState = 1918
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if ((@PASTAT = 2)
               or (@PASTAT = 3)
               or (@PASTAT = 4))
            begin
              select @O_iErrorState = 1919
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          if (@PAcloseProjcosts = 1)
            begin
              select @O_iErrorState = 6466
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if (@PAMCCURNCYID <> '')
    select @PAMCCURRNIDX = isnull(CURRNIDX,0)
    from   DYNAMICS..MC40200 (nolock)
    where  CURNCYID = @PAMCCURNCYID
  
  if @FUNLCURR <> @PAMCCURNCYID
    begin
      select @IsMC = 1
      
      select @RATETPID = isnull(PAFRATETPID,''),
             @EXGTBLID = isnull(PAFEXGTBLID,''),
             @XCHGRATE = isnull(PAFXCHGRATE,0),
             @EXCHDATE = isnull(PAFEXCHDATE,'1/1/1900'),
             @TIME1 = isnull(PAFTIME1,'1/1/1900'),
             @RATECALC = isnull(PAFRTCLCMTD,0),
             @DENXRATE = isnull(PAFDENXRATE,0),
             @MCTRXSTT = isnull(PAFMCTRXSTT,0)
      from   PA01202 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
    end
  
  if ((@I_vPAPROJNUMBER <> '')
       or (@I_vPAPROJNUMBER <> '<NONE>'))
    begin
      select @l_CUSTNMBR = CUSTNMBR,
             @PAcloseProjcosts = isnull(PAcloseProjcosts,0)
      from   PA00501 (nolock)
      where  CUSTNMBR = @CUSTNMBR
      
      if (@l_CUSTNMBR IS NULL)
        begin
          select @O_iErrorState = 1920
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if (@PAcloseProjcosts = 1)
            begin
              select @O_iErrorState = 1921
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if exists (select 1
             from   PA01408 (nolock)
             where  PAPROJNUMBER = @I_vPAPROJNUMBER)
    begin
      if not exists (select 1
                     from   PA01408 (nolock)
                     where  PAPROJNUMBER = @I_vPAPROJNUMBER
                            and EMPLOYID = @I_vEMPLOYID)
        begin
          if (@PAallow_7 = 0)
            begin
              select @O_iErrorState = 1922
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          else
            begin
              select @add_employee_access = 1
            end
        end
    end
  
  if (@I_vPAertrxtype = 2)
    begin
      if (@I_vPAREFNO = '')
        begin
          select @O_iErrorState = 1923
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if not exists (select 1
                         from   PA30501 (nolock)
                         where  PAerdocnumber = @I_vPAREFNO
                                and PAPROJNUMBER = @I_vPAPROJNUMBER
                                and PADT = @I_vPADT)
            begin
              select @O_iErrorState = 1924
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  select @DECPLQTY = isnull(DECPLQTY,0)
  from   PA41701 (nolock)
  
  if (@I_vPAQtyQ = 0)
     and (@PAallow_2 = 0)
    begin
      select @O_iErrorState = 6467
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPACOSTCATID <> '')
    begin
      select @l_PACOSTCATID = isnull(PACOSTCATID,''),
             @PATU = isnull(PATU,0),
             @PAinactive = isnull(PAinactive,0),
             @DECPLQTY = isnull(DECPLQTY,@DECPLQTY),
             @PAIV_Item_Checkbox = isnull(PAIV_Item_Checkbox,0)
      from   PA01001 (nolock)
      where  PACOSTCATID = @I_vPACOSTCATID
      
      if ((@l_PACOSTCATID = '')
          and (@I_vPACOSTCATID <> '<NONE>'))
        begin
          select @O_iErrorState = 1925
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        begin
          if ((@I_vPAPROJNUMBER <> '<NONE>')
              and (@I_vPACOSTCATID = '<NONE>'))
            begin
              select @O_iErrorState = 1926
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          if (@PAinactive = 1)
            begin
              select @O_iErrorState = 1927
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          if ((@PATU <> 5)
              and (@I_vPACOSTCATID <> '<NONE>'))
            begin
              select @O_iErrorState = 1928
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
          
          select @PACHGORDNO = isnull(PACHGORDNO,'')
          from   PA01301 (nolock)
          where  PAPROJNUMBER = @I_vPAPROJNUMBER
                 and PACOSTCATID = @I_vPACOSTCATID
                 and PATU = 5
          
          if (@I_vPAertrxtype = 2)
            begin
              if (@I_vPAReference_Line_Seq_N = 0)
                begin
                  select @I_vPAReference_Line_Seq_N = isnull(LNITMSEQ,0)
                  from   PA30501 (nolock)
                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                         and PACOSTCATID = @I_vPACOSTCATID
                         and PADT = @I_vPADT
                         and PAerdocnumber = @I_vPAREFNO
                         and PAitemnumber = @I_vPAitemnumber
                end
              else
                begin
                  select @I_vPAReference_Line_Seq_N = isnull(LNITMSEQ,0)
                  from   PA30501 (nolock)
                  where  PAerdocnumber = @I_vPAREFNO
                         and LNITMSEQ = @I_vPAReference_Line_Seq_N
                end
              
              if (@I_vPAReference_Line_Seq_N = 0)
                begin
                  select @O_iErrorState = 1929
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  return (@O_iErrorState)
                end
              
              select @I_vPAUnit_of_Measure = PAUnit_of_Measure,
                     @I_vUOMSCHDL = UOMSCHDL,
                     @I_vPAUNITCOST = PAUNITCOST,
                     @PABase_Unit_Cost = PABase_Unit_Cost,
                     @PAMARKPERCENT = PAMARKPERCENT,
                     @PABILRATE = PABILRATE,
                     @PA_Base_Billing_Rate = PA_Base_Billing_Rate,
                     @I_vPAPurchase_Tax_Options = PAPurchase_Tax_Options,
                     @I_vITMTSHID = ITMTSHID,
                     @PAbllngtype = PAbllngtype,
                     @I_vPAPaymentMethod = PAPaymentMethod,
                     @I_vPAExpenseType = PAExpenseType,
                     @PAProfitType = PAProfitType,
                     @PAProfitPercent = PAProfitPercent,
                     @PAProfitAmount = PAProfitAmount,
                     @PATotalProfit = PATotalProfit,
                     @ORUNTCST = ORUNTCST,
                     @PABase_Unit_Cost = PABase_Unit_Cost,
                     @PAORIGBSBILLRTE = PAORIGBSBILLRTE,
                     @PAORIGBILLRATE = PAORIGBILLRATE,
                     @PAORIGTOTPROF = PAORIGTOTPROF,
                     @PAORIGPROFAMT = PAORIGPROFAMT,
                     @PABilling_StatusN = PABilling_StatusN,
                     @PACogs_Idx = PACogs_Idx,
                     @PAContra_Account_IDX = PAContra_Account_IDX,
                     @PAUnbilled_AR_Idx = PAUnbilled_AR_Idx,
                     @PAUnbilled_Proj_Rev_Idx = PAUnbilled_Proj_Rev_Idx,
                     @PACGBWIPIDX = PACGBWIPIDX,
                     @PACHGORDNO = PACHGORDNO
              from   PA30501 (nolock)
              where  PAerdocnumber = @I_vPAREFNO
                     and LNITMSEQ = @I_vPAReference_Line_Seq_N
            end
          else
            begin
              if (@I_vPAPROJNUMBER <> '<NONE>')
                begin
                  if not exists (select 1
                                 from   PA01301 (nolock)
                                 where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                        and PACOSTCATID = @I_vPACOSTCATID
                                        and PATU = @PATU)
                    begin
                      execute @iStatus = taPAProjectBudgetMaster
                        @I_vPAPROJNUMBER = @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID = @I_vPACOSTCATID ,
                        @I_vPABQuantity = 0 ,
                        @O_iErrorState = @taPAProjectBudgetMasterErrorState output ,
                        @oErrString = @otaPAProjectBudgetMasterErrString output
                      
                      select @iError = @@error
                      
                      if ((@iStatus <> 0)
                           or (@taPAProjectBudgetMasterErrorState <> 0)
                           or (@iError <> 0))
                        begin
                          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@taPAProjectBudgetMasterErrorState))
                          
                          select @O_iErrorState = 6613
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      select @temp_PAProfitType = PAProfitType,
                             @temp_PAbllngtype = PAbllngtype,
                             @temp_LNITMSEQ = LNITMSEQ,
                             @temp_PALineItemSeq = PALineItemSeq,
                             @temp_PAFQuantity = PAFQuantity,
                             @temp_PAFUnitCost = PAFUnitCost,
                             @temp_PAFTotalCost = PAFTotalCost,
                             @temp_PAFProfitAmt = PAFProfitAmt,
                             @temp_PAFProfitPcnt = PAFProfitPcnt,
                             @temp_PAForecastOvrhdAmtPerUnt = PAForecastOvrhdAmtPerUnt,
                             @temp_PAForecastOvrhdPct = PAForecastOvrhdPct,
                             @temp_PAForecastPTaxOptions = PAForecastPTaxOptions,
                             @temp_PAForecastCTaxSchedID = PAForecastCTaxSchedID,
                             @temp_PAForecastSTaxOptions = PAForecastSTaxOptions,
                             @temp_PAForecastBTaxSchedID = PAForecastBTaxSchedID,
                             @temp_PAFBeginDate = PAFBeginDate,
                             @temp_PAFEndDate = PAFEndDate
                      from   PA01301 (nolock)
                      where  PAPROJNUMBER = @I_vPAPROJNUMBER
                             and PACOSTCATID = @I_vPACOSTCATID
                             and PATU = @PATU
                      
                      execute @iStatus = taPAProjectBudgetMaster
                        @I_vPAPROJNUMBER = @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID = @I_vPACOSTCATID ,
                        @I_vPASTAT = 1 ,
                        @I_vPAProfitType = @temp_PAProfitType ,
                        @I_vPAbllngtype = @temp_PAbllngtype ,
                        @I_vLNITMSEQ = @temp_LNITMSEQ ,
                        @I_vPALineItemSeq = @temp_PALineItemSeq ,
                        @I_vPAFQuantity = @temp_PAFQuantity ,
                        @I_vPAFUnitCost = @temp_PAFUnitCost ,
                        @I_vPAFTotalCost = @temp_PAFTotalCost ,
                        @I_vPAFProfitAmt = @temp_PAFProfitAmt ,
                        @I_vPAFProfitPcnt = @temp_PAFProfitPcnt ,
                        @I_vPAForecastOvrhdAmtPerUnt = @temp_PAForecastOvrhdAmtPerUnt ,
                        @I_vPAForecastOvrhdPct = @temp_PAForecastOvrhdPct ,
                        @I_vPAForecastPTaxOptions = @temp_PAForecastPTaxOptions ,
                        @I_vPAForecastCTaxSchedID = @temp_PAForecastCTaxSchedID ,
                        @I_vPAForecastSTaxOptions = @temp_PAForecastSTaxOptions ,
                        @I_vPAForecastBTaxSchedID = @temp_PAForecastBTaxSchedID ,
                        @I_vPAFBeginDate = @temp_PAFBeginDate ,
                        @I_vPAFEndDate = @temp_PAFEndDate ,
                        @I_vUpdateExisting = 1 ,
                        @O_iErrorState = @taPAProjectBudgetMasterErrorState output ,
                        @oErrString = @otaPAProjectBudgetMasterErrString output
                      
                      select @iError = @@error
                      
                      if ((@iStatus <> 0)
                           or (@taPAProjectBudgetMasterErrorState <> 0)
                           or (@iError <> 0))
                        begin
                          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@taPAProjectBudgetMasterErrorState))
                          
                          select @O_iErrorState = 6614
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                  
                  select @l_PAPROJNUMBER = PAPROJNUMBER,
                         @PASTAT = isnull(PASTAT,0),
                         @PAbllngtype = PAbllngtype,
                         @I_vUOMSCHDL = case 
                                          when (@I_vUOMSCHDL = '') then isnull(UOMSCHDL,'')
                                          else @I_vUOMSCHDL
                                        end,
                         @PABilling_StatusN = case 
                                                WHEN @PAProjectType = 1
                                                     and @I_vPAbllngtype <> 3 THEN 1
                                                WHEN @PAProjectType <> 1
                                                     and @I_vPAbllngtype <> 3 THEN 5
                                                ELSE 4
                                              END
                  from   PA01301 (nolock)
                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                         and PACOSTCATID = @I_vPACOSTCATID
                         and PATU = 5
                  
                  if (@l_PAPROJNUMBER = '')
                    begin
                      select @O_iErrorState = 1930
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  else
                    begin
                      if ((@PASTAT = 2)
                           or (@PASTAT = 3)
                           or (@PASTAT = 4))
                        begin
                          select @O_iErrorState = 1931
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                end
              
              if ((@I_vPACOSTCATID <> '<NONE>')
                  and (@I_vPAPROJNUMBER = '<NONE>'))
                begin
                  select @I_vUOMSCHDL = case 
                                          when (@I_vUOMSCHDL = '') then isnull(UOMSCHDL,'')
                                          else @I_vUOMSCHDL
                                        end
                  from   PA01001 (nolock)
                  where  PACOSTCATID = @I_vPACOSTCATID
                end
              
              if (@PAVIDROAer = 2)
                begin
                  if (@I_vPAPROJNUMBER <> '<NONE>')
                    begin
                      select @PAUnitCost = isnull(PAFUnitCost,0),
                             @I_vPAUnit_of_Measure = case 
                                                       when @I_vPAUnit_of_Measure = '' then isnull(PAUnit_of_Measure,'')
                                                       else @I_vPAUnit_of_Measure
                                                     end,
                             @DECPLCUR = isnull(DECPLCUR,0)
                      from   PA01301 (nolock)
                      where  PAPROJNUMBER = @I_vPAPROJNUMBER
                             and PACOSTCATID = @I_vPACOSTCATID
                    end
                  else
                    if (@I_vPACOSTCATID <> '<NONE>')
                      begin
                        select @PAUnitCost = isnull(PAUNITCOST,0),
                               @I_vPAUnit_of_Measure = case 
                                                         when @I_vPAUnit_of_Measure = '' then isnull(PAUnit_of_Measure,'')
                                                         else @I_vPAUnit_of_Measure
                                                       end,
                               @DECPLCUR = isnull(DECPLCUR,0)
                        from   PA01001 (nolock)
                        where  PACOSTCATID = @I_vPACOSTCATID
                      end
                    else
                      begin
                        select @PAUnitCost = 0,
                               @PAOverhead_Amount = 0,
                               @PAOverheaPercentage = 0
                      end
                end
              else
                if (@PAVIDROAer = 3)
                  begin
                    if (@I_vPACOSTCATID <> '<NONE>')
                      begin
                        select @PAUnitCost = isnull(PAUNITCOST,0),
                               @I_vPAUnit_of_Measure = case 
                                                         when @I_vPAUnit_of_Measure = '' then isnull(PAUnit_of_Measure,'')
                                                         else @I_vPAUnit_of_Measure
                                                       end,
                               @DECPLCUR = isnull(DECPLCUR,0)
                        from   PA01001 (nolock)
                        where  PACOSTCATID = @I_vPACOSTCATID
                      end
                    
                    if (@I_vPAPROJNUMBER <> '<NONE>')
                      begin
                        select @PAUnitCost = isnull(PAFUnitCost,0),
                               @I_vPAUnit_of_Measure = case 
                                                         when @I_vPAUnit_of_Measure = '' then isnull(PAUnit_of_Measure,'')
                                                         else @I_vPAUnit_of_Measure
                                                       end,
                               @DECPLCUR = isnull(DECPLCUR,0)
                        from   PA01301 (nolock)
                        where  PAPROJNUMBER = @I_vPAPROJNUMBER
                               and PACOSTCATID = @I_vPACOSTCATID
                        
                        if (@I_vPACOSTCATID <> '<NONE>')
                          begin
                            select @DECPLCUR = isnull(DECPLCUR,0)
                            from   PA01001 (nolock)
                            where  PACOSTCATID = @I_vPACOSTCATID
                          end
                      end
                    else
                      begin
                        select @PAUnitCost = 0,
                               @I_vPAUnit_of_Measure = case 
                                                         when @I_vPAUnit_of_Measure = '' then ''
                                                         else @I_vPAUnit_of_Measure
                                                       end
                      end
                  end
                else
                  begin
                    select @I_vPAUnit_of_Measure = case 
                                                     when (@I_vUOMSCHDL <> '') then isnull(BASEUOFM,'')
                                                     else ''
                                                   end,
                           @PAUnitCost = 0
                    from   PA40101 (nolock)
                    where  UOMSCHDL = @I_vUOMSCHDL
                  end
              
              if (@I_vUOMSCHDL <> '')
                begin
                  if (@I_vPAUnit_of_Measure <> '')
                    begin
                      select @BASEUOFM = BASEUOFM
                      from   PA40101 (nolock)
                      where  UOMSCHDL = @I_vUOMSCHDL
                      
                      select @l_UOMSCHDL = isnull(UOMSCHDL,''),
                             @EQUOMQTY = isnull(EQUOMQTY,0)
                      from   PA40102 (nolock)
                      where  UOMSCHDL = @I_vUOMSCHDL
                             and UOFM = @I_vPAUnit_of_Measure
                             and EQUIVUOM = @BASEUOFM
                      
                      if (@l_UOMSCHDL = '')
                        select @I_vPAUnit_of_Measure = @BASEUOFM,
                               @EQUOMQTY = isnull(EQUOMQTY,0)
                        from   PA40102 (nolock)
                        where  UOMSCHDL = @I_vUOMSCHDL
                               and UOFM = @BASEUOFM
                               and EQUIVUOM = @BASEUOFM
                    end
                  else
                    begin
                      if not exists (select 1
                                     from   PA40102 (nolock)
                                     where  UOMSCHDL = @l_UOMSCHDL
                                            and UOFM = @I_vPAUnit_of_Measure
                                            and EQUIVUOM = @BASEUOFM)
                        begin
                          select @O_iErrorState = 6477
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                end
              
              if (@I_vPAUNITCOST = 0)
                begin
                  select @I_vPAUNITCOST = @PAUnitCost
                end
              
              select @PABase_Unit_Cost = case 
                                           when @I_vUOMSCHDL = '' then @I_vPAUNITCOST
                                           else round(@I_vPAUNITCOST / @EQUOMQTY,(@DECPLCUR - 1))
                                         end,
                     @PABaseOvhdCost = case 
                                         when @I_vUOMSCHDL = '' then @PAOverhead_Amount
                                         else round(@PAOverhead_Amount / @EQUOMQTY,(@DECPLCUR - 1))
                                       end,
                     @DECPLCUR = @DECPLCUR + 6
              
              if (@I_vPAPROJNUMBER <> '<NONE>')
                begin
                  select @l_PAPROJNUMBER = PAPROJNUMBER,
                         @PAbllngtype = isnull(PAbllngtype,0),
                         @PAPurchase_Tax_Options = isnull(PAForecastPTaxOptions,0),
                         @ITMTSHID = isnull(PAForecastCTaxSchedID,''),
                         @DECPLQTY = isnull(DECPLQTY,0)
                  from   PA01301 (nolock)
                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                         and PACOSTCATID = @I_vPACOSTCATID
                         and PATU = 5
                end
              else
                begin
                  select @PAbllngtype = 3,
                         @PAPurchase_Tax_Options = isnull(PAPurchase_Tax_Options,0),
                         @ITMTSHID = isnull(PAcostaxscheduleid,''),
                         @I_vUOMSCHDL = case 
                                          when (@I_vUOMSCHDL = '') then isnull(UOMSCHDL,'')
                                          else @I_vUOMSCHDL
                                        end
                  from   PA01001 (nolock)
                  where  PACOSTCATID = @I_vPACOSTCATID
                end
              
              if (@I_vPAbllngtype = -1)
                 and (@PAbllngtype > 0)
                select @I_vPAbllngtype = @PAbllngtype
              
              if (@I_vPAbllngtype = -1)
                 and (@PAbllngtype = 0)
                select @I_vPAbllngtype = 1
              
              if (@IsMC = 1)
                begin
                  select @PABILRATE = case 
                                        when (@PAProjectType = 1)
                                             and (PAProfitType = 1) then isnull(PAFProfitAmt,0)
                                        else 0
                                      end,
                         @PA_MC_Billing_Rate = case 
                                                 when (@PAProjectType = 1)
                                                      and (PAProfitType = 1) then isnull(PAMCFProfitAmt,0)
                                                 else 0
                                               end,
                         @PAProfitType = isnull(PAProfitType,0)
                  from   PA01301 (nolock)
                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                         and PACOSTCATID = @I_vPACOSTCATID
                         and PATU = 5
                end
              else
                begin
                  if (@I_vPAbllngtype = 1)
                    begin
                      if (@PAeeprofittypefrom = 1)
                        begin
                          if (@I_vPAPROJNUMBER <> '<NONE>')
                            begin
                              select @PABILRATE = case 
                                                    when (@PAProjectType = 1)
                                                         and (PAProfitType = 1) then isnull(PAFProfitAmt,0)
                                                    else 0
                                                  end,
                                     @PAMARKPERCENT = case 
                                                        when (@PAProjectType = 1)
                                                             and (PAProfitType = 2) then isnull(PAFProfitPcnt,0)
                                                        else 0
                                                      end,
                                     @PAProfitAmount = case 
                                                         when ((@PAProjectType = 2)
                                                                or (@PAProjectType = 3))
                                                              and ((PAProfitType = 3)
                                                                    or (PAProfitType = 4)) then isnull(PAFProfitAmt,0)
                                                         else 0
                                                       end,
                                     @PATotalProfit = case 
                                                        when ((@PAProjectType = 2)
                                                               or (@PAProjectType = 3))
                                                             and (PAProfitType = 5) then isnull(PAFProfit,0)
                                                        else 0
                                                      end,
                                     @PAProfitPercent = case 
                                                          when ((@PAProjectType = 2)
                                                                 or (@PAProjectType = 3))
                                                               and (PAProfitType = 6) then isnull(PABProfitPcnt,0)
                                                          when (@PAProjectType = 2)
                                                               and (PAProfitType = 7) then isnull(PABProfitPcnt,0)
                                                          else 0
                                                        end,
                                     @PAProfitType = isnull(PAProfitType,0)
                              from   PA01301 (nolock)
                              where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                     and PACOSTCATID = @I_vPACOSTCATID
                                     and PATU = 5
                            end
                          else
                            if (@I_vPACOSTCATID <> '<NONE>')
                              begin
                                select @PABILRATE = case 
                                                      when (@PAProjectType = 1)
                                                           and (PATMProfitType = 1) then isnull(PATMProfitAmount,0)
                                                      else 0
                                                    end,
                                       @PAMARKPERCENT = case 
                                                          when (@PAProjectType = 1)
                                                               and (PATMProfitType = 2) then isnull(PATMProfitPercent,0)
                                                          else 0
                                                        end,
                                       @PAProfitAmount = case 
                                                           when (@PAProjectType = 2)
                                                                and ((PAProfit_Type__CP = 3)
                                                                      or (PAProfit_Type__CP = 4)) then isnull(PAProfitAmountCP,0)
                                                           when (@PAProjectType = 3)
                                                                and ((PAFFProfitType = 3)
                                                                      or (PAFFProfitType = 4)
                                                                      or (PAFFProfitType = 5)) then isnull(PAFFProfitAmount,0)
                                                           else 0
                                                         end,
                                       @PATotalProfit = case 
                                                          when (@PAProjectType = 2)
                                                               and (PAProfit_Type__CP = 5) then isnull(PAProfitAmountCP,0)
                                                          else 0
                                                        end,
                                       @PAProfitPercent = case 
                                                            when (@PAProjectType = 2)
                                                                 and ((PAProfit_Type__CP = 6)
                                                                       or (PAProfit_Type__CP = 7)) then isnull(PAProfitPercentCP,0)
                                                            when (@PAProjectType = 3)
                                                                 and (PAFFProfitType = 6) then isnull(PAFFProfitType,0)
                                                            else 0
                                                          end,
                                       @PAProfitType = isnull(PATMProfitType,0)
                                from   PA01001 (nolock)
                                where  PACOSTCATID = @I_vPACOSTCATID
                              end
                            else
                              begin
                                select @PAProfitType = 0
                              end
                        end
                      else
                        if (@PAeeprofittypefrom = 2)
                          begin
                            if (@I_vPAPROJNUMBER <> '<NONE>')
                              begin
                                select @PAProfitType = isnull(PAProjectType,0)
                                from   PA01201 (nolock)
                                where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                
                                select @PABILRATE = case 
                                                      when (@PAProjectType = 1)
                                                           and (PATMProfitType = 1) then isnull(PATMProfitAmount,0)
                                                      else 0
                                                    end,
                                       @PAMARKPERCENT = case 
                                                          when (@PAProjectType = 1)
                                                               and (PATMProfitType = 2) then isnull(PATMProfitPercent,0)
                                                          else 0
                                                        end,
                                       @PAProfitAmount = case 
                                                           when (@PAProjectType = 2)
                                                                and ((PAProfit_Type__CP = 3)
                                                                      or (PAProfit_Type__CP = 4)) then isnull(PAProfitAmountCP,0)
                                                           when (@PAProjectType = 3)
                                                                and ((PAFFProfitType = 3)
                                                                      or (PAFFProfitType = 4)
                                                                      or (PAFFProfitType = 5)) then isnull(PAFFProfitAmount,0)
                                                           else 0
                                                         end,
                                       @PATotalProfit = case 
                                                          when (@PAProjectType = 2)
                                                               and (PAProfit_Type__CP = 5) then isnull(PAProfitAmountCP,0)
                                                          else 0
                                                        end,
                                       @PAProfitPercent = case 
                                                            when (@PAProjectType = 2)
                                                                 and ((PAProfit_Type__CP = 6)
                                                                       or (PAProfit_Type__CP = 7)) then isnull(PAProfitPercentCP,0)
                                                            when (@PAProjectType = 3)
                                                                 and (PAFFProfitType = 6) then isnull(PAFFProfitType,0)
                                                            else 0
                                                          end,
                                       @PAProfitType = case 
                                                         when (@PAProjectType = 1) then PATMProfitType
                                                         when (@PAProjectType = 2) then PAProfit_Type__CP
                                                         when (@PAProjectType = 3) then PAFFProfitType
                                                         else @PAProfitType
                                                       end
                                from   PA00601 (nolock)
                                where  EMPLOYID = @I_vEMPLOYID
                              end
                            else
                              begin
                                select @PAProfitType = isnull(PATMProfitType,0),
                                       @PABILRATE = case 
                                                      when (PATMProfitType = 1) then isnull(PATMProfitAmount,0)
                                                      else 0
                                                    end,
                                       @PAMARKPERCENT = case 
                                                          when (PATMProfitType = 2) then isnull(PATMProfitPercent,0)
                                                          else 0
                                                        end
                                from   PA00601 (nolock)
                                where  EMPLOYID = @I_vEMPLOYID
                              end
                          end
                        else
                          if (@PAeeprofittypefrom = 3)
                            begin
                              if (@I_vPAPROJNUMBER <> '<NONE>')
                                begin
                                  select @PABILRATE = case 
                                                        when (@PAProjectType = 1)
                                                             and (PATMProfitType = 1) then isnull(PATMProfitAmount,0)
                                                        else 0
                                                      end,
                                         @PAMARKPERCENT = case 
                                                            when (@PAProjectType = 1)
                                                                 and (PATMProfitType = 2) then isnull(PATMProfitPercent,0)
                                                            else 0
                                                          end,
                                         @PAProfitAmount = case 
                                                             when (@PAProjectType = 2)
                                                                  and ((PAProfit_Type__CP = 3)
                                                                        or (PAProfit_Type__CP = 4)) then isnull(PAProfitAmountCP,0)
                                                             when (@PAProjectType = 3)
                                                                  and ((PAFFProfitType = 3)
                                                                        or (PAFFProfitType = 4)
                                                                        or (PAFFProfitType = 5)) then isnull(PAFFProfitAmount,0)
                                                             else 0
                                                           end,
                                         @PATotalProfit = case 
                                                            when (@PAProjectType = 2)
                                                                 and (PAProfit_Type__CP = 5) then isnull(PAProfitAmountCP,0)
                                                            else 0
                                                          end,
                                         @PAProfitPercent = case 
                                                              when (@PAProjectType = 2)
                                                                   and ((PAProfit_Type__CP = 6)
                                                                         or (PAProfit_Type__CP = 7)) then isnull(PAProfitPercentCP,0)
                                                              when (@PAProjectType = 3)
                                                                   and (PAFFProfitType = 6) then isnull(PAFFProfitType,0)
                                                              else 0
                                                            end,
                                         @PAProfitType = case 
                                                           when (@PAProjectType = 1) then PATMProfitType
                                                           when (@PAProjectType = 2) then PAProfit_Type__CP
                                                           when (@PAProjectType = 3) then PAFFProfitType
                                                           else @PAProfitType
                                                         end
                                  from   PA01001 (nolock)
                                  where  PACOSTCATID = @I_vPACOSTCATID
                                end
                              else
                                begin
                                  select @PAProfitType = isnull(PATMProfitType,0),
                                         @PABILRATE = case 
                                                        when (PATMProfitType = 1) then isnull(PATMProfitAmount,0)
                                                        else 0
                                                      end,
                                         @PAMARKPERCENT = case 
                                                            when (PATMProfitType = 2) then isnull(PATMProfitPercent,0)
                                                            else 0
                                                          end
                                  from   PA01001 (nolock)
                                  where  PACOSTCATID = @I_vPACOSTCATID
                                end
                            end
                    end
                end
              
              if (@I_vUOMSCHDL <> '')
                begin
                  select @BASEUOFM = BASEUOFM
                  from   PA40101 (nolock)
                  where  UOMSCHDL = @I_vUOMSCHDL
                  
                  select @l_UOMSCHDL = isnull(UOMSCHDL,''),
                         @EQUOMQTY = isnull(EQUOMQTY,0)
                  from   PA40102 (nolock)
                  where  UOMSCHDL = @I_vUOMSCHDL
                         and UOFM = @I_vPAUnit_of_Measure
                         and EQUIVUOM = @BASEUOFM
                  
                  if (@l_UOMSCHDL <> '')
                    begin
                      select @PA_Base_Billing_Rate = round(@PABILRATE / @EQUOMQTY,(@DECPLCUR - 1))
                      
                      if (@IsMC = 1)
                        select @PA_MC_Base_Billing_Rate = round(@PA_MC_Billing_Rate / @EQUOMQTY,(@DECPLCUR - 1))
                    end
                  else
                    begin
                      select @O_iErrorState = 1932
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                end
              else
                begin
                  select @PA_Base_Billing_Rate = @PABILRATE
                end
              
              if (@PAProjectType = 1)
                begin
                  if (@I_vPAbllngtype = 1)
                    begin
                      if (@PAAcctgMethod <> 1)
                        begin
                          exec @iStatus = taPAAuxAcctsGetIdx
                            @I_vPAPROJNUMBER ,
                            @I_vPACOSTCATID ,
                            @I_vEMPLOYID ,
                            5 ,
                            'EE' ,
                            1 ,
                            @CUSTNMBR ,
                            @PACONTNUMBER ,
                            @PACGBWIPIDX output ,
                            @O_oErrorState output
                          
                          select @iError = @@error
                          
                          if @iStatus = 0
                             and @iError <> 0
                            select @iStatus = @iError
                          
                          if (@iStatus <> 0)
                              or (@O_oErrorState <> 0)
                            begin
                              select @O_iErrorState = 1933
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                        end
                      
                      if (@PACGBWIPIDX = 0)
                        begin
                          exec @iStatus = taPAAuxAcctsGetIdx
                            @I_vPAPROJNUMBER ,
                            @I_vPACOSTCATID ,
                            @I_vEMPLOYID ,
                            5 ,
                            'EE' ,
                            2 ,
                            @CUSTNMBR ,
                            @PACONTNUMBER ,
                            @PACogs_Idx output ,
                            @O_oErrorState output
                          
                          select @iError = @@error
                          
                          if @iStatus = 0
                             and @iError <> 0
                            select @iStatus = @iError
                          
                          if (@iStatus <> 0)
                              or (@O_oErrorState <> 0)
                            begin
                              select @O_iErrorState = 1934
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                        end
                    end
                  else
                    begin
                      exec @iStatus = taPAAuxAcctsGetIdx
                        @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID ,
                        @I_vEMPLOYID ,
                        5 ,
                        'EE' ,
                        2 ,
                        @CUSTNMBR ,
                        @PACONTNUMBER ,
                        @PACogs_Idx output ,
                        @O_oErrorState output
                      
                      select @iError = @@error
                      
                      if @iStatus = 0
                         and @iError <> 0
                        select @iStatus = @iError
                      
                      if (@iStatus <> 0)
                          or (@O_oErrorState <> 0)
                        begin
                          select @O_iErrorState = 1935
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                  
                  if ((@I_vPAPROJNUMBER <> '<NONE>')
                      and (@PAAcctgMethod <> 2))
                    begin
                      exec @iStatus = taPAAuxAcctsGetIdx
                        @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID ,
                        @I_vEMPLOYID ,
                        5 ,
                        'EE' ,
                        4 ,
                        @CUSTNMBR ,
                        @PACONTNUMBER ,
                        @PAUnbilled_AR_Idx output ,
                        @O_oErrorState output
                      
                      select @iError = @@error
                      
                      if @iStatus = 0
                         and @iError <> 0
                        select @iStatus = @iError
                      
                      if (@iStatus <> 0)
                          or (@O_oErrorState <> 0)
                        begin
                          select @O_iErrorState = 1936
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if (@PAUnbilled_AR_Idx <> 0)
                        begin
                          exec @iStatus = taPAAuxAcctsGetIdx
                            @I_vPAPROJNUMBER ,
                            @I_vPACOSTCATID ,
                            @I_vEMPLOYID ,
                            5 ,
                            'EE' ,
                            5 ,
                            @CUSTNMBR ,
                            @PACONTNUMBER ,
                            @PAUnbilled_Proj_Rev_Idx output ,
                            @O_oErrorState output
                          
                          select @iError = @@error
                          
                          if @iStatus = 0
                             and @iError <> 0
                            select @iStatus = @iError
                          
                          if (@iStatus <> 0)
                              or (@O_oErrorState <> 0)
                            begin
                              select @O_iErrorState = 1937
                              
                              exec @iStatus = taUpdateString
                                @O_iErrorState ,
                                @oErrString ,
                                @oErrString output ,
                                @O_oErrorState output
                              
                              return (@O_iErrorState)
                            end
                          
                          if (@PAUnbilled_Proj_Rev_Idx = 0)
                            select @PAUnbilled_AR_Idx = 0
                        end
                    end
                  
                  if ((@PACGBWIPIDX = 0)
                      and (@PACogs_Idx = 0))
                    begin
                      select @O_iErrorState = 3801
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  
                  exec @iStatus = taPAAuxAcctsGetIdx
                    @I_vPAPROJNUMBER ,
                    @I_vPACOSTCATID ,
                    @I_vEMPLOYID ,
                    5 ,
                    'EE' ,
                    3 ,
                    @CUSTNMBR ,
                    @PACONTNUMBER ,
                    @PAContra_Account_IDX output ,
                    @O_oErrorState output
                  
                  select @iError = @@error
                  
                  if @iStatus = 0
                     and @iError <> 0
                    select @iStatus = @iError
                  
                  if (@iStatus <> 0)
                      or (@O_oErrorState <> 0)
                    begin
                      select @O_iErrorState = 1938
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                end
              else
                begin
                  exec @iStatus = taPAAuxAcctsGetIdx
                    @I_vPAPROJNUMBER ,
                    @I_vPACOSTCATID ,
                    @I_vEMPLOYID ,
                    5 ,
                    'EE' ,
                    30 ,
                    @CUSTNMBR ,
                    @PACONTNUMBER ,
                    @PACGBWIPIDX output ,
                    @O_oErrorState output
                  
                  select @iError = @@error
                  
                  if @iStatus = 0
                     and @iError <> 0
                    select @iStatus = @iError
                  
                  if (@iStatus <> 0)
                      or (@O_oErrorState <> 0)
                    begin
                      select @O_iErrorState = 1944
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  
                  if (@PACGBWIPIDX = 0)
                    begin
                      select @O_iErrorState = 3800
                      
                      exec @iStatus = taUpdateString
                        @O_iErrorState ,
                        @oErrString ,
                        @oErrString output ,
                        @O_oErrorState output
                      
                      return (@O_iErrorState)
                    end
                  else
                    begin
                      exec @iStatus = taPAAuxAcctsGetIdx
                        @I_vPAPROJNUMBER ,
                        @I_vPACOSTCATID ,
                        @I_vEMPLOYID ,
                        5 ,
                        'EE' ,
                        31 ,
                        @CUSTNMBR ,
                        @PACONTNUMBER ,
                        @PAContra_Account_IDX output ,
                        @O_oErrorState output
                      
                      select @iError = @@error
                      
                      if @iStatus = 0
                         and @iError <> 0
                        select @iStatus = @iError
                      
                      if (@iStatus <> 0)
                          or (@O_oErrorState <> 0)
                        begin
                          select @O_iErrorState = 1945
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                  
                  if (@FUNCRIDX <> @CURRNIDX)
                    begin
                      if not exists (select 1
                                     from   MC00200 (nolock)
                                     where  ACTINDX = @PACGBWIPIDX
                                            and CURNCYID = @CURNCYID)
                         and (@PACGBWIPIDX <> 0)
                        begin
                          select @O_iErrorState = 1939
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if not exists (select 1
                                     from   MC00200 (nolock)
                                     where  ACTINDX = @PACogs_Idx
                                            and CURNCYID = @CURNCYID)
                         and (@PACogs_Idx <> 0)
                        begin
                          select @O_iErrorState = 1940
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if not exists (select 1
                                     from   MC00200 (nolock)
                                     where  ACTINDX = @PAUnbilled_AR_Idx
                                            and CURNCYID = @CURNCYID)
                         and (@PAUnbilled_AR_Idx <> 0)
                        begin
                          select @O_iErrorState = 1941
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if not exists (select 1
                                     from   MC00200 (nolock)
                                     where  ACTINDX = @PAUnbilled_Proj_Rev_Idx
                                            and CURNCYID = @CURNCYID)
                         and (@PAUnbilled_Proj_Rev_Idx <> 0)
                        begin
                          select @O_iErrorState = 1942
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                      
                      if not exists (select 1
                                     from   MC00200 (nolock)
                                     where  ACTINDX = @PAContra_Account_IDX
                                            and CURNCYID = @CURNCYID)
                         and (@PAContra_Account_IDX <> 0)
                        begin
                          select @O_iErrorState = 1943
                          
                          exec @iStatus = taUpdateString
                            @O_iErrorState ,
                            @oErrString ,
                            @oErrString output ,
                            @O_oErrorState output
                          
                          return (@O_iErrorState)
                        end
                    end
                end
            end
        end
    end
  
  if ((@I_vPAQtyQ <> round(@I_vPAQtyQ,@DECPLQTY))
       or (@I_vPAUNITCOST <> round(@I_vPAUNITCOST,@DECPLCUR)))
    begin
      select @O_iErrorState = 7748
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vLNITMSEQ = 0)
    begin
      select @I_vLNITMSEQ = isnull(max(LNITMSEQ),0) + 32768
      from   PA10501 (nolock)
      where  PAerdocnumber = @I_vPAerdocnumber
    end
  
  if exists (select 1
             from   PA10501 (nolock)
             where  PAerdocnumber = @I_vPAerdocnumber
                    and LNITMSEQ = @I_vLNITMSEQ)
    begin
      select @O_iErrorState = 1948
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @PABase_Qty = case 
                         when (@EQUOMQTY <> 0) then (@I_vPAQtyQ * @EQUOMQTY)
                         else @I_vPAQtyQ
                       end
  
  select @PASTD_Qty = case 
                        when (@I_vPAbllngtype = 1) then @PABase_Qty
                        else 0
                      end,
         @PANC_Qty = case 
                       when (@I_vPAbllngtype = 2) then @PABase_Qty
                       else 0
                     end,
         @PANB_Qty = case 
                       when (@I_vPAbllngtype = 3) then @PABase_Qty
                       else 0
                     end,
         @PAEXTCOST = case 
                        when @MCDECPLCUR > @DECPLCUR then round((@I_vPAUNITCOST * @I_vPAQtyQ),@DECPLCUR)
                        else round((@I_vPAUNITCOST * @I_vPAQtyQ),@MCDECPLCUR)
                      end,
         @PAtotcosts = case 
                         when @MCDECPLCUR > @DECPLCUR then round(((@I_vPAUNITCOST * @I_vPAQtyQ) + @I_vTAXAMNT),
                                                                 @DECPLCUR)
                         else round(((@I_vPAUNITCOST * @I_vPAQtyQ) + @I_vTAXAMNT),
                                    @MCDECPLCUR)
                       end,
         @PAACREV = case 
                      when (@PAProjectType = 1)
                           and (@I_vPAbllngtype = 1)
                           and (@PAProfitType = 1) then round((@PA_Base_Billing_Rate * @PABase_Qty),@DECPLCUR - 1)
                      when (@PAProjectType = 1)
                           and (@I_vPAbllngtype = 1)
                           and (@PAProfitType = 2) then round((((@I_vPAUNITCOST * @I_vPAQtyQ) + @I_vTAXAMNT) * (1 + (@PAMARKPERCENT / 100))),
                                                              @DECPLCUR - 1)
                      when (@PAProjectType = 1)
                           and (@I_vPAbllngtype = 1)
                           and (@PAProfitType = 8) then round((@PA_Base_Billing_Rate * @PA_Base_Billing_Rate),
                                                              @DECPLCUR - 1)
                      else 0
                    end,
         @PAReimbursableAmount = case 
                                   when (@I_vPAExpenseType = 1)
                                        and ((@I_vPAPaymentMethod = 1)
                                              or (@I_vPAPaymentMethod = 3))
                                        and (@MCDECPLCUR > @DECPLCUR) then round(((@I_vPAUNITCOST * @I_vPAQtyQ) + @I_vTAXAMNT),
                                                                                 @DECPLCUR)
                                   when (@I_vPAExpenseType = 1)
                                        and ((@I_vPAPaymentMethod = 1)
                                              or (@I_vPAPaymentMethod = 3))
                                        and (@MCDECPLCUR < @DECPLCUR) then round(((@I_vPAUNITCOST * @I_vPAQtyQ) + @I_vTAXAMNT),
                                                                                 @MCDECPLCUR)
                                   when (@I_vPAExpenseType = 2)
                                        and (@I_vPAPaymentMethod = 2)
                                        and (@MCDECPLCUR > @DECPLCUR) then round(((@I_vPAUNITCOST * @I_vPAQtyQ) + @I_vTAXAMNT),
                                                                                 @DECPLCUR)
                                   when (@I_vPAExpenseType = 2)
                                        and (@I_vPAPaymentMethod = 2)
                                        and (@MCDECPLCUR < @DECPLCUR) then round(((@I_vPAUNITCOST * @I_vPAQtyQ) + @I_vTAXAMNT),
                                                                                 @MCDECPLCUR)
                                   else 0
                                 end
  
  if (@IsMC = 1)
    begin
      select @PA_MC_Accrued_Revenue = case 
                                        when (@PAProjectType = 1)
                                             and (@I_vPAbllngtype = 1)
                                             and (@PAProfitType = 1) then round((@PA_MC_Base_Billing_Rate * @PABase_Qty),
                                                                                @DECPLCUR - 1)
                                        else 0
                                      end
    end
  
  if (@I_vPAPROJNUMBER <> '<NONE>')
    begin
      select @PAPostedQty = PAPostedQty,
             @PAUnpostedQty = PAUnpostedQty,
             @PAFQuantity = PAFQuantity,
             @PAPostedTotalCostN = PAPostedTotalCostN,
             @PAUnpostedTotalCostN = PAUnpostedTotalCostN,
             @PAFTotalCost = PAFTotalCost,
             @PAPosted_Accr_RevN = PAPosted_Accr_RevN,
             @PABilled_Accrued_Revenu = PABilled_Accrued_Revenu,
             @PAUnpostAccrRevN = PAUnpostAccrRevN,
             @PAFBillings = PAFBillings,
             @PAPostedBillingsN = PAPostedBillingsN
      from   PA01301 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PACOSTCATID = @I_vPACOSTCATID
             and PATU = @PATU
      
      select @l_oqty = (@PAPostedQty + @PAUnpostedQty) + @PABase_Qty
      
      if (@l_oqty < 0)
        begin
          select @O_iErrorState = 6469
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if ((@l_oqty > @PAFQuantity)
          and (@PAallow_4 = 0))
        begin
          select @O_iErrorState = 6468
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      select @l_ocost = (@PAPostedTotalCostN + @PAUnpostedTotalCostN)
      
      if (@l_ocost < 0.0)
        begin
          select @O_iErrorState = 6470
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@I_vPAbllngtype = 1)
        begin
          select @l_oaccrued = (@PAPosted_Accr_RevN + @PABilled_Accrued_Revenu + @l_accrued + @PAUnpostAccrRevN)
          
          if (@l_oaccrued < 0.0)
            begin
              select @O_iErrorState = 6471
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      if ((@PAFTotalCost < (@l_ocost + @PAtotcosts))
          and (@PAallow_5 = 0))
        begin
          select @O_iErrorState = 6472
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if ((@PAFBillings < (@l_oaccrued + @PAACREV))
          and (@PAallow_6 = 0))
        begin
          select @O_iErrorState = 6473
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      select @PATotalProfit = round(@PAPostedBillingsN - @PAtotcosts,@MCDECPLCUR)
    end
  
  select @ORUNTCST = @I_vPAUNITCOST,
         @PAORGBSUNITCST = @PABase_Unit_Cost,
         @OREXTCST = @PAEXTCOST,
         @PAORIGTOTCOSTS = @PAtotcosts,
         @PAORIGBILLRATE = @PABILRATE,
         @PAORIGOVHDAMT = @PAOverhead_Amount,
         @PAORIGBASEOVRHCST = @PABaseOvhdCost,
         @PAORIGTOTOVRHD = @PATOTALOVERH,
         @PAORIGPROFAMT = @PAProfitAmount,
         @PAORIGTOTPROF = @PATotalProfit,
         @PAORIACCRREV = @PAACREV,
         @ORTAXAMT = @I_vTAXAMNT,
         @OBTAXAMT = @I_vBCKTXAMT,
         @PAOrigReimbursableAmt = @PAReimbursableAmount,
         @PAORIGBSBILLRTE = @PA_Base_Billing_Rate
  
  if ((@I_vPAPurchase_Tax_Options < 1)
       or (@I_vPAPurchase_Tax_Options > 3))
    begin
      select @O_iErrorState = 1949
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAPaymentMethod < 1)
       or (@I_vPAPaymentMethod > 3))
    begin
      select @O_iErrorState = 1950
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAPaymentMethod = 2)
      and (@I_vPAExpenseType = 1))
    begin
      select @O_iErrorState = 6479
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAbllngtype = -1)
     and (@PAbllngtype > 0)
    select @I_vPAbllngtype = @PAbllngtype
  
  if (@I_vPAbllngtype = -1)
     and (@PAbllngtype = 0)
    select @I_vPAbllngtype = 1
  
  if ((@I_vPAbllngtype < 1)
       or (@I_vPAbllngtype > 3))
    begin
      select @O_iErrorState = 1951
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAExpenseType < 1)
       or (@I_vPAExpenseType > 2))
    begin
      select @O_iErrorState = 1952
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if ((@I_vPAExpenseType = 2)
      and (@I_vPAPROJNUMBER <> '<NONE>'))
    begin
      select @O_iErrorState = 6480
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vITMTSHID <> '')
    begin
      if not exists (select 1
                     from   TX00101 (nolock)
                     where  TAXSCHID = @I_vITMTSHID)
        begin
          select @O_iErrorState = 6478
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@add_employee_access = 1)
    begin
      insert into PA01408
                 (PAPROJNUMBER,
                  EMPLOYID,
                  PACONTNUMBER)
      select @I_vPAPROJNUMBER,
             @I_vEMPLOYID,
             @PACONTNUMBER
      
      if @@error <> 0
        begin
          select @O_iErrorState = 1953
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@oErrString <> '')
      or (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_EmployeeExpense' ,
        @I_vINDEX1 = @I_vPAerdocnumber ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 0 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2033
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  insert into PA10501
             (PAertrxtype,
              PAerdocnumber,
              PAREFNO,
              EMPLOYID,
              PADT,
              PACONTNUMBER,
              PAPROJNUMBER,
              PACOSTCATID,
              PAitemnumber,
              PAbillnoteidx,
              PAQtyQ,
              PABase_Qty,
              PAUnit_of_Measure,
              UOMSCHDL,
              PAUNITCOST,
              ORUNTCST,
              PABase_Unit_Cost,
              PAORGBSUNITCST,
              PAEXTCOST,
              OREXTCST,
              PAtotcosts,
              PAORIGTOTCOSTS,
              PABILRATE,
              PAORIGBILLRATE,
              PA_Base_Billing_Rate,
              PAORIGBSBILLRTE,
              PAMARKPERCENT,
              PAOverhead_Amount,
              PAORIGOVHDAMT,
              PABaseOvhdCost,
              PAORIGBASEOVRHCST,
              PAOverheaPercentage,
              PATOTALOVERH,
              PAORIGTOTOVRHD,
              PAProfitType,
              PAProfitAmount,
              PAORIGPROFAMT,
              PAProfitPercent,
              PATotalProfit,
              PAORIGTOTPROF,
              PAACREV,
              PAORIACCRREV,
              PASTD_Qty,
              PANC_Qty,
              PANB_Qty,
              PAPurchase_Tax_Options,
              ITMTSHID,
              TAXAMNT,
              ORTAXAMT,
              BCKTXAMT,
              OBTAXAMT,
              PAPaymentMethod,
              PAExpenseType,
              PAReimbursableAmount,
              PAOrigReimbursableAmt,
              PACGBWIPIDX,
              PAUnbilled_AR_Idx,
              PACogs_Idx,
              PAContra_Account_IDX,
              PAOverhead_IDX,
              PAUnbilled_Proj_Rev_Idx,
              LNITMSEQ,
              PAReference_Line_Seq_N,
              PAbllngtype,
              PABilling_StatusN,
              PAexptdate,
              PA_EE_Line_Errors,
              PA_EE_Line_Errors_2,
              PAApproved_Quantity,
              PAAPPROVBILLRATE,
              PAAPPROVMKUPPCT,
              PAApproved_Billing_Amou,
              PARemaining_Quantity,
              PARemaining_Total_Cost,
              PARemaining_Accrued_Rev,
              PABILLQTY,
              PA_Billed_Profit_Amount_,
              PABilledProfitPercentage,
              PABilled_Amount,
              PABilledTax,
              PAWrite_UpDown_Amount,
              PAWrite_UpDown_Percenta,
              PAApprover_ID,
              PAApprover_Type,
              PAPartial_Bill,
              PAROUNDAMT,
              RNDDIFF,
              PACHGORDNO,
              PAMCCURNCYID,
              RATETPID,
              EXGTBLID,
              XCHGRATE,
              EXCHDATE,
              TIME1,
              RATECALC,
              DENXRATE,
              MCTRXSTT,
              CURRNIDX,
              PA_MC_Accrued_Revenue,
              PA_MC_Base_Billing_Rate,
              PA_MC_Billing_Rate,
              Correcting_Trx_Type,
              PAORIGINLDOCNUM,
              PAORIGLINEIMSEQ)
  select @I_vPAertrxtype,
         @I_vPAerdocnumber,
         @I_vPAREFNO,
         @I_vEMPLOYID,
         @I_vPADT,
         @PACONTNUMBER,
         @I_vPAPROJNUMBER,
         @I_vPACOSTCATID,
         @I_vPAitemnumber,
         @PAbillnoteidx,
         @I_vPAQtyQ,
         @PABase_Qty,
         @I_vPAUnit_of_Measure,
         @I_vUOMSCHDL,
         @I_vPAUNITCOST,
         @ORUNTCST,
         @PABase_Unit_Cost,
         @PAORGBSUNITCST,
         @PAEXTCOST,
         @OREXTCST,
         @PAtotcosts,
         @PAORIGTOTCOSTS,
         @PABILRATE,
         @PAORIGBILLRATE,
         @PA_Base_Billing_Rate,
         @PAORIGBSBILLRTE,
         @PAMARKPERCENT,
         @PAOverhead_Amount,
         @PAORIGOVHDAMT,
         @PABaseOvhdCost,
         @PAORIGBASEOVRHCST,
         @PAOverheaPercentage,
         @PATOTALOVERH,
         @PAORIGTOTOVRHD,
         @PAProfitType,
         @PAProfitAmount,
         @PAORIGPROFAMT,
         @PAProfitPercent,
         @PATotalProfit,
         @PAORIGTOTPROF,
         @PAACREV,
         @PAORIACCRREV,
         @PASTD_Qty,
         @PANC_Qty,
         @PANB_Qty,
         @I_vPAPurchase_Tax_Options,
         @I_vITMTSHID,
         @I_vTAXAMNT,
         @ORTAXAMT,
         @I_vBCKTXAMT,
         @OBTAXAMT,
         @I_vPAPaymentMethod,
         @I_vPAExpenseType,
         @PAReimbursableAmount,
         @PAOrigReimbursableAmt,
         @PACGBWIPIDX,
         @PAUnbilled_AR_Idx,
         @PACogs_Idx,
         @PAContra_Account_IDX,
         @PAOverhead_IDX,
         @PAUnbilled_Proj_Rev_Idx,
         @I_vLNITMSEQ,
         @I_vPAReference_Line_Seq_N,
         @I_vPAbllngtype,
         @PABilling_StatusN,
         @PAexptdate,
         @PA_EE_Line_Errors,
         @PA_EE_Line_Errors_2,
         @PAApproved_Quantity,
         @PAAPPROVBILLRATE,
         @PAAPPROVMKUPPCT,
         @PAApproved_Billing_Amou,
         @PARemaining_Quantity,
         @PARemaining_Total_Cost,
         @PARemaining_Accrued_Rev,
         @PABILLQTY,
         @PA_Billed_Profit_Amount_,
         @PABilledProfitPercentage,
         @PABilled_Amount,
         @PABilledTax,
         @PAWrite_UpDown_Amount,
         @PAWrite_UpDown_Percenta,
         @PAApprover_ID,
         @PAApprover_Type,
         @PAPartial_Bill,
         @PAROUNDAMT,
         @RNDDIFF,
         @PACHGORDNO,
         @PAMCCURNCYID,
         @RATETPID,
         @EXGTBLID,
         @XCHGRATE,
         @EXCHDATE,
         @TIME1,
         @RATECALC,
         @DENXRATE,
         @MCTRXSTT,
         @CURRNIDX,
         @PA_MC_Accrued_Revenue,
         @PA_MC_Base_Billing_Rate,
         @PA_MC_Billing_Rate,
         0,
         '',
         0
  
  if @@error <> 0
    begin
      select @O_iErrorState = 1954
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAEmpExpenseLineInsertPost
    @I_vPAertrxtype ,
    @I_vPAerdocnumber ,
    @I_vEMPLOYID ,
    @I_vPADT ,
    @I_vPAPROJNUMBER ,
    @I_vPACOSTCATID ,
    @I_vCURNCYID ,
    @I_vLNITMSEQ ,
    @I_vPAitemnumber ,
    @I_vPAREFNO ,
    @I_vPAQtyQ ,
    @I_vPAUnit_of_Measure ,
    @I_vUOMSCHDL ,
    @I_vPAUNITCOST ,
    @I_vPAPurchase_Tax_Options ,
    @I_vITMTSHID ,
    @I_vTAXAMNT ,
    @I_vBCKTXAMT ,
    @I_vPAPaymentMethod ,
    @I_vPAExpenseType ,
    @I_vPAReference_Line_Seq_N ,
    @I_vPAbllngtype ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 1955
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @iAddCodeErrState output
      
      return (@O_iErrorState)
    end
  
  if @@error = 0
     and @O_iErrorState = 0
     and @O_oErrorState = 0
    select @O_iErrorState = 0
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Acct_EmployeeExpense' ,
        @I_vINDEX1 = @I_vPAerdocnumber ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 1 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 2034
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  return (@O_iErrorState)

