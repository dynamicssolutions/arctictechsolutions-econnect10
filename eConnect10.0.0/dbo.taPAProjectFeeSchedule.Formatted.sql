

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAProjectFeeSchedule]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAProjectFeeSchedule]
  
create procedure [dbo].[taPAProjectFeeSchedule]
                @I_vPAPROJNUMBER   char(15),
                @I_vLNITMSEQ       int  = 0,
                @I_vPAFeeID        char(15),
                @I_vPADT           datetime  = '',
                @I_vPAFeeAmount    numeric(19,5)  = 0,
                @I_vUpdateExisting tinyint  = 0,
                @I_vRequesterTrx   smallint  = 0,
                @I_vUSRDEFND1      char(50)  = '',
                @I_vUSRDEFND2      char(50)  = '',
                @I_vUSRDEFND3      char(50)  = '',
                @I_vUSRDEFND4      varchar(8000)  = '',
                @I_vUSRDEFND5      varchar(8000)  = '',
                @O_iErrorState     int  output,
                @oErrString        varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @PAAmount_Billed                          smallint,
           @userdate                                 datetime,
           @O_iglCalculateGLPeriodErrorState         int,
           @PAsfid                                   int,
           @PeriodID                                 int,
           @Closed                                   int,
           @Year                                     int,
           @iCustomGLPeriodErr                       int,
           @iStatus                                  int,
           @PAPROJNUMBER                             char(15),
           @O_oErrorState                            int,
           @iError                                   int,
           @PASTAT                                   smallint,
           @PAFeeType                                smallint,
           @PAFeeID                                  char(15),
           @PAFeeToUse                               smallint,
           @PAFrequency                              int,
           @PAFeeAmount                              numeric(19,5),
           @l_Period                                 int,
           @PAEnDate                                 datetime,
           @sum_FeeAmount                            numeric(19,5),
           @fClosed                                  smallint,
           @YEAR1                                    int,
           @nErr                                     int,
           @iCustomState                             int,
           @PAProjectType                            smallint,
           @PAbegindate                              datetime,
           @PATU                                     smallint,
           @PACONTNUMBER                             char(11),
           @CUSTNMBR                                 char(17),
           @Existing_PADT                            datetime,
           @Existing_PAFeeAmount                     numeric(19,5),
           @Existing_PAPROJNUMBER                    char(15),
           @Existing_PAAmount_Billed                 numeric(19,5),
           @Existing_l_Period                        int,
           @O_Existing_glCalculateGLPeriodErrorState int,
           @Existing_YEAR1                           int,
           @Existing_fClosed                         smallint,
           @Existing_Err                             int,
           @PA_MC_Fee_Amount                         numeric(19,5),
           @PA_MC_Amount_Billed                      numeric(19,5),
           @PAdates_to_be_updated                    smallint
  
  select @PAAmount_Billed = 0,
         @userdate = '',
         @O_iglCalculateGLPeriodErrorState = 0,
         @PAsfid = 0,
         @PeriodID = 0,
         @Closed = 0,
         @Year = 0,
         @iCustomGLPeriodErr = 0,
         @iStatus = 0,
         @PAPROJNUMBER = '',
         @O_oErrorState = 0,
         @iError = 0,
         @PASTAT = 0,
         @PAFeeType = 0,
         @PAFeeID = '',
         @PAFeeToUse = 0,
         @PAFrequency = 0,
         @PAFeeAmount = 0,
         @l_Period = 0,
         @PAEnDate = '',
         @sum_FeeAmount = 0,
         @fClosed = 0,
         @YEAR1 = 0,
         @nErr = 0,
         @iCustomState = 0,
         @PAProjectType = 0,
         @PAbegindate = '',
         @PATU = 0,
         @PACONTNUMBER = '',
         @CUSTNMBR = '',
         @O_iErrorState = 0,
         @oErrString = '',
         @Existing_PADT = '',
         @Existing_PAFeeAmount = 0,
         @Existing_PAPROJNUMBER = '',
         @Existing_PAAmount_Billed = 0,
         @Existing_l_Period = 0,
         @O_Existing_glCalculateGLPeriodErrorState = 0,
         @Existing_YEAR1 = 0,
         @Existing_fClosed = 0,
         @Existing_Err = 0,
         @PA_MC_Fee_Amount = 0,
         @PA_MC_Amount_Billed = 0,
         @PAdates_to_be_updated = 0
  
  exec @iStatus = taPAProjectFeeSchedulePre
    @I_vPAPROJNUMBER output ,
    @I_vLNITMSEQ output ,
    @I_vPAFeeID output ,
    @I_vPADT output ,
    @I_vPAFeeAmount output ,
    @I_vUpdateExisting output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @O_iErrorState = 4886
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAPROJNUMBER is null 
       or @I_vLNITMSEQ is null
       or @I_vPAFeeID is null
       or @I_vPADT is null
       or @I_vPAFeeAmount is null)
    begin
      select @O_iErrorState = 4887
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAFeeID = ''
       or @I_vPAPROJNUMBER = '')
    begin
      select @O_iErrorState = 6535
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAFeeAmount < 0)
    begin
      select @O_iErrorState = 4888
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  select @I_vPAFeeID = upper(@I_vPAFeeID),
         @I_vPAPROJNUMBER = upper(@I_vPAPROJNUMBER)
  
  if (@I_vPAPROJNUMBER <> '')
    begin
      select @PAPROJNUMBER = PAPROJNUMBER,
             @PAProjectType = PAProjectType,
             @PASTAT = PASTAT,
             @PAEnDate = PABEndDate,
             @PAbegindate = PABBeginDate,
             @PACONTNUMBER = PACONTNUMBER,
             @CUSTNMBR = CUSTNMBR
      from   PA01201 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@PAPROJNUMBER = '')
        begin
          select @O_iErrorState = 4889
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
      
      if ((@PASTAT = 3)
           or (@PASTAT = 5))
        begin
          select @O_iErrorState = 4890
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPAFeeID <> '')
    begin
      select @PAFeeID = PAFeeID,
             @PAFeeType = PAFeeType,
             @PAFeeToUse = PAFeeToUse,
             @PAFeeType = PAFeeType,
             @PAFrequency = PAFrequency,
             @PAFeeAmount = PAFeeAmount
      from   PA00401 (nolock)
      where  PAFeeID = @I_vPAFeeID
      
      if (@PAFeeID = '')
        begin
          select @O_iErrorState = 4891
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPAFeeAmount = 0)
    select @I_vPAFeeAmount = @PAFeeAmount
  
  if (@I_vLNITMSEQ = 0)
    begin
      select @I_vLNITMSEQ = isnull(max(LNITMSEQ),0) + 50
      from   PA05200 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and PAFeeID = @I_vPAFeeID
    end
  
  if (@I_vUpdateExisting = 0)
    begin
      if exists (select 1
                 from   PA05200 (nolock)
                 where  PAPROJNUMBER = @I_vPAPROJNUMBER
                        and LNITMSEQ = @I_vLNITMSEQ
                        and PAFeeID = @I_vPAFeeID)
        begin
          select @O_iErrorState = 4892
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      select @Existing_PADT = PADT,
             @Existing_PAFeeAmount = PAFeeAmount,
             @Existing_PAPROJNUMBER = PAPROJNUMBER,
             @Existing_PAAmount_Billed = PAAmount_Billed
      from   PA05200 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and LNITMSEQ = @I_vLNITMSEQ
             and PAFeeID = @I_vPAFeeID
    end
  
  if (@PAbegindate = '')
    begin
      if (@I_vPADT = '')
        begin
          select @I_vPADT = cast(getdate() as varchar(12))
        end
      
      select @PAbegindate = @I_vPADT,
             @PAEnDate = @I_vPADT,
             @PAdates_to_be_updated = 1
    end
  else
    begin
      if (@I_vPADT = '')
        begin
          select @I_vPADT = @PAbegindate
        end
      else
        begin
          if (@I_vPADT < @PAbegindate)
            begin
              select @PAbegindate = @I_vPADT,
                     @PAdates_to_be_updated = 1
            end
          else
            begin
              if (@I_vPADT > @PAEnDate)
                begin
                  select @PAEnDate = @I_vPADT,
                         @PAdates_to_be_updated = 1
                end
            end
        end
    end
  
  if (@PAdates_to_be_updated = 1)
    begin
      update PA01201
      set    PABEndDate = @PAEnDate,
             PABBeginDate = @PAbegindate
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
    end
  
  if @I_vPADT <> ''
    begin
      select @userdate = getdate()
      
      execute @iStatus = glCalculateGLPeriod
        2 ,
        'General Entry' ,
        @I_vPADT ,
        @userdate ,
        @l_Period output ,
        @fClosed output ,
        @YEAR1 output ,
        @nErr output ,
        @O_iglCalculateGLPeriodErrorState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@O_iglCalculateGLPeriodErrorState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@O_iglCalculateGLPeriodErrorState))
          
          select @O_iErrorState = 4893
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPAFeeAmount < @Existing_PAAmount_Billed)
    begin
      select @O_iErrorState = 6430
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@Existing_PAAmount_Billed > 0)
     and (@I_vPADT <> @Existing_PADT)
    begin
      select @O_iErrorState = 6431
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@Existing_PADT <> '')
    begin
      select @userdate = getdate()
      
      execute @iStatus = glCalculateGLPeriod
        2 ,
        'General Entry' ,
        @Existing_PADT ,
        @userdate ,
        @Existing_l_Period output ,
        @Existing_fClosed output ,
        @Existing_YEAR1 output ,
        @Existing_Err output ,
        @O_Existing_glCalculateGLPeriodErrorState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@O_Existing_glCalculateGLPeriodErrorState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@O_Existing_glCalculateGLPeriodErrorState))
          
          select @O_iErrorState = 6432
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPAFeeAmount < 0)
    begin
      if (@PAFeeType = 1)
        begin
          if (@PAProjectType = 1)
            begin
              select @O_iErrorState = 4895
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
            end
        end
      else
        begin
          select @O_iErrorState = 4896
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  if (@Existing_PAPROJNUMBER = '')
    begin
      insert PA05200
            (PAPROJNUMBER,
             LNITMSEQ,
             PAFeeID,
             PADT,
             PAFeeAmount,
             PA_MC_Fee_Amount,
             PA_MC_Amount_Billed)
      select @I_vPAPROJNUMBER,
             @I_vLNITMSEQ,
             @I_vPAFeeID,
             @I_vPADT,
             @I_vPAFeeAmount,
             @PA_MC_Fee_Amount,
             @PA_MC_Amount_Billed
      
      if @@error <> 0
        begin
          select @O_iErrorState = 4897
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      update PA05200
      set    PADT = @I_vPADT,
             PAFeeAmount = @I_vPAFeeAmount
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
             and LNITMSEQ = @I_vLNITMSEQ
             and PAFeeID = @I_vPAFeeID
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6433
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  exec @iStatus = taPAProjectFeeSchedulePost
    @I_vPAPROJNUMBER ,
    @I_vLNITMSEQ ,
    @I_vPAFeeID ,
    @I_vPADT ,
    @I_vPAFeeAmount ,
    @I_vUpdateExisting ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @oErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @O_iErrorState = 4898
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

