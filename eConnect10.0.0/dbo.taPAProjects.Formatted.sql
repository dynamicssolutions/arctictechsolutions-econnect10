

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAProjects]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAProjects]
  
create procedure [dbo].[taPAProjects]
                @I_vCUSTNMBR                char(15),
                @I_vPACONTNUMBER            char(11),
                @I_vPAprojid                char(15),
                @I_vPAprojname              char(31),
                @I_vPAPROJNUMBER            char(15),
                @I_vPAProjectType           smallint  = null,
                @I_vPAAcctgMethod           smallint  = null,
                @I_vPASTAT                  smallint  = null,
                @I_vPABBeginDate            datetime  = null,
                @I_vPABEndDate              datetime  = null,
                @I_vPAprjclsid              char(15)  = null,
                @I_vPApurordnum             char(17)  = null,
                @I_vPAcloseProjcosts        smallint  = null,
                @I_vPAclosetobillings       smallint  = null,
                @I_vPADepartment            char(7)  = null,
                @I_vPAEstimatorID           char(15)  = null,
                @I_vPAprojmngrid            char(15)  = null,
                @I_vPABusMgrID              char(15)  = null,
                @I_vPALabor_Rate_Table_ID   char(31)  = null,
                @I_vPALabor_RateTable_Type  smallint  = null,
                @I_vPALabor_Rate_Table_Acc  smallint  = null,
                @I_vPAEquip_Rate_Table_ID   char(31)  = null,
                @I_vPAEquip_Rate_Table_Acc  smallint  = null,
                @I_vPAbllngtype             smallint  = null,
                @I_vPABILLFORMAT            char(15)  = null,
                @I_vDSCPCTAM                smallint  = null,
                @I_vPA_Retention_Percent    numeric(19,5)  = null,
                @I_vPAUD1Proj               char(51)  = null,
                @I_vPAUD2_Proj              char(51)  = null,
                @I_vPASegmentCB             smallint  = null,
                @I_vPRBTADCD                char(15)  = null,
                @I_vPAbillnoteDefault       char(1000)  = '',
                @I_vPAbillnoteTS            char(1000)  = '',
                @I_vPAbillnoteEL            char(1000)  = '',
                @I_vPAbillnoteML            char(1000)  = '',
                @I_vPAbillnoteVI            char(1000)  = '',
                @I_vPAbillnoteEE            char(1000)  = '',
                @I_vPAbillnoteINV           char(1000)  = '',
                @I_vPAbillnoteFEE           char(1000)  = '',
                @I_vPABILLCYCLEID1          char(15)  = null,
                @I_vPA_RestrictCustomerList smallint  = null,
                @I_vWRKRCOMP                char(7)  = null,
                @I_vSUTASTAT                char(3)  = null,
                @I_vPATRKCHGORDS            smallint  = null,
                @I_vPATRKCOBDGADDFLY        smallint  = null,
                @I_vUpdateExisting          tinyint  = 0,
                @I_vRequesterTrx            smallint  = 0,
                @I_vUSRDEFND1               char(50)  = '',
                @I_vUSRDEFND2               char(50)  = '',
                @I_vUSRDEFND3               char(50)  = '',
                @I_vUSRDEFND4               varchar(8000)  = '',
                @I_vUSRDEFND5               varchar(8000)  = '',
                @O_iErrorState              int  output,
                @oErrString                 varchar(255)  output     /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iStatus                          int,
           @iCustomState                     int,
           @iCustomErrString                 varchar(255),
           @iError                           int,
           @iCursorError                     int,
           @O_oErrorState                    int,
           @PAcontid                         char(15),
           @PABQuantity                      numeric(19,5),
           @PABTotalCost                     numeric(19,5),
           @PABProfit                        numeric(19,5),
           @PABBillings                      numeric(19,5),
           @PABTaxPaidAmt                    numeric(19,5),
           @PABTaxChargedAmt                 numeric(19,5),
           @PABaselineOvhdCost               numeric(19,5),
           @PAACTUALBEGDATE                  datetime,
           @PA_Actual_End_Date               datetime,
           @PAFBeginDate                     datetime,
           @PAFEndDate                       datetime,
           @PAFQuantity                      numeric(19,5),
           @PAFTotalCost                     numeric(19,5),
           @PAFProfit                        numeric(19,5),
           @PAFBillings                      numeric(19,5),
           @PAFTaxPaidAmt                    numeric(19,5),
           @PAFTaxChargedAmt                 numeric(19,5),
           @PAForecastOvhdCost               numeric(19,5),
           @PAtotcbts                        smallint,
           @PAtotcbEL                        smallint,
           @PAtotcbML                        smallint,
           @PAtotcbvi                        smallint,
           @PAtotcber                        smallint,
           @PAtotcbinv                       smallint,
           @PAtotcbfee                       smallint,
           @PAbillnoteidx                    int,
           @PAbillnoteidxts                  int,
           @PAELbillniteidx                  int,
           @PAbillnoteidxML                  int,
           @PAbillnoteidxvi                  int,
           @PAbillnoteidxee                  int,
           @PAbillnoteidxinv                 int,
           @PAbillnoteidxfee                 int,
           @PAUnpostedQty                    numeric(19,5),
           @PAUnpostedTotalCostN             numeric(19,5),
           @PAUnposted_Overhead              numeric(19,5),
           @PAUnpostedProfitN                numeric(19,5),
           @PAUnposted_Tax_Amount            numeric(19,5),
           @PAUnpostAccrRevN                 numeric(19,5),
           @PAUnpostedRecogRevN              numeric(19,5),
           @PAUnpostedCommitedQty            numeric(19,5),
           @PAUnpostedCommitedCost           numeric(19,5),
           @PAUnpostedCommitedTaxAmt         numeric(19,5),
           @PAUnposted_Project_Fee           numeric(19,5),
           @PAUnposted_Retainer_Fee          numeric(19,5),
           @PAUnposted_Service_Fee           numeric(19,5),
           @PAUNPOSTRETAMT                   numeric(19,5),
           @PAUNPOSTBIEEAMOUNT               numeric(19,5),
           @PAUNPEIEBAMOUNT                  numeric(19,5),
           @PA_Unposted_Billed_Reten         numeric(19,5),
           @PAPostedQty                      numeric(19,5),
           @PAPostedTotalCostN               numeric(19,5),
           @PAPostedProfitN                  numeric(19,5),
           @PAPosted_Tax_Amount              numeric(19,5),
           @PAPosted_Accr_RevN               numeric(19,5),
           @PAPostRecogRevN                  numeric(19,5),
           @PAPostedCommitedQty              numeric(19,5),
           @PAPostedCommitedCost             numeric(19,5),
           @PAPostedCommitedTaxAmt           numeric(19,5),
           @PAPosted_Project_Fee             numeric(19,5),
           @PAPosted_Retainer_Fee            numeric(19,5),
           @PAPosted_Service_Fee             numeric(19,5),
           @PAPOSTRETAMT                     numeric(19,5),
           @PAPOSBIEEAMOUNT                  numeric(19,5),
           @PAPOSEIEBAMOUNT                  numeric(19,5),
           @PA_Actual_Billed_Retenti         numeric(19,5),
           @PAWrite_UpDown_Amount            numeric(19,5),
           @PABilled_QtyN                    numeric(19,5),
           @PABilled_Cost                    numeric(19,5),
           @PABilled_Accrued_Revenu          numeric(19,5),
           @PACostPcntCompleted              numeric(19,5),
           @PAQuantityPcntCompleted          numeric(19,5),
           @PA_Receipts_Amount               numeric(19,5),
           @PA_Actual_Receipts_Amoun         numeric(19,5),
           @PA_Earnings                      numeric(19,5),
           @PA_Cost_of_Earnings              numeric(19,5),
           @PAUnpostBillN                    numeric(19,5),
           @PAUnpostDiscDolAmtN              numeric(19,5),
           @PAUnposted_Sales_Tax_Am          numeric(19,5),
           @PAPostedBillingsN                numeric(19,5),
           @PAPostedDiscDolAmtN              numeric(19,5),
           @PAPosted_Sales_Tax_Amou          numeric(19,5),
           @PABCWPAMT                        numeric(19,5),
           @PABCWSAMT                        numeric(19,5),
           @PAACWPAMT                        numeric(19,5),
           @NOTEINDX                         numeric(19,5),
           @PAPosted_Overhead                numeric(19,5),
           @PAApproved_Accrued_Reve          numeric(19,5),
           @PAApproved_Cost                  numeric(19,5),
           @PAApproved_Quantity              numeric(19,5),
           @WROFAMNT                         numeric(19,5),
           @ActualWriteOffAmount             numeric(19,5),
           @DISTKNAM                         numeric(19,5),
           @ActualDiscTakenAmount            numeric(19,5),
           @PACommitted_Costs                numeric(19,5),
           @PACommitted_Qty                  numeric(19,5),
           @PAPOCost                         numeric(19,5),
           @PAPOQty                          numeric(19,5),
           @PAPOPostedCost                   numeric(19,5),
           @PAPOPostedQty                    numeric(19,5),
           @PAtaxpaidamt                     numeric(19,5),
           @PAPostedTaxPaidN                 numeric(19,5),
           @PApretainage                     numeric(19,5),
           @PAunpretainage                   numeric(19,5),
           @PA_Write_Off_Tax_Amount          numeric(19,5),
           @PAActualWOTaxAmt                 numeric(19,5),
           @PA_Terms_Taken_Tax_Amt           numeric(19,5),
           @PAActualTermsTakenTax            numeric(19,5),
           @PA_Actual_Billing_not_RM         numeric(19,5),
           @PA_ActualDiscnotRM               numeric(19,5),
           @PAPosted_Earnings                numeric(19,5),
           @PAActualCostofEarnings           numeric(19,5),
           @PAUnpostedLossAmount             numeric(19,5),
           @PAActualLossAmount               numeric(19,5),
           @PAFQLaborOnly                    numeric(19,5),
           @PAAQLaborOnly                    numeric(19,5),
           @LOCATNID                         char(15),
           @SLPRSNID                         char(15),
           @SALSTERR                         char(15),
           @COMPRCNT                         smallint,
           @COMAPPTO                         smallint,
           @CNTCPRSN                         char(31),
           @DBName                           char(50),
           @O_iInitErrorState                int,
           @oInitErrString                   varchar(255),
           @PAService_Fee_Amount             numeric(19,5),
           @PAProject_Fee_Amount             numeric(19,5),
           @PARetainer_Fee_Amount            numeric(19,5),
           @PARetentionFeeAmount             numeric(19,5),
           @PAProject_Amount                 numeric(19,5),
           @ACCTAMNT                         numeric(19,5),
           @PACONTNUMBER                     char(17),
           @PAContMgrID                      char(15),
           @PABusMgrID                       char(15),
           @PABILLFORMAT                     char(15),
           @PRBTADCD                         char(15),
           @PAUD1_Cont                       char(51),
           @PAUD2_Cont                       char(51),
           @PAProjectType                    smallint,
           @PAAcctgMethod                    smallint,
           @PA_RestrictCustomerList          smallint,
           @sCompanyID                       char(10),
           @iGetNextNoteIdxErrState          int,
           @Cont_Status                      smallint,
           @Existing_CUSTNMBR                char(15),
           @Existing_PAPROJNUMBER            char(15),
           @Existing_PAprojid                char(15),
           @Existing_PACONTNUMBER            char(15),
           @Existing_PASTAT                  smallint,
           @Existing_PAprojname              char(31),
           @Existing_PAprjclsid              char(15),
           @Existing_PAbillnoteidx           int,
           @Existing_PAbillnoteidxts         int,
           @Existing_PAELbillniteidx         int,
           @Existing_PAbillnoteidxML         int,
           @Existing_PAbillnoteidxvi         int,
           @Existing_PAbillnoteidxee         int,
           @Existing_PAbillnoteidxinv        int,
           @Existing_PAbillnoteidxfee        int,
           @userdate                         datetime,
           @l_Period                         int,
           @fClosed                          smallint,
           @YEAR1                            smallint,
           @nErr                             int,
           @iCalculateGLPeriodErrState       int,
           @Existing_PAProjectType           smallint,
           @Existing_PAAcctgMethod           smallint,
           @Existing_PABBeginDate            datetime,
           @Existing_PABEndDate              datetime,
           @Existing_PApurordnum             char(17),
           @Existing_PAcloseProjcosts        smallint,
           @Existing_PAclosetobillings       smallint,
           @Existing_PADepartment            char(7),
           @Existing_PAEstimatorID           char(15),
           @Existing_PAprojmngrid            char(15),
           @Existing_PABusMgrID              char(15),
           @Existing_PALabor_Rate_Table_ID   char(31),
           @Existing_PALabor_RateTable_Type  smallint,
           @Existing_PALabor_Rate_Table_Acc  smallint,
           @Existing_PAEquip_Rate_Table_ID   char(31),
           @Existing_PAEquip_Rate_Table_Acc  smallint,
           @Existing_PAbllngtype             smallint,
           @Existing_PABILLFORMAT            char(15),
           @Existing_DSCPCTAM                smallint,
           @Existing_PA_Retention_Percent    numeric(19,5),
           @Existing_PAUD1Proj               char(51),
           @Existing_PAUD2_Proj              char(51),
           @Existing_PASegmentCB             smallint,
           @Existing_PRBTADCD                char(15),
           @Existing_PABILLCYCLEID1          char(15),
           @Existing_PA_RestrictCustomerList smallint,
           @Existing_WRKRCOMP                char(7),
           @Existing_SUTASTAT                char(3),
           @Existing_PATRKCHGORDS            smallint,
           @Existing_PATRKCOBDGADDFLY        smallint,
           @PAMCProjectAmount                numeric(19,5),
           @PAMCUnpostBillN                  numeric(19,5),
           @PAMCUnpostedSalesTax             numeric(19,5),
           @PAMCUnpostedDiscount             numeric(19,5),
           @PAMCActualBillings               numeric(19,5),
           @PAMCActualSalesTaxAmt            numeric(19,5),
           @PAMCActualDiscountAmt            numeric(19,5),
           @PAMCBBillings                    numeric(19,5),
           @PAMCFBillings                    numeric(19,5),
           @PA_MC_Project_Fee_Amount         numeric(19,5),
           @PA_MC_Retainer_Fee_Amt           numeric(19,5),
           @PA_MC_Service_Fee_Amount         numeric(19,5),
           @PA_MC_UnpostedProjectFee         numeric(19,5),
           @PA_MC_Unposted_Retainer          numeric(19,5),
           @PA_MC_UnpostedServiceFee         numeric(19,5),
           @PA_MC_Actual_Project_Fee         numeric(19,5),
           @PA_MC_Actual_RetainerFee         numeric(19,5),
           @PA_MC_Actual_Service_Fee         numeric(19,5),
           @CURRNIDX                         int,
           @PAMCCURNCYID                     char(15)
  
  select @iStatus = 0,
         @iCustomState = 0,
         @iCustomErrString = '',
         @iError = 0,
         @iCursorError = 0,
         @O_oErrorState = 0,
         @PAcontid = '',
         @PABQuantity = 0,
         @PABTotalCost = 0,
         @PABProfit = 0,
         @PABBillings = 0,
         @PABTaxPaidAmt = 0,
         @PABTaxChargedAmt = 0,
         @PABaselineOvhdCost = 0,
         @PAACTUALBEGDATE = '',
         @PA_Actual_End_Date = '',
         @PAFBeginDate = '',
         @PAFEndDate = '',
         @PAFQuantity = 0,
         @PAFTotalCost = 0,
         @PAFProfit = 0,
         @PAFBillings = 0,
         @PAFTaxPaidAmt = 0,
         @PAFTaxChargedAmt = 0,
         @PAForecastOvhdCost = 0,
         @PAtotcbts = 0,
         @PAtotcbEL = 0,
         @PAtotcbML = 0,
         @PAtotcbvi = 0,
         @PAtotcber = 0,
         @PAtotcbinv = 0,
         @PAtotcbfee = 0,
         @PAbillnoteidx = 0,
         @PAbillnoteidxts = 0,
         @PAELbillniteidx = 0,
         @PAbillnoteidxML = 0,
         @PAbillnoteidxvi = 0,
         @PAbillnoteidxee = 0,
         @PAbillnoteidxinv = 0,
         @PAbillnoteidxfee = 0,
         @PAUnpostedQty = 0,
         @PAUnpostedTotalCostN = 0,
         @PAUnposted_Overhead = 0,
         @PAUnpostedProfitN = 0,
         @PAUnposted_Tax_Amount = 0,
         @PAUnpostAccrRevN = 0,
         @PAUnpostedRecogRevN = 0,
         @PAUnpostedCommitedQty = 0,
         @PAUnpostedCommitedCost = 0,
         @PAUnpostedCommitedTaxAmt = 0,
         @PAUnposted_Project_Fee = 0,
         @PAUnposted_Retainer_Fee = 0,
         @PAUnposted_Service_Fee = 0,
         @PAUNPOSTRETAMT = 0,
         @PAUNPOSTBIEEAMOUNT = 0,
         @PAUNPEIEBAMOUNT = 0,
         @PA_Unposted_Billed_Reten = 0,
         @PAPostedQty = 0,
         @PAPostedTotalCostN = 0,
         @PAPostedProfitN = 0,
         @PAPosted_Tax_Amount = 0,
         @PAPosted_Accr_RevN = 0,
         @PAPostRecogRevN = 0,
         @PAPostedCommitedQty = 0,
         @PAPostedCommitedCost = 0,
         @PAPostedCommitedTaxAmt = 0,
         @PAPosted_Project_Fee = 0,
         @PAPosted_Retainer_Fee = 0,
         @PAPosted_Service_Fee = 0,
         @PAPOSTRETAMT = 0,
         @PAPOSBIEEAMOUNT = 0,
         @PAPOSEIEBAMOUNT = 0,
         @PA_Actual_Billed_Retenti = 0,
         @PAWrite_UpDown_Amount = 0,
         @PABilled_QtyN = 0,
         @PABilled_Cost = 0,
         @PABilled_Accrued_Revenu = 0,
         @PACostPcntCompleted = 0,
         @PAQuantityPcntCompleted = 0,
         @PA_Receipts_Amount = 0,
         @PA_Actual_Receipts_Amoun = 0,
         @PA_Earnings = 0,
         @PA_Cost_of_Earnings = 0,
         @PAUnpostBillN = 0,
         @PAUnpostDiscDolAmtN = 0,
         @PAUnposted_Sales_Tax_Am = 0,
         @PAPostedBillingsN = 0,
         @PAPostedDiscDolAmtN = 0,
         @PAPosted_Sales_Tax_Amou = 0,
         @PABCWPAMT = 0,
         @PABCWSAMT = 0,
         @PAACWPAMT = 0,
         @NOTEINDX = 0,
         @PAPosted_Overhead = 0,
         @PAApproved_Accrued_Reve = 0,
         @PAApproved_Cost = 0,
         @PAApproved_Quantity = 0,
         @WROFAMNT = 0,
         @ActualWriteOffAmount = 0,
         @DISTKNAM = 0,
         @ActualDiscTakenAmount = 0,
         @PACommitted_Costs = 0,
         @PACommitted_Qty = 0,
         @PAPOCost = 0,
         @PAPOQty = 0,
         @PAPOPostedCost = 0,
         @PAPOPostedQty = 0,
         @PAtaxpaidamt = 0,
         @PAPostedTaxPaidN = 0,
         @PApretainage = 0,
         @PAunpretainage = 0,
         @PA_Write_Off_Tax_Amount = 0,
         @PAActualWOTaxAmt = 0,
         @PA_Terms_Taken_Tax_Amt = 0,
         @PAActualTermsTakenTax = 0,
         @PA_Actual_Billing_not_RM = 0,
         @PA_ActualDiscnotRM = 0,
         @PAPosted_Earnings = 0,
         @PAActualCostofEarnings = 0,
         @PAUnpostedLossAmount = 0,
         @PAActualLossAmount = 0,
         @PAFQLaborOnly = 0,
         @PAAQLaborOnly = 0,
         @LOCATNID = '',
         @SLPRSNID = '',
         @SALSTERR = '',
         @COMPRCNT = 0,
         @COMAPPTO = 0,
         @CNTCPRSN = '',
         @DBName = '',
         @O_iInitErrorState = 0,
         @oInitErrString = '',
         @PAService_Fee_Amount = 0,
         @PAProject_Fee_Amount = 0,
         @PARetainer_Fee_Amount = 0,
         @PARetentionFeeAmount = 0,
         @PAProject_Amount = 0,
         @ACCTAMNT = 0,
         @PACONTNUMBER = '',
         @PAContMgrID = '',
         @PABusMgrID = '',
         @PABILLFORMAT = '',
         @PRBTADCD = '',
         @PAUD1_Cont = '',
         @PAUD2_Cont = '',
         @PAProjectType = '',
         @PAAcctgMethod = '',
         @PA_RestrictCustomerList = 0,
         @sCompanyID = '',
         @iGetNextNoteIdxErrState = 0,
         @Cont_Status = 0,
         @Existing_CUSTNMBR = '',
         @Existing_PAPROJNUMBER = '',
         @Existing_PAprojid = '',
         @Existing_PACONTNUMBER = '',
         @Existing_PASTAT = 0,
         @Existing_PAprojname = '',
         @Existing_PAprjclsid = '',
         @Existing_PAbillnoteidx = 0,
         @Existing_PAbillnoteidxts = 0,
         @Existing_PAELbillniteidx = 0,
         @Existing_PAbillnoteidxML = 0,
         @Existing_PAbillnoteidxvi = 0,
         @Existing_PAbillnoteidxee = 0,
         @Existing_PAbillnoteidxinv = 0,
         @Existing_PAbillnoteidxfee = 0,
         @userdate = '',
         @l_Period = 0,
         @fClosed = 0,
         @YEAR1 = 0,
         @nErr = 0,
         @iCalculateGLPeriodErrState = 0,
         @O_iErrorState = 0,
         @Existing_PAProjectType = 0,
         @Existing_PAAcctgMethod = 0,
         @Existing_PABBeginDate = '',
         @Existing_PABEndDate = '',
         @Existing_PApurordnum = '',
         @Existing_PAcloseProjcosts = 0,
         @Existing_PAclosetobillings = 0,
         @Existing_PADepartment = '',
         @Existing_PAEstimatorID = '',
         @Existing_PAprojmngrid = '',
         @Existing_PABusMgrID = '',
         @Existing_PALabor_Rate_Table_ID = '',
         @Existing_PALabor_RateTable_Type = 0,
         @Existing_PALabor_Rate_Table_Acc = 0,
         @Existing_PAEquip_Rate_Table_ID = '',
         @Existing_PAEquip_Rate_Table_Acc = 0,
         @Existing_PAbllngtype = 0,
         @Existing_PABILLFORMAT = '',
         @Existing_DSCPCTAM = 0,
         @Existing_PA_Retention_Percent = 0,
         @Existing_PAUD1Proj = '',
         @Existing_PAUD2_Proj = '',
         @Existing_PASegmentCB = 0,
         @Existing_PRBTADCD = '',
         @Existing_PABILLCYCLEID1 = '',
         @Existing_PA_RestrictCustomerList = 0,
         @Existing_WRKRCOMP = '',
         @Existing_SUTASTAT = '',
         @Existing_PATRKCHGORDS = 0,
         @Existing_PATRKCOBDGADDFLY = 0,
         @PAMCCURNCYID = '',
         @PAMCProjectAmount = 0,
         @PAMCUnpostBillN = 0,
         @PAMCUnpostedSalesTax = 0,
         @PAMCUnpostedDiscount = 0,
         @PAMCActualBillings = 0,
         @PAMCActualSalesTaxAmt = 0,
         @PAMCActualDiscountAmt = 0,
         @PAMCBBillings = 0,
         @PAMCFBillings = 0,
         @PA_MC_Project_Fee_Amount = 0,
         @PA_MC_Retainer_Fee_Amt = 0,
         @PA_MC_Service_Fee_Amount = 0,
         @PA_MC_UnpostedProjectFee = 0,
         @PA_MC_Unposted_Retainer = 0,
         @PA_MC_UnpostedServiceFee = 0,
         @PA_MC_Actual_Project_Fee = 0,
         @PA_MC_Actual_RetainerFee = 0,
         @PA_MC_Actual_Service_Fee = 0,
         @CURRNIDX = 0
  
  if (@oErrString is NULL)
    begin
      select @oErrString = ''
    end
  
  select @DBName = DB_Name()
  
  exec @iStatus = taPAProjectsPre
    @I_vCUSTNMBR output ,
    @I_vPACONTNUMBER output ,
    @I_vPAprojid output ,
    @I_vPAprojname output ,
    @I_vPAPROJNUMBER output ,
    @I_vPAProjectType output ,
    @I_vPAAcctgMethod output ,
    @I_vPASTAT output ,
    @I_vPABBeginDate output ,
    @I_vPABEndDate output ,
    @I_vPAprjclsid output ,
    @I_vPApurordnum output ,
    @I_vPAcloseProjcosts output ,
    @I_vPAclosetobillings output ,
    @I_vPADepartment output ,
    @I_vPAEstimatorID output ,
    @I_vPAprojmngrid output ,
    @I_vPABusMgrID output ,
    @I_vPRBTADCD output ,
    @I_vPALabor_Rate_Table_ID output ,
    @I_vPALabor_RateTable_Type output ,
    @I_vPALabor_Rate_Table_Acc output ,
    @I_vPAEquip_Rate_Table_ID output ,
    @I_vPAEquip_Rate_Table_Acc output ,
    @I_vPAbllngtype output ,
    @I_vPABILLFORMAT output ,
    @I_vDSCPCTAM output ,
    @I_vPA_Retention_Percent output ,
    @I_vPAUD1Proj output ,
    @I_vPAUD2_Proj output ,
    @I_vPASegmentCB output ,
    @I_vPAbillnoteDefault output ,
    @I_vPAbillnoteTS output ,
    @I_vPAbillnoteEL output ,
    @I_vPAbillnoteML output ,
    @I_vPAbillnoteVI output ,
    @I_vPAbillnoteEE output ,
    @I_vPAbillnoteINV output ,
    @I_vPAbillnoteFEE output ,
    @I_vPABILLCYCLEID1 output ,
    @I_vPA_RestrictCustomerList output ,
    @I_vWRKRCOMP output ,
    @I_vSUTASTAT output ,
    @I_vPATRKCHGORDS output ,
    @I_vPATRKCOBDGADDFLY output ,
    @I_vUpdateExisting output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      if @iCustomState <> 0
        select @oErrString = rtrim(@oErrString) + ' ' + @iCustomState
      
      select @O_iErrorState = 6142
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vCUSTNMBR is NULL 
       or @I_vPACONTNUMBER is NULL
       or @I_vPAprojid is NULL
       or @I_vPAprojname is NULL
       or @I_vPAPROJNUMBER is NULL)
    begin
      select @O_iErrorState = 6143
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vCUSTNMBR = ''
       or @I_vPACONTNUMBER = ''
       or @I_vPAprojid = ''
       or @I_vPAprojname = ''
       or @I_vPAPROJNUMBER = '')
    begin
      select @O_iErrorState = 6144
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAProjectType < 0
       or @I_vPAAcctgMethod < 0
       or @I_vPASTAT < 0)
    begin
      select @O_iErrorState = 6145
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  select @I_vCUSTNMBR = UPPER(@I_vCUSTNMBR),
         @I_vPAprojid = UPPER(@I_vPAprojid),
         @I_vPACONTNUMBER = UPPER(@I_vPACONTNUMBER),
         @I_vPAPROJNUMBER = UPPER(@I_vPAPROJNUMBER),
         @I_vPRBTADCD = UPPER(@I_vPRBTADCD),
         @I_vPAprjclsid = UPPER(@I_vPAprjclsid),
         @I_vPADepartment = UPPER(@I_vPADepartment),
         @I_vPAEstimatorID = UPPER(@I_vPAEstimatorID),
         @I_vPAprojmngrid = UPPER(@I_vPAprojmngrid),
         @I_vPABusMgrID = UPPER(@I_vPABusMgrID),
         @I_vPALabor_Rate_Table_ID = UPPER(@I_vPALabor_Rate_Table_ID),
         @I_vPAEquip_Rate_Table_ID = UPPER(@I_vPAEquip_Rate_Table_ID),
         @I_vPABILLFORMAT = UPPER(@I_vPABILLFORMAT),
         @I_vPRBTADCD = UPPER(@I_vPRBTADCD),
         @I_vPABILLCYCLEID1 = UPPER(@I_vPABILLCYCLEID1),
         @I_vWRKRCOMP = UPPER(@I_vWRKRCOMP),
         @I_vSUTASTAT = UPPER(@I_vSUTASTAT)
  
  if (@I_vUpdateExisting = 1)
    begin
      select @Existing_CUSTNMBR = CUSTNMBR,
             @Existing_PAprojid = PAprojid,
             @Existing_PAprojname = PAprojname,
             @Existing_PAPROJNUMBER = PAPROJNUMBER,
             @Existing_PAprjclsid = PAprjclsid,
             @Existing_PASTAT = PASTAT,
             @Existing_PAbillnoteidx = PAbillnoteidx,
             @Existing_PAbillnoteidxts = PAbillnoteidxts,
             @Existing_PAELbillniteidx = PAELbillniteidx,
             @Existing_PAbillnoteidxML = PAbillnoteidxML,
             @Existing_PAbillnoteidxvi = PAbillnoteidxvi,
             @Existing_PAbillnoteidxee = PAbillnoteidxee,
             @Existing_PAbillnoteidxinv = PAbillnoteidxinv,
             @Existing_PAbillnoteidxfee = PAbillnoteidxfee,
             @Existing_PAProjectType = PAProjectType,
             @Existing_PAAcctgMethod = PAAcctgMethod,
             @Existing_PABBeginDate = PABBeginDate,
             @Existing_PABEndDate = PABEndDate,
             @Existing_PApurordnum = PApurordnum,
             @Existing_PAcloseProjcosts = PAcloseProjcosts,
             @Existing_PAclosetobillings = PAclosetobillings,
             @Existing_PADepartment = PADepartment,
             @Existing_PAEstimatorID = PAEstimatorID,
             @Existing_PAprojmngrid = PAprojmngrid,
             @Existing_PABusMgrID = PABusMgrID,
             @Existing_PALabor_Rate_Table_ID = PALabor_Rate_Table_ID,
             @Existing_PALabor_RateTable_Type = PALabor_RateTable_Type,
             @Existing_PALabor_Rate_Table_Acc = PALabor_Rate_Table_Acc,
             @Existing_PAEquip_Rate_Table_ID = PAEquip_Rate_Table_ID,
             @Existing_PAEquip_Rate_Table_Acc = PAEquip_Rate_Table_Acc,
             @Existing_PAbllngtype = PAbllngtype,
             @Existing_PABILLFORMAT = PABILLFORMAT,
             @Existing_DSCPCTAM = DSCPCTAM,
             @Existing_PA_Retention_Percent = PA_Retention_Percent,
             @Existing_PAUD1Proj = PAUD1Proj,
             @Existing_PAUD2_Proj = PAUD2_Proj,
             @Existing_PASegmentCB = PASegmentCB,
             @Existing_PRBTADCD = PRBTADCD,
             @Existing_PA_RestrictCustomerList = PA_RestrictCustomerList,
             @Existing_WRKRCOMP = WRKRCOMP,
             @Existing_SUTASTAT = SUTASTAT
      from   PA01201 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      select @Existing_PATRKCHGORDS = PATRKCHGORDS,
             @Existing_PATRKCOBDGADDFLY = PATRKCOBDGADDFLY
      from   PA01241 (nolock)
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      select @I_vPAprjclsid = case 
                                when (@I_vPAprjclsid is null)
                                     and (@Existing_PAPROJNUMBER <> '') then @Existing_PAprjclsid
                                when (@I_vPAprjclsid is null)
                                     and (@Existing_PAPROJNUMBER = '') then ''
                                else @I_vPAprjclsid
                              end,
             @I_vPASTAT = case 
                            when (@I_vPASTAT is null)
                                 and (@Existing_PAPROJNUMBER <> '') then @Existing_PASTAT
                            when (@I_vPASTAT is null)
                                 and (@Existing_PAPROJNUMBER = '') then 4
                            else @I_vPASTAT
                          end,
             @I_vPAProjectType = case 
                                   when (@I_vPAProjectType is null)
                                        and (@Existing_PAPROJNUMBER <> '') then @Existing_PAProjectType
                                   when (@I_vPAProjectType is null)
                                        and (@Existing_PAPROJNUMBER = '') then 0
                                   else @I_vPAProjectType
                                 end,
             @I_vPAAcctgMethod = case 
                                   when (@I_vPAAcctgMethod is null)
                                        and (@Existing_PAPROJNUMBER <> '') then @Existing_PAAcctgMethod
                                   when (@I_vPAAcctgMethod is null)
                                        and (@Existing_PAPROJNUMBER = '') then 0
                                   else @I_vPAAcctgMethod
                                 end,
             @I_vPABBeginDate = case 
                                  when (@I_vPABBeginDate is null)
                                       and (@Existing_PAPROJNUMBER <> '') then @Existing_PABBeginDate
                                  when (@I_vPABBeginDate is null)
                                       and (@Existing_PAPROJNUMBER = '') then ''
                                  else @I_vPABBeginDate
                                end,
             @I_vPABEndDate = case 
                                when (@I_vPABEndDate is null)
                                     and (@Existing_PAPROJNUMBER <> '') then @Existing_PABEndDate
                                when (@I_vPABEndDate is null)
                                     and (@Existing_PAPROJNUMBER = '') then ''
                                else @I_vPABEndDate
                              end,
             @I_vPApurordnum = case 
                                 when (@I_vPApurordnum is null)
                                      and (@Existing_PAPROJNUMBER <> '') then @Existing_PApurordnum
                                 when (@I_vPApurordnum is null)
                                      and (@Existing_PAPROJNUMBER = '') then ''
                                 else @I_vPApurordnum
                               end,
             @I_vPAcloseProjcosts = case 
                                      when (@I_vPAcloseProjcosts is null)
                                           and (@Existing_PAPROJNUMBER <> '') then @Existing_PAcloseProjcosts
                                      when (@I_vPAcloseProjcosts is null)
                                           and (@Existing_PAPROJNUMBER = '') then 0
                                      else @I_vPAcloseProjcosts
                                    end,
             @I_vPAclosetobillings = case 
                                       when (@I_vPAclosetobillings is null)
                                            and (@Existing_PAPROJNUMBER <> '') then @Existing_PAclosetobillings
                                       when (@I_vPAclosetobillings is null)
                                            and (@Existing_PAPROJNUMBER = '') then 0
                                       else @I_vPAclosetobillings
                                     end,
             @I_vPADepartment = case 
                                  when (@I_vPADepartment is null)
                                       and (@Existing_PAPROJNUMBER <> '') then @Existing_PADepartment
                                  when (@I_vPADepartment is null)
                                       and (@Existing_PAPROJNUMBER = '') then ''
                                  else @I_vPADepartment
                                end,
             @I_vPAEstimatorID = case 
                                   when (@I_vPAEstimatorID is null)
                                        and (@Existing_PAPROJNUMBER <> '') then @Existing_PAEstimatorID
                                   when (@I_vPAEstimatorID is null)
                                        and (@Existing_PAPROJNUMBER = '') then ''
                                   else @I_vPAEstimatorID
                                 end,
             @I_vPAprojmngrid = case 
                                  when (@I_vPAprojmngrid is null)
                                       and (@Existing_PAPROJNUMBER <> '') then @Existing_PAprojmngrid
                                  when (@I_vPAprojmngrid is null)
                                       and (@Existing_PAPROJNUMBER = '') then ''
                                  else @I_vPAprojmngrid
                                end,
             @I_vPABusMgrID = case 
                                when (@I_vPABusMgrID is null)
                                     and (@Existing_PAPROJNUMBER <> '') then @Existing_PABusMgrID
                                when (@I_vPABusMgrID is null)
                                     and (@Existing_PAPROJNUMBER = '') then ''
                                else @I_vPABusMgrID
                              end,
             @I_vPALabor_Rate_Table_ID = case 
                                           when (@I_vPALabor_Rate_Table_ID is null)
                                                and (@Existing_PAPROJNUMBER <> '') then @Existing_PALabor_Rate_Table_ID
                                           when (@I_vPALabor_Rate_Table_ID is null)
                                                and (@Existing_PAPROJNUMBER = '') then ''
                                           else @I_vPALabor_Rate_Table_ID
                                         end,
             @I_vPALabor_RateTable_Type = case 
                                            when (@I_vPALabor_RateTable_Type is null)
                                                 and (@Existing_PAPROJNUMBER <> '') then @Existing_PALabor_RateTable_Type
                                            when (@I_vPALabor_RateTable_Type is null)
                                                 and (@Existing_PAPROJNUMBER = '') then 0
                                            else @I_vPALabor_RateTable_Type
                                          end,
             @I_vPALabor_Rate_Table_Acc = case 
                                            when (@I_vPALabor_Rate_Table_Acc is null)
                                                 and (@Existing_PAPROJNUMBER <> '') then @Existing_PALabor_Rate_Table_Acc
                                            when (@I_vPALabor_Rate_Table_Acc is null)
                                                 and (@Existing_PAPROJNUMBER = '') then 0
                                            else @I_vPALabor_Rate_Table_Acc
                                          end,
             @I_vPAEquip_Rate_Table_ID = case 
                                           when (@I_vPAEquip_Rate_Table_ID is null)
                                                and (@Existing_PAPROJNUMBER <> '') then @Existing_PAEquip_Rate_Table_ID
                                           when (@I_vPAEquip_Rate_Table_ID is null)
                                                and (@Existing_PAPROJNUMBER = '') then ''
                                           else @I_vPAEquip_Rate_Table_ID
                                         end,
             @I_vPAEquip_Rate_Table_Acc = case 
                                            when (@I_vPAEquip_Rate_Table_Acc is null)
                                                 and (@Existing_PAPROJNUMBER <> '') then @Existing_PAEquip_Rate_Table_Acc
                                            when (@I_vPAEquip_Rate_Table_Acc is null)
                                                 and (@Existing_PAPROJNUMBER = '') then 0
                                            else @I_vPAEquip_Rate_Table_Acc
                                          end,
             @I_vPAbllngtype = case 
                                 when (@I_vPAbllngtype is null)
                                      and (@Existing_PAPROJNUMBER <> '') then @Existing_PAbllngtype
                                 when (@I_vPAbllngtype is null)
                                      and (@Existing_PAPROJNUMBER = '') then 0
                                 else @I_vPAbllngtype
                               end,
             @I_vPABILLFORMAT = case 
                                  when (@I_vPABILLFORMAT is null)
                                       and (@Existing_PAPROJNUMBER <> '') then @Existing_PABILLFORMAT
                                  when (@I_vPABILLFORMAT is null)
                                       and (@Existing_PAPROJNUMBER = '') then ''
                                  else @I_vPABILLFORMAT
                                end,
             @I_vDSCPCTAM = case 
                              when (@I_vDSCPCTAM is null)
                                   and (@Existing_PAPROJNUMBER <> '') then @Existing_DSCPCTAM
                              when (@I_vDSCPCTAM is null)
                                   and (@Existing_PAPROJNUMBER = '') then 0
                              else @I_vDSCPCTAM
                            end,
             @I_vPA_Retention_Percent = case 
                                          when (@I_vPA_Retention_Percent is null)
                                               and (@Existing_PAPROJNUMBER <> '') then @Existing_PA_Retention_Percent
                                          when (@I_vPA_Retention_Percent is null)
                                               and (@Existing_PAPROJNUMBER = '') then 0
                                          else @I_vPA_Retention_Percent
                                        end,
             @I_vPAUD1Proj = case 
                               when (@I_vPAUD1Proj is null)
                                    and (@Existing_PAPROJNUMBER <> '') then @Existing_PAUD1Proj
                               when (@I_vPAUD1Proj is null)
                                    and (@Existing_PAPROJNUMBER = '') then ''
                               else @I_vPAUD1Proj
                             end,
             @I_vPAUD2_Proj = case 
                                when (@I_vPAUD2_Proj is null)
                                     and (@Existing_PAPROJNUMBER <> '') then @Existing_PAUD2_Proj
                                when (@I_vPAUD2_Proj is null)
                                     and (@Existing_PAPROJNUMBER = '') then ''
                                else @I_vPAUD2_Proj
                              end,
             @I_vPASegmentCB = case 
                                 when (@I_vPASegmentCB is null)
                                      and (@Existing_PAPROJNUMBER <> '') then @Existing_PASegmentCB
                                 when (@I_vPASegmentCB is null)
                                      and (@Existing_PAPROJNUMBER = '') then 0
                                 else @I_vPASegmentCB
                               end,
             @I_vPRBTADCD = case 
                              when (@I_vPRBTADCD is null)
                                   and (@Existing_PAPROJNUMBER <> '') then @Existing_PRBTADCD
                              when (@I_vPRBTADCD is null)
                                   and (@Existing_PAPROJNUMBER = '') then ''
                              else @I_vPRBTADCD
                            end,
             @I_vPA_RestrictCustomerList = case 
                                             when (@I_vPA_RestrictCustomerList is null)
                                                  and (@Existing_PAPROJNUMBER <> '') then @Existing_PA_RestrictCustomerList
                                             when (@I_vPA_RestrictCustomerList is null)
                                                  and (@Existing_PAPROJNUMBER = '') then 0
                                             else @I_vPA_RestrictCustomerList
                                           end,
             @I_vWRKRCOMP = case 
                              when (@I_vWRKRCOMP is null)
                                   and (@Existing_PAPROJNUMBER <> '') then @Existing_WRKRCOMP
                              when (@I_vWRKRCOMP is null)
                                   and (@Existing_PAPROJNUMBER = '') then ''
                              else @I_vWRKRCOMP
                            end,
             @I_vSUTASTAT = case 
                              when (@I_vSUTASTAT is null)
                                   and (@Existing_PAPROJNUMBER <> '') then @Existing_SUTASTAT
                              when (@I_vSUTASTAT is null)
                                   and (@Existing_PAPROJNUMBER = '') then ''
                              else @I_vSUTASTAT
                            end,
             @I_vPATRKCHGORDS = case 
                                  when (@I_vPATRKCHGORDS is null)
                                       and (@Existing_PAPROJNUMBER <> '') then @Existing_PATRKCHGORDS
                                  when (@I_vPATRKCHGORDS is null)
                                       and (@Existing_PAPROJNUMBER = '') then ''
                                  else @I_vPATRKCHGORDS
                                end,
             @I_vPATRKCOBDGADDFLY = case 
                                      when (@I_vPATRKCOBDGADDFLY is null)
                                           and (@Existing_PAPROJNUMBER <> '') then @Existing_PATRKCOBDGADDFLY
                                      when (@I_vPATRKCOBDGADDFLY is null)
                                           and (@Existing_PAPROJNUMBER = '') then ''
                                      else @I_vPATRKCOBDGADDFLY
                                    end
      
      if (@Existing_CUSTNMBR <> @I_vCUSTNMBR)
         and (@Existing_CUSTNMBR <> '')
        begin
          select @O_iErrorState = 6434
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      if (@Existing_PAprojid <> @I_vPAprojid)
         and (@Existing_PAprojid <> '')
        begin
          select @O_iErrorState = 6435
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      if (@I_vPAProjectType is null)
        select @I_vPAProjectType = 0
      
      if (@I_vPAAcctgMethod is null)
        select @I_vPAAcctgMethod = 0
      
      if (@I_vPASTAT is null)
        select @I_vPASTAT = 4
      
      if (@I_vPABBeginDate is null)
        select @I_vPABBeginDate = ''
      
      if (@I_vPABEndDate is null)
        select @I_vPABEndDate = ''
      
      if (@I_vPAprjclsid is null)
        select @I_vPAprjclsid = ''
      
      if (@I_vPApurordnum is null)
        select @I_vPApurordnum = ''
      
      if (@I_vPAcloseProjcosts is null)
        select @I_vPAcloseProjcosts = 0
      
      if (@I_vPAclosetobillings is null)
        select @I_vPAclosetobillings = 0
      
      if (@I_vPADepartment is null)
        select @I_vPADepartment = ''
      
      if (@I_vPAEstimatorID is null)
        select @I_vPAEstimatorID = ''
      
      if (@I_vPAprojmngrid is null)
        select @I_vPAprojmngrid = ''
      
      if (@I_vPABusMgrID is null)
        select @I_vPABusMgrID = ''
      
      if (@I_vPALabor_Rate_Table_ID is null)
        select @I_vPALabor_Rate_Table_ID = ''
      
      if (@I_vPALabor_RateTable_Type is null)
        select @I_vPALabor_RateTable_Type = 1
      
      if (@I_vPALabor_Rate_Table_Acc is null)
        select @I_vPALabor_Rate_Table_Acc = 0
      
      if (@I_vPAEquip_Rate_Table_ID is null)
        select @I_vPAEquip_Rate_Table_ID = ''
      
      if (@I_vPAEquip_Rate_Table_Acc is null)
        select @I_vPAEquip_Rate_Table_Acc = 0
      
      if (@I_vPAbllngtype is null)
        select @I_vPAbllngtype = 1
      
      if (@I_vPABILLFORMAT is null)
        select @I_vPABILLFORMAT = ''
      
      if (@I_vDSCPCTAM is null)
        select @I_vDSCPCTAM = 0
      
      if (@I_vPA_Retention_Percent is null)
        select @I_vPA_Retention_Percent = 0
      
      if (@I_vPAUD1Proj is null)
        select @I_vPAUD1Proj = ''
      
      if (@I_vPAUD2_Proj is null)
        select @I_vPAUD2_Proj = ''
      
      if (@I_vPASegmentCB is null)
        select @I_vPASegmentCB = 0
      
      if (@I_vPRBTADCD is null)
        select @I_vPRBTADCD = ''
      
      if (@I_vPABILLCYCLEID1 is null)
        select @I_vPABILLCYCLEID1 = ''
      
      if (@I_vPA_RestrictCustomerList is null)
        select @I_vPA_RestrictCustomerList = 0
      
      if (@I_vWRKRCOMP is null)
        select @I_vWRKRCOMP = ''
      
      if (@I_vSUTASTAT is null)
        select @I_vSUTASTAT = ''
      
      if (@I_vPATRKCHGORDS is null)
        select @I_vPATRKCHGORDS = 0
      
      if (@I_vPATRKCOBDGADDFLY is null)
        select @I_vPATRKCOBDGADDFLY = 0
      
      if exists (select 1
                 from   PA01201 (nolock)
                 where  PAPROJNUMBER = @I_vPAPROJNUMBER)
        begin
          select @O_iErrorState = 6149
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  select @PACONTNUMBER = PACONTNUMBER,
         @LOCATNID = LOCATNID,
         @SLPRSNID = SLPRSNID,
         @SALSTERR = SALSTERR,
         @COMPRCNT = COMPRCNT,
         @COMAPPTO = COMAPPTO,
         @PAContMgrID = PAContMgrID,
         @PABusMgrID = PABusMgrID,
         @PABILLFORMAT = PABILLFORMAT,
         @PRBTADCD = PRBTADCD,
         @PAUD1_Cont = PAUD1_Cont,
         @PAUD2_Cont = PAUD2_Cont,
         @PAProjectType = PAProjectType,
         @PAAcctgMethod = PAAcctgMethod,
         @CNTCPRSN = CNTCPRSN,
         @PA_RestrictCustomerList = PA_RestrictCustomerList,
         @PAcontid = PAcontid,
         @Cont_Status = PASTAT,
         @PAMCCURNCYID = PAMCCURNCYID,
         @CURRNIDX = CURRNIDX
  from   PA01101 (nolock)
  where  PACONTNUMBER = @I_vPACONTNUMBER
  
  if (@PACONTNUMBER = '')
    begin
      select @O_iErrorState = 6146
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@Cont_Status <> 4)
     and (@Existing_PAPROJNUMBER = '')
    begin
      select @O_iErrorState = 9391
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAprojmngrid = '')
    select @I_vPAprojmngrid = @PAContMgrID
  
  if (@I_vPABusMgrID = '')
    select @I_vPABusMgrID = @PABusMgrID
  
  if (@I_vPABILLFORMAT = '')
    select @I_vPABILLFORMAT = @PABILLFORMAT
  
  if (@I_vPRBTADCD = '')
    select @I_vPRBTADCD = @PRBTADCD
  
  if (@I_vPAUD1Proj = '')
    select @I_vPAUD1Proj = @PAUD1_Cont
  
  if (@I_vPAUD2_Proj = '')
    select @I_vPAUD2_Proj = @PAUD2_Cont
  
  if (@I_vPAProjectType = '')
    select @I_vPAProjectType = @PAProjectType
  
  if (@I_vPAAcctgMethod = '')
    select @I_vPAAcctgMethod = @PAAcctgMethod
  
  if (@I_vPA_RestrictCustomerList = '')
    select @I_vPA_RestrictCustomerList = @PA_RestrictCustomerList
  
  if not exists (select 1
                 from   RM00101 (nolock)
                 where  CUSTNMBR = @I_vCUSTNMBR)
    begin
      select @O_iErrorState = 6147
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  else
    begin
      if (@CNTCPRSN = '')
        select @CNTCPRSN = CNTCPRSN
        from   RM00101 (nolock)
        where  CUSTNMBR = @I_vCUSTNMBR
    end
  
  if (@I_vPAprjclsid <> '')
    begin
      if not exists (select 1
                     from   PA41201 (nolock)
                     where  PAprjclsid = @I_vPAprjclsid)
        begin
          select @O_iErrorState = 6148
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAProjectType < 1)
      or (@I_vPAProjectType > 3)
    begin
      select @O_iErrorState = 6150
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAAcctgMethod < 1)
      or (@I_vPAAcctgMethod > 6)
    begin
      select @O_iErrorState = 6151
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPASTAT < 1)
      or (@I_vPASTAT > 5)
    begin
      select @O_iErrorState = 6669
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPASTAT <> 4)
     and (@Existing_PAPROJNUMBER = '')
    begin
      select @O_iErrorState = 6152
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vPASTAT = 5
    begin
      if exists (select 1
                 from   PA12000 a (nolock)
                 where  (a.PACHGORDNO in (select b.PACHGORDNO
                                          from   PA12001 b (nolock)
                                          where  b.PAPROJNUMBER = @I_vPAPROJNUMBER)
                          or a.PACHGORDNO in (select c.PACHGORDNO
                                              from   PA12101 c (nolock)
                                              where  c.PAPROJNUMBER = @I_vPAPROJNUMBER)
                          or a.PACHGORDNO in (select d.PACHGORDNO
                                              from   PA12103 d (nolock)
                                              where  d.PAPROJNUMBER = @I_vPAPROJNUMBER))
                        and a.PACONTNUMBER = @I_vPACONTNUMBER
                        and a.PADOCSTATUS < 5)
        begin
          select @O_iErrorState = 6436
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if @I_vPASTAT = 3
    begin
      select @O_iErrorState = 6437
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vPASTAT <> 3
     and @Cont_Status = 3
    begin
      select @O_iErrorState = 6527
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vPASTAT <> 5
     and @Cont_Status = 5
    begin
      select @O_iErrorState = 6438
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if @I_vPASTAT <> 3
     and @Cont_Status = 3
    begin
      select @O_iErrorState = 6439
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAcloseProjcosts < 0)
      or (@I_vPAcloseProjcosts > 1)
    begin
      select @O_iErrorState = 6153
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAclosetobillings < 0)
      or (@I_vPAclosetobillings > 1)
    begin
      select @O_iErrorState = 6154
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPA_RestrictCustomerList < 0)
      or (@I_vPA_RestrictCustomerList > 1)
    begin
      select @O_iErrorState = 6155
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPASegmentCB < 0)
      or (@I_vPASegmentCB > 1)
    begin
      select @O_iErrorState = 6156
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAprojmngrid <> '')
    begin
      if not exists (select 1
                     from   UPR00100 (nolock)
                     where  EMPLOYID = @I_vPAprojmngrid)
        begin
          select @O_iErrorState = 6157
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        if not exists (select 1
                       from   PA00601 (nolock)
                       where  EMPLOYID = @I_vPAprojmngrid
                              and PApmcb = 1)
          begin
            select @O_iErrorState = 6158
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @O_oErrorState output
            
            return (@O_iErrorState)
          end
    end
  
  if (@I_vPABusMgrID <> '')
    begin
      if not exists (select 1
                     from   UPR00100 (nolock)
                     where  EMPLOYID = @I_vPABusMgrID)
        begin
          select @O_iErrorState = 6159
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      else
        if not exists (select 1
                       from   PA00601 (nolock)
                       where  EMPLOYID = @I_vPABusMgrID
                              and PAbmcb = 1)
          begin
            select @O_iErrorState = 6160
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @O_oErrorState output
            
            return (@O_iErrorState)
          end
    end
  
  if (@I_vPRBTADCD <> '')
    begin
      if not exists (select 1
                     from   RM00102 (nolock)
                     where  CUSTNMBR = @I_vCUSTNMBR
                            and ADRSCODE = @I_vPRBTADCD)
        begin
          select @O_iErrorState = 6161
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPABILLFORMAT <> '')
    begin
      if not exists (select 1
                     from   PA43101 (nolock)
                     where  PA_Bill_Format_Number = @I_vPABILLFORMAT)
        begin
          select @O_iErrorState = 6164
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vDSCPCTAM < 0)
      or (@I_vDSCPCTAM > 100)
    begin
      select @O_iErrorState = 6165
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPA_Retention_Percent < 0)
      or (@I_vPA_Retention_Percent > 100)
    begin
      select @O_iErrorState = 6166
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPABILLCYCLEID1 <> '')
    begin
      if not exists (select 1
                     from   PA02010 (nolock)
                     where  PABILLCYCLEID1 = @I_vPABILLCYCLEID1)
        begin
          select @O_iErrorState = 6167
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      select @I_vPABILLCYCLEID1 = PABILLCYCLEID1
      from   PA02501 (nolock)
      where  PACONTNUMBER = @I_vPACONTNUMBER
    end
  
  if (@I_vPADepartment <> '')
    begin
      if not exists (select 1
                     from   UPR40300 (nolock)
                     where  DEPRTMNT = @I_vPADepartment)
        begin
          select @O_iErrorState = 6168
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAEstimatorID <> '')
    begin
      if not exists (select 1
                     from   PA00601 (nolock)
                     where  EMPLOYID = @I_vPAEstimatorID)
        begin
          select @O_iErrorState = 6169
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPALabor_RateTable_Type < 1)
      or (@I_vPALabor_RateTable_Type > 2)
    begin
      select @O_iErrorState = 6172
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPALabor_Rate_Table_ID <> '')
    begin
      if (@I_vPALabor_RateTable_Type = 1)
        begin
          if not exists (select 1
                         from   PA01402 (nolock)
                         where  PARate_Table_ID = @I_vPALabor_Rate_Table_ID)
            begin
              select @O_iErrorState = 6171
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          if not exists (select 1
                         from   PA01404 (nolock)
                         where  PARate_Table_ID = @I_vPALabor_Rate_Table_ID)
            begin
              select @O_iErrorState = 9372
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
    end
  
  if (@I_vPALabor_Rate_Table_Acc < 0)
      or (@I_vPALabor_Rate_Table_Acc > 1)
    begin
      select @O_iErrorState = 6173
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAEquip_Rate_Table_ID <> '')
    begin
      if not exists (select 1
                     from   PA01406 (nolock)
                     where  PARate_Table_ID = @I_vPAEquip_Rate_Table_ID)
        begin
          select @O_iErrorState = 6174
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAEquip_Rate_Table_Acc < 0)
      or (@I_vPAEquip_Rate_Table_Acc > 1)
    begin
      select @O_iErrorState = 6175
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAbllngtype < 1)
      or (@I_vPAbllngtype > 3)
    begin
      select @O_iErrorState = 6176
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vSUTASTAT <> '')
    begin
      if not exists (select 1
                     from   UPR41100 (nolock)
                     where  STATECD = @I_vSUTASTAT)
        begin
          select @O_iErrorState = 6177
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vWRKRCOMP <> '')
    begin
      if not exists (select 1
                     from   UPR40700 (nolock)
                     where  WRKRCOMP = @I_vWRKRCOMP)
        begin
          select @O_iErrorState = 6178
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@Existing_PAPROJNUMBER = '')
    begin
      select @sCompanyID = CMPANYID
      from   DYNAMICS..SY01500 (nolock)
      where  INTERID = db_name()
      
      exec @iStatus = DYNAMICS..tasmGetNextNoteIndex
        @I_sCompanyID = @sCompanyID ,
        @I_iSQLSessionID = 0 ,
        @I_noteincrement = 9 ,
        @O_mNoteIndex = @PAbillnoteidx output ,
        @O_iErrorState = @iGetNextNoteIdxErrState output
      
      select @iError = @@error
      
      if ((@iStatus <> 0)
           or (@iGetNextNoteIdxErrState <> 0)
           or (@iError <> 0))
        begin
          select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iGetNextNoteIdxErrState))
          
          select @O_iErrorState = 6528
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      select @PAbillnoteidxts = @PAbillnoteidx + 1,
             @PAELbillniteidx = @PAbillnoteidx + 2,
             @PAbillnoteidxML = @PAbillnoteidx + 3,
             @PAbillnoteidxvi = @PAbillnoteidx + 4,
             @PAbillnoteidxee = @PAbillnoteidx + 5,
             @PAbillnoteidxinv = @PAbillnoteidx + 6,
             @PAbillnoteidxfee = @PAbillnoteidx + 7,
             @NOTEINDX = @PAbillnoteidx + 8
    end
  else
    begin
      select @PAbillnoteidx = @Existing_PAbillnoteidx,
             @PAbillnoteidxts = @Existing_PAbillnoteidxts,
             @PAELbillniteidx = @Existing_PAELbillniteidx,
             @PAbillnoteidxML = @Existing_PAbillnoteidxML,
             @PAbillnoteidxvi = @Existing_PAbillnoteidxvi,
             @PAbillnoteidxee = @Existing_PAbillnoteidxee,
             @PAbillnoteidxinv = @Existing_PAbillnoteidxinv,
             @PAbillnoteidxfee = @Existing_PAbillnoteidxfee
    end
  
  select @userdate = getdate()
  
  execute @iStatus = glCalculateGLPeriod
    7 ,
    'Timesheet Entry' ,
    @userdate ,
    @userdate ,
    @l_Period output ,
    @fClosed output ,
    @YEAR1 output ,
    @nErr output ,
    @iCalculateGLPeriodErrState output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCalculateGLPeriodErrState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCalculateGLPeriodErrState))
      
      select @O_iErrorState = 6440
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Projects' ,
        @I_vINDEX1 = @I_vPAPROJNUMBER ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 0 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 6179
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@Existing_PAPROJNUMBER <> '')
    begin
      delete PA01601
      where  PAbillnoteidx = @PAbillnoteidx
              or PAbillnoteidx = @PAbillnoteidxts
              or PAbillnoteidx = @PAELbillniteidx
              or PAbillnoteidx = @PAbillnoteidxML
              or PAbillnoteidx = @PAbillnoteidxvi
              or PAbillnoteidx = @PAbillnoteidxee
              or PAbillnoteidx = @PAbillnoteidxinv
              or PAbillnoteidx = @PAbillnoteidxfee
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6441
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteINV <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidxinv,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteINV
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5271
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteEE <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidxee,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteEE
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5272
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteVI <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidxvi,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteVI
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5273
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteML <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidxML,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteML
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5274
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteEL <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAELbillniteidx,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteEL
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5275
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteDefault <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidx,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteDefault
      
      if @@error <> 0
        begin
          select @O_iErrorState = 9390
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteTS <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidxts,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteTS
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5276
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@I_vPAbillnoteFEE <> '')
    begin
      insert into PA01601
                 (PAbillnoteidx,
                  DATE1,
                  TIME1,
                  PAtx500)
      select @PAbillnoteidxfee,
             cast(getdate() as varchar(12)),
             substring(cast(getdate() as varchar(17)),13,5),
             @I_vPAbillnoteFEE
      
      if @@error <> 0
        begin
          select @O_iErrorState = 8079
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@Existing_PAPROJNUMBER = '')
    begin
      delete PA61040
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if (@I_vPABILLCYCLEID1 <> '')
        begin
          insert into PA61040
                     (PAPROJNUMBER,
                      PABILLCYCLEID1)
          select @I_vPAPROJNUMBER,
                 @I_vPABILLCYCLEID1
          
          if @@error <> 0
            begin
              select @O_iErrorState = 6529
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      
      insert into PA01211
                 (PACONTNUMBER,
                  PAPROJNUMBER,
                  CUSTNMBR,
                  PABillingMode,
                  PARevSourceAmt,
                  PARevSourcePercent)
      select @I_vPACONTNUMBER,
             @I_vPAPROJNUMBER,
             @I_vCUSTNMBR,
             1,
             0,
             0
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6181
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      if (@I_vPABILLCYCLEID1 <> '')
        begin
          update PA61040
          set    PABILLCYCLEID1 = @I_vPABILLCYCLEID1
          where  PAPROJNUMBER = @I_vPAPROJNUMBER
          
          if @@error <> 0
            begin
              select @O_iErrorState = 6180
              
              exec @iStatus = taUpdateString
                @O_iErrorState ,
                @oErrString ,
                @oErrString output ,
                @O_oErrorState output
              
              return (@O_iErrorState)
            end
        end
      else
        begin
          delete PA61040
          where  PAPROJNUMBER = @I_vPAPROJNUMBER
        end
    end
  
  if not exists (select 1
                 from   PA01241 (nolock)
                 where  PAPROJNUMBER = @I_vPAPROJNUMBER)
    begin
      insert into PA01241
                 (PAPROJNUMBER,
                  PATRKCHGORDS,
                  PATRKCOBDGADDFLY,
                  PATOTCHGORDAMT,
                  PANOPENDCO,
                  PANUMCO,
                  PACOBASEPROJAMT,
                  PAUNPRRTOTPROJAMT,
                  PACOBSLNFEEPROJAMT,
                  PAUNABSTOTBILL,
                  PAUNPBSTOTCST,
                  PAUNPBSLTOTPROF,
                  PAUNPBSTOTQTY,
                  PAUNAPFRTOTBILL,
                  PAUNAPPFRTOTCST,
                  PAUNFRCTOTPROF,
                  PAUNPFRTOTQTY,
                  PAPostedQty,
                  PAPostedTotalCostN,
                  PAPosted_Overhead,
                  PAPostedProfitN,
                  PAPosted_Tax_Amount,
                  PAACREV,
                  PAPostRecogRevN,
                  PAPOSBIEEAMOUNT,
                  PAPOSEIEBAMOUNT,
                  PApostbillamt,
                  PA_Actual_Receipts_Amoun,
                  PAPostedBillingsN,
                  PAPostedDiscDolAmtN,
                  PAPosted_Sales_Tax_Amou,
                  ActualWriteOffAmount,
                  ActualDiscTakenAmount,
                  PAactjtaxpd,
                  PAPOSTRETAMT,
                  PAActualWOTaxAmt,
                  PAActualTermsTakenTax,
                  PAPosted_Project_Fee,
                  PAPosted_Retainer_Fee,
                  PAPosted_Service_Fee)
      select @I_vPAPROJNUMBER,
             @I_vPATRKCHGORDS,
             @I_vPATRKCOBDGADDFLY,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6182
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      update PA01241
      set    PATRKCHGORDS = @I_vPATRKCHGORDS,
             PATRKCOBDGADDFLY = @I_vPATRKCOBDGADDFLY
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6442
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  if (@Existing_PAPROJNUMBER <> '')
     and (@I_vPASTAT <> @Existing_PASTAT)
    begin
      if @Existing_PASTAT = 3
          or @I_vPASTAT = 5
        begin
          UPDATE a
          set    a.PASTAT = @I_vPASTAT,
                 a.PAPreviouslyOpen = case 
                                        when (@I_vPASTAT <> 4) then 1
                                        else a.PAPreviouslyOpen
                                      end
          FROM   PA01301 a
                 INNER JOIN PA01201 c
                   ON a.PAPROJNUMBER = c.PAPROJNUMBER
          where  (@I_vPASTAT = 5
                  and a.PASTAT <> 3)
                  or (@Existing_PASTAT = 4
                      and a.PASTAT = @Existing_PASTAT
                      and @I_vPASTAT <> 5)
                  or (@Existing_PASTAT = 4
                      and a.PASTAT = @Existing_PASTAT
                      and @I_vPASTAT > 5)
                     and a.PAPROJNUMBER = @I_vPAPROJNUMBER
        end
    end
  
  if (@Existing_PAPROJNUMBER = '')
    begin
      insert into PA41301
                 (PAPROJNUMBER,
                  PAOVERIDESEGTMWIP,
                  PAOVERIDESEGTMCOGS,
                  PAOVERIDESEGTMCONTRA,
                  PAOVERIDESEGTMUNBILLAR,
                  PASEGTMUNBILLREV,
                  PASEGTMAR,
                  PASEGTMPROJREV,
                  PASEGTMOVERHEAD,
                  PASEGTMDEFEREV,
                  OVRSegTMProjSales,
                  OVRSegTMARFee,
                  PASEGFPWIP,
                  PASEGFPCONTRA,
                  PASEGFPAR,
                  PASEGFPROJBILL,
                  PASEGFPROJEXP,
                  PASEGFPROJREV,
                  PASEGFPROJLOSS,
                  PASEGFPBIEE,
                  PASEGFPEIEB,
                  PASEGFPOVERHD,
                  PASEGFPRETENAR,
                  PASEGFPDEFEREV,
                  OVRSegFPARFee,
                  OVRSegFPExcessBlngFee,
                  OVRSegFPExcessEarnFee,
                  OVRSegFPProjBillingsFee,
                  OVRSegFPProjRevFee,
                  OVRSegFPProjWIPFee)
      select @I_vPAPROJNUMBER,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5453
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      insert into PA41302
                 (PAPROJNUMBER,
                  SGMTNUMB,
                  SGMTNAME,
                  SGMNTID)
      select @I_vPAPROJNUMBER,
             SGMTNUMB,
             SGMTNAME,
             ''
      from   SY00300 (nolock)
      where  MNSEGIND = 0
      
      if @@error <> 0
        begin
          select @O_iErrorState = 5454
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
      
      insert into PA01201
                 (CUSTNMBR,
                  PACONTNUMBER,
                  PAcontid,
                  PAprojid,
                  PAprojname,
                  PAPROJNUMBER,
                  PAprjclsid,
                  PAProjectType,
                  PAAcctgMethod,
                  PASTAT,
                  CURRNIDX,
                  PApurordnum,
                  PABBeginDate,
                  PABEndDate,
                  PABQuantity,
                  PABTotalCost,
                  PABProfit,
                  PABBillings,
                  PABTaxPaidAmt,
                  PABTaxChargedAmt,
                  PABaselineOvhdCost,
                  PAACTUALBEGDATE,
                  PA_Actual_End_Date,
                  PAFBeginDate,
                  PAFEndDate,
                  PAFQuantity,
                  PAFTotalCost,
                  PAFProfit,
                  PAFBillings,
                  PAFTaxPaidAmt,
                  PAFTaxChargedAmt,
                  PAForecastOvhdCost,
                  PAcloseProjcosts,
                  PAclosetobillings,
                  PADepartment,
                  PAEstimatorID,
                  PAprojmngrid,
                  PABusMgrID,
                  LOCATNID,
                  SLPRSNID,
                  SALSTERR,
                  COMPRCNT,
                  COMAPPTO,
                  CNTCPRSN,
                  PRBTADCD,
                  PALabor_Rate_Table_ID,
                  PALabor_RateTable_Type,
                  PALabor_Rate_Table_Acc,
                  PAEquip_Rate_Table_ID,
                  PAEquip_Rate_Table_Acc,
                  PAbllngtype,
                  PAService_Fee_Amount,
                  PAProject_Fee_Amount,
                  PARetainer_Fee_Amount,
                  PARetentionFeeAmount,
                  PAProject_Amount,
                  ACCTAMNT,
                  PABILLFORMAT,
                  DSCPCTAM,
                  PA_Retention_Percent,
                  PAtotcbts,
                  PAtotcbEL,
                  PAtotcbML,
                  PAtotcbvi,
                  PAtotcber,
                  PAtotcbinv,
                  PAtotcbfee,
                  PAUD1Proj,
                  PAUD2_Proj,
                  PASegmentCB,
                  PAbillnoteidx,
                  PAbillnoteidxts,
                  PAELbillniteidx,
                  PAbillnoteidxML,
                  PAbillnoteidxvi,
                  PAbillnoteidxee,
                  PAbillnoteidxinv,
                  PAbillnoteidxfee,
                  PAUnpostedQty,
                  PAUnpostedTotalCostN,
                  PAUnposted_Overhead,
                  PAUnpostedProfitN,
                  PAUnposted_Tax_Amount,
                  PAUnpostAccrRevN,
                  PAUnpostedRecogRevN,
                  PAUnpostedCommitedQty,
                  PAUnpostedCommitedCost,
                  PAUnpostedCommitedTaxAmt,
                  PAUnposted_Project_Fee,
                  PAUnposted_Retainer_Fee,
                  PAUnposted_Service_Fee,
                  PAUNPOSTRETAMT,
                  PAUNPOSTBIEEAMOUNT,
                  PAUNPEIEBAMOUNT,
                  PA_Unposted_Billed_Reten,
                  PAPostedQty,
                  PAPostedTotalCostN,
                  PAPostedProfitN,
                  PAPosted_Tax_Amount,
                  PAPosted_Accr_RevN,
                  PAPostRecogRevN,
                  PAPostedCommitedQty,
                  PAPostedCommitedCost,
                  PAPostedCommitedTaxAmt,
                  PAPosted_Project_Fee,
                  PAPosted_Retainer_Fee,
                  PAPosted_Service_Fee,
                  PAPOSTRETAMT,
                  PAPOSBIEEAMOUNT,
                  PAPOSEIEBAMOUNT,
                  PA_Actual_Billed_Retenti,
                  PAWrite_UpDown_Amount,
                  PABilled_QtyN,
                  PABilled_Cost,
                  PABilled_Accrued_Revenu,
                  PACostPcntCompleted,
                  PAQuantityPcntCompleted,
                  PA_Receipts_Amount,
                  PA_Actual_Receipts_Amoun,
                  PA_Earnings,
                  PA_Cost_of_Earnings,
                  PAUnpostBillN,
                  PAUnpostDiscDolAmtN,
                  PAUnposted_Sales_Tax_Am,
                  PAPostedBillingsN,
                  PAPostedDiscDolAmtN,
                  PAPosted_Sales_Tax_Amou,
                  PABCWPAMT,
                  PABCWSAMT,
                  PAACWPAMT,
                  NOTEINDX,
                  PAPosted_Overhead,
                  PAApproved_Accrued_Reve,
                  PAApproved_Cost,
                  PAApproved_Quantity,
                  WROFAMNT,
                  ActualWriteOffAmount,
                  DISTKNAM,
                  ActualDiscTakenAmount,
                  PACommitted_Costs,
                  PACommitted_Qty,
                  PAPOCost,
                  PAPOQty,
                  PAPOPostedCost,
                  PAPOPostedQty,
                  PAtaxpaidamt,
                  PAPostedTaxPaidN,
                  PApretainage,
                  PAunpretainage,
                  PA_Write_Off_Tax_Amount,
                  PAActualWOTaxAmt,
                  PA_Terms_Taken_Tax_Amt,
                  PAActualTermsTakenTax,
                  WRKRCOMP,
                  SUTASTAT,
                  PA_Actual_Billing_not_RM,
                  PA_ActualDiscnotRM,
                  PA_RestrictCustomerList,
                  PAPosted_Earnings,
                  PAActualCostofEarnings,
                  PAUnpostedLossAmount,
                  PAActualLossAmount,
                  PAFQLaborOnly,
                  PAAQLaborOnly,
                  PAMCCURNCYID,
                  PAMCProjectAmount,
                  PAMCUnpostBillN,
                  PAMCUnpostedSalesTax,
                  PAMCUnpostedDiscount,
                  PAMCActualBillings,
                  PAMCActualSalesTaxAmt,
                  PAMCActualDiscountAmt,
                  PAMCBBillings,
                  PAMCFBillings,
                  PA_MC_Project_Fee_Amount,
                  PA_MC_Retainer_Fee_Amt,
                  PA_MC_Service_Fee_Amount,
                  PA_MC_UnpostedProjectFee,
                  PA_MC_Unposted_Retainer,
                  PA_MC_UnpostedServiceFee,
                  PA_MC_Actual_Project_Fee,
                  PA_MC_Actual_RetainerFee,
                  PA_MC_Actual_Service_Fee)
      select @I_vCUSTNMBR,
             @I_vPACONTNUMBER,
             @PAcontid,
             @I_vPAprojid,
             @I_vPAprojname,
             @I_vPAPROJNUMBER,
             @I_vPAprjclsid,
             @I_vPAProjectType,
             @I_vPAAcctgMethod,
             @I_vPASTAT,
             @CURRNIDX,
             @I_vPApurordnum,
             @I_vPABBeginDate,
             @I_vPABEndDate,
             @PABQuantity,
             @PABTotalCost,
             @PABProfit,
             @PABBillings,
             @PABTaxPaidAmt,
             @PABTaxChargedAmt,
             @PABaselineOvhdCost,
             @PAACTUALBEGDATE,
             @PA_Actual_End_Date,
             @PAFBeginDate,
             @PAFEndDate,
             @PAFQuantity,
             @PAFTotalCost,
             @PAFProfit,
             @PAFBillings,
             @PAFTaxPaidAmt,
             @PAFTaxChargedAmt,
             @PAForecastOvhdCost,
             @I_vPAcloseProjcosts,
             @I_vPAclosetobillings,
             @I_vPADepartment,
             @I_vPAEstimatorID,
             @I_vPAprojmngrid,
             @I_vPABusMgrID,
             @LOCATNID,
             @SLPRSNID,
             @SALSTERR,
             @COMPRCNT,
             @COMAPPTO,
             @CNTCPRSN,
             @I_vPRBTADCD,
             @I_vPALabor_Rate_Table_ID,
             @I_vPALabor_RateTable_Type,
             @I_vPALabor_Rate_Table_Acc,
             @I_vPAEquip_Rate_Table_ID,
             @I_vPAEquip_Rate_Table_Acc,
             @I_vPAbllngtype,
             @PAService_Fee_Amount,
             @PAProject_Fee_Amount,
             @PARetainer_Fee_Amount,
             @PARetentionFeeAmount,
             @PAProject_Amount,
             @ACCTAMNT,
             @I_vPABILLFORMAT,
             @I_vDSCPCTAM,
             @I_vPA_Retention_Percent,
             @PAtotcbts,
             @PAtotcbEL,
             @PAtotcbML,
             @PAtotcbvi,
             @PAtotcber,
             @PAtotcbinv,
             @PAtotcbfee,
             @I_vPAUD1Proj,
             @I_vPAUD2_Proj,
             @I_vPASegmentCB,
             @PAbillnoteidx,
             @PAbillnoteidxts,
             @PAELbillniteidx,
             @PAbillnoteidxML,
             @PAbillnoteidxvi,
             @PAbillnoteidxee,
             @PAbillnoteidxinv,
             @PAbillnoteidxfee,
             @PAUnpostedQty,
             @PAUnpostedTotalCostN,
             @PAUnposted_Overhead,
             @PAUnpostedProfitN,
             @PAUnposted_Tax_Amount,
             @PAUnpostAccrRevN,
             @PAUnpostedRecogRevN,
             @PAUnpostedCommitedQty,
             @PAUnpostedCommitedCost,
             @PAUnpostedCommitedTaxAmt,
             @PAUnposted_Project_Fee,
             @PAUnposted_Retainer_Fee,
             @PAUnposted_Service_Fee,
             @PAUNPOSTRETAMT,
             @PAUNPOSTBIEEAMOUNT,
             @PAUNPEIEBAMOUNT,
             @PA_Unposted_Billed_Reten,
             @PAPostedQty,
             @PAPostedTotalCostN,
             @PAPostedProfitN,
             @PAPosted_Tax_Amount,
             @PAPosted_Accr_RevN,
             @PAPostRecogRevN,
             @PAPostedCommitedQty,
             @PAPostedCommitedCost,
             @PAPostedCommitedTaxAmt,
             @PAPosted_Project_Fee,
             @PAPosted_Retainer_Fee,
             @PAPosted_Service_Fee,
             @PAPOSTRETAMT,
             @PAPOSBIEEAMOUNT,
             @PAPOSEIEBAMOUNT,
             @PA_Actual_Billed_Retenti,
             @PAWrite_UpDown_Amount,
             @PABilled_QtyN,
             @PABilled_Cost,
             @PABilled_Accrued_Revenu,
             @PACostPcntCompleted,
             @PAQuantityPcntCompleted,
             @PA_Receipts_Amount,
             @PA_Actual_Receipts_Amoun,
             @PA_Earnings,
             @PA_Cost_of_Earnings,
             @PAUnpostBillN,
             @PAUnpostDiscDolAmtN,
             @PAUnposted_Sales_Tax_Am,
             @PAPostedBillingsN,
             @PAPostedDiscDolAmtN,
             @PAPosted_Sales_Tax_Amou,
             @PABCWPAMT,
             @PABCWSAMT,
             @PAACWPAMT,
             @NOTEINDX,
             @PAPosted_Overhead,
             @PAApproved_Accrued_Reve,
             @PAApproved_Cost,
             @PAApproved_Quantity,
             @WROFAMNT,
             @ActualWriteOffAmount,
             @DISTKNAM,
             @ActualDiscTakenAmount,
             @PACommitted_Costs,
             @PACommitted_Qty,
             @PAPOCost,
             @PAPOQty,
             @PAPOPostedCost,
             @PAPOPostedQty,
             @PAtaxpaidamt,
             @PAPostedTaxPaidN,
             @PApretainage,
             @PAunpretainage,
             @PA_Write_Off_Tax_Amount,
             @PAActualWOTaxAmt,
             @PA_Terms_Taken_Tax_Amt,
             @PAActualTermsTakenTax,
             @I_vWRKRCOMP,
             @I_vSUTASTAT,
             @PA_Actual_Billing_not_RM,
             @PA_ActualDiscnotRM,
             @I_vPA_RestrictCustomerList,
             @PAPosted_Earnings,
             @PAActualCostofEarnings,
             @PAUnpostedLossAmount,
             @PAActualLossAmount,
             @PAFQLaborOnly,
             @PAAQLaborOnly,
             @PAMCCURNCYID,
             @PAMCProjectAmount,
             @PAMCUnpostBillN,
             @PAMCUnpostedSalesTax,
             @PAMCUnpostedDiscount,
             @PAMCActualBillings,
             @PAMCActualSalesTaxAmt,
             @PAMCActualDiscountAmt,
             @PAMCBBillings,
             @PAMCFBillings,
             @PA_MC_Project_Fee_Amount,
             @PA_MC_Retainer_Fee_Amt,
             @PA_MC_Service_Fee_Amount,
             @PA_MC_UnpostedProjectFee,
             @PA_MC_Unposted_Retainer,
             @PA_MC_UnpostedServiceFee,
             @PA_MC_Actual_Project_Fee,
             @PA_MC_Actual_RetainerFee,
             @PA_MC_Actual_Service_Fee
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6183
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  else
    begin
      update PA01201
      set    PAprojname = @I_vPAprojname,
             PAprjclsid = @I_vPAprjclsid,
             PAProjectType = @I_vPAProjectType,
             PAAcctgMethod = @I_vPAAcctgMethod,
             PASTAT = @I_vPASTAT,
             PApurordnum = @I_vPApurordnum,
             PABBeginDate = @I_vPABBeginDate,
             PABEndDate = @I_vPABEndDate,
             PAcloseProjcosts = @I_vPAcloseProjcosts,
             PAclosetobillings = @I_vPAclosetobillings,
             PADepartment = @I_vPADepartment,
             PAEstimatorID = @I_vPAEstimatorID,
             PAprojmngrid = @I_vPAprojmngrid,
             PABusMgrID = @I_vPABusMgrID,
             PRBTADCD = @I_vPRBTADCD,
             PALabor_Rate_Table_ID = @I_vPALabor_Rate_Table_ID,
             PALabor_RateTable_Type = @I_vPALabor_RateTable_Type,
             PALabor_Rate_Table_Acc = @I_vPALabor_Rate_Table_Acc,
             PAEquip_Rate_Table_ID = @I_vPAEquip_Rate_Table_ID,
             PAEquip_Rate_Table_Acc = @I_vPAEquip_Rate_Table_Acc,
             PAbllngtype = @I_vPAbllngtype,
             PABILLFORMAT = @I_vPABILLFORMAT,
             DSCPCTAM = @I_vDSCPCTAM,
             PA_Retention_Percent = @I_vPA_Retention_Percent,
             PAUD1Proj = @I_vPAUD1Proj,
             PAUD2_Proj = @I_vPAUD2_Proj,
             PASegmentCB = @I_vPASegmentCB,
             WRKRCOMP = @I_vWRKRCOMP,
             SUTASTAT = @I_vSUTASTAT,
             PA_RestrictCustomerList = @I_vPA_RestrictCustomerList
      where  PAPROJNUMBER = @I_vPAPROJNUMBER
      
      if @@error <> 0
        begin
          select @O_iErrorState = 6443
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  exec @iStatus = taPAProjectsPost
    @I_vCUSTNMBR ,
    @I_vPACONTNUMBER ,
    @I_vPAprojid ,
    @I_vPAprojname ,
    @I_vPAPROJNUMBER ,
    @I_vPAProjectType ,
    @I_vPAAcctgMethod ,
    @I_vPASTAT ,
    @I_vPABBeginDate ,
    @I_vPABEndDate ,
    @I_vPAprjclsid ,
    @I_vPApurordnum ,
    @I_vPAcloseProjcosts ,
    @I_vPAclosetobillings ,
    @I_vPADepartment ,
    @I_vPAEstimatorID ,
    @I_vPAprojmngrid ,
    @I_vPABusMgrID ,
    @I_vPRBTADCD ,
    @I_vPALabor_Rate_Table_ID ,
    @I_vPALabor_RateTable_Type ,
    @I_vPALabor_Rate_Table_Acc ,
    @I_vPAEquip_Rate_Table_ID ,
    @I_vPAEquip_Rate_Table_Acc ,
    @I_vPAbllngtype ,
    @I_vPABILLFORMAT ,
    @I_vDSCPCTAM ,
    @I_vPA_Retention_Percent ,
    @I_vPAUD1Proj ,
    @I_vPAUD2_Proj ,
    @I_vPASegmentCB ,
    @I_vPAbillnoteDefault ,
    @I_vPAbillnoteTS ,
    @I_vPAbillnoteEL ,
    @I_vPAbillnoteML ,
    @I_vPAbillnoteVI ,
    @I_vPAbillnoteEE ,
    @I_vPAbillnoteINV ,
    @I_vPAbillnoteFEE ,
    @I_vPABILLCYCLEID1 ,
    @I_vPA_RestrictCustomerList ,
    @I_vWRKRCOMP ,
    @I_vSUTASTAT ,
    @I_vPATRKCHGORDS ,
    @I_vPATRKCOBDGADDFLY ,
    @I_vUpdateExisting ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if @iStatus = 0
     and @iError <> 0
    begin
      select @iStatus = @iError
    end
  
  if (@iStatus <> 0)
      or (@iCustomState <> 0)
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 6184
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vRequesterTrx = 0)
    begin
      exec @iStatus = eConnectOutVerify
        @I_vDOCTYPE = 'Project_Projects' ,
        @I_vINDEX1 = @I_vPAPROJNUMBER ,
        @I_vINDEX2 = '' ,
        @I_vINDEX3 = '' ,
        @I_vINDEX4 = '' ,
        @I_vINDEX5 = '' ,
        @I_vINDEX6 = '' ,
        @I_vINDEX7 = '' ,
        @I_vINDEX8 = '' ,
        @I_vINDEX9 = '' ,
        @I_vINDEX10 = '' ,
        @I_vINDEX11 = '' ,
        @I_vINDEX12 = '' ,
        @I_vINDEX13 = '' ,
        @I_vINDEX14 = '' ,
        @I_vINDEX15 = '' ,
        @I_vDelete = 1 ,
        @O_iErrorState = @iCustomState output
      
      select @iError = @@error
      
      if @iStatus = 0
         and @iError <> 0
        begin
          select @iStatus = @iError
        end
      
      if (@iStatus <> 0)
          or (@iCustomState <> 0)
        begin
          select @O_iErrorState = 6185
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
          
          return (@O_iErrorState)
        end
    end
  
  return (@O_iErrorState)

