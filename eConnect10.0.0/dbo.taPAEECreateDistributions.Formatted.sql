

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAEECreateDistributions]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAEECreateDistributions]
  
create procedure [dbo].[taPAEECreateDistributions]
                @I_vCURNCYID      char(15),
                @I_vCURRNIDX      smallint,
                @I_vPAerdocnumber char(17),
                @I_vEMPLOYID      char(15),
                @I_vPAProjectType smallint,
                @I_vPAAcctgMethod smallint,
                @I_vCHAMCBID      char(15)  = '',
                @I_vFRTAMNT       numeric(19,5)  = 0,
                @I_vMSCCHAMT      numeric(19,5)  = 0,
                @I_vCASHAMNT      numeric(19,5)  = 0,
                @I_vCHEKAMNT      numeric(19,5)  = 0,
                @I_vCRCRDAMT      numeric(19,5)  = 0,
                @I_vORCASHAMNT    numeric(19,5)  = 0,
                @I_vORCHEKAMNT    numeric(19,5)  = 0,
                @I_vORCRCRDAMT    numeric(19,5)  = 0,
                @I_vOMISCAMT      numeric(19,5)  = 0,
                @I_vORFRTAMT      numeric(19,5)  = 0,
                @I_vCARDNAME      char(15)  = '',
                @I_vCAMCBKID      char(15)  = '',
                @O_iErrorState    int  = NULL output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @O_oErrorState int,
           @cDTYPE        int,
           @cDTAMT        numeric(19,5),
           @cCTAMT        numeric(19,5),
           @iCursorError  int,
           @cINDEX        int,
           @cStatement    varchar(255),
           @iStatus       int,
           @iError        int,
           @ORDTAMT       numeric(19,5),
           @ORCTAMT       numeric(19,5),
           @ACTINDX       int,
           @PTCSHACF      smallint,
           @CKBKNUM2      char(15),
           @PYBLGRBX      smallint,
           @VENDORID      char(15),
           @ISMCREG       tinyint,
           @MINLNITMSEQ   int
  
  select @O_oErrorState = 0,
         @cDTYPE = 0,
         @cDTAMT = 0,
         @cCTAMT = 0,
         @iCursorError = 0,
         @cINDEX = 0,
         @cStatement = '',
         @iStatus = 0,
         @iError = 0,
         @ORDTAMT = 0,
         @ORCTAMT = 0,
         @ACTINDX = 0,
         @PTCSHACF = 0,
         @CKBKNUM2 = '',
         @PYBLGRBX = 0,
         @VENDORID = '',
         @ISMCREG = 0,
         @O_iErrorState = 0,
         @MINLNITMSEQ = 0
  
  if (exists (select 1
              from   CM00100 (nolock)
              where  CURNCYID <> ''))
    begin
      select @ISMCREG = 1
    end
  
  if (@I_vCURNCYID is NULL 
       or @I_vCURRNIDX is NULL
       or @I_vPAerdocnumber is NULL
       or @I_vEMPLOYID is NULL)
    begin
      select @O_iErrorState = 1802
      
      return (@O_iErrorState)
    end
  
  if (@I_vCURNCYID = ''
       or @I_vCURRNIDX = 0
       or @I_vPAerdocnumber = ''
       or @I_vEMPLOYID = '')
    begin
      select @O_iErrorState = 1803
      
      return (@O_iErrorState)
    end
  
  select @I_vCURNCYID = UPPER(@I_vCURNCYID),
         @I_vPAerdocnumber = UPPER(@I_vPAerdocnumber),
         @I_vEMPLOYID = UPPER(@I_vEMPLOYID)
  
  delete PA10504
  where  PAerdocnumber = @I_vPAerdocnumber
  
  if (@@error <> 0)
    begin
      select @O_iErrorState = 1804
      
      return (@O_iErrorState)
    end
  
  create table #temp (
    DTYPE   int,
    DTAMT   numeric(19,5),
    CTAMT   numeric(19,5),
    ORDTAMT numeric(19,5),
    ORCTAMT numeric(19,5),
    DTINDEX int)
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select   6,
           case 
             when ((isnull(sum(PAReimbursableAmount),0) - isnull(sum(BCKTXAMT),0)) - isnull(sum(TAXAMNT),0) > 0) then ((isnull(sum(PAReimbursableAmount),0) - isnull(sum(BCKTXAMT),0)) - isnull(sum(TAXAMNT),0))
             else 0
           end,
           case 
             when ((isnull(sum(PAReimbursableAmount),0) - isnull(sum(BCKTXAMT),0)) - isnull(sum(TAXAMNT),0) < 0) then ((isnull(sum(PAReimbursableAmount),0) - isnull(sum(BCKTXAMT),0)) - isnull(sum(TAXAMNT),0)) * -1
             else 0
           end,
           case 
             when ((isnull(sum(PAOrigReimbursableAmt),0) - isnull(sum(OBTAXAMT),0)) - isnull(sum(ORTAXAMT),0) > 0) then ((isnull(sum(PAOrigReimbursableAmt),0) - isnull(sum(OBTAXAMT),0)) - isnull(sum(ORTAXAMT),0))
             else 0
           end,
           case 
             when ((isnull(sum(PAOrigReimbursableAmt),0) - isnull(sum(OBTAXAMT),0)) - isnull(sum(ORTAXAMT),0) < 0) then ((isnull(sum(PAOrigReimbursableAmt),0) - isnull(sum(OBTAXAMT),0)) - isnull(sum(ORTAXAMT),0)) * -1
             else 0
           end,
           PACogs_Idx
  from     PA10501 (nolock)
  where    PAerdocnumber = @I_vPAerdocnumber
           and PACogs_Idx <> 0
  group by PACogs_Idx
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select   6,
           case 
             when ((isnull(sum(PAReimbursableAmount),0) - isnull(sum(BCKTXAMT),0)) - isnull(sum(TAXAMNT),0) > 0) then ((isnull(sum(PAReimbursableAmount),0) - isnull(sum(BCKTXAMT),0)) - isnull(sum(TAXAMNT),0))
             else 0
           end,
           case 
             when ((isnull(sum(PAReimbursableAmount),0) - isnull(sum(BCKTXAMT),0)) - isnull(sum(TAXAMNT),0) < 0) then ((isnull(sum(PAReimbursableAmount),0) - isnull(sum(BCKTXAMT),0)) - isnull(sum(TAXAMNT),0)) * -1
             else 0
           end,
           case 
             when ((isnull(sum(PAOrigReimbursableAmt),0) - isnull(sum(OBTAXAMT),0)) - isnull(sum(ORTAXAMT),0) > 0) then ((isnull(sum(PAOrigReimbursableAmt),0) - isnull(sum(OBTAXAMT),0)) - isnull(sum(ORTAXAMT),0))
             else 0
           end,
           case 
             when ((isnull(sum(PAOrigReimbursableAmt),0) - isnull(sum(OBTAXAMT),0)) - isnull(sum(ORTAXAMT),0) < 0) then ((isnull(sum(PAOrigReimbursableAmt),0) - isnull(sum(OBTAXAMT),0)) - isnull(sum(ORTAXAMT),0)) * -1
             else 0
           end,
           PACGBWIPIDX
  from     PA10501 (nolock)
  where    PAerdocnumber = @I_vPAerdocnumber
           and PACGBWIPIDX <> 0
  group by PACGBWIPIDX
  
  select @MINLNITMSEQ = min(LNITMSEQ)
  from   PA10501 (nolock)
  where  PAerdocnumber = @I_vPAerdocnumber
         and PAContra_Account_IDX <> 0
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select   2,
           case 
             when ((isnull(sum(PAReimbursableAmount),0) + @I_vFRTAMNT + @I_vMSCCHAMT - @I_vCASHAMNT - @I_vCHEKAMNT - @I_vCRCRDAMT) < 0) then ((isnull(sum(PAReimbursableAmount),0) + @I_vFRTAMNT + @I_vMSCCHAMT - @I_vCASHAMNT - @I_vCHEKAMNT - @I_vCRCRDAMT)) * -1
             else 0
           end,
           case 
             when ((isnull(sum(PAReimbursableAmount),0) + @I_vFRTAMNT + @I_vMSCCHAMT - @I_vCASHAMNT - @I_vCHEKAMNT - @I_vCRCRDAMT) > 0) then ((isnull(sum(PAReimbursableAmount),0) + @I_vFRTAMNT + @I_vMSCCHAMT - @I_vCASHAMNT - @I_vCHEKAMNT - @I_vCRCRDAMT))
             else 0
           end,
           case 
             when ((isnull(sum(PAOrigReimbursableAmt),0) + @I_vORFRTAMT + @I_vOMISCAMT - @I_vORCASHAMNT - @I_vORCHEKAMNT - @I_vORCRCRDAMT) < 0) then ((isnull(sum(PAOrigReimbursableAmt),0) + @I_vORFRTAMT + @I_vOMISCAMT - @I_vORCASHAMNT - @I_vORCHEKAMNT - @I_vORCRCRDAMT)) * -1
             else 0
           end,
           case 
             when ((isnull(sum(PAOrigReimbursableAmt),0) + @I_vORFRTAMT + @I_vOMISCAMT - @I_vORCASHAMNT - @I_vORCHEKAMNT - @I_vORCRCRDAMT) > 0) then ((isnull(sum(PAOrigReimbursableAmt),0) + @I_vORFRTAMT + @I_vOMISCAMT - @I_vORCASHAMNT - @I_vORCHEKAMNT - @I_vORCRCRDAMT))
             else 0
           end,
           PAContra_Account_IDX
  from     PA10501 (nolock)
  where    PAerdocnumber = @I_vPAerdocnumber
           and PAContra_Account_IDX <> 0
           and LNITMSEQ = @MINLNITMSEQ
  group by PAContra_Account_IDX
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select   2,
           case 
             when ((isnull(sum(PAReimbursableAmount),0) + @I_vFRTAMNT + @I_vMSCCHAMT - @I_vCASHAMNT - @I_vCHEKAMNT - @I_vCRCRDAMT) < 0) then ((isnull(sum(PAReimbursableAmount),0) + @I_vFRTAMNT + @I_vMSCCHAMT - @I_vCASHAMNT - @I_vCHEKAMNT - @I_vCRCRDAMT)) * -1
             else 0
           end,
           case 
             when ((isnull(sum(PAReimbursableAmount),0)) > 0) then (isnull(sum(PAReimbursableAmount),0))
             else 0
           end,
           case 
             when ((isnull(sum(PAOrigReimbursableAmt),0) + @I_vORFRTAMT + @I_vOMISCAMT - @I_vORCASHAMNT - @I_vORCHEKAMNT - @I_vORCRCRDAMT) < 0) then ((isnull(sum(PAOrigReimbursableAmt),0) + @I_vORFRTAMT + @I_vOMISCAMT - @I_vORCASHAMNT - @I_vORCHEKAMNT - @I_vORCRCRDAMT)) * -1
             else 0
           end,
           case 
             when ((isnull(sum(PAOrigReimbursableAmt),0)) > 0) then (isnull(sum(PAOrigReimbursableAmt),0))
             else 0
           end,
           PAContra_Account_IDX
  from     PA10501 (nolock)
  where    PAerdocnumber = @I_vPAerdocnumber
           and PAContra_Account_IDX <> 0
           and LNITMSEQ <> @MINLNITMSEQ
  group by PAContra_Account_IDX
  
  if (@ISMCREG = 1)
    begin
      insert #temp
            (DTYPE,
             DTAMT,
             CTAMT,
             ORDTAMT,
             ORCTAMT,
             DTINDEX)
      select   18,
               case 
                 when (isnull(sum(PAACREV),0) < 0) then (isnull(sum(PAACREV),0) * -1)
                 else 0
               end,
               case 
                 when (isnull(sum(PAACREV),0) > 0) then (isnull(sum(PAACREV),0))
                 else 0
               end,
               case 
                 when (isnull(sum(PAORIACCRREV),0) < 0) then (isnull(sum(PAORIACCRREV),0) * -1)
                 else 0
               end,
               case 
                 when (isnull(sum(PAORIACCRREV),0) > 0) then (isnull(sum(PAORIACCRREV),0))
                 else 0
               end,
               PAUnbilled_Proj_Rev_Idx
      from     PA10501 (nolock)
      where    PAerdocnumber = @I_vPAerdocnumber
               and PAUnbilled_Proj_Rev_Idx <> 0
      group by PAUnbilled_Proj_Rev_Idx
      
      insert #temp
            (DTYPE,
             DTAMT,
             CTAMT,
             ORDTAMT,
             ORCTAMT,
             DTINDEX)
      select   17,
               case 
                 when (isnull(sum(PAACREV),0) > 0) then (isnull(sum(PAACREV),0))
                 else 0
               end,
               case 
                 when (isnull(sum(PAACREV),0) < 0) then (isnull(sum(PAACREV),0) * -1)
                 else 0
               end,
               case 
                 when (isnull(sum(PAORIACCRREV),0) > 0) then (isnull(sum(PAORIACCRREV),0))
                 else 0
               end,
               case 
                 when (isnull(sum(PAORIACCRREV),0) < 0) then (isnull(sum(PAORIACCRREV),0) * -1)
                 else 0
               end,
               PAUnbilled_AR_Idx
      from     PA10501 (nolock)
      where    PAerdocnumber = @I_vPAerdocnumber
               and PAUnbilled_AR_Idx <> 0
      group by PAUnbilled_AR_Idx
    end
  else
    begin
      insert #temp
            (DTYPE,
             DTAMT,
             CTAMT,
             ORDTAMT,
             ORCTAMT,
             DTINDEX)
      select   17,
               case 
                 when (isnull(sum(PAACREV),0) < 0) then (isnull(sum(PAACREV),0) * -1)
                 else 0
               end,
               case 
                 when (isnull(sum(PAACREV),0) > 0) then (isnull(sum(PAACREV),0))
                 else 0
               end,
               case 
                 when (isnull(sum(PAORIACCRREV),0) < 0) then (isnull(sum(PAORIACCRREV),0) * -1)
                 else 0
               end,
               case 
                 when (isnull(sum(PAORIACCRREV),0) > 0) then (isnull(sum(PAORIACCRREV),0))
                 else 0
               end,
               PAUnbilled_Proj_Rev_Idx
      from     PA10501 (nolock)
      where    PAerdocnumber = @I_vPAerdocnumber
               and PAUnbilled_Proj_Rev_Idx <> 0
      group by PAUnbilled_Proj_Rev_Idx
      
      insert #temp
            (DTYPE,
             DTAMT,
             CTAMT,
             ORDTAMT,
             ORCTAMT,
             DTINDEX)
      select   16,
               case 
                 when (isnull(sum(PAACREV),0) > 0) then (isnull(sum(PAACREV),0))
                 else 0
               end,
               case 
                 when (isnull(sum(PAACREV),0) < 0) then (isnull(sum(PAACREV),0) * -1)
                 else 0
               end,
               case 
                 when (isnull(sum(PAORIACCRREV),0) > 0) then (isnull(sum(PAORIACCRREV),0))
                 else 0
               end,
               case 
                 when (isnull(sum(PAORIACCRREV),0) < 0) then (isnull(sum(PAORIACCRREV),0) * -1)
                 else 0
               end,
               PAUnbilled_AR_Idx
      from     PA10501 (nolock)
      where    PAerdocnumber = @I_vPAerdocnumber
               and PAUnbilled_AR_Idx <> 0
      group by PAUnbilled_AR_Idx
    end
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select   10,
           isnull(sum(TAXAMNT),0),
           0,
           isnull(sum(TAXAMNT),0),
           0,
           ACTINDX
  from     PA10502 (nolock)
  where    PAerdocnumber = @I_vPAerdocnumber
           and LNITMSEQ <> 0
  group by ACTINDX
  
  select @ACTINDX = 0
  
  select @ACTINDX = isnull(PMFRTIDX,0)
  from   PM00200 (nolock)
  where  VENDORID = @I_vEMPLOYID
  
  if (@ACTINDX = 0)
    begin
      select @ACTINDX = isnull(ACTINDX,0)
      from   SY01100 (nolock)
      where  SERIES = 4
             and SEQNUMBR = 900
    end
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select 9,
         @I_vFRTAMNT,
         0,
         @I_vORFRTAMT,
         0,
         @ACTINDX
  
  select @ACTINDX = 0
  
  select @ACTINDX = isnull(PMMSCHIX,0)
  from   PM00200 (nolock)
  where  VENDORID = @I_vEMPLOYID
  
  if (@ACTINDX = 0)
    begin
      select @ACTINDX = isnull(ACTINDX,0)
      from   SY01100 (nolock)
      where  SERIES = 4
             and SEQNUMBR = 800
    end
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select 8,
         @I_vMSCCHAMT,
         0,
         @I_vOMISCAMT,
         0,
         @ACTINDX
  
  select @ACTINDX = 0
  
  select @PTCSHACF = PTCSHACF
  from   PM00200 (nolock)
  where  VENDORID = @I_vEMPLOYID
  
  if (@PTCSHACF = 1)
    begin
      select @ACTINDX = isnull(PMCSHIDX,0)
      from   PM00200 (nolock)
      where  VENDORID = @I_vEMPLOYID
    end
  else
    begin
      select @ACTINDX = isnull(ACTINDX,0)
      from   CM00100 (nolock)
      where  CHEKBKID = @I_vCAMCBKID
    end
  
  if (@ACTINDX = 0)
    begin
      select @ACTINDX = isnull(ACTINDX,0)
      from   SY01100 (nolock)
      where  SERIES = 4
             and SEQNUMBR = 100
    end
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select 1,
         0,
         @I_vCASHAMNT,
         0,
         @I_vORCASHAMNT,
         @ACTINDX
  
  select @ACTINDX = 0
  
  if (@PTCSHACF = 1)
    begin
      select @ACTINDX = isnull(PMCSHIDX,0)
      from   PM00200 (nolock)
      where  VENDORID = @I_vEMPLOYID
    end
  else
    begin
      select @ACTINDX = isnull(ACTINDX,0)
      from   CM00100 (nolock)
      where  CHEKBKID = @I_vCHAMCBID
    end
  
  if (@ACTINDX = 0)
    begin
      select @ACTINDX = isnull(ACTINDX,0)
      from   SY01100 (nolock)
      where  SERIES = 4
             and SEQNUMBR = 100
    end
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select 1,
         0,
         @I_vCHEKAMNT,
         0,
         @I_vORCHEKAMNT,
         @ACTINDX
  
  select @ACTINDX = 0
  
  select @ACTINDX = isnull(ACTINDX,0),
         @PYBLGRBX = PYBLGRBX,
         @CKBKNUM2 = CKBKNUM2,
         @VENDORID = VENDORID
  from   SY03100 (nolock)
  where  CARDNAME = @I_vCARDNAME
  
  if (@PYBLGRBX = 1)
    begin
      select @ACTINDX = isnull(ACTINDX,0)
      from   CM00100 (nolock)
      where  CHEKBKID = @CKBKNUM2
    end
  else
    begin
      select @ACTINDX = isnull(PMAPINDX,0)
      from   PM00200 (nolock)
      where  VENDORID = @VENDORID
    end
  
  if (@ACTINDX = 0)
    begin
      select @ACTINDX = isnull(ACTINDX,0)
      from   SY01100 (nolock)
      where  SERIES = 4
             and SEQNUMBR = 100
    end
  
  insert #temp
        (DTYPE,
         DTAMT,
         CTAMT,
         ORDTAMT,
         ORCTAMT,
         DTINDEX)
  select 1,
         0,
         @I_vCRCRDAMT,
         0,
         @I_vORCRCRDAMT,
         @ACTINDX
  
  delete #temp
  where  DTAMT + CTAMT = 0.00
  
  declare DistLine INSENSITIVE cursor  for
  select   DTYPE,
           sum(DTAMT),
           sum(CTAMT),
           sum(ORDTAMT),
           sum(ORCTAMT),
           DTINDEX
  from     #temp
  group by DTYPE,DTINDEX
  order by DTYPE
  
  open DistLine
  
  select @iCursorError = @@cursor_rows
  
  if (@iCursorError > 0)
    begin
      fetch next from DistLine
      into @cDTYPE,
           @cDTAMT,
           @cCTAMT,
           @ORDTAMT,
           @ORCTAMT,
           @cINDEX
      
      while (@@fetch_status <> -1)
        begin
          if (@@fetch_status = -2)
            begin
              select @O_iErrorState = 1805
              
              break
            end
          
          if (@cDTYPE <> 1)
            begin
              select @I_vCURNCYID = '',
                     @I_vCURRNIDX = 0,
                     @I_vEMPLOYID = ''
            end
          
          exec @iStatus = taPAEEDistributionInsert
            @I_vCURNCYID ,
            @I_vCURRNIDX ,
            @ORCTAMT ,
            @ORDTAMT ,
            @I_vPAerdocnumber ,
            '' ,
            0 ,
            @cCTAMT ,
            @cDTAMT ,
            @cINDEX ,
            @cDTYPE ,
            '' ,
            '' ,
            @I_vEMPLOYID ,
            @O_oErrorState output
          
          select @iError = @@error
          
          if ((@iStatus = 0)
              and (@iError <> 0))
            begin
              select @iStatus = @iError
            end
          
          if ((@iStatus <> 0)
               or (@O_oErrorState <> 0))
            begin
              if (@iStatus <> 0)
                begin
                  select @O_iErrorState = 1806
                end
              else
                begin
                  select @O_iErrorState = @O_oErrorState
                end
              
              deallocate DistLine
              
              return (@O_iErrorState)
            end
          
          fetch next from DistLine
          into @cDTYPE,
               @cDTAMT,
               @cCTAMT,
               @ORDTAMT,
               @ORCTAMT,
               @cINDEX
        end
    end
  
  deallocate DistLine
  
  return (@O_iErrorState)

