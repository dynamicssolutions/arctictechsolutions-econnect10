

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taProcessAnalytics]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taProcessAnalytics]
  
create procedure [dbo].[taProcessAnalytics]
                @I_vDOCNMBR    varchar(50),
                @I_vDOCTYPE    smallint,
                @I_vKey        varchar(50),
                @O_iErrorState int  output,
                @oErrString    varchar(255)  output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iError                       int,
           @O_oErrorState                int,
           @iCursorError                 int,
           @iStatus                      int,
           @iDOCNMBR                     varchar(50),
           @iDOCTYPE                     smallint,
           @iAMOUNT                      numeric(19,5),
           @iaaAssignedPercent           numeric(5,2),
           @iDistSequence                int,
           @iACTNUMST                    varchar(75),
           @iACTINDX                     int,
           @iDistRef                     char(31),
           @iNOTETEXT                    varchar(8000),
           @iaaTrxDim                    char(31),
           @iaaTrxDimCode                char(31),
           @iaaTrxDimCodeNum             numeric(19,5),
           @iaaTrxDimCodeBool            smallint,
           @iaaTrxDimCodeDate            datetime,
           @iaaTrxDimID                  int,
           @iaaTrxCodeID                 int,
           @iaaSubLedgerHdrID            int,
           @iaaSubLedgerDistID           int,
           @iaaSubLedgerAssignID         int,
           @iUpdateIfExists              smallint,
           @iRequesterTrx                smallint,
           @iUSRDEFND1                   char(50),
           @iUSRDEFND2                   char(50),
           @iUSRDEFND3                   char(50),
           @iUSRDEFND4                   varchar(8000),
           @iUSRDEFND5                   varchar(8000),
           @itaProcessAnalyticsErrState  int,
           @itaProcessAnalyticsErrString varchar(8000),
           @iRowID                       int
  
  select @itaProcessAnalyticsErrState = 0,
         @itaProcessAnalyticsErrString = '',
         @iRowID = 0
  
  select @O_iErrorState = 0
  
  if (@oErrString is null)
    begin
      select @oErrString = ''
    end
  
  if (@I_vDOCNMBR is null 
       or @I_vDOCTYPE is null)
    begin
      select @O_iErrorState = 9427
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if exists (select 1
             from   tempdb..sysobjects
             where  name = '##taAnalyticsDistribution')
    begin
      select @iRowID = max(RowID)
      from   ##taAnalyticsDistribution (nolock)
      
      declare taAnalyticsDistribution insensitive CURSOR  for
      select   DOCNMBR,
               DOCTYPE,
               AMOUNT,
               aaAssignedPercent,
               DistSequence,
               ACTNUMST,
               ACTINDX,
               DistRef,
               NOTETEXT,
               aaTrxDim,
               aaTrxDimCode,
               aaTrxDimCodeNum,
               aaTrxDimCodeBool,
               aaTrxDimCodeDate,
               aaTrxDimID,
               aaTrxCodeID,
               aaSubLedgerHdrID,
               aaSubLedgerDistID,
               aaSubLedgerAssignID,
               UpdateIfExists,
               RequesterTrx,
               USRDEFND1,
               USRDEFND2,
               USRDEFND3,
               USRDEFND4,
               USRDEFND5
      from     ##taAnalyticsDistribution a,
               ##taAnalyticsDistributionNOTETEXT b,
               ##taAnalyticsDistributionUSRDEFND4 c,
               ##taAnalyticsDistributionUSRDEFND5 d
      where    a.uKey = @I_vKey
               and a.Spid = @@Spid
               and a.RowID = b.RowID
               and b.RowID = c.RowID
               and c.RowID = d.RowID
      order by a.RowID
      
      set nocount on
      
      OPEN taAnalyticsDistribution
      
      select @iCursorError = @@cursor_rows
      
      if @iCursorError = 0
        begin
          deallocate taAnalyticsDistribution
        end
      else
        begin
          FETCH NEXT FROM taAnalyticsDistribution
          INTO @iDOCNMBR,
               @iDOCTYPE,
               @iAMOUNT,
               @iaaAssignedPercent,
               @iDistSequence,
               @iACTNUMST,
               @iACTINDX,
               @iDistRef,
               @iNOTETEXT,
               @iaaTrxDim,
               @iaaTrxDimCode,
               @iaaTrxDimCodeNum,
               @iaaTrxDimCodeBool,
               @iaaTrxDimCodeDate,
               @iaaTrxDimID,
               @iaaTrxCodeID,
               @iaaSubLedgerHdrID,
               @iaaSubLedgerDistID,
               @iaaSubLedgerAssignID,
               @iUpdateIfExists,
               @iRequesterTrx,
               @iUSRDEFND1,
               @iUSRDEFND2,
               @iUSRDEFND3,
               @iUSRDEFND4,
               @iUSRDEFND5
          
          WHILE (@@FETCH_STATUS <> -1)
            begin
              if (@@fetch_status = -2)
                begin
                  select @O_iErrorState = 9397
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
              
              exec @iStatus = taAnalyticsDistribution
                @I_vDOCNMBR ,
                @iDOCTYPE ,
                @iAMOUNT ,
                @iaaAssignedPercent ,
                @iDistSequence ,
                @iACTNUMST ,
                @iACTINDX ,
                @iDistRef ,
                @iNOTETEXT ,
                @iaaTrxDim ,
                @iaaTrxDimCode ,
                @iaaTrxDimCodeNum ,
                @iaaTrxDimCodeBool ,
                @iaaTrxDimCodeDate ,
                @iaaTrxDimID ,
                @iaaTrxCodeID ,
                @iaaSubLedgerHdrID ,
                @iaaSubLedgerDistID ,
                @iaaSubLedgerAssignID ,
                @iUpdateIfExists ,
                @iRequesterTrx ,
                @iUSRDEFND1 ,
                @iUSRDEFND2 ,
                @iUSRDEFND3 ,
                @iUSRDEFND4 ,
                @iUSRDEFND5 ,
                @itaProcessAnalyticsErrState output ,
                @itaProcessAnalyticsErrString output
              
              select @iError = @@error
              
              if ((@iStatus <> 0)
                   or (@itaProcessAnalyticsErrState <> 0)
                   or (@iError <> 0))
                begin
                  select @oErrString = rtrim(@oErrString) + ' ' + @itaProcessAnalyticsErrString
                  
                  select @O_iErrorState = 9398
                  
                  exec @iStatus = taUpdateString
                    @O_iErrorState ,
                    @oErrString ,
                    @oErrString output ,
                    @O_oErrorState output
                  
                  break
                end
              
              FETCH NEXT from taAnalyticsDistribution
              INTO @iDOCNMBR,
                   @iDOCTYPE,
                   @iAMOUNT,
                   @iaaAssignedPercent,
                   @iDistSequence,
                   @iACTNUMST,
                   @iACTINDX,
                   @iDistRef,
                   @iNOTETEXT,
                   @iaaTrxDim,
                   @iaaTrxDimCode,
                   @iaaTrxDimCodeNum,
                   @iaaTrxDimCodeBool,
                   @iaaTrxDimCodeDate,
                   @iaaTrxDimID,
                   @iaaTrxCodeID,
                   @iaaSubLedgerHdrID,
                   @iaaSubLedgerDistID,
                   @iaaSubLedgerAssignID,
                   @iUpdateIfExists,
                   @iRequesterTrx,
                   @iUSRDEFND1,
                   @iUSRDEFND2,
                   @iUSRDEFND3,
                   @iUSRDEFND4,
                   @iUSRDEFND5
            end
          
          deallocate taAnalyticsDistribution
        end
    end
  
  return (@O_iErrorState)

