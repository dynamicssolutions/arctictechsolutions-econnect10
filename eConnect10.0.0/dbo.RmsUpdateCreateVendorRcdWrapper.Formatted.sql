

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsUpdateCreateVendorRcdWrapper]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsUpdateCreateVendorRcdWrapper]
  
create procedure [dbo].[RmsUpdateCreateVendorRcdWrapper]
                @iVendorID    char(15),
                @iVendName    char(64),
                @iVendClass   char(10),
                @iAddressCode char(15),
                @iVendContact char(60),
                @iAddress1    char(60),
                @iAddress2    char(60),
                @iCity        char(35),
                @iState       char(29),
                @iZip         char(10),
                @iCountry     char(60),
                @iPhone1      char(14),
                @iFax         char(14),
                @oErrorState  int  output,
                @oErrString   varchar(255)  output   /*with encryption*/
AS
  EXEC RmsUpdateCreateVendorRcd
    @I_vVENDORID = @iVendorID ,
    @I_vVENDNAME = @iVendName ,
    @I_vVNDCHKNM = @iVendName ,
    @I_vVNDCLSID = @iVendClass ,
    @I_vVADDCDPR = @iAddressCode ,
    @I_vVADCDPAD = @iAddressCode ,
    @I_vVADCDSFR = @iAddressCode ,
    @I_vVADCDTRO = @iAddressCode ,
    @I_vVNDCNTCT = @iVendContact ,
    @I_vADDRESS1 = @iAddress1 ,
    @I_vADDRESS2 = @iAddress2 ,
    @I_vCITY = @iCity ,
    @I_vSTATE = @iState ,
    @I_vZIPCODE = @iZip ,
    @I_vCOUNTRY = @iCountry ,
    @I_vPHNUMBR1 = @iPhone1 ,
    @I_vFAXNUMBR = @iFax ,
    @I_vUseVendorClass = 1 ,
    @O_iErrorState = @oErrorState OUTPUT ,
    @oErrString = @oErrString OUTPUT

