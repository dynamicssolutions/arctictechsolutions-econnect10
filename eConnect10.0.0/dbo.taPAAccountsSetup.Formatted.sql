

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAAccountsSetup]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAAccountsSetup]
  
create procedure [dbo].[taPAAccountsSetup]
                @I_vPAsfid       smallint,
                @I_vPArecordid   char(15),
                @I_vPAcosttrxid  char(17)  = '',
                @I_vPAaccttype   smallint,
                @I_vPAAccount    varchar(75),
                @I_vRequesterTrx smallint  = 0,
                @I_vUSRDEFND1    char(50)  = '',
                @I_vUSRDEFND2    char(50)  = '',
                @I_vUSRDEFND3    char(50)  = '',
                @I_vUSRDEFND4    varchar(8000)  = '',
                @I_vUSRDEFND5    varchar(8000)  = '',
                @O_iErrorState   int  = NULL output,
                @oErrString      varchar(255)  output    /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iCustomState     int,
           @iCustomErrString varchar(255),
           @iError           int,
           @iCursorError     int,
           @O_oErrorState    int,
           @iStatus          int,
           @PACONTNUMBER     char(17),
           @PAPROJNUMBER     char(17),
           @PAProjectType    smallint,
           @PAACTINDX        int
  
  select @iCustomState = 0,
         @iCustomErrString = '',
         @iCursorError = 0,
         @O_iErrorState = 0,
         @iStatus = 0,
         @iError = 0,
         @PAACTINDX = 0,
         @O_oErrorState = 0,
         @PACONTNUMBER = '',
         @PAPROJNUMBER = '',
         @PAProjectType = 0
  
  exec @iStatus = taPAAccountsSetupPre
    @I_vPAsfid output ,
    @I_vPArecordid output ,
    @I_vPAcosttrxid output ,
    @I_vPAaccttype output ,
    @I_vPAAccount output ,
    @I_vRequesterTrx output ,
    @I_vUSRDEFND1 output ,
    @I_vUSRDEFND2 output ,
    @I_vUSRDEFND3 output ,
    @I_vUSRDEFND4 output ,
    @I_vUSRDEFND5 output ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4942
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAsfid is null 
       or @I_vPArecordid is null
       or @I_vPAcosttrxid is null
       or @I_vPAaccttype is null
       or @I_vPAAccount is null)
    begin
      select @O_iErrorState = 4978
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAsfid = 0
       or @I_vPArecordid = ''
       or @I_vPAaccttype = 0
       or @I_vPAAccount = '')
    begin
      select @O_iErrorState = 4979
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  if (@I_vPAAccount <> '')
    begin
      select @PAACTINDX = ACTINDX
      from   GL00105 (nolock)
      where  ACTNUMST = @I_vPAAccount
      
      select @PAACTINDX = isnull(@PAACTINDX,0)
      
      if (@PAACTINDX = 0)
        begin
          select @O_iErrorState = 4980
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  
  if (@I_vPAsfid <> 27)
     and (@I_vPAsfid <> 28)
     and (@I_vPAsfid <> 34)
     and (@I_vPAsfid <> 35)
    begin
      select @O_iErrorState = 4981
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPAsfid = 27)
       or (@I_vPAsfid = 28))
    begin
      if (@I_vPAcosttrxid <> 'TS')
         and (@I_vPAcosttrxid <> 'EL')
         and (@I_vPAcosttrxid <> 'VI')
         and (@I_vPAcosttrxid <> 'ML')
         and (@I_vPAcosttrxid <> 'EE')
        begin
          select @O_iErrorState = 4982
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    begin
      select @I_vPAcosttrxid = ''
    end
  
  if ((@I_vPAsfid = 27)
       or (@I_vPAsfid = 28))
     and ((@I_vPAaccttype between 9 and 29)
           or (@I_vPAaccttype > 39))
    begin
      select @O_iErrorState = 5268
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if ((@I_vPAsfid = 34)
       or (@I_vPAsfid = 35))
     and (@I_vPAaccttype not in (6,7,9,30,
                                 32,33,35,37,
                                 38,49,10655))
    begin
      select @O_iErrorState = 5269
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
    end
  
  if (@I_vPAsfid = 27)
      or (@I_vPAsfid = 35)
    begin
      select @PACONTNUMBER = PACONTNUMBER
      from   PA01101 (nolock)
      where  PACONTNUMBER = @I_vPArecordid
      
      if (@PACONTNUMBER = '')
        begin
          select @O_iErrorState = 4983
          
          exec @iStatus = taUpdateString
            @O_iErrorState ,
            @oErrString ,
            @oErrString output ,
            @O_oErrorState output
        end
    end
  else
    if (@I_vPAsfid = 28)
        or (@I_vPAsfid = 34)
      begin
        select @PAPROJNUMBER = PAPROJNUMBER,
               @PAProjectType = PAProjectType
        from   PA01201 (nolock)
        where  PAPROJNUMBER = @I_vPArecordid
        
        if (@PAPROJNUMBER = '')
          begin
            select @O_iErrorState = 4985
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @O_oErrorState output
          end
        
        if (@PAProjectType = 1)
           and ((@I_vPAaccttype < 0)
                 or (@I_vPAaccttype > 9))
          begin
            select @O_iErrorState = 4986
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @O_oErrorState output
          end
        
        if (@PAProjectType <> 1)
           and ((@I_vPAaccttype < 30)
                 or (@I_vPAaccttype > 39))
          begin
            select @O_iErrorState = 4987
            
            exec @iStatus = taUpdateString
              @O_iErrorState ,
              @oErrString ,
              @oErrString output ,
              @O_oErrorState output
          end
      end
    else
      begin
        select @O_iErrorState = 4988
        
        exec @iStatus = taUpdateString
          @O_iErrorState ,
          @oErrString ,
          @oErrString output ,
          @O_oErrorState output
      end
  
  if (@O_iErrorState <> 0)
    return (@O_iErrorState)
  
  insert into PA43001
             (PAsfid,
              PArecordid,
              PAcosttrxid,
              PAaccttype,
              PAACTINDX)
  select @I_vPAsfid,
         @I_vPArecordid,
         @I_vPAcosttrxid,
         @I_vPAaccttype,
         @PAACTINDX
  
  if @@error <> 0
    begin
      select @O_iErrorState = 4989
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  exec @iStatus = taPAAccountsSetupPost
    @I_vPAsfid ,
    @I_vPArecordid ,
    @I_vPAcosttrxid ,
    @I_vPAaccttype ,
    @I_vPAAccount ,
    @I_vRequesterTrx ,
    @I_vUSRDEFND1 ,
    @I_vUSRDEFND2 ,
    @I_vUSRDEFND3 ,
    @I_vUSRDEFND4 ,
    @I_vUSRDEFND5 ,
    @O_iErrorState = @iCustomState output ,
    @oErrString = @iCustomErrString output
  
  select @iError = @@error
  
  if ((@iStatus <> 0)
       or (@iCustomState <> 0)
       or (@iError <> 0))
    begin
      select @oErrString = rtrim(@oErrString) + ' ' + @iCustomErrString
      
      select @oErrString = rtrim(@oErrString) + ' ' + ltrim(rtrim(@iCustomErrString))
      
      select @O_iErrorState = 4990
      
      exec @iStatus = taUpdateString
        @O_iErrorState ,
        @oErrString ,
        @oErrString output ,
        @O_oErrorState output
      
      return (@O_iErrorState)
    end
  
  return (@O_iErrorState)

