

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[taPAAuxAcctsGetIdx]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[taPAAuxAcctsGetIdx]
  
create procedure [dbo].[taPAAuxAcctsGetIdx]
                @I_vPAPROJNUMBER char(15),
                @I_vPACOSTCATID  char(15),
                @I_vEMPLOYID     char(15),
                @I_vPATU         int,
                @I_vPAcosttrxid  char(17),
                @I_vTYPE         int,
                @I_vCUSTNMBR     char(17),
                @I_vPACONTNUMBER char(11),
                @O_iINDEX        int  output,
                @O_iErrorState   int  = NULL output  /*with encryption*/
as
  set transaction isolation  level  read  uncommitted
  
  set nocount on
  
  declare  @iStatus          int,
           @iError           int,
           @AcctSelect       int,
           @PAsfid           int,
           @ACTINDX          int,
           @ACTIVE           int,
           @LOFSGMNTALL      int,
           @NEW_ACCNT_STRING varchar(100),
           @MAXSEG           int,
           @LOFSGMNTEND      int,
           @Location_Segment varchar(67),
           @ACSGFLOC         smallint,
           @ACCNT_STRING     varchar(100),
           @LOFSGMNT         int,
           @OverrideSegment  int,
           @iCursorError     int,
           @SGMTNUMB         int,
           @SGMTNAME         varchar(50),
           @SGMNTID          varchar(50),
           @OverideINDEX     int
  
  select @O_iErrorState = 0,
         @iStatus = 0,
         @O_iINDEX = 0,
         @iError = 0,
         @AcctSelect = 0,
         @PAsfid = 0,
         @ACTINDX = 0,
         @ACTIVE = 0,
         @LOFSGMNTALL = 0,
         @NEW_ACCNT_STRING = '',
         @MAXSEG = 0,
         @LOFSGMNTEND = 0,
         @Location_Segment = '',
         @ACSGFLOC = 0,
         @ACCNT_STRING = '',
         @LOFSGMNT = 0,
         @OverrideSegment = 0,
         @iCursorError = 0,
         @SGMTNUMB = 0,
         @SGMTNAME = '',
         @SGMNTID = '',
         @OverideINDEX = 0
  
  if (@I_vPAPROJNUMBER is NULL 
       or @I_vPACOSTCATID is NULL
       or @I_vEMPLOYID is NULL
       or @I_vPATU is NULL
       or @I_vTYPE is NULL
       or @I_vCUSTNMBR is NULL
       or @I_vPACONTNUMBER is NULL)
    begin
      select @O_iErrorState = 1800
      
      return @O_iErrorState
    end
  
  select @I_vPAPROJNUMBER = UPPER(@I_vPAPROJNUMBER),
         @I_vPACOSTCATID = UPPER(@I_vPACOSTCATID),
         @I_vEMPLOYID = UPPER(@I_vEMPLOYID),
         @I_vCUSTNMBR = UPPER(@I_vCUSTNMBR),
         @I_vPACONTNUMBER = UPPER(@I_vPACONTNUMBER)
  
  if @I_vPATU = 6
    select @I_vPATU = 4
  
  if ((@I_vPAPROJNUMBER = '<NONE>')
      and (@I_vPACOSTCATID = '<NONE>'))
    begin
      select @AcctSelect = 5
      
      if ((@I_vTYPE = 2)
           or (@I_vTYPE = 30))
        begin
          select @O_iINDEX = isnull(PMPRCHIX,0)
          from   PM00200 (nolock)
          where  VENDORID = @I_vEMPLOYID
          
          if @O_iINDEX = 0
            select @O_iINDEX = isnull(ACTINDX,0)
            from   SY01100 (nolock)
            where  SERIES = 4
                   and SEQNUMBR = 600
        end
      else
        if ((@I_vTYPE = 3)
             or (@I_vTYPE = 31))
          begin
            select @O_iINDEX = isnull(PMAPINDX,0)
            from   PM00200 (nolock)
            where  VENDORID = @I_vEMPLOYID
            
            if @O_iINDEX = 0
              select @O_iINDEX = isnull(ACTINDX,0)
              from   SY01100 (nolock)
              where  SERIES = 4
                     and SEQNUMBR = 200
          end
    end
  else
    begin
      if (@I_vPAPROJNUMBER = '<NONE>')
        begin
          select @AcctSelect = 5
        end
      else
        if (@I_vTYPE = 1)
          begin
            select @AcctSelect = isnull(TM_WIP_SRC,0)
            from   PA01301 (nolock)
            where  PAPROJNUMBER = @I_vPAPROJNUMBER
                   and PACOSTCATID = @I_vPACOSTCATID
                   and PATU = @I_vPATU
            
            select @OverrideSegment = PAOVERIDESEGTMWIP
            from   PA41301 (nolock)
            where  PAPROJNUMBER = @I_vPAPROJNUMBER
          end
        else
          if (@I_vTYPE = 2)
            begin
              select @AcctSelect = isnull(TM_COGS_SRC,0)
              from   PA01301 (nolock)
              where  PAPROJNUMBER = @I_vPAPROJNUMBER
                     and PACOSTCATID = @I_vPACOSTCATID
                     and PATU = @I_vPATU
              
              select @OverrideSegment = PAOVERIDESEGTMCOGS
              from   PA41301 (nolock)
              where  PAPROJNUMBER = @I_vPAPROJNUMBER
            end
          else
            if (@I_vTYPE = 3)
              begin
                select @AcctSelect = isnull(TM_Cost_Contra_SRC,0)
                from   PA01301 (nolock)
                where  PAPROJNUMBER = @I_vPAPROJNUMBER
                       and PACOSTCATID = @I_vPACOSTCATID
                       and PATU = @I_vPATU
                
                select @OverrideSegment = PAOVERIDESEGTMCONTRA
                from   PA41301 (nolock)
                where  PAPROJNUMBER = @I_vPAPROJNUMBER
              end
            else
              if (@I_vTYPE = 4)
                begin
                  select @AcctSelect = isnull(TM_Unbilled_AR_SRC,0)
                  from   PA01301 (nolock)
                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                         and PACOSTCATID = @I_vPACOSTCATID
                         and PATU = @I_vPATU
                  
                  select @OverrideSegment = PAOVERIDESEGTMUNBILLAR
                  from   PA41301 (nolock)
                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                end
              else
                if (@I_vTYPE = 5)
                  begin
                    select @AcctSelect = isnull(TM_Unbilled_Rev_SRC,0)
                    from   PA01301 (nolock)
                    where  PAPROJNUMBER = @I_vPAPROJNUMBER
                           and PACOSTCATID = @I_vPACOSTCATID
                           and PATU = @I_vPATU
                    
                    select @OverrideSegment = PASEGTMUNBILLREV
                    from   PA41301 (nolock)
                    where  PAPROJNUMBER = @I_vPAPROJNUMBER
                  end
                else
                  if (@I_vTYPE = 6)
                    begin
                      select @AcctSelect = isnull(TM_AR_SRC,0)
                      from   PA01301 (nolock)
                      where  PAPROJNUMBER = @I_vPAPROJNUMBER
                             and PACOSTCATID = @I_vPACOSTCATID
                             and PATU = @I_vPATU
                      
                      select @OverrideSegment = PASEGTMAR
                      from   PA41301 (nolock)
                      where  PAPROJNUMBER = @I_vPAPROJNUMBER
                    end
                  else
                    if (@I_vTYPE = 7)
                      begin
                        select @AcctSelect = isnull(TM_Prj_Rev_SRC,0)
                        from   PA01301 (nolock)
                        where  PAPROJNUMBER = @I_vPAPROJNUMBER
                               and PACOSTCATID = @I_vPACOSTCATID
                               and PATU = @I_vPATU
                        
                        select @OverrideSegment = PASEGTMPROJREV
                        from   PA41301 (nolock)
                        where  PAPROJNUMBER = @I_vPAPROJNUMBER
                      end
                    else
                      if (@I_vTYPE = 8)
                        begin
                          select @AcctSelect = isnull(TM_OVHD_SRC,0)
                          from   PA01301 (nolock)
                          where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                 and PACOSTCATID = @I_vPACOSTCATID
                                 and PATU = @I_vPATU
                          
                          select @OverrideSegment = PASEGTMOVERHEAD
                          from   PA41301 (nolock)
                          where  PAPROJNUMBER = @I_vPAPROJNUMBER
                        end
                      else
                        if (@I_vTYPE = 30)
                          begin
                            select @AcctSelect = isnull(FF_WIP_SRC,0)
                            from   PA01301 (nolock)
                            where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                   and PACOSTCATID = @I_vPACOSTCATID
                                   and PATU = @I_vPATU
                            
                            select @OverrideSegment = PASEGFPWIP
                            from   PA41301 (nolock)
                            where  PAPROJNUMBER = @I_vPAPROJNUMBER
                          end
                        else
                          if (@I_vTYPE = 31)
                            begin
                              select @AcctSelect = isnull(FF_Cost_Contra_SRC,0)
                              from   PA01301 (nolock)
                              where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                     and PACOSTCATID = @I_vPACOSTCATID
                                     and PATU = @I_vPATU
                              
                              select @OverrideSegment = PASEGFPCONTRA
                              from   PA41301 (nolock)
                              where  PAPROJNUMBER = @I_vPAPROJNUMBER
                            end
                          else
                            if (@I_vTYPE = 32)
                              begin
                                select @AcctSelect = isnull(FF_AR_SRC,0)
                                from   PA01301 (nolock)
                                where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                       and PACOSTCATID = @I_vPACOSTCATID
                                       and PATU = @I_vPATU
                                
                                select @OverrideSegment = PASEGFPAR
                                from   PA41301 (nolock)
                                where  PAPROJNUMBER = @I_vPAPROJNUMBER
                              end
                            else
                              if (@I_vTYPE = 33)
                                begin
                                  select @AcctSelect = isnull(FF_Prj_Billings_SRC,0)
                                  from   PA01301 (nolock)
                                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                         and PACOSTCATID = @I_vPACOSTCATID
                                         and PATU = @I_vPATU
                                  
                                  select @OverrideSegment = PASEGFPROJBILL
                                  from   PA41301 (nolock)
                                  where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                end
                              else
                                if (@I_vTYPE = 34)
                                  begin
                                    select @AcctSelect = isnull(FF_Prj_Expense_SRC,0)
                                    from   PA01301 (nolock)
                                    where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                           and PACOSTCATID = @I_vPACOSTCATID
                                           and PATU = @I_vPATU
                                    
                                    select @OverrideSegment = PASEGFPROJEXP
                                    from   PA41301 (nolock)
                                    where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                  end
                                else
                                  if (@I_vTYPE = 35)
                                    begin
                                      select @AcctSelect = isnull(FF_Prj_Revenue_SRC,0)
                                      from   PA01301 (nolock)
                                      where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                             and PACOSTCATID = @I_vPACOSTCATID
                                             and PATU = @I_vPATU
                                      
                                      select @OverrideSegment = PASEGFPROJREV
                                      from   PA41301 (nolock)
                                      where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                    end
                                  else
                                    if (@I_vTYPE = 36)
                                      begin
                                        select @AcctSelect = isnull(FF_Prj_Loss_SRC,0)
                                        from   PA01301 (nolock)
                                        where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                               and PACOSTCATID = @I_vPACOSTCATID
                                               and PATU = @I_vPATU
                                        
                                        select @OverrideSegment = PASEGFPROJLOSS
                                        from   PA41301 (nolock)
                                        where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                      end
                                    else
                                      if (@I_vTYPE = 37)
                                        begin
                                          select @AcctSelect = isnull(FF_BIEE_SRC,0)
                                          from   PA01301 (nolock)
                                          where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                                 and PACOSTCATID = @I_vPACOSTCATID
                                                 and PATU = @I_vPATU
                                          
                                          select @OverrideSegment = PASEGFPBIEE
                                          from   PA41301 (nolock)
                                          where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                        end
                                      else
                                        if (@I_vTYPE = 38)
                                          begin
                                            select @AcctSelect = isnull(FF_EIEB_SRC,0)
                                            from   PA01301 (nolock)
                                            where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                                   and PACOSTCATID = @I_vPACOSTCATID
                                                   and PATU = @I_vPATU
                                            
                                            select @OverrideSegment = PASEGFPEIEB
                                            from   PA41301 (nolock)
                                            where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                          end
                                        else
                                          if (@I_vTYPE = 39)
                                            begin
                                              select @AcctSelect = isnull(FF_OVHD_SRC,0)
                                              from   PA01301 (nolock)
                                              where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                                     and PACOSTCATID = @I_vPACOSTCATID
                                                     and PATU = @I_vPATU
                                              
                                              select @OverrideSegment = PASEGFPOVERHD
                                              from   PA41301 (nolock)
                                              where  PAPROJNUMBER = @I_vPAPROJNUMBER
                                            end
      
      if (@AcctSelect = 1)
        begin
          select @O_iINDEX = 0
        end
      else
        if (@AcctSelect = 2)
          begin
            select @O_iINDEX = isnull(PAACTINDX,0)
            from   PA43001 (nolock)
            where  PAsfid = 21
                   and PArecordid = @I_vCUSTNMBR
                   and PAcosttrxid = @I_vPAcosttrxid
                   and PAaccttype = @I_vTYPE
            
            if ((@O_iINDEX = 0)
                and (@I_vTYPE > 20))
              select @O_iINDEX = isnull(ACTINDX,0)
              from   SY01100 (nolock)
              where  SERIES = 7
                     and SEQNUMBR = (50 + ((@I_vTYPE - 30) * 3) + ((@I_vPATU - 1) * 100))
            
            if ((@O_iINDEX = 0)
                and (@I_vTYPE <= 20))
              select @O_iINDEX = isnull(ACTINDX,0)
              from   SY01100 (nolock)
              where  SERIES = 7
                     and SEQNUMBR = (10 + ((@I_vTYPE - 1) * 3) + ((@I_vPATU - 1) * 100))
          end
        else
          if (@AcctSelect = 3)
            begin
              select @O_iINDEX = isnull(PAACTINDX,0)
              from   PA43001 (nolock)
              where  PAsfid = 27
                     and PArecordid = @I_vPACONTNUMBER
                     and PAcosttrxid = @I_vPAcosttrxid
                     and PAaccttype = @I_vTYPE
              
              if ((@O_iINDEX = 0)
                  and (@I_vTYPE > 20))
                select @O_iINDEX = isnull(ACTINDX,0)
                from   SY01100 (nolock)
                where  SERIES = 7
                       and SEQNUMBR = (50 + ((@I_vTYPE - 30) * 3) + ((@I_vPATU - 1) * 100))
              
              if ((@O_iINDEX = 0)
                  and (@I_vTYPE <= 20))
                select @O_iINDEX = isnull(ACTINDX,0)
                from   SY01100 (nolock)
                where  SERIES = 7
                       and SEQNUMBR = (10 + ((@I_vTYPE - 1) * 3) + ((@I_vPATU - 1) * 100))
            end
          else
            if (@AcctSelect = 4)
              begin
                select @O_iINDEX = isnull(PAACTINDX,0)
                from   PA43001 (nolock)
                where  PAsfid = 28
                       and PArecordid = @I_vPAPROJNUMBER
                       and PAcosttrxid = @I_vPAcosttrxid
                       and PAaccttype = @I_vTYPE
                
                if ((@O_iINDEX = 0)
                    and (@I_vTYPE > 20))
                  select @O_iINDEX = isnull(ACTINDX,0)
                  from   SY01100 (nolock)
                  where  SERIES = 7
                         and SEQNUMBR = (50 + ((@I_vTYPE - 30) * 3) + ((@I_vPATU - 1) * 100))
                
                if ((@O_iINDEX = 0)
                    and (@I_vTYPE <= 20))
                  select @O_iINDEX = isnull(ACTINDX,0)
                  from   SY01100 (nolock)
                  where  SERIES = 7
                         and SEQNUMBR = (10 + ((@I_vTYPE - 1) * 3) + ((@I_vPATU - 1) * 100))
              end
            else
              if (@AcctSelect = 5)
                begin
                  select @O_iINDEX = isnull(PAACTINDX,0)
                  from   PA43001 (nolock)
                  where  PAsfid = 26
                         and PArecordid = @I_vPACOSTCATID
                         and PAcosttrxid = ''
                         and PAaccttype = @I_vTYPE
                  
                  if ((@O_iINDEX = 0)
                      and (@I_vTYPE > 20))
                    select @O_iINDEX = isnull(ACTINDX,0)
                    from   SY01100 (nolock)
                    where  SERIES = 7
                           and SEQNUMBR = (50 + ((@I_vTYPE - 30) * 3) + ((@I_vPATU - 1) * 100))
                  
                  if ((@O_iINDEX = 0)
                      and (@I_vTYPE <= 20))
                    select @O_iINDEX = isnull(ACTINDX,0)
                    from   SY01100 (nolock)
                    where  SERIES = 7
                           and SEQNUMBR = (10 + ((@I_vTYPE - 1) * 3) + ((@I_vPATU - 1) * 100))
                end
              else
                if (@AcctSelect = 6)
                  begin
                    select @PAsfid = case @I_vPATU 
                                       when 1 then 22
                                       when 2 then 23
                                       when 3 then 24
                                       when 4 then 25
                                       when 5 then 25
                                     end
                    
                    select @O_iINDEX = isnull(PAACTINDX,0)
                    from   PA43001 (nolock)
                    where  PAsfid = @PAsfid
                           and PArecordid = @I_vEMPLOYID
                           and PAcosttrxid = ''
                           and PAaccttype = @I_vTYPE
                    
                    if ((@O_iINDEX = 0)
                        and (@I_vTYPE > 20))
                      select @O_iINDEX = isnull(ACTINDX,0)
                      from   SY01100 (nolock)
                      where  SERIES = 7
                             and SEQNUMBR = (50 + ((@I_vTYPE - 30) * 3) + ((@I_vPATU - 1) * 100))
                    
                    if ((@O_iINDEX = 0)
                        and (@I_vTYPE <= 20))
                      select @O_iINDEX = isnull(ACTINDX,0)
                      from   SY01100 (nolock)
                      where  SERIES = 7
                             and SEQNUMBR = (10 + ((@I_vTYPE - 1) * 3) + ((@I_vPATU - 1) * 100))
                  end
                else
                  if (@AcctSelect = 7)
                    begin
                      select @O_iINDEX = isnull(PAACTINDX,0)
                      from   PA43001 (nolock)
                      where  PAsfid = 29
                             and PArecordid = @I_vPAPROJNUMBER
                             and PAcosttrxid = @I_vPACOSTCATID
                             and PAaccttype = @I_vTYPE
                      
                      if (@O_iINDEX <> 0)
                        begin
                          select @ACTINDX = isnull(ACTINDX,0),
                                 @ACTIVE = isnull(ACTIVE,0)
                          from   GL00100 (nolock)
                          where  ACTINDX = @O_iINDEX
                          
                          if ((@ACTINDX = 0)
                               or (@ACTIVE = 0))
                            begin
                              select @O_iErrorState = 1801
                              
                              return @O_iErrorState
                            end
                        end
                      
                      return
                    end
      
      select @MAXSEG = max(SGMTNUMB)
      from   SY00300 (nolock)
      
      if ((@O_iINDEX <> 0)
          and (@OverrideSegment <> 0))
        begin
          select @ACTINDX = 0,
                 @ACCNT_STRING = '',
                 @NEW_ACCNT_STRING = ''
          
          select @ACCNT_STRING = ACTNUMST
          from   GL00105 (nolock)
          where  ACTINDX = @O_iINDEX
          
          declare PASegmentOveride INSENSITIVE cursor  for
          select isnull(SGMTNUMB,0),
                 isnull(SGMTNAME,''),
                 isnull(SGMNTID,'')
          from   PA41302 (nolock)
          WHERE  PAPROJNUMBER = @I_vPAPROJNUMBER
          
          open PASegmentOveride
          
          select @iCursorError = @@cursor_rows
          
          if (@iCursorError > 0)
            begin
              fetch next from PASegmentOveride
              into @SGMTNUMB,
                   @SGMTNAME,
                   @SGMNTID
              
              while (@@fetch_status <> -1)
                begin
                  if (@@fetch_status = -2)
                    begin
                      select @O_iErrorState = 3473
                      
                      break
                    end
                  
                  if (@SGMNTID <> '')
                    begin
                      if (@SGMTNUMB < @MAXSEG)
                        begin
                          select @LOFSGMNT = sum(LOFSGMNT) + (@MAXSEG - @SGMTNUMB)
                          from   SY00300 (nolock)
                          where  SGMTNUMB >= @SGMTNUMB
                          
                          select @LOFSGMNTEND = sum(LOFSGMNT) + (@MAXSEG - @SGMTNUMB)
                          from   SY00300 (nolock)
                          where  SGMTNUMB > @SGMTNUMB
                        end
                      else
                        begin
                          select @LOFSGMNT = sum(LOFSGMNT)
                          from   SY00300 (nolock)
                          where  SGMTNUMB = @SGMTNUMB
                        end
                      
                      select @LOFSGMNTALL = sum(LOFSGMNT) + (@MAXSEG - 1)
                      from   SY00300 (nolock)
                      
                      if (@SGMTNUMB = @MAXSEG)
                        begin
                          select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@SGMNTID))
                        end
                      else
                        begin
                          select @NEW_ACCNT_STRING = substring(@ACCNT_STRING,1,@LOFSGMNTALL - @LOFSGMNT) + rtrim(ltrim(@SGMNTID)) + substring(@ACCNT_STRING,@LOFSGMNTALL - @LOFSGMNTEND + 1,
                                                                                                                                              @LOFSGMNTALL)
                        end
                      
                      select @ACCNT_STRING = @NEW_ACCNT_STRING
                    end
                  
                  fetch next from PASegmentOveride
                  into @SGMTNUMB,
                       @SGMTNAME,
                       @SGMNTID
                end
            end
          
          deallocate PASegmentOveride
          
          select @OverideINDEX = ACTINDX
          from   GL00105 (nolock)
          where  ACTNUMST = @ACCNT_STRING
          
          if (@OverideINDEX = 0)
            begin
              select @O_iErrorState = 7324
              
              return @O_iErrorState
            end
          else
            begin
              select @ACTINDX = isnull(ACTINDX,0),
                     @ACTIVE = isnull(ACTIVE,0)
              from   GL00100 (nolock)
              where  ACTINDX = @OverideINDEX
              
              if ((@ACTINDX = 0)
                   or (@ACTIVE = 0))
                begin
                  select @O_iErrorState = 7325
                  
                  return @O_iErrorState
                end
              else
                begin
                  select @O_iINDEX = @OverideINDEX
                end
            end
        end
      
      if (@AcctSelect > 1)
         and (@O_iINDEX = 0)
        begin
          select @O_iErrorState = 2055
          
          return @O_iErrorState
        end
    end
  
  return (@O_iErrorState)

