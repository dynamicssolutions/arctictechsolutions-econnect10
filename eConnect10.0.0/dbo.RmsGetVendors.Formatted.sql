

if exists (select *
           from   dbo.sysobjects
           where  id = object_id(N'[dbo].[RmsGetVendors]')
                  and objectproperty(id,N'IsProcedure') = 1)
  drop procedure [dbo].[RmsGetVendors]
  
create procedure [dbo].[RmsGetVendors]
                @iVendorTempTable VARCHAR(255),
                @iRmsServerName   VARCHAR(255),
                @iHqDbName        VARCHAR(255),
                @iReImport        TINYINT  /*with encryption*/
AS
  SET ANSI_WARNINGS ON
  
  DECLARE  @lSqlError INT
  
  DECLARE  @lSupplierTable VARCHAR(255)
  
  SELECT @lSupplierTable = @iRmsServerName + '.' + @iHqDbName + '.dbo.Supplier as Supplier'
  
  EXEC( 'INSERT INTO ' + @iVendorTempTable + '(VENDORID,  RmsVendorID,  VENDNAME,  VNDCNTCT,  ADDRESS1,  ADDRESS2,  CITY,  STATE,  ZIP,  COUNTRY,  PHONE1,  FAX,  INET1,  INET2,  MARKED,  RmsIntegrationNotes)  SELECT  ISNULL(LEFT(UPPER(Code), 15),''''),  ID,  ISNULL(SupplierName,''''),  ISNULL(ContactName,''''),  ISNULL(Address1,''''),  ISNULL(Address2,''''),  ISNULL(City,''''),  ISNULL(State,''''),  ISNULL(LEFT(Zip, 11),''''),  ISNULL(Country,''''),  ISNULL(LEFT(replace(replace(replace(replace(PhoneNumber,''('',''''),'')'',''''),''-'',''''),'' '',''''), 21),''''),  ISNULL(LEFT(replace(replace(replace(replace(FaxNumber,''('',''''),'')'',''''),''-'',''''),'' '',''''), 21),''''),  ISNULL(LEFT(EmailAddress, 201),''''),  ISNULL(LEFT(WebPageAddress, 201),''''),  1,  ''''  FROM ' + @lSupplierTable)
  
  IF @iReImport = 0
    BEGIN
      EXEC( 'DELETE ' + @iVendorTempTable + ' FROM  RMS30100 JOIN ' + @iVendorTempTable + ' ON RMS30100.[RmsVendorID] = ' + @iVendorTempTable + '.RmsVendorID')
    END

